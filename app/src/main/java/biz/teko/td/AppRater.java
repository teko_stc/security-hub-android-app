package biz.teko.td;

import android.app.Activity;

import com.google.android.play.core.review.ReviewInfo;
import com.google.android.play.core.review.ReviewManager;
import com.google.android.play.core.review.ReviewManagerFactory;
import com.google.android.play.core.tasks.Task;

/**
 * Created by td13017 on 19.10.2017.
 */

public class AppRater
{

	private OnRateResultListener onRateResultListener;

	public interface OnRateResultListener{
		void onResult();
	}

	public void setOnRateResultListener(OnRateResultListener onRateResultListener){
		this.onRateResultListener = onRateResultListener;
	}

	private final static int DAYS_UNTIL_PROMPT = 2;
	private final static int LAUNCHES_UNTIL_PROMPT = 20;
	private static long launchCount;
	private static long dateFirstLaunch;

//	public static void appLaunched(Context context) {
//		SharedPreferences prefs = context.getSharedPreferences("apprater", 0);
//		if (Func.getBooleanSPDefFalse(prefs, "dontshowagain"))
//			return;
//
//		SharedPreferences.Editor editor = prefs.edit();
//
//		// Increment launch counter
//		launchCount = prefs.getLong("launch_count", 0) + 1;
//		editor.putLong("launch_count", launchCount);
//
//		// Get date of first launch
//		dateFirstLaunch = prefs.getLong("date_firstlaunch", 0);
//		if (dateFirstLaunch == 0) {
//			editor.putLong("date_firstlaunch", System.currentTimeMillis());
//		}
//
//		editor.commit();
//	}

	public static void review(Activity activity, OnRateResultListener onRateResultListener) {
		ReviewManager manager = ReviewManagerFactory.create(activity);
		Task<ReviewInfo> request = manager.requestReviewFlow();
		request.addOnCompleteListener(task -> {
			if (task.isSuccessful()) {
				ReviewInfo reviewInfo = task.getResult();
				Task<Void> flow = manager.launchReviewFlow(activity, reviewInfo);
				flow.addOnCompleteListener(result -> {
					if (result.isSuccessful()){
						if(null!=onRateResultListener) onRateResultListener.onResult();
					}
				});
			}
		});
	}

//	public static void reviewApp(Activity activity) {
//		// Wait at least n days before opening dialog
//		if (launchCount >= LAUNCHES_UNTIL_PROMPT) {
//			if (System.currentTimeMillis() >= dateFirstLaunch + (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000))
//				review(activity, activity.getResources().getString(R.string.THANK_FOR_REVIEW));
//		}
//	}

//	public static void showRateDialog(final Context context, final SharedPreferences.Editor editor) {
//		final AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
//		final AlertDialog ad = adb.create();
//		ad.setTitle(context.getResources().getString(R.string.application_name));
//
//		View v = ((Activity)context).getLayoutInflater().inflate(R.layout.rate_dialog_view, null);
//
//		TextView message = (TextView) v.findViewById(R.id.rate_message);
//
//		String mess = context.getResources().getString(R.string.RATE_DIALOG_MESS_1) + " " + context.getResources().getString(R.string.application_name) + context.getResources().getString(R.string.RATE_DIALOG_MESS_2);
//		message.setText(mess);
//
//		Button buttonOk = (Button) v.findViewById(R.id.buttonRate);
//		Button buttonLater = (Button) v.findViewById(R.id.buttonLater);
//		Button buttonCancel = (Button) v.findViewById(R.id.buttonCancel);
//
//		buttonOk.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(View v)
//			{
//				if(editor != null)
//				{
//					editor.putBoolean("dontshowagain", true);
//					editor.commit();
//					ad.dismiss();
//				}
//				context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
//			}
//		});
//
//		buttonLater.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(View v)
//			{
//				if(editor !=null)
//				{
//					editor.putLong("date_firstlaunch", System.currentTimeMillis());
//					editor.putLong("launch_count", 0);
//					editor.commit();
//					ad.dismiss();
//				}
//			}
//		});
//
//		buttonCancel.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(View v)
//			{
//				if (editor != null) {
//					editor.putBoolean("dontshowagain", true);
//					editor.commit();
//					ad.dismiss();
//				}
//			}
//		});
//
//		ad.setView(v);
//		ad.show();
//	}
}
