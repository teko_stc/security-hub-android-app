package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.TextView;

public class AutoFitTextView extends TextView {

	final static float INCREMENT = 10f; // size range factor
	final static float DELTA = 1f; // successive approximation window in pixels
	final static int ITERATIONS = 10; // maximum iterations limit

	int height = 0;
	int width = 0;
	private float minimum, maximum;

	public AutoFitTextView(Context context) {
		super(context);
		constructor();
	}

	public AutoFitTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		constructor();
	}

	protected void constructor() {
		this.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
			@Override
			public boolean onPreDraw() {
				fit();
				return true;
			}
		});
		minimum = this.getTextSize();
		maximum = minimum * INCREMENT;
	}

	protected void fit() {
		int height = this.getHeight();
		int width = this.getWidth();

		if ((height != this.height) || (width != this.width)) {
			this.height = height;
			this.width = width;

			float minimum = this.minimum;
			float maximum = this.maximum;

			int iterations = ITERATIONS;

			do {
				float median = (minimum)+ (maximum - minimum) / 4;
				this.setTextSize(android.util.TypedValue.COMPLEX_UNIT_PX, median);
				this.measure(0, 0);

				if ((this.getMeasuredHeight() < height) && (this.getMeasuredWidth() < width)) {
					minimum = median;
				}
				else {
					maximum = median;
				}
			}
			while ((--iterations > 0) && ((maximum - minimum) > DELTA));

			this.setTextSize(android.util.TypedValue.COMPLEX_UNIT_PX, minimum);
		}
	}
}