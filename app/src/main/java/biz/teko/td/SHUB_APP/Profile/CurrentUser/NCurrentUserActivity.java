package biz.teko.td.SHUB_APP.Profile.CurrentUser;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Profile.Activities.NProfileLogOutActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

public  class NCurrentUserActivity extends BaseActivity
{
	private Context context;
	private boolean bound;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private int sending = 0;
	private DBHelper dbHelper;
	private UserInfo userInfo;
	private NMenuListElement name;
	private TextView login;
	private TextView roles;
	private NMenuListElement pass;
	private NMenuListElement logOut;
	private LinearLayout buttonBack;

	private BroadcastReceiver operatorChangedReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -99));
		}
	};

	private BroadcastReceiver operatorUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			bind();
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_current_user);
		context = this;
		dbHelper = DBHelper.getInstance(context);

		setup();
	}

	private void bind()
	{
		if(null!=buttonBack) buttonBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		userInfo = dbHelper.getUserInfo();

		if(null!=userInfo){
			Operator operator = dbHelper.getCurrentOperator(userInfo);
			if(null!=name){
				name.setTitle(operator.name);
				if(!((userInfo.roles&Const.Roles.DOMAIN_ADMIN)==0 && (userInfo.roles&Const.Roles.DOMAIN_HOZ_ORG)==0)){
					name.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NRenameActivity.class);
							intent.putExtra("type", Const.USER);
							intent.putExtra("name", operator.name);
							intent.putExtra("user", operator.id);
							intent.putExtra("transition",true);
							startActivity(intent, getTransitionOptions(name).toBundle());
						}
					});
				}else{
					name.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Func.nShowMessage(context, getString(R.string.N_NO_PERMISSION));
						}
					});
				}
			}
			if(null!=login){
				login.setText(userInfo.userLogin);
			}
			if(null!=roles){
				roles.setText(getResources().getStringArray(R.array.user_roles)[userInfo.getRoleValue()]);
			}
			if(null!=pass){
				if(!((userInfo.roles&Const.Roles.DOMAIN_ADMIN)==0 && (userInfo.roles&Const.Roles.DOMAIN_HOZ_ORG)==0))
				{
					pass.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							showPassChangeDialog();
						}
					});
				}else{
					pass.setEnabled(false);
				}
			}
			if(null!=logOut){
				logOut.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						startActivity(new Intent(context, NProfileLogOutActivity.class), getTransitionOptions(logOut).toBundle());
//						NDialog nDialog = new NDialog(context, R.layout.n_dialog_question);
//						nDialog.setTitle(getString(R.string.N_CURRENT_USER_LOGOUT_MESSAGE));
//						nDialog.setOnActionClickListener(new NDialog.OnActionClickListener() {
//							@Override
//							public void onActionClick(int value) {
//								switch (value){
//									case NActionButton.VALUE_CANCEL:
//										nDialog.dismiss();
//										break;
//									case NActionButton.VALUE_OK:
//										Func.log_d("D3BaseActivity", "LOGOUT");
//										Intent manualLogoutIntent  = new Intent(D3Service.BROADCAST_CONNECTION_DROP);
//										manualLogoutIntent.putExtra("AppName", context.getPackageName());
//										manualLogoutIntent.putExtra("base", true);
//										context.sendBroadcast(manualLogoutIntent);
//										break;
//								}
//							}
//						});
//						nDialog.show();
					}
				});
			}
		}
	}

	private void showPassChangeDialog()
	{
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_change);
		nDialog.setTitle(getString(R.string.PA_PW_CHANGE));
		View view = getLayoutInflater().inflate(R.layout.n_change_pass_view, null);
		nDialog.addView(view);

		NEditText editPass = (NEditText) view.findViewById(R.id.nEditPass);
		NEditText repeatPass = (NEditText) view.findViewById(R.id.nEditRepeatPass);
		TextInputLayout editPassInput = (TextInputLayout) view.findViewById(R.id.nEditPassInput);
		TextInputLayout editRepeatInput = (TextInputLayout) view.findViewById(R.id.nEditRepeatInput);
		//		NActionButton buttonPaste = (NActionButton) view.findViewById(R.id.nButtonPaste);

		if(null!=editPass)
		{
			editPass.setFocusableInTouchMode(true);
			editPass.setText("");
			editPass.setSelection(editPass.getText().length());
			editPass.showKeyboard();
			editPass.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					editPassInput.setError(null);
				}

				@Override
				public void afterTextChanged(Editable s)
				{

				}
			});
			editPass.setOnKeyListener(new View.OnKeyListener()
			{
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if((event.getAction() == KeyEvent.ACTION_DOWN)&& (keyCode == KeyEvent.KEYCODE_ENTER)){
						repeatPass.setSelection(repeatPass.getText()!=null ? repeatPass.getText().toString().length() : 0);
					}
					return false;
				}
			});
		}
		if(null!=repeatPass){
			repeatPass.setFocusableInTouchMode(true);
			repeatPass.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					editRepeatInput.setError(null);
				}

				@Override
				public void afterTextChanged(Editable s)
				{
					String result = s.toString().replaceAll(" ", "");
					if (!s.toString().equals(result))
					{
						repeatPass.setText(result);
						repeatPass.setSelection(result.length());
					}
				}
			});
			repeatPass.setOnKeyListener(new View.OnKeyListener()
			{
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
					{
						return changePass(editPass, repeatPass);
					}
					return false;
				}

				private boolean changePass(NEditText editPass, NEditText repeatPass) {
					if (validate(editPass)) {
						if (validate(repeatPass)) {
							if (editPass.getText().toString().equals(repeatPass.getText().toString())) {
								editPass(editPass.getText().toString());
								nDialog.dismiss();
								return true;
							} else {
								repeatPass.showKeyboard();
								editRepeatInput.setError(" ");
							}
						} else {
							repeatPass.showKeyboard();
							editRepeatInput.setError(" ");
						}
					} else {
						editPass.showKeyboard();
						editPassInput.setError(" ");
					}
					return false;
				}
			});
		}

		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value)
				{
					case NActionButton.VALUE_OK:
						changePass(editPass, repeatPass);
						break;
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}

			private void changePass(NEditText editPass, NEditText repeatPass) {
				if (validate(editPass)) {
					if (validate(repeatPass)) {
						if (editPass.getText().toString().equals(repeatPass.getText().toString())) {
							editPass(editPass.getText().toString());
							nDialog.dismiss();
						} else {
							repeatPass.showKeyboard();
							editRepeatInput.setError(" ");
						}
					} else {
						repeatPass.showKeyboard();
						editRepeatInput.setError(" ");
					}
				} else {
					editPass.showKeyboard();
					editPassInput.setError(" ");
				}
			}
		});
		nDialog.show();
	}

	private boolean validate(NEditText name)
	{
		return (null!=name.getText() && !name.getText().toString().equals(""));
	}


	private void editPass(String pass)
	{
		JSONObject message = new JSONObject();
		try
		{
			Operator operator = dbHelper.getCurrentOperator(userInfo);
			message.put("id", operator.id);
			message.put("password", Func.md5(pass));
			nSendMessageToServer(null, D3Request.OPERATOR_SET, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void setup()
	{
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		name = (NMenuListElement) findViewById(R.id.nMenuName);
		login = (TextView) findViewById(R.id.nTextLogin);
		roles = (TextView) findViewById(R.id.nTextPerm);
		pass = (NMenuListElement) findViewById(R.id.nMenuChangePass);
		logOut = (NMenuListElement) findViewById(R.id.nMenuExit);
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((NCurrentUserActivity)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		bind();
		bindD3();
		registerReceiver(operatorChangedReceiver, new IntentFilter(D3Service.BROADCAST_OPERATOR_SET));
		registerReceiver(operatorUpdateReceiver, new IntentFilter(D3Service.BROADCAST_OPERATORS));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if(bound)unbindService(serviceConnection);
		unregisterReceiver(operatorChangedReceiver);
		unregisterReceiver(operatorUpdateReceiver);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};
}
