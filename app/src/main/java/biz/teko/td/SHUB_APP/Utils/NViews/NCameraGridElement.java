package biz.teko.td.SHUB_APP.Utils.NViews;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Handler;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public  class NCameraGridElement extends LinearLayout
{
	private final  static  String DEFAULT_URL = "https://openapi-alpha-eu01.ivideon.com/cameras/";
	private final static String DEFAULT_ATTR = "/live_preview?access_token=";

	private int layout;
	private final Context context;
	private boolean increase = true;
	private boolean clickable = true;

	private ImageView imageBigView;
	private ImageView imagePreview;
	private TextView titleText;
	private TextView subTitleText;
	private TextView textBig;
	private ProgressBar imageProgress;

	private int image;
	private String title;
	private String subtitle;
	private Camera camera;
	private String token;
	private OnElementListener onElementListener;

	private boolean pressed;
	private final Handler handler = new Handler();
	private final Runnable runnable = new Runnable()
	{
		public void run()
		{
			checkPressedState();
		}
	};
	private boolean loading = false;

	public void setOnElementClickListener(OnElementListener onElementListener){
		this.onElementListener = onElementListener;
	}

	public interface OnElementListener
	{
		void onClick();
		void onLongClick();
		void onLoaded();
	}

	public NCameraGridElement(Context context)
	{
		super(context);
		this.context = context;
		setup();
		bind();
	}


	public NCameraGridElement(Context context, int layout)
	{
		super(context);
		this.context = context;
		this.layout = layout;
		setup();
		bind();
	}

	public NCameraGridElement(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setup();
		bind();
	}

	private void setup()
	{
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(layout, this, true);

		imageBigView = findViewById(R.id.nImageBarBig);
		imagePreview = findViewById(R.id.nImagePreview);
		imageProgress = findViewById(R.id.pb_search);

		textBig = findViewById(R.id.nTextBarBig);

		titleText = findViewById(R.id.nTextTitle);
		subTitleText = findViewById(R.id.nTextBarSubTitle);
	}

	private void bind()
	{
		setTitleView();
		setSubTitleView();
		setBigImageView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NCameraGridElement, 0,0);
		try{
			layout = a.getResourceId(R.styleable.NCameraGridElement_NCameraGridElementLayout, 0);
		}finally
		{
			a.recycle();
		}
	}

	private void setTitleView()
	{
		if (null != titleText)
		{
			if (null != title)
			{
				titleText.setText(title);
			}
		}
	}

	private void setSubTitleView()
	{
		if(null!= subTitleText){
			if(null!=subtitle){
				subTitleText.setText(subtitle);
				subTitleText.setVisibility(VISIBLE);
			}
		}
	}

	private void setBigImagePreview(){
		if(null!=imagePreview){
			switch (camera.type){
				case IV:
					if(null!=camera.id && null!=token){
						imagePreview.setVisibility(VISIBLE);
						imageProgress.setVisibility(VISIBLE);
						String url = DEFAULT_URL + camera.id + DEFAULT_ATTR + token;
						Picasso.with(context)
								.load(url)
								.memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
								.into(imagePreview, new Callback() {
									@Override
									public void onSuccess() {
										//do something when picture is loaded successfully
										if(null!=imageProgress) imageProgress.setVisibility(GONE);
										//							if(null!=onElementListener) onElementListener.onLoaded();
									}

									@Override
									public void onError() {
										//do something when there is picture loading error
									}
								});
						if(null!=imageBigView) imageBigView.setVisibility(GONE);
					}
					break;
				case DAHUA:
					if(null!=camera.sn)
					{
						imagePreview.setVisibility(VISIBLE);
						imageProgress.setVisibility(VISIBLE);
						if (camera.preview != null)
						{
							if (null != imageProgress) imageProgress.setVisibility(GONE);
//							Bitmap newBmp = zoomBitmap(Func.getImage(camera.preview), imagePreview.getWidth(), imagePreview.getHeight());
							imagePreview.setImageBitmap(Func.scaleToFitWidth(Func.getImage(camera.preview), 144));
						}
						if(null!=imageBigView) imageBigView.setVisibility(GONE);

					}
					break;
			}
		}
	}

	public Bitmap zoomBitmap(Bitmap bitmap, int w, int h) {
		if(0!=w && 0!=h)
		{
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();
			Matrix matrix = new Matrix();

			float scaleWidth = ((float) w) / width;
			float scaleHeight = ((float) h) / height;
			matrix.postScale(scaleWidth, scaleHeight);

			bitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
		}

		return bitmap;
	}

	private void setBigImageView()
	{
		if(null!= imageBigView){
			if(0!=image){
				imageBigView.setImageDrawable(getResources().getDrawable(image));
				if(null!=textBig)textBig.setVisibility(GONE);
			}
		}
	}

	public void setTitle (String title){
		this.title = title;
		setTitleView();
	}

	public void setSubTitle(String subTitle){
		this.subtitle = subTitle;
		setSubTitleView();
	}

	public void setPreview(Camera camera, String token){
		this.camera = camera;
		this.token = token;
		setBigImagePreview();
	}

	public void setIncrease(boolean increase){
		this.increase = increase;
	}

	public  void setClickable(boolean clickable){
		this.clickable = clickable;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (clickable){
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					if(!pressed) {
						handler.postDelayed(runnable, 1000);
						pressed = true;
					}
					animateDown();
					return true;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
					if(pressed){
						pressed = false;
						handler.removeCallbacks(runnable);
						if(event.getAction() == MotionEvent.ACTION_UP
								&& null!=onElementListener) {
							onElementListener.onClick();
						}
					}
					animateUp();

					return true;
				case MotionEvent.ACTION_MOVE:
					return true;
				default:
					if(pressed)
					{
						pressed = false;
						handler.removeCallbacks(runnable);
					}
					animateUp();
			}
		}
		return false;
	}


	private void checkPressedState()
	{
		if(pressed){
			pressed = false;
			if(null!=onElementListener) {
				onElementListener.onLongClick();
				((Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(10);
			}
		}
	}

	private void animateUp()
	{
		AnimatorSet regainer = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.regain_size);
		regainer.setTarget(this);
		regainer.start();
	}

	private void animateDown()
	{
		AnimatorSet reducer = (AnimatorSet) AnimatorInflater.loadAnimator(context, increase ? R.animator.increase_size_1_1 : R.animator.reduce_size);
		reducer.setTarget(this);
		reducer.start();
	}
}
