package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.LinkedList;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;

public class ScriptsShortListAdapter extends ArrayAdapter
{
	private final Context context;
	private final DBHelper dbHelper;
	private final int resource;
	private final LinkedList<Script> scripts;
	private final LayoutInflater layoutInflater;
	private final SharedPreferences sp;
	private final String iso;

	public ScriptsShortListAdapter(Context context, int resource, LinkedList<Script> scripts)
	{
		super(context, resource);
		this.context = context;
		this.resource = resource;
		this.dbHelper = DBHelper.getInstance(context);
		this.scripts = scripts;
		this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));
	}

	@NonNull
	@Override
	public View getView(int position,View convertView, ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(resource, parent, false);
		}
		Script script = getItem(position);
		TextView textView = (TextView) view.findViewById(R.id.textName);
		textView.setText(script.nameLib != null ? Func.getScriptDataForCurrLang(script.nameLib, iso)  : context.getString(R.string.SCRIPTS_UNKNOWN_SCRIPT));
		return view;
	}

	public int getCount() {
		return scripts.size();
	}

	@Override
	public Script getItem(int position){
		return scripts.get(position);
	}

	public long getItemId(int position){
		return position;
	}
}
