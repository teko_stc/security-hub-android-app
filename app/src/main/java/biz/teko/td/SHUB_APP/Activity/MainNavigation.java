package biz.teko.td.SHUB_APP.Activity;

import android.content.Context;
import android.view.MenuItem;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomNavigationView;

public abstract class MainNavigation extends BaseActivity {
    @Override
    protected void onResume() {
        super.onResume();
        setBottomInfoBehaviour(this);
    }

    public void setBottomNavigationItem(int itemId){
        if (prevItem!= -1) {
            MenuItem item = bottomNavigation.getMenu().findItem(prevItem);
            item.setChecked(false);
        }
        MenuItem item = bottomNavigation.getMenu().findItem(itemId);
        item.setChecked(true);
    }

    protected NBottomNavigationView bottomNavigation;
    protected int prevItem = -1;

    public void setBottomNavigation(int currentId, Context context){
        bottomNavigation = (NBottomNavigationView) findViewById(R.id.nBottomMenu);
        bottomNavigation.inflateMenu(R.menu.bottom_menu);
        prevItem = currentId;
        MenuItem item = bottomNavigation.getMenu().findItem(currentId);
        item.setChecked(true);
        bottomNavigation.setOnNavigationItemSelectedListener(item1 -> {
            bottomNavigation.post(() -> {
                int itemId = item1.getItemId();
                if (currentId != itemId) {
                    switch (itemId)
                    {
                        case R.id.bottom_navigation_item_favorited:
                            openMain();
                            break;
                        case R.id.bottom_navigation_item_devices:
                            openNewDevices();
                            break;
                        case R.id.bottom_navigation_item_messages:
                            openHistory();
                            break;
                        case R.id.bottom_navigation_item_cameras:
                            openCameras();
                            break;
                        case R.id.bottom_navigation_item_menu:
                            openProfile();
                            break;

                    }
                    overridePendingTransition(0, 0);
                    finish();
                }
            });
            return true;
        });
        setBottomInfoBehaviour(bottomNavigation, findViewById(android.R.id.content), context);
    }

    public void setBottomInfoBehaviour(Context context){
        setBottomInfoBehaviour((NBottomNavigationView) findViewById(R.id.nBottomMenu), this.findViewById(android.R.id.content), context);
    }


    /*BOTTOM MENU*/

    public abstract void openMain();

    public abstract void openNewDevices();

    public abstract void openHistory();

    public abstract void openScripts();

    public abstract void openCameras();

    public abstract void openProfile();

}
