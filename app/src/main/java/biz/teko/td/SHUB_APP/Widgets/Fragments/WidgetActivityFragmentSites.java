package biz.teko.td.SHUB_APP.Widgets.Fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Widgets.Adapters.ArrayAdapters.WidgetActivityColorsAdapter;
import biz.teko.td.SHUB_APP.Widgets.Adapters.ArrayAdapters.WidgetActivitySitesAdapter;
import biz.teko.td.SHUB_APP.Widgets.Adapters.ArrayAdapters.WidgetActivityTranspAdapter;
import biz.teko.td.SHUB_APP.Widgets.Classes.ColorX;
import biz.teko.td.SHUB_APP.Widgets.Classes.Transparency;
import biz.teko.td.SHUB_APP.Widgets.WidgetConfigActivity;

/**
 * Created by td13017 on 24.05.2017.
 */

public class WidgetActivityFragmentSites extends Fragment
{
	Context context;
	DBHelper dbHelper;
	View view;
	private WidgetConfigActivity activity;

	private int site_id = -1;
	private String background = "fffffff";
	private String coloring = "ff";
	private String transping = "ffffff";

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		activity = (WidgetConfigActivity) getActivity();



		view = inflater.inflate(R.layout.widget_fragment_sites, container, false);

		Spinner sitesSpinner = (Spinner) view.findViewById(R.id.wfs_spinner_set_site);
		Spinner colorSpinner = (Spinner) view.findViewById(R.id.wfs_spinner_set_color);
		Spinner transparencySpinner = (Spinner) view.findViewById(R.id.wfs_spinner_set_transparency);

		String[] color = new String[]{"FFFFFF", "000000"};
		String[] colorsString = new String[]{getString(R.string.WHITE), getString(R.string.BLACK)};

		ColorX[] colors = new ColorX[2];
		for(int i = 0; i < colors.length; i++){
			colors[i] = new ColorX(color[i], colorsString[i]);
		}

		String[] transp = new String[]{"FF", "F2", "E6", "D9", "CC", "BF", "B3", "A6", "99", "8C", "80", "73", "66", "59", "4D", "40", "33", "26", "1A", "0D", "00"};
		String[] transpString = new String[]{"100%", "95%", "90%", "85%", "80%", "75%", "70%", "65%", "60%", "55%", "50%", "45%", "40%", "35%", "30%", "25%", "20", "15%", "10%", "5%", "0%"};

		Transparency[] transparencies = new Transparency[21];
		for(int i = 0; i < transparencies.length; i++){
			transparencies[i] = new Transparency(transp[i], transpString[i]);
		}

		final WidgetActivityColorsAdapter widgetActivityColorsAdapter = new WidgetActivityColorsAdapter(context, R.layout.spinner_item, colors);
		final WidgetActivityTranspAdapter widgetActivityTranspAdapter = new WidgetActivityTranspAdapter(context, R.layout.spinner_item, transparencies);

		colorSpinner.setAdapter(widgetActivityColorsAdapter);
		colorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
			{
				coloring = widgetActivityColorsAdapter.getItem(position).hx;
				activity.setColorFromFragment("#" + transping + coloring);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});
		transparencySpinner.setAdapter(widgetActivityTranspAdapter);
		transparencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
			{
				transping = widgetActivityTranspAdapter.getItem(position).hx;
				activity.setColorFromFragment("#" + transping + coloring);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		Site[] sites = dbHelper.getAllSitesArray();
		final WidgetActivitySitesAdapter widgetActivitySitesAdapter = new WidgetActivitySitesAdapter(context, R.layout.spinner_item, sites);
		sitesSpinner.setAdapter(widgetActivitySitesAdapter);
		sitesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
			{
				site_id = widgetActivitySitesAdapter.getItem(position).id;
				activity.setSiteIdFromFragment(site_id);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent)
			{

			}
		});
		activity.setSiteIdFromFragment(widgetActivitySitesAdapter.getSelectedItem().id);
		activity.setColorFromFragment("#" + transping + coloring);

		//
//		ListView sitesList = (ListView) view.findViewById(R.id.sites_list_for_widget);
//		Site[] sites = dbHelper.getSitesArray();
//		if(sites!=null)
//		{
//			final WidgetActivitySitesAdapter widgetActivitySitesAdapter = new WidgetActivitySitesAdapter(context, R.layout.widget_activity_list_element, sites);
//			sitesList.setAdapter(widgetActivitySitesAdapter);
//			//set  first value of site_id
//			activity.setSiteIdFromFragment(widgetActivitySitesAdapter.getSelectedItem().id);
//		}

		return view;
	}
}
