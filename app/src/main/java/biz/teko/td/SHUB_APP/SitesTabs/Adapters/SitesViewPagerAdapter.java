package biz.teko.td.SHUB_APP.SitesTabs.Adapters;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import biz.teko.td.SHUB_APP.SitesTabs.Fragments.SiteHistoryTabFragment;
import biz.teko.td.SHUB_APP.SitesTabs.Fragments.SitesTabFragment;

/**
 * Created by td13017 on 07.02.2017.
 */

public class SitesViewPagerAdapter extends FragmentStatePagerAdapter
{
	private CharSequence Titles[];
	private int NumOfTabs;
	private SiteHistoryTabFragment siteHFragment;
	private SitesTabFragment siteTFragment;

	public SitesViewPagerAdapter(FragmentManager fm, CharSequence titles[], int num){
		super(fm);
		this.Titles = titles;
		this.NumOfTabs = num;
	}

	@Override
	public Fragment getItem(int position)
	{
		if(position == 0){
			SitesTabFragment sitesTabFragment  = new SitesTabFragment();
			return sitesTabFragment;
		}else{
			return new SiteHistoryTabFragment();
		}
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		return Titles[position];
	}

	@Override
	public int getCount()
	{
		return NumOfTabs;
	}

	// on the ViewPager position.
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
		// save the appropriate reference depending on position
		switch (position) {
			case 0:
				siteTFragment = (SitesTabFragment) createdFragment;
				break;
			case 1:
				siteHFragment = (SiteHistoryTabFragment) createdFragment;
				break;
		}
		return createdFragment;
	}

	public void closeFab(){
		siteTFragment.closeFABMenu();
	}
}
