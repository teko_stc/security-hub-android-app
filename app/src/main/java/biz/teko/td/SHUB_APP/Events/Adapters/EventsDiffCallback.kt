package biz.teko.td.SHUB_APP.Events.Adapters

import androidx.recyclerview.widget.DiffUtil
import biz.teko.td.SHUB_APP.D3DB.Event

//This is the DifUtil component. you can remove it if you don't use it.
class EventsDiffCallback(_oldEventList: List<NEventsListAdapterNew.NEventsListItem>, _newEventList: List<NEventsListAdapterNew.NEventsListItem>): DiffUtil.Callback() {
    private val oldEventList: List<NEventsListAdapterNew.NEventsListItem> = _oldEventList
    private val newEventList: List<NEventsListAdapterNew.NEventsListItem> = _newEventList

    override fun getOldListSize(): Int {
        return oldEventList.size
    }

    override fun getNewListSize(): Int {
        return newEventList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldEventList[oldItemPosition].type == newEventList[newItemPosition].type
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldEvent = oldEventList[oldItemPosition]
        val newEvent = newEventList[newItemPosition]

        return (oldEvent.event == newEvent.event
                && oldEvent.title == newEvent.title)
    }
}