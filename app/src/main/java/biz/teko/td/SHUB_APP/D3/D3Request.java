package biz.teko.td.SHUB_APP.D3;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 01.02.2017.
 */

public class D3Request extends JSONObject
{
	public static final String SITE_SET = "SITE_SET";
	public static final String ZONE_SET = "ZONE_SET";
	public static final String ZONE_DEL = "ZONE_DEL";
	public static final String ZONE_NAME = "ZONE_NAME";
	public static final String DEVICE_ASSIGN_DOMAIN = "DEVICE_ASSIGN_DOMAIN";
	public static final String DEVICE_FORCE_ASSIGN = "DEVICE_FORCE_ASSIGN";
	public static final String DEVICE_REVOKE_DOMAIN = "DEVICE_REVOKE_DOMAIN";
	public static final String DEVICE_ASSIGN_SITE = "DEVICE_ASSIGN_SITE";
	public static final String DEVICE_REVOKE_SITE = "DEVICE_REVOKE_SITE";
	public static final String SCRIPT_SET = "SCRIPT_SET";
	public static final String SCRIPT_DEL = "SCRIPT_DEL";
	public static final String SCRIPTS_DEL = "SCRIPTS_DEL";
	public static final String SCRIPTS = "SCRIPTS";
	public static final String LIBRARY_SCRIPTS = "LIBRARY_SCRIPTS";
	public static final String LIBRARY_SCRIPT_IMPORT = "LIBRARY_SCRIPT_IMPORT";
	public static final String OPERATOR_SET = "OPERATOR_SET";
	public static final String OPERATOR_DEL = "OPERATOR_DEL";
	public static final String SITE_DEL = "SITE_DEL";
	public static final String DELEGATE_NEW = "DELEGATE_NEW";
	public static final String DELEGATE_REQ = "DELEGATE_REQ";
	public static final String EVENTS = "EVENTS";
	public static final String FORCED = "FORCED";
	public static final String IV_GET_TOKEN = "IV_GET_TOKEN";
	public static final String IV_GET_RELATIONS = "IV_GET_RELATIONS";
	public static final String IV_DEL_RELATION = "IV_DEL_RELATION";
	public static final String OPERATOR_SAFE_SESSION = "OPERATOR_SAFE_SESSION";
	public static final String IV_BIND_CAM = "IV_BIND_CAM";
	public static final String IV_DEL_USER = "IV_DEL_USER";
	public static final String DELEGATE_REVOKE = "DELEGATE_REVOKE";
	public static final String SET_LOCALE = "SET_LOCALE";
	public static final String ALARM_OPEN = "ALARM_OPEN";
	public static final String ALARM_CLOSE = "ALARM_CLOSE";
	public static final String SITES = "SITES";
	public static final String DEVICES = "DEVICES";
	public static final String TEMP_TRESHOLDS_GET = "TEMP_TRESHOLDS_GET";


	public int id;
	public D3Element d3Element;
	public String message;
	public String error;
	public int roles;


	public enum RType
	{

	}

	public D3Request(){

	}

	public D3Request(String type, JSONObject data){
		try
		{
			this.put("ID", PrefUtils.getInstance(App.getContext()).getRequestId());
			this.put("Q", type);
			this.put("D", data);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public D3Request(String type, int number){
		try
		{
			this.put("Q", type);
			this.put("ID", number);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public D3Request(String type, JSONObject data, int number){
		try
		{
			this.put("Q", type);
			this.put("D", data);
			this.put("ID", number);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public static D3Request createMessage(int roles, String mess_type, JSONObject messageAddress)
	{
		D3Request d3Request = new D3Request(mess_type, messageAddress);
		switch (mess_type){


			case ZONE_SET:
			case ZONE_DEL:
			case ZONE_NAME:
			case DEVICE_ASSIGN_DOMAIN:
			case DEVICE_FORCE_ASSIGN:
			case DEVICE_REVOKE_DOMAIN:
			case DEVICE_ASSIGN_SITE:
			case DEVICE_REVOKE_SITE:
			case SCRIPT_SET:
			case SCRIPT_DEL:
			case SCRIPTS_DEL:
				if(((roles&Const.Roles.DOMAIN_ENGIN)==0)&&((roles& Const.Roles.ORG_ENGIN)==0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case SCRIPTS:
				if((roles&Const.Roles.TRINKET)!=0
						|| ((roles&Const.Roles.DOMAIN_OPER)==0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
			case LIBRARY_SCRIPTS:
				if((roles&Const.Roles.TRINKET)!=0
						|| (((roles&Const.Roles.DOMAIN_ENGIN)==0)&&((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.SERVER_ADMIN)==0)))
				{
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case LIBRARY_SCRIPT_IMPORT:
				if((roles&Const.Roles.SERVER_ADMIN)==0){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case OPERATOR_SET:
			case OPERATOR_DEL:
				if((roles&Const.Roles.DOMAIN_ADMIN)==0 &&(roles&Const.Roles.DOMAIN_HOZ_ORG)==0)
				{
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case SITE_SET:
			case SITE_DEL:
			case DELEGATE_NEW:
			case DELEGATE_REQ:
				if(((roles&Const.Roles.DOMAIN_LAWYER)==0)&&((roles&Const.Roles.DOMAIN_LAWYER)==0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case EVENTS:
				d3Request = new D3Request(mess_type, messageAddress, D3Service.EVENTS_REQUEST_ID);
				if((roles&Const.Roles.DOMAIN_OPER) == 0){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case FORCED:
			case IV_GET_TOKEN:
			case IV_GET_RELATIONS:
				break;
			case IV_DEL_RELATION:
				if((roles&Const.Roles.DOMAIN_ADMIN)==0)
				{
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case OPERATOR_SAFE_SESSION:
				if((roles&Const.Roles.DOMAIN_OPER) == 0){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case IV_BIND_CAM:
				if((roles&Const.Roles.DOMAIN_ADMIN)==0)
				{
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case IV_DEL_USER:
				if((roles&Const.Roles.DOMAIN_ADMIN)==0)
				{
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case DELEGATE_REVOKE:
				if((roles&Const.Roles.DOMAIN_ADMIN) == 0){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;
			case SET_LOCALE:
				if((roles&Const.Roles.DOMAIN_ADMIN) == 0){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_4_CHAGE_DOMAIN_LANG);
				}
				break;
			case ALARM_CLOSE:
			case ALARM_OPEN:
				if((roles&Const.Roles.DOMAIN_ADMIN)==0)
				{
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES);
				}
				break;

			case SITES:
			case DEVICES:
				break;
			default:
				d3Request.error = App.getContext().getString(R.string.COMMAND_UNKNOWN_TYPE);
				break;
		}
		return d3Request;
	}

	public static  D3Request createCommand(int roles, int device_id, String command, JSONObject params, int id){
		D3Request d3Request = new D3Request("COMMAND_SET", id);
		switch (command){
			case Command.ARM:
			case Command.DISARM:
			case Command.RELAY:
			case Command.SWITCH:
			case Command.RESET:
				if((roles& Const.Roles.DOMAIN_HOZ_ORG)==0){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_TO_CONTROL);
				}
				break;
			case Command.ZONE_SET:
			case Command.ZONE_DEL:
				if(((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.DOMAIN_ENGIN)== 0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_TO_SET_DEL_ZONES);
				}
				break;
			case Command.USER_SET:
			case Command.USER_DEL:
				if(((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.DOMAIN_ENGIN)== 0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_TO_SET_CODES);
				}
				break;
			case Command.SECTION_SET:
				if(((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.DOMAIN_ENGIN)== 0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_TO_REGISTER_SEC);
				}
				break;
			case Command.SECTION_DEL:
				if(((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.DOMAIN_ENGIN)== 0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_TO_DEREGISTER_SEC);
				}
				break;
			case Command.REGISTER:
				if(((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.DOMAIN_ENGIN)== 0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_TO_REGISTER);
				}
				break;
			case Command.DEREGISTER:
				if(((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.DOMAIN_ENGIN)== 0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_TO_DEREGISTER);
				}
				break;
			case Command.UPDATE:
			case Command.UPDATE_APPLY:
			case Command.REBOOT:
			case Command.USSD:
				if(((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.DOMAIN_ENGIN)== 0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_TO_USSD);
				}
				break;
			case Command.INTERACTIVE:
				if(((roles&Const.Roles.ORG_ENGIN)==0)&&((roles&Const.Roles.DOMAIN_ENGIN)== 0)){
					d3Request.error = App.getContext().getString(R.string.ERROR_NO_RULES_FOR_REGISTRATION);
				}
				break;
			default:
				d3Request.error = App.getContext().getString(R.string.COMMAND_UNKNOWN_TYPE);
				break;
		}

		if (null == d3Request.error)
		{
			JSONObject commandObject = new JSONObject();
			JSONObject sendObject = new JSONObject();
			try
			{
				if(null!=params)
				{
					commandObject.put(command, params);
				}else{
					commandObject.put(command, 1);
				}
				sendObject.put("command", commandObject.toString());
				sendObject.put("device", device_id);
				d3Request.put("D", sendObject);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return d3Request;
	}

	public void setQ (String type){
		try
		{
			this.put("Q:", type);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}


	public void send(Context context, D3Service service, int sending)
	{

	}

}

