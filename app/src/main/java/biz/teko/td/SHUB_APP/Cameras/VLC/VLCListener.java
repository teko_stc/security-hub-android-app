package biz.teko.td.SHUB_APP.Cameras.VLC;

public interface VLCListener {
    void onComplete();
    void onError();
}
