package biz.teko.td.SHUB_APP.Wizards.Activity.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.View;
import android.widget.ImageView;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 07.06.2017.
 */

public class WizardWelcomeActivity extends BaseActivity {
    private static final String LOGIN_ERROR_SESSION_EXIST_STRING = "Session exist";
    private static final long TIMEOUT = 10000;

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_wizard_welcome);
        this.context = this;

        NActionButton buttonLogin = (NActionButton) findViewById(R.id.wizLoginButtonLogin);
		NActionButton buttonReg = (NActionButton) findViewById(R.id.wizButtonReg);
		ImageView logo = (ImageView) findViewById(R.id.nImageLogo);
        if(null!=logo)
            if(!Func.tekoBuild()){
                logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_name));
            }else{
                logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_sh));
            }

        buttonLogin.setOnButtonClickListener(v -> {
			Intent activityIntent = new Intent(getBaseContext(), WizardLoginActivity.class);
			startActivity(activityIntent);
			overridePendingTransition(R.animator.enter, R.animator.exit);
		});

        if (null != buttonReg) {
            if (Func.needRegForBuild()) {
                buttonReg.setOnButtonClickListener(action -> {
                    NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_vertical_small_link);
                    nDialog.setTitle(getResources().getString(R.string.LA_SINGUP_DIALOG_MESS_1) + " " + getResources().getString(R.string.application_name) + " " + getString(R.string.LA_SINGUP_DIALOG_MESS_2));
                    nDialog.setOnActionClickListener((value, b) -> {
                        switch (value){
                            case NActionButton.VALUE_OK:
                                nDialog.dismiss();
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Func.getRegLink(context));
                                startActivity(browserIntent);break;
                            case NActionButton.VALUE_CANCEL:
                                nDialog.dismiss();
                                break;
                        }
                    });
                    nDialog.show();
                });
            }else{
                buttonReg.setVisibility(View.GONE);
            }
        }

//		buttonReg.setEnabled(false);

        if (getIntent().getIntExtra("SERVICE", -1) == D3Service.NEW_PROTOCOL_VERSION) {
            Func.showUpdateDialog(context);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                    PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                    if (!pm.isIgnoringBatteryOptimizations(getPackageName())) {
                        Func.showDozeDialog(context);
                    }
                }
            }

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                if (getIntent().getIntExtra("SERVICE", -1) == D3Service.SERVICE_BEEN_STOPED) {
                    Func.showServiceStopedDialog(context);
                }
            }
            if (getIntent().getIntExtra("SERVICE", -1) == D3Service.CONNECTION_DROP) {
                Func.nShowMessage(context, getResources().getString(R.string.CONNECTION_DROP_MESSAGE)
                        + "\n"
                        + Func.getDate(context, System.currentTimeMillis(), Func.DATE)
                        + " "
                        + Func.getDate(context, System.currentTimeMillis(), Func.TIME));
            }
            if (Func.needLicenseAForBuild()) {
                if (!PrefUtils.getInstance(context).getLicenseAgreed()) {
                    Func.showLicenseAgreementDialog(context);
                }
            }
        }
    }

    private void openLink() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Func.getRegLink(this));
        startActivity(browserIntent);
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        //        if (-1 == getIntent().getIntExtra("FROM", 0)) {
//            finishAffinity();
//        } else {
//            super.onBackPressed();
//        }
    }

}
