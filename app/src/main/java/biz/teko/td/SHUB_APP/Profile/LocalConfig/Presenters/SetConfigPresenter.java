package biz.teko.td.SHUB_APP.Profile.LocalConfig.Presenters;

import android.app.Activity;
import android.os.Bundle;

import java.util.concurrent.TimeUnit;

import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.ISetConfig;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.CustomizableConfig;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SetConfigPresenter {
    private ISetConfig iSetConfig;
    private final DeviceDTO device;
    private Disposable timerDisposable;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final SocketConnection localConnection;
    private boolean isCancel = false, isAccept = false;

    public SetConfigPresenter(SocketConnection localConnection, DeviceDTO deviceDTO1) {
        this.localConnection = localConnection;
        this.device = deviceDTO1;
    }

    public void attachSetActivity(ISetConfig iSetConfig) {
        this.iSetConfig = iSetConfig;
        disposeTimerConditional();
    }

    private void disposeTimerConditional() {
        disposables.add(localConnection.getDeviceSubject().subscribe(udpDevice -> {
            if (udpDevice.ip.equals(device.getIp())) {
                if (udpDevice.mode == 0) {
                    if (isAccept) {
                        timerDisposable.dispose();
                        iSetConfig.dismissProgressBar(true);
                        iSetConfig.close(Activity.RESULT_OK);
                    }
                }
            }
        }));
    }

    public void cancel() {
        isCancel = true;
    }

    private void sendConfig(byte[] data) {
        disposables.add(localConnection.getSendConfigSubject().subscribe(aBoolean -> {
            if (aBoolean) {
                isAccept = true;
                sendTestModeOff();
            }
        }));
        localConnection.sendConfig(device.getIp(), device.getPin(), data);
        iSetConfig.showProgressBar();
        timerDisposable = getTimerObservable().subscribe(aLong -> iSetConfig.dismissProgressBar(false));
    }

    public void setConfig(Bundle configBundle, String configType, int hubVersion) {
        CustomizableConfig replyConfig = null;
        switch (hubVersion) {
            case (1):
                replyConfig = localConnection.getConfig1h();
                break;
            case (2):
                replyConfig = localConnection.getConfig();
                break;
            case (3):
                replyConfig = localConnection.getConfigNg();
                break;
            case (4):
                replyConfig = localConnection.getConfig1hNg();
                break;
            default:
                break;
        }
        if (replyConfig != null) {
            replyConfig.setNewAnotherConfig(configBundle, configType);
            sendConfig(replyConfig.getData());
        }
    }

    private Observable<Long> getTimerObservable() {
        return Observable
                .timer(15, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    public void sendReplyTimer() {
        Observable.interval(3, TimeUnit.SECONDS)
                .delay(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> localConnectionSendSurveyRequest()).isDisposed();
    }

    private void localConnectionSendSurveyRequest() {
        if (!localConnection.isClosed()) {
            localConnection.sendSurveyRequest();
        }
    }

    private void sendTestModeOff() {
        localConnection.sendTestModeOff(device.getIp(), device.getPin());
        device.setTestMode(false);
    }
    public void sendTestModeOn() {
        localConnection.sendTestModeOn(device.getIp(), device.getPin());
        device.setTestMode(true);
    }

    public void detachSetActivity() {
        iSetConfig = null;
        disposables.dispose();
    }

    public boolean isCancel() {
        return isCancel;
    }

    public boolean isAccept() {
        return isAccept;
    }
}
