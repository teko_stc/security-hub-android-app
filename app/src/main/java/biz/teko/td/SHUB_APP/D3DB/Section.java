package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;

/**
 * Created by td13017 on 15.07.2016.
 */
public class Section extends D3Element
{
	public int device_id;
	public int detector;
	public String name;
	public Zone[] zones;
	public boolean checked = false;
	public long armed;
	public Extra extra = null;
	public String high_limit;
	public String  low_limit;
	public int zone_count;

	public Section(){

	}

	public Section(int id, String name, Zone[] zones, int detector){
		this.id = id;
		this.name = name;
		this.zones = zones;
		this.detector = detector;
	}

	public Section(int id, String name, int device_id, Zone[] zones, int detector, long armed, Extra extra)
	{
		this.id = id;
		this.name = name;
		this.device_id = device_id;
		this.zones = zones;
		this.detector = detector;
		this.armed = armed;
		this.extra = extra;
	}

	public Section(Cursor cursor)
	{
		this.name = cursor.getString(2);
		this.id = cursor.getInt(3);
		this.device_id = cursor.getInt(4);
		this.detector = cursor.getInt(8);
		this.armed = cursor.getLong(9);
	}

	public Section(Cursor cursor, String extra_type)
	{
		this.name = cursor.getString(2);
		this.id = cursor.getInt(3);
		this.device_id = cursor.getInt(4);
		this.detector = cursor.getInt(8);
		this.armed = cursor.getLong(9);
		String e_name = cursor.getString(12);
		String e_value = cursor.getString(13);
		if(null!=e_name && null!=e_value)
		{
			this.extra = new Extra(e_name, e_value);
			if (null != this.extra) {
				switch(extra_type)
				{
					case Const.EXTRAS.TEMP_THRESHOLDS:
						low_limit = extra.getLowLimit();
						high_limit = extra.getTopLimit();
						break;
					default:
						break;
				}
			}
		}
	}

	public Section(int device_id, int section_id)
	{
		this.id =section_id;
		this.device_id = device_id;
	}

	public Section(int type)
	{
		this.detector = type;
	}

	public SType getType()
	{
		if(null!= D3Service.d3XProtoConstEvent){
			return  D3Service.d3XProtoConstEvent.getSTypeById(this.detector);
		}
		return null;
	}
}
