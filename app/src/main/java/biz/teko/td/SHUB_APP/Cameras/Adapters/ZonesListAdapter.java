package biz.teko.td.SHUB_APP.Cameras.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 27.02.2018.
 */

public class ZonesListAdapter extends ArrayAdapter<Zone>
{
	private final Zone[] zones;
	private final LayoutInflater layoutInflater;
	private final Context context;
	private int curZone = -1;

	public ZonesListAdapter(Context context, int resource, Zone[] zones, int curZone)
	{
		super(context, resource);
		this.context = context;
		this.zones = zones;
		this.curZone = curZone;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		Zone zone = zones[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(zone.name);
		if(curZone ==  zone.id){
			textView.setTextColor(context.getResources().getColor(R.color.brandColorAccentColor));
		}else{
			textView.setTextColor(context.getResources().getColor(R.color.brandColorTextTitle));
		}
		return view;
	}

	public int getCount() {
		return zones.length;
	}

	public Zone getItem(int position){
		return zones[position];
	}

	public long getItemId(int position){
		return position;
	}
}
