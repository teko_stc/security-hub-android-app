package biz.teko.td.SHUB_APP.Fragments;

import android.app.Dialog;
import androidx.fragment.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;

public class NInfoDialogFragment extends DialogFragment {
    private NDialog dialog;
    private TextView titleView, messView;
    private String title, mess, cancelText, okText;
    private NActionButton cancel, ok;
    private boolean isCancelGone, isTitleGone;
    private IButtonClick listener;

    public interface IButtonClick {
        void cancel();
        void confirm();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        bind();
        setup();
        return dialog;
    }

    private void bind()
    {
        dialog = new NDialog(getContext(), R.layout.n_dialog_info_fragment);
        titleView = dialog.findViewById(R.id.nDialogTitle);
        messView = dialog.findViewById(R.id.nDialogMess);
        ok = dialog.findViewById(R.id.nDialogButtonOk);
        cancel = dialog.findViewById(R.id.nDialogButtonCancel);
    }

    private void setup() {
        if (title != null) titleView.setText(title);
        if (mess != null) messView.setText(mess);
        if (isTitleGone)
            titleView.setVisibility(View.GONE);
        if(null!=cancel)
        {
            if (cancelText != null) cancel.setText(cancelText);
            if (isCancelGone) cancel.setVisibility(View.GONE);
            cancel.setOnButtonClickListener(action -> listener.cancel());
        }
        if(null!=ok)
        {
            ok.setOnButtonClickListener(action -> {
                dismiss();
                listener.confirm();
            });
            if (okText != null) ok.setText(okText);
        }
    }

    public void setListener(IButtonClick listener) {
        this.listener = listener;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTextOk(String okText) {
        this.okText = okText;
    }

    public void setTextCancel(String cancelText) {
        this.cancelText = cancelText;
    }

    public void setCancelGone(boolean isGone) {
        isCancelGone = isGone;
    }

    public void setTitleGone(boolean isGone) {
        isTitleGone = isGone;
    }
}
