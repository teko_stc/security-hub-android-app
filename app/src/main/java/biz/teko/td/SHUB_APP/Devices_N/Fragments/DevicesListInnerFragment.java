package biz.teko.td.SHUB_APP.Devices_N.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.Devices_N.Adapters.DevicesListRecyclerAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Wizards.Activity.WizardAllActivity;

public class DevicesListInnerFragment extends Fragment {
	private Context context;
	private Filter filter = new Filter("");
	private DBHelper dbHelper;
	private int site;
	private Cursor cursor;
	private DevicesListRecyclerAdapter devicesListAdapter;
	private RecyclerView devicesList;
	private NWithAButtonElement noElementsView;

	public static DevicesListInnerFragment newInstance(int siteId, Filter filter) {

		DevicesListInnerFragment devicesListInnerFragment = new DevicesListInnerFragment();
		devicesListInnerFragment.setSiteId(siteId);
		devicesListInnerFragment.setFilter(filter);
		return devicesListInnerFragment;
	}

	public Filter getFilter() {
		return filter;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
		if (dbHelper != null && devicesListAdapter != null) {
			updateCursor(filter);
			devicesListAdapter.update(cursor);
			devicesListAdapter.setFilter(filter);
		}
	}

	public Cursor getCursor() {
		return cursor;
	}

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			devicesListAdapter.setSynchronous(true);
			updateDevicesList();
		}
	};

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
		devicesListAdapter.update(cursor);
	}

	public void setSiteId(int id) {
		Bundle b = new Bundle();
		b.putInt("site", id);
		setArguments(b);
		site = id;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.n_fragment_devices_list, container, false);
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		site = getArguments().getInt("site");

		noElementsView= v.findViewById(R.id.noElementsView);

		devicesList = v.findViewById(R.id.nDevicesList);
		return v;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	public void updateDevicesList() {
		updateCursor(filter);
		updateView();
	}

	public void updateContents() {
		updateCursor(filter);
		if (null != devicesListAdapter)
			devicesListAdapter.update(cursor);
	}


	private void updateView() {
		if (null != devicesListAdapter) {
			devicesListAdapter.update(cursor);
		} else {
			devicesListAdapter = new DevicesListRecyclerAdapter(context, cursor, (DevicesListFragment) getParentFragment(), true, true);
			devicesListAdapter.setFilter(filter);
			devicesList.setAdapter(devicesListAdapter);
		}
		refreshViewVisibility();
	}

	private void refreshViewVisibility()
	{
		if (null != cursor && 0 < cursor.getCount()) {
			if (null != noElementsView) noElementsView.setVisibility(View.GONE);
			if (null != devicesList) devicesList.setVisibility(View.VISIBLE);
		} else if (filter.getQuery().isEmpty()) {
			if (filter.getQuery().isEmpty() && null != noElementsView) {
				noElementsView.setVisibility(View.VISIBLE);
				if(0 != dbHelper.getSitesCount())
				{
					noElementsView.setTitle(getString(R.string.N_DEVICES_MESSAGE_NO_DEVICES));
					noElementsView.setActionTitle(getString(R.string.N_DEVICES_BUTTON_SET));
				}else{
					noElementsView.setTitle(getString(R.string.N_MAIN_EMPTY));
					noElementsView.setActionTitle(getString(R.string.N_MAIN_EMPTY_START));
				}
				noElementsView.setOnChildClickListener((action, option) -> startActivity(new Intent(context, WizardAllActivity.class)));
			}
			if (null != devicesList) devicesList.setVisibility(View.GONE);
		}
	}

	private void updateCursor(Filter filter) {

		if (filter.getQuery().isEmpty()) {
			cursor = dbHelper.getNAllElementsCursorSiteOrder(site);
		} else
			cursor =
					dbHelper.getNAllElementsCursorSiteOrderFilteredWithAllControllers(site, filter.getQuery());
	}


	public void onResume() {
		super.onResume();
		if (devicesListAdapter != null)
			devicesListAdapter.setSynchronous(devicesListAdapter.getItemCount() != 0);
		updateDevicesList();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_ADD);
//		intentFilter.addAction(D3Service.BROADCAST_EVENT);
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		getActivity().registerReceiver(updateReceiver, intentFilter);
		setupTutorial();
	}

	private void setupTutorial() {
	}

	public void startDeviceActivityTutorial() {
		//devicesListAdapter.openTutorialDevice(cursor, devicesList);
	}

	public void onPause(){
		super.onPause();
		getActivity().unregisterReceiver(updateReceiver);
	}
}
