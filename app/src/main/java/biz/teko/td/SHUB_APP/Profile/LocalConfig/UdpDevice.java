package biz.teko.td.SHUB_APP.Profile.LocalConfig;

public class UdpDevice
{
	public String version;
	public  int mode;
	public int serial;
	public int config;
	public String ip;

	public  UdpDevice(){

	}

	public UdpDevice(int mode, String ip, int serial, int config, String version){
		this.mode = mode;
		this.ip = ip;
		this.serial = serial;
		this.config=config;
		this.version = version;
	}
}
