package biz.teko.td.SHUB_APP.SectionsTabs.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.Adapter.NRListAdapter;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SectionsTabs.Adapters.SectionsTreeAdapter;
import biz.teko.td.SHUB_APP.SectionsTabs.SectionsActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardExitSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardRelaySetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardZoneSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardDevicesSpinnerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardSTypesSpinnerAdapter;

/**
 * Created by td13017 on 10.02.2017.
 */

public class SectionsTabFragment extends Fragment
{
	private DBHelper dbHelper;
	private Context context;
	private int previousItem = -1;
	private int siteId;
	private SectionsTreeAdapter sectionsTreeAdapter;
	private  ExpandableListView expandableListView;
	private D3Service myService;
	private int sending = 0;
	private FloatingActionButton fab;
	private FloatingActionButton fab1;
	private FloatingActionButton fab2;
	private FloatingActionButton fab3;
	private FloatingActionButton fab4;
	private FrameLayout fabLayout;
	private FrameLayout fabLayout1;
	private FrameLayout fabLayout2;
	private FrameLayout fabLayout3;
	private FrameLayout fabLayout4;
	private TextView fabText1;
	private TextView fabText2;
	private TextView fabText3;
	private TextView fabText4;
	private boolean isFABOpen = false;

	private AlertDialog.Builder nrDialogBuilder;
	private AlertDialog nrDialog;
	private NRListAdapter nrListAdapter;
	private ListView nrList;
	private LinkedList<Event> eventsNRList = new LinkedList<>();
	private int deviceId;

	public static SectionsTabFragment newInstance(int siteId, int deviceId){
		SectionsTabFragment sectionsTabFragment = new SectionsTabFragment();
		Bundle b = new Bundle();
		b.putInt("site", siteId);
		b.putInt("device", deviceId);
		sectionsTabFragment.setArguments(b);
		return sectionsTabFragment;
	}

	private BroadcastReceiver zoneSetReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if( result > 0){
				Func.pushToast(context, context.getString(R.string.SUCCESS_SET), (Activity) context);
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
			}
		}
	};

	private BroadcastReceiver affectsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(sectionsTreeAdapter!=null){
				Cursor sectionsCursor  = getRefreshedSectionsCursor();
				sectionsTreeAdapter.update(sectionsCursor, sending);
			}
		}
	};

	private BroadcastReceiver sitesUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(sectionsTreeAdapter!=null){
				Cursor sectionsCursor  = getRefreshedSectionsCursor();
				sectionsTreeAdapter.update(sectionsCursor, sending);
			}
		}
	};


	private BroadcastReceiver zoneDeleteReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result == 1)
			{
				Func.pushToast(context, getString(R.string.SECTION_DELETE_SUCCESS), (Activity) context);
			}else{
				Func.pushToast(context,Func.handleResult(context, result), (Activity) context);
			}
		}
	};

	private BroadcastReceiver notReadyReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int eventId = intent.getIntExtra("eventId", -1);
			Event event = dbHelper.getEventById(eventId);
			if(null!=event)
			{
				if (deviceId != -1)
				{
					if (deviceId == event.device)
					{
						eventsNRList.add(event);
						showNotReadyMessage(context);
					}
				} else
				{
					if (siteId != -1)
					{
						Device[] devices = dbHelper.getAllDevicesForSite(siteId);
						if (null != devices && devices.length != 0)
						{
							int count = 0;
							for (Device device : devices)
							{
								if (event.device == device.id)
								{
									count++;
								}
							}
							if (count > 0)
							{
								eventsNRList.add(event);
								showNotReadyMessage(context);
							}
						}
					}
				}
			}
		}
	};

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, getActivity().findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
						} else
						{
							Func.pushToast(context, context.getString(R.string.COMMAND_BEEN_SEND), (Activity) context);
						}
						break;
					case -1:
						Func.pushToast(context, context.getString(R.string.COMMAND_NOT_BEEN_SEND), (Activity) context);
						sending = 0;
						break;
					default:
						sending = 0;
						break;
				}
			}
			Cursor sectionsCursor  = getRefreshedSectionsCursor();
			sectionsTreeAdapter.update(sectionsCursor, sending);
			if(null!=fab){
				if(sending == 0){
					fab.setEnabled(true);
				}else{
					fab.setEnabled(false);
				}
			}
		}
	};

	public Cursor getRefreshedSectionsCursor(){
		Cursor refreshedCursor;
		if(deviceId != -1){
			refreshedCursor = dbHelper.getSectionsCursorForSiteDevice(siteId, deviceId);
		}else{
			refreshedCursor =dbHelper.getSectionsCursorForSite(siteId);
		}
		return refreshedCursor;
	}


	@Override
	public void onResume()
	{
		super.onResume();
		getActivity().registerReceiver(affectsReceiver, new IntentFilter(D3Service.BROADCAST_AFFECT));
		getActivity().registerReceiver(sitesUpdateReceiver, new IntentFilter(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
		getActivity().registerReceiver(zoneDeleteReceiver, new IntentFilter(D3Service.BROADCAST_ZONE_DEL));
		getActivity().registerReceiver(notReadyReceiver, new IntentFilter(D3Service.BROADCAST_SITE_NOT_READY));
		getActivity().registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		getActivity().registerReceiver(zoneSetReceiver, new IntentFilter(D3Service.BROADCAST_ZONE_SET));

		if(expandableListView!=null){
			if(sectionsTreeAdapter!=null){
				Cursor cursor = getRefreshedSectionsCursor();
				sectionsTreeAdapter.update(cursor, sending);
			}
		}
		eventsNRList.clear();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(affectsReceiver);
		getActivity().unregisterReceiver(sitesUpdateReceiver);
		getActivity().unregisterReceiver(zoneDeleteReceiver);
		getActivity().unregisterReceiver(notReadyReceiver);
		getActivity().unregisterReceiver(commandResultReceiver);
		getActivity().unregisterReceiver(commandSendReceiver);
		getActivity().unregisterReceiver(zoneSetReceiver);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.tab_site_sections, container, false);
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		siteId = getArguments().getInt("site");
		deviceId = getArguments().getInt("device");
		UserInfo userInfo = dbHelper.getUserInfo();

		expandableListView = (ExpandableListView) v.findViewById(R.id.sectionsList);
		expandableListView.setGroupIndicator(null);
		Cursor sectionsCursor  = getRefreshedSectionsCursor();
		sectionsTreeAdapter = new SectionsTreeAdapter(context, sectionsCursor, R.layout.card_section, R.layout.card_section_expanded, R.layout.card_affect, R.layout.card_affect, siteId, deviceId);
		expandableListView.setAdapter(sectionsTreeAdapter);

//		expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//			@Override
//			public void onGroupExpand(int groupPosition) {
//				if(previousItem == -1){
//					expandableListView.setBackgroundColor(getResources().getColor(R.color.brandColorLightPattern));
//				}
//				previousItem = groupPosition;
//			}
//		});

		fabLayout = (FrameLayout) v.findViewById(R.id.fabLayout);
		fabLayout1 = (FrameLayout) v.findViewById(R.id.fabLayout1);
		fabLayout2 = (FrameLayout) v.findViewById(R.id.fabLayout2);
		fabLayout3 = (FrameLayout) v.findViewById(R.id.fabLayout3);
		fabLayout4 = (FrameLayout) v.findViewById(R.id.fabLayout4);
		fab = (FloatingActionButton) v.findViewById(R.id.fab);
		fab1 = (FloatingActionButton) v.findViewById(R.id.fab1);
		fab2 = (FloatingActionButton) v.findViewById(R.id.fab2);
		fab3 = (FloatingActionButton) v.findViewById(R.id.fab3);
		//		fab3.setEnabled(false);
		fab4 = (FloatingActionButton) v.findViewById(R.id.fab4);
		fabText1 = (TextView) v.findViewById(R.id.fabText1);
		fabText2 = (TextView) v.findViewById(R.id.fabText2);
		fabText3 = (TextView) v.findViewById(R.id.fabText3);
		fabText4 = (TextView) v.findViewById(R.id.fabText4);

		if(deviceId != -1 ? !Func.getRemoteSetRights(userInfo, dbHelper, siteId, deviceId) : (((userInfo.roles& Const.Roles.DOMAIN_ENGIN)==0)&&((userInfo.roles&Const.Roles.ORG_ENGIN)==0))){
			fabLayout.setVisibility(View.GONE);
//			fab.setVisibility(View.GONE);
		}else
		{
			fab.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(!isFABOpen){
						showFABMenu();
					}else{
						closeFABMenu();
					}
				}
			});
			expandableListView.setOnScrollListener(new AbsListView.OnScrollListener()
			{
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState)
				{

				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
				{
					if (0 != firstVisibleItem && fab.isShown())
					{
						fab.hide();
						fab1.hide();
						fab2.hide();
						fab3.hide();
						fab4.hide();
						fabText3.setVisibility(View.INVISIBLE);
						fabText4.setVisibility(View.INVISIBLE);
						fabLayout.setClickable(false);
					}
					if (0 == firstVisibleItem && !fab.isShown())
					{
						fab.show();
						fab1.show();
						fab2.show();
						fab3.show();
						fab4.show();
						if(isFABOpen)
							closeFABMenu();
					}
				}
			});
			fab4.setOnClickListener(new AddSectionOnClickListener());
			fab3.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					if (dbHelper.isDeviceOnline(deviceId))
					{
						if(0 == dbHelper.getSiteDeviceArmStatus(siteId, deviceId))
						{
							Intent intent = new Intent(context, WizardZoneSetActivity.class);
							intent.putExtra("site_id", siteId);
							intent.putExtra("device_id", deviceId);
							startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
							((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
						}else{
							Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_ZONE_ADD));
						}
					} else
					{
						Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
					}
				}
			});
			fab2.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					if (dbHelper.isDeviceOnline(deviceId))
					{
						if(0 == dbHelper.getSiteDeviceArmStatus(siteId, deviceId))
						{
							Intent intent = new Intent(context, WizardRelaySetActivity.class);
							intent.putExtra("site_id", siteId);
							intent.putExtra("device_id", deviceId);
							startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
							((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
						}else{
							Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_ZONE_ADD));
						}
					} else
					{
						Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
					}
				}
			});
			fab1.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					if (dbHelper.isDeviceOnline(deviceId))
					{
						if(0 == dbHelper.getSiteDeviceArmStatus(siteId, deviceId))
						{
							Intent intent = new Intent(context, WizardExitSetActivity.class);
							intent.putExtra("site_id", siteId);
							intent.putExtra("device_id", deviceId);
							startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
							((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
						}else{
							Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_ZONE_ADD));
						}
					} else
					{
						Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
					}
				}
			});
		}

		expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
		{
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
			{
				return false;
			}
		});

		return v;
	}

	private void showFABMenu(){

		isFABOpen=true;
		setFabInMain();
		fab.animate().rotation(-45);
		fabLayout.setClickable(true);
		fabLayout.setBackgroundColor(getResources().getColor(R.color.brandTransparentWhite50));
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		fabLayout1.animate().translationY(-(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 65, metrics));
		fabLayout2.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 125, metrics));
		fabLayout3.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 185, metrics));
		fabLayout4.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 245, metrics));
		fabText1.setVisibility(View.VISIBLE);
		fabText2.setVisibility(View.VISIBLE);
		fabText3.setVisibility(View.VISIBLE);
		fabText4.setVisibility(View.VISIBLE);
	}

	public void closeFABMenu(){
		isFABOpen=false;
		fab.animate().rotation(0);
		setFabInMain();
		fabText1.setVisibility(View.INVISIBLE);
		fabText2.setVisibility(View.INVISIBLE);
		fabText3.setVisibility(View.INVISIBLE);
		fabText4.setVisibility(View.INVISIBLE);
		fabLayout1.animate().translationY(0);
		fabLayout2.animate().translationY(0);
		fabLayout3.animate().translationY(0);
		fabLayout4.animate().translationY(0);
		fabLayout.setClickable(false);
		fabLayout.setBackgroundColor(getResources().getColor(R.color.brandTransparentWhite00));
	}

	private void setFabInMain(){
		Activity activity = getActivity();
		if(activity != null){
			SectionsActivity sectionsActivity = (SectionsActivity) activity;
			sectionsActivity.setFabStatus(isFABOpen);
		}
	}

	public class AddSectionOnClickListener implements View.OnClickListener
	{
		private int setTypeId;
		private int setDeviceId;
		private View view;

		@Override
		public void onClick(View v)
		{
			closeFABMenu();
			setDeviceId = deviceId;
			final UserInfo userInfo = dbHelper.getUserInfo();

			if(setDeviceId == -1){
				View view = ((SectionsActivity) context).getLayoutInflater().inflate(R.layout.dialog_section_add_big, null);
				showDialog(view, userInfo);
			}else{
				if(dbHelper.isDeviceOnline(setDeviceId)){
					if(0 == dbHelper.getSiteDeviceArmStatus(siteId, deviceId)){
						View view = ((SectionsActivity) context).getLayoutInflater().inflate(R.layout.dialog_section_add, null);
						showDialog(view, userInfo);
					}else {
						Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_SECTION_ADD));
					}
				}else{
					Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
				}

			}
		}

		private void showDialog(final View view, final UserInfo userInfo)
		{
			AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
			adb.setTitle(R.string.SETA_ADD_SECTION_TITLE);
			adb.setMessage(R.string.SETA_ADD_SECTION_MESS);
			adb.setPositiveButton(R.string.ADB_ADD, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{

				}
			});
			adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					Func.hideKeyboard((SectionsActivity) context);
					dialog.dismiss();
				}
			});
			final AlertDialog ad = adb.create();



			final EditText editName = (EditText) view.findViewById(R.id.editName);
			editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
			int editNameMaxLength = 32;
			editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
			editName.setOnKeyListener(new View.OnKeyListener()
			{

				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					// If the event is a key-down event on the "enter" button
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
					{
						// Perform action on Enter key press
						editName.clearFocus();
						return true;
					}
					return false;
				}
			});

			if(null!=D3Service.d3XProtoConstEvent)
			{
				LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
				LinkedList<SType> sTypes = new LinkedList<SType>();
				if(deviceId != -1)
				{
					Device device = dbHelper.getDeviceById(deviceId);
					if (device.canBeSetFromApp())
					{
						for (SType sType : sTypesList)
						{
							if (null != sType)
							{
								if (sType.id != 4 && sType.id != 6)
								{
									sTypes.add(sType);
								}
							}
						}
					} else
					{
						sTypes = sTypesList;
					}

					Spinner sTypeSpinner = (Spinner) view.findViewById(R.id.wizSectionTypeSpinner);
					if (null != sTypeSpinner)
					{
						final WizardSTypesSpinnerAdapter wizardSTypesSpinnerAdapter = new WizardSTypesSpinnerAdapter(context, R.layout.spinner_item_dropdown, sTypes);
						sTypeSpinner.setAdapter(wizardSTypesSpinnerAdapter);
						sTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
						{
							@Override
							public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
							{
								SType sType = wizardSTypesSpinnerAdapter.getItem(position);
								setTypeId = sType.id;
							}

							@Override
							public void onNothingSelected(AdapterView<?> parent)
							{
								SType sType = wizardSTypesSpinnerAdapter.getItem(0);
								setTypeId = sType.id;
							}
						});
					}
				}else{
					Spinner devicesSpinner = (Spinner) view.findViewById(R.id.wizDevicesSpinner);
					Device[] devices  = dbHelper.getAllDevicesForSiteWithRemoteSet(siteId);
					if(null != devices && devices.length != 0)
					{
						final WizardDevicesSpinnerAdapter devicesSpinnerAdapter = new WizardDevicesSpinnerAdapter(context, R.layout.spinner_item_dropdown, devices);
						devicesSpinner.setAdapter(devicesSpinnerAdapter);
						devicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
						{
							@Override
							public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
							{
								Device device = devicesSpinnerAdapter.getItem(position);
								setDeviceId = device.id;
								setSectionSpinner(setDeviceId);
							}

							@Override
							public void onNothingSelected(AdapterView<?> parent)
							{
								Device device = devicesSpinnerAdapter.getItem(0);
								setDeviceId = device.id;
								setSectionSpinner(setDeviceId);
							}


							public void setSectionSpinner(int setDeviceId)
							{

								if(null!=D3Service.d3XProtoConstEvent)
								{

									Spinner sTypeSpinner = (Spinner) view.findViewById(R.id.wizSectionTypeSpinner);
									if (null != sTypeSpinner)
									{
										final LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
										LinkedList<SType> sTypes = new LinkedList<SType>();

										Device device = dbHelper.getDeviceById(setDeviceId);
										if(device.canBeSetFromApp())
										{
											for (SType sType: sTypesList)
											{
												if(null!=sType)
												{
													if (sType.id != 4 && sType.id !=6)
													{
														sTypes.add(sType);
													}
												}
											}
										}else{
											sTypes = sTypesList;
										}
										final WizardSTypesSpinnerAdapter wizardSTypesSpinnerAdapter = new WizardSTypesSpinnerAdapter(context, R.layout.spinner_item_dropdown, sTypes);
										sTypeSpinner.setAdapter(wizardSTypesSpinnerAdapter);
										sTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
										{
											@Override
											public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
											{
												SType sType = wizardSTypesSpinnerAdapter.getItem(position);
												setTypeId = sType.id;
											}

											@Override
											public void onNothingSelected(AdapterView<?> parent)
											{
												SType sType = wizardSTypesSpinnerAdapter.getItem(0);
												setTypeId = sType.id;
											}
										});
									}
								}
							}

						});


					}
					else {
						setDeviceId = 0;
					}
				}
			}
			ad.setView(view);
			ad.show();
			ad.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

			ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					String name = editName.getText().toString();
					if (!name.equals(""))
					{
						if(dbHelper.isDeviceOnline(setDeviceId))
						{
							if (0 == dbHelper.getSiteDeviceArmStatus(siteId, setDeviceId))
							{
								JSONObject commandAddr = new JSONObject();
								try
								{
									commandAddr.put("type", setTypeId);
									commandAddr.put("name", name);
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
								int command_count = Func.getCommandCount(context);
								D3Request d3Request = D3Request.createCommand(userInfo.roles, setDeviceId, "SECTION_SET", commandAddr, command_count);
								if (d3Request.error == null)
								{
									if (sendMessageToServer(null, d3Request, true))
									{
										ad.dismiss();
									}
								} else
								{
									Func.pushToast(context, d3Request.error, (Activity) context);
								}
							} else
							{
								Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_SECTION_ADD));
							}
						}else{
							Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
						}
					} else
					{
						Func.pushToast(context, getString(R.string.SETA_ENTER_SEC_NUM), (SectionsActivity) context);
					}
				}
			});
		}
	}

	private class AddZoneOnClickListener implements View.OnClickListener
	{
		@Override
		public void onClick(View v)
		{
			if(dbHelper.isDeviceOnline( deviceId))
			{
				if ((0 == dbHelper.getSiteDeviceArmStatus(siteId, deviceId)))
				{
					Intent intent = new Intent(context, WizardZoneSetActivity.class);
					intent.putExtra("site_id", siteId);
					startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
				} else
				{
					Func.nShowMessage(context, context.getString(R.string.OBJECT_DISARM_MESSAGE_ZONE_ADD));
				}
			}else{
				Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
			}
		}
	}

	public boolean sendMessageToServer(D3Element d3Element, D3Request d3Request, boolean command)
	{
		Activity activity = (SectionsActivity)context;
		if(activity != null){
			SectionsActivity sectionsActivity = (SectionsActivity) activity;
			return sectionsActivity.sendMessageToServer(d3Element, d3Request, command);
		}
		return false;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.ADD_ZONE_IN_SECTION){
			if(resultCode == getActivity().RESULT_OK){
				int sectionId = data.getIntExtra("section", 0);
				if(0 != sectionId)
				{
					if (null != expandableListView)
					{
						if (null != sectionsTreeAdapter)
						{
							Cursor c = sectionsTreeAdapter.getCursor();
							c.moveToFirst();
							int position = 0;
							boolean diff = true;
							do{
								if(sectionId == c.getInt(3)){
									diff = false;
									position = c.getPosition();
								}
							}while (diff && (c.moveToNext()));
							if(!diff){
								expandableListView.expandGroup(position);
								expandableListView.setSelectedGroup(position);
								expandableListView.setSelection(position);
								expandableListView.smoothScrollToPosition(position);
								expandableListView.requestLayout();
							}
						}
					}
				}
			}
		}
	}

	public void showNotReadyMessage(final Context context)
	{
		if((null!=nrDialog)&&(nrDialog.isShowing())){
			//updateAdapter
			if(nrListAdapter != null){
				nrListAdapter.notifyDataSetChanged();
			}
		}else
		{
			nrDialogBuilder = Func.adbForCurrentSDK(context);
			nrDialog = nrDialogBuilder.create();
			nrDialog.setTitle(R.string.TITLE_FAILED);
			nrDialog.setMessage(getString(R.string.MESS_NOT_READY));

			nrList = new ListView(context);

			UserInfo userInfo = dbHelper.getUserInfo();
			if((userInfo.roles&Const.Roles.DOMAIN_HOZ_ORG)!=0)
			{
				nrDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ADB_RETRY), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						UserInfo userInfo = dbHelper.getUserInfo();
						if ((eventsNRList != null) && (eventsNRList.size() != 0))
						{
							int deviceId = eventsNRList.get(0).device;
							if (dbHelper.isDeviceOnline(deviceId))
							{
								try
								{
									JSONObject commandAddress = new JSONObject();
									commandAddress.put("section", eventsNRList.get(0).section);
									commandAddress.put("zone", 0);
									int command_count = Func.getCommandCount(context);
									D3Request d3Request = D3Request.createCommand(userInfo.roles, deviceId, "ARM", commandAddress, command_count);
									if (d3Request.error == null)
									{
										sendMessageToServer(null, d3Request, true);
									} else
									{
										Func.pushToast(context, d3Request.error, (Activity) context);
									}
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
							} else
							{
								Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
							}
							eventsNRList.clear();
						}
					}
				});
			}

			nrDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.ADB_CANCEL), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
					eventsNRList.clear();
				}
			});

			nrListAdapter = new NRListAdapter(context, eventsNRList);
			nrList.setAdapter(nrListAdapter);
			FrameLayout frameLayout = (FrameLayout) new FrameLayout(context);
			frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			frameLayout.addView(nrList);

			nrDialog.setView(frameLayout);
			nrDialog.show();
		}
	}

}
