package biz.teko.td.SHUB_APP.Profile.LocalConfig.Activities

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiManager
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Lifecycle
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceConfigDto
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments.DeviceFactoryResetFragment
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments.LocalConfigSetFragment
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments.LocalDeviceConfigFragment
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments.LocalDeviceListFragment
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.DeviceConnectionProvider
import biz.teko.td.SHUB_APP.R
import biz.teko.td.SHUB_APP.UDP.SocketConnection
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog


class LocalConfigurationNavigationActivity:  DeviceConnectionProvider, LocalConfigNavigation() {
    private lateinit var socketConnection: SocketConnection
    var device: DeviceDTO? = null
    private var noWifi = false
    private var noWifiDialog: NDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_local_config_navigation)
        val networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()
        (getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager).run{
            registerNetworkCallback(networkRequest, object: ConnectivityManager.NetworkCallback() {
                override fun onLost(network: Network) {
                    noWifi = true
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                        showNoWifiDialog()
                }

                override fun onAvailable(network: Network) {
                    super.onAvailable(network)
                    noWifi = false
                    if (lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
                        noWifiDialog?.dismiss()
                }
            })
        }
        socketConnection =
            SocketConnection((applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager).run { dhcpInfo })
        openDeviceList()
    }

    override fun onResume() {
        super.onResume()
        if (noWifi){
            showNoWifiDialog()
        } else {
            noWifiDialog?.dismiss()
            socketConnection.initialize()
        }
    }

    override fun onPause() {
        super.onPause()
        device?.run {
            socketConnection.sendTestModeOffWithCallback(ip, pin){
                closeSocketConnection()
            }
            isTestMode = false
        } ?: closeSocketConnection()
    }
    override fun showNoWifiDialog(){
        if (noWifiDialog?.isShowing != true)
            noWifiDialog = NDialog(this, R.layout.n_dialog_question_change).apply {
                setTitle(resources.getString(R.string.ERROR_NO_WIFI))
                setSubTitle(resources.getString(R.string.WIFI_SUGGESTION))
                setCanceledOnTouchOutside(false)
                setOnActionClickListener { value: Int, b: Boolean ->
                    when (value) {
                        NActionButton.VALUE_CANCEL -> {
                            dismiss()
                            finish()
                        }
                        NActionButton.VALUE_OK -> {
                            dismiss()
                            startActivity(Intent(Settings.ACTION_SETTINGS))
                        }
                    }
                }
                setOnCancelListener { finish() }
                show()
            }
    }

    override fun getConnection(): SocketConnection = socketConnection

    private fun navigate(fragment: Fragment){
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.nav_host, fragment )
        transaction.commit()
    }

    override fun provideSocketConnection(): SocketConnection {
        return socketConnection
    }

    override fun closeSocketConnection() {
        socketConnection.close()
    }


    override fun openDeviceConfig(deviceDTO: DeviceDTO) {
        this.device = deviceDTO
        navigate(LocalDeviceConfigFragment(this, deviceDTO, socketConnection))
    }

    override fun openDeviceList() {
        device?.let {
            socketConnection.sendTestModeOff(it.ip, it.pin)
        }
        device = null
        navigate(LocalDeviceListFragment(this, socketConnection))
    }

    override fun openConfigCustomization(deviceConfig: DeviceConfigDto) {
        navigate(LocalConfigSetFragment(this, device, socketConnection, deviceConfig))
    }
    override fun openFactoryResetConfirmation(deviceDTO: DeviceDTO) {
        navigate(DeviceFactoryResetFragment(this, device, socketConnection))
    }


}