package biz.teko.td.SHUB_APP.Alarms.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Alarm;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NAlarmTopView;
import biz.teko.td.SHUB_APP.Utils.Other.NListView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class AlarmActivity extends BaseActivity {
    private NAlarmTopView alarmTopFrame;
    private NListView alarmEventsList;
    private int alarm_id;
    private Context context;
    private DBHelper dbHelper;
    private Alarm alarm;

    private int sending = 0;
    private ServiceConnection serviceConnection;
    private D3Service myService;
    private boolean bound;
    private UserInfo userInfo;

    private boolean loading = false;
    private boolean handle;

    private NEventsListAdapter eventsAdapter;

    private BroadcastReceiver alarmOpenCloseReceiver = new BroadcastReceiver(){

        @Override
        public void onReceive(Context context, Intent intent)
        {
            if(alarm_id == intent.getIntExtra("result", 0)){
                if(Const.ALARM_CLOSE_INTENT == intent.getIntExtra("action", -1)){
                    handle = false;
                    Intent intent1 = new Intent();
                    AlarmActivity.this.setResult(RESULT_OK, intent1);
                    finish();
                }else if(Const.ALARM_OPEN_INTENT == intent.getIntExtra("action", -1)){
                    updateTopView();
                }
            }
        }
    };

    private BroadcastReceiver alarmsReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if(alarm_id == intent.getIntExtra("alarm_id", 0)){
                updateTopView();
            }
        }
    };

    private BroadcastReceiver eventReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if(alarm_id == intent.getIntExtra("alarm_id", 0)){
                if(null!=alarmEventsList){
                    updateEventsList();
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_alarm);
        context = this;
        dbHelper = DBHelper.getInstance(context);
        userInfo = dbHelper.getUserInfo();

        alarm_id = getIntent().getIntExtra("alarm", 0);
        if(0!=alarm_id) alarm = dbHelper.getAlarmById(alarm_id);

        setup();
        bind();
    }

    private void setup()
    {
        alarmTopFrame = (NAlarmTopView) findViewById(R.id.nAlarmTopView);
        alarmEventsList = (NListView) findViewById(R.id.nListAlarmEvents);
    }

    private void bind()
    {
        if(null!=alarm)
        {
            updateTopView();
            updateEventsList();
        }
    }

    private void updateTopView()
    {
        Device device = dbHelper.getDeviceById(alarm.device);
        if (null != alarmTopFrame)
        {
            alarmTopFrame.setState(alarm.state);
            alarmTopFrame.setDevice(device.getName() + " " + device.account);
            alarmTopFrame.setSite(dbHelper.getSiteNameByDeviceSection(device.id, 0));
            alarmTopFrame.setOnButtonClickListener(new NAlarmTopView.OnButtonClickListener()
            {
                @Override
                public void onClose()
                {
                    onBackPressed();
                }

                @Override
                public void onHandle()
                {
                    JSONObject json = new JSONObject();
                    if(alarm.state == 0){
                        try
                        {
                            json.put("alarm_id" , alarm_id);
                            json.put("comment", "");
                            json.put("extra", "");
                            nSendMessageToServer(null, D3Request.ALARM_OPEN, json, false);
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }else if(alarm.state == 1){
                        try
                        {
                            json.put("alarm_id", alarm.id);
                            json.put("success" , 11); //TODO get saved result value
                            json.put("comment", "");
                            nSendMessageToServer(null, D3Request.ALARM_CLOSE, json, false);
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }else{
                        Func.showMessage(context, getString(R.string.N_ERROR_STATE_TO_HANDLE_ALARM));
                    }
                    loading = true;
                    alarmTopFrame.setLoading(loading);
                    handle = true;
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            if(handle)
                            {
                                loading = false;
                                alarmTopFrame.setLoading(false);
                                Func.pushToast(context, getString(R.string.N_NO_RESPONCE), (Activity) context);
                            }
                        }
                    }, 10000);
                }
            });
            alarmTopFrame.setButtonVisibility(alarm.state == 0
                    || (alarm.state ==1 && alarm.managed_user == userInfo.id) ?
                    VISIBLE : GONE);
            alarmTopFrame.setLoading(loading);
        }
    }

    private void updateEventsList()
    {
        Cursor cursor = updateCritCursor();
        if(null!=cursor)
            if(null==eventsAdapter)
            {
                eventsAdapter = new NEventsListAdapter((Activity) context, R.layout.n_events_list_element, cursor, 0, false);
                alarmEventsList.setAdapter(eventsAdapter);
            }else{
                eventsAdapter.update(cursor);
            }
    }

    private Cursor updateCritCursor()
    {
        return dbHelper.getEventsCursorByAlarm(alarm_id);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        bindD3();
        registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_ALARM_EVENTS));
        registerReceiver(alarmsReceiver, new IntentFilter(D3Service.BROADCAST_ALARM_ADD_UPD_RMV));
        registerReceiver(alarmOpenCloseReceiver, new IntentFilter(D3Service.BROADCAST_ALARM_OPEN_CLOSE));
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        unbindService(serviceConnection);
        unregisterReceiver(eventReceiver);
        unregisterReceiver(alarmsReceiver);
        unregisterReceiver(alarmOpenCloseReceiver);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }

    private void bindD3(){
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = getServiceConnection();
        bindService(intent, serviceConnection, 0);
    }

    private ServiceConnection getServiceConnection(){
        return new ServiceConnection()
        {
            public void onServiceConnected(ComponentName name, IBinder binder)
            {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name)
            {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
    }

    public D3Service getLocalService()
    {
        if(bound){
            return myService;
        }else{
            bindD3();
            if(null!=myService){
                return myService;
            }
            Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
        }
        return null;

    }

    private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
    {
        return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
    }
}
