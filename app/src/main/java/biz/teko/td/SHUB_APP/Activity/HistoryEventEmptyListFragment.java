package biz.teko.td.SHUB_APP.Activity;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 20.10.2016.
 */
public class HistoryEventEmptyListFragment extends Fragment
{
	View view;

	public static HistoryEventEmptyListFragment newInstance(String message){
		HistoryEventEmptyListFragment historyEventEmptyListFragment = new HistoryEventEmptyListFragment();
		Bundle b  = new Bundle();
		b.putString("Message", message);
		historyEventEmptyListFragment.setArguments(b);
		return historyEventEmptyListFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Context context = container.getContext();
		LinearLayout linearLayout = new LinearLayout(context);
		linearLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		linearLayout.setGravity(Gravity.CENTER);
		linearLayout.setPadding(Func.dpToPx(20, context), Func.dpToPx(20, context), Func.dpToPx(20, context), Func.dpToPx(20, context));

		TextView textView = new TextView(context);
		textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
		textView.setText(getArguments().getString("Message"));
		textView.setGravity(Gravity.CENTER);

		linearLayout.addView(textView);

		view = linearLayout;
		return view;
	}
}
