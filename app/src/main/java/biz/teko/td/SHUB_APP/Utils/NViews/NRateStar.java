package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.R;

public class NRateStar extends FrameLayout
{
	private Context context;
	private int layout;
	private CheckBox star;
	private OnStarClickListener onStarClickListener;

	private int position;
	private boolean checked;


	public interface OnStarClickListener{
		void onClick();
	}

	public void setOnStarClickListener( OnStarClickListener onStarClickListener){
		this.onStarClickListener = onStarClickListener;
	}

	public NRateStar(@NonNull Context context)
	{
		super(context);
	}

	public NRateStar(Context context, int layout){
		super(context);
		this.context = context;
		this.layout = layout;

		setup();
		bind();
	}

	private void setup()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		layoutInflater.inflate(layout, this, true);

		star  = findViewById(R.id.nCheckBox);
	}

	private void bind()
	{
		setStar();
	}

	public void setPosition(int position)
	{
		this.position = position;
	}

	public int getPosition()
	{
		return position;
	}

	public void setChecked(boolean b){
		this.checked = b;
		setStar();
	}

	private void setStar()
	{
		if(null!=star){
			star.setChecked(checked);
			star.setOnTouchListener(new OnTouchListener()
			{
				@Override
				public boolean onTouch(View v, MotionEvent event)
				{
					if(event.getAction() == MotionEvent.ACTION_DOWN)
					{
						if (null != onStarClickListener) onStarClickListener.onClick();
						return true;
					}
					return false;
				}
			});
		}
	}
}
