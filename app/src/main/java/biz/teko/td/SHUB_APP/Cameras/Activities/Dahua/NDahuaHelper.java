package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua;

import android.content.Context;

import com.company.NetSDK.NET_DEVICEINFO_Ex;

import java.util.HashMap;

import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaP2PLoginModule;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NetSDKLib;

public  class NDahuaHelper
{
	public static final String BROADCAST_GET_DAHUA_PREVIEW = "Get dahua preview image";

	private static NDahuaHelper instance;
	private final NetSDKLib netSdk;

	HashMap<String, NDahuaP2PLoginModule> nDahuaP2PLoginModules= new HashMap<>();;

	private final Context context;
	private long loginHandle;
	private NET_DEVICEINFO_Ex deviceInfo;

	public NDahuaHelper(Context context){
		this.context = context;
		this.netSdk = NetSDKLib.getInstance();
		this.nDahuaP2PLoginModules= new HashMap<>();
	}

	public static final NDahuaHelper getInstance(Context context){
		if(null==instance){
			instance = new NDahuaHelper(context);
		}
		return instance;
	}

	public final void init(){
		if(null!=this.netSdk) this.netSdk.init();
	}




//	//Search device
//	public long startSearchDevices(CB_fSearchDevicesCB callback) {
//		return nDahuaCameraSearchModule.startSearchDevices(callback);
//	}
//
//	//Stop search device
//	public void stopSearchDevices() {
//		nDahuaCameraSearchModule.stopSearchDevices();
//	}
//
//	//init dev
//	public int initDevAccount(DEVICE_NET_INFO_EX mDeviceInfo, String username, String password, String mInitPhoneOrMail) {
//		return  nDahuaInitDevAccountModule.initDevAccount(mDeviceInfo, username, password, mInitPhoneOrMail);
//	}
//
//	public boolean initDevAccountByIP(DEVICE_NET_INFO_EX mDeviceInfo, String username, String password, String mInitPhoneOrMail)
//	{
//		return nDahuaInitDevAccountModule.initDevAccountByIP(mDeviceInfo, username, password, mInitPhoneOrMail);
//	}

	//p2p login
//	public boolean isServiceStarted() {
//		return p2pLoginModule.isServiceStarted();
//	}
//
//	public boolean startP2pService(String sn, String login, String pass) {
//		return p2pLoginModule.startP2pService(SRV_ADDRESS, SRV_PORT, login, pass, sn, DEVICE_PORT, login, pass);
//	}
//
//	public boolean stopP2pService() {
//		return p2pLoginModule.stopP2pService();
//	}

//////	public void startLoginTask(Camera camera, CI_login_int ci_login_int){
//////		new P2PLoginTask(camera, ci_login_int).execute();
//////	}
//////
//////	private class P2PLoginTask extends AsyncTask<String, Integer, Boolean>
//////	{
//////		private Camera camera;
//////		private NDahuaP2PLoginModule.CI_login_int ci_login_int;
//////
//////		public P2PLoginTask(Camera camera, NDahuaP2PLoginModule.CI_login_int ci_login_int)
//////		{
//////			this.camera = camera;
//////			this.ci_login_int = ci_login_int;
//////		}
//////
//////		@Override
//////		protected void onPreExecute(){
//////			super.onPreExecute();
//////		}
//////
//////		@Override
//////		protected Boolean doInBackground(String... params) {
//////			boolean result =  false;
//////			if(startP2pService(camera.sn, camera.login, camera.pass)) {
//////				result = login(camera);
//////			}
//////			return result;
//////		}
//////
//////		@Override
//////		protected void onPostExecute(Boolean result){
//////			if(isServiceStarted()) {
//////				if (result) {
//////					NET_DEVICEINFO_Ex info = p2pLoginModule.getDeviceInfo();
//////
//////					camera.logged_in = true;
//////					camera.setLoginHandle(p2pLoginModule.getLoginHandle());
//////					camera.setDeviceInfo(p2pLoginModule.getDeviceInfo());
//////
//////					ci_login_int.callback(camera);
//////				}
//////			}
//////		}
//////	}
////
////	public boolean login(Camera camera) {
////		Func.log_d("P2p login: " + camera.login + "@"  + camera.pass);
////		return p2pLoginModule.login(camera.login, camera.pass);
////	}
//
//	public boolean logout(Camera camera) {
//		Func.log_d("P2p logout");
//		return  p2pLoginModule.logout(camera.getLoginHandle());
//	}

//	//screenshots
//	public boolean localCapturePicture(int nPort, String picFileName) {
//		return nDahuaSnapshotModule.localCapturePicture(nPort, picFileName);
//	}
//
//	public boolean remoteCapturePicture(Camera camera, int chn) { return nDahuaSnapshotModule.remoteCapturePicture(camera.getLoginHandle(), chn); }
//
//	public boolean timerCapturePicture(Camera camera, int chn) {
//		return nDahuaSnapshotModule.timerCapturePicture(camera.getLoginHandle(), chn);
//	}
//
//	public boolean stopCapturePicture(Camera camera, int chn) {
//		return nDahuaSnapshotModule.stopCapturePicture(camera.getLoginHandle(), chn);
//	}
//
//	public void setSnapRevCallBack(CB_fSnapRev snapReceiveCB){
//		nDahuaSnapshotModule.setSnapRevCallBack(snapReceiveCB);
//	}

	public void addP2PModule(String sn, NDahuaP2PLoginModule p2pLoginModule)
	{
		nDahuaP2PLoginModules.put(sn, p2pLoginModule);
	}

	public NDahuaP2PLoginModule getP2PModule(String sn)
	{
		if(null!=nDahuaP2PLoginModules) {
			return nDahuaP2PLoginModules.get(sn);
		}
		return null;
	}

	public HashMap<String, NDahuaP2PLoginModule> getNDahuaP2PLoginModules(){
		return nDahuaP2PLoginModules;
	}
}
