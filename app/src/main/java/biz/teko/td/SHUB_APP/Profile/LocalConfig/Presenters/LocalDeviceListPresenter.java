package biz.teko.td.SHUB_APP.Profile.LocalConfig.Presenters;

import java.util.concurrent.TimeUnit;

import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.ILoadDevice;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;
import biz.teko.td.SHUB_APP.Utils.Callback;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class LocalDeviceListPresenter {
    public static final int DEVICE_CONFIGURATION_REQUEST_TIMEOUT = 20;
    public static final int SURVEY_REQUEST_TIMEOUT = 30;
    private final PublishSubject<Boolean> resetSubject = PublishSubject.create();
    private final CompositeDisposable disposables = new CompositeDisposable();
    private Callback onLoadComplete;
    private ILoadDevice iDeviceActivity = null;
    private final SocketConnection localConnection;
    private String lastIp = "";
    private int lastPin = -1;
    private boolean isTestMode = false;
    private Disposable timerDisposable;
    private boolean isCancel;

    private Disposable surveyTimerDisposable;
    private Disposable surveyDisposable;

    public void setOnLoadComplete(Callback onLoadComplete) {
        this.onLoadComplete = onLoadComplete;
    }

    public LocalDeviceListPresenter(SocketConnection connectionProvider) {
        localConnection = connectionProvider;
    }


    public void startSearchingForDevices(ILoadDevice activity) {
        this.iDeviceActivity = activity;
        disposeTimerDeviceConditional();
    }


    public void sendTestModeOn(Device device, int pin) {
        this.lastPin = pin;
        this.lastIp = device.ip;
        localConnection.sendTestModeOn(device.ip, pin);
        iDeviceActivity.showProgressBar();
        disposables.add(timerDisposable = getTimerObservable(DEVICE_CONFIGURATION_REQUEST_TIMEOUT)
                .subscribe(aLong -> {
                    if (iDeviceActivity != null)
                        iDeviceActivity.dismissProgressBar(false);
                }));
    }



    private void disposeTimerDeviceConditional() {
        disposables.add(localConnection.getDeviceSubject().subscribe(udpDevice -> {
            if (udpDevice.ip.equals(lastIp)) {
                if (udpDevice.mode == 1) {
                    timerDisposable.dispose();
                    if (iDeviceActivity != null && !isTestMode ) {
                        iDeviceActivity.dismissProgressBar(true);
                        iDeviceActivity.startConfigChooseActivity(lastIp, lastPin);
                    }
                    isTestMode = true;
                } else if (udpDevice.mode == 0) {
                    isTestMode = false;
                }
            } else {
                if (null != iDeviceActivity) iDeviceActivity.updateList(udpDevice);
            }
        }));
    }

    private Observable<Long> getTimerObservable(int timeout) {
        return Observable
                .timer(timeout, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void localConnectionSendSurveyRequest() {
        if (!localConnection.isClosed()) {
            localConnection.sendSurveyRequest();

        } else {
            localConnection.initialize();
        }
    }
    public void startSurvey(){
        disposables.add(surveyTimerDisposable = getTimerObservable(SURVEY_REQUEST_TIMEOUT).subscribe(_i -> {
            onLoadComplete.complete();
            cancelSurvey();
        }));
        sendSendSurveyRequestWithInterval(-1);
    }

    public void sendSendSurveyRequestWithInterval(int timeout) {
        Observable<Long> o = Observable.interval(3, TimeUnit.SECONDS)
                .delay(1, TimeUnit.SECONDS);

        if (timeout > 0) {
            o = o.timeout(timeout, TimeUnit.SECONDS);
        }
        surveyDisposable = o.subscribeOn(Schedulers.io())
                .subscribe(aLong -> {
                    localConnectionSendSurveyRequest();
                });
        disposables.add(surveyDisposable);
    }

    public void cancelSurvey() {
        if (surveyDisposable != null)
            surveyDisposable.dispose();
        if (surveyTimerDisposable!= null){
            surveyTimerDisposable.dispose();
        }
    }

    public void detach() {
        lastIp = "";
        lastPin = -1;
        iDeviceActivity = null;
        disposables.clear();
        disposables.dispose();
    }


    public boolean isCancel() {
        return isCancel;
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
    }


}
