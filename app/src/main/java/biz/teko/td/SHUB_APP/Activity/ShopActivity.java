package biz.teko.td.SHUB_APP.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.ServiceConnection;
import android.os.Bundle;

import com.mikepenz.materialdrawer.Drawer;

import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 25.05.2016.
 */
public class ShopActivity extends BaseActivity
{
	private final  static int activityPosition = 4;
	private ServiceConnection serviceConection;
	private Context context;
	private Drawer drawerResult;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity_faq);
		getSwipeBackLayout().setEnableGesture(false);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public void onBackPressed()
	{
		if ((drawerResult != null) && (drawerResult.isDrawerOpen()))
		{
			drawerResult.closeDrawer();
		}else{
			AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
			adb.setMessage(R.string.EXIT_FROM_APP_MESSAGE);
			adb.setPositiveButton(getResources().getString(R.string.YES), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialogInterface, int i)
				{
					finishAffinity();
				}
			});
			adb.setNegativeButton(getResources().getString(R.string.NO), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialogInterface, int i)
				{
					dialogInterface.dismiss();
				}
			});
			adb.show();
		}
	}
}
