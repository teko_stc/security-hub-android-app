package biz.teko.td.SHUB_APP.SecCompanies.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 13.04.2018.
 */

public class SecCompBottomListAdater extends ArrayAdapter
{
	private final Context context;
	private final String[] strings;
	private final LayoutInflater layoutInflater;
	private final String[] captions;

	public SecCompBottomListAdater(@NonNull Context context, @LayoutRes int resource, String[] strings, String[] captions)
	{
		super(context, resource);
		this.context = context;
		this.strings = strings;
		this.captions = captions;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		String string = strings[position];
		if(null!=captions) string+= "("  + captions[position] + ")";
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(string);
		return view;

	}


	public int getCount() {
		return strings.length;
	}

	public String getItem(int position){
		return strings[position];
	}

	public long getItemId(int position){
		return position;
	}
}
