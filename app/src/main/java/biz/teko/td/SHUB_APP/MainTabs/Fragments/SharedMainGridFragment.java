package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.MainTabs.Adapters.MainGridAdapter;
import biz.teko.td.SHUB_APP.R;
import kotlin.jvm.Synchronized;

public class SharedMainGridFragment extends Fragment implements ClickableGridFrame {
    //private int site_id;
    private static SharedMainGridFragment instance;
    private MainGridFragment.OnClickListener onClickListener;
    private Cursor cursor;
    private DBHelper dbHelper;
    private MainGridAdapter mainGridAdapter;
    private final BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateViews();
        }
    };

    @Synchronized
    public static SharedMainGridFragment newInstance() {
        SharedMainGridFragment mainGridFragment = new SharedMainGridFragment();
        instance = mainGridFragment;
        //Bundle b = new Bundle();
        //b.putInt("site", site_id);
        //mainGridFragment.setArguments(b);
        return mainGridFragment;
    }

    @Synchronized
    public static SharedMainGridFragment getInstance() {
        if (null == instance) {
            instance = new SharedMainGridFragment();
        }
        return instance;
    }

    public Fragment getFragment() {
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final Context context = container.getContext();
        dbHelper = DBHelper.getInstance(context);
        View view = inflater.inflate(R.layout.fragment_main_grid_shared, container, false);

        LinearLayout addNewElementView = (LinearLayout) view.findViewById(R.id.nButtonMainAdd);
        if (null != addNewElementView) addNewElementView.setOnClickListener(view12 -> {
            onClickListener.onNewClickListener();
        });

        GridView mainGrid = view.findViewById(R.id.mainGrid);
        refreshCursor();

        mainGridAdapter = new MainGridAdapter(context, cursor, true, true, new MainGridAdapter.OnGridElementClickListener() {
            @Override
            public void onElementClick(int id) {
                onClickListener.onBarClick(id, view);
            }

            @Override
            public void onEmptyElementClick() {
                onClickListener.onEmptySharedClickListener();
            }

            @Override
            public void onElementLongClick(int id) {
                onClickListener.onBarLongClick(id);
            }

            @Override
            public void onRefresh() {
                updateViews();
            }
        });
        mainGrid.setAdapter(mainGridAdapter);
        MainFragment parent = ((MainFragment) getParentFragment());
        if (parent != null)
            parent.setOnFragmentRefreshListener(this::updateViews);
        return view;
    }

    private void refreshCursor() {
        cursor = dbHelper.getMainBars(0, true);
    }

    private void updateViews() {
        refreshCursor();
        if (null != mainGridAdapter)
            mainGridAdapter.update(cursor);
    }

    @Override
    public void setClickListener(MainGridFragment.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
        intentFilter.addAction(D3Service.BROADCAST_AFFECT);
        intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
        getActivity().registerReceiver(updateReceiver, intentFilter);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(updateReceiver);
    }
}
