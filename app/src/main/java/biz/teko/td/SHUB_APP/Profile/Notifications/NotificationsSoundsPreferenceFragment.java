package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import androidx.annotation.Nullable;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.D3XProtoConstEvent;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 04.08.2017.
 */

public class NotificationsSoundsPreferenceFragment extends PreferenceFragment
{
	private Context context;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.sounds_preferences);
		context  =getActivity();
		PreferenceScreen preferenceScreen = getPreferenceScreen();

		D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
		if(null!=d3XProtoConstEvent){
			LinkedList<EClass> eClasses = d3XProtoConstEvent.classes;
			for(EClass eClass : eClasses){
				RingtonePreference ringtonePreference = new RingtonePreference(context);
				ringtonePreference.setKey("sound_" + eClass.caption);
				ringtonePreference.setTitle(eClass.caption);
				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
				Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(sp.getString("sound_" + eClass.caption, getResources().getString(R.string.SUMMARY_DEFAULT))));
				if(null!=ringtone)
				{
					ringtonePreference.setSummary(ringtone.getTitle(context));
				}
//				String notif_sound_ring = sp.getString("sound_" + eClass.caption,getResources().getString(R.string.SUMMARY_DEFAULT));
//				ringtonePreference.setSummary(notif_sound_ring);
				ringtonePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener()
				{
					@Override
					public boolean onPreferenceChange(Preference preference, Object o)
					{
						Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse((String) o));
						if (ringtone != null)
							preference.setSummary(ringtone.getTitle(context));
						else
							preference.setSummary(R.string.SUMMARY_DEFAULT);
						return true;
					}
				});
				preferenceScreen.addPreference(ringtonePreference);
			}
		}
	}
}
