package biz.teko.td.SHUB_APP.D3.D3UniConfig;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * Created by td13017 on 29.06.2016.
 */
public abstract class InterfaceNode extends DescribedNode{
	@Attribute(name="caption", required = false)
	public String Caption;

	@Attribute(name="count", required = false)
	public int Count;

	@Element(name="hide", required = false)
	public ConditionNode Hide;

	@Element(name="readonly", required = false)
	public ConditionNode Readonly;
}
