package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

/**
 * Created by td13017 on 13.04.2018.
 */

public class Domain
{
	public int id;
	public String name;

	public Domain(){}

	public Domain(int id, String name){
		this.id = id;
		this.name = name;
	}

	public Domain(Cursor c){
		this.id = c.getInt(2);
		this.name = c.getString(3);
	}
}
