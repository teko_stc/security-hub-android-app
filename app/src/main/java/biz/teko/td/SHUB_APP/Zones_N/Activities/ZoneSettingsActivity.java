package biz.teko.td.SHUB_APP.Zones_N.Activities;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.Activity.NTemperatureSetActivity;
import biz.teko.td.SHUB_APP.Codes.Activities.NLocalUsersActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class ZoneSettingsActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private int zone_id;
	private int section_id;
	private int device_id;
	private Zone zone;
	private LinearLayout buttonBack;
	private NMenuListElement nameView;
	private NMenuListElement delayView;
	private NMenuListElement deleteView;
	private BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			bindView();
		}
	};
	private boolean bound;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private int sending = 0;
	private UserInfo userInfo;
	private Device device;
	private NMenuListElement tempView;
	private NMenuListElement usersView;

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};

	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_zone_settings);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();

		zone_id = getIntent().getIntExtra("zone", -1);
		section_id = getIntent().getIntExtra("section", -1);
		device_id = getIntent().getIntExtra("device", -1);

		setupView();
		bindView();

	}

	private void bindView()
	{
		zone = dbHelper.getZoneById(device_id, section_id, zone_id);
		device = dbHelper.getDeviceById(device_id);

		if(null!=buttonBack)buttonBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		if(null!=zone)
		{
			if (null!= nameView)
			{
				nameView.setTitle(zone.name);
				if(maySet()){
					nameView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NRenameActivity.class);
							intent.putExtra("type", Const.ZONE);
							intent.putExtra("device", zone.device_id);
							intent.putExtra("section", zone.section_id);
							intent.putExtra("zone", zone.id);
							intent.putExtra("name", zone.name);
							intent.putExtra("transition",true);
							startActivity(intent, getTransitionOptions(nameView).toBundle());
						}
					});
				}
			}

			if (null != delayView)
			{
				switch (zone.getDetector()){
					case Const.DETECTOR_BIK:
					case Const.DETECTOR_LAMP:
					case Const.DETECTOR_SIREN:
					case Const.DETECTOR_SZO:
					case Const.DETECTOR_KEYPAD:
					case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
						delayView.setVisibility(View.GONE);
						break;
					default:
						Section section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
						if(null!=section && section.detector == Const.SECTIONTYPE_GUARD)
						{
							delayView.setVisibility(View.VISIBLE);
							delayView.setChecked(zone.delay > 0);
							if (maySet())
							{
								delayView.setOnCheckedChangeListener(new NMenuListElement.OnCheckedChangeListener()
								{
									@Override
									public void onCheckedChanged()
									{
										JSONObject commandAddr = new JSONObject();
										try
										{
											commandAddr.put("section", zone.section_id);
											commandAddr.put("index", zone.id);
											commandAddr.put("delay", zone.delay > 0 ? 0 : 1);
											nSendMessageToServer(zone, Command.ZONE_SET, commandAddr, true);
										} catch (JSONException e)
										{
											e.printStackTrace();
										}
									}
								});
							} else
							{
								delayView.setCheckable(false);
							}
						}else{
							delayView.setVisibility(View.GONE);
						}
						break;
				}
			}
			if(null!=tempView){
				if(maySystemSet() && zone.detector == Const.DETECTOR_THERMOSTAT){
					tempView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NTemperatureSetActivity.class);
							intent.putExtra("device", zone.device_id);
							intent.putExtra("section", zone.section_id);
							intent.putExtra("zone", zone.id);
							intent.putExtra("transition",true);
							startActivity(intent, getTransitionOptions(nameView).toBundle());
						}
					});
				}else{
					tempView.setVisibility(View.GONE);
				}
			}
			if(null!=usersView){
				if(maySystemSet()
						&& (zone.getDetector() == Const.DETECTOR_KEYPAD || zone.getDetector() == Const.DETECTOR_KEYPAD_WITH_DISPLAY)){
					usersView.setVisibility(View.VISIBLE);
					usersView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(getBaseContext(), NLocalUsersActivity.class);
							intent.putExtra("device", zone.device_id);
							startActivity(intent);
							overridePendingTransition(R.animator.enter, R.animator.exit);
						}
					});
				}else{
					usersView.setVisibility(View.GONE);
				}
			}
			if(null!=deleteView) {
				if(maySystemSet()){
					deleteView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NDeleteActivity.class);
							intent.putExtra("type", Const.ZONE);
							intent.putExtra("device", zone.device_id);
							intent.putExtra("section", zone.section_id);
							intent.putExtra("zone", zone.id);
							intent.putExtra("name", zone.name);
							intent.putExtra("transition",true);
							startActivityForResult(intent, Const.REQUEST_DELETE, getTransitionOptions(delayView).toBundle());
						}
					});
				}else{
					deleteView.setVisibility(View.GONE);
				}
			}
		}
	}

	private boolean maySet()
	{
		return Func.nGetRemoteDeviceSetRights(userInfo, device);
	}

	private boolean maySystemSet()
	{
		return Func.nGetRemoteSystemSetRights(userInfo);
	}

	private void setupView()
	{
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		nameView = (NMenuListElement) findViewById(R.id.nMenuName);
		delayView = (NMenuListElement) findViewById(R.id.nMenuDelay);
		deleteView = (NMenuListElement) findViewById(R.id.nMenuDelete);
		tempView = (NMenuListElement) findViewById(R.id.nMenuTemp);
		usersView = (NMenuListElement) findViewById(R.id.nMenuLocalUsers);
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((ZoneSettingsActivity)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	public void  onResume(){
		super.onResume();
		bindView();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		//		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		//		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		registerReceiver(updateReceiver, intentFilter);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		bindD3();
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(updateReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
}
