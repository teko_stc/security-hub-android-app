package biz.teko.td.SHUB_APP.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 27.07.2017.
 */

public class HistoryClassesSpinnerAdapter extends ArrayAdapter<EClass>
{
	private final Context context;
	private final EClass[] eClasses;
	private final LayoutInflater layoutInflater;
	private final int curClass;

	public HistoryClassesSpinnerAdapter(Context context, int resource, EClass[] eClasses, int curClass)
	{
		super(context, resource);
		this.context = context;
		this.eClasses = eClasses;
		this.curClass = curClass;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	public int getCount(){
		return eClasses.length;
	}

	public EClass getItem(int position){
		return eClasses[position];
	}

	public long getItemId(int position){
		return position;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		EClass eClass = eClasses[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(eClass.caption);
		if(curClass ==  eClass.id){
			textView.setTextColor(context.getResources().getColor(R.color.brandColorAccentColor));
		}else{
			textView.setTextColor(context.getResources().getColor(R.color.n_text_dark_grey));
		}
		return view;
	}

}
