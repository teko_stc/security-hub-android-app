package biz.teko.td.SHUB_APP.D3DB;

/**
 * Created by td13017 on 08.11.2016.
 */
public class Statement
{
	public int site_id;
	public int site_online;

	public Statement(int site_id, int site_online){
		this.site_id = site_id;
		this.site_online = site_online;
	}
}
