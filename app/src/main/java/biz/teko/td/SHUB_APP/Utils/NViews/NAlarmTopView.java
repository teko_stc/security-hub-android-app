package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;

import biz.teko.td.SHUB_APP.R;

public class NAlarmTopView extends LinearLayout
{
    private OnButtonClickListener onButtonClickListener;
    private NActionButton handleButton;
    private TextView siteTextView;
    private TextView deviceTextView;
    private LinearLayout closeButton;
    private TextView stateTextView;
    private ProgressBar progressView;

    private String state;
    private String device;
    private String site;
    private int state_val;
    private boolean loading;


    public void setOnButtonClickListener(OnButtonClickListener onButtonClickListener){
        if(null!=onButtonClickListener) {
            this.onButtonClickListener = onButtonClickListener;
            if(null!=closeButton) closeButton.setVisibility(VISIBLE);
        }else{
            if(null!=closeButton) closeButton.setVisibility(GONE);
        }
    }

    public interface OnButtonClickListener{
        void onClose();
        void onHandle();
    }

    private final Context context;
    private int layout;

    public NAlarmTopView(Context context) {
        super(context);
        this.context = context;
    }

    public NAlarmTopView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context  = context;
        parseAttributes(attrs);
        setup();
        bind();
    }

    private void setup() {
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(layout, this, true);

        stateTextView = findViewById(R.id.nTextState);
        deviceTextView = findViewById(R.id.nTextDevice);
        siteTextView = findViewById(R.id.nTextSite);


        closeButton = findViewById(R.id.nButtonBack);
        handleButton = findViewById(R.id.nButtonHandle);
        progressView  = findViewById(R.id.pb_search);

    }

    private void bind() {

        if(null!=closeButton){
            closeButton.setOnClickListener(new OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    if(null!=onButtonClickListener) onButtonClickListener.onClose();
                }
            });
        }

        if(null!=handleButton){
            if(state_val== 1){
                handleButton.setTitle(getContext().getString(R.string.N_BUTTON_ALARM_CLOSE));
            }
            if(state_val == 1 || state_val ==0){
                handleButton.setOnButtonClickListener(new NActionButton.OnButtonClickListener()
                {
                    @Override
                    public void onButtonClick(int action)
                    {
                        if(null!=onButtonClickListener)onButtonClickListener.onHandle();
                    }
                });
            }
        }

    }

    private void parseAttributes(AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NAlarmTopView, 0, 0);
        try {
            layout = a.getResourceId(R.styleable.NAlarmTopView_NAlarmTopViewLayout, 0);
        } finally {
            a.recycle();
        }
    }

    private void setStateTextView(){
        if(null!=state){
            stateTextView.setText(state);
        }
    }

    private void setDeviceTextView(){
        if(null!=device){
            deviceTextView.setText(device);
        }
    }

    private void setSiteTextView(){
        if(null!=siteTextView){
            siteTextView.setText(site);
        }
    }

    public void setState(int state){
        state_val = state;
        String[] states = context.getResources().getStringArray(R.array.alarm_states);
        if(null!=states){
            this.state = states[state_val];
            setStateTextView();
        }
        showButtons(loading);
    }

    public void setButtonVisibility(int i)
    {
        if(null!=handleButton) handleButton.setVisibility(i);
    }

    public void setDevice(String device){
        this.device = device;
        setDeviceTextView();
    }

    public void setSite(String site){
        this.site = site;
        setSiteTextView();
    }

    public void setLoading(boolean b){
        this.loading = b;
        showButtons(loading);
    }

    private void showButtons(boolean b){
        if(state_val < 2){
            if(b){
                if(null!=progressView) progressView.setVisibility(VISIBLE);
                if(null!=handleButton) handleButton.setVisibility(GONE);
            }else{
                if(null!=progressView) progressView.setVisibility(GONE);
                if(null!=handleButton) handleButton.setVisibility(VISIBLE);
            }
        }else{
            if(null!=progressView) progressView.setVisibility(GONE);
            if(null!=handleButton) handleButton.setVisibility(GONE);
        }
    }
}
