package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.view.View;
import android.widget.ListView;

import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 21.06.2017.
 */

public class RegisterNotifPrefFragment extends PreferenceFragment
{
	@Override
	public void onCreate(final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.reg_notification_preference);

		View rootView = getView();
		if (rootView != null) {
			ListView list = (ListView) rootView.findViewById(android.R.id.list);
			list.setPadding(0, 0, 0, 0);
			list.setDivider(null);
		}
	}
}
