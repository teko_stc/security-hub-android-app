package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputFilter;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Models.ZoneAlarm;
import biz.teko.td.SHUB_APP.Utils.Other.InputFilterMinMax;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardZoneSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Adapter.BottomSheet.ZoneAlarmTypesAdapter;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardDWITypesSpinnerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardSTypesSpinnerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardSectionsSpinnerAdapter;

/**
 * Created by td13017 on 23.06.2017.
 */

public class ZoneSetFragment extends Fragment
{

	private int position;
	private Context context;
	private View v;
	private DBHelper dbHelper;
	private int setSectionId;
	private int setDeviceId;
	private int setSiteId;
	private int setTypeId;
	private int setZoneConType;
	private int setZoneType;
	private int setRegType;
	private int setZoneSN;
	private int setUIDFormat;
	private String setUID;
	private WizardZoneSetActivity activity;
	private D3Service myService;
	private int sending = 0;
	private ViewGroup container;
	private CountDownTimer timer;
	private Section[] sections;
	private int setZoneIOType;
	private boolean status;
	private int statusPosition;
	private String setZoneName;
	private int inputMin = 1;
	private int inputMax = 2;
	private int setZoneDelay;
	private int setZoneModel;
	private TextView zoneNameSetText;
	private EditText zoneNameText;
	private CheckBox setSectionCheckbox;
	private TextView setNoSectionText;
	private LinearLayout setSectionFullLayout;
	private TextView sectionSetText;
	private LinearLayout sectionsSpinnerLayout;
	private Spinner sectionsSpinner;
	private Button newSectionButton;
	private CheckBox zoneDelayCheckBox;

	public static ZoneSetFragment newInstance(int position){
		ZoneSetFragment zoneSetFragment = new ZoneSetFragment();
		Bundle b = new Bundle();
		b.putInt("position", position);
		zoneSetFragment.setArguments(b);
		return zoneSetFragment;
	}

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
//				/*INIT CRASH FOR DEBUG*/
//				final Activity activity = (WizardZoneSetActivity)context;
//				final WizardZoneSetActivity wizardZoneSetActivity = (WizardZoneSetActivity) activity;
				((WizardZoneSetActivity) getActivity()).refreshViews();
			}
		}
	};



	private BroadcastReceiver sectionAddReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int section_id = intent.getIntExtra("section", -1);
			final WizardZoneSetActivity wizardZoneSetActivity = (WizardZoneSetActivity) getActivity();
			WizardZoneSetActivity.Zone zone = wizardZoneSetActivity.getZone();
			final int siteId = zone.site;
			final int deviceId = zone.device;
			Section[] sections = dbHelper.getSectionsForDeviceWithoutControllerKnownType(deviceId);
			if((sections!=null)&&(sections.length > 0))
			{
				View v = container.getChildAt(0);
				Spinner spinner = (Spinner) v.findViewById(R.id.wizSectionSpinner);
				final CheckBox zoneDelayCheckBox = (CheckBox) v.findViewById(R.id.checkZoneDelay);

				final WizardSectionsSpinnerAdapter sectionsSpinnerAdapter = new WizardSectionsSpinnerAdapter(context, R.layout.spinner_item_dropdown, sections);
				if(null!=spinner)
				{
					spinner.requestLayout();
					spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
					{
						@Override
						public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
						{
							Section section = sectionsSpinnerAdapter.getItem(position);
							setSectionId = section.id;
							if(section.detector == Func.SECTION_GUARD_TYPE){
								zoneDelayCheckBox.setVisibility(View.VISIBLE);
							}else{
								zoneDelayCheckBox.setVisibility(View.GONE);
								zoneDelayCheckBox.setChecked(false);
							}
							wizardZoneSetActivity.setSectionId(setSectionId);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent)
						{
							Section section = sectionsSpinnerAdapter.getItem(0);
							setSectionId = section.id;
							if(section.detector == Func.SECTION_GUARD_TYPE){
								zoneDelayCheckBox.setVisibility(View.VISIBLE);
							}else{
								zoneDelayCheckBox.setVisibility(View.GONE);
								zoneDelayCheckBox.setChecked(false);
							}
							wizardZoneSetActivity.setSectionId(setSectionId);
						}
					});
					spinner.setAdapter(sectionsSpinnerAdapter);
					if(section_id != -1){
						setSectionId = section_id;
						Section currentSection = null;
						for(Section section: sections){
							if(section.id == setSectionId){
								currentSection = section;
							}
						}
						if(null!=currentSection){
							spinner.setSelection(sectionsSpinnerAdapter.getPosition(currentSection));
							wizardZoneSetActivity.setSectionId(setSectionId);
						}
					}
				}
			}
		}
	};

	@Override
	public void onResume()
	{
		super.onResume();
		getActivity().registerReceiver(sectionAddReceiver, new IntentFilter(D3Service.BROADCAST_SECTION_ADD));
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));

	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(sectionAddReceiver);
		getActivity().unregisterReceiver(commandSendReceiver);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
	{
		position = getArguments().getInt("position");
		int layout;
		context = container.getContext();
		this.container = container;
		activity = (WizardZoneSetActivity) getActivity();
		dbHelper = DBHelper.getInstance(context);
		switch (position)
		{
			case 0:
				if(null!=activity){
					setZoneConType = activity.getZone().con_type;
				}
				if(setZoneConType == Const.WIRELESS){
					//wireless
					layout = R.layout.fragment_zone_set_3_wl;
				}else{
					//wired
					layout = R.layout.fragment_zone_set_3_wd;
				}
				v = inflater.inflate(layout, container, false);
				setFragmentOne(v);
				break;
			case 1:
				/*founded sensor || select section*/
				if(null!=activity){
					setZoneConType = activity.getZone().con_type;
				}
				if(setZoneConType == Const.WIRELESS){
					//wiredless
					layout = R.layout.fragment_zone_set_4_wl;
				}else{
					//wired
					layout = R.layout.fragment_zone_set_4_wd;
				}
				v = inflater.inflate(layout, container, false);
				setFragmentTwo(v);
				break;
			case 2:
				if(null!=activity){
					setZoneConType = activity.getZone().con_type;
				}
				if(setZoneConType == Const.WIRELESS){
					//wireless
					layout = R.layout.fragment_zone_set_6_wl;
				}else{
					//wired
					layout = R.layout.fragment_zone_set_6_wd;
				}
				v = inflater.inflate(layout, container, false);
				setFragmentThree(v);
				break;
			default:
				break;
		}

		return v;
	}

	public String checkDigit(int number) {
		return number <= 9 ? "0" + number : String.valueOf(number);
	}

	private void setFragmentThree(View v){
		final WizardZoneSetActivity.Zone zone = activity.getZone();
		setupViews3(v);

		switch (zone.model){
			case 2331: /*szo*/
				zone.section = 0;
				if(setSectionCheckbox != null){
					setSectionCheckbox.setVisibility(View.VISIBLE);
					setSectionCheckbox.setOnCheckedChangeListener(getSetSectionCheckBoxCheckListener(zone));
				}
				break;
			case 8231: /*rk relay*/
			case 8731: /*rk relay socket*/
			case 8131: /*rk keyboard 8131*/
			case 15: /*keyboard 8121*/
				zone.section = 0;
				activity.setSectionId(zone.section);
				if(null!=setSectionCheckbox) setSectionCheckbox.setVisibility(View.GONE);
				if(null!=setSectionFullLayout) setSectionFullLayout.setVisibility(View.GONE);
				if(null!=setNoSectionText) setNoSectionText.setVisibility(View.GONE);
				if(null!=zoneNameSetText)zoneNameSetText.setText((zone.model == 15 || zone.model == 8131)? getString(R.string.WIZ_ZONE_SET_KEYPAD_NAME):getString(R.string.WIZ_ENTER_RELAY_NAME));
				break;
			default:
				if(zone.wired_type == Const.WIREDTYPE_BIK) {
					if(setSectionCheckbox != null){
						setSectionCheckbox.setVisibility(View.VISIBLE);
						setSectionCheckbox.setOnCheckedChangeListener(getSetSectionCheckBoxCheckListener(zone));
					}
				}else
				{
					if(null!=setNoSectionText) setNoSectionText.setVisibility(View.INVISIBLE);
					if(null!=setSectionFullLayout) setSectionFullLayout.setVisibility(View.VISIBLE);
					if(null!=setSectionCheckbox) setSectionCheckbox.setVisibility(View.GONE);

					int sectionsFilter = 23;
					if (0 != zone.detector)
					{
						if (null != D3Service.d3XProtoConstEvent)
						{
							if (zone.con_type == Const.WIRELESS)
							{
								ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(zone.model);
								if (null != zType)
								{
									DType dType = zType.getDetectorType(zone.detector);
									if (null != dType) sectionsFilter = Integer.valueOf(dType.sectionmask);
								}
							} else
							{
								DWIType dwiType = D3Service.d3XProtoConstEvent.getDWIType(zone.wired_type);
								if (null != dwiType)
								{
									DType dType = dwiType.getDetectorType(zone.detector);
									if (dType != null) sectionsFilter = Integer.valueOf(dType.sectionmask);
								}
							}

						}
					}

					Section[] sections = dbHelper.getSectionsForDeviceWithoutControllerKnownType(zone.device);

					if (null != sections && sectionsFilter != 23)
					{
						sections = filterSections(sections, sectionsFilter);
					}

					if (null != sections && null != sectionsSpinner)
					{
						if (0 != sections.length)
						{
							final WizardSectionsSpinnerAdapter sectionsSpinnerAdapter = new WizardSectionsSpinnerAdapter(context, R.layout.spinner_item_dropdown, sections);
							sectionsSpinner.setAdapter(sectionsSpinnerAdapter);
							sectionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
							{
								@Override
								public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
								{
									Section section = sectionsSpinnerAdapter.getItem(position);
									zone.section = section.id;
									if (section.detector == Func.SECTION_GUARD_TYPE)
									{
										zoneDelayCheckBox.setVisibility(View.VISIBLE);
									} else
									{
										zoneDelayCheckBox.setVisibility(View.GONE);
										zoneDelayCheckBox.setChecked(false);
									}
									activity.setSectionId(zone.section);
								}

								@Override
								public void onNothingSelected(AdapterView<?> parent)
								{
									Section section = sectionsSpinnerAdapter.getItem(0);
									zone.section = section.id;
									if (section.detector == Func.SECTION_GUARD_TYPE)
									{
										zoneDelayCheckBox.setVisibility(View.VISIBLE);
									} else
									{
										zoneDelayCheckBox.setVisibility(View.GONE);
										zoneDelayCheckBox.setChecked(false);
									}
									activity.setSectionId(zone.section);
								}
							});
						} else
						{
							zone.section = -1;
						}
					}
					if (null != newSectionButton)
					{
						newSectionButton.setOnClickListener(new View.OnClickListener()
						{
							@Override
							public void onClick(View v)
							{
								showSectionAdb(zone.site, zone.device, 0, activity);
							}
						});
					}
				}

				break;

		}

		if(null!=zoneNameText){
			zoneNameText.requestFocus();
			zoneNameText.setOnEditorActionListener(new TextView.OnEditorActionListener()
			{
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
					{
						Func.hideKeyboard(activity);
						return true;
					}
					return false;
				}
			});
		}


		final Button next = (Button) v.findViewById(R.id.wizZoneButtonNext);
		if(null!=next)
		{
			next.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					String zoneName = zoneNameText.getText().toString();
					if(!zoneName.equals("")){
						if(zone.section >= 0 || zone.wired_type == 4)
						{
							UserInfo userInfo = dbHelper.getUserInfo();
							myService = getService();
							JSONObject commandAddr = new JSONObject();
							try
							{
								commandAddr.put("uid", zone.con_type == 0 ? zone.uid:
										String.format("%02X", zone.uid_format) + String.format("%02X", zone.input) + String.format("%02X", zone.wired_type) );
								if(zone.section >=0 ) commandAddr.put("section", zone.section);
								if(zone.detector != 0){
									if(zone.alarm != 0){
										String ss = String.format("%02X", zone.alarm) + String.format("%02X", zone.detector);
										commandAddr.put("detector", Integer.parseInt(ss, 16) );
									}else{
										commandAddr.put("detector", zone.detector );
									}
								}
								commandAddr.put("name", zoneName);
								commandAddr.put("delay", zoneDelayCheckBox!=null && zoneDelayCheckBox.isChecked()? 1 : 0);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							int command_count = Func.getCommandCount(context);
							D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device, "ZONE_SET", commandAddr, command_count);
							if (d3Request.error == null)
							{
								if (sendCommandToServer(d3Request))
								{
									sendIntentCommandBeenSend(1);
								}
							} else
							{
								Func.pushToast(context, d3Request.error, (Activity) context);
							}
						}else{
							Func.nShowMessage(context, getString(R.string.WIZ_CHOOSE_SECTION));
						}
					}else{
						Func.nShowMessage(context, getString(R.string.WIZ_ZONE_ENTER_NAME));
					}
				}
			});
		}
	}

	private CompoundButton.OnCheckedChangeListener getSetSectionCheckBoxCheckListener(WizardZoneSetActivity.Zone zone)
	{
		return (compoundButton, b) -> {
			if(!b){
				zone.section = 0;
				activity.setSectionId(zone.section);
				if(setSectionFullLayout !=null) setSectionFullLayout.setVisibility(View.INVISIBLE);
				if(setNoSectionText!=null) setNoSectionText.setVisibility(View.VISIBLE);
			}else{
				if(setNoSectionText!=null) setNoSectionText.setVisibility(View.INVISIBLE);
				if(setSectionFullLayout !=null)	setSectionFullLayout.setVisibility(View.VISIBLE);
				if(sectionSetText!=null) sectionSetText.setVisibility(View.GONE);

				Section[] sections = zone.wired_type == Const.WIREDTYPE_BIK ?
						dbHelper.getGuardSectionsForDeviceWithoutZones(zone.device)
						: dbHelper.getSectionsForDeviceWithoutControllerKnownType(zone.device);
				if(null!= sections && null!=sectionsSpinner)
				{
					final WizardSectionsSpinnerAdapter sectionsSpinnerAdapter = new WizardSectionsSpinnerAdapter(context, R.layout.spinner_item_dropdown, sections);
					sectionsSpinner.setAdapter(sectionsSpinnerAdapter);
					sectionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
					{
						@Override
						public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
						{
							Section section = sectionsSpinnerAdapter.getItem(position);
							zone.section = section.id;
							activity.setSectionId(zone.section);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent)
						{
							Section section = sectionsSpinnerAdapter.getItem(0);
							zone.section = section.id;
							activity.setSectionId(zone.section);
						}
					});
				}

				if(null!=newSectionButton) newSectionButton.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						showSectionAdb(zone.site,  zone.device, 0, activity);
					}
				});
				if(null!=zoneDelayCheckBox)	zoneDelayCheckBox.setVisibility(View.GONE);
			}
		};
	}

	private void setupViews3(View v)
	{
		zoneNameSetText = (TextView) this.v.findViewById(R.id.wizZoneNameSetText);
		zoneNameText = (EditText) this.v.findViewById(R.id.wizZoneName);

		setSectionCheckbox = (CheckBox) this.v.findViewById(R.id.wizExitBindToSecCheck);
		setNoSectionText = (TextView) this.v.findViewById(R.id.wizZoneBindToSecText);
		setSectionFullLayout = (LinearLayout) this.v.findViewById(R.id.wizZoneSetSectionLayout);

		sectionSetText = (TextView) this.v.findViewById(R.id.textSectionSet);
		sectionsSpinnerLayout = (LinearLayout) this.v.findViewById(R.id.wizSectionSpinnerBack);
		sectionsSpinner = (Spinner) this.v.findViewById(R.id.wizSectionSpinner);
		newSectionButton = (Button) this.v.findViewById(R.id.wizZoneAddSectionButton);
		zoneDelayCheckBox = (CheckBox) this.v.findViewById(R.id.checkZoneDelay);
	}

	private void setFragmentTwo(View v)
	{
		final WizardZoneSetActivity activity = (WizardZoneSetActivity) getActivity();
		final WizardZoneSetActivity.Zone zone = activity.getZone();

		final LinearLayout alarmFrame  = (LinearLayout) v.findViewById(R.id.usageSetFrame);
		final LinearLayout clickableFrame  = (LinearLayout) v.findViewById(R.id.wizZoneAlarmSetLinear);
		final TextView alarmButton = (TextView) v.findViewById(R.id.wizAlarmTypeSetButton);

		if(zone.con_type == Const.WIRELESS){
			/*wireless*/
			TextView zoneType = (TextView) v.findViewById(R.id.wizZoneType);
			ImageView zoneImage = (ImageView) v.findViewById(R.id.wizZoneFoundImage);

			ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(zone.model);
			if (zType!=null)
			{
				if(null!=zoneType)zoneType.setText(zType.caption);
				if(null!=zoneImage)zoneImage.setImageResource(context.getResources().getIdentifier(zType.img, null, context.getPackageName()));

				DType dType = zone.detector == 0 ? zType.getDefDetectorType() : zType.getDetectorType(zone.detector);
				if (null != dType)
				{
					zone.detector = dType.id;
					AType alarmType = zone.alarm == 0 ? dType.getDefaultAType() : dType.getAlarmType(zone.alarm);
					if (null != alarmType)
					{
						zone.alarm = alarmType.id;
						if(null!=alarmButton)alarmButton.setText(alarmType.caption);
						if(null!=alarmFrame) alarmFrame.setVisibility(View.VISIBLE);
					}else{
						if(null!=alarmFrame) alarmFrame.setVisibility(View.GONE);
					}
				}else{
					if(null!=alarmFrame) alarmFrame.setVisibility(View.GONE);
				}
			}
			switch (zone.model){
				case 2331:
				case 8131:
				case 8231:
				case 8731:
				case 15:
					if(null!=alarmFrame) alarmFrame.setVisibility(View.GONE);
					break;
				default:
					if(null!=alarmFrame) alarmFrame.setVisibility(View.VISIBLE);
					break;
			}
		}else{
			/*wired*/
			final Spinner typesSpinner = (Spinner) v.findViewById(R.id.wizZoneTypeSpinner);
			if(null!= typesSpinner && null!= D3Service.d3XProtoConstEvent){
				LinkedList<DWIType> dwiTypesList = D3Service.d3XProtoConstEvent.dwitypes;
				DWIType[] dwiTypes = new DWIType[dwiTypesList.size()];
				for (int i = 0; i < dwiTypesList.size(); i++)
				{
					dwiTypes[i] = dwiTypesList.get(i);
				}
				final WizardDWITypesSpinnerAdapter wizardDWITypesSpinnerAdapter = new WizardDWITypesSpinnerAdapter(context, R.layout.spinner_item_dropdown, dwiTypes);
				typesSpinner.setAdapter(wizardDWITypesSpinnerAdapter);
				typesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
				{
					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
					{
						DWIType dwiType = wizardDWITypesSpinnerAdapter.getItem(position);
						if (null != dwiType )
						{
							DType dType = (dwiType.id==zone.wired_type ?
									(zone.detector == 0 ?
											dwiType.getDefDetectorType()
											: dwiType.getDetectorType(zone.detector))
									: dwiType.getDefDetectorType());
							if (null != dType)
							{
								zone.detector = dType.id;
								AType alarmType = dwiType.id==zone.wired_type ?
										(zone.alarm == 0 ?
												dType.getDefaultAType()
												: dType.getAlarmType(zone.alarm))
										: dType.getDefaultAType();
								if (null != alarmType)
								{
									zone.alarm = alarmType.id;
									if(null!=alarmButton) alarmButton.setText(alarmType.caption);
									if(null!=alarmFrame) alarmFrame.setVisibility(View.VISIBLE);
								}else{
									if(null!=alarmFrame) alarmFrame.setVisibility(View.GONE);
								}
							}else{
								if(null!=alarmFrame) alarmFrame.setVisibility(View.GONE);
							}
							activity.setWiredType(dwiType.id);
						}
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent)
					{

					}
				});
			}
		}

		if(null!=clickableFrame)
		{
			clickableFrame.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					BottomSheetFragmentForAlarms bottomSheetFragmentForAlarms = BottomSheetFragmentForAlarms.newInstance(zone.con_type == 0 ? zone.model : zone.wired_type, zone.con_type);
					bottomSheetFragmentForAlarms.show(activity.getSupportFragmentManager(), "tagAlarm");
				}
				});
		}

		Button next = (Button) v.findViewById(R.id.wizZoneButtonNext);
		if(null!=next)
		{
			next.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					activity.setNextPage();
				}
			});
		}
	}

	private void setFragmentOne(View v){

		final WizardZoneSetActivity activity = (WizardZoneSetActivity) getActivity();
		WizardZoneSetActivity.Zone zone = activity.getZone();
		setSiteId = zone.site;
		setDeviceId = zone.device;
		setZoneConType = zone.con_type;

		if(setZoneConType == Const.WIRELESS){
			//wireless
			Button buttonInteractive = (Button) v.findViewById(R.id.wizZoneStartInteractive);
			Button buttonManual = (Button) v.findViewById(R.id.wizZoneStartManual);
			ImageView helperImage = (ImageView) v.findViewById(R.id.wizZoneSetImage3);
			TextView helperText = (TextView) v.findViewById(R.id.wizZoneSetText3);
			TextView helperBigText = (TextView) v.findViewById(R.id.wizZoneSetBigText3);
			ProgressBar interactiveProgress = (ProgressBar) v.findViewById(R.id.wizZoneSetInteractiveProgress);

			boolean interactive = dbHelper.getDeviceInteractiveStatus(setDeviceId);
			if(interactive){
				if(null!=buttonInteractive)
				{
					buttonInteractive.setText(R.string.WIZ_INTERACTIVE_STATEMENT_ON);
					buttonInteractive.setVisibility(View.GONE);
				}
				if(null!=helperImage) helperImage.setImageDrawable(getResources().getDrawable(R.drawable.device_3));
				if(null!=helperText) helperText.setText(R.string.WIZ_DEVICE_PUT_BATTERY);
				if(null!=helperText) helperBigText.setVisibility(View.VISIBLE);
				if(null!=interactiveProgress) interactiveProgress.setVisibility(View.VISIBLE);

			}else{
				if(null!=buttonInteractive){
					buttonInteractive.setText(R.string.WIZ_INTERACTIVE_TURN_ON_ZONE);
					buttonInteractive.setVisibility(View.VISIBLE);
				}
				if(null!=helperImage) helperImage.setImageDrawable(getResources().getDrawable(R.drawable.device_2));
				if(null!=helperText) helperText.setText(R.string.WIZ_DEVICE_ADD_MESS_IN_INTERACTIVE);
				if(null!=helperText) helperBigText.setVisibility(View.GONE);
				if(null!=interactiveProgress) interactiveProgress.setVisibility(View.GONE);


			}

			if(null!=buttonInteractive){
				buttonInteractive.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						UserInfo userInfo = dbHelper.getUserInfo();
						myService = getService();
						JSONObject commandAddr = new JSONObject();
						int command_count = Func.getCommandCount(context);
						D3Request d3Request = D3Request.createCommand(userInfo.roles, setDeviceId, "INTERACTIVE", commandAddr, command_count);
						if (d3Request.error == null)
						{
							if (sendCommandToServer(d3Request))
							{
								sendIntentCommandBeenSend(1);
							}
						} else
						{
							Func.pushToast(context, d3Request.error, (Activity) context);
						}
					}
				});
			}

			if(null!=buttonManual){
				buttonManual.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (null != activity)
						{
							Func.nShowMessage(context, getString(R.string.WIZ_ZONE_CANT_ADD_BY_SN));
						}
					}
				});
			}
		}else{
			//wired
			Device device = dbHelper.getDeviceById(setDeviceId);
			if(null!=device)
			{
				switch (device.type){
					case Const.DEVICETYPE_SH_2:
					case Const.DEVICETYPE_SH_4G:
						inputMax = 4;
						break;
					default:
						inputMax = 2;
						break;
				}
			}

			final EditText editExitNumber = (EditText) v.findViewById(R.id.wizExitNumber);
			final Button next = (Button) v.findViewById(R.id.wizZoneButtonNext);

			if(null!=editExitNumber){
				editExitNumber.requestFocus();
				editExitNumber.setFilters(new InputFilter[]{new InputFilterMinMax(inputMin, inputMax)});
				editExitNumber.setOnEditorActionListener(new TextView.OnEditorActionListener()
				{
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
					{
						if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
						{
							editExitNumber.clearFocus();
							Func.hideKeyboard(activity);
							if(null!=next) next.callOnClick();
							return true;
						}
						return false;
					}
				});
			}

			if(null!=next)
			{
				next.setText(R.string.WIZ_GONEXT);
				next.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (dbHelper.isDeviceOnline(setDeviceId))
						{
							if (0 == dbHelper.getSiteDeviceArmStatus(setSiteId, setDeviceId))
							{
								String exitNumber = editExitNumber.getText().toString();
								if (!exitNumber.equals(""))
								{
									activity.setWiredInput(Integer.valueOf(exitNumber));
//									wizardZoneSetActivity.setUID(String.format("%02X", 4) + String.format("%02X", Integer.valueOf(exitNumber)) + String.format("%02X", setZoneType));
									activity.setUIDFormat(4);
									activity.setNextPage();

								} else
								{
									Func.nShowMessage(context, context.getString(R.string.WIZ_ZONE_ENTER_EXIT_NUMBER));
								}
							} else
							{
								Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_ZONE_ADD));
							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.DTA_NO_CONNECTION_DEVICE));
						}
					}
				});
			}
		}
	}


	public static class BottomSheetFragmentForAlarms extends BottomSheetDialogFragment
	{
		private Context context;
		private WizardZoneSetActivity activity;
		private int zoneType;
		private int zoneConnectType;

		public static BottomSheetFragmentForAlarms newInstance(int zoneType, int zoneConnection){
			BottomSheetFragmentForAlarms bottomSheetFragmentForAlarms = new BottomSheetFragmentForAlarms();
			Bundle bundle = new Bundle();
			bundle.putInt("zone_type", zoneType);
			bundle.putInt("zone_connect_type", zoneConnection);
			bottomSheetFragmentForAlarms.setArguments(bundle);
			return  bottomSheetFragmentForAlarms;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState)
		{
			context = getContext();
			activity = (WizardZoneSetActivity) getActivity();
			zoneType = getArguments().getInt("zone_type");
			zoneConnectType = getArguments().getInt("zone_connect_type");

			final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(context);

			bottomSheetDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

			View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
			TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
			title.setText(R.string.WIZ_ZONESET_SELECT_ALARM);
			ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
			listView.setDivider(null);

			if(null != D3Service.d3XProtoConstEvent){
				LinkedList<ZoneAlarm> zoneAlarms = new LinkedList<>();
				switch (zoneConnectType){
					case 0:
						ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(zoneType);
						if(null!=zType){
							if(null!=zType.detectortypes && zType.detectortypes.size() > 0){
								for(DType detectorType : zType.detectortypes){
									if(null!=detectorType.alarmtypes && detectorType.alarmtypes.size() > 0){
										for(AType aType: detectorType.alarmtypes){
											ZoneAlarm zoneAlarm = new ZoneAlarm(aType.id, detectorType.id, aType.caption, aType.icon, detectorType.sectionmask, aType.icons);
											zoneAlarms.add(zoneAlarm);
										}
									}
								}
							}
						}
						break;
					case 1:
						DWIType dwiType = D3Service.d3XProtoConstEvent.getDWIType(zoneType);
						if(null!=dwiType){
							if(null!=dwiType.detectortypes && dwiType.detectortypes.size() > 0){
								for(DType detectorType : dwiType.detectortypes){
									if(null!=detectorType.alarmtypes && detectorType.alarmtypes.size() > 0){
										for(AType aType: detectorType.alarmtypes){
											ZoneAlarm zoneAlarm = new ZoneAlarm(aType.id, detectorType.id, aType.caption, aType.icon, detectorType.sectionmask, aType.icons);
											zoneAlarms.add(zoneAlarm);
										}
									}
								}
							}
						}
						break;
				}

				if(null!=zoneAlarms && zoneAlarms.size() > 0){
					ZoneAlarm[] zoneAlarmsArray = new ZoneAlarm[zoneAlarms.size()];
					for(int i=0 ; i < zoneAlarms.size(); i++){
						zoneAlarmsArray[i] = zoneAlarms.get(i);
					}
					if(null!=zoneAlarmsArray) {
						ZoneAlarmTypesAdapter zoneAlarmTypesAdapter = new ZoneAlarmTypesAdapter(context, R.layout.n_devices_list_element, zoneAlarmsArray);
						zoneAlarmTypesAdapter.setOnItemClickListener(new ZoneAlarmTypesAdapter.OnItemClickListener()
						{
							@Override
							public void onClick(ZoneAlarm zoneAlarm)
							{
								activity.setZoneDetector(zoneAlarm.detector);
								activity.setZoneAlarm(zoneAlarm.id);
								activity.refreshViews();

								bottomSheetDialog.dismiss();
							}
						});
						listView.setAdapter(zoneAlarmTypesAdapter);
					}
				}
			}



			bottomSheetDialog.setContentView(bottomView);

			bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
				@Override public void onShow(DialogInterface dialog) {
					FrameLayout bottomSheet =
							bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

					if (null != bottomSheet) {
						BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
						behavior.setHideable(false);
					}
				}
			});

			return bottomSheetDialog;
		}
	}


	private AlertDialog showSectionAdb(final int siteId, final int deviceId, int source, final WizardZoneSetActivity activity)
	{
		final UserInfo userInfo = dbHelper.getUserInfo();
		AlertDialog.Builder adb = 	Func.adbForCurrentSDK(context);
		switch (source){
			case 0:
				adb.setTitle(R.string.SETA_ADD_SECTION_TITLE);
				break;
			case 1:
				adb.setMessage(R.string.DSF_NO_SECTIONS_MESSAGE);
				break;
		}
		adb.setPositiveButton(R.string.ADB_ADD, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

			}
		});
		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Func.hideKeyboard(activity);
				dialog.dismiss();
			}
		});
		final AlertDialog ad = adb.create();

		View view = activity.getLayoutInflater().inflate(R.layout.dialog_section_add, null);
		final EditText editName = (EditText) view.findViewById(R.id.editName);
		editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
		int editNameMaxLength = 32;
		editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
		editName.setOnKeyListener(new View.OnKeyListener()
		{

			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
				{
					// Perform action on Enter key press
					editName.clearFocus();
					return true;
				}
				return false;
			}
		});

		if(null!=D3Service.d3XProtoConstEvent)
		{
			LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
			LinkedList<SType> sTypes = new LinkedList<SType>();
			Device device = dbHelper.getDeviceById(deviceId);
			if(device.canBeSetFromApp())
			{
				for (SType sType: sTypesList)
				{
					if(null!=sType)
					{
						if (sType.id != 4 && sType.id !=6)
						{
							sTypes.add(sType);
						}
					}
				}
			}else{
				sTypes = sTypesList;
			}

			Spinner sTypeSpinner = (Spinner) view.findViewById(R.id.wizSectionTypeSpinner);
			if (null != sTypeSpinner)
			{
				final WizardSTypesSpinnerAdapter wizardSTypesSpinnerAdapter = new WizardSTypesSpinnerAdapter(context, R.layout.spinner_item_dropdown, sTypes);
				sTypeSpinner.setAdapter(wizardSTypesSpinnerAdapter);
				sTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
				{
					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
					{
						SType sType = wizardSTypesSpinnerAdapter.getItem(position);
						setTypeId = sType.id;
						activity.setSectionType(setTypeId);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent)
					{
						SType sType = wizardSTypesSpinnerAdapter.getItem(0);
						setTypeId = sType.id;
						activity.setSectionType(setTypeId);
					}
				});
			}
		}
		ad.setView(view);
		ad.show();
		ad.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

		ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String name = editName.getText().toString();
				if (!name.equals(""))
				{
					if(dbHelper.isDeviceOnline(deviceId))
					{
						if (0 == dbHelper.getSiteDeviceArmStatus(siteId, deviceId))
						{
							myService = getService();
							if (null != myService)
							{
								JSONObject commandAddr = new JSONObject();
								try
								{
									commandAddr.put("type", setTypeId);
									commandAddr.put("name", name);
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
								int command_count = Func.getCommandCount(context);
								D3Request d3Request = D3Request.createCommand(userInfo.roles, deviceId, "SECTION_SET", commandAddr, command_count);
								if (d3Request.error == null)
								{
									if (sendCommandToServer(d3Request))
									{
										sendIntentCommandBeenSend(1);
										ad.dismiss();
									}
								} else
								{
									Func.pushToast(context, d3Request.error, (Activity) context);
								}
							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.OBJECT_DISARM_MESSAGE_ZONE_DEL));
						}
					}
				} else
				{
					Func.pushToast(context, getString(R.string.SETA_ENTER_SEC_NUM), activity);
				}
			}

		});
		return  ad;
	}


	private boolean sendCommandToServer(D3Request d3Request)
	{
		if (null != myService)
		{
			if (myService.send(d3Request.toString()))
			{
				return true;
			} else
			{
				sendIntentCommandBeenSend(-1);
			}
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
		}
		return false;
	};

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if (null != myService)
		{
			if (myService.send(d3Request.toString()))
			{
				return true;
			}
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
		}
		return false;
	};


	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	public D3Service getService()
	{
		WizardZoneSetActivity activity = (WizardZoneSetActivity) getActivity();
		if(activity != null){
			return activity.getLocalService();
		}
		return null;
	}

	private Section[] filterSections(Section[] sections, int sectionsFilter)
	{
		LinkedList<Section> filteredSections = new LinkedList<>();
		for(Section section:sections){
			if(((sectionsFilter >> (section.detector - 1))&0x1) != 0){
				filteredSections.add(section);
			}
		}
		return filteredSections.toArray(new Section[filteredSections.size()]);
	}

}
