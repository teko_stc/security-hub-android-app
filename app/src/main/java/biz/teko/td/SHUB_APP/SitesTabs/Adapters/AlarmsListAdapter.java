package biz.teko.td.SHUB_APP.SitesTabs.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraRecordViewActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 22.08.2017.
 * DEPRECATED 18.11.2020
 */

public class AlarmsListAdapter extends ResourceCursorAdapter
{
	private final Context context;
	private final Cursor cursor;
	private final DBHelper dbHelper;
	private final boolean active;

	public AlarmsListAdapter(Context context, int layout, Cursor c, int flags, boolean active)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.cursor = c;
		this.dbHelper = DBHelper.getInstance(context);
		this.active = active;
	}

	public void update(Cursor cursor){
		this.changeCursor(cursor);
	}

	@Override
	public void bindView(View view, final Context context, Cursor cursor)
	{
		final Event event = dbHelper.getEvent(cursor);
		if(null!=event){
			ImageView alarmImage = (ImageView) view.findViewById(R.id.imageAlarm);
			if(null!=alarmImage) alarmImage.setImageResource(context.getResources().getIdentifier(event.explainAffectIconBig(), null, context.getPackageName()));

			setTextViewText(view, context, R.id.nTextSiteName, dbHelper.getSiteNameByDeviceSection(event.device, event.section));

			final Device device = dbHelper.getDeviceById(event.device);
			if(null!=device){
				setTextViewText(view, context, R.id.textSecDeviceName, device.getName() + "(" + Integer.toString(device.account) + ")");
			}else{
				setTextViewText(view, context, R.id.textSecDeviceName, context.getString(R.string.UNKNOWN_DEVICE));
			}

			String sourceDescription = "";
			if(event.section == 0 ){
				if(event.zone == 0){
					sourceDescription = context.getString(R.string.CONTROLLER);
				}else{
					sourceDescription = dbHelper.getZoneNameById(event.device, event.section, event.zone) + "(" + event.zone + ")";
				}
			}else{
				if(event.zone == 0){
					sourceDescription = dbHelper.getSectionNameByIdWithOptions(event.device, event.section) + "(" + event.section + ")";
				}else{
					sourceDescription = dbHelper.getSectionNameByIdWithOptions(event.device, event.section) + "(" + event.section + ")" + " : " + dbHelper.getZoneNameById(event.device, event.section, event.zone) + "(" + event.zone + ")";
				}
			}
			setTextViewText(view, context, R.id.textSecName, sourceDescription);
			setTextViewText(view, context, R.id.textAlarmDescription, event.explainAffectRegDescription(context));
			setTextViewText(view, context, R.id.textDate, Func.getServerDateText(context, event.time));
			setTextViewText(view, context, R.id.textTime, Func.getServerTimeText(context, event.time));
		}
		if(!active){
			ImageView imageRepair = (ImageView) view.findViewById(R.id.imageRepair);
			TextView textRepair = (TextView) view.findViewById(R.id.textRepairTime);

			Event repairEvent = dbHelper.getRepairEventForAlarm(event);
			if(null!=repairEvent)
			{

				imageRepair.setImageResource(context.getResources().getIdentifier(repairEvent.explainIcon(), null, context.getPackageName()));
				setTextViewText(view, context, R.id.textRepairDate, Func.getServerDateText(context, repairEvent.time));
				setTextViewText(view, context, R.id.textRepairTime, Func.getServerTimeText(context, repairEvent.time));
			}else{
				imageRepair.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_question_grey_500_24dip));
				setTextViewText(view, context, R.id.textRepairTime, context.getString(R.string.ALARM_REPAIR_TIME_DONT_KNOW));
				setTextViewText(view, context, R.id.textRepairDate, "");
			}

		}

		ImageView optionImage = (ImageView) view.findViewById(R.id.alarmListImage);
		if(null!=optionImage)
		{
			optionImage.setImageResource(android.R.color.transparent);
			optionImage.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{

				}
			});
			if(((event.classId == 2)||(event.classId == 1)||(event.classId == 0))&&((event.jdata !=null)&&(!event.jdata.equals("")))){
				if(event.jdata.contains("url")){
					optionImage.setImageResource(R.drawable.ic_camera_brand_color);
					optionImage.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							if ((event.jdata != null) && (!event.jdata.equals("")))
							{
								Intent intent = new Intent(context, NCameraRecordViewActivity.class);
								intent.putExtra("event", event.id);
								((Activity)context).startActivity(intent);
								((Activity)context).overridePendingTransition(R.animator.enter, R.animator.exit);
							}
						}
					});
				}else if(event.jdata.contains("notification")){
					optionImage.setImageResource(R.drawable.ic_videocam_off);
					optionImage.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							Func.nShowMessage(context, context.getString(R.string.IV_CLIP_FAILED));
						}
					});
				}

			}
		}
	}

	private void setTextViewText(View view, Context context, int id, String string){
		TextView textView = (TextView) view.findViewById(id);
		if(textView!=null){
			textView.setText(string==null? "" : string);
		}
	}


}
