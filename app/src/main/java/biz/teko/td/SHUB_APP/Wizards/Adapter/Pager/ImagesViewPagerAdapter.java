package biz.teko.td.SHUB_APP.Wizards.Adapter.Pager;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Fragments.ImagesFragment;

/**
 * Created by td13017 on 26.06.2017.
 */

public class ImagesViewPagerAdapter extends FragmentPagerAdapter
{
	private int IMAGES_PAGES_COUNT = 4;

	public ImagesViewPagerAdapter(FragmentManager fm)
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int position)
	{
		return ImagesFragment.newInstance(position);
	}

	@Override
	public int getCount()
	{
		return IMAGES_PAGES_COUNT;
	}
}
