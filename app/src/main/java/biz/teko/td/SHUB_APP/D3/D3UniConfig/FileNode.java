package biz.teko.td.SHUB_APP.D3.D3UniConfig;

import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

/**
 * Created by td13017 on 29.06.2016.
 */
public class FileNode extends NamedNode{
	@ElementList(entry="field", inline = true)
	public LinkedList<FieldNode> Fields;
}
