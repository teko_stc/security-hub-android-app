package biz.teko.td.SHUB_APP.Utils.Other.RadioGroup;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.util.ArrayList;

import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.R;

public class NPresetValueButton extends LinearLayout implements RadioCheckable {
	public static final int[] STATE_PR = {R.attr.state_pr};

	private int layout = R.layout.n_button_custom_preset;
	// Views
	private TextView titleTextView, subTitleTextView;
	private ImageView imageSwitcher;

	public ImageView getImageSwitcher() {
		return imageSwitcher;
	}

	public void setImageSwitcher(ImageView imageSwitcher) {
		this.imageSwitcher = imageSwitcher;
	}

	// Constants
	public static final int DEFAULT_TEXT_COLOR = Color.TRANSPARENT;

	// Attribute Variables
	private String title;
	private String subTitle;
	private int image;
	private Drawable imagePressed;
	private int titleTextColor;
	private int subTitleTextColor;
	private int pressedTitleTextColor;
	private int titleTextColorInt;


	// Variables
	private Drawable backGround;

	private OnClickListener onPressedListener;
	private OnClickListener onClickListener;
	private OnTouchListener onTouchListener;
	private boolean checked;
	private ArrayList<OnCheckedChangeListener> onCheckedChangeListeners = new ArrayList<>();
	private boolean animation;
	private Animation in, out, fade_in, fade_out;


	private float rippleX;
	private float rippleY;
	private Handler handler;
	int touchAction;
	float radius;
	float endRadius;
	float width, height, speed, duration;
	private Paint paint = new Paint();
	private int position;
	private int value;
	private int index;

	private MainBar.Type barType;
	private MainBar.SubType subType;
	private String elementId;
	private boolean uncheckable;
	private boolean custom_pressed;


	//================================================================================
	// Constructors
	//================================================================================


	public TextView getTitleTextView() {
		return titleTextView;
	}

	public void setTitleTextView(TextView titleTextView) {
		this.titleTextView = titleTextView;
	}

	public TextView getSubTitleTextView() {
		return subTitleTextView;
	}

	public void setSubTitleTextView(TextView subTitleTextView) {
		this.subTitleTextView = subTitleTextView;
	}

	public NPresetValueButton(Context context) {
		super(context);
		setupView();
	}

	public NPresetValueButton(Context context, int layout) {
		super(context);
		this.layout = layout;
		setupView();
	}

	public NPresetValueButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		parseAttributes(attrs);
		setupView();
	}

	@RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
	public NPresetValueButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		parseAttributes(attrs);
		setupView();
	}

	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public NPresetValueButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		parseAttributes(attrs);
		setupView();
	}

	//================================================================================
	// Init & inflate methods
	//================================================================================

	private void parseAttributes(AttributeSet attrs) {
		TypedArray a = getContext().obtainStyledAttributes(attrs,
				R.styleable.PresetValueButton, 0, 0);
		Resources resources = getContext().getResources();
		try {
			layout = a.getResourceId(R.styleable.PresetValueButton_presetButtonLayout, R.layout.n_checkable_layout);
			title = a.getString(R.styleable.PresetValueButton_presetButtonTitleText);
			subTitle = a.getString(R.styleable.PresetValueButton_presetButtonSubtitleText);
			position = a.getInt(R.styleable.PresetValueButton_presetButtonPosition, 0);
			image = a.getResourceId(R.styleable.PresetValueButton_presetButtonImage, R.drawable.n_image_section_arm_list_selector);
			titleTextColor = a.getResourceId(R.styleable.PresetValueButton_presetButtonImage, R.color.n_site_select_text_color_selector);
			subTitleTextColor = a.getResourceId(R.styleable.PresetValueButton_presetButtonSubtitleTextColor, R.color.n_site_select_text_color_selector);
		} finally {
			a.recycle();
		}
	}

	// Template method
	private void setupView() {
		inflateView();
		bindView();
		setCustomTouchListener();
	}

	protected void inflateView() {
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(layout, this, true);

		imageSwitcher = (ImageView) findViewById(R.id.image_value);

		titleTextView = (TextView) findViewById(R.id.text_view_title);
		subTitleTextView = (TextView) findViewById(R.id.text_view_subtitle);
		if(null==backGround)backGround = getBackground();

		handler = new Handler();
		setClickable(true);
	}

	public void bindView() {
		if(null!=titleTextView){
			setTitleText();
			if (titleTextColor != DEFAULT_TEXT_COLOR) {
				titleTextView.setTextColor(getResources().getColorStateList(titleTextColor));
			}
		}
		if(null!=subTitleTextView)
			if (subTitleTextColor != DEFAULT_TEXT_COLOR) {
				subTitleTextView.setTextColor(getResources().getColorStateList(subTitleTextColor));
			}
			if(null!=subTitle){
				setSubTitleText();

			}else{
				subTitleTextView.setVisibility(GONE);
			}

		setImageViewImage();

//		if(animation){
//			initAnim();}
	}

	private void initAnim()
	{
		in  = AnimationUtils.loadAnimation(getContext(), R.anim.turn_around);
		out = AnimationUtils.loadAnimation(getContext(), R.anim.turn_around_back);
		fade_in = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
		fade_in.setDuration(500);
		fade_out = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
		fade_out.setDuration(100);
		//		imageSwitcher.setInAnimation(in);
		//		imageSwitcher.setOutAnimation(fade_out);
	}

	//================================================================================
	// Overriding default behavior
	//================================================================================

	@Override
	public void setOnClickListener(@Nullable OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}

	public void setOnPressedListener(@Nullable OnClickListener onClickListener) {
		this.onPressedListener = onClickListener;
	}

	private void setCustomTouchListener() {
		super.setOnTouchListener(new TouchListener());
	}

	@Override
	public void setOnTouchListener(OnTouchListener onTouchListener) {
		this.onTouchListener = onTouchListener;
	}

	public OnTouchListener getOnTouchListener() {
		return onTouchListener;
	}

	private void onTouchDown(MotionEvent motionEvent) {
//		setCustomPressed(true);
//		refreshDrawableState();
//		setPressed(true);
	}

	private void setCustomPressed(boolean custom_pressed)
	{
		this.custom_pressed = custom_pressed;
	}

	private void onTouchUp(MotionEvent motionEvent) {
		// Handle user defined click listeners
//		setCustomPressed(false);
//		refreshDrawableState();
//		setPressed(false);

		if (uncheckable) {
			setChecked(!isChecked());
		} else {
			setChecked(true);
		}
		if (onClickListener != null) {
			onClickListener.onClick(this);
		}

	}
	//================================================================================
	// Public methods
	//================================================================================

	public void setCheckedState() {
		setSelected(true);
	}

	public void setNormalState() {
		setSelected(false);
	}

	public int getValue(){
		return  value;
	}

	public void setValue(int value){
		this.value = value;
	}

	public int getPosition(){
		return position;
	}

	public void setPosition(int position){
		this.position = position;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
		setTitleText();
	}

	private void setTitleText()
	{
		if(null!=titleTextView && null!=title)titleTextView.setText(title);
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
		setSubTitleText();
	}

	private void setSubTitleText(){
		if(null!=subTitleTextView && null!=subTitle && !subTitle.equals("")){
			subTitleTextView.setText(subTitle);
			subTitleTextView.setVisibility(VISIBLE);
		}
	}

	public void setImage(int drawable){
		this.image = drawable;
		setImageViewImage();
	}

	private void setImageViewImage()
	{
		if(null!=imageSwitcher){
			if(image == 0) image = R.drawable.ic_check_circle_grey_72;
			imageSwitcher.setImageDrawable(getResources().getDrawable(image));
		}
	}

	public void setPressedImage(Drawable drawable){
		this.imagePressed = drawable;
	}

	public void setTitleTextColor(int color){
		this.titleTextColor = color;
	}

	public void setSubTitleTextColor(int color){
		this.subTitleTextColor = color;
	}

	public void setPressedTitleTextColor(int color){
		this.pressedTitleTextColor = color;
	}


	public void setAnim(boolean b){
		this.animation = b;
	}
	//================================================================================
	// Checkable implementation
	//================================================================================

	public void setUncheckable(boolean uncheckable){
		this.uncheckable = uncheckable;
	}

	@Override
	public void setChecked(boolean checked) {
		if (this.checked != checked) {
			this.checked = checked;
			if (!onCheckedChangeListeners.isEmpty()) {
				for (int i = 0; i < onCheckedChangeListeners.size(); i++) {
					onCheckedChangeListeners.get(i).onCheckedChanged(this, this.checked);
				}
			}
			if (this.checked) {
				setCheckedState();
			} else {
				setNormalState();
			}
		}
	}

	@Override
	public boolean isChecked() {
		return checked;
	}

	@Override
	public void toggle() {
		setChecked(!checked);
	}

	@Override
	public void addOnCheckChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
		onCheckedChangeListeners.add(onCheckedChangeListener);
	}

	@Override
	public void removeOnCheckChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
		onCheckedChangeListeners.remove(onCheckedChangeListener);
	}

	public int getIndex()
	{
		return index;
	}

	public void setIndex(int id){
		this.index = id;
	}

	public void setBarType(MainBar.Type barType)
	{
		this.barType = barType;
	}

	public MainBar.Type getBarType()
	{
		return barType;
	}

	public void setSubType(MainBar.SubType subType)
	{
		this.subType = subType;
	}
	public MainBar.SubType getSubType(){
		return this.subType;
	}

	public void setElementId(String id)
	{
		this.elementId = id;
	}

	public String getElementId()
	{
		return elementId;
	}

	public boolean getCheckable()
	{
		return checked;
	}

	//================================================================================
	// Inner classes
	//================================================================================
	private final class TouchListener implements OnTouchListener
	{

		@Override
		public boolean onTouch(final View v, MotionEvent event) {

			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					onTouchDown(event);
					break;
				case MotionEvent.ACTION_UP:
					onTouchUp(event);
				case MotionEvent.ACTION_CANCEL:

					break;
				default:
					break;
			}
			if (onTouchListener != null) {
				onTouchListener.onTouch(v, event);
			}
			return true;
		}
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		if(custom_pressed) mergeDrawableStates(drawableState, STATE_PR);
		return drawableState;
	}
}
