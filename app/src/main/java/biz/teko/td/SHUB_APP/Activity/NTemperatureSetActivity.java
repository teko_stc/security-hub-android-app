package biz.teko.td.SHUB_APP.Activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;

public  class NTemperatureSetActivity extends  BaseActivity
{
	private Site site;
	private Device device;
	private Section section;
	private Zone zone;

	private LinearLayout buttonClose;
	private LinearLayout buttonNext;
	private boolean transition;
	private NEditText editValue;
	private TextView editTitle;
	private String name;
	private Context context;
	private DBHelper dbHelper;
	private UserInfo userInfo;
	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private int sending = 0;
	private int type;

	private int site_id;
	private int device_id;
	private int section_id;
	private int zone_id;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_temp_set);

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();

		device_id = getIntent().getIntExtra("device", -1);
		section_id = getIntent().getIntExtra("section", -1);
		zone_id = getIntent().getIntExtra("zone", -1);
		transition = getIntent().getBooleanExtra("transition", false);

		if(-1!=device_id) device = dbHelper.getDeviceById(device_id);
		if(-1!=section_id) section = dbHelper.getDeviceSectionById(device_id, section_id);
		if(-1!=zone_id) zone = dbHelper.getZoneById(device_id, section_id, zone_id);

		addTransitionListener(transition);

		setup();
		bind();

	}

	private void bind()
	{
		if(null!=buttonClose) buttonClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		if(null!=buttonNext) buttonNext.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				setValue();
			}
		});

		if(null!=editValue) {
			editValue.setFocusableInTouchMode(true);

			if(null!=name) {
				editValue.setText(name);
				editValue.setSelection(editValue.getText().length());
				try
				{
					Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
					f.setAccessible(true);
					f.set(editValue, R.drawable.n_cursor);
				} catch (Exception ignored)
				{
				}
				editValue.setSelection(editValue.getText().length());
			}
			editValue.setInputType(InputType.TYPE_CLASS_NUMBER);
			editValue.setOnEditorActionListener(new TextView.OnEditorActionListener()
			{
				@Override
				public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent)
				{
					if(i == EditorInfo.IME_ACTION_DONE){
						setValue();
					}
					return false;
				}
			});
			editValue.showKeyboard();
		}
		if(null!=editTitle) {
			editTitle.setText(R.string.N_TEMP_SET_TITLE);
		}
	}

	private void setValue()
	{
		if(validate()){
			setNewValue();
		}else{
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
			nDialog.setTitle(getString(R.string.N_TEMP_SET_CHECK_ENTERED_DATA));
			nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
			{
				@Override
				public void onActionClick(int value, boolean b)
				{
					switch (value){
						case NActionButton.VALUE_CANCEL:
							nDialog.dismiss();
							break;
					}
				}
			});
			nDialog.show();
		}
	}

	private void setNewValue()
	{
		String newName = editValue.getText().toString();
		JSONObject commandAddr = new JSONObject();
		try
		{
			commandAddr.put("section", zone.section_id);
			commandAddr.put("zone", zone.id);
			commandAddr.put("value", Integer.valueOf(newName));
			if (nSendMessageToServer(section, Command.SWITCH, commandAddr, true))
			{
				onBackPressed();
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	private boolean validate()
	{
		if(null!=editValue.getText().toString() && !editValue.getText().toString().equals("")){
			return true;
		}
		return false;
	}

	private void setup()
	{
		buttonClose = (LinearLayout) findViewById(R.id.nButtonClose);
		buttonNext = (LinearLayout) findViewById(R.id.nButtonNext);
		editTitle = (TextView) findViewById(R.id.nTextTitle);
		editValue = (NEditText) findViewById(R.id.nText);
	}

	public void  onResume(){
		super.onResume();

		bindD3();
	}

	public void onPause(){
		super.onPause();
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

}
