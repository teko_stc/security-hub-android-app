package biz.teko.td.SHUB_APP.UDP;

import android.content.Context;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.UdpDevice;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.ReplyConfig;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.ReplyConfig1h;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.ReplyConfig1hNg;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.ReplyConfigNg;
import biz.teko.td.SHUB_APP.UDP.Entities.RequestConfig;
import biz.teko.td.SHUB_APP.UDP.Entities.RequestFactoryReset;
import biz.teko.td.SHUB_APP.UDP.Entities.RequestReset;
import biz.teko.td.SHUB_APP.UDP.Entities.RequestSurvey;
import biz.teko.td.SHUB_APP.UDP.Entities.SendConfig;
import biz.teko.td.SHUB_APP.UDP.Entities.TestMode;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;
import biz.teko.td.SHUB_APP.Utils.Callback;
import io.reactivex.Completable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.AsyncSubject;
import io.reactivex.subjects.PublishSubject;

@Deprecated
public class LocalConnection {
	private static final Charset UTF8_CHARSET = StandardCharsets.UTF_8;
	private static final Charset UCS_CHARSET = Charset.forName("UNICODE");
	private static final Charset CP1251_CHARSET = Charset.forName("Windows-1251");
	private static final Charset LATIN_CHARSET = StandardCharsets.ISO_8859_1;
	private static final Charset ASCII_CHARSET = StandardCharsets.US_ASCII;
	private static LocalConnection instance;
	private final PublishSubject<UdpDevice> testModeSubject = PublishSubject.create();
	private final PublishSubject<Boolean> sendConfigSubject = PublishSubject.create();
	private AsyncSubject<HashMap<String, String>> configSubject = AsyncSubject.create();
	private static ReplyConfig config;
	private static ReplyConfigNg configNg;
	private static ReplyConfig1h config1h;
	private static ReplyConfig1hNg config1hNg;

	public UDPThread udpThread;

	public LocalConnection() {
	}

	public static LocalConnection getInstance(){
		if( null == instance){
			instance = new LocalConnection();
		}
		return instance;
	}

	public ReplyConfigNg getConfigNg() {
		return configNg;
	}

	public ReplyConfig getConfig() {
		return config;
	}

	public ReplyConfig1h getConfig1h() {
		return config1h;
	}

	public ReplyConfig1hNg getConfig1hNg() {
		return config1hNg;
	}

	public PublishSubject<Boolean> getSendConfigSubject() {
		return sendConfigSubject;
	}

	public PublishSubject<UdpDevice> getTestModeSubject() {
		return testModeSubject;
	}

	public AsyncSubject<HashMap<String, String>> getConfigSubject() {
		return configSubject;
	}

	public void establishConnection() {
		if (null != udpThread) {
			udpThread.stopRunning();
//			udpThread.getUdpSocket().close();
		}
		udpThread = new UDPThread();
		udpThread.start();
		configSubject = AsyncSubject.create();
	}

	public void stop() {
		if (null != udpThread) {

			udpThread.stopRunning();
//			udpThread.getUdpSocket().close();
		}
	}

	public boolean getConnectionStatus() {
		if (this.udpThread == null) {
			return false;
		}
		return !this.udpThread.isRunning();
	}

	public static byte[] invertByte(byte[] bytes) {
		for (int i = 0; i < bytes.length / 2; i++) {
			byte temp = bytes[i];
			bytes[i] = bytes[bytes.length - i - 1];
			bytes[bytes.length - i - 1] = temp;
		}
		return bytes;
	}

	public void sendSurveyRequest() {
		if (this.udpThread == null) {
			return;
		}
		this.udpThread.sendSurveyRequest();
	}

	public void sendTestModeOn(String ip, int pin) {
		if (this.udpThread == null) {
			return;
		}
		this.udpThread.sendTestModeOn(ip, pin);
	}

	public void sendTestModeOff(String ip, int pin) {
		if (this.udpThread == null) {
			return;
		}
		this.udpThread.sendTestModeOff(ip, pin);
	}

	public void sendTestModeOffWithCallback(String ip, int pin, Callback callback) {
		if (this.udpThread == null) {
			return;
		}
		this.udpThread.sendTestModeOffWithCallback(ip, pin, callback);
	}


	public void sendConfig(String ip, int pin, byte[] data) {
		if (this.udpThread == null) {
			return;
		}
		this.udpThread.sendConfig(ip, pin, data);
	}

	public void sendReset(String ip, int pin) {
		if (this.udpThread == null) {
			return;
		}
		this.udpThread.setReset(ip, pin);
	}

	public void sendRequestConfig(String ip, int pin) {
		if (this.udpThread == null) {
			return;
		}
		this.udpThread.sendRequestConfig(ip, pin);
	}

	public void sendFactoryReset(String ip, int pin) {
		if (this.udpThread == null) {
			return;
		}
		this.udpThread.setFactoryReset(ip, pin);
	}

	public static byte[] deleteZeros(byte[] bytes) {
		List<Byte> list = new ArrayList<>();
		for (byte aByte : bytes) list.add(aByte);
		list.removeAll(Arrays.asList((byte) 0));
		bytes = new byte[list.size()];
		for (int i = 0; i < list.size(); i++)
			bytes[i] = list.get(i);
		return bytes;
	}

	public static String decodeUTF8(byte[] bytes) {
		return new String(deleteZeros(bytes), UTF8_CHARSET);
	}

	public static String decodeUNICODE(byte[] bytes) {
		return new String(bytes, UCS_CHARSET);
	}

	public static String decodeCP1251(byte[] bytes) {
		return new String(bytes, CP1251_CHARSET);
	}

	public static String decodeLatin(byte[] bytes) {
		return new String(bytes, LATIN_CHARSET);
	}

	public static String decodeASCIIn(byte[] bytes) {
		return new String(bytes, ASCII_CHARSET);
	}

	public static int byteArrayToInt(byte[] b) {
		if (b.length == 8) {
			return b[0] << 56 | (b[1] & 0xff) << 48 | (b[2] & 0xff) << 40 | (b[3] & 0xff) << 32 | b[4] << 24 | (b[5] & 0xff) << 16 | (b[6] & 0xff) << 8 | (b[7] & 0xff);
		} else {
			if (b.length == 4)
				return b[0] << 24 | (b[1] & 0xff) << 16 | (b[2] & 0xff) << 8 | (b[3] & 0xff);
			else if (b.length == 2)
				return 0x00 << 24 | 0x00 << 16 | (b[0] & 0xff) << 8 | (b[1] & 0xff);
		}
		return 0;
	}

	public static byte[] leIntToByteArray(int i) {
		final ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putInt(i);
		return bb.array();
	}

	public static byte[] leShortToByteArray(short i) {
		final ByteBuffer bb = ByteBuffer.allocate(Short.SIZE / Byte.SIZE);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putShort(i);
		return bb.array();
	}

	public class UDPThread extends Thread {
		private final Context context;
		private DatagramSocket udpSocket;
		private boolean running = false;
		private static final int PORT = 22239;
		private static final int COMMAND_PORT = 22238;


		public UDPThread() {
			super();
			this.context = App.getContext();
			initSocket();
		}

		public void initSocket() {
			/*if (udpSocket != null) {
				closeSocket();
			}*/
			newSocket();
		}

		private void newSocket() {
			try {
				udpSocket = new DatagramSocket(PORT);
				udpSocket.setBroadcast(true);
				udpSocket.setReuseAddress(true);
				System.err.println("NEW SOCKET");
				System.out.println(udpSocket.getPort());
			} catch (SocketException e) {
				e.printStackTrace();
			}

		}

		public  DatagramSocket getUdpSocket(){
			return this.udpSocket;
		}

		public boolean getStatus(){
			return running;
		}

		public void stopRunning() {
			running = false;
			closeSocket();
		}

		private void closeSocket(){
			if(null!=udpSocket) {
				if (udpSocket.isConnected())
					udpSocket.disconnect();
				udpSocket.close();
			}
		}

		public void run(){
			running = true;
			while (running) {
				try {
					if (!udpSocket.isClosed())
						receive();
					else {
						break;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		private void receive() throws IOException {
			System.out.println("receive");
			byte[] start = new byte[800];
			DatagramPacket receiveData = new DatagramPacket(start, start.length);

			if (null != udpSocket) {
				udpSocket.receive(receiveData);
				if ((start[0] & 0xff) == 255 && receiveData.getLength() > 10) {
					InetAddress IPAddress = receiveData.getAddress();
					if (!IPAddress.equals(getMyAddress())) {
						byte[] sn_byte = new byte[4];
						System.arraycopy(start, 5, sn_byte, 0, sn_byte.length);
						int serial = ByteBuffer.wrap(invertByte(sn_byte)).getInt();

						byte[] config_byte = new byte[4];
						System.arraycopy(start, 9, config_byte, 0, config_byte.length);
						int config = ByteBuffer.wrap((config_byte)).getInt();

						String version = start[4] + "." + start[3];

						int testMode = start[19];

						testModeSubject.onNext(new UdpDevice(testMode, IPAddress.getHostAddress(), serial, config, version));

						//						Intent intent = new Intent(D3Service.BROADCAST_GET_UDP_SURVEY);
						//						intent.putExtra("serial", serial);
						//						intent.putExtra("config", config);
						//						intent.putExtra("ip", IPAddress.getHostAddress());
						//						context.sendBroadcast(intent);
					}
				}
				if ((start[0] & 0x7F) == RequestConfig._command && receiveData.getLength() > 0) {
					int hubVersion = -1;
					HashMap<String, String> map = new HashMap<>();
					if (receiveData.getLength() == ConfigConstants.hubVersion2) {
						hubVersion = 2;
						byte[] dataHub2 = new byte[ConfigConstants.hubVersion2 - 1];
						System.arraycopy(start, 1, dataHub2, 0, dataHub2.length);
						config = new ReplyConfig(dataHub2);
						map = config.getMap();
					} else if (receiveData.getLength() == ConfigConstants.hubVersion1) {
						hubVersion = 1;
						byte[] dataHub1 = new byte[ConfigConstants.hubVersion1 - 1];
						System.arraycopy(start, 1, dataHub1, 0, dataHub1.length);
						config1h = new ReplyConfig1h(dataHub1);
						map = config1h.getMap();
					} else if (receiveData.getLength() == ConfigConstants.hubVersion3) {
						hubVersion = 3;
						byte[] dataHub3 = new byte[ConfigConstants.hubVersion3 - 2];
						System.arraycopy(start, 1, dataHub3, 0, dataHub3.length);
						configNg = new ReplyConfigNg(dataHub3);
						map = configNg.getMap();
					} else if (receiveData.getLength() == ConfigConstants.hubVersion4) {
						hubVersion = 4;
						byte[] dataHub4 = new byte[ConfigConstants.hubVersion3 - 1];
						System.arraycopy(start, 1, dataHub4, 0, dataHub4.length);
						config1hNg = new ReplyConfig1hNg(dataHub4);
						map = config1hNg.getMap();
					}
					if (hubVersion != -1) {
						map.put(ConfigConstants.HUB_VERSION, String.valueOf(hubVersion));
						if (!configSubject.hasComplete()) {
							configSubject.onNext(map);
							configSubject.onComplete();
						}
					}
				}
				if ((start[0] & 0x7F) == SendConfig._command && !receiveData.getAddress().equals(getMyAddress())) {
					sendConfigSubject.onNext(true);
				}
			} else {
				//initSocket();
			}

		}

		private void sendUdpPacket(byte[] buf, String ip) {
			try {
				DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(ip), PORT);
				Completable.fromAction(() -> udpSocket.send(packet))
						.subscribeOn(Schedulers.io())
						.subscribe(() -> {
						}, throwable -> {
						}).isDisposed();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void sendUdpPacketWithCallback(byte[] buf, String ip, Callback stateChangeListener) {
			try {
				DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(ip), PORT);
				Completable.fromAction(() -> udpSocket.send(packet))
						.subscribeOn(Schedulers.io())
						.subscribe(stateChangeListener::complete, throwable -> {
							stateChangeListener.complete();
						}).isDisposed();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		private void setReset(String ip, int pin) {
			sendUdpPacket(new RequestReset(pin).getBytes(), ip);
		}

		private void setFactoryReset(String ip, int pin) {
			sendUdpPacket(new RequestFactoryReset(pin).getBytes(), ip);
		}

		private void sendConfig(String ip, int pin, byte[] data) {
			sendUdpPacket(new SendConfig(data, pin).getBytes(), ip);
		}

		private void sendRequestConfig(String ip, int pin) {
			sendUdpPacket(new RequestConfig(pin).getBytes(), ip);
		}

		private void sendTestModeOff(String ip, int pin) {
			sendUdpPacket(new TestMode(TestMode.TestModeState.Off, pin).getBytes(), ip);
		}

		private void sendTestModeOffWithCallback(String ip, int pin, Callback callback) {
			sendUdpPacketWithCallback(new TestMode(TestMode.TestModeState.Off, pin).getBytes(), ip, callback);
		}

		private void sendTestModeOn(String ip, int pin) {
			sendUdpPacket(new TestMode(TestMode.TestModeState.On, pin).getBytes(), ip);
		}

		private void sendSurveyRequest() {
			sendUdpPacket(new RequestSurvey().getBytes(), "255.255.255.255");
		}

		private InetAddress getBroadcastAddress() throws IOException {
			WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			DhcpInfo dhcp = wifi.getDhcpInfo();
			// handle null somehow

			int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
			byte[] quads = new byte[4];
			for (int k = 0; k < 4; k++)
				quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
			return InetAddress.getByAddress(quads);
		}

		private InetAddress getMyAddress() throws IOException {
			WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			DhcpInfo dhcp = wifi.getDhcpInfo();
			// handle null somehow

			int ip = dhcp.ipAddress;
			byte[] quads = new byte[4];
			for (int k = 0; k < 4; k++)
				quads[k] = (byte) ((ip >> k * 8) & 0xFF);
			return InetAddress.getByAddress(quads);
		}

		public boolean isRunning() {
			return running;
		}
	}
}
