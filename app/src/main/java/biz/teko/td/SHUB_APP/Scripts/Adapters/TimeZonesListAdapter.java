package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class TimeZonesListAdapter extends ArrayAdapter<Integer>
{
	private final Context context;
	private final int resource;
	private final Integer[] tZoneOffsets;
	private final String curTZone;
	private final LayoutInflater layoutInflater;
	private final HashMap<Integer, String> tzMap;

	public TimeZonesListAdapter(Context context, int resource, Integer[] tZoneOffsets, HashMap<Integer, String> tzMap, String curTZone)
	{
		super(context, resource);
		this.context = context;
		this.resource = resource;
		this.tZoneOffsets = tZoneOffsets;
		this.curTZone = curTZone;
		this.tzMap = tzMap;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		int tZoneOffset = tZoneOffsets[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(null!=tzMap? tzMap.get(tZoneOffset):"");
		if(Integer.valueOf(curTZone)*60 == tZoneOffset){
			view.setBackgroundColor(context.getResources().getColor(R.color.brandColorLightPattern));
			textView.setTextColor(context.getResources().getColor(R.color.brandColorAccentColor));
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		}else{
			view.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
			textView.setTextColor(context.getResources().getColor(R.color.brandColorTextTitle));
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
		}
		return view;
	}

	public int getCount() {
		return tZoneOffsets.length;
	}

	public Integer getItem(int position){
		return tZoneOffsets[position];
	}

	public long getItemId(int position){
		return position;
	}
}
