package biz.teko.td.SHUB_APP.SectionsTabs;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SectionsTabs.Adapters.SectionsViewPagerAdapter;
import biz.teko.td.SHUB_APP.SlidingTabsLib.SlidingTabLayout;

/**
 * Created by td13017 on 10.02.2017.
 */

public class SectionsActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private String siteName;
	private SectionsViewPagerAdapter adapter;
	private CharSequence[] titles;
	private int numboftabs = 2;
	private ViewPager pager;
	private SlidingTabLayout tabs;
	private int siteId;
	private int deviceId;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private boolean fabShown  = false;
	private int sending = 0;

	private BroadcastReceiver connectionRepairReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int status = intent.getIntExtra("status", -1);
			if(status == 1)
			{
				setStatusConnected();
			}
		}
	};

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sections);

		context = this;
		dbHelper = DBHelper.getInstance(this);

		siteId= getIntent().getIntExtra("site", -1);
		deviceId= getIntent().getIntExtra("device", -1);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		if(-1!=siteId)
		{
			siteName = dbHelper.getSiteNameById(siteId);
			toolbar.setTitle(siteName);
		}
		if(-1!=deviceId)
		{
			Device device = dbHelper.getDeviceById(deviceId);
			if (null != device)
			{
				toolbar.setSubtitle(device.getName() + "(" +  Integer.toString(device.account) + ")  •  " + getString(R.string.SECTIONS));
			}
		}else{
			toolbar.setSubtitle(getString(R.string.SECTIONS));
		}
		toolbar.setSubtitleTextColor(getResources().getColor(R.color.brandColorWhite));

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		// Creating The SitesViewPagerAdapter and Passing Fragment Manager, titles fot the Tabs and Number Of Tabs.
		titles = new CharSequence[]{getString(R.string.SECTIONS), getString(R.string.SUBTITLE_EVENTS)};
		adapter =  new SectionsViewPagerAdapter(getSupportFragmentManager(), titles, numboftabs, siteId, deviceId);

		// Assigning ViewPager View and setting the adapter
		pager = (ViewPager) findViewById(R.id.sectionsPager);
		pager.setAdapter(adapter);

		// Assiging the Sliding Tab Layout View
		tabs = (SlidingTabLayout) findViewById(R.id.sectionsTabs);
		tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

		// Setting Custom ColorX for the Scroll bar indicator of the Tab View
		tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
			@Override
			public int getIndicatorColor(int position) {
				return getResources().getColor(R.color.tabsScrollColor);
			}
		});

		// Setting the ViewPager For the SlidingTabsLayout
		tabs.setViewPager(pager);
	}

	public void onDestroy()
	{
		super.onDestroy();
	}

	public void onStop()
	{
		super.onStop();
	}

	public void onStart()
	{
		super.onStart();
	}

	public void  onResume(){
		super.onResume();
		registerReceiver(connectionRepairReceiver, new IntentFilter(D3Service.BROADCAST_CONNECTION_STATUS_CHANGE));
		bindD3();
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(connectionRepairReceiver);
		unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
				setStatusConnected();
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	public boolean sendMessageToServer(D3Element d3Element, D3Request d3Request, boolean command)
	{
		return  false;
//				Func.sendMessageToServer(d3Element ,d3Request, getLocalService(), context, command, sending);
	};

	private void setStatusConnected(){
		if(null!=myService)
		{
			final RelativeLayout noConnLayout = (RelativeLayout) findViewById(R.id.noConnLayout);
			if (myService.getStatus())
			{
				if (null != noConnLayout) noConnLayout.setVisibility(View.GONE);
				pager.setVisibility(View.VISIBLE);
			} else
			{
				if (null != noConnLayout) noConnLayout.setVisibility(View.VISIBLE);
				pager.setVisibility(View.GONE);
			}
		}
	}

	public void setFabStatus(boolean status){
		fabShown = status;
	}

	@Override
	public void onBackPressed()
	{
		if(fabShown){
			adapter.closeFab();
		}else
		{
			super.onBackPressed();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
	}
}
