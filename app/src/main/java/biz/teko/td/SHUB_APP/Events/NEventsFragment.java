package biz.teko.td.SHUB_APP.Events;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.util.Pair;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.datepicker.CalendarConstraints;
import com.google.android.material.datepicker.MaterialDatePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Adapter.HistoryClassesSpinnerAdapter;
import biz.teko.td.SHUB_APP.Adapter.HistorySitesSpinnerAdapter;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapterNew;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NEAListElement;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class NEventsFragment extends Fragment {
	private final String DATE_DIALOG_TAG = "DATE_TAG";
	private final BaseActivity activity;

	private View layout;

	public NEventsFragment(BaseActivity activity) {
		this.activity = activity;
	}

	private boolean bound;
	private D3Service myService;
	private ServiceConnection serviceConnection;

	private DBHelper dbHelper;
	private PrefUtils prefUtils;
	private UserInfo userInfo;
	private FrameLayout filterFrame;
	private FrameLayout filterSetFrame;
	private FrameLayout filterLayout;
	private LinearLayout buttonFilter;
	private RecyclerView eventList;
	private NEventsListAdapterNew eventAdapter;
	private ImageView buttonImageFilter;
	private Filter filter;
	private NEAListElement filterSite;
	private NEAListElement filterClass;
	private NEAListElement filterTo;
	private NEAListElement filterFrom;
	private boolean animationEnd;
	private boolean isLoading = false;
	private ProgressBar progressBar;
	private AppBarLayout appBarLayout;
	private ArrayList<Long> eventTimes = new ArrayList<>();

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			updateBottomHelper();
		}
	};

	private void bind() {

		if (filter == null) {
			filter = new Filter();
		} else {
			if (filterSite != null && dbHelper.getSiteById(filter.site) != null)
				filterSite.setTitle(dbHelper.getSiteById(filter.site).name);
			if (filterClass != null)
				for (EClass e : getClasses().first) {
					if (e.id == filter.eClass) {
						filterClass.setTitle(e.caption);
						break;
					}
				}
		}
		changeCallback(filter);
		appBarLayout.setExpanded(false, false);

		ViewCompat.setNestedScrollingEnabled(layout.findViewById(R.id.nListEvents), false);
		CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
		if (params.getBehavior() == null)
			params.setBehavior(new AppBarLayout.Behavior());
		((AppBarLayout.Behavior) params.getBehavior()).setDragCallback(new AppBarLayout.Behavior.DragCallback() {

			@Override
			public boolean canDrag(@NonNull AppBarLayout appBarLayout) {
				return false;
			}
		});
		appBarLayout.addOnOffsetChangedListener(
				(appBarLayout, verticalOffset) -> {
					if (verticalOffset == 0) {
						ViewCompat.setNestedScrollingEnabled(layout.findViewById(R.id.nListEvents), true);
						if (null != buttonImageFilter) buttonImageFilter.setImageDrawable(
								ResourcesCompat.getDrawable(getResources(), R.drawable.ic_stair_up_inverted, activity.getTheme()));
					} else {
						if (appBarLayout.isLifted()) {
							ViewCompat.setNestedScrollingEnabled(layout.findViewById(R.id.nListEvents), false);
						}
						if (null != buttonImageFilter) buttonImageFilter.setImageDrawable(
								ResourcesCompat.getDrawable(getResources(), R.drawable.ic_stair_down_inverted, activity.getTheme()));
					}
				});
		if (null != buttonFilter) buttonFilter.setOnClickListener(view -> resizeFilter());

		if (null != filterFrame) filterFrame.setOnClickListener(view -> resizeFilter());

		MaterialDatePicker<Pair<Long, Long>> datePickingDialog = MaterialDatePicker.Builder.dateRangePicker()
				.setSelection(
						Pair.create(
								filter.from,
								filter.to
						)
				)
				.setCalendarConstraints(new CalendarConstraints.Builder()
						.setStart(getCalendarStart())
						.setEnd(getCalendarEnd())
						.build())
				.build();

		datePickingDialog.addOnPositiveButtonClickListener(selection -> {
			if (filter.from != Func.getStartOfDay(selection.first) || filter.to != Func.getEndOfDay(selection.second)) {
				filter.from = Func.getStartOfDay(selection.first);
				filter.to = Func.getEndOfDay(selection.second);
				long dbTime = dbHelper.getEventMinTime() * 1000;
				if ((dbTime > filter.from)) {
					getDataFromServer();
				}
				filterFrom.setTitle(Func.getDate(activity, filter.from, "dd.MM.yyyy"));
				filterTo.setTitle(Func.getDate(activity, filter.to, "dd.MM.yyyy"));
//				getEventTimes(filter.from);
				updateList();
				changeCallback(filter);
			}
		});
		if (null != filterFrom) {
			filterFrom.setTitle(Func.getDate(activity, filter.from, "dd.MM.yyyy"));
			Calendar oldCalendar = Calendar.getInstance();
			oldCalendar.setTimeInMillis(filter.from);
//			getEventTimes(filter.from);
			filterFrom.setOnElementClickListener(() ->
					datePickingDialog.show(getChildFragmentManager(), DATE_DIALOG_TAG));
		}

		if (null != filterTo) {
			filterTo.setTitle(Func.getDate(activity, filter.to, "dd.MM.yyyy"));
			filterTo.setOnElementClickListener(() ->
					datePickingDialog.show(getChildFragmentManager(), DATE_DIALOG_TAG));
		}


		if (null != filterClass) {
			filterClass.setOnElementClickListener(this::showBottomClassSelectDialog);
		}

		if (null != filterSite) {
			filterSite.setOnElementClickListener(this::showBottomSiteSelectDialog);
		}


		updateList();
	}

	private long getCalendarStart() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(2010, 1, 1);
		return calendar.getTimeInMillis();
	}

	private long getCalendarEnd() {
		return Calendar.getInstance().getTimeInMillis();
	}

	private void updateBottomHelper(){
		activity.setBottomInfoBehaviour(activity);
	}

	private final BroadcastReceiver eventReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
			updateBottomHelper();
		}
	};

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.n_activity_events, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		layout = getView();
		activity.getSwipeBackLayout().setEnableGesture(false);
		setup();
		bind();

	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		this.dbHelper = DBHelper.getInstance(activity);
		this.prefUtils = PrefUtils.getInstance(activity);
		this.userInfo = dbHelper.getUserInfo();
		serviceConnection = getServiceConnection();
		activity.setOnBackPressedCallback(this::onBackPressed);
	}

	private void setup() {

		appBarLayout = layout.findViewById(R.id.appbar);
		filterLayout = layout.findViewById(R.id.nEventsFilterLayout);
		filterFrame = layout.findViewById(R.id.nEventsFilter2);
		filterSetFrame = layout.findViewById(R.id.nEventsFilterFrame);

		buttonFilter = layout.findViewById(R.id.nButtonShow);
		buttonImageFilter = layout.findViewById(R.id.nImageShow);
//		targetHeight = filterSetFrame.getLayoutParams().height;

		filterSite = layout.findViewById(R.id.nSelectorEventFilterSite);
		filterClass = layout.findViewById(R.id.nSelectorEventFilterClass);
		filterFrom = layout.findViewById(R.id.nSelectorEventFilterFrom);
		filterTo = layout.findViewById(R.id.nSelectorEventFilterTo);

		eventList = layout.findViewById(R.id.nListEvents);

		progressBar = layout.findViewById(R.id.progressBar);
	}

	private void changeCallback(Filter filter) {
		if (filter.to == filter.DEFAULT_TO && filter.eClass == filter.DEFAULT_ECLASS &&
				filter.from == filter.DEFAULT_FROM && filter.site == filter.DEFAULT_SITE) {
			filterLayout.setActivated(false);
			layout.findViewById(R.id.nEventsFilterLayout2).setActivated(false);
		} else {
			filterLayout.setActivated(true);
			layout.findViewById(R.id.nEventsFilterLayout2).setActivated(true);
		}
	}


	private String[] getSitesArray(Site[] sites) {
		String[] sitesArray;
		if (null != sites) {
			sitesArray = new String[sites.length + 1];
			int i1 = 1;
			for (Site site : sites) {
				sitesArray[i1] = site.name;
				i1++;
			}
		} else {
			sitesArray = new String[1];
		}
		sitesArray[0] = getString(R.string.ALL);
		return sitesArray;
	}

	private void showBottomSiteSelectDialog() {
		final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(activity);

		View bottomView = activity.getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
		TextView title = bottomView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.N_EVENTS_CHOOSE_SITE);
		ListView listView = bottomView.findViewById(R.id.dialogBottomList);

		Site[] sites = dbHelper.getAllSitesArray();
		Site[] sitesArray;
		if (null != sites) {
			sitesArray = new Site[sites.length + 1];
			int i1 = 1;
			for (Site site : sites) {
				sitesArray[i1] = site;
				i1++;
			}
		} else {
			sitesArray = new Site[1];
		}
		sitesArray[0] = new Site(0, getString(R.string.ALL));


		final HistorySitesSpinnerAdapter historySitesAdapter = new HistorySitesSpinnerAdapter(activity, R.layout.spinner_item_dropdown, sitesArray, filter.site);
		listView.setAdapter(historySitesAdapter);
		listView.setDivider(null);
		listView.setOnItemClickListener((parent, view, position, id) -> {
			Site site = (Site) parent.getItemAtPosition(position);
			bottomSheetDialog.dismiss();
			filter.site = site.id;
			updateList();
			filterSite.setTitle(site.name);
			changeCallback(filter);
		});

		bottomSheetDialog.setContentView(bottomView);

		bottomSheetDialog.setOnShowListener(dialog -> {
			FrameLayout bottomSheet = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

			if (null != bottomSheet) {
				BottomSheetBehavior<FrameLayout> behavior = BottomSheetBehavior.from(bottomSheet);
				behavior.setHideable(false);
			}
		});

		try {
			Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
			mBehaviorField.setAccessible(true);

			final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
			behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
				@Override
				public void onStateChanged(@NonNull View bottomSheet, int newState) {
					if (newState == BottomSheetBehavior.STATE_DRAGGING) {
						behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
					}
				}

				@Override
				public void onSlide(@NonNull View bottomSheet, float slideOffset) {
				}
			});
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}

		bottomSheetDialog.show();
	}

	private void resizeFilter() {
		if (appBarLayout.isLifted()) {
			appBarLayout.setExpanded(true);
			if (null != buttonImageFilter) buttonImageFilter.setImageDrawable(
					ResourcesCompat.getDrawable(getResources(), R.drawable.ic_stair_up_inverted
							, activity.getTheme()));
		} else {
			appBarLayout.setExpanded(false);
			if (null != buttonImageFilter) buttonImageFilter.setImageDrawable(
					ResourcesCompat.getDrawable(getResources(), R.drawable.ic_stair_down_inverted, activity.getTheme()));
		}
	}

	private void updateList() {
		Cursor cursor = updateCursor();
		if (null != cursor && null != eventAdapter && eventList.getAdapter() != null) {
			eventAdapter.update(cursor);
		} else {
			eventAdapter = new NEventsListAdapterNew(activity, R.layout.n_events_list_element, cursor, 0, true);
			if (null != eventList) {
				eventList.setAdapter(eventAdapter);
				//initScrollListener();
			}
		}
		if (null != eventAdapter && 0 != eventAdapter.getItemCount())
		{
			eventList.setVisibility(View.VISIBLE);
		} else
		{
			eventList.setVisibility(View.GONE);
		}
		isLoading = false;
	}

	private Cursor updateCursor()
	{
		return  dbHelper.getEventCursorForHistory(filter.site, filter.eClass, filter.from, filter.to);
	}

	//this method gets a list of unique dates of events. it gets dates, without time.
	private void getEventTimes(long timeFrom){
		eventTimes = dbHelper.getEventTimes(timeFrom);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}


	@Override
	public void onResume()
	{
		super.onResume();
		Intent intent = new Intent(activity, D3Service.class);
		activity.bindService(intent, serviceConnection, 0);

		activity.registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_ALARM_ADD_UPD_RMV);
		activity.registerReceiver(updateReceiver, intentFilter);

		NotificationManager notifManager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.cancelAll();
		updateList();

		updateBottomHelper();
	}

	@Override
	public void onPause() {
		super.onPause();
		activity.unbindService(serviceConnection);
		activity.unregisterReceiver(eventReceiver);
		activity.unregisterReceiver(updateReceiver);
	}

	private Pair<List<EClass>, String[]> getClasses() {
		LinkedList<EClass> eClasses = new LinkedList<>();
		LinkedList<EClass> eClassesList = D3Service.d3XProtoConstEvent.classes;
		ArrayList<String> captions = new ArrayList<>();

		eClasses.add(new EClass(-1, getString(R.string.N_EVENTS_ALL_CLASSES)));
		captions.add(getString(R.string.N_EVENTS_ALL_CLASSES));
		for (EClass eClass : eClassesList) {
			if (eClass.id != Const.CLASS_CONNECTIVITY
					&& eClass.id != Const.CLASS_SUPPLY
					&& eClass.id != Const.CLASS_INNER
					&& eClass.id != Const.CLASS_DEBUG
			) {
				eClasses.add(eClass);
				captions.add(eClass.caption);
			}
		}
		return new Pair<>(eClasses, captions.toArray(new String[eClasses.size()]));
	}

	private void showBottomClassSelectDialog() {
		final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(activity);

		View bottomView = activity.getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
		TextView title = bottomView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.N_EVENTS_CHOOSE_CLASS);
		ListView listView = bottomView.findViewById(R.id.dialogBottomList);
		LinkedList<EClass> eClassesList = D3Service.d3XProtoConstEvent.classes;
		LinkedList<EClass> eClasses = new LinkedList<>();

		eClasses.add(new EClass(-1, getString(R.string.N_EVENTS_ALL_CLASSES)));
		for (EClass eClass : eClassesList) {
			if (eClass.id != Const.CLASS_CONNECTIVITY
					&& eClass.id != Const.CLASS_SUPPLY
					&& eClass.id != Const.CLASS_INNER
					&& eClass.id != Const.CLASS_DEBUG
			) {
				eClasses.add(eClass);
			}
		}
		EClass[] eClassesArray = eClasses.toArray(new EClass[eClasses.size()]);
		if (null != eClassesArray) {
			final HistoryClassesSpinnerAdapter historyClassesAdapter = new HistoryClassesSpinnerAdapter(activity, R.layout.n_card_for_list, eClassesArray, filter.eClass);
			listView.setAdapter(historyClassesAdapter);
			listView.setDivider(null);
			listView.setOnItemClickListener((parent, view, position, id) -> {
				EClass eClass = (EClass) parent.getItemAtPosition(position);
				if (filter.eClass != eClass.id) {
					bottomSheetDialog.dismiss();
					filter.eClass = eClass.id;
					filterClass.setTitle(eClass.caption);
					updateList();
					changeCallback(filter);
				}
			});
		}

		bottomSheetDialog.setContentView(bottomView);

		bottomSheetDialog.setOnShowListener(dialog -> {
			FrameLayout bottomSheet = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

			if (null != bottomSheet) {
				BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
				behavior.setHideable(false);
			}
		});

		try {
			Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
			mBehaviorField.setAccessible(true);

			final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
			behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
				@Override
				public void onStateChanged(@NonNull View bottomSheet, int newState) {
					if (newState == BottomSheetBehavior.STATE_DRAGGING) {
						behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
					}
				}

				@Override
				public void onSlide(@NonNull View bottomSheet, float slideOffset) {
				}
			});
		} catch (NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}

		bottomSheetDialog.show();
	}


	private void resizeWithAnimation(final View view, int duration, int type) {
		int initHeight = 0;
		int targetHeight = 0;

		int availableWidth = getParentWidth(filterSetFrame);

		int widthSpec = View.MeasureSpec.makeMeasureSpec(availableWidth, View.MeasureSpec.AT_MOST);
		int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

		filterSetFrame.measure(widthSpec, heightSpec);
		int measuredHeight = filterSetFrame.getMeasuredHeight();

		switch (type){
			case 0:
				filterSetFrame.setVisibility(View.GONE);
				initHeight = measuredHeight;
				targetHeight = filterFrame.getHeight();
				break;
			case 1:
				filterSetFrame.setVisibility(View.VISIBLE);
				initHeight = filterFrame.getHeight();
				targetHeight = measuredHeight;
				break;
		}

		int finalTargetHeight = targetHeight;
		int finalInitHeight = initHeight;

		final int distance = targetHeight - initHeight;

		view.getLayoutParams().height = initHeight;
		view.setVisibility(View.VISIBLE);



		Animation animation = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if(!animationEnd)
				{
					if (interpolatedTime == 1 && finalTargetHeight == 0)
					{
						view.setVisibility(View.GONE);
					}
					view.getLayoutParams().height = (int) (finalInitHeight + distance * interpolatedTime);
					view.requestLayout();
					if(interpolatedTime == 1){
						animationEnd  = true;
						LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						view.setLayoutParams(layoutParams);
					}
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		animation.setAnimationListener(new Animation.AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{
				animationEnd = false;
			}

			@Override
			public void onAnimationEnd(Animation animation) {}

			@Override
			public void onAnimationRepeat(Animation animation) {}
		});

		animation.setDuration(duration);
		view.startAnimation(animation);
	}

	private int getParentWidth(View viewToScale)
	{
		final ViewParent parent = viewToScale.getParent();
		if (parent instanceof View) {
			final int parentWidth = ((View) parent).getWidth();
			if (parentWidth > 0) {
				return parentWidth;
			}
		}

		throw new IllegalStateException("View to scale must have parent with measured width");
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}


	private void getDataFromServer(){
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject message = new JSONObject();
		long tmax = dbHelper.getEventMinTime();
		try
		{
			message.put("tmin", filter.from/1000);
			//			message.put("tmax", timeTo/1000);
			message.put("tmax", tmax == 0?  System.currentTimeMillis()/1000 : tmax);

		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		D3Request d3Request = D3Request.createMessage(userInfo.roles, "EVENTS", message);
		if(d3Request.error == null){
			sendMessageToServer(d3Request);
		}else{
			Func.pushToast(activity, d3Request.error, activity);
		}
	}

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if (null != myService)
		{
			return myService.send(d3Request.toString());
		} else
		{
			Func.nShowMessage(activity, activity.getString(R.string.EA_D3_OFF_1) + " " + activity.getString(R.string.application_name) + " " + activity.getString(R.string.EA_D3_OFF_2));
		}
		return false;
	}

	public void onBackPressed()
	{
		NDialog nDialog = new NDialog(activity, R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.N_LOGOUT_Q));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value){
				case NActionButton.VALUE_OK:
					activity.finishAffinity();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}

		});
		nDialog.show();
	}

	private class Filter {
		public int site;
		public int eClass;
		public long from;
		public long to;
		public final int DEFAULT_SITE = 0;
		public final int DEFAULT_ECLASS = -1;

		public final long DEFAULT_FROM = MaterialDatePicker.todayInUtcMilliseconds() - 3600 * 24 * 1000;
		public final long DEFAULT_TO = Func.getEndOfDay(System.currentTimeMillis());

		public Filter() {
			this.site = DEFAULT_SITE;
			this.eClass = DEFAULT_ECLASS;
			this.from = DEFAULT_FROM;
			to = DEFAULT_TO;
		}
	}

	//this is the method for adding a header.
	//it is needed to make the header of the dialog box look the same as everywhere else.
	public void brandAlertDialog(AlertDialog.Builder dialogBuilder, String title) {
		try {
			View view = getLayoutInflater().inflate(R.layout.dialog_events_custom_title, null);
			((TextView)view.findViewById(R.id.titleText)).setText(title);
			dialogBuilder.setCustomTitle(view);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
