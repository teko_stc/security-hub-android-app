package biz.teko.td.SHUB_APP.FaqTab;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.FaqTab.Faq.FAnswer;
import biz.teko.td.SHUB_APP.FaqTab.Faq.FImage;
import biz.teko.td.SHUB_APP.FaqTab.Faq.FQuestion;

/**
 * Created by td13017 on 14.02.2017.
 */

public class FPQuestion implements Parcelable
{

	public int id;
	public String caption;
	public LinkedList<FAnswer> fAnswers;
	public LinkedList<FImage> fImages;


	protected FPQuestion(Parcel in)
	{
		id = in.readInt();
		caption = in.readString();
	}

	public static final Creator<FPQuestion> CREATOR = new Creator<FPQuestion>()
	{
		@Override
		public FPQuestion createFromParcel(Parcel in)
		{
			return new FPQuestion(in);
		}

		@Override
		public FPQuestion[] newArray(int size)
		{
			return new FPQuestion[size];
		}
	};

	public FPQuestion(FQuestion fQuestion)
	{
		this.id = fQuestion.id;
		this.caption = fQuestion.caption;
		this.fAnswers = fQuestion.fAnswers;
		this.fImages = fQuestion.fImages;
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(id);
		dest.writeString(caption);
	}
}
