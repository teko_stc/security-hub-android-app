package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;

/**
 * Created by td13017 on 15.07.2016.
 */
public class Zone extends  D3Element
{
	public int section_id;
	public int device_id;
	public int physic = 0;
	public int detector = 0;
	public String name;
	public int armed;
	public int delay;
	public int uid_type = 0;
	public boolean checked = false;
	public ZType zType;
	public int site;

	public Zone(){}

	public Zone(int id, String name, int physic, int detector){
		this.id = id;
		this.name = name;
		this.physic = physic;
		this.detector = detector;
	}

	public Zone(int id, int section_id, int device_id){
		this.id = id;
		this.section_id = section_id;
		this.device_id = device_id;
	}

	public Zone(int id, String name, int section_id, int device_id, int physic, int detector, int delay, int uid_type)
	{
		this.id = id;
		this.name = name;
		this.section_id = section_id;
		this.device_id = device_id;
		this.physic = physic;
		this.detector = detector;
		this.delay = delay;
		this.uid_type = uid_type;
	}

	public Zone(Cursor cursor)
	{
		if(null!=cursor){
			this.id = cursor.getInt(1);
			this.name = cursor.getString(2);
			this.section_id = cursor.getInt(3);
			this.device_id = cursor.getInt(4);
			this.physic = cursor.getInt(7);
			this.detector =cursor.getInt(8);
			this.delay =cursor.getInt(10);
			this.uid_type =cursor.getInt(11);
			/*5 - actual, 6 - camera*/
		}
	}

	public Zone(Cursor cursor, int site)
	{
		if(null!=cursor){
			this.id = cursor.getInt(1);
			this.name = cursor.getString(2);
			this.section_id = cursor.getInt(3);
			this.device_id = cursor.getInt(4);
			this.physic = cursor.getInt(7);
			this.detector =cursor.getInt(8);
			this.delay =cursor.getInt(10);
			this.uid_type =cursor.getInt(11);
			/*5 - actual, 6 - camera*/
			this.site = site;
		}
	}

	public int getModelId(){
		if(0!=this.uid_type){
			int type = this.uid_type>>24;
			switch (type){
				case 1:
					return this.uid_type&0xff;
				case 2:
					return this.uid_type&0xffff;
				case 4:
					return Const.SENSORTYPE_WIRED_INPUT;
				case 5:
					return Const.SENSORTYPE_WIRED_OUTPUT;
				default:
					break;
			}
		}
		return 0;
	}

	public int getDwiTypeId(){
		if(0!=this.uid_type){
			return this.uid_type&0xff;
		}
		return 0;
	}

	public int getWIOId(){
		if(0!=this.uid_type){
			int id = this.uid_type >> 8;
			return ((id)&0xff);
		}
		return 0;
	}

	public AType getAType(){
		if(this.detector > 100){
			int alarm = getAlarm();
			if(null!=D3Service.d3XProtoConstEvent)
			{
				AType aType = D3Service.d3XProtoConstEvent.getAlarmType(alarm);
				if (null != aType)
				{
					return aType;
				}
			}
		}
		return null;
	}

	public int getAlarm()
	{
		if(this.detector > 100)
		{
			String s = Integer.toString(this.detector, 16);
			return Integer.valueOf(s.substring(0, s.length() - 2), 16);
		}
		return -1;
	}

	public int getDetector(){
		if(this.detector > 100){
			String s = Integer.toString(this.detector, 16);
			return  Integer.valueOf(s.substring(2), 16);
		}
		return this.detector;
	}

	public DType getDType()
	{
		int detector = getDetector();
		if(null!=D3Service.d3XProtoConstEvent)
		{
			DType dType = D3Service.d3XProtoConstEvent.getDetectorType(detector);
			if (null != dType)
			{
				return dType;
			}
		}
		return null;
	}

	public boolean isManualControllable()
	{
		if(this.detector == Const.DETECTOR_MANUAL_RELAY)
			return true;
		return false;
	}

	public ZType getModel()
	{
		int model = getModelId();
		if(0!=model)
			if(null!=D3Service.d3XProtoConstEvent){
				return D3Service.d3XProtoConstEvent.getZTypeById(model);
			}
		return  null;
	}

	public DWOType getDWOType()
	{
		if(null!=D3Service.d3XProtoConstEvent){
			return D3Service.d3XProtoConstEvent.getDWOType(detector);
		}
		return null;
	}

	public DWIType getDWIType()
	{
		int dwiId = getDwiTypeId();
		if(0!=dwiId)
			if(null!=D3Service.d3XProtoConstEvent){
				return D3Service.d3XProtoConstEvent.getDWIType(dwiId);
			}
		return null;
	}
}
