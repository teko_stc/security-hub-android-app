package biz.teko.td.SHUB_APP.D3.D3XProtoEvent;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

/**
 * Created by td13017 on 05.08.2016.
 */
public class EDetector
{
	@Attribute(name = "id")
	public int id;

	@Attribute(name = "caption")
	public String caption;


	@ElementList(name = "reasons", entry = "reason", required = false)
	public LinkedList<EReason> reasons;

	@ElementList(name = "statements", entry = "statement", required = false)
	public LinkedList<EStatement> statements;

	public EStatement getStatementById(int id){
		LinkedList<EStatement> statements = this.statements;
		if(null!=statements)
		{
			for (EStatement statement : statements)
			{
				if (statement.id == id)
				{
					return statement;
				}
			}
		}
		return null;
	}

	public EReason getReasonById(int id){
		LinkedList<EReason> reasons = this.reasons;
		for(EReason reason:reasons){
			if(reason.id == id ){
				return reason;
			}
		}
		return null;
	}

}
