package biz.teko.td.SHUB_APP.Cameras.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraRecordViewActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 14.03.2018.
 */

public class NRecordsAdapter extends ResourceCursorAdapter
{
	private final Context context;
	private final DBHelper dbHelper;
	private String url;


	public NRecordsAdapter(Context context, int layout, Cursor c, int flags)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
	}

	public void update(Cursor cursor){
		this.changeCursor(cursor);
	}

	@Override
	public void bindView(View view, final Context context, Cursor cursor)
	{
		TextView caption = (TextView) view.findViewById(R.id.recordCaption);
		TextView time = (TextView) view.findViewById(R.id.recordTime);
		TextView date = (TextView) view.findViewById(R.id.recordDate);

		TextView site = (TextView) view.findViewById(R.id.recordSite);
		TextView section = (TextView) view.findViewById(R.id.recordSection);
		TextView zone = (TextView) view.findViewById(R.id.recordZone);

		ImageView image = (ImageView) view.findViewById(R.id.recordImage);

		final Event event = dbHelper.getEvent(cursor);
		String desc = event.explainEventRegDescription(context);
		caption.setText(desc);
		time.setText(Func.getServerTimeText(context, event.time));
		date.setText(Func.getServerDateText(context, event.time));

		site.setText(dbHelper.getSiteNameByDeviceSection(event.device, event.section));
		section.setText(dbHelper.getSectionNameByIdWithOptions(event.device, event.section));
		zone.setText(dbHelper.getZoneNameById(event.device, event.section, event.zone));

		if(event.jdata.contains("url")){
			image.setImageResource(R.drawable.ic_camera_brand_color);
			view.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent(context, NCameraRecordViewActivity.class);
					intent.putExtra("event", event.id);
					((Activity)context).startActivity(intent);
					((Activity)context).overridePendingTransition(R.animator.enter, R.animator.exit);

				}
			});
		}else if(event.jdata.contains("notification")){
			image.setImageResource(R.drawable.ic_videocam_off);
			view.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Func.nShowMessage(context, context.getString(R.string.IV_CLIP_FAILED));
				}
			});
		}

	}
}
