package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class NBottomInfoLayout extends LinearLayout
{
	private int layout;
	private TextView detectorText;
	private TextView modelText;
	private TextView zoneNameTitleText;
	private TextView zoneNameSubTitleText;
	private TextView zoneIdSubTitleText;
	private TextView zoneIdTitleText;
	private TextView zoneIOTitleText;
	private TextView zoneIOSubTitleText;
	private TextView sectionNameTitleText;
	private TextView sectionNameSubTitleText;
	private TextView deviceNameTitleText;
	private TextView deviceNameSubTitleText;
	private TextView siteNameTitleText;
	private TextView siteNameSubTitleText;
	private TextView descTitleText;
	private TextView descSubTitleText;
	private ImageView modelImageView;
	private TextView typeText;
	private TextView alarmText;

	private int image = 0;

	private String type;
	private String model;
	private String detector;
	private String alarm;
	private String zoneName;
	private String zoneNameSubTitle;
	private String zoneId;
	private String zoneIO;
	private String sectionName;
	private String sectionNameSubTitle;
	private String deviceName;
	private String deviceNameSubTitle;
	private String siteName;
	private String siteNameSubTitle;
	private String desc;
	private TextView snTitleText;
	private TextView snSubTitleText;
	private String sn;
	private String snSubTitle;


	public NBottomInfoLayout(Context context)
	{
		super(context);
	}

	public NBottomInfoLayout(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		parseAttributes(attrs);
		setupView();
//		bindView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NBottonInfoLayout, 0, 0);
		try{
			layout = a.getResourceId(R.styleable.NBottonInfoLayout_NBottomInfoLayout, 0);
		}finally
		{
			a.recycle();
		}
	}

	private void setupView()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
		layoutInflater.inflate(layout, this, true);

		modelImageView = (ImageView) findViewById(R.id.nImageModel);
		typeText = (TextView) findViewById(R.id.nTextType);
		modelText = (TextView) findViewById(R.id.nTextModel);

		alarmText = (TextView) findViewById(R.id.nTextZoneAlarm);
		detectorText = (TextView) findViewById(R.id.nTextZoneDetector);

		zoneNameTitleText = (TextView) findViewById(R.id.nTextZoneName);
		zoneNameSubTitleText = (TextView) findViewById(R.id.nTextZone);

		zoneIOSubTitleText = (TextView) findViewById(R.id.nTextIOSubTitle);
		zoneIOTitleText = (TextView) findViewById(R.id.nTextIOId);

		zoneIdSubTitleText = (TextView) findViewById(R.id.nTextZoneNum);
		zoneIdTitleText = (TextView) findViewById(R.id.nTextZoneId);

		sectionNameTitleText = (TextView) findViewById(R.id.nTextSectionName);
		sectionNameSubTitleText = (TextView) findViewById(R.id.nTextSection);

		deviceNameTitleText = (TextView) findViewById(R.id.nTextDeviceName);
		deviceNameSubTitleText = (TextView) findViewById(R.id.nTextDevice);

		siteNameTitleText = (TextView) findViewById(R.id.nTextSiteName);
		siteNameSubTitleText = (TextView) findViewById(R.id.nTextSite);

		descTitleText = (TextView) findViewById(R.id.nTextDescContent);
		descSubTitleText = (TextView) findViewById(R.id.nTextDesc);

		snTitleText = (TextView) findViewById(R.id.nTextSNTitle);
		snSubTitleText = (TextView) findViewById(R.id.nTextSNSubTitle);
	}

	private void bindView()
	{
		setTypeText();
		setModelText();
		setModelImageView();

		setDetectorText();
		setAlarmText();
		setZoneNameText();
		setZoneIdText();
		setSectionNameText();
		setDeviceNameText();
		setSiteNameText();
		setDescText();
	}

	private void setDescText()
	{
		if(null!=descTitleText){
			if(null!=desc){
				descTitleText.setText(desc);
				descTitleText.setVisibility(VISIBLE);
				if(null!=descSubTitleText){
					descSubTitleText.setText(R.string.N_BOTTOM_INFO_DESC);
					descSubTitleText.setVisibility(VISIBLE);
				}
			}
		}
	}

	private void setSiteNameText()
	{
		if(null!=siteNameTitleText){
			if(null!=siteName){
				siteNameTitleText.setText(siteName);
				siteNameTitleText.setVisibility(VISIBLE);
				if(null!=siteNameSubTitleText){
					siteNameSubTitleText.setText(R.string.N_BOTTOM_INFO_SITE);
					siteNameSubTitleText.setVisibility(VISIBLE);
				}
			}
		}
	}

	private void setDeviceNameText()
	{
		if(null!=deviceNameTitleText){
			if(null!=deviceName){
				deviceNameTitleText.setText(deviceName);
				deviceNameTitleText.setVisibility(VISIBLE);
				if(null!=deviceNameSubTitleText){
					deviceNameSubTitleText.setText(R.string.N_BOTTOM_INFO_CONTROLLER);
					deviceNameSubTitleText.setVisibility(VISIBLE);
				}
			}
		}
	}

	private void setSectionNameText()
	{
		if(null!=sectionNameTitleText){
			if(null!=sectionName){
				sectionNameTitleText.setText(sectionName);
				sectionNameTitleText.setVisibility(VISIBLE);
				if(null!=sectionNameSubTitleText){
					sectionNameSubTitleText.setText(R.string.N_BOTTOM_INFO_SECTION);
					sectionNameSubTitleText.setVisibility(VISIBLE);
				}
			}
		}
	}


	private void setZoneIdText()
	{
		if(null!=zoneIdTitleText){
			if(null!=zoneId){
				zoneIdTitleText.setText(zoneId);
				zoneIdTitleText.setVisibility(VISIBLE);
				if(null!=zoneIdSubTitleText){
					zoneIdSubTitleText.setText(R.string.N_BOTTOM_INFO_ZONE_NUMBER);
					zoneIdSubTitleText.setVisibility(VISIBLE);
				}
			}
		}
	}

	private void setZoneIOText()
	{
		if(null!=zoneIOTitleText){
			if(null!=zoneIO){
				zoneIOTitleText.setText(zoneIO);
				zoneIOTitleText.setVisibility(VISIBLE);
				if(null!=zoneIdSubTitleText){
					zoneIOSubTitleText.setText(R.string.N_BOTTOM_INFO_INPUT_NUMBER);
					zoneIOSubTitleText.setVisibility(VISIBLE);
				}
			}
		}
	}

	private void setZoneNameText()
	{
		if(null!=zoneNameTitleText){
			if(null!=zoneName){
				zoneNameTitleText.setText(zoneName);
				zoneNameTitleText.setVisibility(VISIBLE);
				if(null!=zoneNameSubTitleText){
					zoneNameSubTitleText.setText(R.string.N_BOTTOM_INFO_ZONE_NAME);
					zoneNameSubTitleText.setVisibility(VISIBLE);
				}
			}
		}
	}

	private void setSNTitleText()
	{
		if(null!=snTitleText){
			if(null!=sn){
				snTitleText.setText(sn);
				snTitleText.setVisibility(VISIBLE);
				if(null!=snSubTitleText){
					snSubTitleText.setVisibility(VISIBLE);
					snSubTitleText.setText(R.string.N_BOTTOM_INFO_SERIAL_NUMBER);
				}
			}
		}
	}


	private void setModelImageView()
	{
		if(null!= modelImageView){
			if(0!=image){
				modelImageView.setImageDrawable(getContext().getResources().getDrawable(image));
				modelImageView.setVisibility(VISIBLE);
			}
		}
	}

	private void setTypeText()
	{
		if(null!=typeText){
			if(null!=type){
				typeText.setVisibility(VISIBLE);
				typeText.setText(type);
			}else{
				typeText.setVisibility(GONE);
			}
		}
	}

	private void setModelText()
	{
		if(null!=modelText){
			if(null!=model){
				modelText.setVisibility(VISIBLE);
				modelText.setText(model);
			}else{
				modelText.setVisibility(GONE);
			}
		}
	}

	private void setDetectorText()
	{
		if(null!=detectorText){
			if(null!=detector){
				detectorText.setText(detector);
				detectorText.setVisibility(VISIBLE);
			}else{
				detectorText.setVisibility(GONE);
			}
		}
	}

	private void setAlarmText()
	{
		if(null!=alarmText){
			if(null!=alarm){
				alarmText.setText(alarm);
				alarmText.setVisibility(VISIBLE);
			}else{
				alarmText.setVisibility(GONE);
			}
		}
	}

	public void setSn(String sn){
		this.sn = sn;
		setSNTitleText();
	}

	public void setDetector(String detector){
		this.detector= detector;
		setDetectorText();
	}

	public void setType(String type){
		this.type = type;
		setTypeText();
	}

	public  void setAlarm(String alarm){
		this.alarm = alarm;
		setAlarmText();
	}

	public void setModel(String model){
		this.model = model;
		setModelText();
	}

	public void setModelImage(int image){
		this.image = image;
		setModelImageView();
	}

	public void setZoneName(String zoneName){
		this.zoneName = zoneName;
		setZoneNameText();
	}

	public void setZoneId(String zoneId){
		this.zoneId = zoneId;
		setZoneIdText();
	}

	public  void setZoneIO(String zoneIO){
		this.zoneIO = zoneIO;
		setZoneIOText();
	}

	public void setSectionName(String sectionName){
		this.sectionName = sectionName;
		setSectionNameText();
	}

	public void setDeviceName(String deviceName){
		this.deviceName = deviceName;
		setDeviceNameText();
	}

	public void setSiteName(String siteName){
		this.siteName = siteName;
		setSiteNameText();
	}

	public void setDesc(String desc){
		this.desc = desc;
		setDescText();
	}

}
