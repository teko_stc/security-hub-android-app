package biz.teko.td.SHUB_APP.Cameras.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 27.02.2018.
 */

public class SitesListAdapter extends ArrayAdapter<Site>
{
	private final LayoutInflater layoutInflater;
	private final Site[] sites;
	private final int cur;
	private final Context context;

	public SitesListAdapter(Context context, int resource, Site[] sites, int cur)
	{
		super(context, resource);
		this.context = context;
		this.sites = sites;
		this.cur = cur;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}



	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		Site site = sites[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(site.name);
		if(cur == site.id){
			textView.setTextColor(context.getResources().getColor(R.color.brandColorAccentColor));
		}else{
			textView.setTextColor(context.getResources().getColor(R.color.brandColorTextTitle));
		}
		return view;
	}

	public int getCount() {
		return sites.length;
	}

	public Site getItem(int position){
		return sites[position];
	}

	public long getItemId(int position){
		return position;
	}
}
