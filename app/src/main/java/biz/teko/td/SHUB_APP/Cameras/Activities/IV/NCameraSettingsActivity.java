package biz.teko.td.SHUB_APP.Cameras.Activities.IV;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.Cameras.Adapters.QualitiesListAdapter;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.Cameras.Quality;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

/**
 * Created by td13017 on 28.02.2018.
 */

public class NCameraSettingsActivity extends BaseActivity
{

	private Context context;
	private DBHelper dbHelper;
	private String id="";
	private Camera camera;
	private int from = 0;
	private Toolbar toolbar;

	private BroadcastReceiver broadcastCameraSetSuccess = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getIntExtra("result", -1) == Const.IV_CAMERA_GET_SUCCESS)
			{
//				Func.pushToast(context, getString(R.string.TOAST_SUCC_CHANGED), (NCameraSettingsActivity) context);
				bind();
			}
		}
	};
	private LinearLayout back;
	private NMenuListElement nameView;
	private NMenuListElement bindView;
	private NMenuListElement qView;
	private NMenuListElement soundView;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera_settings);

		this.context = this;
		dbHelper = DBHelper.getInstance(context);
		id = getIntent().getStringExtra("cameraId");
		camera = dbHelper.getIVCameraById(id);

		setup();
	}

	private void setup()
	{
		back = (LinearLayout) findViewById(R.id.nButtonBack);
		nameView = (NMenuListElement) findViewById(R.id.nMenuName);
		bindView = (NMenuListElement) findViewById(R.id.nMenuBind);
		qView = (NMenuListElement) findViewById(R.id.nMenuQuality);
		soundView = (NMenuListElement) findViewById(R.id.nMenuSound);
	}

	private void bind()
	{
		camera = dbHelper.getIVCameraById(id);
		if(null!=back)back.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});
		if(null!=nameView) {
			nameView.setTitle(camera.name);
			nameView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					Intent intent = new Intent(context, NRenameActivity.class);
					intent.putExtra("type", Const.CAMERA_IV);
					intent.putExtra("name", camera.name);
					intent.putExtra("transition",true);
					intent.putExtra("camera_iv", camera.id);
					startActivity(intent, getTransitionOptions(nameView).toBundle());
				}
			});
		}
		if(null!=soundView){
			soundView.setChecked(camera.audio.equals("both"));
			soundView.setOnCheckedChangeListener(new NMenuListElement.OnCheckedChangeListener()
			{
				@Override
				public void onCheckedChanged()
				{
					dbHelper.setIVCameraStreamSound(camera.id, !camera.audio.equals("both"));
					bind();
				}
			});
		}
		if(null!=bindView)bindView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
		{
			@Override
			public void onRootClick()
			{
				Intent intent = new Intent(getBaseContext(), NCameraBindListActivity.class);
				intent.putExtra("cameraId", camera.id);
				intent.putExtra("from", Const.OPEN_IV_FROM_SETTINGS);
				startActivity(intent, ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle());
			}
		});
		if(null!=qView){
			String[] qualities = getResources().getStringArray(R.array.stream_qualities_status_list);
			qView.setTitle(qualities[camera.quality + 1]);
			qView.setOnRootClickListener(()->{
				final NBottomSheetDialog bottomSheetDialog =new NBottomSheetDialog(context);

				View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
				TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
				title.setText(R.string.CAMERAA_SET_QUALITY_TITLE);

				ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
				listView.setDivider(null);

				String[] qualities_status = getResources().getStringArray(R.array.stream_qualities_status_list);
				String[] qualities_value = getResources().getStringArray(R.array.stream_qualities_value_list);

				final Quality[] qs = new Quality[qualities_status.length];
				for(int i = 0; i < qualities_status.length; i++ ){
					qs[i] = new Quality(qualities_status[i], qualities_value[i]);
				}

				QualitiesListAdapter qualitiesListAdapter = new QualitiesListAdapter(context, R.layout.n_card_for_list, qs, camera.quality);
				listView.setAdapter(qualitiesListAdapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id)
					{
						Quality quality = (Quality) parent.getItemAtPosition(position);
						camera.quality = Integer.valueOf(quality.value);
						dbHelper.setIVCameraQuality(camera.id, quality.value);
						bottomSheetDialog.dismiss();
						bind();
					}
				});

				bottomSheetDialog.setContentView(bottomView);
				bottomSheetDialog.show();

			});
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(broadcastCameraSetSuccess, new IntentFilter(WebHelper.BROADCAST_CAMERA_GET));
		bind();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(broadcastCameraSetSuccess);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((NCameraSettingsActivity)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}
}
