package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Events.Adapters.NAffectsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NTopLayout extends LinearLayout
{

	private final Context context;
	private OnActionListener onActionListener;
	private int layout;
	private LinkedList<Event> affects;
	private ListView affectsList;
	private Script script;

	public void setOnActionListener(OnActionListener onActionListener){
		this.onActionListener = onActionListener;
	}

	public interface OnActionListener{
		void onFavoriteChanged();
		void backPressed();
		void onSettingsPressed();
		void onControlPressed(int value);
		void onLinkPressed(int id);
		void onStateClick(int type);
	}

	private Type type;
	private LinkedList<NActionButton> buttons;
	private ControlType controlType;

	private NTopInfoLayout topInfoFrame;
	private NTopControlLayout controlFrame;

	public enum Type
	{zone_wireless,
		zone_wired ,
		zone_temp,
		relay_socket,
		relay_wireless,
		relay_wired,
		relay_wired_auto,
		relay_wired_auto_binded,
		relay_wired_siren,
		relay_szo,
		section,
		device_common,
		device_sh,
		device_sh_2,
		device_mb};

	public enum ControlType
	{none, arm, monitor, control, script, reset_fire, reset, tech};

	public static  final int[] STATE_ALARM = {R.attr.state_alarm};
	public static  final int[] STATE_SABOTAGE = {R.attr.state_sabotage};
	public static  final int[] STATE_MALF = {R.attr.state_malf};
	public static  final int[] STATE_ATTENTION = {R.attr.state_attention};

	public static  final int[] STATE_NORMAL = {R.attr.state_normal};
	public static  final int[] STATE_OFFLINE = {R.attr.state_offline};
	public static  final int[] STATE_CLOUD_OFFLINE = {R.attr.state_cloud_offline};
	public static  final int[] STATE_RK_OFFLINE = {R.attr.state_rk_offline};
	public static  final int[] STATE_BLACK = {R.attr.state_black};
	public static  final int[] STATE_INVERT = {R.attr.state_invert};
	public static  final int[] STATE_ARM = {R.attr.state_arm};
	public static  final int[] STATE_DISARM = {R.attr.state_disarm};
	public static  final int[] STATE_PARTLY_ARM = {R.attr.state_partly_arm};
	public static  final int[] STATE_ON = {R.attr.state_on};
	public static  final int[] STATE_OFF = {R.attr.state_off};
	public static  final int[] STATE_NO_SECTION_CONTROL = {R.attr.state_no_section_control};

	public static  final int[] STATE_NO_SOCKET = {R.attr.state_no_socket};
	public static  final int[] STATE_NO_LAN = {R.attr.state_no_lan};
	public static  final int[] STATE_AUTO = {R.attr.state_auto};
	public static  final int[] STATE_SCRIPTED = {R.attr.state_scripted};

	public static  final int[] STATE_DELAY = {R.attr.state_delay};
	public static  final int[] STATE_DELAY_ARM = {R.attr.state_delay_arm};

	public static  final int[] STATE_UPDATE = {R.attr.state_update};

	public static  final int[] STATE_BATTERY_HIGH = {R.attr.state_battery_high};
	public static  final int[] STATE_BATTERY_MID = {R.attr.state_battery_mid};
	public static  final int[] STATE_BATTERY_LOW = {R.attr.state_battery_low};
	private static final int[] STATE_BATTERY_EMPTY ={R.attr.state_battery_empty}; ;
	public static  final int[] STATE_BATTERY_NO = {R.attr.state_battery_no};
	public static  final int[] STATE_BATTERY_ERROR = {R.attr.state_battery_error};

	public static  final int[] STATE_LEVEL_TOP = {R.attr.state_level_top};
	public static  final int[] STATE_LEVEL_HIGH = {R.attr.state_level_high};
	public static  final int[] STATE_LEVEL_MID = {R.attr.state_level_mid};
	public static  final int[] STATE_LEVEL_LOW = {R.attr.state_level_low};

	public static  final int[] STATE_SIM_EXIST = {R.attr.state_sim_exist};
	public static  final int[] STATE_NO_SIGNAL_SIM = {R.attr.state_sim_no_signal};

	public static  final int[] STATE_SIM_1_EXIST = {R.attr.state_sim_1_exist};
	public static  final int[] STATE_NO_SIGNAL_SIM_1 = {R.attr.state_sim_1_no_signal};

	public static  final int[] STATE_SIM_2_EXIST = {R.attr.state_sim_2_exist};
	public static  final int[] STATE_NO_SIGNAL_SIM_2 = {R.attr.state_sim_2_no_signal};

	private boolean alarm;
	private boolean sabotage;
	private boolean malf;
	private boolean attention;

	private boolean offline;
	private boolean cloud_offline;
	private boolean rk_offline;
	private long offline_time;
	private long rk_offline_time;
	private int offline_type;
	private int rk_offline_type;

	private boolean black;
	private boolean normal;
	private boolean inverted;
	private boolean armed;
	private boolean disarmed;
	private boolean partly_armed;
	private boolean turned_on;
	private boolean turned_off;
	private boolean no_control;
	private boolean no_socket;
	private boolean no_lan;
	private boolean auto;
	private boolean scripted;
	private boolean delayed;
	private boolean delay_arm;
	private boolean update_ready;
	private boolean battery_high;
	private boolean battery_mid;
	private boolean battery_low;
	private boolean battery_empty;
	private boolean battery_no;
	private boolean battery_error;
	private boolean level_top;
	private boolean level_high;
	private boolean level_mid;
	private boolean level_low;
	private boolean sim_exist;
	private boolean sim_no_signal;
	private boolean sim1_exist;
	private boolean sim1_no_signal;
	private boolean sim2_exist;
	private boolean sim2_no_signal;

	public NTopLayout(Context context)
	{
		super(context);
		this.context = context;
		setupView();
	}

	public NTopLayout(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setupView();
//		bindView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NTopLayout, 0, 0);
		try{
			layout = a.getResourceId(R.styleable.NTopLayout_NToplayout, 0);
			controlType = NTopLayout.ControlType.values()[a.getInt(R.styleable.NTopLayout_NTopContolType, 0)];
		}finally
		{
			a.recycle();
		}
	}

	private void setupView()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		layoutInflater.inflate(layout, this, true);

		topInfoFrame = (NTopInfoLayout) findViewById(R.id.nTopInfoFrame);
		controlFrame = (NTopControlLayout) findViewById(R.id.nControlFrame);
		affectsList = (ListView) findViewById(R.id.nListAffects);
	}

	public void bindView(){
		setTopInfoFrame();
		setControlFrame();
		setAffectsFrame();
	}

	private void setAffectsFrame()
	{
		if(null!=affects && null!=affectsList){
			NAffectsListAdapter adapter = new NAffectsListAdapter(context, R.layout.n_affects_list_element, affects);
			affectsList.setAdapter(adapter);
			Func.setDynamicHeight(affectsList);
		}
	}

	private void setTopInfoFrame()
	{
		if (null != topInfoFrame)
		{
			topInfoFrame.setType(type);
			topInfoFrame.setOnTopNavigationListener(new NTopInfoLayout.OnTopNavigationListener()
			{
				@Override
				public void favoriteChanged()
				{
					if(null!=onActionListener) onActionListener.onFavoriteChanged();
				}

				@Override
				public void backPressed()
				{
					if(null!=onActionListener) onActionListener.backPressed();
				}

				@Override
				public void setOptions()
				{
					if(null!=onActionListener) onActionListener.onSettingsPressed();
				}
			});
			topInfoFrame.setOnStateClickListener(type -> {
				switch (type){
					case Const.STATE_TYPE_CONNECTION:
						Func.nShowMessage(context,
								offline ?
										(offline_type == Const.CONTROLLER ? getContext().getString(R.string.N_CONTROLLER_OFFLINE) :
												(offline_type == Const.FULL ?
														getContext().getString(R.string.N_SYSTEM_PART_OFFLINE) : getContext().getString(R.string.N_SYSTEM_PART_OFFLINE_PARTLY)))
										:  getContext().getString(R.string.N_SYSTEM_PART_ONLINE),
								offline ? (Func.getServerTimeDateTextShort(context, offline_time) +
										 (rk_offline && cloud_offline ? "\n\nСвязь устройства с контроллером была потеряна " + Func.getServerTimeDateTextShort(context, rk_offline_time) : "" ))
										: null, R.layout.n_dialog_element_connection);
						break;
					case Const.STATE_TYPE_DELAY:
						if(null!=onActionListener) {
							if(delay_arm) onActionListener.onStateClick(Const.STATE_TYPE_DELAY_ARM);
							else if (delayed) onActionListener.onStateClick( Const.STATE_TYPE_DELAY );
						}
						break;
					case Const.STATE_TYPE_UPDATE:
						if(update_ready && null!=onActionListener) onActionListener.onStateClick(Const.STATE_TYPE_UPDATE);
						break;
					default:
						if(null!=onActionListener) onActionListener.onStateClick(type);
						break;
				}
			});
		}
	}

	private void setControlFrame()
	{
		if(null!=controlFrame){
			controlFrame.setType(controlType);
			controlFrame.setScript(script);
			controlFrame.setOnActionClickListener(new NTopControlLayout.OnActionClickListener()
			{
				@Override
				public void onActionClick(int value)
				{
					if(null!=onActionListener) onActionListener.onControlPressed(value);
				}

				@Override
				public void onLinkClick(int id)
				{
					if(null!=onActionListener) onActionListener.onLinkPressed(id);
				}
			});
		}
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 35);
		if(alarm) {
			mergeDrawableStates(drawableState, STATE_ALARM);
		}
		if(sabotage) {
			mergeDrawableStates(drawableState, STATE_SABOTAGE);
		}
		if(malf) {
			mergeDrawableStates(drawableState, STATE_MALF);
		}
		if(attention) {
			mergeDrawableStates(drawableState, STATE_ATTENTION);
		}

		if (offline) {
			mergeDrawableStates(drawableState, STATE_OFFLINE);
		}
		if (cloud_offline) {
			mergeDrawableStates(drawableState, STATE_CLOUD_OFFLINE);
		}
		if (rk_offline) {
			mergeDrawableStates(drawableState, STATE_RK_OFFLINE);
		}
		if (black) {
			mergeDrawableStates(drawableState, STATE_BLACK);
		}
		if (normal) {
			mergeDrawableStates(drawableState, STATE_NORMAL);
		}

		if (inverted) {
			mergeDrawableStates(drawableState, STATE_INVERT);
		}
		if(armed){
			mergeDrawableStates(drawableState, STATE_ARM);
		}
		if(disarmed){
			mergeDrawableStates(drawableState, STATE_DISARM);
		}
		if(partly_armed){
			mergeDrawableStates(drawableState, STATE_PARTLY_ARM);
		}
		if(turned_on){
			mergeDrawableStates(drawableState, STATE_ON);
		}
		if(turned_off){
			mergeDrawableStates(drawableState, STATE_OFF);
		}
		if(no_control){
			mergeDrawableStates(drawableState, STATE_NO_SECTION_CONTROL);
		}
		if(no_socket) mergeDrawableStates(drawableState, STATE_NO_SOCKET);
		if(no_lan) mergeDrawableStates(drawableState, STATE_NO_LAN);
		if(auto) mergeDrawableStates(drawableState, STATE_AUTO);
		if(scripted) mergeDrawableStates(drawableState, STATE_SCRIPTED);
		if(delayed) mergeDrawableStates(drawableState, STATE_DELAY);
		if(delay_arm) mergeDrawableStates(drawableState, STATE_DELAY_ARM);
		if(update_ready) mergeDrawableStates(drawableState, STATE_UPDATE);
		if(battery_high) mergeDrawableStates(drawableState, STATE_BATTERY_HIGH);
		if(battery_mid) mergeDrawableStates(drawableState, STATE_BATTERY_MID);
		if(battery_low) mergeDrawableStates(drawableState, STATE_BATTERY_LOW);
		if(battery_empty) mergeDrawableStates(drawableState, STATE_BATTERY_EMPTY);
		if(battery_no) mergeDrawableStates(drawableState, STATE_BATTERY_NO);
		if(battery_error) mergeDrawableStates(drawableState, STATE_BATTERY_ERROR);
		if(level_top) mergeDrawableStates(drawableState, STATE_LEVEL_TOP);
		if(level_high) mergeDrawableStates(drawableState, STATE_LEVEL_HIGH);
		if(level_mid) mergeDrawableStates(drawableState, STATE_LEVEL_MID);
		if(level_low) mergeDrawableStates(drawableState, STATE_LEVEL_LOW);
		if(sim_exist) mergeDrawableStates(drawableState, STATE_SIM_EXIST);
		if(sim_no_signal) mergeDrawableStates(drawableState, STATE_NO_SIGNAL_SIM);
		if(sim1_exist) mergeDrawableStates(drawableState, STATE_SIM_1_EXIST);
		if(sim1_no_signal) mergeDrawableStates(drawableState, STATE_NO_SIGNAL_SIM_1);
		if(sim2_exist) mergeDrawableStates(drawableState, STATE_SIM_2_EXIST);
		if(sim2_no_signal) mergeDrawableStates(drawableState, STATE_NO_SIGNAL_SIM_2);
		return drawableState;
	}

	//Background states
	public void setAlarm(boolean alarm)
	{
		this.alarm = alarm;
//		if(null!=topInfoFrame) topInfoFrame.setAlarm(alarm);
		setInvert(alarm);
	}

	public void setSabotage(boolean sabotage)
	{
		this.sabotage = sabotage;
//		setBlack(sabotage);
//		setNormal(!sabotage);
	}

	public void setMalf(boolean malf)
	{
		this.malf = malf;
//		setBlack(malf);
//		setNormal(!malf);
	}

	public void setAttention(boolean attention)
	{
		this.attention = attention;
//		setBlack(attention);
//		setNormal(!attention);

	}

	//SET STATES
	public void setNo_socket(boolean no_socket)
	{
		this.no_socket = no_socket;
		refreshDrawableState();
	}

	public void setNo_lan(boolean no_lan)
	{
		this.no_lan = no_lan;
		refreshDrawableState();
	}

	//only for relay
	public void setAuto(boolean auto)
	{
		this.auto = auto;
		refreshDrawableState();
	}

	//only for relay
	public void setScripted(boolean scripted)
	{
		this.scripted = scripted;
		refreshDrawableState();
	}

	public void setDelayed(boolean delayed){
		this.delayed = delayed;
		refreshDrawableState();
	}

	public void setDelayArm(boolean delay_arm){
		this.delay_arm = delay_arm;
		refreshDrawableState();
	}

	public void setUpdate_ready(boolean update_ready){
		this.update_ready = update_ready;
		refreshDrawableState();
	}
	public void setBattery_high(boolean battery_high)
	{
		this.battery_high = battery_high;
		refreshDrawableState();
	}

	public void setBattery_mid(boolean battery_mid)
	{
		this.battery_mid = battery_mid;
		refreshDrawableState();
	}

	public void setBattery_low(boolean battery_low)
	{
		this.battery_low = battery_low;
		refreshDrawableState();
	}

	public void setBattery_empty(boolean battery_empty)
	{
		this.battery_empty = battery_empty;
		refreshDrawableState();
	}

	public void setBattery_no(boolean battery_no)
	{
		this.battery_no = battery_no;
		refreshDrawableState();
	}

	public void setBattery_error(boolean battery_error)
	{
		this.battery_error = battery_error;
		refreshDrawableState();
	}

	public void setLevel_top(boolean level_top)
	{
		this.level_top = level_top;
		refreshDrawableState();
	}

	public void setLevel_high(boolean level_high)
	{
		this.level_high = level_high;
		refreshDrawableState();
	}

	public void setLevel_mid(boolean level_mid)
	{
		this.level_mid = level_mid;
		refreshDrawableState();
	}

	public void setLevel_low(boolean level_low)
	{
		this.level_low = level_low;
		refreshDrawableState();
	}

	public void setSim_exist(boolean sim_exist)
	{
		this.sim_exist = sim_exist;
		refreshDrawableState();
	}

	public void setSim_no_signal(boolean sim_no_signal)
	{
		this.sim_no_signal = sim_no_signal;
		refreshDrawableState();
	}

	public void setSim1_exist(boolean sim1_exist)
	{
		this.sim1_exist = sim1_exist;
		refreshDrawableState();
	}

	public void setSim1_no_signal(boolean sim1_no_signal)
	{
		this.sim1_no_signal = sim1_no_signal;
		refreshDrawableState();
	}

	public void setSim2_exist(boolean sim2_exist)
	{
		this.sim2_exist = sim2_exist;
		refreshDrawableState();
	}

	public void setSim2_no_signal(boolean sim2_no_signal)
	{
		this.sim2_no_signal = sim2_no_signal;
		refreshDrawableState();
	}

	public void setControlType(ControlType controlType){
		this.controlType = controlType;
	}

	public void setScript(Script script){
		this.script = script;
	}

	public void setType(Type type)
	{
		this.type = type;
	}

	//only for section type == 6
	public void setNoControl(boolean no_control){
		this.no_control = no_control;
		refreshDrawableState();
	}

	public void setTurnedOn(boolean on){
		this.turned_on=on;
		this.turned_off=!on;
		refreshDrawableState();
	}

//	public void setTurnedOff(boolean off)
//	{
//		this.turned_off= off;
//		refreshDrawableState();
//	}

	public void setArmed(int value){
		switch (value){
			case Const.STATUS_ARMED:
				this.armed = true;
				this.disarmed = false;
				this.partly_armed = false;
				break;
			case Const.STATUS_DISARMED:
				this.armed = false;
				this.disarmed = true;
				this.partly_armed = false;
				break;
			case Const.STATUS_PARTLY_ARMED:
				this.armed = false;
				this.disarmed = false;
				this.partly_armed = true;
				break;
			case Const.STATUS_NO_ARM_CONTROL:
				this.armed = false;
				this.disarmed = false;
				this.partly_armed = false;
				break;
		}
		refreshDrawableState();
	}

//	public void setDisarmed(boolean disarmed)
//	{
//		this.disarmed = disarmed;
//		this.armed = false;
//		this.partly_armed = false;
//		refreshDrawableState();
//	}
//
//	public void setPartlyArmed(boolean armed){
//		this.partly_armed =armed;
//		this.armed = false;
//		this.disarmed = false;
//		refreshDrawableState();
//	}

	public void setOffline(boolean offline, long offline_time, int offline_type)
	{
		this.offline = offline;
		this.offline_time = offline_time;
		this.offline_type = offline_type;
//		if(null!=topInfoFrame) topInfoFrame.setOffline(offline);
		refreshDrawableState();
	}

	public void setCloudOffline(boolean cloud_offline, long offline_time, int offline_type)
	{
		this.cloud_offline = cloud_offline;
		this.offline_time = offline_time;
		this.offline_type = offline_type;
		//		if(null!=topInfoFrame) topInfoFrame.setOffline(offline);
		refreshDrawableState();
	}

	public void setRKOffline(boolean rk_offline, long offline_time, int offline_type)
	{
		this.rk_offline = rk_offline;
		this.rk_offline_time = offline_time;
		this.rk_offline_type = offline_type;
		//		if(null!=topInfoFrame) topInfoFrame.setOffline(offline);
		refreshDrawableState();
	}

	public void setBlack(boolean black)
	{
		this.black = black;
//		if(null!=topInfoFrame) topInfoFrame.setBlack(black);
		refreshDrawableState();
	}

	public void setInvert(boolean invert){
		this.inverted = invert;
//		if(null!=topInfoFrame) topInfoFrame.setInvert(invert);
		refreshDrawableState();
	}

	public void setNormal(boolean normal)
	{
		this.normal = normal;
		refreshDrawableState();
	}

	public void setStates(LinkedList<Event> affects, D3Element d3Element, boolean been_alarmed)
	{
		this.affects = new LinkedList<>();

		boolean alarm = false;
		boolean sabotage= false;
		boolean malf = false;
		boolean offline = false;
		boolean cloud_offline = false;
		boolean rk_offline = false;
		boolean attention = false;
		boolean delay_arm = false;
		boolean update_ready = false;

		long offline_time = 0;
		int offline_type = Const.NONE;

		long rk_offline_time = 0;
		int rk_offline_type = Const.NONE;

		int alarmImage = 0;

		boolean sim_exist = false;
		boolean sim1_exist = false;
		boolean sim2_exist = false;

		boolean sim_no_signal = false;
		boolean sim1_no_signal = false;
		boolean sim2_no_signal = false;

		if(null!=affects){
			for(Event event:affects){
				switch (event.classId){
					case Const.CLASS_ALARM:
						this.affects.add(event);
						alarm = true;
						alarmImage = event.getBigIcon(context);
						break;
					case Const.CLASS_SABOTAGE:
						this.affects.add(event);
						sabotage = true;
						break;
					case Const.CLASS_MALFUNCTION:
						switch (event.reasonId){
							case Const.REASON_MALF_LOST_CONNECTION:
								offline = true;
								if(d3Element instanceof Zone){
									if(event.section == 0
										&& (event.zone == 0 || event.zone == 100)
										&& offline_type!= Const.PART){
											cloud_offline = true;
											offline_type = Const.FULL;
											offline_time = event.time;
										break;
									}else{
										if(((Zone) d3Element).id == event.zone){
											rk_offline = true;
											rk_offline_type = Const.PART;
											rk_offline_time = event.time;
										}
									}
								}else if(d3Element instanceof Device){
									offline_type = Const.CONTROLLER;
									offline_time = event.time;
								}
								break;
							case Const.REASON_MAFL_BATT_NO:
								setBattery_no(true);
							default:
								malf = true;
								this.affects.add(event);
								break;
						}
						break;
					case Const.CLASS_ATTENTION:
						this.affects.add(event);
						attention = true;
						break;
					case Const.CLASS_ARM:
						break;
					case Const.CLASS_CONTROL:
						if(event.reasonId == Const.REASON_TURN_ON) {
							setTurnedOn(true);
						}
						break;
					case Const.CLASS_SUPPLY:
						switch (event.reasonId){
							case Const.REASON_SOCKET_FAULT:
								setNo_socket(true);
								break;
							case Const.REASON_BATTERY_LEVEL:
								analyzeBatteryLevel(event);
								break;
							case Const.REASON_BATTERY_ERROR:
								setBattery_error(true);
								break;
							default:
								break;
						}
						break;
					case Const.CLASS_CONNECTIVITY:
						switch (event.reasonId){
							case Const.REASON_CONNECTIVITY_UNAVAILABLE:
								if(event.detectorId == Const.DETECTOR_MODEM){
									sim_no_signal = true;
//									setSim_no_signal(true);
								}else if(event.detectorId == Const.DETECTOR_GPRS_1)
								{
									sim1_no_signal = true;
//									setSim1_no_signal(true);
								}else if(event.detectorId == Const.DETECTOR_GRPS_2){
									sim2_no_signal = true;
//									setSim2_no_signal(true);
								}else if (event.detectorId == Const.DETECTOR_ETHERNET){
									setNo_lan(true);
								}
								break;
							case Const.REASON_CONNECTIVITY_LEVEL:
								if(event.detectorId == Const.DETECTOR_MODEM){
									switch (event.getBearer()){
										case 1:
											topInfoFrame.setSim1Text(event.getLevel());
											break;
										case 2:
											topInfoFrame.setSim2Text(event.getLevel());
											break;
										default:
											topInfoFrame.setSimText(event.getLevel());
											break;
									}
								}else if(event.detectorId == Const.DETECTOR_GPRS_1)
								{
									topInfoFrame.setSim1Text(event.getLevel());
								}else if(event.detectorId == Const.DETECTOR_GRPS_2){
									topInfoFrame.setSim2Text(event.getLevel());
								}else{
									analyzeConnectivityLevel(event);
								}
								break;
							case Const.REASON_CONNECTIVITY_IMSI:
								if(event.detectorId == Const.DETECTOR_MODEM){
									switch (event.getBearer()){
										case 1:
											sim1_exist = true;
//											setSim1_exist(true);
											break;
										case 2:
											sim2_exist = true;
//											setSim2_exist(true);
											break;
										default:
											sim_exist = true;
//											setSim_exist(true);
											break;
									}
									//								setSim_exist(true);
								}else if(event.detectorId == Const.DETECTOR_GPRS_1)
								{
									sim1_exist = true;
//									setSim1_exist(true);
								}else if(event.detectorId == Const.DETECTOR_GRPS_2){
									sim2_exist = true;
//									setSim2_exist(true);
								}
								break;
							default:
								break;
						}
						break;
					case Const.CLASS_INFORMATION:
						switch (event.reasonId){
							case Const.REASON_INFORMATION_UPDATE_READY:
								update_ready = true;
								break;
							case Const.REASON_INFORMATION_ARM_DELAY:
								delay_arm = true;
								break;
							default:
								break;
						}
						break;
					case Const.CLASS_DEBUG:
						break;
					case Const.CLASS_TELEMETRY:
						switch (event.reasonId){
							case Const.REASON_TEMPERATURE:
								if(d3Element instanceof Zone){
									if (event.zone == d3Element.id){
										topInfoFrame.setTempText(event.getTemperatureString());
									}else{
										topInfoFrame.setTempOutText(event.getTemperatureString());
									}
								}else{
									topInfoFrame.setTempText(event.getTemperatureString());
								}
								break;
							default:
								break;
						}
						break;
					case Const.CLASS_GEOLOCATION:
						break;
					case Const.CLASS_INNER:
						break;
					default:
						break;
				}
			}
		}

		setSim_exist(sim_exist);
		setSim1_exist(sim1_exist);
		setSim2_exist(sim2_exist);
		if(sim_exist) setSim_no_signal(sim_no_signal);
		if(sim1_exist) setSim1_no_signal(sim1_no_signal || sim_no_signal);
		if(sim2_exist) setSim_no_signal(sim2_no_signal || sim_no_signal);

		setUpdate_ready(update_ready);
		setDelayArm(delay_arm);
		setOffline(offline, offline_time, offline_type);
		setCloudOffline(cloud_offline, offline_time, offline_type);
		setRKOffline(rk_offline, rk_offline_time, rk_offline_type);
		setAttention(attention);
		setMalf(malf);
		setSabotage(sabotage);
		setBlack(offline||attention||malf||sabotage);
		if(null!=controlFrame)
			switch (controlType){
				case reset:
					controlFrame.setVisibility((alarm||been_alarmed)? VISIBLE : GONE);
					break;
				case reset_fire:
					controlFrame.setVisibility((alarm||offline||attention||malf||sabotage||been_alarmed)? VISIBLE : GONE);
					break;
				default:
					controlFrame.setVisibility(VISIBLE);
			}
		setNormal(!(offline||attention||malf||sabotage));
		setAlarm(alarm);
		if(0!=alarmImage) setBigImage(alarmImage);
	}

	public void analyzeBatteryLevel(Event event)
	{
		String level = event.getLevel();
		if(null!=level){
			try{
				int levelI  = Integer.parseInt(level.substring(0, level.indexOf(" ")));

				if(levelI < 10){
					setBattery_empty(true);
				}else if(levelI < 50){
					setBattery_low(true);
				}else if(levelI < 90){
					setBattery_mid(true);
				}else{
					setBattery_high(true);
				}
			}catch (Exception exception){
				setBattery_mid(true);
			}
		}
		topInfoFrame.setBatteryText(level);
	}

	public void analyzeConnectivityLevel(Event event)
	{
		String level = event.getLevel();
		if(null!=level){
			try{
				int levelI  = Integer.parseInt(level.substring(0, level.indexOf(" ")));
				if(levelI < 10){
					setLevel_low(true);
				}else if(levelI < 50){
					setLevel_mid(true);
				}else if(levelI < 90){
					setLevel_high(true);
				}else{
					setLevel_top(true);
				}
			}catch (Exception exception){
				setLevel_mid(true);
			}
		}
		topInfoFrame.setLevelText(level);
	}

	public void setBigImage(int ic)
	{
		if(null!=topInfoFrame) topInfoFrame.setBigImage(ic);
	}

	public void setFavorited(boolean favorited)
	{
		if(null!=topInfoFrame) topInfoFrame.setFavorited(favorited);
	}

	public void setTransition(Context context, boolean transition, int transition_start)
	{
		if(null!=topInfoFrame) topInfoFrame.setTransition(context, transition, transition_start);
	}

	public void setTitle(String s)
	{
		if(null!=topInfoFrame) topInfoFrame.setTitle(s);
	}
}
