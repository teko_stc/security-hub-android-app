package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.company.NetSDK.CB_fDisConnect;
import com.company.NetSDK.CB_fHaveReConnect;
import com.company.NetSDK.EM_OPTTYPE_MOBILE_TYPE;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.LOG_SET_PRINT_INFO;
import com.company.NetSDK.NET_PARAM;

import java.io.File;

import biz.teko.td.SHUB_APP.Utils.Dir.Const;

public final class NetSDKLib {
	private final static String TAG = "NetSDKLib";
	private static NetSDKLib instance = new NetSDKLib();
	private boolean mbInit = false;

	private DeviceDisConnect mDisconnect;
	private DeviceReConnect mReconnect;

	/// Timeout of NetSDK API
	public static final int TIMEOUT_5S = 5000;      // 5 second
	public static final int TIMEOUT_10S = 10000;    // 10 second
	public static final int TIMEOUT_30S = 30000;    // 30 second

	private NetSDKLib() {
		mDisconnect = new DeviceDisConnect();
		mReconnect = new DeviceReConnect();
	}

	public static NetSDKLib getInstance() {
		return instance;
	}

	/// Init NetSDK library's resources.
	public synchronized void init() {
		INetSDK.LoadLibrarys();
		if (mbInit) {
			Log.d(Const.LOG_TAG_DAHUA, "Already init.");
			return;
		}
		mbInit = true;

		/// Init NetSDK, and set disconnect callback.
		boolean zRet;
		try
		{
			zRet = INetSDK.Init(mDisconnect);
		}catch (Exception e){
			zRet = false;
		}
		if (!zRet) {
			Log.e(TAG, "init NetSDK error!");
			return;
		}

		int pParam = EM_OPTTYPE_MOBILE_TYPE.OPTTYPE_MOBILE_DEVICE_ATTR|EM_OPTTYPE_MOBILE_TYPE.OPTTYPE_MOBILE_DEVICE_SN|EM_OPTTYPE_MOBILE_TYPE.OPTTYPE_MOBILE_DISK_INFO|EM_OPTTYPE_MOBILE_TYPE.OPTTYPE_MOBILE_DIGITAL_NUM|
				EM_OPTTYPE_MOBILE_TYPE.OPTTYPE_MOBILE_ALARM_IO|EM_OPTTYPE_MOBILE_TYPE.OPTTYPE_MOBILE_DEVICE_ABILITY|EM_OPTTYPE_MOBILE_TYPE.OPTTYPE_MOBILE_STATE_SOFTWARE;

//		boolean bRet = INetSDK.SetOptimizeMode(EM_OPTIMIZE_TYPE.EM_OPT_TYPE_MOBILE_OPTION, pParam);
//		if(bRet == false)
//		{
//			Func.log_d(Const.LOG_TAG_DAHUA, "SetOptimizeMode-EM_OPT_TYPE_MOBILE_OPTION failed,pParam:" + pParam + "LastError:" + INetSDK.GetLastError());
//		}
		/// Set Reconnect callback.
		/// INetSDK.SetAutoReconnect(mReconnect);

		/// Close the SDK Log
		// closeSDKLog();

		/// Set global parameters of NetSDK.
		NET_PARAM stNetParam = new NET_PARAM();
		stNetParam.nConnectTime = TIMEOUT_5S;
		stNetParam.nWaittime = TIMEOUT_10S; // Time out of common Interface.
		stNetParam.nSearchRecordTime = TIMEOUT_30S; // Time out of Playback interface.
		INetSDK.SetNetworkParam(stNetParam);
	}

	/// Cleanup NetSDK library's resources.
	public synchronized void cleanup() {
		/// only be invoked for once
		if (mbInit) {
			INetSDK.Cleanup();
			mbInit = false;
		}
	}

	public boolean isFileExist(String fileName) {
		if (fileName == null) {
			return false;
		}

		File file = new File(fileName);
		return file.exists();
	}

	/// Open SDK log
	public boolean openLog(String logFile) {
		Log.d(TAG, "log file -> " + logFile);
		if (!isFileExist(logFile)) {
			return false;
		}

		LOG_SET_PRINT_INFO logInfo = new LOG_SET_PRINT_INFO();
		logInfo.bSetPrintStrategy = true;
		logInfo.nPrintStrategy = 0; // 0 - Saved as file. 1 - show log in the console.
		logInfo.bSetFilePath = true;
		System.arraycopy(logFile.getBytes(), 0, logInfo.szLogFilePath, 0, logFile.length());

		return INetSDK.LogOpen(logInfo);
	}

	/// Close SDK log
	public boolean closeLog() {
		return INetSDK.LogClose();
	}

	/// while app disconnect with device, the interface will be invoked.
	public class DeviceDisConnect implements CB_fDisConnect
	{
		@Override
		public void invoke(long loginHandle, String deviceIp, int devicePort) {
			Log.d(TAG, "Device " + deviceIp + " is disConnected !");
			mHandler.post(() -> {
//					ToolKits.alertDisconnected();
			});
		}
	}

	/// After app reconnect the device, the interface will be invoked.
	public class DeviceReConnect implements CB_fHaveReConnect
	{
		@Override
		public void invoke(long loginHandle, String deviceIp, int devicePort) {
			Log.d(TAG, "Device " + deviceIp + " is reconnect !");
		}
	}

	private Handler mHandler = new Handler(Looper.myLooper());

}
