package biz.teko.td.SHUB_APP.DevicesTabs.Adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ResourceCursorTreeAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import biz.teko.td.SHUB_APP.Codes.Activities.NLocalUsersActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.DevicesTabs.Activities.DevicesActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptsListActivity;
import biz.teko.td.SHUB_APP.SectionsTabs.SectionsActivity;
import biz.teko.td.SHUB_APP.UDP.Request_SURVEY;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.ZonesTabs.ZonesActivity;

public class DevicesTreeAdapter extends ResourceCursorTreeAdapter {

	private final Context context;
	private final DBHelper dbHelper;
	private final UserInfo userInfo;
	private final int collapsedGroupLayoutViewId;
	private final int expandedGroupLayoutViewId;
	private final int siteId;
	private final Site site;
	private int sending = 0;
	private final AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
	private D3Service myService;
	private final boolean showSections;
	private final boolean found = false;
	//private LocalConnection localConnection;


	public DevicesTreeAdapter(Context context, Cursor cursor, int collapsedGroupLayout, int expandedGroupLayout, int childLayout, int lastChildLayout, int siteId, boolean showSections) {
		super(context, cursor, collapsedGroupLayout, expandedGroupLayout, childLayout, lastChildLayout);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.siteId = siteId;
		site = dbHelper.getSiteById(siteId);
		this.showSections = showSections;
		userInfo = dbHelper.getUserInfo();
		View view = newGroupView(context, null, false, null);
		collapsedGroupLayoutViewId = view.getId();
		view = newGroupView(context, null, true, null);
		expandedGroupLayoutViewId = view.getId();
	}

	public void update(Cursor cursor, int sending){
		this.changeCursor(cursor);
		this.sending = sending;
	}

	@Override
	protected Cursor getChildrenCursor(Cursor groupCursor)
	{
		int device_id= groupCursor.getInt(1);
		return dbHelper.getAffectCursorBySiteDeviceId(siteId, device_id);	}

	@Override
	protected void bindGroupView(View view, final Context context, Cursor cursor, boolean isExpanded) {
		final Device device = new Device(cursor);

		RelativeLayout cardLinear = view.findViewById(R.id.cardLinear);
		LinearLayout cardLinearExp = view.findViewById(R.id.cardLinearExp);
		LinearLayout divider = view.findViewById(R.id.dividerExp);

		TextView modelText = view.findViewById(R.id.deviceModel); // model text capslock
		TextView serialText = view.findViewById(R.id.deviceSerial); //serial number text
		TextView statementText = view.findViewById(R.id.deviceStatement); //arm statement text

		ImageView imageArm = view.findViewById(R.id.deviceArmImage); //arm status image
		ImageView imageStatement = view.findViewById(R.id.deviceImage); // big image for statement

		LinearLayout buttonsLayout = view.findViewById(R.id.buttonsLayout);
		final TextView buttonMore = view.findViewById(R.id.buttonMore);
		final TextView buttonCodes = view.findViewById(R.id.buttonCodes);
		final TextView buttonArm = view.findViewById(R.id.buttonArm);
		final TextView buttonDisarm = view.findViewById(R.id.buttonDisarm);
		final TextView buttonScipts = view.findViewById(R.id.buttonScripts);
		final TextView buttonRelay = view.findViewById(R.id.buttonRelay);
		ImageView buttonMenu = view.findViewById(R.id.imageMore);


		serialText.setText(context.getString(R.string.DTA_SERIAL_NUMBER) + " " + device.account);
		String[] armStatements = context.getResources().getStringArray(R.array.arm_statement_titles);


		int armStatus = dbHelper.getSiteDeviceArmStatus(siteId, device.id);
//		if(device.type != 9)
//		{
//			if(null!=buttonsLayout){ buttonsLayout.setVisibility(View.VISIBLE); }
//			statementText.setText(armStatements[armStatus]);
//			statementText.setTextColor(armStatus > 0 ? context.getResources().getColor(R.color.brandColorGreen) : context.getResources().getColor(R.color.brandColorDark));
//			switch (armStatus)
//			{
//				case 0:
//					imageArm.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_restriction_shield_grey_500_big));
//					break;
//				case 1:
//					imageArm.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_security_checked_green_500_big_48));
//					break;
//				case 2:
//					imageArm.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_partly_armed_big));
//					break;
//				default:
//					break;
//			}
//		}else{
//			if(null!=buttonsLayout){ buttonsLayout.setVisibility(View.GONE); }
//			statementText.setVisibility(View.INVISIBLE);
//			imageArm.setVisibility(View.INVISIBLE);
//		}

		final  LinkedList<Event> affects = dbHelper.getAffectsBySiteDeviceId(siteId, device.id);

//		if(null!=affects && 0!=affects.size()){
//				if(divider!=null){
//					divider.setVisibility(View.GONE);
//				}
//				if(cardLinearExp!=null){
////					LinearLayout.LayoutParams layoutParams = device.type==Const.DEVICETYPE_MB? (LinearLayout.LayoutParams)cardLinear.getLayoutParams() :  (LinearLayout.LayoutParams) cardLinearExp.getLayoutParams();
////					cardLinearExp.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, layoutParams.height + Func.dpToPx(1,context)));
//				}
//		}else if (device.type == Const.DEVICETYPE_MB){
//				if(cardLinearExp!=null){
//					LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)cardLinear.getLayoutParams();
//					cardLinearExp.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, layoutParams.height + Func.dpToPx(1,context)));
//				}
//			}

		ImageView[] imagesSmall = new ImageView[7];
		for (int i = 0; i < 7; i++)
		{
			imagesSmall[i] = new ImageView(context);
			imagesSmall[i].setLayoutParams(new LinearLayout.LayoutParams(Func.dpToPx(24, context), Func.dpToPx(24, context)));
		}

		if (null != affects)
		{
			int imgSmallNum = 0;
			int sabI = 0;
			int malfI = 0;
			int warnI = 0;
			int armI = 0;
			int powI = 0;
			int gprsI = 0;
			int ethI = 0;
			int radioI = 0;

			for (Event affect : affects)
			{
				switch (affect.classId)
				{
					case 0:
						imageStatement.setImageResource(context.getResources().getIdentifier(affect.explainAffectIconBig(), null, context.getPackageName()));
						break;
					case 1:
						if (0 == sabI)
						{
							imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
							imgSmallNum++;
							sabI++;
						}
						break;
					case 2:
						if (0 == malfI)
						{
							imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
							imgSmallNum++;
							malfI++;
						}
						break;
					case 3:
						if (0 == warnI)
						{
							imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
							imgSmallNum++;
							warnI++;
						}
						break;
					case 4:
						break;
					case 5:
						break;
					case 6:
						if (9 != affect.reasonId)
						{
							if (0 == powI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								powI++;
							}
						}
						break;
					case 7:
						if (1 == affect.reasonId)
						{
							switch (affect.detectorId){
								case 21:
									if (0 == gprsI)
									{
										imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
										imgSmallNum++;
										gprsI++;
									}
									break;
								case 22:
									if (0 == ethI)
									{
										imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
										imgSmallNum++;
										ethI++;
									}
									break;
								default:
									if (0 == radioI)
									{
										imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
										imgSmallNum++;
										radioI++;
									}
									break;
							}
						}
						break;
				}
			}

			LinearLayout imageSmallStLayout = view.findViewById(R.id.smallIconLayoutDevice);
			if (null != imageSmallStLayout) {
				for (int n = 0; n < imgSmallNum; n++) {
					imageSmallStLayout.addView(imagesSmall[n]);
				}
			}
		}


		if((userInfo.roles&Const.Roles.DOMAIN_HOZ_ORG)==0)
		{
			if (buttonArm != null)
			{
				buttonArm.setVisibility(View.GONE);
			}
			if (buttonDisarm != null)
			{
				buttonDisarm.setVisibility(View.GONE);
			}
		}

		if(null != buttonArm)
		{
			if (sending == 0)
			{
				buttonArm.setEnabled(true);
				buttonArm.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						v.startAnimation(buttonClick);
						sendMessageToServer(device, Command.ARM, new JSONObject(), true);
					}
				});
			} else
			{
				buttonArm.setEnabled(false);
			}
		}

		if(null != buttonDisarm)
		{
			if (sending == 0)
			{
				buttonDisarm.setEnabled(true);
				buttonDisarm.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						v.startAnimation(buttonClick);
						sendMessageToServer(device, Command.DISARM, new JSONObject(), true);
					}
				});
			} else
			{
				buttonDisarm.setEnabled(false);
			}
		}

		if(null!=buttonMore){
			if(!showSections){
				buttonMore.setText(context.getString(R.string.DEVICES_BIG));
				buttonMore.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v) {
						v.startAnimation(buttonClick);
						Intent intent = new Intent(context, ZonesActivity.class);
						intent.putExtra("site", siteId);
						intent.putExtra("device", device.id);
						intent.putExtra("section", -1);
						intent.putExtra("version", 0);
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
			}else
			{
				buttonMore.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v) {
						v.startAnimation(buttonClick);
						Intent intent = new Intent(context, SectionsActivity.class);
						intent.putExtra("site", siteId);
						intent.putExtra("device", device.id);
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
			}
		}

		if(null!=buttonCodes){
			if((userInfo.roles & Const.Roles.TRINKET) == 0)
			{
				//			if(((userInfo.roles&Const.Roles.ORG_ENGIN) != 0)||((userInfo.roles& Const.Roles.DOMAIN_ENGIN) != 0)){
				buttonCodes.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v) {
						v.startAnimation(buttonClick);
						Intent intent = new Intent(context, NLocalUsersActivity.class);
						intent.putExtra("site", siteId);
						intent.putExtra("device", device.id);
						context.startActivity(intent);
						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
				//			}else{
				//				buttonCodes.setEnabled(false);
				//			}
			}else{
				buttonCodes.setVisibility(View.GONE);
			}
		}

		if(null!=buttonRelay){
			buttonRelay.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v) {
					v.startAnimation(buttonClick);
					Intent intent = new Intent(context, ZonesActivity.class);
					intent.putExtra("site", siteId);
					intent.putExtra("device", device.id);
					intent.putExtra("section", -1);
					intent.putExtra("version", 1);
					context.startActivity(intent);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
		}

		if(null!=buttonScipts){
			if(Func.needScriptsForBuild())
			{
				if (
//					((!context.getPackageName().equals("public.shub")) ||PreferenceManager.getDefaultSharedPreferences(context).getString("prof_language_value", "ru").equals("ru")) &&
					Func.getRemoteSetRights(userInfo, dbHelper, siteId, device.id)
					&& (0 == (userInfo.roles & Const.Roles.TRINKET)))
				{
					buttonScipts.setVisibility(View.VISIBLE);
					buttonScipts.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View view)
						{
							openScripts(device);
						}
					});
				}
			}
		}
		if(null!=buttonMenu){
			buttonMenu.setOnClickListener(new deviceCardMenuOnClick(device, dbHelper));
		}
	}

	private class deviceCardMenuOnClick implements View.OnClickListener
	{
		Device device;
		DBHelper dbHelper;

		public deviceCardMenuOnClick(Device device, DBHelper dbHelper)
		{
			this.device = device;
			this.dbHelper = dbHelper;
		}

		@Override
		public void onClick(View v)
		{
			final UserInfo userInfo = dbHelper.getUserInfo();
			AlertDialog.Builder adb = Func.adbForCurrentSDK(context);

			final String[] menuItemsArray = context.getResources().getStringArray(R.array.device_menu_values);
			List<String> menuItemsList = new ArrayList<>();

			menuItemsList.add(menuItemsArray[0]);

			if(!(((userInfo.roles&Const.Roles.DOMAIN_ENGIN)==0)&&((userInfo.roles&Const.Roles.ORG_ENGIN)==0))){
				if((userInfo.roles&Const.Roles.TRINKET) == 0)
				{
					menuItemsList.add(menuItemsArray[1]);
				}
				menuItemsList.add(menuItemsArray[4]);
				if(!dbHelper.getDevicePartedStatusForSite(siteId, device.id)
						&& device.canBeSetFromApp()){
					menuItemsList.add(menuItemsArray[2]);
					menuItemsList.add(menuItemsArray[3]);
//					if(context.getPackageName().equals("dev.shub")){
//						menuItemsList.add(menuItemsArray[5]);
//					}
//					menuItemsList.add(menuItemsArray[5]);
				}
			}

			final CharSequence[] items = menuItemsList.toArray(new CharSequence[menuItemsList.size()]);
			if(null != items){
				adb.setItems(items, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						String curValue = items[which].toString();
						if(curValue.equals(menuItemsArray[0])){showInfoDialog(device);}
						else if(curValue.equals(menuItemsArray[1])){showUSSDDialog(device);}
						else if(curValue.equals(menuItemsArray[2])){showRebootDialog(device);}
						else if(curValue.equals(menuItemsArray[3])){showUpdateDialog(device);}
						else if(curValue.equals(menuItemsArray[4])){showRevokeDialog(device);}
//						else if(curValue.equals(menuItemsArray[5])){openScripts(device);}
//						else if(curValue.equals(menuItemsArray[5])){
//							goToAPNSet(device);};
						dialog.dismiss();
					}
				});
				adb.show();
			}
		}
	}

	private void openScripts(Device device) {
		Intent intent = new Intent(context, ScriptsListActivity.class);
		intent.putExtra("site_id", siteId);
		intent.putExtra("device_id", device.id);
		context.startActivity(intent);
		((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
	}

	//	private void goToAPNSet(Device device)
//	{
//		Intent intent = new Intent(context, APNSetAcitivity.class);
//		intent.putExtra("site", siteId);
//		intent.putExtra("device", device.id);
//		intent.putExtra("section", -1);
//		intent.putExtra("version", 0);
//		((Activity) context).startActivity(intent);
//		((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
////
////		localConnection = new LocalConnection();
////		localConnection.shake();
////		(new UdpSearch()).execute();
////		UDPThread udpThread = new UDPThread();
////		udpThread.start();
//	}



	private void showRebootDialog(final Device device)
	{
		if (dbHelper.isDeviceOnline(device.id))
		{
			AlertDialog.Builder updateDialogBuilder = Func.adbForCurrentSDK(context);
			updateDialogBuilder.setTitle(R.string.REBOOT_DIALOG_TITLE);
			updateDialogBuilder.setMessage(R.string.REBOOT_DIALOG_MESS);
			updateDialogBuilder.setPositiveButton(R.string.REBOOT, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					sendMessageToServer(device, Command.REBOOT, new JSONObject(), true);
				}
			});
			updateDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
				}
			});
			updateDialogBuilder.show();
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showUpdateDialog(final Device device)
	{
		if (dbHelper.isDeviceOnline(device.id))
		{
			AlertDialog.Builder updateDialogBuilder = Func.adbForCurrentSDK(context);
			updateDialogBuilder.setTitle(R.string.STA_UPDATE_TITLE);
			updateDialogBuilder.setMessage(R.string.STA_UPDATE_MESS);
			updateDialogBuilder.setPositiveButton(R.string.STA_UPDATE, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if (dbHelper.getSiteDeviceArmStatus(siteId, device.id) == 0)
					{
						JSONObject commandAddr = new JSONObject();
						try
						{
							commandAddr.put("file", 0);
							sendMessageToServer(device, Command.UPDATE, commandAddr, true);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
					} else
					{
						Func.nShowMessage(context, context.getString(R.string.OBJECT_DISARM_MESSAGE_UPDATE));
					}
				}
			});
			updateDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
				}
			});
			updateDialogBuilder.show();
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showRevokeDialog(final Device device)
	{
		AlertDialog.Builder revokeDialogBuilder = Func.adbForCurrentSDK(context);
		revokeDialogBuilder.setTitle(R.string.DTA_UNBINDING);
		revokeDialogBuilder.setMessage(R.string.DTA_UNBINDONG_MESSAGE);
		revokeDialogBuilder.setPositiveButton(R.string.DTA_UNBIND, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				JSONObject message = new JSONObject();
				try
				{
					message.put("device_id", device.id);
					message.put("domain_id", userInfo.domain);
					sendMessageToServer(null, D3Request.DEVICE_REVOKE_DOMAIN, message,false);
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
			}
		});
		revokeDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		revokeDialogBuilder.show();
	}

	private void showUSSDDialog(final Device device) {
		AlertDialog.Builder ussdRequestDialog = Func.adbForCurrentSDK(context);
		AlertDialog addDevDialog = ussdRequestDialog.create();
		addDevDialog.setTitle(R.string.USSD_DIALOG_TITLE);
		addDevDialog.setMessage(context.getString(R.string.USSD_DIALOG_MESSAGE));
		FrameLayout frameLayout = new FrameLayout(context);
		frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		frameLayout.setPadding(Func.dpToPx(20, context), 0, Func.dpToPx(20, context), 0);
		final EditText editText = new EditText(context);
		String ussd = "*100#";
		String imsi = dbHelper.getCurrectDeviceImsi(device.id);
		if ((null != imsi) && (!imsi.equals(""))) {
			ussd = Func.getUSSD(Integer.valueOf(imsi.substring(0, 8)));
		}
		editText.setText(ussd);
		try {
			@SuppressLint("SoonBlockedPrivateApi") Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
			f.setAccessible(true);
			f.set(editText, R.drawable.caa_cursor);
		} catch (Exception ignored)
		{
		}
		editText.setCursorVisible(true);
		editText.isCursorVisible();
		editText.setTextColor(context.getResources().getColor(R.color.md_black_1000));
		editText.setGravity(Gravity.CENTER);
		editText.requestFocus();

		frameLayout.addView(editText);
		addDevDialog.setButton(Dialog.BUTTON_POSITIVE, context.getString(R.string.ADB_USSD), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String request = editText.getText().toString();
				if((request!=null)&&(request.length()!=0))
				{
					JSONObject commandAddr = new JSONObject();
					try
					{
						commandAddr.put("text", request);
						sendMessageToServer(device, Command.USSD, commandAddr, true);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
				}else{
					Func.pushToast(context, context.getString(R.string.ENTER_USSD), (Activity) context);
				}
			}
		});
		addDevDialog.setButton(Dialog.BUTTON_NEGATIVE, context.getString(R.string.ADB_CANCEL), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		addDevDialog.setView(frameLayout);
		addDevDialog.show();
	}

	private void showInfoDialog(Device device)
	{
		AlertDialog.Builder infoDialogBuilder = Func.adbForCurrentSDK(context);
		infoDialogBuilder.setTitle(R.string.INFORMATION);
		String message = "\n";

		if(null!=device) {
			message += context.getString(R.string.DTA_DEVICE_DOTS) + " " + device.getName() + "\n";
			message += context.getString(R.string.DTA_DEVICE_SN_DOTS) + " " + device.account + "\n";
			message += context.getString(R.string.DTA_DEVICE_VER_DOTS) + " " + Func.getDeviceVersion(device.config_version);
		}


		infoDialogBuilder.setMessage(message);
		infoDialogBuilder.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		infoDialogBuilder.show();
	}

	@Override
	protected void bindChildView(View view, final Context context, Cursor cursor, boolean isLastChild) {
		TextView affectName = view.findViewById(R.id.affectName);
		ImageView affectImage = view.findViewById(R.id.affectImage);
		TextView affectPlace = view.findViewById(R.id.affectPlace);
		affectPlace.setVisibility(View.VISIBLE);
		TextView affectDevice = view.findViewById(R.id.affectDevice);
		affectDevice.setVisibility(View.GONE);

		final Event affect = dbHelper.getAffect(cursor);

		final String affectExplanation = affect.explainAffectRegDescription(context);
		affectName.setText(Html.fromHtml(affectExplanation));

		String sectionText = "";
		String zoneText = "";
		String place = "";
		if(affect.section == 0){
			if(affect.zone != 0){
				zoneText = dbHelper.getZoneNameById(affect.device, affect.section, affect.zone) + "(" + affect.zone + ")" ;
			}else{
				affectPlace.setVisibility(View.GONE);
			}
			place += zoneText;
		}else{
			if(affect.zone ==0){
				sectionText = dbHelper.getSectionNameByIdWithOptions(affect.device, affect.section) + "(" + affect.section + ") - " + context.getString(R.string.WHOLE_SECTION);
				place += sectionText;
			} else {
				zoneText = dbHelper.getZoneNameById(affect.device, affect.section, affect.zone) + "(" + affect.zone + ")";
				sectionText = dbHelper.getSectionNameByIdWithOptions(affect.device, affect.section) + "(" + affect.section + ")";
				place += zoneText + " • " + sectionText;
			}
		}

		affectPlace.setText(place);
		affectImage.setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
		View divider = view.findViewById(R.id.childDivider);
		View padding = view.findViewById(R.id.childPadding);
		if (isLastChild) {
			divider.setVisibility(View.VISIBLE);
			padding.setVisibility(View.VISIBLE);
		} else {
			divider.setVisibility(View.GONE);
			padding.setVisibility(View.GONE);

		}

		final String finalZoneText = zoneText;
		final String finalSectionText = sectionText;
		view.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Func.showAffectInfo(context, site.id, finalZoneText, finalSectionText, affectExplanation, affect);
			}
		});
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		Cursor cursor = getGroup(groupPosition);
		View v = convertView;
		if (cursor != null)
		{
			if (convertView == null)
			{
				v = newGroupView(context, cursor, isExpanded, parent);
			} else
			{
				if ((isExpanded && convertView.getId() == collapsedGroupLayoutViewId) || (!isExpanded && convertView.getId() == expandedGroupLayoutViewId))
				{
					v = newGroupView(context, cursor, isExpanded, parent);
				}
			}
			bindGroupView(v, context, cursor, isExpanded);
		}
		return v;
	}

	@Override
	public void notifyDataSetChanged() {
		notifyDataSetChanged(false);
	}

	public class UDPThread extends Thread{

		private InetAddress address;
		private DatagramSocket udpSocket;
		private boolean running;
		private  static  final int PORT = 22237;

		public UDPThread()
		{
			super();
			initSocket();
		}

		public void initSocket(){
			try
			{
//				this.address = InetAddress.getByName("0.0.0.0");
				this.udpSocket = new DatagramSocket();
				this.udpSocket.setBroadcast(true);
			} catch (SocketException e)
			{
				e.printStackTrace();
			}

		}

		public  DatagramSocket getUdpSocket(){
			return this.udpSocket;
		}

		public boolean getStatus(){
			return running;
		}

		public void stopRunning(){
			running = false;
		}

		public void run(){
			running = true;
			long localtime = System.currentTimeMillis()/1000;
			while(running && (System.currentTimeMillis()/1000 < (localtime + 300))){
				try
				{
					byte[] buf = (new Request_SURVEY((byte)0, 0)).getBytes();
					DatagramPacket packet = new DatagramPacket(buf, buf.length, getBroadcastAddress(), PORT);
					if(null == udpSocket)
					{
						initSocket();
					}

					udpSocket.send(packet);

					Func.log_d(D3Service.LOG_TAG, buf.toString());
					synchronized (this) {
						wait(3000);
					}
//					// receive request
//					DatagramPacket packet1 = new DatagramPacket(buf, buf.length);
//					udpSocket.receive(packet1);     //this code block the program flow
//
//					// sendReply the response to the client at "address" and "port"
//					InetAddress address = packet.getAddress();
//					int port = packet.getPort();
//
//					Func.log_d("D3Service", "LocalConfig from: " + address + ":" + port + "\n");
				} catch (IOException e)
				{
					e.printStackTrace();
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

		}

		InetAddress getBroadcastAddress() throws IOException {
			WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			DhcpInfo dhcp = wifi.getDhcpInfo();
			// handle null somehow

			int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
			byte[] quads = new byte[4];
			for (int k = 0; k < 4; k++)
				quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
			return InetAddress.getByAddress(quads);
		}
	}

	public boolean sendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		Activity activity = (DevicesActivity)context;
		if(activity != null){
			DevicesActivity devicesActivity = (DevicesActivity) activity;
			return  devicesActivity.nSendMessageToServer(d3Element, Q, data, command);
		}
		return false;
	}
}
