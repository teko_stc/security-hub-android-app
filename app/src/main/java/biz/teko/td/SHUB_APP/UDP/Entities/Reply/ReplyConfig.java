package biz.teko.td.SHUB_APP.UDP.Entities.Reply;

import android.os.Bundle;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;

import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.ArmWaitReviewed;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.ArmWaitServer;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.DisarmWaitServer;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.RadioLiter;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.RoamingEnabled;
import biz.teko.td.SHUB_APP.UDP.LocalConnection;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;

public class ReplyConfig extends BaseReplyConfig implements CustomizableConfig {

    public ReplyConfig(byte[] data) {
        this.data = data;
        setFlags();
    }

    public byte[] getData() {
        return data;
    }

    public void setNewAnotherConfig(Bundle bundle, String configType) {
        if (Arrays.asList(ConfigConstants.stringsWithDialogBoolean).contains(configType)) {
            setBooleanConfig(bundle, configType);
        } else {
            switch (configType) {
                case ConfigConstants.CONFIG_PIN:
                    setNewFromInt(afterIntLe(bundle.getString(ConfigConstants.CONFIG_PIN)), 0x2F8, 4); break;
                case ConfigConstants.CONFIG_RESERVE_PORT:
                    setNewFromInt(afterIntLe(bundle.getString(ConfigConstants.CONFIG_RESERVE_PORT)), 0x18, 2); break;
                case ConfigConstants.CONFIG_MAIN_PORT:
                    setNewFromInt(afterIntLe(bundle.getString(ConfigConstants.CONFIG_MAIN_PORT)), 0x12, 2); break;
                case ConfigConstants.CONFIG_RADIO_OFFSET:
                    setNewFromInt(afterShortLe(bundle.getString(ConfigConstants.CONFIG_RADIO_OFFSET)), 0x0C, 2); break;
                case ConfigConstants.CONFIG_SERVER_IP:
                    setNewFromIp(bundle.getString(ConfigConstants.CONFIG_SERVER_IP), 0x0E); break;
                case ConfigConstants.CONFIG_MASK_IP:
                    setNewFromIp(bundle.getString(ConfigConstants.CONFIG_MASK_IP), 0x304); break;
                case ConfigConstants.CONFIG_GATEWAY_IP:
                    setNewFromIp(bundle.getString(ConfigConstants.CONFIG_GATEWAY_IP), 0x300); break;
                case ConfigConstants.CONFIG_STATIC_IP:
                    setNewFromIp(bundle.getString(ConfigConstants.CONFIG_STATIC_IP), 0x2FC); break;
                case ConfigConstants.CONFIG_RESERVE_IP:
                    setNewFromIp(bundle.getString(ConfigConstants.CONFIG_RESERVE_IP), 0x14); break;
                case ConfigConstants.CONFIG_ENTRY_DELAY:
                    this.data[0x2F6] = Byte.parseByte(bundle.getString(ConfigConstants.CONFIG_ENTRY_DELAY)); break;
                case ConfigConstants.CONFIG_EXIT_DELAY:
                    this.data[0x2F7] = Byte.parseByte(bundle.getString(ConfigConstants.CONFIG_EXIT_DELAY)); break;
                case ConfigConstants.APN_SERVER:
                    setNewFromString(bundle.getString(ConfigConstants.APN_SERVER).getBytes(), 0x276, 32); break;
                case ConfigConstants.APN_LOGIN:
                    setNewFromString(bundle.getString(ConfigConstants.APN_LOGIN).getBytes(), 0x2B6, 16); break;
                case ConfigConstants.APN_PASSWORD:
                    setNewFromString(bundle.getString(ConfigConstants.APN_PASSWORD).getBytes(), 0x2D6, 16); break;
                case ConfigConstants.APN2_SERVER:
                    setNewFromString(bundle.getString(ConfigConstants.APN2_SERVER).getBytes(), 0x296, 32); break;
                case ConfigConstants.APN2_LOGIN:
                    setNewFromString(bundle.getString(ConfigConstants.APN2_LOGIN).getBytes(), 0x2C6, 16); break;
                case ConfigConstants.APN2_PASSWORD:
                    setNewFromString(bundle.getString(ConfigConstants.APN2_PASSWORD).getBytes(), 0x2E6, 16); break;
            }
        }
    }

    public HashMap<String, String> getMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put(ConfigConstants.APN_SERVER, LocalConnection.decodeUTF8(getAPNServer()));
        map.put(ConfigConstants.APN_LOGIN, LocalConnection.decodeUTF8(getAPNLogin()));
        map.put(ConfigConstants.APN_PASSWORD, LocalConnection.decodeUTF8(getAPNPassword()));
        map.put(ConfigConstants.APN2_SERVER, LocalConnection.decodeUTF8(getAPN2Server()));
        map.put(ConfigConstants.APN2_LOGIN, LocalConnection.decodeUTF8(getAPN2Login()));
        map.put(ConfigConstants.APN2_PASSWORD, LocalConnection.decodeUTF8(getAPN2Password()));
        map.put(ConfigConstants.CONFIG_PIN, String.valueOf(LocalConnection.byteArrayToInt(LocalConnection.invertByte(getConfigPin()))));
        map.put(ConfigConstants.CONFIG_RESERVE_PORT, String.valueOf(LocalConnection.byteArrayToInt(LocalConnection.invertByte(getReservePort()))));
        map.put(ConfigConstants.CONFIG_MAIN_PORT, String.valueOf(LocalConnection.byteArrayToInt(LocalConnection.invertByte(getMainPort()))));
        map.put(ConfigConstants.CONFIG_RADIO_OFFSET, String.valueOf(ByteBuffer.wrap(LocalConnection.invertByte(getRadioOffset())).getShort()));
        map.put(ConfigConstants.CONFIG_SERVER_IP, getIpFromBytes(getServerIP()));
        map.put(ConfigConstants.CONFIG_MASK_IP, getIpFromBytes(getMaskIP()));
        map.put(ConfigConstants.CONFIG_GATEWAY_IP, getIpFromBytes(getGatewayIP()));
        map.put(ConfigConstants.CONFIG_STATIC_IP, getIpFromBytes(getStaticIP()));
        map.put(ConfigConstants.CONFIG_RESERVE_IP, getIpFromBytes(getReserveIP()));
        map.put(ConfigConstants.CONFIG_RADIO_LITER, String.valueOf(new RadioLiter().get(this.flags[0])));
        map.put(ConfigConstants.CONFIG_WAIT_REVIEWED, String.valueOf(new ArmWaitReviewed().get(this.flags[0])));
        map.put(ConfigConstants.CONFIG_ARM_SERVER, String.valueOf(new ArmWaitServer().get(this.flags[1])));
        map.put(ConfigConstants.CONFIG_DISARM_SERVER, String.valueOf(new DisarmWaitServer().get(this.flags[1])));
        map.put(ConfigConstants.CONFIG_ROAMING, String.valueOf(new RoamingEnabled().get(this.flags[1])));
        map.put(ConfigConstants.CONFIG_ENTRY_DELAY, String.valueOf(getEntryDelay()[0]));
        map.put(ConfigConstants.CONFIG_EXIT_DELAY, String.valueOf(getExitDelay()[0]));
        return map;
    }

    private byte[] getExitDelay() {
        return getBytes(1, 0x2F7);
    }

    private byte[] getEntryDelay() {
        return getBytes(1, 0x2F6);
    }

    private byte[] getReserveIP() {
        return getBytes(4, 0x14);
    }

    private byte[] getStaticIP() {
        return getBytes(4, 0x2FC);
    }

    private byte[] getGatewayIP() {
        return getBytes(4, 0x300);
    }

    private byte[] getMaskIP() {
        return getBytes(4, 0x304);
    }

    private byte[] getServerIP() {
        return getBytes(4, 0x0E);
    }

    private byte[] getRadioOffset() {
        return getBytes(2, 0x0C);
    }

    private byte[] getMainPort() {
        return getBytes(2, 0x12);
    }

    private byte[] getReservePort() {
        return getBytes(2, 0x18);
    }

    private byte[] getConfigPin() {
        return getBytes(4, 0x2F8);
    }

    private byte[] getAPNServer() {
        return getBytes(32, 0x276);
    }

    private byte[] getAPNLogin() {
        return getBytes(16, 0x2B6);
    }

    private byte[] getAPNPassword() {
        return getBytes(16, 0x2D6);
    }

    private byte[] getAPN2Server() {
        return getBytes(32, 0x296);
    }

    private byte[] getAPN2Login() {
        return getBytes(16, 0x2C6);
    }

    private byte[] getAPN2Password() {
        return getBytes(16, 0x2E6);
    }
}
