package biz.teko.td.SHUB_APP.Profile.LocalConfig.Presenters;

import java.util.concurrent.TimeUnit;

import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;
import biz.teko.td.SHUB_APP.Utils.Callback;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DeviceFactoryResetViewModel {

    private final SocketConnection  localConnection;
    private final CompositeDisposable connectionDisposables = new CompositeDisposable();
    private Disposable timerDisposable;
    private final DeviceDTO device;

    public DeviceDTO getDevice() {
        return device;
    }
    public void sendTestModeOn(){
        localConnection.sendTestModeOn(device.getIp(), device.getPin());
        device.setTestMode(true);
    }

    public DeviceFactoryResetViewModel(SocketConnection localConnection, DeviceDTO device) {
        this.localConnection = localConnection;
        this.device = device;
    }
    public void resetToDefaults(Callback callback, Callback onTimeOut,Callback onError) {
        localConnection.sendResetWithErrorCallback(device.getIp(), device.getPin(), onError);
        waitForDeviceToActivate(() -> {
            localConnection.sendTestModeOff(device.getIp(), device.getPin());
            device.setTestMode(false);
            callback.complete();
        }, () -> {
            localConnection.sendTestModeOff(device.getIp(), device.getPin());
            device.setTestMode(false);
            onTimeOut.complete();
        } );
    }
    private Observable<Long> getTimerObservable(int timeout) {
        return Observable
                .timer(timeout, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    private void waitForDeviceToActivate(Callback callback,Callback onTimeOut){
        sendReplyTimer();
        waitForDeviceToShowUp(callback);
        timerDisposable = getTimerObservable(10).subscribe(aLong -> {
            connectionDisposables.dispose();
            onTimeOut.complete();
        });
    }
    public void finish(){
        connectionDisposables.dispose();
        timerDisposable.dispose();
    }

    private void sendReplyTimer() {
        connectionDisposables.add(Observable.interval(3, TimeUnit.SECONDS)
                //.delay(1, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .subscribe(aLong -> localConnectionSendSurveyRequest()));
    }

    private void localConnectionSendSurveyRequest() {
        if (!localConnection.isClosed()) {
            localConnection.sendSurveyRequest();
        }
    }

    private void waitForDeviceToShowUp(Callback callback) {
        connectionDisposables.add(localConnection.getDeviceSubject()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(udpDevice -> {
            if (udpDevice.ip.equals(device.getIp())) {
                callback.complete();
            }
        }));
    }


}