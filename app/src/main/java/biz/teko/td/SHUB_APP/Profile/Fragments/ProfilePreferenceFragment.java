package biz.teko.td.SHUB_APP.Profile.Fragments;

import static android.app.Activity.RESULT_OK;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.text.Html;

import com.afollestad.materialdialogs.MaterialDialog;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Delegation.Activities.DelegateAddActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NAboutActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NLicenseActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NProfileLanguageActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NProfileUsersActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.ProfileConnectionActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.ProfileMasterActivity;
import biz.teko.td.SHUB_APP.Profile.CurrentUser.CurrentUserActivity;
import biz.teko.td.SHUB_APP.Profile.Interface.ProfileInterfaceAcitivty;
import biz.teko.td.SHUB_APP.Profile.Notifications.NotificationsActivity;
import biz.teko.td.SHUB_APP.Profile.Security.SecurityActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerSetActivity;

/**
 * Created by td13017 on 25.03.2017.
 */

public class ProfilePreferenceFragment extends PreferenceFragment
{
	private DBHelper dbHelper;
	private Activity context;
	private D3Service service;
	private final int sending = 0;
	private D3Service myService;
	private Preference prof_language;
	private SharedPreferences sp;
	private MaterialDialog progressDialog;


	@Override
	public void onCreate(final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		context = getActivity();
		dbHelper = DBHelper.getInstance(context);
		final UserInfo userInfo = dbHelper.getUserInfo();
		sp = PreferenceManager.getDefaultSharedPreferences(context);

		if ((userInfo.roles & Const.Roles.TRINKET) == 0) // укороченный преференс для брелка
		{
			if(Func.isMasterMode(sp)){
				addPreferencesFromResource(R.xml.profile_preferences);
			}else{
				addPreferencesFromResource(R.xml.profile_preferences_light);
			}
		}else{
			addPreferencesFromResource(R.xml.profile_preferences_trinket);
		}

		CheckBoxPreference pref_menu_mode = (CheckBoxPreference) findPreference("pref_menu_mode");

		Preference prof_current_user = (Preference) findPreference("prof_current_user");
		Preference prof_domain_users = (Preference) findPreference("prof_domain_users");
		Preference prof_security = (Preference) findPreference("prof_security");
		Preference prof_notif_settings = (Preference) findPreference("prof_notif_settings");
		Preference prof_master_settings = (Preference) findPreference("prof_master_settings");
		Preference prof_interface = (Preference) findPreference("prof_interface_settings");
		Preference prof_connection = (Preference) findPreference("prof_connection_settings");

		PreferenceCategory prof_object_settings = (PreferenceCategory) findPreference("prof_object_settings");
		Preference prof_add_object = (Preference) findPreference("prof_add_object");
		Preference prof_add_deleg_object = (Preference) findPreference("prof_add_deleg_object");
		Preference prof_device_apn = (Preference) findPreference("prof_device_apn");

		PreferenceCategory prof_information = (PreferenceCategory) findPreference("prof_information");

		if((userInfo.roles&Const.Roles.DOMAIN_ADMIN)==0)
		{
			//			prof_change_name.setEnabled(false);
			//			prof_change_pass.setEnabled(false);
			if(null!= prof_domain_users)prof_domain_users.setEnabled(false);
		}
		if(((userInfo.roles& Const.Roles.DOMAIN_LAWYER)==0)&&((userInfo.roles&Const.Roles.ORG_LAWYER)==0))
		{
			if(null!=prof_add_object)prof_add_object.setEnabled(false);
			if(null!=prof_add_deleg_object)prof_add_deleg_object.setEnabled(false);
			if(null!=prof_device_apn)prof_device_apn.setEnabled(false);
		}

		if(null!=pref_menu_mode) pref_menu_mode.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object o)
			{
				restartActivity();
				return true;
			}
		});

		if(null!=prof_current_user) prof_current_user.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				Intent intent = new Intent(context, CurrentUserActivity.class);
				startActivity(intent);
				getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);

				return false;
			}
		});

		if(null!=prof_security)
		{
			prof_security.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					Intent intent = new Intent(context, SecurityActivity.class);
					startActivity(intent);
					getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
					return false;
				}
			});
		}

		if(null!=prof_domain_users)prof_domain_users.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				UserInfo userInfo = dbHelper.getUserInfo();
				if((userInfo.roles&Const.Roles.DOMAIN_ADMIN)!=0)
				{
					Intent intent = new Intent(context, NProfileUsersActivity.class);
					startActivity(intent);
					getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
				}else{
					Func.nShowMessage(context, context.getString(R.string.EA_NO_RIGHTS));
				}
				return false;

			}
		});

		if(null!=prof_notif_settings) prof_notif_settings.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				Intent intent = new Intent(context, NotificationsActivity.class);
				startActivity(intent);
				getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
				return false;
			}
		});

		if(null!= prof_add_object) prof_add_object.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				Intent intent = new Intent(context, WizardControllerSetActivity.class);
				intent.putExtra("from", 1);
				((Activity) context).startActivity(intent);
				((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				return false;
			}
		});

		if(null!=prof_object_settings){
			if(Func.tekoBuild()){
				getPreferenceScreen().removePreference(prof_object_settings);
			}
		}

		if(null!=prof_add_deleg_object)
		{
			prof_add_deleg_object.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					UserInfo userInfo = dbHelper.getUserInfo();
					if(((userInfo.roles&Const.Roles.DOMAIN_LAWYER)!=0)||((userInfo.roles&Const.Roles.ORG_LAWYER)!=0))
					{
						Intent intent = new Intent(context, DelegateAddActivity.class);
						startActivity(intent);
						getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
					} else
					{
						Func.nShowMessage(context, context.getString(R.string.EA_NO_RIGHTS));
					}
					return false;
				}
			});
		}

		if(null!=prof_device_apn)
		{
			prof_device_apn.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
//					Intent intent = new Intent(context, ConfigListActivity.class);
//					startActivity(intent);
//					getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
					return false;
				}
			});
		}

		if(null!=prof_information)
		{
			Preference prof_license = (Preference) prof_information.findPreference("prof_license");
			Preference prof_about = (Preference) prof_information.findPreference("prof_about");
			prof_language = (Preference) prof_information.findPreference("prof_language");

			if(null!=prof_license)
			{
				if(Func.needLicenseAForBuild()){
					prof_license.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
					{
						@Override
						public boolean onPreferenceClick(Preference preference)
						{
							Intent intent = new Intent(context, NLicenseActivity.class);
							startActivity(intent);
							context.overridePendingTransition(R.animator.enter, R.animator.exit);
							return false;
						}
					});
				}else{
					prof_information.removePreference(prof_license);
				}
			}

			if(null!=prof_about) prof_about.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					Intent intent = new Intent(context, NAboutActivity.class);
					startActivity(intent);
					context.overridePendingTransition(R.animator.enter, R.animator.exit);
					return false;
				}
			});

			if(null != prof_language){
				if(Func.needLangForBuild(context)){
					prof_language.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
					{
						@Override
						public boolean onPreferenceClick(final Preference preference)
						{
							Intent intent = new Intent(context, NProfileLanguageActivity.class);
							startActivityForResult(intent, D3Service.CHOOSE_LANGUAGE);
							context.overridePendingTransition(R.animator.enter, R.animator.exit);
							return false;
						}
					});
				}else{
					prof_information.removePreference(prof_language);
				}
			}
		}


		if(null!=prof_master_settings){
			prof_master_settings.setTitle(Html.fromHtml("<b>"
					+ (Func.isMasterMode(sp)?getString(R.string.MASTER_MODE_ON_TITLE):getString(R.string.MASTER_MODE_OFF_TITLE))
					+ "</b>"));
			prof_master_settings.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					Intent intent =  new Intent(context, ProfileMasterActivity.class);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
					{
						startActivityForResult(intent, D3Service.CHOOSE_MASTER_MODE, ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle());
					}else{
						context.overridePendingTransition(R.animator.enter, R.animator.exit);
						startActivityForResult(intent, D3Service.CHOOSE_MASTER_MODE);
					}
					return false;
				}
			});
		}

		if(null!= prof_interface){
			prof_interface.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					Intent intent =  new Intent(context, ProfileInterfaceAcitivty.class);
					startActivity(intent);
					context.overridePendingTransition(R.animator.enter, R.animator.exit);
					return false;
				}
			});
		}

		if(null!= prof_connection){
			prof_connection.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					Intent intent =  new Intent(context, ProfileConnectionActivity.class);
					startActivity(intent);
					context.overridePendingTransition(R.animator.enter, R.animator.exit);
					return false;
				}
			});
		}

	}


	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.CHOOSE_LANGUAGE
				|| requestCode == D3Service.CHOOSE_MASTER_MODE
		){
			if(resultCode == RESULT_OK){
//				progressDialog = new MaterialDialog.Builder(context)
//						.title(R.string.DIALOG_WAIT_TITLE)
//						.content(R.string.DIALOG_WAIT_MESS)
//						.progress(true, 0)
//						.show();
				restartActivity();
			}
		}
	}

	private void restartActivity() {
//		Intent serviceIntent = new Intent(context, D3Service.class);
//		context.startService(serviceIntent);
		Intent intent = context.getIntent();
		int position = context.getIntent().getIntExtra("position", 0);
		intent.putExtra("position", position == 0 ?
				(Func.getBooleanSPDefFalse(sp, "prof_master_set")? ProfileFragment.DEF_POSITION_MASTER : ProfileFragment.DEF_POSITION)
				: (Func.getBooleanSPDefFalse(sp, "prof_master_set")? position +1 : position -1 ));

		//		if(null!=progressDialog&&progressDialog.isShowing()){
//			progressDialog.dismiss();
//		}
		context.finish();
		context.overridePendingTransition(R.animator.enter, R.animator.exit);
		startActivity(intent);
	}
}
