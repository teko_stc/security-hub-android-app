package biz.teko.td.SHUB_APP.SecCompanies.Adapters;

import static android.content.Context.TELEPHONY_SERVICE;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.lang.reflect.Field;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SecCompanies.Models.CPhone;
import biz.teko.td.SHUB_APP.SecCompanies.Models.SCDepart;
import biz.teko.td.SHUB_APP.SecCompanies.Models.SCRc;
import biz.teko.td.SHUB_APP.SecCompanies.Models.SCompany;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;

/**
 * Created by td13017 on 14.03.2018.
 */

public class SecCompAdapter extends RecyclerView.Adapter<SecCompAdapter.SecCompViewHolder> {
    public static class SecCompViewHolder extends RecyclerView.ViewHolder {
        ConstraintLayout secCompFrame;
        ImageView logo;
        TextView name;
        TextView address;
        TextView phone;
        TextView email;

        public SecCompViewHolder(View itemView) {
            super(itemView);
            secCompFrame = (ConstraintLayout) itemView.findViewById(R.id.sec_comp_layout);
            logo = (ImageView) itemView.findViewById(R.id.sec_comp_logo);
            name = (TextView) itemView.findViewById(R.id.sec_comp_name);
        }
    }

    LinkedList<SCompany> companies;
    Context context;

    public SecCompAdapter(LinkedList<SCompany> companies, Context context) {
        this.companies = companies;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }


    @Override
    public SecCompViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_security_company, parent, false);
        return new SecCompViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final SecCompViewHolder holder, final int position) {
        if (!companies.get(position).logo.equals("null")) {
            holder.logo.setVisibility(View.VISIBLE);
            if (!companies.get(position).logo.equals("")) {
                holder.logo.setImageResource(context.getResources().getIdentifier(companies.get(position).logo, null, context.getPackageName()));
            } else {
                holder.logo.setImageResource(context.getResources().getIdentifier("@drawable/ic_shield", null, context.getPackageName()));
            }
        } else {
            holder.logo.setVisibility(View.GONE);
        }
        holder.name.setText(null != companies.get(position).title ? companies.get(position).title : companies.get(position).caption);
        holder.secCompFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != companies.get(position).departs && companies.get(position).departs.size() > 0) {
                    showDepartsBottomSheet(companies.get(position));
                } else if (null != companies.get(position).scrcs && companies.get(position).scrcs.size() > 0) {
                    showPRsBottomSheet(companies.get(position), companies.get(position).scrcs);
                } else {
                    showSCInfoDialog(companies.get(position));
                }

            }
        });
    }

    private void showSCInfoDialog(SCompany sCompany) {
        NDialog dialog = new NDialog(context, R.layout.n_dialog_companies_item);
        ((TextView) dialog.findViewById(R.id.nDialogTitle)).setText(sCompany.caption);
        NActionButton callButton = dialog.findViewById(R.id.nDialogButtonCall);
        NActionButton emailButton = dialog.findViewById(R.id.nDialogButtonEmail);
        NActionButton websiteButton = dialog.findViewById(R.id.nDialogButtonSite);
        TextView messageView = dialog.findViewById(R.id.textMessage);
        TextView addressesView = dialog.findViewById(R.id.textAddress);
        TextView phonesView = dialog.findViewById(R.id.textPhone);
        TextView emailsView = dialog.findViewById(R.id.textMail);

        String mess = "";
        String addressesString = "";
        String phonesString = "";
        String emailsString = "";

        if (null != sCompany.title)
            mess += sCompany.title + "\n\n";
        else if (null != sCompany.message)
            mess += sCompany.message + "\n\n";
        if (!mess.equals("")) {
            messageView.setVisibility(View.VISIBLE);
            messageView.setText(mess);
        }

        String[] addresses = sCompany.addresses;
        if (null != addresses && addresses.length > 0) {
            for (String address : addresses)
                addressesString += address;
            addressesView.setVisibility(View.VISIBLE);
            addressesView.setText(addressesString);
        }

        final String[] phones = sCompany.phones;
        final LinkedList<CPhone> cphones = sCompany.cphones;

        if (setInfo(sCompany.phones, phonesView))
            callButton.setVisibility(View.VISIBLE);

        if (null != cphones && cphones.size() > 0) {
            for (CPhone cPhone : cphones)
                phonesString += cPhone.phone + "(" + cPhone.caption + ")\n";
            StringBuilder buffer = new StringBuilder(phonesString);
            phonesString = (buffer.deleteCharAt(phonesString.length() - 1)).toString();
            phonesView.setVisibility(View.VISIBLE);
            phonesView.setText(phonesString);
            callButton.setVisibility(View.VISIBLE);
        }

        final String[] emails = sCompany.emails;
        if (setInfo(emails, emailsView))
            emailButton.setVisibility(View.VISIBLE);

        final String[] sites = sCompany.sites;
        if (null != sites && sites.length > 0) {
            websiteButton.setVisibility(View.VISIBLE);
        }
        dialog.show();

        callButton.setOnButtonClickListener(action -> {
            if (null != phones && phones.length > 0) {
                dialog.dismiss();
                doCall(phones, null);
                dialog.dismiss();
            } else if (null != cphones && cphones.size() > 0) {
                dialog.dismiss();
                String[] phone = new String[cphones.size()];
                String[] captions = new String[cphones.size()];
                int i = 0;
                for (CPhone cPhone : cphones) {
                    phone[i] = cPhone.phone;
                    captions[i] = cPhone.caption;
                    i++;
                }
                doCall(phone, captions);
                dialog.dismiss();
            }
        });
        emailButton.setOnButtonClickListener(action -> {
            if (null != emails && emails.length > 0) {
                dialog.dismiss();
                doMail(emails);
            }
        });
        websiteButton.setOnButtonClickListener(action -> {
            dialog.dismiss();
            doOpenSite(sites);
        });
    }

    private void showDepartsBottomSheet(final SCompany sCompany) {
        LinkedList<SCDepart> departs = sCompany.departs;
        int i = 0;
        SCDepart[] departsArray = new SCDepart[departs.size()];
        for (SCDepart depart : departs) {
            departsArray[i] = depart;
            i++;
        }

        final NBottomSheetDialog departsDialog = new NBottomSheetDialog(context);

        View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
        TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
        title.setText(context.getResources().getString(R.string.TITLE_SELECT_SEC_COMP_DEPART));

        ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
        listView.setDivider(null);

        SecCompDepartsBottomSheetAdapter secCompDepartsBottomSheetAdapter = new SecCompDepartsBottomSheetAdapter(context, R.layout.n_card_for_list, departsArray);
        listView.setAdapter(secCompDepartsBottomSheetAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showPRsBottomSheet(sCompany, ((SCDepart) parent.getItemAtPosition(position)).scrcs);
                departsDialog.dismiss();
            }
        });

        departsDialog.setContentView(bottomView);

        departsDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                FrameLayout bottomSheet =
                        departsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

                if (null != bottomSheet) {
                    BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                    behavior.setHideable(false);
                }
            }
        });

        departsDialog.show();
    }

    private void showPRsBottomSheet(final SCompany sCompany, LinkedList<SCRc> scRcs) {
        if (null != scRcs) {
            SCRc[] scRcsArray = new SCRc[scRcs.size()];
            int i = 0;
            if (null != scRcs && scRcs.size() > 0) {
                for (SCRc scRc : scRcs) {
                    scRcsArray[i] = scRc;
                    i++;
                }
            }
            final NBottomSheetDialog departsDialog = new NBottomSheetDialog(context);

            View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
            TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
            title.setText(context.getResources().getString(R.string.TITLE_SELECT_SEC_COMP_RC));

            ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
            listView.setDivider(null);

            SecCompRCsBottomSheetAdapter secCompRCsBottomSheetAdapter = new SecCompRCsBottomSheetAdapter(context, R.layout.n_card_for_list, scRcsArray);
            listView.setAdapter(secCompRCsBottomSheetAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    departsDialog.dismiss();

                    NDialog dialog = new NDialog(context, R.layout.n_dialog_companies_item);
                    ((TextView) dialog.findViewById(R.id.nDialogTitle)).setText(sCompany.caption);
                    NActionButton callButton = dialog.findViewById(R.id.nDialogButtonCall);
                    NActionButton emailButton = dialog.findViewById(R.id.nDialogButtonEmail);
                    NActionButton websiteButton = dialog.findViewById(R.id.nDialogButtonSite);
                    TextView messageView = dialog.findViewById(R.id.textMessage);
                    TextView addressView = dialog.findViewById(R.id.textAddress);
                    TextView phonesView = dialog.findViewById(R.id.textPhone);
                    TextView emailView = dialog.findViewById(R.id.textMail);
                    dialog.show();

                    String message = (sCompany.title != null ? sCompany.title : sCompany.message != null ? sCompany.message : sCompany.caption) + "\n\n";
                    final SCRc rc = (SCRc) parent.getItemAtPosition(position);
                    message += rc.caption + "\n";
                    message += rc.city != null ? rc.city + "\n" : "";
                    StringBuilder b = new StringBuilder(message);
                    message = (b.deleteCharAt(message.length() - 1)).toString();
                    messageView.setVisibility(View.VISIBLE);
                    messageView.setText(message);

                    setInfo(rc.phones, phonesView);

                    setInfo(rc.addresses, addressView);

                    setInfo(rc.emails, emailView);

                    if (null != rc.emails) {
                        emailButton.setVisibility(View.VISIBLE);
                        emailButton.setOnButtonClickListener(action -> {
                            if (null != rc.emails && rc.emails.length > 0) {
                                dialog.dismiss();
                                doMail(rc.emails);
                            }
                        });
                    }

                    if (null != rc.phones) {
                        callButton.setVisibility(View.VISIBLE);
                        callButton.setOnButtonClickListener(action -> {
                            if (null != rc.phones && rc.phones.length > 0) {
                                dialog.dismiss();
                                doCall(rc.phones, null);
                            }
                        });
                    }

                    if (null != rc.sites) {
                        websiteButton.setVisibility(View.VISIBLE);
                        websiteButton.setOnButtonClickListener(action -> {
                            dialog.dismiss();
                            doOpenSite(rc.sites);
                        });
                    }
                }
            });

            departsDialog.setContentView(bottomView);

            departsDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    FrameLayout bottomSheet = departsDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

                    if (null != bottomSheet) {
                        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                        behavior.setHideable(false);
                    }
                }
            });

            departsDialog.show();
        }
    }

    private boolean setInfo(String[] strings, TextView textView) {
        if (null != strings && strings.length > 0) {
            String text = "";
            for (String phone : strings)
                text += phone + "\n";
            StringBuilder buffer = new StringBuilder(text);
            text = (buffer.deleteCharAt(text.length() - 1)).toString();
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
            return true;
        }
        return false;
    }

    private void doOpenSite(String[] sites) {
        if (sites.length == 1) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sites[0]));
            context.startActivity(intent);
        } else {
            final NBottomSheetDialog siteDialog = new NBottomSheetDialog(context);

            View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_small, null);
            TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
            title.setText(R.string.SELECT_SITE);

            ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
            listView.setDivider(null);

            SecCompBottomListAdater secCompBottomListAdater = new SecCompBottomListAdater(context, R.layout.n_card_for_list, sites, null);
            listView.setAdapter(secCompBottomListAdater);

            listView.setOnItemClickListener((parent, view, position, id) -> {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(parent.getItemAtPosition(position).toString()));
                context.startActivity(intent);
                siteDialog.dismiss();
            });
            siteDialog.setContentView(bottomView);

            try {
                Field mBehaviorField = siteDialog.getClass().getDeclaredField("mBehavior");
                mBehaviorField.setAccessible(true);

                final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(siteDialog);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            siteDialog.show();
        }
    }

    private void doMail(String[] emails) {
        if (emails.length == 1) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.parse("mailto:?to=" + emails[0]);
            intent.setData(data);
            context.startActivity(intent);
        } else {
            final NBottomSheetDialog mailDialog = new NBottomSheetDialog(context);

            View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_small, null);
            TextView title = bottomView.findViewById(R.id.dialogBottomTitle);
            title.setText(R.string.SELECT_MAIL);

            ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
            listView.setDivider(null);

            SecCompBottomListAdater secCompBottomListAdater = new SecCompBottomListAdater(context, R.layout.n_card_for_list, emails, null);
            listView.setAdapter(secCompBottomListAdater);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String mail = parent.getItemAtPosition(position).toString();
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri data = Uri.parse("mailto:?to=" + mail);
                    intent.setData(data);
                    context.startActivity(intent);
                    mailDialog.dismiss();
                }
            });
            mailDialog.setContentView(bottomView);

            try {
                Field mBehaviorField = mailDialog.getClass().getDeclaredField("mBehavior");
                mBehaviorField.setAccessible(true);

                final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(mailDialog);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    }
                });
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            mailDialog.show();
        }
    }

    private void doCall(String[] phones, String[] captions) {
        if (null != phones && phones.length > 0) {
            if (phones.length == 1) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
                if (telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
                    Func.nShowMessage(context, context.getString(R.string.ERROR_CANT_MAKE_CALL_HARDWARE));
                } else {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(context.getString(R.string.TEL) + " " + phones[0]));
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 101);
                        return;
                    }
                    context.startActivity(intent);
                }
            } else {
                final NBottomSheetDialog callDialog = new NBottomSheetDialog(context);

                View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_small, null);
                TextView title = bottomView.findViewById(R.id.dialogBottomTitle);
                title.setText(R.string.WIZ_ENTER_ZONE_NUMBER);

                ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
                listView.setDivider(null);

                SecCompBottomListAdater secCompBottomListAdater = new SecCompBottomListAdater(context, R.layout.n_card_for_list, phones, captions);
                listView.setAdapter(secCompBottomListAdater);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String phone = parent.getItemAtPosition(position).toString();

                        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
                        if (telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
                            Func.nShowMessage(context, context.getString(R.string.ERROR_CANT_MAKE_CALL_HARDWARE));
                        } else {
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(context.getString(R.string.TEL) + " " + phone));
                            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, 101);
                                return;
                            }
                            context.startActivity(intent);
                        }

                        callDialog.dismiss();
                    }
                });

                callDialog.setContentView(bottomView);
                try {
                    Field mBehaviorField = callDialog.getClass().getDeclaredField("mBehavior");
                    mBehaviorField.setAccessible(true);

                    final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(callDialog);
                    behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                        @Override
                        public void onStateChanged(@NonNull View bottomSheet, int newState) {
                            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                            }
                        }

                        @Override
                        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                        }
                    });
                } catch (NoSuchFieldException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

                callDialog.show();
            }
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}
