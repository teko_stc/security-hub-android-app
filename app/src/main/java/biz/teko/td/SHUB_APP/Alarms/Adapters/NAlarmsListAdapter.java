package biz.teko.td.SHUB_APP.Alarms.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Alarms.Activities.AlarmActivity;
import biz.teko.td.SHUB_APP.D3DB.Alarm;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NEAListElement;

public class NAlarmsListAdapter extends ArrayAdapter<Alarm>
{
	private final Context context;
	private final int layout;
	private final LinkedList<Alarm> alarms;
	private final DBHelper dbHelper;

	public NAlarmsListAdapter(@NonNull Context context, int resource, LinkedList<Alarm> alarms)
	{
		super(context, resource);
		this.context = context;
		this.layout = resource;
		this.alarms = alarms;
		this.dbHelper = DBHelper.getInstance(context);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		NEAListElement view = (NEAListElement) convertView;
		if(null==view)
		{
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = (NEAListElement) layoutInflater.inflate(layout, parent, false);
		}
		Alarm alarm = alarms.get(position);
		if(null!=alarm){
			view.setLocation(dbHelper.getSiteNameByDeviceSection(alarm.device, 0));
			Device device = dbHelper.getDeviceById(alarm.device);
			if(null!=device){
				view.setTitle(device.getName() + " " + device.account);
			};
		}
		//		view.bindView();
		view.setOnElementClickListener(new NEAListElement.OnElementClickListener()
		{
			@Override
			public void onClick()
			{
				openAlarmActivity(alarm.id);
			}
		});
		return view;
	}

	private void openAlarmActivity(int id)
	{
		Intent intent = new Intent(context, AlarmActivity.class);
		intent.putExtra("alarm", id);
		((Activity)context).startActivityForResult(intent, Const.REQUEST_SHOW_LIST_ELEMENT);
		((Activity)context).overridePendingTransition(R.animator.enter, R.animator.exit);
	}

	public int getCount(){
		return alarms.size();
	}

	public Alarm getItem(int position){
		return alarms.get(position);
	}

	public  long getItemId(int position){
		return  position;
	}
}
