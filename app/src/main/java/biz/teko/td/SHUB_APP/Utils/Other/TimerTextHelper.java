package biz.teko.td.SHUB_APP.Utils.Other;

import android.os.Handler;

import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;

public class TimerTextHelper implements Runnable {
	private final Handler handler = new Handler();
	private final NActionButton nActionButton;
	private volatile long startTime;
	private volatile long elapsedTime;

	public TimerTextHelper(NActionButton nActionButton) {
		this.nActionButton = nActionButton;
	}

	@Override
	public void run() {
		long millis = System.currentTimeMillis() - startTime;
		int seconds = (int) (millis / 1000);
		int minutes = seconds / 60;
		seconds = seconds % 60;

		nActionButton.setTitle(String.format("%d:%02d", minutes, seconds));

		if (elapsedTime == -1) {
			handler.postDelayed(this, 500);
		}
	}

	public void start() {
		this.startTime = System.currentTimeMillis();
		this.elapsedTime = -1;
		handler.post(this);
	}

	public void stop() {
		this.elapsedTime = System.currentTimeMillis() - startTime;
		nActionButton.setTitle("");
		handler.removeCallbacks(this);
	}

	public long getElapsedTime() {
		return elapsedTime;
	}
}
