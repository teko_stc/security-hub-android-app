package biz.teko.td.SHUB_APP.MainTabs.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import biz.teko.td.SHUB_APP.MainTabs.Fragments.MainSetFragment;

public class MainSetPagerAdapter extends FragmentPagerAdapter
{
	private final boolean shared;
	public static final int MAIN_SET_PAGER_COUNT = 4;
	private final int site_id;
	private final MainSetFragment.OnSelectListener onSelectListener;

	public MainSetPagerAdapter(FragmentManager fm, boolean shared, int site_id, MainSetFragment.OnSelectListener onSelectListener) {
		super(fm);
		this.shared = shared;
		this.site_id = site_id;
		this.onSelectListener = onSelectListener;
	}

	@Override
	public Fragment getItem(int i)
	{
		MainSetFragment mainSetFragment = MainSetFragment.newInstance(i, site_id, shared);
		mainSetFragment.setSelectListener(onSelectListener);
		return mainSetFragment;
	}

	@Override
	public int getCount()
	{
		return MAIN_SET_PAGER_COUNT;
	}

	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}

	@Override
	public int getItemPosition(@NonNull Object object)
	{
		return POSITION_NONE;
	}
}
