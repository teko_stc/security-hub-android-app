package biz.teko.td.SHUB_APP.Utils.NAdapters;

import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NMainBarElement;

public class DetectorGridHolder extends RecyclerView.ViewHolder{

	public DetectorGridHolder(NMainBarElement nMainBarElement)
	{
		super(nMainBarElement);
	}

	public static DetectorGridHolder newInstance(ViewGroup parent) {
		return new DetectorGridHolder(new NMainBarElement(parent.getContext(), R.layout.n_zones_grid_element_view));
	}

	public void reset(){
		itemView.findViewById(R.id.innerFrame).setVisibility(View.VISIBLE);
		itemView.findViewById(R.id.nTextTitle).setVisibility(View.VISIBLE);
		((NMainBarElement)itemView).resetStates();
	}
	public void hide(){
		itemView.findViewById(R.id.innerFrame).setVisibility(View.GONE);
		itemView.findViewById(R.id.nTextTitle).setVisibility(View.GONE);
	}
}