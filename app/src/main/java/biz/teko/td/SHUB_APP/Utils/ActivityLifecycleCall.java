package biz.teko.td.SHUB_APP.Utils;

import android.app.Activity;
import android.app.Application;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.Events.Activities.NFullScreenNotifActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class ActivityLifecycleCall implements Application.ActivityLifecycleCallbacks {
    private static final String LOG_TAG = "D3Activity";
    private static int openCount = 0;
    private static int activeCount = 0;
    private static boolean isFullNotifActivityLaunch = false;

    public static int getOpenCount() {
        return openCount;
    }

    public static int getActiveCount() {
        return activeCount;
    }


    public static boolean getIsFullNotifActivityLaunch() {
        return isFullNotifActivityLaunch;
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {
        Func.log_v(LOG_TAG, "Activity created " + activity.getLocalClassName());
        if (activity.getLocalClassName().equals(NFullScreenNotifActivity.class.getName()))
        {
            isFullNotifActivityLaunch = true;
        }else{
            activeCount ++;
            if (activeCount == Integer.MAX_VALUE) activeCount = 1;
        }
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        Func.log_v(LOG_TAG, "Activity start " + activity.getLocalClassName());
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        Func.log_v(LOG_TAG, "Activity resumed " + activity.getLocalClassName());
        if (!activity.getLocalClassName().equals(NFullScreenNotifActivity.class.getName())
//        && !(activity.getLocalClassName().equals(MainActivity.class.getName()) && ((KeyguardManager) App.getContext().getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE)).isKeyguardLocked())
        )
        {
            if(openCount == 0){
                if(!((KeyguardManager) App.getContext().getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE)).isKeyguardLocked()
                 && ((PowerManager) App.getContext().getSystemService(Context.POWER_SERVICE)).isInteractive()){
                    //сбрасываем все процессы уведомления будильника при открытии приложения если экран разблокирован и активен
                    App.getContext().sendBroadcast(new Intent(D3Service.BROADCAST_STOP_ALARM_NOTIFICATION));
                }
            }
            openCount++;
            if (openCount == Integer.MAX_VALUE) openCount = 1;
        }
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        Func.log_v(LOG_TAG, "Activity paused " + activity.getLocalClassName());
        if (!activity.getLocalClassName().equals(NFullScreenNotifActivity.class.getName()))
        {
            if(openCount > 0) openCount--;
        }
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
        Func.log_v(LOG_TAG, "Activity stoped " + activity.getLocalClassName());
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        Func.log_v(LOG_TAG, "Activity destroyed " + activity.getLocalClassName());
        if (activity.getLocalClassName().equals(NFullScreenNotifActivity.class.getName()))
        {
            isFullNotifActivityLaunch = false;
        }else{
            if(activeCount>0) activeCount--;
        }
    }
}