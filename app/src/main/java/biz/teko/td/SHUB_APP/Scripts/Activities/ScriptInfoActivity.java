package biz.teko.td.SHUB_APP.Scripts.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;

public class ScriptInfoActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private SharedPreferences sp;
	private String iso;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_script_description);
		context = this;
		dbHelper = DBHelper.getInstance(context);
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));

		int scriptId = getIntent().getIntExtra("script", 0);
		final int deviceId = getIntent().getIntExtra("device_id", -1);
		final int siteId = getIntent().getIntExtra("site_id", -1);
		final int bind = getIntent().getIntExtra("bind", -1);

//		Cursor c = dbHelper.getAllLibraryScriptsCursor();
//		String[] c1 = dbHelper.getAllScriptsCategoriesByConfigVersion();
		final Script script = dbHelper.getLibraryScriptByLibId(scriptId);

		ImageView backButton = (ImageView) findViewById(R.id.scriptCloseButton);
		backButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		if(null!=script){
			TextView scriptName = (TextView) findViewById(R.id.scriptDescNameTitle);
			scriptName.setText(Func.getScriptDataForCurrLang(script.nameLib, iso));
			TextView scriptDesc = (TextView) findViewById(R.id.scriptDescMess);
			scriptDesc.setText(Html.fromHtml(Func.getScriptDataForCurrLang(script.description, iso)));
		}

		TextView setButton = (TextView) findViewById(R.id.scriptSetButton);
		setButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Intent intent = new Intent(getBaseContext(), WizardScriptSetActivity.class);
				intent.putExtra("script_id", script.id);
				intent.putExtra("lib_script_id", script.libId);
				intent.putExtra("device_id", deviceId);
				intent.putExtra("site_id", siteId);
				intent.putExtra("bind", bind);
				intent.putExtra("target", 0);
				overridePendingTransition(R.animator.enter, R.animator.exit);
				startActivityForResult(intent, D3Service.ADD_NEW_SCRIPT);
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.ADD_NEW_SCRIPT){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
}
