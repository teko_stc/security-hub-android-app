package biz.teko.td.SHUB_APP.SecCompanies.Models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

public class SCDepart
{

	@Attribute(name = "subject")
	public String subject;

	@Attribute(name = "center")
	public String center;

	@ElementList(name = "rcs", entry = "rc", required = false)
	public LinkedList<SCRc> scrcs;
}
