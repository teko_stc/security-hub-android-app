package biz.teko.td.SHUB_APP.D3;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import java.util.Locale;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;

public class D3Tts extends TextToSpeech
{

	private static final String LOG_TAG_TTS = "_TTS";
	private static D3Tts instance;
	private int id;

	public static D3Tts getInstance(final Context context){
		if(null == instance)
		{
			instance = newInstance(context);
		}
		return instance;
	}

	public static D3Tts updateInstance(Context context){
		if(null!=instance) instance.stop();
		instance = newInstance(context);
		return instance;
	}

	private static D3Tts newInstance(Context context)
	{
		return new D3Tts(context, status -> {
			// TODO Auto-generated method stub
			if (status == TextToSpeech.SUCCESS)
			{
				int result = instance.setLanguage(context);
				if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED)
				{
					Func.log_e(D3Service.LOG_TAG, "This Language is not supported");
				}
			} else Func.log_e(D3Service.LOG_TAG, "Initilization Failed!");
		});
	}

	private D3Tts(Context context, OnInitListener listener)
	{
		super(context, listener);
	}

	public void speak(String mess){
		if(null == mess || mess.equals(""))
		{
			mess = "Content not available";
			instance.speak(mess, TextToSpeech.QUEUE_ADD, null, getId());
		}else
			instance.speak(mess, TextToSpeech.QUEUE_ADD, null, getId());
		Func.log_d(D3Service.LOG_TAG + LOG_TAG_TTS, "TTS: " + mess);
	}

	private String getId()
	{
		return id < 1000000 ? Integer.toString(id + 1) : Integer.toString(1);
	}

	public int setLanguage(Context context)
	{
		String localeId = LocaleHelper.getLanguage(context);
		Locale locale = Locale.getDefault();
		if(null!= localeId)
		{
			locale=  new Locale(localeId);
		}
		Func.log_d(D3Service.LOG_TAG + LOG_TAG_TTS, locale.getDisplayName());
		return instance.setLanguage(locale);
	}

}
