package biz.teko.td.SHUB_APP.SectionsTabs.Adapters;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import biz.teko.td.SHUB_APP.SectionsTabs.Fragments.SectionsHistoryTabFragment;
import biz.teko.td.SHUB_APP.SectionsTabs.Fragments.SectionsTabFragment;

/**
 * Created by td13017 on 10.02.2017.
 */

public class SectionsViewPagerAdapter extends FragmentStatePagerAdapter
{
	private final int siteId, deviceId;
	private CharSequence Titles[];
	private int NumOfTabs;
	private SectionsTabFragment sectionTFragment;
	private SectionsHistoryTabFragment sectionHFragment;

	public SectionsViewPagerAdapter(FragmentManager fm, CharSequence titles[], int num, int siteId, int deviceId)
	{
		super(fm);
		this.Titles = titles;
		this.NumOfTabs = num;
		this.siteId = siteId;
		this.deviceId = deviceId;
	}

	@Override
	public Fragment getItem(int position)
	{
		if(position == 0){
			return SectionsTabFragment.newInstance(siteId, deviceId);
		}else{
			return SectionsHistoryTabFragment.newInstance(siteId, deviceId);
		}
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		return Titles[position];
	}

	@Override
	public int getCount()
	{
		return NumOfTabs;
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
		// save the appropriate reference depending on position
		switch (position) {
			case 0:
				sectionTFragment = (SectionsTabFragment) createdFragment;
				break;
			case 1:
				sectionHFragment = (SectionsHistoryTabFragment) createdFragment;
				break;
		}
		return createdFragment;
	}

	public void closeFab(){
		sectionTFragment.closeFABMenu();
	}
}
