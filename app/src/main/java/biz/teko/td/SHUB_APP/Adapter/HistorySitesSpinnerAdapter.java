package biz.teko.td.SHUB_APP.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 27.07.2017.
 */

public class HistorySitesSpinnerAdapter extends ArrayAdapter<Site>
{
	private final Site[] sites;
	private final Context context;
	private final int curSite;
	private final LayoutInflater layoutInflater;

	public HistorySitesSpinnerAdapter(Context context, int resource, Site[] sites, int curSite)
	{
		super(context, resource);
		this.context = context;
		this.sites = sites;
		this.curSite = curSite;
		this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount(){
		return sites.length;
	}

	public Site getItem(int position){
		return sites[position];
	}

	public long getItemId(int position){
		return position;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		Site site = sites[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(site.name);
		if(curSite ==  site.id){
			textView.setTextColor(context.getResources().getColor(R.color.brandColorAccentColor));
		}else{
			textView.setTextColor(context.getResources().getColor(R.color.n_text_dark_grey));
		}
		return view;
	}

}
