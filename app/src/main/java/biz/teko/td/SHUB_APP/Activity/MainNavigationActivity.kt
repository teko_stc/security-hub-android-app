package biz.teko.td.SHUB_APP.Activity

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import biz.teko.td.SHUB_APP.Cameras.Fragments.NCamerasListFragment
import biz.teko.td.SHUB_APP.Devices_N.Fragments.DevicesListFragment
import biz.teko.td.SHUB_APP.Events.NEventsFragment
import biz.teko.td.SHUB_APP.MainTabs.Fragments.MainFragment
import biz.teko.td.SHUB_APP.Profile.Fragments.ProfileFragment
import biz.teko.td.SHUB_APP.R
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptsListActivity
import biz.teko.td.SHUB_APP.Utils.Dir.Const
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomNavigationView
import biz.teko.td.SHUB_APP.Utils.Screen
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import kotlin.reflect.KClass


class MainNavigationActivity: MainNavigation() {

    private val fragmentMap = mutableMapOf<KClass< out Fragment>,Fragment>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)

        val tutorialState = intent.getBooleanExtra(Const.IS_FIRST_TUTORIAL, true)
        isFirstTutorial = tutorialState
        when(intent.getSerializableExtra(Const.SCREEN_NAME)) {
            Screen.DEVICES -> {
                setBottomNavigation(R.id.bottom_navigation_item_devices, this)
                openNewDevices()
            }
            else -> {
                setBottomNavigation(R.id.bottom_navigation_item_favorited, this)
                openMain()
            }
        }
    }


    override fun setBottomNavigation(currentId: Int, context: Context?) {
        bottomNavigation = findViewById(R.id.nBottomMenu) as NBottomNavigationView?
        with(bottomNavigation) {
            inflateMenu(R.menu.bottom_menu)
            menu.findItem(currentId).apply {
                isChecked = true
            }
            val ob: Observable<MenuItem> = Observable.create { emitter ->
                setOnNavigationItemSelectedListener {
                    emitter.onNext(it)
                    true
                }

            }
            ob.debounce(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread()).subscribe {
                val itemId = it.itemId
                if (prevItem != itemId) {
                    when (itemId) {
                        R.id.bottom_navigation_item_favorited -> openMain()
                        R.id.bottom_navigation_item_devices -> openNewDevices()
                        R.id.bottom_navigation_item_messages -> openHistory()
                        R.id.bottom_navigation_item_cameras -> openCameras()
                        R.id.bottom_navigation_item_menu -> openProfile()
                    }
                    setBottomNavigationItem(itemId)
                    prevItem = itemId
                    overridePendingTransition(0, 0)
                }
            }
        }
        setBottomInfoBehaviour(bottomNavigation, findViewById(android.R.id.content), context)
    }



    override fun onDestroy() {
        fragmentMap.clear()
        super.onDestroy()
    }



    override fun openHistory() {
        navigateToFragment(NEventsFragment::class) {
            NEventsFragment(this)
        }
    }

    override fun openScripts() {
        startActivity(Intent(baseContext, ScriptsListActivity::class.java))
        finish()
    }

    override fun openMain() {
        navigateToFragment(MainFragment::class){
            MainFragment(this)
        }
    }

    override fun openNewDevices() {
        navigateToFragment(DevicesListFragment::class){
            DevicesListFragment()
        }
    }

    override fun openCameras() {
        navigateToFragment(NCamerasListFragment::class){
            NCamerasListFragment(this)
        }
    }

    override fun openProfile() {
        navigateToFragment(ProfileFragment::class){
            ProfileFragment(this)
        }
    }

    private fun <F : Fragment> navigateToFragment(klass: KClass<F>, constructor: ()-> F){
        if (!fragmentMap.containsKey(klass)){
            fragmentMap[klass] = constructor()
        }
        navigate(fragmentMap[klass] ?: constructor())
    }

    private fun navigate(fragment: Fragment){
        val transaction: FragmentTransaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.nav_host, fragment )
        transaction.commit()
        when (fragment) {
            is MainFragment -> R.id.bottom_navigation_item_favorited
            is DevicesListFragment -> R.id.bottom_navigation_item_devices
            is NEventsFragment -> R.id.bottom_navigation_item_messages
            is NCamerasListFragment -> R.id.bottom_navigation_item_cameras
            is ProfileFragment -> R.id.bottom_navigation_item_menu
            else -> null
        }?.let { setBottomNavigationItem(it) }
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            val v = getCurrentFocus();
            if (v is EditText) {
                val outRect = Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains(event.getRawX().toInt(), event.getRawY().toInt())) {
                    v.clearFocus();
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager;
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

}