package biz.teko.td.SHUB_APP.D3.D3XProto;

import org.simpleframework.xml.Attribute;

public  class Statement
{
	@Attribute(name="id")
	public int id;

	@Attribute(name="caption")
	public String caption;

	public Statement() {}
}
