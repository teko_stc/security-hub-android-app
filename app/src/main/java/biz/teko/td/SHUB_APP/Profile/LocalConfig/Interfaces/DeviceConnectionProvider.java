package biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces;

import biz.teko.td.SHUB_APP.UDP.SocketConnection;

public interface DeviceConnectionProvider {
    SocketConnection provideSocketConnection();
    void closeSocketConnection();
}
