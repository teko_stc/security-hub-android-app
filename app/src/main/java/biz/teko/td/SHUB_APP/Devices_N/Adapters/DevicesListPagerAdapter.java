package biz.teko.td.SHUB_APP.Devices_N.Adapters;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import biz.teko.td.SHUB_APP.Devices_N.Fragments.DevicesListInnerFragment;
import biz.teko.td.SHUB_APP.Devices_N.Fragments.Filter;
import biz.teko.td.SHUB_APP.Devices_N.Fragments.SectionsListFragment;

public class DevicesListPagerAdapter extends FragmentPagerAdapter {
	private final CharSequence[] titles;
	private final int numOfTabs;
	private Filter filter = new Filter("");
	private int siteId;
	private DevicesListInnerFragment devicesListInnerFragment;
	private SectionsListFragment sectionsListFragment;

	public Filter getFilter() {
		return filter;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
		if (getDevicesListFragment() != null && getSectionsListFragment() != null) {
			getDevicesListFragment().setFilter(filter);
			getSectionsListFragment().setFilter(filter);
		}
	}

	public DevicesListPagerAdapter(@NonNull FragmentManager fm, CharSequence[] titles, int numoftabs, int siteId) {
		super(fm);
		this.titles = titles;
		this.numOfTabs = numoftabs;
		this.siteId = siteId;
	}

	public int getSiteId() {
		return siteId;
	}

	public void setSiteId(int siteId) {
		this.siteId = siteId;
		if (getDevicesListFragment() != null) {
			getDevicesListFragment().setSiteId(siteId);
			getDevicesListFragment().updateContents();
		}
		if (getSectionsListFragment() != null) {
			getSectionsListFragment().setSiteId(siteId);
			getSectionsListFragment().updateContents();
		}
	}

	@Override
	public Fragment getItem(int position) {
		if (position == 0) {
			devicesListInnerFragment = DevicesListInnerFragment.newInstance(siteId, filter);
			return devicesListInnerFragment;
		} else {
			sectionsListFragment = SectionsListFragment.newInstance(siteId, filter);
			return sectionsListFragment;
		}
	}

	@Override
	public int getCount()
	{
		return numOfTabs;
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		return titles[position];
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
		// save the appropriate reference depending on position
		switch (position) {
			case 0:
				devicesListInnerFragment = (DevicesListInnerFragment) createdFragment;
				break;
			case 1:
				sectionsListFragment = (SectionsListFragment) createdFragment;
				break;
		}
		return createdFragment;
	}

	public SectionsListFragment getSectionsListFragment() {
		return sectionsListFragment;
	}

	public DevicesListInnerFragment getDevicesListFragment() {
		return devicesListInnerFragment;
	}
}
