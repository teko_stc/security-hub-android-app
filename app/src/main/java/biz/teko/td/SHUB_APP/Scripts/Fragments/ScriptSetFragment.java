package biz.teko.td.SHUB_APP.Scripts.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.TextStyle;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.Cameras.Adapters.ZonesListAdapter;
import biz.teko.td.SHUB_APP.Codes.Adapters.UserAddSectionsListAdapter;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Activities.WizardScriptSetActivity;
import biz.teko.td.SHUB_APP.Scripts.Adapters.SectionsListAdapter;
import biz.teko.td.SHUB_APP.Scripts.Adapters.TimeZonesListAdapter;
import biz.teko.td.SHUB_APP.Scripts.Adapters.ZoneCheckArrayAdapter;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.PresetRadioGroup;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.PresetValueButton;

public class ScriptSetFragment extends Fragment
{
	private static final int REQ_RESULT_SELECTOR = 10234;
	private int position;
	private Context context;
	private WizardScriptSetActivity activity;
	private DBHelper dbHelper;
	private ViewGroup container;
	private int scriptId;
	private Script script;
	private SharedPreferences sp;
	private String iso;
	private int deviceId;
	private View v;
	private final String regexFloat = "^([1-9]|0|\\.)$";

	private int bind = -1;
	private JSONObject paramSentJSON;
//	private JSONArray paramArray;
	private HashMap<String, JSONObject> map;
	private TextView timeTextView;
	private int siteId;
	private int curSelect = -1;
	private UserAddSectionsListAdapter userAddSectionsListAdapter;
	private View buttonLayout;
	private View textLayout;
	private TextView selectorTextView;
	private int libScriptId;
	private ZoneCheckArrayAdapter zoneAddSectionsListAdapter;
	private ProgressBar progressBar;
	private TextView buttonNext;
	private boolean handle;
	private Button buttonDatePicker;
	private TextView timeChangeTextView;

	public static ScriptSetFragment getInstance(int position, int script_id, int scriptLibId, int device_id, int site_id, int bind){
		ScriptSetFragment scriptSetFragment = new ScriptSetFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("position", position);
		bundle.putInt("script_id", script_id);
		bundle.putInt("lib_script_id", scriptLibId);
		bundle.putInt("device_id", device_id);
		bundle.putInt("site_id", site_id);
		bundle.putInt("bind", bind);
		scriptSetFragment.setArguments(bundle);
		return scriptSetFragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		position = getArguments().getInt("position", 0);
		scriptId = getArguments().getInt("script_id", 0);
		libScriptId = getArguments().getInt("lib_script_id", 0);
		deviceId = getArguments().getInt("device_id", -1);
		siteId = getArguments().getInt("site_id", -1);
		bind = getArguments().getInt("bind", -1);

		context = container.getContext();
		this.container = container;
		activity = (WizardScriptSetActivity)context;
		dbHelper = DBHelper.getInstance(context);
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));

		if(0!= scriptId)
		{
			script = dbHelper.getFullDeviceScriptByScriptId(scriptId);
		}else{
			script = dbHelper.getLibraryScriptByLibId(libScriptId);
		}

		bind = ((WizardScriptSetActivity)context).getSelBind();
		v = inflater.inflate(R.layout.fragment_script_set, container, false);
		setFrameConsts(v);

		switch (position){
			case 0:
				setBindFragment(v, position);
				break;
			default:
				setParamsFragment(v, position);
				break;
		}
		return v;
	}

	@Override
	public void onStop()
	{
		handle = false;
		super.onStop();
	}

	private void setBindFragment(View v, int position)
	{
		LinearLayout paramsLinear = (LinearLayout) v.findViewById(R.id.scriptParamsLinear);
		LinearLayout bindFrame = (LinearLayout) v.findViewById(R.id.scriptBindFrame);
		if(null!=paramsLinear) paramsLinear.setVisibility(View.GONE);
		if(null!=bindFrame) bindFrame.setVisibility(View.VISIBLE);

		LinearLayout scriptBindLinear = (LinearLayout) v.findViewById(R.id.scriptBindLinear);
		LinearLayout scriptEditLinear = (LinearLayout) v.findViewById(R.id.scriptBindEditLinear);
		TextView scriptEdit = (TextView) v.findViewById(R.id.scriptBindEdit);
		LinearLayout scriptBindButton = (LinearLayout) v.findViewById(R.id.scriptBindButton);

		if(-1 == bind){
			if(null!=scriptBindButton)scriptBindButton.setVisibility(View.VISIBLE);
			if(null!=scriptEditLinear)scriptEditLinear.setVisibility(View.GONE);
		}else{
			if(null!=scriptBindButton)scriptBindButton.setVisibility(View.GONE);
			if(null!=scriptEditLinear)scriptEditLinear.setVisibility(View.VISIBLE);
			enableNext(true);
			scriptEdit.setText(dbHelper.getZoneNameById(deviceId, bind));
		}
		if(null!=scriptBindLinear && ((WizardScriptSetActivity)context).getTarget() == WizardScriptSetActivity.Target.SET){
			scriptBindLinear.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					showBindDialog();
				}
			});
		}else{
			TextView pushToSetText = (TextView) v.findViewById(R.id.wizardScriptPushToSetText);
			if(null!=pushToSetText) pushToSetText.setVisibility(View.GONE);
			TextView scriptBindTitle = (TextView) v.findViewById(R.id.wizScriptBindTitle);
			scriptBindTitle.setText(R.string.WIZ_BINDED_RELAY);
		}
	}

	private void showBindDialog()
	{
		new BottomSheetDialogForBind().newInstance(deviceId, bind).show(getFragmentManager(), "tagRelay");
	}

	public static class BottomSheetDialogForBind extends BottomSheetDialogFragment
	{
		private Context context;
		private DBHelper dbHelper;
		private int deviceId;
		private int bind;

		public static BottomSheetDialogForBind newInstance(int deviceId, int bind){
			BottomSheetDialogForBind bottomSheetDialogForBind = new BottomSheetDialogForBind();
			Bundle b = new Bundle();
			b.putInt("device_id", deviceId);
			b.putInt("bind", bind);
			bottomSheetDialogForBind.setArguments(b);
			return bottomSheetDialogForBind;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState)
		{
			context = getContext();
			dbHelper = DBHelper.getInstance(context);
			deviceId = getArguments().getInt("device_id", -1);
			bind = getArguments().getInt("bind", -1);

			final BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

			View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
			TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
			title.setText(R.string.SCRIPT_SET_CHOOSE_RELAY);
			ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
			listView.setDivider(null);
			Zone[] relays = dbHelper.getAllAutoRelaysByDeviceId(deviceId);
			if(null!=relays) {
				ZonesListAdapter zoneslistAdapter = new ZonesListAdapter(context, R.layout.n_card_for_list, relays, bind);
				listView.setAdapter(zoneslistAdapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id)
					{
						Zone relay = (Zone) parent.getItemAtPosition(position);
						((WizardScriptSetActivity) context).setSelBind(relay.id);
						bottomSheetDialog.dismiss();
					}
				});
			}

			bottomSheetDialog.setContentView(bottomView);

			try {
				Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
				mBehaviorField.setAccessible(true);

				final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
				behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
					@Override
					public void onStateChanged(@NonNull View bottomSheet, int newState) {
						if (newState == BottomSheetBehavior.STATE_DRAGGING) {
							behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
						}
					}

					@Override
					public void onSlide(@NonNull View bottomSheet, float slideOffset) {
					}
				});
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

			return bottomSheetDialog;
		}
	}

	private void setParamsFragment(View v, int position)
	{

		map = activity.getParamArray(position);
		if(null == map){
			 map = new HashMap<String, JSONObject>();
		}
//		paramArray = new JSONArray();

		LinearLayout paramsLinear = (LinearLayout) v.findViewById(R.id.scriptParamsLinear);
		LinearLayout bindFrame = (LinearLayout) v.findViewById(R.id.scriptBindFrame);
		if(null!=paramsLinear) paramsLinear.setVisibility(View.VISIBLE);
		if(null!=bindFrame) bindFrame.setVisibility(View.GONE);


		try
		{
			LinearLayout paramFrame = (LinearLayout) v.findViewById(R.id.scriptParamsFrame);
			JSONObject paramObject = (new JSONArray(script.paramsLib)).getJSONObject(position - 1);
			addParamView(paramFrame, paramObject);
			if(((WizardScriptSetActivity) context).getAnim()){
				Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);
				paramFrame.startAnimation(fadeIn);
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}



	}

	private void addParamView(LinearLayout paramFrame, JSONObject paramObject)
	{
		addDescription(paramFrame, paramObject);
		addInput(paramFrame, paramObject);
	}

	//DESCRIPTION
	private void addDescription(LinearLayout paramFrame, JSONObject paramObject)
	{
		LinearLayout paramFrame2 = (LinearLayout) getLayoutInflater().inflate(R.layout.template_linear_match_wrap, null);
		paramFrame2.setGravity(Gravity.START);

		String title = Func.getS(paramObject, "description");
		if(null!=title){
			TextView  titleText = (TextView) getLayoutInflater().inflate(R.layout.template_description_text, null);
			titleText.setText(Func.getScriptDataForCurrLang(title, iso) + ":");
			paramFrame2.addView(titleText);
		}
		paramFrame.addView(paramFrame2);
	}

	//INPUT
	private void addInput(LinearLayout paramFrame, JSONObject paramObject)
	{
		String type = Func.getS(paramObject, "type");
		if(null!=type)
		{
			switch (type){
				case "int":
				case "bool":
				case "float":
				case "xfloat":
				case "duration_s":
				case "duration_m":
					addEdit(paramFrame, paramObject, type);
					break;
				case "time":
					addTime(paramFrame, paramObject, type);
					break;
				case "zone":
				case "zones":
				case "section":
				case "sections":
				case "time_zone":
				case "time_zone_m":
					addSelector(paramFrame, paramObject, type);
				case "group":
				case "time_utc_group":
					addGroup(paramFrame, paramObject);
					break;
				case "bit_mask":
//					decrecated
// 					addBitMask();
					break;
				case "list":
					addListSelector(paramFrame, paramObject);
					break;
				case "out":
					break;
				case "time_m_utc":
					addTimePicker(paramFrame, paramObject, type);
					break;
				default:
					break;
			}
		}else{
			addEdit(paramFrame, paramObject, type);
		}
	}

	//LIST
	private void addListSelector(LinearLayout paramFrame, final JSONObject paramObject)
	{
		int checkedValue  = -1;
		PresetRadioGroup presetRadioGroup = new PresetRadioGroup(context);
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		presetRadioGroup.setLayoutParams(layoutParams);
		presetRadioGroup.setOrientation(LinearLayout.VERTICAL);
		presetRadioGroup.setPadding(0, Func.dpToPx(12, context), 0, 0);

		presetRadioGroup.setOnCheckedChangeListener(new PresetRadioGroup.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(View radioGroup, View radioButton, boolean isChecked, int checkedId)
			{
				if(null!=radioButton && isChecked){
					int value = ((PresetValueButton) radioButton).getValue();
					mapParam(Func.getS(paramObject, "name"), String.valueOf(value));
					enableNext();
				}

			}
		});

		try
		{
			if(null!=map)
				if(map.size() > 0){
					JSONObject paramSentJSON = map.get(Func.getS(paramObject, "name"));
					if(null!=paramSentJSON)
					{
						String value = Func.getS(paramSentJSON, "value");
						if (null != value && !value.equals(""))
						{
							checkedValue = Integer.valueOf(value);
							enableNext();
						}
					}else{
						paramSentJSON = new JSONObject();
						paramSentJSON.put("name", Func.getS(paramObject, "name"));
						map.put(Func.getS(paramObject, "name"), paramSentJSON);
					}
				}else{
					JSONObject paramSentJSON = new JSONObject();
					paramSentJSON.put("name", Func.getS(paramObject, "name"));
					map.put(Func.getS(paramObject, "name"), paramSentJSON);
				}

			JSONArray extraArray = paramObject.getJSONArray("extra");
			if(checkedValue == -1) {
				String def = Func.getS(paramObject, "default");
				if(null!=def)
					checkedValue = Integer.valueOf(def);
					enableNext();
			}
			if(null!=extraArray && extraArray.length() > 0){
				for(int i = 0; i < extraArray.length(); i++){
					JSONObject extraObject = extraArray.getJSONObject(i);
					PresetValueButton presetValueButton = (PresetValueButton) getLayoutInflater().inflate(R.layout.template_preset_picker_button, null);
					presetValueButton.setPosition(i);
					presetValueButton.setTitle(Func.getScriptDataForCurrLang(extraObject.getString("description"),iso));
					int extraValue = extraObject.getInt("value");
					presetValueButton.setValue(extraValue);
					presetValueButton.bindView();
					presetValueButton.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View view)
						{

						}
					});
					presetRadioGroup.addView(presetValueButton);
				}
				for(int i = 0; i  < presetRadioGroup.getChildCount(); i++){
					PresetValueButton presetValueButton = (PresetValueButton) presetRadioGroup.getChildAt(i);
					if(presetValueButton.getValue() == checkedValue){
						presetValueButton.setChecked(true);
					}
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}



		paramFrame.addView(presetRadioGroup);


	}

	private void addTimePicker(final LinearLayout paramFrame, final JSONObject paramObject, String type){
		//        TODO открыть после того, как поле времени станет приниматься в нормальном формате на входе компилятора, а не так как сейчас
		LinearLayout paramFrame2 = (LinearLayout) getLayoutInflater().inflate(R.layout.template_linear_match_wrap, null);
		paramFrame2.setOrientation(LinearLayout.VERTICAL);
		paramFrame2.setGravity(Gravity.CENTER);

		FrameLayout frameLayout = new FrameLayout(context);
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		frameLayout.setLayoutParams(lp);

		LinearLayout buttonLayout = new LinearLayout(context);
		LinearLayout.LayoutParams blp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		buttonLayout.setLayoutParams(blp);
		buttonLayout.setGravity(Gravity.CENTER);

		LinearLayout textLayout = new LinearLayout(context);
		LinearLayout.LayoutParams elp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		textLayout.setLayoutParams(elp);
		textLayout.setGravity(Gravity.CENTER);

		timeTextView = new TextView(context);
		timeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		timeTextView.setPadding(0, Func.dpToPx(48, context), 0, 0);
//		timeTextView.setVisibility(View.GONE);
		timeTextView.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				showTimePicker(paramObject);
			}
		});

		textLayout.addView(timeTextView);
		frameLayout.addView(textLayout);


		buttonDatePicker = new Button(context, null, R.style.ButtonStyleLight);
		LinearLayout.LayoutParams lp3 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, Func.dpToPx(40, context));
		buttonDatePicker.setLayoutParams(lp3);
		buttonDatePicker.setPadding(Func.dpToPx(48, context), 0, Func.dpToPx(48, context), 0);
		buttonDatePicker.setBackgroundResource(R.drawable.button_square_background_white);
		Typeface typeface = ResourcesCompat.getFont(context, R.font.open_sans);
		buttonDatePicker.setTypeface(typeface);
		buttonDatePicker.setTextColor(getResources().getColor(R.color.brandColorDark));
		buttonDatePicker.setText(R.string.N_SCRIPT_SELECT_TIME_TITLE);
		buttonDatePicker.setTextSize(TypedValue.COMPLEX_UNIT_SP, 25);
		buttonDatePicker.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				showTimePicker(paramObject);
			}
		});

		try
		{
			String name = Func.getS(paramObject, "name");
			if (null != name)
			{
				if (null != map)
				{
					JSONObject paramSentJSON = map.get(Func.getS(paramObject, "name"));
					if (null != paramSentJSON)
					{
						String value = Func.getS(paramSentJSON, "value");
						if (null != value && !value.equals(""))
						{
							int time = Integer.parseInt(value);
							time += Func.getTimeZoneOffset();
							int hour = time / 60;
							int minute = time % 60;
							timeTextView.setText((hour > 9 ? hour : "0" + hour )+ " : " + (minute > 9 ? minute : "0" + minute));
							buttonDatePicker.setVisibility(View.GONE);
							enableNext(true);
						}
					} else
					{
						paramSentJSON = new JSONObject();
						paramSentJSON.put("name", name);
						String def = Func.getS(paramObject, "default");
						if(null == def){
							if(Func.getB(paramSentJSON, "required")){
								def = Integer.toString(0);
							}
						}
						if(null != def)
						{
							paramSentJSON.put("value", String.valueOf(def));
							int time = Integer.parseInt(def);
							time += Func.getTimeZoneOffset();
							int hour = time / 60;
							int minute = time % 60;
							timeTextView.setText((hour > 9 ? hour : "0" + hour ) + " : " + (minute > 9 ? minute : "0" + minute));
							buttonDatePicker.setVisibility(View.GONE);
							enableNext(true);
						}
						map.put(Func.getS(paramObject, "name"), paramSentJSON);
					}

				}
			}
		}catch (JSONException e)
		{
			e.printStackTrace();
		}

		buttonLayout.addView(buttonDatePicker);
		frameLayout.addView(buttonLayout);

		timeChangeTextView = new TextView(context);
		LinearLayout.LayoutParams tlp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		timeChangeTextView.setText(context.getResources().getString(R.string.SCRIPT_PRESS_TO_SET));
		timeChangeTextView.setGravity(Gravity.CENTER_HORIZONTAL);
		timeChangeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
		timeChangeTextView.setPadding(0, Func.dpToPx(12, context), 0, 0);


		paramFrame.addView(frameLayout);
		paramFrame.addView(timeChangeTextView);

	}

	//TIME
	private void addTime(final LinearLayout paramFrame, final JSONObject paramObject, String type)
	{
		LinearLayout paramFrame2 = (LinearLayout) getLayoutInflater().inflate(R.layout.template_linear_match_wrap, null);
		paramFrame2.setOrientation(LinearLayout.HORIZONTAL);
		paramFrame2.setGravity(Gravity.CENTER);

		try
		{
			JSONArray groupArray = paramObject.getJSONArray("group");
			if(null!=groupArray && groupArray.length() > 0){
				for (int i = 0; i < groupArray.length(); i++){
					LinearLayout paramFrame3 = (LinearLayout) getLayoutInflater().inflate(R.layout.template_linear_wrap_wrap, null);
					paramFrame3.setPadding(Func.dpToPx(12, context), 0, Func.dpToPx(12, context), 0);
					paramFrame3.setGravity(Gravity.CENTER);

					JSONObject groupObject = groupArray.getJSONObject(i);
					addParamView(paramFrame3, groupObject);
					paramFrame2.addView(paramFrame3);
				}
				paramFrame.addView(paramFrame2);
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void showTimePicker(final JSONObject paramObject)
	{
		TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.datepicker, new TimePickerDialog.OnTimeSetListener()
		{
			@Override
			public void onTimeSet(TimePicker timePicker, int hour, int minute)
			{
				setTimeFromPicker(paramObject , hour, minute);
			}
		}, Calendar.getInstance().get(Calendar.HOUR_OF_DAY), Calendar.getInstance().get(Calendar.MINUTE), DateFormat.is24HourFormat(getActivity()));
		String mappedValue = getMappedParamValue(paramObject);
		if(null!=mappedValue)timePickerDialog.updateTime((Integer.valueOf(mappedValue) + Func.getTimeZoneOffset())/60, (Integer.valueOf(mappedValue) + Func.getTimeZoneOffset())%60);
		timePickerDialog.show();
	}

	private void setTimeFromPicker(JSONObject paramObject, int hour, int minute){
		if(null!=timeTextView)
		{
			timeTextView.setText((hour > 9 ? hour : "0" + hour ) + " : " + (minute > 9 ? minute : ("0" + minute)));
			timeTextView.setVisibility(View.VISIBLE);
		}
		mapParam(Func.getS(paramObject, "name"), String.valueOf(hour*60+minute - Func.getTimeZoneOffset()));
		if(null!=buttonDatePicker) buttonDatePicker.setVisibility(View.GONE);
		if(null!=buttonNext) buttonNext.setEnabled(true);
	}

	private void mapParam(String name, String value)
	{
		try
		{
			JSONObject paramSentJSON = new JSONObject();
			paramSentJSON.put("name", name);
			paramSentJSON.put("value", value);
			map.put(name, paramSentJSON);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private String getMappedParamValue(JSONObject jsonObject){
		String name = Func.getS(jsonObject, "name");
		if(null!=name)
		{
			if (null != map) if (map.size() > 0)
			{
				JSONObject paramSentJSON = map.get(Func.getS(jsonObject, "name"));
				if (null != paramSentJSON)
				{
					String value = Func.getS(paramSentJSON, "value");
					if (null != value && !value.equals(""))
					{
						return value;
					}
				}
			}
		}
		return null;

	}

	//SELECTOR
	private void addSelector(LinearLayout paramFrame, final JSONObject paramObject, final String type)
	{
		LinearLayout linearLayout = new LinearLayout(context);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
		linearLayout.setLayoutParams(lp);
		linearLayout.setGravity(Gravity.CENTER);

		LinearLayout frameLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.frame_script_selector_type, null);
		frameLayout.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if(null!=type){
					showSelector(paramObject, type);
				}
			}
		});

		buttonLayout = frameLayout.findViewById(R.id.buttonFrame);
		textLayout = frameLayout.findViewById(R.id.textFrame);
		selectorTextView = frameLayout.findViewById(R.id.selectText);
		selectorTextView.setGravity(Gravity.CENTER);

		String def = "";
		if(null!=type && (type.equals("time_zone")||type.equals("time_zone_m"))){
			def = String.valueOf(ZoneId.systemDefault().getRules().getOffset(Instant.now()).getTotalSeconds()/60);
		}else{
			def = Func.getS(paramObject, "default");
		}
//		if(curSelect == -1){
//			textLayout.setVisibility(View.GONE);
//		}else{
//			textLayout.setVisibility(View.VISIBLE);
//
//		}

		try
		{
			if(null!=map)
			{
				if (((map.size() == 0) || (null==(paramSentJSON=map.get(Func.getS(paramObject, "name"))))))
				{
					paramSentJSON = new JSONObject();
					paramSentJSON.put("name", Func.getS(paramObject, "name"));
					if(null!=def && !def.equals("")) paramSentJSON.put("value", def);
					map.put(Func.getS(paramObject, "name"), paramSentJSON);
				}
				String value = Func.getS(paramSentJSON, "value");
				if (null != value && !value.equals(""))
				{
					curSelect = Integer.valueOf(value);
					if (-1 != curSelect)
					{
						String valueText = "";
						if(null!=type){
							switch (type)
							{
								case "zones":
									if (0 != curSelect)
									{
										enableNext();
										int i = 1;
										while ((curSelect >> (i - 1)) > 0)
										{
											if (((curSelect >> (i - 1)) & 0x1) == 1)
											{
												valueText += dbHelper.getZoneNameById(deviceId, i) + ", ";
											}
											i++;
										}
										valueText = valueText.substring(0, valueText.length() - 2);
									} else
									{
										valueText = getString(R.string.SCRIPT_NO_SELECT);
										textLayout.setVisibility(View.GONE);
										buttonLayout.setVisibility(View.VISIBLE);
									}
									break;
								case "sections":
									if (0 != curSelect)
									{
										enableNext();
										int i1 = 1;
										while ((curSelect >> (i1 - 1)) > 0)
										{
											if (((curSelect >> (i1 - 1)) & 0x1) == 1)
											{
												valueText += dbHelper.getSectionNameByIdWithOptions(deviceId, i1) + ", ";
											}
											i1++;
										}
										valueText = valueText.substring(0, valueText.length() - 2);
									} else
									{
										valueText = getString(R.string.SCRIPT_NO_SELECT);
										textLayout.setVisibility(View.GONE);
										buttonLayout.setVisibility(View.VISIBLE);
									}
									break;
								case "zone":
									valueText = dbHelper.getZoneNameById(deviceId, Integer.valueOf(value));
									enableNext();
									break;
								case "section":
									valueText = dbHelper.getSectionNameByIdWithOptions(deviceId, Integer.valueOf(value));
									enableNext();
									break;
								case "time_zone":
								case "time_zone_m":
									//								valueText = valueText + "GMT " + (curSelect == 0 ? "" : ((curSelect > 0 ? "+" : "") + curSelect));
									valueText = ZoneOffset.ofTotalSeconds(Integer.valueOf(curSelect)*60).getDisplayName(TextStyle.FULL_STANDALONE, Locale.getDefault());
									enableNext();
									break;
							}
						}
						selectorTextView.setText(valueText);
						textLayout.setVisibility(View.VISIBLE);
						buttonLayout.setVisibility(View.GONE);
					} else
					{
						textLayout.setVisibility(View.GONE);
						buttonLayout.setVisibility(View.VISIBLE);
					}
				} else
				{
					curSelect = -1;
					textLayout.setVisibility(View.GONE);
					buttonLayout.setVisibility(View.VISIBLE);
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		linearLayout.addView(frameLayout);
		paramFrame.addView(linearLayout);
	}

	private void  showSelector(JSONObject paramObject, String type){
		if(null!=type){
			switch (type){
				case "zone":
				case "section":
				case "time_zone":
				case "time_zone_m":
					int zType =0;
					try
					{
						JSONArray extra = paramObject.getJSONArray("extra");
						for(int i=0; i<extra.length(); i++){
							JSONObject typeObject  = extra.getJSONObject(i);
							if(typeObject.getString("name").equals("zone_type")){
								String zTypeString = typeObject.getString("value");
								if(null!=zTypeString) zType = Integer.valueOf(zTypeString);
							}
						}
					} catch (JSONException e)
					{
						e.printStackTrace();
					}

					BottomSheetFragmentForSelector bottomSheetFragmentForSelector =
							new BottomSheetFragmentForSelector().newInstance(Func.getS(paramObject, "name"),
									deviceId,
									siteId,
									type,
									curSelect,
									position, zType, bind);
					bottomSheetFragmentForSelector.show(getFragmentManager(), "tagSelect");
					break;
				case "zones":
				case "sections":
					showCheckSelectorDialog(paramObject, type);
					break;
			}
		}

	}

	private void showCheckSelectorDialog(final JSONObject paramObject, String type)
	{
		AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		final AlertDialog checkComponentDialog = adb.create();

		FrameLayout frameLayout = (FrameLayout) new FrameLayout(context);
		frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

		if(null!=type){
			switch (type){
				case "zones":
					checkComponentDialog.setTitle(getString(R.string.SCRIPT_SET_CHOOSE_ZONE));

					ListView zonesChecklist = new ListView(context);
					int zType = 0;
					try
					{
						JSONArray extra = paramObject.getJSONArray("extra");
						for(int i=0; i<extra.length(); i++){
							JSONObject typeObject  = extra.getJSONObject(i);
							if(typeObject.getString("name").equals("zone_type")){
								String zTypeString = typeObject.getString("value");
								zType = Integer.valueOf(zTypeString);
							}
						}
					} catch (JSONException e)
					{
						e.printStackTrace();
					}

					Zone[] allZones = dbHelper.getAllZonesForDeviceWithType(deviceId, zType, bind);

					if(allZones != null && allZones.length != 0)
					{
						if(0 < curSelect){
							for(int i = 0; i < allZones.length; i++)
							{
								Zone zone = allZones[i];
								if(null!= zone){
									if((curSelect&(1<<(zone.id -1)))!=0){
										zone.checked = true;
									}
								}
							}
						}
						zoneAddSectionsListAdapter = new ZoneCheckArrayAdapter(context, R.layout.card_for_section_zone_check, allZones);
						zonesChecklist.setAdapter(zoneAddSectionsListAdapter);
						frameLayout.addView(zonesChecklist);
					}else{
						TextView textView  = (TextView) getLayoutInflater().inflate(R.layout.template_description_text, null);
						textView.setPadding(Func.dpToPx(8, context), Func.dpToPx(8, context), Func.dpToPx(8, context),0);
						textView.setGravity(Gravity.CENTER);
						textView.setText(R.string.SCRIPT_SET_NO_ZONES);
						frameLayout.addView(textView);
					}

					checkComponentDialog.setView(frameLayout);

					checkComponentDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.ADB_OK), new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialogInterface, int i)
						{
							if(null!=zoneAddSectionsListAdapter){
								Zone[] zones = zoneAddSectionsListAdapter.getZones();
								int zN = 0;
								String zS = "";
								for(Zone zone:zones){
									if(zone.checked)
									{
										zN += 1 << (zone.id - 1);
										zS += zone.name + ", ";
									}
								}
								curSelect = zN == 0? -1: zN;
								mapParam(Func.getS(paramObject, "name"), String.valueOf(curSelect));

								enableNext();
								if(curSelect > 0){
									buttonLayout.setVisibility(View.GONE);
									textLayout.setVisibility(View.VISIBLE);
									selectorTextView.setText(zS.substring(0, zS.length()-2));
								}else{
									buttonLayout.setVisibility(View.VISIBLE);
									textLayout.setVisibility(View.GONE);
								}
							}
							((WizardScriptSetActivity) context).addParamArray(map);
							((WizardScriptSetActivity) context).refreshViews(false);
						}
					});
					break;
				case "sections":
					checkComponentDialog.setTitle(getString(R.string.SCRIPT_SET_CHOOSE_SECTIONS));

					ListView sectionsCheckList = new ListView(context);
					int sType = 0;
					try
					{
						JSONArray extra = paramObject.getJSONArray("extra");
						for(int i=0; i<extra.length(); i++){
							JSONObject typeObject  = extra.getJSONObject(i);
							if(typeObject.getString("name").equals("section_type")){
								String zTypeString = typeObject.getString("value");
								sType = Integer.valueOf(zTypeString);
							}
						}
					} catch (JSONException e)
					{
						e.printStackTrace();
					}

					Section[] allSections = dbHelper.getAllSectionsForSiteDeviceWithoutControllerByType(deviceId, sType);

					if(allSections != null && allSections.length != 0)
					{
						if(0 < curSelect){
							for(int i = 0; i < allSections.length; i++)
							{
								Section section = allSections[i];
								if(null!= section){
									if((curSelect&(1<<(section.id -1)))!=0){
										section.checked = true;
									}
								}
							}
						}
						userAddSectionsListAdapter = new UserAddSectionsListAdapter(context, R.layout.card_for_section_zone_check, allSections);
						sectionsCheckList.setAdapter(userAddSectionsListAdapter);
						frameLayout.addView(sectionsCheckList);
					}else{
						TextView textView  = (TextView) getLayoutInflater().inflate(R.layout.template_description_text, null);
						textView.setPadding(Func.dpToPx(8, context), Func.dpToPx(8, context), Func.dpToPx(8, context),0);
						textView.setGravity(Gravity.CENTER);
						textView.setText(R.string.SCRIPT_SET_NO_SECTIONS);
						frameLayout.addView(textView);
					}

					checkComponentDialog.setView(frameLayout);

					checkComponentDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.ADB_OK), new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialogInterface, int i)
						{
							if(null!=userAddSectionsListAdapter){
								Section[] sections = userAddSectionsListAdapter.getSections();
								int sec = 0;
								String secS = "";
								for(Section section:sections){
									if(section.checked)
									{
										sec += 1 << (section.id - 1);
										secS += section.name + ", ";
									}
								}
								curSelect = sec == 0? -1: sec;
								mapParam(Func.getS(paramObject, "name"), String.valueOf(curSelect));

								enableNext();
								if(curSelect > 0){
									buttonLayout.setVisibility(View.GONE);
									textLayout.setVisibility(View.VISIBLE);
									selectorTextView.setText(secS.substring(0, secS.length()-2));

								}else{
									buttonLayout.setVisibility(View.VISIBLE);
									textLayout.setVisibility(View.GONE);
								}
							}
							((WizardScriptSetActivity) context).addParamArray(map);
							((WizardScriptSetActivity) context).refreshViews(false);
						}
					});
					break;
			}
		}



		checkComponentDialog.show();
	}

	public static class BottomSheetFragmentForSelector extends BottomSheetDialogFragment
	{
		private Context context;
		private DBHelper dbHelper;
		private int deviceId = -1;
		private int siteId = -1;
		private String name;
		private String type;
		private int curSelect;
		private int pagerPosition;
		private int zType;
		private int bind;

		public static BottomSheetFragmentForSelector newInstance(String name, int deviceId, int siteId, String type, int curSelect, int pagerPosition, int zType, int bind){
			BottomSheetFragmentForSelector bottomSheetFragmentForSelector = new BottomSheetFragmentForSelector();
			Bundle b = new Bundle();
			b.putInt("cur_select", curSelect);
			b.putInt("device_id", deviceId);
			b.putInt("site_id", siteId);
			b.putString("type", type);
			b.putString("name", name);
			b.putInt("position", pagerPosition);
			b.putInt("ztype", zType);
			b.putInt("bind", bind);
			bottomSheetFragmentForSelector.setArguments(b);
			return bottomSheetFragmentForSelector;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState)
		{
			context =getContext();
			dbHelper = DBHelper.getInstance(context);
			curSelect = getArguments().getInt("cur_select", -1);
			deviceId = getArguments().getInt("device_id", -1);
			siteId = getArguments().getInt("site_id", -1);
			type = getArguments().getString("type");
			name = getArguments().getString("name");
			zType = getArguments().getInt("ztype");
			bind = getArguments().getInt("bind");
			pagerPosition  = getArguments().getInt("position");

			final BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

			View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
			TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
			ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
			listView.setDivider(null);
			if(null!=type){
				switch (type){
					case "section":
						title.setText(R.string.SCRIPT_SET_CHOOSE_SECTION);
						Section[] sections;
						if(deviceId !=-1)
						{
							if(siteId !=-1){

								sections = dbHelper.getAllSectionsForSiteDeviceWithController(siteId, deviceId);
							}else{
								sections = dbHelper.getAllSectionsForDeviceWithController(deviceId);
							}
							if (null != sections)
							{
								SectionsListAdapter zonesListAdapter = new SectionsListAdapter(context, R.layout.n_card_for_list, sections, curSelect);
								listView.setAdapter(zonesListAdapter);
								listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
								{
									@Override
									public void onItemClick(AdapterView<?> parent, View view, int position, long id)
									{
										Section section = (Section) parent.getItemAtPosition(position);
										setMap(Integer.toString(section.id));
										bottomSheetDialog.dismiss();
									}
								});
							}
						}
						break;
					case "zone":
						title.setText(R.string.CAMERA_TITLE_CHOOSE_ZONE);
						Zone[] zones;
						if(deviceId !=-1)
						{
							zones = dbHelper.getAllZonesForDeviceWithType(deviceId, zType, bind);
							if (null != zones)
							{
								ZonesListAdapter zonesListAdapter = new ZonesListAdapter(context, R.layout.n_card_for_list, zones, curSelect);
								listView.setAdapter(zonesListAdapter);
								listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
								{
									@Override
									public void onItemClick(AdapterView<?> parent, View view, int position, long id)
									{
										Zone zone = (Zone) parent.getItemAtPosition(position);
										setMap(Integer.toString(zone.id));
										bottomSheetDialog.dismiss();
									}
								});
							}
						}
						break;
					case "time_zone":
					case "time_zone_m":
						title.setText(R.string.SCRIPT_SET_CHOOSE_TIMEZONE);
						Set<String> tZonesSet = ZoneId.getAvailableZoneIds();
						String[] tZonesArray = tZonesSet.toArray(new String[tZonesSet.size()]);
						HashMap<Integer, String> tzMap = new HashMap<>();
						Instant instant = Instant.now();
						for (String tZone: tZonesArray){
							ZoneId zoneId = ZoneId.of(tZone);
							int off = zoneId.getRules().getStandardOffset(instant).getTotalSeconds();
							String name = zoneId.getDisplayName(TextStyle.SHORT, Locale.getDefault());
							tzMap.put(off, name);
						}
						List<Integer> tzList = getSortedList(tzMap);
						Integer[] tZoneOffsets = tzList.toArray(new Integer[tzList.size()]);
						if (null != tZoneOffsets)
						{
							TimeZonesListAdapter zonesListAdapter = new TimeZonesListAdapter(context, R.layout.n_card_for_list, tZoneOffsets, tzMap, String.valueOf(curSelect));
							listView.setAdapter(zonesListAdapter);
							listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
							{
								@Override
								public void onItemClick(AdapterView<?> parent, View view, int position, long id)
								{
									curSelect = (int) parent.getItemAtPosition(position)/60;
									//								int timeZone = Integer.valueOf(tZoneOffset);
									setMap(Long.toString(curSelect));
									bottomSheetDialog.dismiss();
								}
							});
						}
						break;
				}
			}

			bottomSheetDialog.setContentView(bottomView);

			try {
				Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
				mBehaviorField.setAccessible(true);
				final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
				behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
					@Override
					public void onStateChanged(@NonNull View bottomSheet, int newState) {
						if (newState == BottomSheetBehavior.STATE_DRAGGING) {
							behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
						}
					}

					@Override
					public void onSlide(@NonNull View bottomSheet, float slideOffset) {
					}
				});
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}

			bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener() {
				@Override public void onShow(DialogInterface dialog) {
					FrameLayout bottomSheet = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);
					if (null != bottomSheet) {
						BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
						behavior.setHideable(false);
					}
				}
			});



			return bottomSheetDialog;
		}

		private void setMap(String s)
		{
			try
			{
				JSONObject paramSentJSON = new JSONObject();
				paramSentJSON.put("name", name);
				paramSentJSON.put("value", s);
				HashMap map = ((WizardScriptSetActivity) context).getParamArray(pagerPosition);
				if(null==map) map = new HashMap();
				map.put(name, paramSentJSON);
				((WizardScriptSetActivity) context).addParamArray(map);//тут вроде дублируется сохранение параметров, но пока пусть остается
				((WizardScriptSetActivity) context).refreshViews(false);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}

		}
	}

	//EDIT
	private void addEdit(LinearLayout paramFrame, final JSONObject paramObject, final String type)
	{

		TextInputLayout textInputLayout = new TextInputLayout(context);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		textInputLayout.setLayoutParams(lp);
		textInputLayout.setGravity(Gravity.CENTER);

		final EditText editText = new EditText(context);
		LinearLayout.LayoutParams lpE = new LinearLayout.LayoutParams(Func.dpToPx(60, context), ViewGroup.LayoutParams.WRAP_CONTENT);
		editText.setLayoutParams(lpE);
		editText.setGravity(Gravity.CENTER);
		if(null!=type)
			switch (type){
				case "int":
				case "duration_s":
				case "duration_m":
					editText.setInputType(InputType.TYPE_CLASS_NUMBER);
					break;
				case "xfloat":
					editText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
					break;
				default:
					break;
			}
		editText.setSingleLine(true);
		editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
		editText.setImeOptions(EditorInfo.IME_ACTION_DONE);

		editText.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent)
			{
				if(i == EditorInfo.IME_ACTION_DONE){
					if(null!=buttonNext && buttonNext.isEnabled()) {
						buttonNext.callOnClick();
					}else{
						switchFocus();
					}
				}
				return false;
			}
		});

		editText.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{
				//set params array
				if(null!=type){
					switch (type){
						case "xfloat":
							String s = charSequence.toString();
							if(null!=s && s.length() != 0)
							{
								if (Pattern.matches("([0-9]*[.])?[0-9]+", s))
								{
									String regex = Func.getS(paramObject, "format");
									float f = Float.parseFloat(s) * 100;
									String fval = Integer.toString(Math.round(f));
									if (Pattern.matches(regex, fval))
									{
										mapParam(Func.getS(paramObject, "name"), fval);
										enableNext();
										break;
									}
								}
							}
							mapParam(Func.getS(paramObject, "name"), "");
							enableNext();
							break;
						default:
							mapParam(Func.getS(paramObject, "name"), charSequence.toString());
							enableNext();
							break;
					}
				}else{
					mapParam(Func.getS(paramObject, "name"), charSequence.toString());
					enableNext();
				}
			}

			@Override
			public void afterTextChanged(Editable editable)
			{
				String regex = Func.getS(paramObject, "format");
				String text = editable.toString();
				if(null!=regex && text.length() > 0)
				{
					if(null!=type)
					{
						switch (type)
						{
							case "int":
								if (!Pattern.matches(regex, text))
								{
									editable.delete(text.length() - 1, text.length());
								}
								break;
							case "xfloat":
								if (text.length() > 1)
								{
									String last = text.substring(text.length() - 1);
									//проверяем, что не две точки подряд
									if (last.equals("."))
									{
										if (!text.substring(0, text.length() - 1).contains(last))
										{
											break;
										}
									} else
									{
										if (Pattern.matches(".{0,4}", text))
										{
											if (Pattern.matches("(([0-9]*[.])?[0-9]+)", text))
											{
												float f = Float.valueOf(text);
												float d = f % 1;
												if ((f % 1) < 100)//проверяем, что не больше двух символов после точки
												{
													int v = Math.round(f * 100);
													if (Pattern.matches(regex, Integer.toString(v)))
													{
														break;
													}
												}
											}
										}
									}
								} else if (Pattern.matches("[0-9]", text))
								{
									break;
								}
								editable.delete(text.length() - 1, text.length());
								break;
							default:
								break;
						}
					}
				}
			}
		});

		textInputLayout.addView(editText);

		try
		{

			if(null!=map)
			{
				//				if(map.size() > 0){
				JSONObject paramSentJSON = map.get(Func.getS(paramObject, "name"));
				if (null != paramSentJSON)
				{
					String value = Func.getS(paramSentJSON, "value");
					if (null != value && !value.equals(""))
					{
						if(null!=type)
						{
							switch (type)
							{
								case "xfloat":
									editText.setText(Float.toString(Float.parseFloat(value) / 100));
									break;
								default:
									editText.setText(value);
									break;
							}
						}
						enableNext();
					}
				} else
				{
					paramSentJSON = new JSONObject();
					paramSentJSON.put("name", Func.getS(paramObject, "name"));
					String def = Func.getS(paramObject, "default");
					if(null!=def && null!=type){
						if(null!=type)
							if ("xfloat".equals(type))
							{
								editText.setText(Float.toString(Float.parseFloat(def) / 100));
							} else
							{
								editText.setText(def);
							}
						paramSentJSON.put("value", def);
					}
					map.put(Func.getS(paramObject, "name"), paramSentJSON);
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		paramFrame.addView(textInputLayout);
	}

	private void switchFocus()
	{
		TextView t = (TextView) activity.getCurrentFocus();
		if (t != null && t.length() > 0)
		{
			t.clearFocus();
			View next = t.focusSearch(View.FOCUS_RIGHT); // or FOCUS_FORWARD
			if (next != null){
				next.requestFocus();
			}else{
				next = t.focusSearch(View.FOCUS_DOWN);
				if (next != null){
					next.requestFocus();
				}
			}

		}
	}

	private void enableNext()
	{
		if(null!=buttonNext)
		{
			boolean enable = true;
			for(String key : map.keySet()){
				JSONObject jsonObject = map.get(key);
				if(null== Func.getS(jsonObject, "value") || Func.getS(jsonObject, "value").equals("")){
					enable = false;
				}
			}
			buttonNext.setEnabled(enable);
		}
	}

	private void enableNext(boolean b)
	{
		if(null!=buttonNext)
		{
			buttonNext.setEnabled(b);
		}
	}

	//GROUP
	private void addGroup(LinearLayout paramFrame, JSONObject paramObject)
	{
		LinearLayout paramFrame2 = (LinearLayout) getLayoutInflater().inflate(R.layout.template_linear_match_wrap, null);

		try
		{
			JSONArray groupArray = paramObject.getJSONArray("group");
			if(null!=groupArray && groupArray.length() > 0){
				for (int i = 0; i < groupArray.length(); i++){
					JSONObject groupObject = groupArray.getJSONObject(i);
					addParamView(paramFrame2, groupObject);
				}
				paramFrame.addView(paramFrame2);
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	private void setFrameConsts(View v)
	{
		ImageButton buttonInfo = (ImageButton) v.findViewById(R.id.buttonScriptInfo);
		buttonInfo.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				showScriptInfo();
			}
		});

		TextView title = (TextView) v.findViewById(R.id.wizScriptTitleText);
		title.setText(Func.getScriptDataForCurrLang(script.nameLib, iso));
		if(-1!= deviceId)
		{
			Device device = dbHelper.getDeviceById(deviceId);
			if(null!=device){
				TextView dest = (TextView) v.findViewById(R.id.wizScriptDescText);
				dest.setText(device.getName() + " " + context.getString(R.string.DTA_SERIAL_NUMBER) + device.account);
			}

		}

		buttonNext = (TextView) v.findViewById(R.id.wizScriptButtonNext);
		progressBar  = (ProgressBar) v.findViewById(R.id.wizScriptsNextProgress);
		TextView buttonPrevious = (TextView) v.findViewById(R.id.wizScriptButtonCancel);

		if(null!= buttonNext){
			if(position == activity.getPagerCount() - 1){
				if(activity.getTarget() == WizardScriptSetActivity.Target.SET) {
					buttonNext.setText(R.string.SCRIPT_SET_ADD);
				}else{
					buttonNext.setText(R.string.SCRIPT_SET_FINISH);
				}
			}else{
				buttonNext.setText(R.string.SCRIPT_SET_NEXT);
			}
			buttonNext.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					if(0!=position)activity.addParamArray(map);
					if(position == activity.getPagerCount() - 1){
						buttonNext.setVisibility(View.GONE);
 						progressBar.setVisibility(View.VISIBLE);
 						handle = true;
						Handler handler = new Handler();
						handler.postDelayed(new Runnable()
						{
							@Override
							public void run()
							{
								if(handle)
								{
									progressBar.setVisibility(View.GONE);
									buttonNext.setVisibility(View.VISIBLE);
									Func.pushToast(context, "No response", activity);
								}
							}
						}, 5000);
					}
					activity.goNext();
				}
			});
		}

		if(null!=buttonPrevious){
			if(position == 0){
				buttonPrevious.setText(R.string.SCRIPT_SET_CANCEL);
			}else{
				buttonPrevious.setText(R.string.SCRIPT_SET_BACK);
			}
			buttonPrevious.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					activity.goPrevious();
				}
			});
		}
	}

	private void showScriptInfo()
	{
		String scriptInfo =  Func.getScriptDataForCurrLang(script.description, iso);
		Func.showInfo(context, getString(R.string.SCRIPT_SET_CATEGORY) + Func.getScriptDataForCurrLang(script.category, iso).substring(1).replace("/", ": ") + "\n\n" + scriptInfo);
	}

	private static List getSortedList(HashMap map) {
		List list = new LinkedList(map.entrySet());
		// Defined Custom Comparator here
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o1)).getKey())
						.compareTo(((Map.Entry) (o2)).getKey());
			}
		});
		List<Integer> tList = new LinkedList<>();
		for(Iterator it = list.iterator(); it.hasNext();){
			Map.Entry entry = (Map.Entry) it.next();
			tList.add((Integer) entry.getKey());
		}

		return tList;
	}



}
