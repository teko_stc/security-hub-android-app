package biz.teko.td.SHUB_APP.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;

public class NInfoSupportDialogFragment extends DialogFragment {
    private NDialog dialog;
    private TextView titleView, messView;
    private String title, mess, cancelText, okText;
    private NActionButton cancel, ok;
    private boolean isCancelGone, isTitleGone;
    private IButtonClick listener;

    public interface IButtonClick {
        void cancelClick();
        void continueClick();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        dialog = new NDialog(getContext(), R.layout.n_dialog_info_fragment);
        titleView = dialog.findViewById(R.id.nDialogTitle);
        messView = dialog.findViewById(R.id.nDialogMess);
        cancel = dialog.findViewById(R.id.nDialogButtonCancel);
        cancel.setOnButtonClickListener(action -> listener.cancelClick());
        ok = dialog.findViewById(R.id.nDialogButtonOk);
        ok.setOnButtonClickListener(action -> {
            dismiss();
            listener.continueClick();
        });
        setView();
        return dialog;
    }

    private void setView() {
        if (title != null) titleView.setText(title);
        if (mess != null) messView.setText(mess);
        if (okText != null) ok.setText(okText);
        if (cancelText != null) cancel.setText(cancelText);
        if (isCancelGone)
            cancel.setVisibility(View.GONE);
        if (isTitleGone)
            titleView.setVisibility(View.GONE);
    }

    public void setListener(IButtonClick listener) {
        this.listener = listener;
    }

    public void setMess(String mess) {
        this.mess = mess;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setTextOk(String okText) {
        this.okText = okText;
    }

    public void setTextCancel(String cancelText) {
        this.cancelText = cancelText;
    }

    public void setCancelGone(boolean isGone) {
        isCancelGone = isGone;
    }

    public void setTitleGone(boolean isGone) {
        isTitleGone = isGone;
    }
}
