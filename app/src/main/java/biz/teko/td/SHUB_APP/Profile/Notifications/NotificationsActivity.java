package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import androidx.core.app.NotificationManagerCompat;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;

/**
 * Created by td13017 on 10.03.2017.
 */
public class NotificationsActivity extends BaseActivity
{
    private Context context;
    private DBHelper dbHelper;
    private boolean toSounds = false;
    private PreferenceFragment lastFragment = new NotificationsPreferenceFragment();
    private NTopToolbarView toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_notifications);
        context = this;
        dbHelper = DBHelper.getInstance(context);
        initView();
    }

    private void initView() {
        toolbar = (NTopToolbarView) findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);
    }

    public void replaceFragment(PreferenceFragment fragment, String tag) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        if (tag == null)
            transaction.replace(R.id.prefContainer, fragment)
                    .commit();
        else
            transaction.replace(R.id.prefContainer, fragment)
                    .addToBackStack(tag)
                    .commit();
        lastFragment = fragment;
    }

    @Override
    protected void onResume() {
        super.onResume();
        toSounds = false;
        ((App) this.getApplication()).stopActivityTransitionTimer();
        replaceFragment(lastFragment, null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!NotificationManagerCompat.from(context).areNotificationsEnabled()) {
                ((NActionButton) toolbar.getViewById(R.id.nDialogButtonSetting))
                        .setOnButtonClickListener(view -> {
                            Intent intent = new Intent();
                            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                            intent.putExtra("android.provider.extra.APP_PACKAGE", getPackageName());
                            startActivity(intent);
                        });
                ((NActionButton) toolbar.getViewById(R.id.nDialogButtonCancel))
                        .setOnButtonClickListener(view -> toolbar.setStateInfo(false));
                toolbar.setStateInfo(true);
            } else {
                toolbar.setStateInfo(false);
            }
        }
    }

    @Override
    protected void onPause() {
        if (!toSounds) {
            ((App) this.getApplication()).startActivityTransitionTimer(context);
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }

}
