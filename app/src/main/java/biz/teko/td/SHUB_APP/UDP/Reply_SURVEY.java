package biz.teko.td.SHUB_APP.UDP;

/**
 * Created by td13017 on 11.01.2017.
 */
public class Reply_SURVEY
{
	public int ip_address;
	public int serial;
	public Byte hw_revision;
	public Byte hw_type;
	public Byte sw_minor;
	public Byte sw_major;

	public Reply_SURVEY(){

	};

	public Reply_SURVEY(int ip_address, int serial, Byte hw_revision, Byte hw_type, Byte sw_minor, Byte sw_major){
		this.ip_address = ip_address;
		this.serial = serial;
		this.hw_revision = hw_revision;
		this.hw_type = hw_type;
		this.sw_minor = sw_minor;
		this.sw_major = sw_major;
	}

	public Reply_SURVEY(byte[] buf){
	}
}