package biz.teko.td.SHUB_APP.D3DB;

/**
 * Created by td13017 on 29.07.2016.
 */
public class ConfigFileObject
{
	public int site_id, number;
	public int[] bytes;

	public ConfigFileObject(){};

	public ConfigFileObject(int site_id, int number, int[] bytes){
		this.site_id = site_id;
		this.number = number;
		this.bytes = bytes;
	}
}
