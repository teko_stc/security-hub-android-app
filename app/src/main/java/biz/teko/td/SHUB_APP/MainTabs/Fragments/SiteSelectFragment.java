package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.MainTabs.Adapters.SiteSelectAdapter;
import biz.teko.td.SHUB_APP.R;

public class SiteSelectFragment extends DialogFragment
{
	private Context context;
	private DBHelper dbHelper;
	private OnClickListener onClickListener;

	public enum Result{
		CHANGE
	}

	public  interface OnClickListener
	{
		void callback(Result result, int site_id);
	}

	public void setListener(OnClickListener onClickListener){
		this.onClickListener = onClickListener;
	}

	public static SiteSelectFragment getInstance(int site_id)
	{
		SiteSelectFragment siteSelectFragment = new SiteSelectFragment();
		Bundle b = new Bundle();
		b.putInt("site_id", site_id);
		siteSelectFragment.setArguments(b);
		return siteSelectFragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		context = getContext();
		dbHelper = DBHelper.getInstance(context);
		final int site_id = getArguments().getInt("site_id");
		View view = ((Activity) context).getLayoutInflater().inflate(R.layout.fragment_site_select, null);

		LinearLayout closeButton = (LinearLayout) view.findViewById(R.id.nButtonClose);
		closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				dismiss();
			}
		});



		final ListView sitesList = (ListView) view.findViewById(R.id.sitesRecycler);
		Site[] sites = dbHelper.getAllSitesArray();
		if(null!=sites)
		{
			final SiteSelectAdapter siteSelectListAdapter = new SiteSelectAdapter(context, R.layout.n_site_select_list_element, sites, site_id);
			siteSelectListAdapter.setListener(new SiteSelectAdapter.OnViewItemClick()
			{
				@Override
				public void callBackToFragment(SiteSelectAdapter.ACTION action, int site_id)
				{
					switch (action){
						case SELECT:
							onClickListener.callback(Result.CHANGE, site_id);
							break;
						case DISMISS:
							break;
					}
					dismiss();
				}
			});
			sitesList.setAdapter(siteSelectListAdapter);
		}

		getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		return view;
	}
}
