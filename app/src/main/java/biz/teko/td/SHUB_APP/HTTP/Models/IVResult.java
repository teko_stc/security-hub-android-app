package biz.teko.td.SHUB_APP.HTTP.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by td13017 on 22.02.2018.
 */

public class IVResult
{
	@SerializedName("items")
	public IVResult[] items;

	@SerializedName("name")
	public String name;

	@SerializedName("id")
	public String id;

	@SerializedName("connected")
	public boolean status;

}
