package biz.teko.td.SHUB_APP.Wizards.Adapter.Pager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardRelaySetActivity;
import biz.teko.td.SHUB_APP.Wizards.Fragments.RelaySetFragment;

public class RelaySetViewPagerAdapter extends FragmentPagerAdapter
{
	public RelaySetViewPagerAdapter(FragmentManager fm)
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int position)
	{
		return RelaySetFragment.newInstance(position);
	}

	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}

	@Override
	public int getItemPosition(@NonNull Object object)
	{
		return POSITION_NONE;
	}

	@Override
	public int getCount()
	{
		return WizardRelaySetActivity.getPagerCount();
	}
}
