package biz.teko.td.SHUB_APP.Scripts.Activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Adapters.ScriptsLibraryStringsListAdapter;

public class ScriptsLibraryActivity extends BaseActivity
{
	private int deviceId;
	private DBHelper dbHelper;
	private Context context;
	private Device device;
	private int siteId;
	private Site site;
	private int sending = 0;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private TextView noScriptsText;
	private ListView scriptsCategoryList;
	private String[] libraryScriptsArray;
	private SharedPreferences sp;
	private String iso;
	private ScriptsLibraryStringsListAdapter scriptsCategoryAdapter;
	private int bind;
	//	private HashMap<String, String> scriptsMap;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scripts_library);
		context = this;
		dbHelper = DBHelper.getInstance(context);
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));

		deviceId = getIntent().getIntExtra("device_id", -1);
		siteId = getIntent().getIntExtra("site_id", -1);
		bind = getIntent().getIntExtra("bind", -1);

		if(-1!= siteId){
			site = dbHelper.getSiteById(siteId);
		}
		if(-1!=deviceId)
		{
			device = dbHelper.getDeviceById(deviceId);
		}

		TextView backButton = (TextView) findViewById(R.id.scriptBackButton);
		backButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		serviceConnection = new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		animateEntry();

		scriptsCategoryList = (ListView) findViewById(R.id.scriptsCategoryList);
		scriptsCategoryList.setDivider(null);
		updateList();

	}

	private void updateList(){
		refreshList();
		refreshViewVisiblity();
	}

	private void refreshList(){
		refreshArray();
		refreshAdapter();
	}

	private void refreshArray(){
		libraryScriptsArray = dbHelper.getAllScriptsCategoriesByConfigVersion(device!= null? device.config_version : -1);
	}

	private void refreshAdapter()
	{
		if(null!=libraryScriptsArray)
		{
			scriptsCategoryAdapter = new ScriptsLibraryStringsListAdapter(context, R.layout.card_script_library_list, libraryScriptsArray);
			if (null != scriptsCategoryList && null != scriptsCategoryAdapter) {
				scriptsCategoryList.setAdapter(scriptsCategoryAdapter);
				scriptsCategoryList.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long l)
					{
						String category = (String) parent.getItemAtPosition(position);
						if(null!=category){
							Intent intent = new Intent(getBaseContext(), ScriptsCategoryActivity.class);
							intent.putExtra("category", category);
							intent.putExtra("device_id", deviceId);
							intent.putExtra("site_id", siteId);
							intent.putExtra("bind", bind);
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
							{
								// Apply activity transition
								startActivityForResult(intent, D3Service.ADD_NEW_SCRIPT, ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle());
							} else
							{
								// Swap without transition
								startActivityForResult(intent, D3Service.ADD_NEW_SCRIPT);
								overridePendingTransition(R.animator.enter, R.animator.exit);
							}
						}
					}
				});
			}
		}
	}


	private void refreshViewVisiblity()
	{
		if(null!= scriptsCategoryList && null != noScriptsText){
			if(null!=scriptsCategoryAdapter && scriptsCategoryAdapter.getCount() != 0)
			{
				scriptsCategoryList.setVisibility(View.VISIBLE);
//				noScriptsText.setVisibility(View.GONE);
			}else{
				scriptsCategoryList.setVisibility(View.GONE);
//				noScriptsText.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
		updateList();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

//	private boolean sendCommandToServer(D3Request d3Request)
//	{
//		if(sending == 0)
//		{
//			if (null != myService)
//			{
//				if (myService.send(d3Request.toString()))
//				{
//					return true;
//				} else
//				{
//					sendIntentCommandBeenSend(-1);
//				}
//			} else
//			{
//				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
//			}
//		}else{
//			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
//		}
//		return false;
//	};
//
//	private boolean sendMessageToServer(D3Request d3Request)
//	{
//		if(sending == 0)
//		{
//			if (null != myService)
//			{
//				if (myService.send(d3Request.toString()))
//				{
//					return true;
//				}
//			} else
//			{
//				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
//			}
//		}else{
//			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
//		}
//		return false;
//	};

	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	private void animateEntry()
	{
		LinearLayout title = (LinearLayout) findViewById(R.id.scriptsHeaderFrame);
		Animation slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);

		if (title.getVisibility() == View.INVISIBLE) {
			title.setVisibility(View.VISIBLE);
			title.startAnimation(slideUp);
		}

		FrameLayout frameOne = (FrameLayout) findViewById(R.id.scriptsInfoFrame);
		Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);
		frameOne.startAnimation(fadeIn);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.ADD_NEW_SCRIPT){
			if(resultCode == RESULT_OK){
				finish();
			}
		}
	}
}
