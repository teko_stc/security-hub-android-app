package biz.teko.td.SHUB_APP.D3

import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONException
import org.json.JSONObject
import kotlin.coroutines.CoroutineContext

class D3FCM(_d3Service: D3Service) : CoroutineScope {
    private val d3Service = _d3Service

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO

    fun setFCMToken() {
        launch {
            FirebaseMessaging.getInstance().token.addOnSuccessListener { s: String? ->
                if (s != null) {
                    if (s != "") {
                        val fcmObject = JSONObject()
                        try {
                            fcmObject.put("class_mask", 31)
                            fcmObject.put("id", s)
                            fcmObject.put("client_type", 1)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                        d3Service.send(D3Request("FIREBASE_ID_SET", fcmObject).toString())
                    }
                }
            }
        }
    }

    private fun fcmIdObjectGet(): JSONObject? {
        var fcmToken = ""
        FirebaseMessaging.getInstance().token.addOnSuccessListener {
            fcmToken = it
        }
        if (fcmToken != "") {
            val fcmObject = JSONObject()
            try {
                fcmObject.put("class_mask", 31)
                fcmObject.put("id", fcmToken)
                fcmObject.put("client_type", 1)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            return fcmObject
        }
        return null
    }
}
