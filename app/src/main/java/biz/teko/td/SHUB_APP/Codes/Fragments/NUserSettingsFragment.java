//package biz.teko.td.SHUB_APP.Codes.Fragments;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.ServiceConnection;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ListView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.LinkedList;
//
//import biz.teko.td.SHUB_APP.Codes.Activities.NUserSettingActivity;
//import biz.teko.td.SHUB_APP.Codes.Adapters.UserAddSectionsListAdapter;
//import biz.teko.td.SHUB_APP.D3.D3Request;
//import biz.teko.td.SHUB_APP.D3.D3Service;
//import biz.teko.td.SHUB_APP.D3DB.DBHelper;
//import biz.teko.td.SHUB_APP.D3DB.Section;
//import biz.teko.td.SHUB_APP.D3DB.User;
//import biz.teko.td.SHUB_APP.D3DB.UserInfo;
//import biz.teko.td.SHUB_APP.R;
//import biz.teko.td.SHUB_APP.Utils.Dir.Func;
//import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
//import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
//import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;
//
//public class NUserSettingsFragment extends Fragment {
//    private NMenuListElement renameMenu;
//    private NMenuListElement partitionsMenu;
//    private NMenuListElement deleteMenu;
//    private UserAddSectionsListAdapter userAddSectionsListAdapter;
//    private NUserSettingActivity activity;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.n_fragment_user_settings, container, false);
//        setupView(view);
//        return view;
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        activity = (NUserSettingActivity) getActivity();
//    }
//
//    private void setupView(View view) {
//        String[] menuItemsArray = getResources().getStringArray(R.array.codes_menu_values);
//        renameMenu = view.findViewById(R.id.nMenuRename);
//        renameMenu.setTitle(menuItemsArray[0]);
//        partitionsMenu = view.findViewById(R.id.nMenuPartitions);
//        partitionsMenu.setTitle(menuItemsArray[1]);
//        deleteMenu = view.findViewById(R.id.nMenuDelete);
//        deleteMenu.setTitle(menuItemsArray[2]);
//        view.findViewById(R.id.nButtonBack).setOnClickListener(v -> onBackPressed());
//        setClickListeners();
//    }
//
//    private void setClickListeners() {
//        renameMenu.setOnRootClickListener(() -> activity.replaceFragment(
//                new NUserSettingRenameFragment(), "rename"
//        ));
//        partitionsMenu.setOnRootClickListener(this::showChangeSectionBindingDialog);
//        deleteMenu.setOnRootClickListener(() -> activity.replaceFragment(
//                new NUserSettingDeleteFragment(), "delete"
//        ));
//    }
//
//    private void showChangeSectionBindingDialog() {
//        NDialog dialog = new NDialog(activity, R.layout.n_dialog_user_setting_partitions);
//        ListView sectionsCheckList = dialog.findViewById(R.id.listPartitions);
//        Section[] allSections = activity.getDbHelper().getGuardSectionsForSiteDeviceWithoutZones(activity.getSiteId(), activity.getUser().device);
//        if (allSections != null && allSections.length != 0) {
//            String userSections = activity.getUser().sections.substring(1, activity.getUser().sections.length() - 1);
//            if (!userSections.equals("")) {
//                String[] secIds = userSections.split(", ");
//                LinkedList<Section> sectionsList = new LinkedList<>();
//                for (String id : secIds) {
//                    Section section = activity.getDbHelper().getDeviceSectionById(activity.getUser().device, Integer.valueOf(id));
//                    if (null != section) {
//                        sectionsList.add(section);
//                    }
//                }
//                if (sectionsList.size() != 0) {
//                    for (Section allSection : allSections) {
//                        for (Section section : sectionsList) {
//                            if (section.id == allSection.id && section.device_id == allSection.device_id) {
//                                allSection.checked = true;
//                            }
//                        }
//                    }
//                }
//            }
//            userAddSectionsListAdapter = new UserAddSectionsListAdapter(activity, R.layout.card_for_section_zone_check, allSections);
//            sectionsCheckList.setAdapter(userAddSectionsListAdapter);
//        } else {
//            dialog.findViewById(R.id.noneDevices).setVisibility(View.VISIBLE);
//        }
//
//        dialog.setOnActionClickListener(value -> {
//            switch (value) {
//                case NActionButton.VALUE_CANCEL:
//                    dialog.dismiss();
//                    break;
//                case NActionButton.VALUE_OK:
//                    if (null != userAddSectionsListAdapter) {
//                        Section[] sections = userAddSectionsListAdapter.getSections();
//                        JSONObject commandAddress = new JSONObject();
//                        try {
//                            commandAddress.put("index", activity.getUser().id);
//                            if (sections != null && sections.length != 0) {
//                                JSONArray cSections = new JSONArray();
//                                for (Section section : sections) {
//                                    if (section.checked)
//                                        cSections.put(section.id);
//                                }
//                                if (cSections.length() != 0) {
//                                    if (!cSections.toString().equals(activity.getUser().sections)) {
//                                        commandAddress.put("sections", cSections);
//                                        D3Request d3Request = D3Request.createCommand(
//                                                activity.getUserInfo().roles,
//                                                activity.getUser().device,
//                                                "USER_SET",
//                                                commandAddress,
//                                                Func.getCommandCount(activity));
//                                        if (d3Request.error == null) {
//                                            if (activity.sendCommandToServer(d3Request)) {
//                                                activity.sendIntentCommandBeenSend(1);
//                                                dialog.dismiss();
//                                            }
//                                        } else
//                                            Func.pushToast(activity, d3Request.error, activity);
//                                    }
//                                } else
//                                    Func.pushToast(activity, activity.getString(R.string.USER_ERROR_NO_SECTIONS), activity);
//                            } else
//                                Func.pushToast(activity, activity.getString(R.string.USER_ERROR_NO_SECTIONS), activity);
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    break;
//            }
//        });
//        dialog.show();
//    }
//
//    private void onBackPressed() {
//        activity.finish();
//    }
//}
