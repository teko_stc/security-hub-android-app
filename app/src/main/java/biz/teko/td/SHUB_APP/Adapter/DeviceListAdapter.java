package biz.teko.td.SHUB_APP.Adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cursoradapter.widget.ResourceCursorAdapter;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 04.08.2016.
 */
public class DeviceListAdapter extends ResourceCursorAdapter
{
	Context context;
	Cursor cursor;
	DBHelper dbHelper;
	int layout;
	private Zone zone;

	public DeviceListAdapter(Context context, int layout, Cursor c, int flags)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.layout = layout;
		this.cursor = c;
	}

	public void update(Cursor cursor){
		this.changeCursor(cursor);
	}

	private void setTextViewText(View view, int id, String string){
		TextView textView = (TextView) view.findViewById(id);
		if(textView!=null){
			textView.setText(string==null? "" : string);
		}
	}


	@Override
	public void bindView(View view, final Context context, Cursor cursor)
	{
		dbHelper = DBHelper.getInstance(context);
		zone = dbHelper.getZone(cursor);
		setTextViewText(view, R.id.siteName, zone.name);
		//		setTextViewText(view, R.id.site, Integer.toString(site.id));
		//		view.setOnClickListener(new OnItemClickListener(site.id));
	}



	private class OnItemClickListener implements View.OnClickListener
	{
		private int id;
		private String name;
		public OnItemClickListener(int id) {
			super();
			this.name  = name;
			this.id = id;
		}
		@Override
		public void onClick(View v) {
			// position is an id.
			Toast.makeText(context, name, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint)
	{
		return super.runQueryOnBackgroundThread(constraint);
	}
}
