package biz.teko.td.SHUB_APP.Profile.Security;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ListView;

import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.Nullable;
import androidx.legacy.app.ActivityCompat;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
import biz.teko.td.SHUB_APP.Utils.Other.NListPreference;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardPinActivity;

/**
 * Created by td13017 on 21.08.2017.
 */

public class SecurityPreferenceFragment extends PreferenceFragment {
    private Context context;
    private DBHelper dbHelper;
    private String pref_finger_summary;
    private PrefUtils prefUtils;
    private CheckBoxPreference pref_enable_pin;
    private Preference pref_change_pin;
    private Preference pref_reset_pin;
    private Preference pref_force;
    private CheckBoxPreference pref_finger;
    private NListPreference blockingTimeList;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView list = view.findViewById(android.R.id.list);
        list.setDivider(new ColorDrawable(Color.TRANSPARENT));
        list.setDividerHeight(0);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        setup();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        dbHelper = DBHelper.getInstance(context);
        prefUtils = PrefUtils.getInstance(context);

        addPreferencesFromResource(R.xml.security_preferences_oreo);
        bind();
    }

    private void setup()
    {
        setupDependencies();

        pref_enable_pin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                return doOnClickAction();
            }
        });
        pref_enable_pin.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                return false;
            }
        });

        pref_change_pin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(context, WizardPinActivity.class);
                intent.putExtra("FROM", D3Service.OPEN_FROM_RESET_PIN);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
                return false;
            }
        });

        pref_reset_pin.setOnPreferenceClickListener(preference -> {
            NDialog dialog = new NDialog(context, R.layout.n_dialog_reset_pin);
            NEditText editPW = dialog.findViewById(R.id.passEditText);
            /*TODO
             *  убрать в диалог*/
            TextInputLayout passwordLayout = dialog.findViewById(R.id.nEditPassInput);
            editPW.showKeyboard();
            setErrorNull(editPW, passwordLayout);
            dialog.setOnActionClickListener((value, b) -> {
                switch (value) {
                    case NActionButton.VALUE_CANCEL:
                        Func.hideEditTextKeyboard(editPW);
                        dialog.dismiss();
                        break;
                    case NActionButton.VALUE_OK:
                        newPassword(editPW, dialog, passwordLayout);
                        break;
                }
            });
            editPW.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    newPassword(editPW, dialog, passwordLayout);
                    return true;
                }
                return false;
            });
            dialog.show();
            return false;
        });

        if (null != pref_force) {
            pref_force.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    if (0 != dbHelper.getSitesCount()) {
                        Intent intent = new Intent(context, SecurityForceActivity.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
                    } else {
                        Func.nShowMessage(context, getString(R.string.SPF_NO_OBJECTS_TITLE));
                    }
                    return false;
                }
            });
        }

        final String blockingValue = blockingTimeList.getValue();
        final String[] blockingStrings = getResources().getStringArray(R.array.blocking_time_list);
        blockingTimeList.setSummary(blockingStrings[Integer.valueOf(blockingValue) / 5]);
        blockingTimeList.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(blockingStrings[Integer.valueOf(newValue.toString()) / 5]);
                return true;
            }
        });
    }

    private void setupDependencies()
    {
        pref_enable_pin.setChecked(prefUtils.getPinEnabled());

        if (!available()) {
            pref_finger.setEnabled(false);
            pref_finger.setSummary(pref_finger_summary);
            prefUtils.setFingerprintEnabled(false);
        }
    }

    private void bind()
    {
        pref_enable_pin = (CheckBoxPreference) findPreference("prof_enable_pin");
        pref_change_pin = (Preference) findPreference("prof_change_pin");
        pref_reset_pin = (Preference) findPreference("prof_reset_pin");
        pref_force = (Preference) findPreference("prof_security_force");
        pref_finger = (CheckBoxPreference) findPreference("prof_finger");
        blockingTimeList = (NListPreference) findPreference("prof_blocking_time");
    }

    private void setErrorNull(NEditText edit, TextInputLayout passwordLayout) {
        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                passwordLayout.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void newPassword(NEditText editPW, NDialog dialog, TextInputLayout passwordLayout) {
        if (0 != editPW.getText().length()) {
            String ps = Func.md5(editPW.getText().toString());
            if (null != ps) {
                UserInfo userInfo = dbHelper.getUserInfo();
                if (0 == (userInfo.userPassword).compareTo(ps)) {
                    /*yes am*/
                    dbHelper.setUserInfoPIN("");
                    Func.pushToast(context, getString(R.string.PA_ON_CHANGE_SUCCESS));
                    Func.hideEditTextKeyboard(editPW);
                    dialog.dismiss();
                } else {
                    showErrorAndKeyboard(editPW, passwordLayout);
                    Func.pushToast(context, getString(R.string.PA_PASS_CHANGE_WRONG_PASS));
                }
            } else {
                showErrorAndKeyboard(editPW, passwordLayout);
                Func.pushToast(context, getString(R.string.PA_PASS_CHANGE_WRONG_PASS));
            }
        } else {
            showErrorAndKeyboard(editPW, passwordLayout);
            Func.pushToast(context, getString(R.string.PA_PASS_CHANGE_ENTER_OLD_PASS));
        }
    }

    private void showErrorAndKeyboard(NEditText editPW, TextInputLayout passwordLayout) {
        editPW.setFocus();
        passwordLayout.setError(" ");
    }

    private boolean doOnClickAction() {
        boolean value = prefUtils.getPinEnabled();
        if (value) {
            showPinDisableWarningDialog();
        } else {
            showPinEnableDialog();
        }
        return true;
    }

    private void showPinEnableDialog() {
        prefUtils.setPinEnabled(true);
        setupDependencies();

        NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_vertical_small);
        nDialog.setTitle(getResources().getString(R.string.SPF_ENABLE_PIN_DIALOG));
        nDialog.setPositiveButton(getResources().getString(R.string.N_SPF_PIN_ENABLE_DIALOG_OK), () -> {
            nDialog.dismiss();
            Intent intent = new Intent(context, WizardPinActivity.class);
            intent.putExtra("FROM", D3Service.OPEN_FROM_RESET_PIN);
            startActivity(intent);
            getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
        });
        nDialog.setNegativeButton(getResources().getString(R.string.N_SPF_PIN_ENABLE_DIALOG_CANCEL), new NDialog.OnButtonClick()
        {
            @Override
            public void onClick()
            {
                nDialog.dismiss();
            }
        });
        nDialog.show();
    }

    private void showPinDisableWarningDialog() {
        if (((KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE)).isKeyguardSecure()) {
            NDialog nDialog = new NDialog(context, R.layout.pin_protection_info);
            NEditText editPW = nDialog.findViewById(R.id.passEditText);
            TextInputLayout passwordLayout = nDialog.findViewById(R.id.nEditPassInput);
            editPW.showKeyboard();
            setErrorNull(editPW, passwordLayout);
            nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
            {
                @Override
                public void onActionClick(int value, boolean b)
                {
                    switch (value){
                        case NActionButton.VALUE_OK:
                            if (editPW.getText().toString().length() == 0) {
                                Func.pushToast(context, getString(R.string.ERROR_ENTER_PASS));
                                passwordLayout.setError(" ");
                            } else {
                                UserInfo userInfo = dbHelper.getUserInfo();
                                if (!userInfo.userPassword.equals(Func.md5(editPW.getText().toString().trim()))) {
                                    Func.pushToast(context, getString(R.string.ERROR_WRONG_PASS));
                                    passwordLayout.setError(" ");
                                } else {
                                    Func.hideEditTextKeyboard(editPW);
                                    prefUtils.setPinEnabled(false);
                                    setupDependencies();
                                    dbHelper.setUserInfoPIN("");
                                    nDialog.dismiss();
                                }
                            }
                            break;
                        case NActionButton.VALUE_CANCEL:
                            Func.hideEditTextKeyboard(editPW);
                            nDialog.dismiss();
                            break;
                    }
                }
            });

            nDialog.show();
        } else {
            NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_vertical_small);
            nDialog.setTitle(getResources().getString(R.string.PIN_DISABLE_DIALOG_TURN_ON_PHONE_PIN_MESS));
            nDialog.setNegativeButton("");
            nDialog.setPositiveButton(getResources().getString(R.string.N_BUTTON_OK), new NDialog.OnButtonClick()
            {
                @Override
                public void onClick()
                {
                    nDialog.dismiss();
                }
            });

        }
    }

    private boolean available() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Get an instance of KeyguardManager and FingerprintManager//
            KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
            FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
            if (null != fingerprintManager) {
                //checkStates whether the device has a fingerprint sensor//
                if (!fingerprintManager.isHardwareDetected()) {
                    // If a fingerprint sensor isn’t available, then inform the user that they’ll be unable to use your app’s fingerprint functionality//
                    pref_finger_summary = context.getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NO_FP_MODULE);
                } else {
                    //checkStates whether the user has granted your app the USE_FINGERPRINT permission//
                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        // If your app doesn't have this permission, then display the following text//
                        pref_finger_summary = context.getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NO_RULES);
                    } else {

                        //checkStates that the user has registered at least one fingerprint//
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            // If the user hasn’t configured any fingerprints, then display the following message//
                            pref_finger_summary = context.getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NO_FINGERPINTS);
                        } else {

                            //checkStates that the lockscreen is secured//
                            if (!keyguardManager.isKeyguardSecure()) {
                                // If the user hasn’t secured their lockscreen with a PIN password or pattern, then display the following text//
                                pref_finger_summary = context.getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NO_PIN);
                            } else {
                                return true;
                            }
                        }
                    }
                }
            } else {
                pref_finger_summary = context.getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_COMMON);
            }
        } else {
            pref_finger_summary = context.getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NOT_SUPORT_IN_ANDROID_VERSION);
        }

        return false;
    }


}
