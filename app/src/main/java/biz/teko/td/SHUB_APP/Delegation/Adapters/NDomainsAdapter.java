package biz.teko.td.SHUB_APP.Delegation.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Domain;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

/**
 * Created by td13017 on 12.04.2018.
 */

public class NDomainsAdapter extends ResourceCursorAdapter
{
	private final Context context;
	private final DBHelper dbHelper;
	private Cursor cursor;
	private boolean editable = false;
	private int sending = 0;
	private D3Service myService;
	private  int siteId;
	private OnItemClickListener onItemClickListener;

//	public void setOnItemClickListener(OnItemClickListener onItemClickListener){
//		this.onItemClickListener = onItemClickListener;
//	}

	public interface OnItemClickListener{
		void onClick(int id);
	}

	public NDomainsAdapter(Context context, int layout, Cursor c, int flags, int siteId, OnItemClickListener onItemClickListener)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.cursor = c;
		this.siteId = siteId;
		this.onItemClickListener = onItemClickListener;
	}

	public void update(Cursor c){
		this.changeCursor(c);
	}

	@Override
	public void bindView(View view, final Context context, Cursor cursor)
	{
		final Domain domain = new Domain(cursor);

		if(null!=domain){
			NMenuListElement element = view.findViewById(R.id.nListElement);
			if(null!=element){
				element.setTitle(domain.name);
				element.setSubtitle(context.getString(R.string.DELEGATE_DOMAIN_NUMBER_TEXT) + String.valueOf(domain.id));
				element.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						onItemClickListener.onClick(domain.id);
					}
				});
			}
		}
	}
}
