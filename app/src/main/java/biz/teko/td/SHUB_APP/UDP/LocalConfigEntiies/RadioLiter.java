package biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies;

import java.util.Map;

public class RadioLiter extends BaseBooleanConfigEntity {

    public RadioLiter() {
        map.put(false, "3");
        map.put(true, "1");
        setValues();
    }

    public boolean get(byte flag) {
        now = ((1 << 5 & flag) > 0);
        return now;
    }

    public void set(byte[] flags, String choice) {
        for (Map.Entry<Boolean, String> entry : map.entrySet()) {
            if (entry.getValue().equals(choice))
                now = entry.getKey();
        }
        byte mask;
        mask = (byte) (1 << 5);
        if (now)
            flags[0] = (byte) (flags[0] | mask);
        else
            flags[0] = (byte) (flags[0] & ~mask);
    }
}
