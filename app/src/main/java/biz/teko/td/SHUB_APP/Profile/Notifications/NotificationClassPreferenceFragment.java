package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.Other.NListPreference;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 11.08.2017.
 */

public class NotificationClassPreferenceFragment extends PreferenceFragment {

    private Context context;
    private int classId;
    private OnChangeListener onChangeListener;
    private NotificationClassActivity activity;
    private OnPriorityChangeListener onPriorityChangeListener;
    private OnModeChangeListener onModeChangeListener;
    private OnGrantPermissionListener onGrantPermissionListener;
	private PrefUtils prefUtils;

	public interface OnChangeListener {
        void callback();
    }

    public interface OnGrantPermissionListener
	{
        void checkGrantPermission();
    }

    public void setOnChangeListener(OnChangeListener onChangeListener) {
        this.onChangeListener = onChangeListener;
    }

    public void setOnGrantPermissionListener(OnGrantPermissionListener onGrantPermissionListener) {
        this.onGrantPermissionListener = onGrantPermissionListener;
    }

    public interface OnPriorityChangeListener {
        void callback();
    }

    ;

    public void setOnPriorityChangeListener(OnPriorityChangeListener onChangeListener) {
        this.onPriorityChangeListener = onChangeListener;
    }

    public interface OnModeChangeListener {
        void callback();
    }

    ;

    public void setOnModeChangeListener(OnModeChangeListener onModeChangeListener) {
        this.onModeChangeListener = onModeChangeListener;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView list = view.findViewById(android.R.id.list);
        list.setDivider(new ColorDrawable(Color.TRANSPARENT));
        list.setDividerHeight(0);
    }


    public static Fragment newInstance(String eClass, int id) {
        NotificationClassPreferenceFragment notificationClassPreferenceFragment = new NotificationClassPreferenceFragment();
        Bundle b = new Bundle();
        b.putString("class", eClass);
        b.putInt("classId", id);
        notificationClassPreferenceFragment.setArguments(b);
        return notificationClassPreferenceFragment;
    }

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		context = getActivity();
		prefUtils  = PrefUtils.getInstance(context);
		activity = ((NotificationClassActivity )context);

		classId = getArguments().getInt("classId");

		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            addPreferencesFromResource(R.xml.notification_class_preferences);
            setPreferencesList();
        } else {
			addPreferencesFromResource(R.xml.notification_class_preferences_oreo);
			setPreferencesListOreo();
		}
	}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        view.setBackgroundColor(getResources().getColor(R.color.n_light_grey));

        ListView lv = view.findViewById(android.R.id.list);
        lv.setPadding(0, Func.GetPixelFromDips(11, context), 0, Func.GetPixelFromDips(28, context));

        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setPreferencesListOreo() {

        switch (activity.getImportance()) {
            case NotificationManager.IMPORTANCE_HIGH:
                setHighImportancePrefs();
                break;
            case NotificationManager.IMPORTANCE_DEFAULT:
                setDefaultImportancePrefs();
                break;
            case NotificationManager.IMPORTANCE_LOW:
                setLowImportancePrefs();
                break;
            case NotificationManager.IMPORTANCE_MIN:
                setMinImportancePrefs();
                break;
        }
    }

	private void setMinImportancePrefs() {
		PreferenceScreen preferenceScreen = getPreferenceScreen();

		setImportanceList(preferenceScreen);
		deleteRingtone(preferenceScreen);
		deleteTts(preferenceScreen);
		deleteFullScreenRingtone(preferenceScreen);
		deleteVibro(preferenceScreen);
		deleteLights(preferenceScreen);
		setBadge(preferenceScreen);
	}

	private void setLowImportancePrefs() {
		PreferenceScreen preferenceScreen = getPreferenceScreen();

		setImportanceList(preferenceScreen);
		deleteRingtone(preferenceScreen);
		deleteTts(preferenceScreen);
		deleteFullScreenRingtone(preferenceScreen);
		deleteVibro(preferenceScreen);
		deleteLights(preferenceScreen);
		setBadge(preferenceScreen);
	}

	private void setDefaultImportancePrefs() {
		PreferenceScreen preferenceScreen = getPreferenceScreen();

		setImportanceList(preferenceScreen);
		setRingtone(preferenceScreen, getResources().getString(R.string.NOTIF_PREF_OREO_SOUND_TITLE));
		setTts(preferenceScreen);
		if (classId < 5) {
			setFullScreenAlarm(preferenceScreen);
		}
		setVibro(preferenceScreen);
		setLights(preferenceScreen);
		setBadge(preferenceScreen);
	}

	private void setHighImportancePrefs() {
		PreferenceScreen preferenceScreen = getPreferenceScreen();

		setImportanceList(preferenceScreen);
		setRingtone(preferenceScreen, getResources().getString(R.string.NOTIF_PREF_OREO_SOUND_TITLE));
		setTts(preferenceScreen);
		if (classId < 5) {
			setFullScreenAlarm(preferenceScreen);
		}
		setVibro(preferenceScreen);
		setLights(preferenceScreen);
		setBadge(preferenceScreen);
	}

    private void deleteLights(PreferenceScreen preferenceScreen) {
        SwitchPreference lightsPreference = (SwitchPreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_LIGHTS));
        if (null != lightsPreference) preferenceScreen.removePreference(lightsPreference);
    }

    private void deleteVibro(PreferenceScreen preferenceScreen) {
        SwitchPreference vibroPreference = (SwitchPreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_VIBRO));
        if (null != vibroPreference) preferenceScreen.removePreference(vibroPreference);
    }

    private void deleteTts(PreferenceScreen preferenceScreen) {
        SwitchPreference ttsPreference = (SwitchPreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_TTS));
        if (null != ttsPreference) preferenceScreen.removePreference(ttsPreference);
    }

    private void deleteRingtone(PreferenceScreen preferenceScreen) {
        RingtonePreference ringtonePreference = (RingtonePreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_RINGTONE));
        if (null != ringtonePreference) preferenceScreen.removePreference(ringtonePreference);
    }

    private void deleteFullScreenRingtone(PreferenceScreen preferenceScreen) {
        RingtonePreference ringtonePreference = (RingtonePreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_ALARMALARM_RINGTONE));
        if (ringtonePreference != null) preferenceScreen.removePreference(ringtonePreference);
    }

	private void setFullScreenAlarm(PreferenceScreen preferenceScreen)
	{
		RingtonePreference ringtoneAlarm = getAlarmRingtonePreference(preferenceScreen);
		NListPreference intervalAlarm = getAlarmIntervalPreference();
		setAlarmPreference(preferenceScreen, ringtoneAlarm, intervalAlarm);
	}

	private void setAlarmPreference(PreferenceScreen preferenceScreen, RingtonePreference ringtone, ListPreference interval) {
		NListPreference alarm = (NListPreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_ALARMALARM_MODE));
		if(null== alarm) {
			alarm = new NListPreference(context);
			alarm.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_ALARMALARM_MODE));
			alarm.setLayoutResource(R.layout.n_custom_preference_layout_grey);
			alarm.setTitle(getRes(context).getString(R.string.NOTIF_ALARM));
			alarm.setEntries(getRes(context).getStringArray(R.array.pref_alarm_notif));
			alarm.setEntryValues(getRes(context).getStringArray(R.array.pref_alarm_notif_values));
			alarm.setDialogTitle(getRes(context).getString(R.string.NOTIF_ALARM_DIALOG_TITLE));
			alarm.setDefaultValue(classId == 0 ? getRes(context).getStringArray(R.array.pref_alarm_notif_values)[2] : getRes(context).getStringArray(R.array.pref_alarm_notif_values)[0]);
			preferenceScreen.addPreference(alarm);
			preferenceScreen.addPreference(interval);
			preferenceScreen.addPreference(ringtone);
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && !Settings.canDrawOverlays(getActivity()))
		{
			alarm.setValue(getResources().getStringArray(R.array.pref_alarm_notif_values)[0]);
		}
		alarm.setSummary(alarm.getEntry());
//		interval.setSummary(interval.getEntry());
		setAlarmLimitations(ringtone, interval, Integer.parseInt(alarm.getValue()));

		NListPreference finalAlarm = alarm;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			finalAlarm.setPermission(Settings.canDrawOverlays(context));
		}
		finalAlarm.setOnPreferenceChangeListener((preference, newValue) -> false);
		finalAlarm.getChangeListener().subscribe(value -> {
			if (value >= 0) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
					if (Settings.canDrawOverlays(getActivity())) {
						setAlarmLimitations(ringtone, interval, value);
						return;
					} else {
						onGrantPermissionListener.checkGrantPermission();
						return;
					}
				}
			}else{
				onGrantPermissionListener.checkGrantPermission();
				return;
			}
			setAlarmLimitations(ringtone, interval, value);
			return;
		}).isDisposed();
		activity.getNotifSubject().subscribe(value->{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			{
				finalAlarm.setPermission(Settings.canDrawOverlays(context));
			}
		}).isDisposed();
	}

	private void setAlarmLimitations(RingtonePreference ringtone, ListPreference interval, int value) {
		if (value == 0) {
			ringtone.setEnabled(false);
			interval.setEnabled(false);
		} else if (value == 1) {
			ringtone.setEnabled(true);
			interval.setEnabled(false);
		} else if (value == 2) {
			ringtone.setEnabled(true);
			interval.setEnabled(true);
		}
		prefUtils.setClassNotificationAlarmMode(Integer.toString(value), classId);
	}

	private RingtonePreference getAlarmRingtonePreference(PreferenceScreen preferenceScreen) {
		RingtonePreference ringPreference = (RingtonePreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_ALARMALARM_RINGTONE));
		if (null == ringPreference) {
			final RingtonePreference ringtonePreference = new RingtonePreference(context);
			ringtonePreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_ALARMALARM_RINGTONE));
			ringtonePreference.setLayoutResource(R.layout.n_custom_preference_layout_grey);
			ringtonePreference.setTitle(getRes(context).getString(R.string.NOTIF_ALARM_RINGTONE));
			ringtonePreference.setDefaultValue(Func.defaultRingtone);

			String ringtonePath = prefUtils.getClassNotifAlarmRingtone(classId);

			if (ringtonePath != null &&(ringtonePath.isEmpty() || ringtonePath.equals(""))) {
				ringtonePreference.setSummary(
						(RingtoneManager.getRingtone(context, Uri.parse(Func.defaultRingtone)).getTitle(context)));
			} else
				ringtonePreference.setSummary(
						(RingtoneManager.getRingtone(context, Uri.parse(ringtonePath)).getTitle(context)));

			ringtonePreference.setOnPreferenceChangeListener((preference, o) -> {
				prefUtils.setClassNotifAlarmRingtone((String) o, classId);
				ringtonePreference.setSummary(RingtoneManager.getRingtone(context, Uri.parse((String) o)).getTitle(context));
				return true;
			});return ringtonePreference;
		}
		return null;
	}

	private NListPreference getAlarmIntervalPreference() {
		NListPreference interval = new NListPreference(context);
		if(null!=interval)
		{
			interval.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_ALARMALARM_INTERVAL));
			interval.setLayoutResource(R.layout.n_custom_preference_layout_grey);
			interval.setTitle(getRes(context).getString(R.string.NOTIF_ALARM_INTERVAL));
			interval.setEntries(getRes(context).getStringArray(R.array.pref_interval_notif));
			interval.setEntryValues(getRes(context).getStringArray(R.array.pref_interval_notif_values));
			interval.setDefaultValue(getRes(context).getStringArray(R.array.pref_interval_notif_values)[0]);
		}
//		interval.setValue(Integer.toString(prefUtils.getClassNotifAlarmInterval(classId)));
		interval.setSummary(getRes(context).getStringArray(R.array.pref_interval_notif)[prefUtils.getClassNotifAlarmInterval(classId) / 5]);
		interval.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object value)
			{
				prefUtils.setClassNotificationAlarmInterval((int)value, classId);
				interval.setSummary(getRes(context).getStringArray(R.array.pref_interval_notif)[(int)value / 5]);
				return true;
			}
		});
		//		interval.getChangeListener().subscribe(value -> {
		//			prefUtils.setClassNotificationAlarmInterval(value, classId);
		//			interval.setSummary(getResources().getStringArray(R.array.pref_interval_notif)[value / 5]);
		//		}).isDisposed();
		return interval;
	}

    @SuppressLint("NewApi")
    private void setBadge(PreferenceScreen preferenceScreen) {
        CheckBoxPreference badgePreference = (CheckBoxPreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_BADGE));
        if (null == badgePreference) {
            badgePreference = new CheckBoxPreference(context);
            badgePreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_BADGE));
            badgePreference.setLayoutResource(R.layout.n_custom_switch_preference_layout_grey);
            badgePreference.setTitle(getResources().getString(R.string.NOTIF_PREF_BAGE_TITLE));
            badgePreference.setChecked(activity.getBadge());
            badgePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    activity.setBadge((boolean) o);
                    onChangeListener.callback();
                    return true;
                }
            });
        }
        preferenceScreen.addPreference(badgePreference);
    }

    @SuppressLint("NewApi")
    private void setLights(PreferenceScreen preferenceScreen) {
        CheckBoxPreference lightsPreference = (CheckBoxPreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_LIGHTS));
        if (null == lightsPreference) {
            lightsPreference = new CheckBoxPreference(context);
            lightsPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_LIGHTS));
            lightsPreference.setLayoutResource(R.layout.n_custom_switch_preference_layout_grey);
            lightsPreference.setTitle(getResources().getString(R.string.NOTIF_PREF_OREO_LIGHTS_TITLE));
            lightsPreference.setChecked(activity.getLights());
            lightsPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    activity.setLights((boolean) o);
                    onChangeListener.callback();
                    return true;
                }
            });
        }
        preferenceScreen.addPreference(lightsPreference);
    }


    @SuppressLint("NewApi")
    private void setVibro(PreferenceScreen preferenceScreen) {
        CheckBoxPreference vibroPreference = (CheckBoxPreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_VIBRO));
        if (null == vibroPreference) {
            vibroPreference = new CheckBoxPreference(context);
            vibroPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_VIBRO));
            vibroPreference.setLayoutResource(R.layout.n_custom_switch_preference_layout_grey);
            vibroPreference.setTitle(getResources().getString(R.string.NOTIF_PREF_OREO_VIBRO_TITLE));
            vibroPreference.setChecked(activity.getVibration());
            vibroPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    activity.setVirbation((boolean) o);
                    onChangeListener.callback();
                    return true;
                }
            });
        }
        preferenceScreen.addPreference(vibroPreference);
    }

    private CheckBoxPreference setTts(PreferenceScreen preferenceScreen) {
        CheckBoxPreference ttsPreference = new CheckBoxPreference(context);
        ttsPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_TTS));
        ttsPreference.setLayoutResource(R.layout.n_custom_switch_preference_layout_grey);
        ttsPreference.setTitle(R.string.NOTIF_PREF_OREO_TTS_TITLE);
        ttsPreference.setDefaultValue(classId < 5);
        preferenceScreen.addPreference(ttsPreference);
        ttsPreference.setOnPreferenceChangeListener((preference, o) -> {
            boolean newValue = (boolean) o;
            if (!newValue && ttsPreference.isEnabled())
                ttsPreference.setSummaryOff("");
            return true;
        });
        return ttsPreference;
    }

    @SuppressLint("NewApi")
    private void setRingtone(PreferenceScreen preferenceScreen, String title) {
        RingtonePreference ringPreference = (RingtonePreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_RINGTONE));
        if (null == ringPreference) {
            final RingtonePreference ringtonePreference = new RingtonePreference(context);
            ringtonePreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_RINGTONE));
            ringtonePreference.setLayoutResource(R.layout.n_custom_preference_layout_grey);
            ringtonePreference.setTitle(title);
            ringtonePreference.setOnPreferenceClickListener(preference -> {
                onModeChangeListener.callback();
                onChangeListener.callback();
                return false;
            });
            ringtonePreference.setOnPreferenceChangeListener((preference, o) -> {
                Uri uriSound = Uri.parse((String) o);
                activity.setSound(uriSound);
                ringtonePreference.setSummary(RingtoneManager.getRingtone(context, uriSound).getTitle(context));
                onChangeListener.callback();
                return true;
            });
            ringtonePreference.setSummary(RingtoneManager.getRingtone(context, activity.getSound()).getTitle(context));
            preferenceScreen.addPreference(ringtonePreference);
        }
    }

    @SuppressLint("NewApi")
    private void setImportanceList(PreferenceScreen preferenceScreen) {
        NListPreference importancelist = (NListPreference) preferenceScreen.findPreference(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_IMPORTANCE));
        if (null == importancelist) {
            importancelist = new NListPreference(context);
            importancelist.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_OREO_IMPORTANCE));
            importancelist.setLayoutResource(R.layout.n_custom_preference_layout_grey);
            importancelist.setTitle(getResources().getString(R.string.NOTIF_PREF_OREO_ACTIONS_TITLE));
            importancelist.setEntries(getResources().getStringArray(R.array.pref_notif_actions_list));
            importancelist.setEntryValues(new CharSequence[]{Integer.toString(NotificationManager.IMPORTANCE_HIGH),
                    Integer.toString(NotificationManager.IMPORTANCE_DEFAULT),
                    Integer.toString(NotificationManager.IMPORTANCE_LOW),
                    Integer.toString(NotificationManager.IMPORTANCE_MIN)});
            preferenceScreen.addPreference(importancelist);
        }
        importancelist.setValue(Integer.toString(activity.getImportance()));
        importancelist.setSummary(importancelist.getEntry());
        importancelist.getChangeListener().subscribe(value -> {
            activity.setImportance(value);
            onPriorityChangeListener.callback();
        }).isDisposed();
    }

    private void setPreferencesList() {
        PreferenceScreen preferenceScreen = getPreferenceScreen();

        CheckBoxPreference vibroPreference = new CheckBoxPreference(context);
        vibroPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_VIBRO));
        vibroPreference.setLayoutResource(R.layout.n_custom_switch_preference_layout_grey);
        vibroPreference.setTitle(R.string.PREF_NOTIF_VIBRO_STATUS_TITLE);
        vibroPreference.setDefaultValue(true);
        vibroPreference.setSummaryOn(getResources().getString(R.string.NOTIFICATION_VIBRO_ENABLED));
        vibroPreference.setSummaryOff(getResources().getString(R.string.NOTIFICATION_VIBRO_DISABLED));
        preferenceScreen.addPreference(vibroPreference);

        CheckBoxPreference soundPreference = new CheckBoxPreference(context);
        soundPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_SOUND_STATUS));
        soundPreference.setLayoutResource(R.layout.n_custom_switch_preference_layout_grey);
        soundPreference.setTitle(R.string.PREF_NOTIF_SOUND_STATUS_TITLE);
        soundPreference.setDefaultValue(true);
        soundPreference.setSummaryOn(getResources().getString(R.string.NOTIFICATION_SOUND_ENABLED));
        soundPreference.setSummaryOff(getResources().getString(R.string.NOTIFICATIN_SOUND_DISABLED));
        preferenceScreen.addPreference(soundPreference);

        CheckBoxPreference ttsPreference = new CheckBoxPreference(context);
        ttsPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_TTS));
        ttsPreference.setLayoutResource(R.layout.n_custom_switch_preference_layout_grey);
        ttsPreference.setTitle(getResources().getString(R.string.NOTIF_PREF_TTS_TITLE));
        ttsPreference.setDefaultValue(classId < 5);
        preferenceScreen.addPreference(ttsPreference);

		setFullScreenAlarm(preferenceScreen);

        RingtonePreference ringtonePreference = new RingtonePreference(context);
        ringtonePreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_RINGTONE));
        ringtonePreference.setLayoutResource(R.layout.n_custom_preference_layout_grey);
        ringtonePreference.setTitle(R.string.PREF_NOTIF_MELODY_TITLE);
        if (soundPreference.isChecked()) {
            ringtonePreference.setEnabled(true);
        } else {
            ringtonePreference.setEnabled(false);
        }
        Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse(prefUtils.getClassNotificationRingtone(classId)));
        if (null != ringtone) {
            ringtonePreference.setSummary(ringtone.getTitle(context));
        }

        ringtonePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                final NotificationClassActivity activity = (NotificationClassActivity) context;
                if (null != activity) {
                    activity.setToSounds(true);
                }
                return false;
            }
        });
        ringtonePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object o) {
                Ringtone ringtone = RingtoneManager.getRingtone(context, Uri.parse((String) o));
                if (ringtone != null)
                    preference.setSummary(ringtone.getTitle(context));
                else
                    preference.setSummary(R.string.SUMMARY_DEFAULT);
                return true;
            }
        });
        preferenceScreen.addPreference(ringtonePreference);

        NListPreference ledPreference = new NListPreference(context);
        ledPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_LED));
        ledPreference.setLayoutResource(R.layout.n_custom_preference_layout_grey);
        ledPreference.setTitle(getResources().getString(R.string.NOTIFICATION_LED));
        ledPreference.setEntryValues(getResources().getStringArray(R.array.hub_led_values));
        ledPreference.setEntries(getResources().getStringArray(R.array.hub_led_colors));
        ledPreference.setDefaultValue("0");
        String led_value = prefUtils.getClassNotificationLed(classId);
        final String[] led_string_values = getResources().getStringArray(R.array.hub_led_colors);
        ledPreference.setSummary(led_string_values[Integer.valueOf(led_value)]);
        ledPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                //				sharedPreferences.edit().putString("notif_led_hub", newValue.toString()).commit();
                preference.setSummary(led_string_values[Integer.valueOf(newValue.toString())]);
                return true;
            }
        });
        preferenceScreen.addPreference(ledPreference);


        NListPreference popupPreference = new NListPreference(context);
        popupPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_POPUP));
        popupPreference.setLayoutResource(R.layout.n_custom_preference_layout_grey);
        popupPreference.setTitle(R.string.PREF_NOTIF_POPUP_TITLE);
        final String[] popup_status_list = getResources().getStringArray(R.array.hub_popup_status_list);
        popupPreference.setEntries(popup_status_list);
        popupPreference.setEntryValues(getResources().getStringArray(R.array.hub_popup_status_values));
        popupPreference.setDefaultValue("0");
        String popup_status_value = prefUtils.getClassNotificationPopUp(classId);
        popupPreference.setSummary(popup_status_list[Integer.valueOf(popup_status_value)]);
        popupPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                preference.setSummary(popup_status_list[Integer.valueOf(newValue.toString())]);
                return true;
            }
        });
        preferenceScreen.addPreference(popupPreference);

        CheckBoxPreference priorityPreference = new CheckBoxPreference(context);
        priorityPreference.setKey(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_PRIORITY));
        priorityPreference.setLayoutResource(R.layout.n_custom_switch_preference_layout_grey);
        priorityPreference.setTitle(R.string.PREF_NOTIF_PRIORITY_TITLE);
        priorityPreference.setDefaultValue(true);
        preferenceScreen.addPreference(priorityPreference);

        ringtonePreference.setDependency(getKey(PrefUtils.KEY_PREF_CLASS_NOTIF_CLASS_SOUND_STATUS));
    }


	private String getKey(String key)
	{
		return prefUtils.getFullClassKey(key, classId);
	}

	private static Resources getRes(Context context)
	{
		return LocaleHelper.onAttach(context).getResources();
	}

}


