package biz.teko.td.SHUB_APP.Codes.Activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.Codes.Adapters.UserAddSectionsListAdapter;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.User;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class NLocalUserSettingsActivity extends BaseActivity
{
    private DBHelper dbHelper;
    private UserInfo userInfo;
    private User user;
    private int sending = 0, userId, deviceId;
    private D3Service myService;
    private NMenuListElement nameView;
    private NMenuListElement partitions;
    private NMenuListElement delete;
    private Context context;
    private boolean bound;
    private ServiceConnection serviceConnection;
    private UserAddSectionsListAdapter userAddSectionsListAdapter;
    private LinearLayout back;
    private Device device;

    private BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            bind();
        }
    };

    private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
        }
    };

    private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            int sending = intent.getIntExtra("command_been_send", 0);
            if(sending!=1)
            {
                switch (sending)
                {
                    case 0:
                        int result = intent.getIntExtra("result", 1);
                        if (result < 1)
                        {
                            Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
                        } else
                        {
                            Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
                        }
                        break;
                    case -1:
                        Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
                        break;
                    default:
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_user_setting);
        dbHelper = DBHelper.getInstance(this);
        context = this;
        this.userInfo = dbHelper.getUserInfo();

        userId = getIntent().getIntExtra("userId", 0);
        deviceId = getIntent().getIntExtra("deviceId", 0);

        setup();
    }

    private void setup() {
        nameView = (NMenuListElement)  findViewById(R.id.nMenuRename);
        partitions = (NMenuListElement) findViewById(R.id.nMenuPartitions);
        delete = (NMenuListElement) findViewById(R.id.nMenuDelete);
        back  = (LinearLayout) findViewById(R.id.nButtonBack);
    }

    private void bind()
    {
        user = dbHelper.getDeviceUserById(userId, deviceId);
        device= dbHelper.getDeviceById(deviceId);

        if(null!=user)
        {
            if (null != nameView)
            {
                nameView.setTitle(user.name);
                nameView.setOnRootClickListener(this::startRenameActivity);
            }
            if(null!=partitions) partitions.setOnRootClickListener(this::showChangeSectionBindingDialog);
            if(null!= delete) delete.setOnRootClickListener(this::startDeleteActivity);

            if (null != back) back.setOnClickListener(v -> onBackPressed());
        }
    }

    private void startDeleteActivity() {
        Intent intent = new Intent(context, NDeleteActivity.class);
        putExtra(intent);
        startActivityForResult(intent, Const.REQUEST_DELETE);
    }

    private void startRenameActivity() {
        Intent intent = new Intent(context, NRenameActivity.class);
        putExtra(intent);
        startActivity(intent, getTransitionOptions(nameView).toBundle());
    }

    private void putExtra(Intent intent) {
        intent.putExtra("type", Const.LOCAL_USER);
        intent.putExtra("local_user", user.id);
        intent.putExtra("device", device.id);
        intent.putExtra("name", user.name);
    }

    private void showChangeSectionBindingDialog() {
        NDialog dialog = new NDialog(this, R.layout.n_dialog_user_setting_partitions);
        ListView sectionsCheckList = dialog.findViewById(R.id.listPartitions);
        Section[] allSections = dbHelper.getGuardSectionsForDeviceWithoutZones(user.device);
        if (allSections != null && allSections.length != 0) {
            String userSections = user.sections.substring(1, user.sections.length() - 1);
            if (!userSections.equals("")) {
                String[] secIds = userSections.split(", ");
                LinkedList<Section> sectionsList = new LinkedList<>();
                for (String id : secIds) {
                    Section section = this.dbHelper.getDeviceSectionById(user.device, Integer.valueOf(id));
                    if (null != section) {
                        sectionsList.add(section);
                    }
                }
                if (sectionsList.size() != 0) {
                    for (Section allSection : allSections) {
                        for (Section section : sectionsList) {
                            if (section.id == allSection.id && section.device_id == allSection.device_id) {
                                allSection.checked = true;
                            }
                        }
                    }
                }
            }
            userAddSectionsListAdapter = new UserAddSectionsListAdapter(context, R.layout.card_for_section_zone_check, allSections);
            sectionsCheckList.setAdapter(userAddSectionsListAdapter);
        } else {
            dialog.findViewById(R.id.noneDevices).setVisibility(View.VISIBLE);
        }

        dialog.setOnActionClickListener((value, b) -> {
            switch (value) {
                case NActionButton.VALUE_CANCEL:
                    dialog.dismiss();
                    break;
                case NActionButton.VALUE_OK:
                    if (null != userAddSectionsListAdapter) {
                        Section[] sections = userAddSectionsListAdapter.getSections();
                        JSONArray sectionsArray = getSectionsArray(sections);
                        if(null!=sectionsArray)
                        {
                            JSONObject commandAddress = new JSONObject();
                            try
                            {
                                commandAddress.put("index", user.id);
                                commandAddress.put("sections", sectionsArray);
                                nSendMessageToServer(device, Command.USER_SET, commandAddress, true);
                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                        }
                        dialog.dismiss();
                    }
                    break;
            }
        });
        dialog.show();
    }

    private JSONArray getSectionsArray(Section[] sections)
    {
        if (sections != null && sections.length != 0) {
            JSONArray cSections = new JSONArray();
            for (Section section : sections) {
                if (section.checked)
                    cSections.put(section.id);
            }
            if (cSections.length() != 0) {
                if (!cSections.toString().equals(user.sections)) {
                    return cSections;
                }
            } else
                Func.pushToast(context, context.getString(R.string.USER_ERROR_NO_SECTIONS), (Activity) context);
        } else
            Func.pushToast(context, context.getString(R.string.USER_ERROR_NO_SECTIONS), (Activity) context);
        return null;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }

    public void  onResume(){
        super.onResume();
        bind();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(D3Service.BROADCAST_DEVICE_USER_UPD);
        intentFilter.addAction(D3Service.BROADCAST_DEVICE_USER_RMV);
        registerReceiver(updateReceiver, intentFilter);
        registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
        registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
        bindD3();
    }

    public void onPause(){
        super.onPause();
        unregisterReceiver(updateReceiver);
        unregisterReceiver(commandResultReceiver);
        unregisterReceiver(commandSendReceiver);
        if(bound)unbindService(serviceConnection);
    }

    private void bindD3(){
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = getServiceConnection();
        bindService(intent, serviceConnection, 0);
    }

    private ServiceConnection getServiceConnection(){
        return new ServiceConnection()
        {
            public void onServiceConnected(ComponentName name, IBinder binder)
            {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name)
            {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
    }

    private D3Service getLocalService()
    {
        if(bound){
            return myService;
        }else{
            bindD3();
            if(null!=myService){
                return myService;
            }
            Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
        }
        return null;

    }

    private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
    {
        return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
    };

    private ActivityOptions getTransitionOptions(View view)
    {
        return ActivityOptions.makeSceneTransitionAnimation((NLocalUserSettingsActivity)context,
                new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Const.REQUEST_DELETE){
            if(resultCode == RESULT_OK){
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }
}
