package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationManagerCompat;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.D3XProtoConstEvent;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NotificationUtils;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import io.reactivex.subjects.PublishSubject;

public class NotificationClassActivity extends BaseActivity
{
//    private static final String KEY_PREF_NOTIF_STATUS = "notif_class_status_";

    private static final int ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST = 5;
    private Context context;
    private EClass eClass;
    private Switch statusSwitch;
    private NotificationManager notificationService;
    private NotificationChannel notificationChannel;
    private Prefs prefs;
    private boolean toSounds = false;
    private PublishSubject<Boolean> notifSubject = PublishSubject.create();
    private LinearLayout backButton;
    private int classId;
    private NotificationUtils notificationUtils;
    private PrefUtils prefUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_notifications_class);
        context = this;

        notificationUtils = NotificationUtils.getInstance(context);
        prefUtils = PrefUtils.getInstance(context);

        prefs = new Prefs();
        initView();
        setClickListeners();

        classId = getIntent().getIntExtra("classId", -1);
        if (-1 != classId) {
            D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
            if (null != d3XProtoConstEvent) {
                eClass = d3XProtoConstEvent.getClassById(classId);
                if (null != eClass) {
                    ImageView classImage = (ImageView) findViewById(R.id.notifClassTitleImage);
                    classImage.setImageResource(getResources().getIdentifier(eClass.iconBig, null, context.getPackageName()));
                    TextView className = (TextView) findViewById(R.id.nTextTitle);
                    className.setText(eClass.caption);

                    statusSwitch = (Switch) findViewById(R.id.notificationStatusSwitch);
                    statusSwitch.setOnClickListener(v -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                            prefUtils.setClassNotificationStatus(classId, statusSwitch.isChecked());
                            prefs.importance = statusSwitch.isChecked() ? NotificationManager.IMPORTANCE_HIGH : NotificationManager.IMPORTANCE_NONE;
                            updateNotificationChannels();
                        }else{
                            prefUtils.setClassNotificationStatus(classId, statusSwitch.isChecked());
                        }
                        updateFragment();
                    });
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!NotificationManagerCompat.from(context).areNotificationsEnabled())
            finish();
        toSounds = false;
        ((App) this.getApplication()).stopActivityTransitionTimer();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            statusSwitch.setChecked(prefUtils.getClassNotificationStatus(eClass.id));
        } else {
            notificationService = ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE));
            getNotificationChannel();
            statusSwitch.setChecked(notificationChannel.getImportance() > NotificationManager.IMPORTANCE_NONE);
        }
        updateFragment();
    }

    private void setClickListeners() {
        backButton.setOnClickListener(view -> onBackPressed());
    }

    private void initView() {
        backButton = (LinearLayout) findViewById(R.id.includeBackButton);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Func.isCurrentVersionSdk(Build.VERSION_CODES.Q)) {
            if (requestCode == ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST)
                notifSubject.onNext(Settings.canDrawOverlays(this));
        }
    }

    private void updateFragment() {
        if (statusSwitch.isChecked()) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                getNotificationChannel();
            } else {
                /*TODO update pref list*/

//                statusSwitch.setVisibility(View.GONE);
            }
            NotificationClassPreferenceFragment notificationClassPreferenceFragment = (NotificationClassPreferenceFragment) NotificationClassPreferenceFragment.newInstance(eClass.caption, eClass.id);
            notificationClassPreferenceFragment.setOnGrantPermissionListener(() -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Settings.canDrawOverlays(this))
                        confirmDialog();
                }
            });
            notificationClassPreferenceFragment.setOnChangeListener(() -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    updateNotificationChannels();
                }
            });
            notificationClassPreferenceFragment.setOnPriorityChangeListener(() -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    updateNotificationChannels();
                }
                updateFragment();
            });
            notificationClassPreferenceFragment.setOnModeChangeListener(() -> toSounds = true);
            getFragmentManager().beginTransaction().replace(R.id.notificationClassFrame, notificationClassPreferenceFragment).commit();
        } else {
            getFragmentManager().beginTransaction().replace(R.id.notificationClassFrame, new NotificationDisabledFragment()).commit();
        }
    }

	private void confirmDialog() {
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_allow);
		nDialog.setTitle(getResources().getString(R.string.NOTIF_DISPLAY_DISPLAY_OVER) + " " + getResources().getString(R.string.application_name));
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_OK:
						Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
								Uri.parse("package:" + getPackageName()));
						startActivityForResult(intent, ACTION_MANAGE_OVERLAY_PERMISSION_REQUEST);
						nDialog.dismiss();
						break;
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}
		});
		nDialog.show();
	}

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getNotificationChannel() {
        notificationChannel = notificationUtils.getClassChannel(eClass.id);
        setPrefs();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void setPrefs() {
        prefs.importance = notificationChannel.getImportance();
        prefs.sound = notificationChannel.getSound();
        prefs.vibrate = notificationChannel.shouldVibrate();
        prefs.light = notificationChannel.shouldShowLights();
        prefs.badge = notificationChannel.canShowBadge();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void updateNotificationChannels() {
        deleteNotificationChannel();
        createNotificationChannel();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void deleteNotificationChannel() {
        notificationUtils.deleteClassChannel(classId);
        notificationUtils.updateClassChannelID(classId); // чтобы при создании , создавался новый канал
//        notificationService.deleteNotificationChannel(notificationChannel.getId());
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotificationChannel() {

        notificationChannel = new NotificationChannel(notificationUtils.getClassChannelId(eClass.id), eClass.caption, prefs.importance);

        notificationChannel.setSound(prefs.sound, new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                .build());
        notificationChannel.enableVibration(prefs.vibrate);
//		notificationChannel.setLockscreenVisibility(prefs.block_screen);
        notificationChannel.enableLights(prefs.light);
        notificationChannel.setShowBadge(prefs.badge);
//		notificationChannel.setBypassDnd(prefs.dnd);
        notificationUtils.createNotificationChannel(eClass.id, notificationChannel);
        setPrefs();
    }

    public void setImportance(int importance) {
        this.prefs.importance = importance;
    }

    public int getImportance() {
        return this.prefs.importance;
    }

    public void setSound(Uri sound) {
        this.prefs.sound = sound;
    }

    public Uri getSound() {
        return this.prefs.sound;
    }

    public boolean getVibration() {
        return this.prefs.vibrate;
    }

    public void setVirbation(boolean virbation) {
        this.prefs.vibrate = virbation;
    }
//	public int getBlockScreenAction()
//	{
//		return this.prefs.block_screen;
//	}
//
//	public void setBlockScreenAction(int block_action){
//		this.prefs.block_screen = block_action;

//	}

    public boolean getLights() {
        return this.prefs.light;
    }

    public void setLights(boolean lights) {
        this.prefs.light = lights;
    }

    public boolean getBadge() {
        return this.prefs.badge;
    }

    public void setBadge(boolean badge) {
        this.prefs.badge = badge;
    }
//	public boolean getDnd()
//	{
//		return this.prefs.dnd;
//	}
//
//
//	public void setDnd(boolean dnd){
//		this.prefs.dnd = dnd;
//	}

//	}

    class Prefs {
        int importance;
        Uri sound;
        boolean vibrate;
        //		int block_screen;
        boolean light;
        boolean badge;
//		boolean dnd;

    }

    @Override
    protected void onPause() {
        if (!toSounds) {
            ((App) this.getApplication()).startActivityTransitionTimer(context);
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }

    public void setToSounds(boolean b) {
        toSounds = b;
    }

    public PublishSubject<Boolean> getNotifSubject() {
        return notifSubject;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}
