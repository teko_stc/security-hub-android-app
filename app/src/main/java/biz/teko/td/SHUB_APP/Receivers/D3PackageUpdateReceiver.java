package biz.teko.td.SHUB_APP.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 12.05.2017.
 */

public class D3PackageUpdateReceiver extends BroadcastReceiver
{
	Context context;
	@Override
	public void onReceive(Context context, Intent intent)
	{
		this.context = context;
		Func.restartD3Service(context, "D3PackageUpdateReveicer");
	}
}
