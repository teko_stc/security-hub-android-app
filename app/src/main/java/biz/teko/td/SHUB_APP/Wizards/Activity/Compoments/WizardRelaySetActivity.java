package biz.teko.td.SHUB_APP.Wizards.Activity.Compoments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.RelaySetViewPagerAdapter;

public class WizardRelaySetActivity extends BaseActivity
{
	private Context context;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private int selDeviceId;
	private int selSectionId;
	private int selRelayConType; // 0 - wireless, 1 - wired
	private String selRelayName;
	private int selExitNumber;
	private int selUIDFormat;
	private String selUID;
	private int output;
	public final  static int PAGER_COUNT  = 3;
	private static final int PAGER_COUNT_NO_SCRIPTS = 2;

	private RelaySetViewPagerAdapter adapter;
	private ViewPager pager;

	private static int pagerCount = PAGER_COUNT ;

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver newZoneRegisteredReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(pager.getCurrentItem() == (pagerCount - 1) )
			{
				if(selDeviceId == intent.getIntExtra("device", 0));
				{
					selSectionId = intent.getIntExtra("section", 0);
					int zone = intent.getIntExtra("zone", 0);
					if (zone != 0)
					{
						setNextPage();
					}
				}
			}
		}
	};

	private BroadcastReceiver interactiveReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if (pager.getCurrentItem() == 0)
			{
				if(selDeviceId == intent.getIntExtra("device", -1))
				{
					int status = intent.getIntExtra("status", -1);
					if (status == 0)
					{
						String jdata = intent.getStringExtra("jdata");
						if (!jdata.equals(""))
						{
							try
							{
								JSONObject jsonObject = new JSONObject(jdata);
								int uid_type = jsonObject.getInt("uid_type");
								if (uid_type == 1)
								{
									String uid = jsonObject.getString("uid");
									int uid_format = 0;
									if(null!=uid) uid_format = Integer.parseInt(uid.substring(0, 2), 16);
									if(uid_format == 2 ){
										if (null != D3Service.d3XProtoConstEvent)
										{
											String ss =uid.substring(uid.length()-4, uid.length());
											String le = "";
											for(int i = ss.length() - 2; i >= 0; i -= 2){
												le += ss.substring(i, i+2);
											}
											int model = Integer.parseInt(le, 16);
											if(model == 8231 || model == 8731){
												selUIDFormat = uid_format;
												selUID = uid;
												setNextPage();
											}else{
												Func.nShowMessage(context, getString(R.string.WIZ_RELAY_SENSOR_INSTEAD_RELAY));
											}
										}
									}else{
										Func.nShowMessage(context, getString(R.string.WIZ_RELAY_SENSOR_INSTEAD_RELAY));
									}
								} else
								{
									if (uid_type == 2)
									{
										Func.nShowMessage(context, getString(R.string.WIZ_ERROR_USER_INSTEAD_ZONE));
									} else
									{
										finish();
									}
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}

						} else
						{
							Func.nShowMessage(context, getString(R.string.WIZ_ERROR_INTERACTIVE_TIMEOUT));
						}
					}

					refreshViews();
				}
			}
		}
	};

	public static int getPagerCount()
	{
		return pagerCount;
	}


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_relay_set);
		context = this;

		pagerCount = Func.needScriptsForBuild() ? PAGER_COUNT : PAGER_COUNT_NO_SCRIPTS;

		serviceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.RELAY_ADD_TITLE);

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		ImageView closeButton = (ImageView) findViewById(R.id.wizCloseButton);
		closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});

		selDeviceId = getIntent().getIntExtra("device_id", -1);
		selRelayConType = getIntent().getIntExtra("con_type", 0);

		adapter = new RelaySetViewPagerAdapter(getSupportFragmentManager());
		pager = (ViewPager) findViewById(R.id.deviceSetViewPager);
		pager.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true;
			}
		});
		pager.setAdapter(adapter);


	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(newZoneRegisteredReceiver, new IntentFilter(D3Service.BROADCAST_ZONE_ADD));
		registerReceiver(interactiveReceiver, new IntentFilter(D3Service.BROADCAST_INTERACTIVE_STATUS));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(newZoneRegisteredReceiver);
		unregisterReceiver(interactiveReceiver);
	}

	@Override
	public void onBackPressed() {
		Func.hideKeyboard((WizardRelaySetActivity)context);
		if (pager.getCurrentItem() == 0) {
			super.onBackPressed();
		} else {
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position - 1);
//			updateIndicators(position - 1);
		}
	}

	public void setNextPage()
	{
		if(pager.getCurrentItem() != pagerCount - 1)
		{
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position + 1);
			refreshViews();
//			updateIndicators(position + 1);
		}else{
			Intent activityIntent = new Intent(getBaseContext(), WizardZoneSetFinishActivity.class);
			activityIntent.putExtra("device", selDeviceId );
			activityIntent.putExtra("con_type", selRelayConType);
			activityIntent.putExtra("wizard", 1);
			startActivityForResult(activityIntent, D3Service.ADD_ZONE_IN_SECTION);
			finish();
		}
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	public int getSectionId(){
		return selSectionId;
	}

	public void setSectionId(int sectionId){
		this.selSectionId = sectionId;
	}

	public int  getDeviceId(){
		return selDeviceId;
	}

	public void  setDeviceid(int device_id){
		this.selDeviceId = device_id;
	}

	public String getRelayName()
	{
		return selRelayName;
	}

	public void setRelayName(String selRelayName)
	{
		this.selRelayName = selRelayName;
	}

	public int getExitNumber()
	{
		return selExitNumber;
	}

	public void setExitNumber(int selExitNumber)
	{
		this.selExitNumber = selExitNumber;
	}

	public int getRelayConType(){return  this.selRelayConType;}

	public void setRelayConType(int selRelayConType)
	{
		this.selRelayConType = selRelayConType;
	}

	public String getUID(){return  this.selUID;}

	public void setUID(String UID)
	{
		this.selUID = UID;
	}

	public int getUIDFormat(){return  this.selUIDFormat;}

	public void setUID(int UIDFormat)
	{
		this.selUIDFormat = UIDFormat;
	}

	public void setRelayOutput(int output){
		this.output = output;
	}

	public int getRelayOutput (){
		return  this.output;
	}


	public void refreshViews(){
		adapter.notifyDataSetChanged();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.ADD_ZONE_IN_SECTION){
			if(resultCode == RESULT_OK){
				int sectionId = data.getIntExtra("section", 0);
				Intent intent = new Intent();
				intent.putExtra("section", sectionId);
				setResult(RESULT_OK, intent);
				finish();
			}
			else{
				onBackPressed();
			}
		}else{
			onBackPressed();
		}
	}

	public void showRKRelaySetDialog(){
		AlertDialog.Builder adb = new AlertDialog.Builder(context);
		adb.setTitle(R.string.TITLE_ATTENTION);
		adb.setMessage(R.string.RELAY_8231_MESS);
		adb.setPositiveButton(R.string.ADB_GOTO, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				finish();
				Intent intent = new Intent(getBaseContext(), WizardZoneSetActivity.class);
				startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
				((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
			}
		});
		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				dialogInterface.dismiss();
			}
		});
		adb.show();
	}

}
