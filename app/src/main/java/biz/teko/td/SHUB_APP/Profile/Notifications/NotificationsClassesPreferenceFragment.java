package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.view.View;
import android.widget.ListView;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.D3XProtoConstEvent;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 11.08.2017.
 */

public class NotificationsClassesPreferenceFragment extends PreferenceFragment
{
	private Context context;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.notification_types_preferences);
		context = getActivity();

		PreferenceScreen preferenceScreen = getPreferenceScreen();

		D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
		if(null!=d3XProtoConstEvent){
			final LinkedList<EClass> eClasses = d3XProtoConstEvent.classes;
			for(final EClass eClass : eClasses){
				if(App.getMode() || (eClass.id < 5))
				{
					Preference classPreference = new Preference(context);
					classPreference.setKey("notif_class_pref" + eClass.id);
					classPreference.setTitle(eClass.caption);
					classPreference.setLayoutResource(R.layout.n_custom_preference_layout);
					classPreference.setOnPreferenceClickListener(preference -> {
						Intent intent = new Intent(context, NotificationClassActivity.class);
						intent.putExtra("classId",eClass.id);
						startActivity(intent);
						getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
						return true;
					});
					preferenceScreen.addPreference(classPreference);
				}
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		if (!NotificationManagerCompat.from(context).areNotificationsEnabled()) {
			getActivity().finish();
		}
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ListView list = view.findViewById(android.R.id.list);
		list.setDivider(new ColorDrawable(Color.TRANSPARENT));
		list.setDividerHeight(0);
	}
}
