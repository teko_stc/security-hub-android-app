package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor; /**
 * Created by td13017 on 14.07.2016.
 */
public class Site extends D3Element
{
	public int is_crossing,  domain, contract_id, contract_status, type, arm_mode,category;
	public String name,  comment, extra;
	public Device[] devices;
	public Address address;
	public int delegated = 0;

	public Site(){}

	public Site(int id, String name){
		this.id = id;
		this.name = name;
	}

	public Site(int id, int contract_id, int contract_status, String name, String comment, String extra, Device[] devices, Address address, int is_crossing, int domain, int delegated,int type, int category, int arm_mode){
		this.id= id;
		this.contract_id = contract_id;
		this.contract_status = contract_status;
		this.name = name;
		this.comment = comment;
		this.extra = extra;
		this.is_crossing = is_crossing;
		this.devices = devices;
		this.address =address;
		this.domain = domain;
		this.delegated = delegated;
		this.category = category;
		this.arm_mode = arm_mode;
		this.type = type;
	}

//	public Site(int id, int contract_id, int contract_status, String name, String comment, String extra, int is_crossing, int domain, int category, int delegated, int arm_mode, int type)
//	{
//		this.id= id;
//		this.contract_id = contract_id;
//		this.contract_status = contract_status;
//		this.name = name;
//		this.comment = comment;
//		this.extra = extra;
//		this.is_crossing = is_crossing;
//		this.domain = domain;
//		this.delegated = delegated;
//		this.category = category;
//		this.arm_mode = arm_mode;
//		this.type = type;
//	}

	/*site constructor from db*/
	public Site(Cursor cursor)
	{
		try
		{
			this.id= cursor.getInt(1);
			this.contract_id = cursor.getInt(2);
			this.contract_status = cursor.getInt(3);
			this.name = cursor.getString(4);
			this.comment = cursor.getString(5);
			this.extra = cursor.getString(6);
			this.is_crossing = cursor.getInt(7);
			this.domain =cursor.getInt(8);
			this.category = cursor.getInt(10);
			this.arm_mode = cursor.getInt(11);
			this.delegated = cursor.getInt(12);
			this.type = cursor.getInt(13);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

}
