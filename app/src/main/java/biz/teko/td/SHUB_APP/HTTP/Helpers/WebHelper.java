package biz.teko.td.SHUB_APP.HTTP.Helpers;

import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.HTTP.CamerasApi;
import biz.teko.td.SHUB_APP.HTTP.Models.IVResponse;
import biz.teko.td.SHUB_APP.HTTP.Models.IVResult;
import biz.teko.td.SHUB_APP.HTTP.Models.IVSetBody;
import biz.teko.td.SHUB_APP.HTTP.WebApi;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by td13017 on 22.02.2018.
 */
/*
* Camera Retrofit Helper
*
* */
public class WebHelper
{
	public static final String BROADCAST_CAMERA_GET = "Broadcast cameras";

	private static CamerasApi camerasApi;
	private static WebApi webApi;
	private static WebHelper instance;
	private final Retrofit cameras;
	private final Retrofit web;
	private String baseURL_cloud = "https://cloud.security-hub.ru/";
	private String baseURL_cameras = "https://openapi-alpha-eu01.ivideon.com/";
	private static String CAMERAS_LIST_OP = "FIND";
	private static String CAMERAS_SET_OP= "SET";
//	private static String access_token = "100-U033d1a35-7d1b-4ff7-b780-84b80f8f6364";
	private static String WEB_API_TOKEN = "573464d333882d4f69bac13b4867d681";

	public WebHelper(){
		Gson gson = new GsonBuilder()
				.setLenient()
				.create();

		cameras = new Retrofit.Builder()
				.baseUrl(baseURL_cameras)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();
		camerasApi = cameras.create(CamerasApi.class);

		web = new Retrofit.Builder()
				.baseUrl(baseURL_cloud)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.build();
		webApi = web.create(WebApi.class);
	}

	public static WebHelper getInstance(){
		if(instance == null){
			instance = new WebHelper();
		}
		return  instance;
	}

	public CamerasApi getCamerasApi(){
		return  camerasApi;
	}

	public WebApi getWebApi(){
		return  webApi;
	}

	public void setLogs(String logs){
		Call<ResponseBody> setLogs = getWebApi().setLogs(logs, WEB_API_TOKEN);
		setLogs.enqueue(new Callback<ResponseBody>()
		{
			@Override
			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
			{
				Func.log_d(D3Service.LOG_TAG + " WebHelper", "Response result " + response.code());
			}

			@Override
			public void onFailure(Call<ResponseBody> call, Throwable t)
			{
				sendHttpConnectionFailedBroad();
			}
		});
	}

	public IVSetBody getRequestBody(String value){
		IVSetBody ivSetBody = new IVSetBody();
		ivSetBody.value = value;
		return ivSetBody;
	}

	public void getCamerasList(String access_token){
		Call<IVResponse> getCamerasList = getCamerasApi().getCamerasList(CAMERAS_LIST_OP, access_token);
		getCamerasList.enqueue(new Callback<IVResponse>()
		{
			@Override
			public void onResponse(Call<IVResponse> call, Response<IVResponse> response)
			{
				if (response.code() == Const.IV_CAMERA_GET_SUCCESS)
				{
					IVResponse ivResponse = response.body();
					IVResult responceObject = ivResponse.result;
					IVResult[] items = responceObject.items;
					DBHelper.getInstance(App.getContext()).updateIVCamerasActual(0);
					if ((items != null) && (items.length != 0))
					{
						int count = items.length;
						for (int i = 0; i < count; i++)
						{
							IVResult ivResult = items[i];
							Camera camera = new Camera();
							camera.id = ivResult.id;
							camera.name = ivResult.name;
							camera.status = ivResult.status? 1 : 0;
//							getCameraPreview(camera.id);
							DBHelper dbHelper = DBHelper.getInstance(App.getContext());
							dbHelper.addIVCamera(camera);
						}
						Func.log_v(D3Service.LOG_TAG, "GOT IT !!" + new Gson().toJson(responceObject));
					}
					DBHelper.getInstance(App.getContext()).deleteNotActualIVCameras();
				}
				else {
					//response.code() можно вывести в лог ошибки
					Func.log_v(D3Service.LOG_TAG, "GOT ERROR !!");
					handleHttpError(response.body());
				}

				Intent intent = new Intent(BROADCAST_CAMERA_GET);
				intent .putExtra("result", response.code());
				App.getContext().sendBroadcast(intent);
			}

			@Override
			public void onFailure(Call<IVResponse> call, Throwable t)
			{
				sendHttpConnectionFailedBroad();
			}
		});
	}

	public  void renameCamera(final String camera_id, final String name, String access_token){

		Call<IVResponse> renameCamera = getCamerasApi().renameCamera(camera_id, CAMERAS_SET_OP, access_token, getRequestBody(name));
		renameCamera.enqueue(new Callback<IVResponse>()
		{
			@Override
			public void onResponse(Call<IVResponse> call, Response<IVResponse> response)
			{
				if (response.code() == 200)
				{
					DBHelper dbHelper = DBHelper.getInstance(App.getContext());
					dbHelper.setCameraName(name, camera_id);
				}else{
					handleHttpError(response.body());
				}
				Intent intent = new Intent(BROADCAST_CAMERA_GET);
				intent.putExtra("result", response.code());
				App.getContext().sendBroadcast(intent);
			}

			@Override
			public void onFailure(Call<IVResponse> call, Throwable t)
			{
				Func.log_v(D3Service.LOG_TAG, "GOT ERROR !!");
				sendHttpConnectionFailedBroad();
			}
		});
	}

	private void handleHttpError(IVResponse body)
	{
		/*TODO
		* */
	}


	private void sendHttpConnectionFailedBroad()
	{
		Func.log_v(D3Service.LOG_TAG, "GOT HTTP ERROR !!");

		/*TODO
		* */
	}

//	public void getCameraPreview(String id, String access_token){
//		Call<ResponseBody> getCameraPreview = getApi().getCameraPreview(id, access_token);
//		getCameraPreview.enqueue(new Callback<ResponseBody>()
//		{
//			@Override
//			public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
//			{
//				if (response.code() == 200) {
//					InputStream in = response.body().byteStream();
//					Bitmap bm  = BitmapFactory.decodeStream(in);
//					Headers headers = response.headers();
////					String redirTo = response.headers().get("Location");
//				}
//				else {
//					//response.code() можно вывести в лог ошибки
//					Func.log_v(D3Service.LOG_TAG, "GOT ERROR !!");
//				}
//			}
//
//			@Override
//			public void onFailure(Call<ResponseBody> call, Throwable t)
//			{
//				Func.log_v(D3Service.LOG_TAG, "GOT ERROR !!");
//				sendHttpConnectionFailedBroad();
//			}
//
//
//
//		});
//	}



}
