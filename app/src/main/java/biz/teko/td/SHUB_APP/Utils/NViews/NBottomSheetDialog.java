package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.R;

public  class NBottomSheetDialog extends BottomSheetDialog
{
	public NBottomSheetDialog(@NonNull Context context)
	{
		super(context, R.style.BottomSheetDialogTheme); //set dialog style with transparent background
		this.setOnShowListener(new OnShowListener()
		{
			@Override
			public void onShow(DialogInterface dialog)
			{
				FrameLayout bottomSheet = findViewById(com.google.android.material.R.id.design_bottom_sheet);
				if (null != bottomSheet)
				{
					BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
					behavior.setHideable(false);
				}
			}
		});

		try
		{
			Field mBehaviorField = getClass().getDeclaredField("behavior");
			mBehaviorField.setAccessible(true);

			final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(this);
			behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
			{
				@Override
				public void onStateChanged(@NonNull View bottomSheet, int newState)
				{
					//disable closing by dragging
					if (newState == BottomSheetBehavior.STATE_DRAGGING)
					{
						behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
					}
				}

				@Override
				public void onSlide(@NonNull View bottomSheet, float slideOffset)
				{
				}
			});
		} catch (NoSuchFieldException e)
		{
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}


}
