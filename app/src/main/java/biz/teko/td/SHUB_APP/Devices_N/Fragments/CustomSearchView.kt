package biz.teko.td.SHUB_APP.Devices_N.Fragments

import android.content.Context
import android.util.AttributeSet
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.Group
import androidx.core.widget.doAfterTextChanged
import biz.teko.td.SHUB_APP.R


class CustomSearchView
@JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null
) : ConstraintLayout(context, attributeSet) {
    var searchState = false
        private set

    init {
        inflate(context, R.layout.search_view, this)
    }

    val editText = findViewById<EditText>(R.id.et_search)
    val sqGroup = findViewById<Group>(R.id.gr_search_query)
    val ivSearch = findViewById<ImageView>(R.id.iv_search)
    val ivCancel = findViewById<ImageView>(R.id.iv_cancel)

    init {
        setTextChangeListener()
        setOnEditorActionListener()
        setDrawablesListener()
        editText.setOnFocusChangeListener { v, hasFocus ->
            if (!hasFocus && editText.text.isNullOrBlank())
                hide()
        }
    }

    fun setQueryTextChangeListener(queryTextListener: QueryTextListener) {
        this.queryTextListener = queryTextListener
    }


    private var queryTextListener: QueryTextListener? = null

    private fun setTextChangeListener() {
        editText.doAfterTextChanged {
            if (it.isNullOrBlank()) {
                //hide()
            } else {
                //setRightDrawable(android.R.drawable.ic_menu_close_clear_cancel)
            }
            queryTextListener?.onQueryTextChange(it.toString())
        }
    }

    private fun setOnEditorActionListener() {
        editText.run {
            setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    hideKeyboard()
                    queryTextListener?.onQueryTextSubmit(text.toString())
                    true
                } else {
                    false
                }
            }
        }
    }

    fun hide() {
        sqGroup.visibility = INVISIBLE
        ivSearch.visibility = VISIBLE
        editText.setText("")
        editText.clearFocus()
        hideKeyboard()
        searchState = false
        onSearchStateListener?.invoke(false)
    }

    private var onSearchStateListener: ((Boolean) -> Unit)? = null
    fun setOnSearchStateListener(f: (Boolean) -> Unit) {
        onSearchStateListener = f
    }

    fun expand(keyboard: Boolean = true) {
        ivSearch.run {
            sqGroup.visibility = VISIBLE
            ivSearch.visibility = INVISIBLE
            editText.requestFocus()
            if (keyboard)
                showKeyboard()
        }
        searchState = true
        onSearchStateListener?.invoke(true)
    }

    private fun hideKeyboard() {
        val imm: InputMethodManager? =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(getWindowToken(), 0)
    }

    private fun showKeyboard() {
        val imm: InputMethodManager? =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(editText, 0)
    }

    private fun setDrawablesListener() {
        ivCancel.setOnClickListener {
            hide()
        }
        ivSearch.setOnClickListener {
            expand()
        }
    }

    interface QueryTextListener {

        fun onQueryTextSubmit(query: String?)

        fun onQueryTextChange(newText: String?)

    }

}
