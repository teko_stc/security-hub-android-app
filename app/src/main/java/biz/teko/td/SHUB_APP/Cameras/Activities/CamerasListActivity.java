package biz.teko.td.SHUB_APP.Cameras.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.mikepenz.materialdrawer.DrawerBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.CamerasLoginActivity;
import biz.teko.td.SHUB_APP.Cameras.Fragments.CamerasListFragment;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;

/**
 * Created by td13017 on 15.09.2017.
 * DEPRECATED
 */

public class CamerasListActivity extends BaseActivity
{
	private DBHelper dbHelper;
	private Context context;
	private final  static int DEF_POSITION = 4;
	private DrawerBuilder drawerBuilder;
	private WebHelper webHelper;
	private String access_token;
	private D3Service myService;
	private boolean bound;

	private boolean repeat = true;

	private Button loginButton;
	private TextView noLoginText;
	private ImageView iVExitButton;
	private UserInfo userInfo;
	private final int sending  = 0;
	private ServiceConnection serviceConnection;

	private final BroadcastReceiver IVDelUserReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result > 0){
				getFragmentManager().popBackStackImmediate();
				getFragmentManager().popBackStack();
				getFragmentManager().popBackStack();

				showEmpty();

			}else{
				Func.pushToast(context, Func.handleResult(context, result), (Activity)context);
			}
		}
	};

	private final BroadcastReceiver IVGetTokenResponseFromServer = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getIntExtra("result", 0 ) > 0){
//				07.10.2020
//				moved in d3service
//				getCameras();
			}else{
				showEmpty();
			}
		}
	};

	/*WHEN we get new safe session we open login iv page to login in their system*/
	private final BroadcastReceiver openIVLoginWebPageAfterGetSaveSession = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String session = intent.getStringExtra("session");
			if(null!=session){
				Intent webIntent = new Intent(getBaseContext(), CamerasLoginActivity.class);
				webIntent.putExtra("session", session);
				startActivityForResult(webIntent, D3Service.IV_LOGIN);
			}
		}
	};

	private final BroadcastReceiver camerasGetReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			/*WHEN we get error from chhelper http request for cameras list
			 * it means that token is broken and we need new one */
			if(intent.getIntExtra("result", -1) == Const.IV_ERROR_NOT_AUTHORIZED){
				if (repeat)
				{
					repeat=!repeat;
					getToken();
				}
			}
			/*WHEN we get success cameras list request from crhelper
			 * we need info about cameras bindings*/
			else if(intent.getIntExtra("result", -1) == Const.IV_CAMERA_GET_SUCCESS)
			{

//				07.10.2020
//				moved in d3service
//				getRelations();
				showCameras();
			}else{
				showUnknownError();
			}
		}
	};

	private boolean goNext = true;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cameras_list);
		getSwipeBackLayout().setEnableGesture(false);

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.CAMERAS);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});


		setup();
		bind();
	}

	private void setup()
	{
		iVExitButton = (ImageView) findViewById(R.id.buttonIvLogOut);
		noLoginText = (TextView) findViewById(R.id.ivLoginFaultText);
		loginButton = (Button) findViewById(R.id.ivLoginFaultButton);
	}

	private void bind()
	{
		if(null!=iVExitButton)iVExitButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				NDialog adb = new NDialog(context, R.layout.n_dialog_question_small);
				adb.setTitle(getString(R.string.CAMERA_EXIT_IV_DIALOG_MES));
				adb.setOnActionClickListener(new NDialog.OnActionClickListener()
				{
					@Override
					public void onActionClick(int value, boolean b)
					{
						switch (value){
							case NActionButton.VALUE_OK:
								UserInfo userInfo = dbHelper.getUserInfo();
								JSONObject message = new JSONObject();
								try
								{
									message.put("domain", userInfo.domain);
									nSendMessageToServer(null, D3Request.IV_DEL_USER, message, false);
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
								break;
							default:
								break;
						}
						adb.dismiss();
					}
				});
				adb.show();
			}
		});
	}

	private void showEmpty()
	{
		noLoginText.setVisibility(View.VISIBLE);
		if(!((userInfo.roles & Const.Roles.DOMAIN_ADMIN) == 0))
		{
			loginButton.setVisibility(View.VISIBLE);
			loginButton.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					getSafeSession();
				}
			});
		}else{
			loginButton.setVisibility(View.GONE);
		}
	}

	private void showCameras()
	{
		getFragmentManager().beginTransaction().replace(R.id.camerasListFrame, new CamerasListFragment()).addToBackStack("0").commit();
		if (!((userInfo.roles & Const.Roles.DOMAIN_ADMIN) == 0))
		{
			iVExitButton.setVisibility(View.VISIBLE);
		}
	}

	private void showUnknownError()
	{
		/*TODO*/
	}


	private void update()
	{
		if(goNext){
			if(!getCameras()){
				getToken();
			}
		}
	}

	private boolean getCameras()
	{
		String iv_token = dbHelper.getUserInfoIVToken();
		if (!(null == iv_token || iv_token.equals("")))
		{
			webHelper = WebHelper.getInstance();
			webHelper.getCamerasList(iv_token);
			return true;
		}else{
			return false;
		}
	}

	private void getToken()
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject message = new JSONObject();
		try
		{
			message.put("domain", userInfo.domain);
			nSendMessageToServer(null, D3Request.IV_GET_TOKEN, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

//	07.10.2020
//	moved in d3service
//	private void getRelations (){
//		JSONObject message = new JSONObject();
//		try
//		{
//			message.put("device", 0);
//			message.put("section", 0);
//			message.put("zone", 0);
//			nSendMessageToServer(null, D3Request.IV_GET_RELATIONS, message, false);
//		} catch (JSONException e)
//		{
//			e.printStackTrace();
//		}
//	}


	private void getSafeSession()
	{
		nSendMessageToServer(null, D3Request.OPERATOR_SAFE_SESSION, new JSONObject(), false);
	}

	@Override
	public void onBackPressed()
	{
		finish();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		bindD3();
		registerReceiver(openIVLoginWebPageAfterGetSaveSession, new IntentFilter(D3Service.BROADCAST_OPERATOR_SAFE_SESSION));
		registerReceiver(camerasGetReceiver, new IntentFilter(WebHelper.BROADCAST_CAMERA_GET));
		registerReceiver(IVGetTokenResponseFromServer, new IntentFilter(D3Service.BROADCAST_IV_GET_TOKEN));
		registerReceiver(IVDelUserReceiver, new IntentFilter(D3Service.BROADCAST_IV_DEL_USER));

	}

	@Override
	public void onPause()
	{
		super.onPause();
		if(bound)unbindService(serviceConnection);
		unregisterReceiver(IVGetTokenResponseFromServer);
		unregisterReceiver(openIVLoginWebPageAfterGetSaveSession);
		unregisterReceiver(camerasGetReceiver);
		unregisterReceiver(IVDelUserReceiver);
		((App)this.getApplication()).stopActivityTransitionTimer();
	}

	public void bindD3(){
		serviceConnection = getServiceConnection();
		bindService(new Intent(this, D3Service.class), serviceConnection, 0);
	}

	public   ServiceConnection getServiceConnection(){
		return  new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
				/*обновляем список*/
				update();
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.IV_LOGIN){
			if(resultCode == RESULT_OK){
				noLoginText.setVisibility(View.GONE);
				loginButton.setVisibility(View.GONE);
				loginButton.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{

					}
				});
//				update();
			}
			else{
				goNext = false;
				showEmpty();
			}
		}
	}
}
