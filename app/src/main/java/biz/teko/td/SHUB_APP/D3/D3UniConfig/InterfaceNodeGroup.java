package biz.teko.td.SHUB_APP.D3.D3UniConfig;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementListUnion;

import java.util.LinkedList;

/**
 * Created by td13017 on 29.06.2016.
 */
public class InterfaceNodeGroup extends InterfaceNode
{
	@ElementListUnion({
			@ElementList(entry="group", inline=true, type=InterfaceNodeGroup.class, required = false),
			@ElementList(entry="control", inline=true, type=InterfaceNodeControl.class, required = false),
	})
	public LinkedList<InterfaceNode> Members;

}
