package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK;

import android.content.Context;
import android.content.res.Resources;
import android.view.Surface;

import com.company.NetSDK.CB_pfAudioDataCallBack;
import com.company.NetSDK.EM_USEDEV_MODE;
import com.company.NetSDK.FinalVar;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.NET_SPEAK_PARAM;
import com.company.NetSDK.SDKDEV_TALKFORMAT_LIST;
import com.company.NetSDK.SDK_TALK_CODING_TYPE;
import com.company.PlaySDK.IPlaySDK;
import com.company.PlaySDK.IPlaySDKCallBack;

import java.util.ArrayList;

import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NDahuaTalkModule {
	SDKDEV_TALKFORMAT_LIST mTalkFormatList = new SDKDEV_TALKFORMAT_LIST();
	public long mTalkHandle = 0;
	String errMsg = "";
	boolean mOpenAudioRecord = false;
	boolean bTransfer = false;
	CB_pfAudioDataCallBack cbAudioDataCallBack = null;
	IPlaySDKCallBack.pCallFunction cbAudioRecord = null;
	ArrayList<String> channelList = new ArrayList<String>();
	Context mContext;
	Resources res;
	int nPort = 99;
	private long loginHandle;

	public NDahuaTalkModule(Context context, long loginHandle) {
		this.mContext = context;
		this.loginHandle = loginHandle;
		res = mContext.getResources();
		cbAudioDataCallBack = new AudioDataCallBack();
		cbAudioRecord = new AudioRecordCallBack();
	}

	public boolean startClientTalk() {
		///Set talk mode
		mTalkFormatList.type[0].encodeType = SDK_TALK_CODING_TYPE.SDK_TALK_PCM;
		mTalkFormatList.type[0].dwSampleRate = 8000;
		mTalkFormatList.type[0].nAudioBit = 16;
		mTalkFormatList.type[0].nPacketPeriod = 25;
		if(!INetSDK.SetDeviceMode(loginHandle, EM_USEDEV_MODE.SDK_TALK_ENCODE_TYPE, mTalkFormatList.type[0]) ) {
			Func.log_d(Const.LOG_TAG_DAHUA, "Set Talk Encode Mode Failed!");
			errMsg = "Error SDK_TALK_ENCODE_TYPE";
			return false;
		}

		if(!INetSDK.SetDeviceMode(loginHandle, EM_USEDEV_MODE.SDK_TALK_CLIENT_MODE, null)) {
			Func.log_d(Const.LOG_TAG_DAHUA, "Set Talk Client Mode Failed!");
			errMsg = "Error SDK_TALK_CLIENT_MODE";
			return false;
		}

		NET_SPEAK_PARAM stParam = new NET_SPEAK_PARAM();
		stParam.nMode = 0;
		stParam.nEnableWait = 0;
		if(!INetSDK.SetDeviceMode(loginHandle, EM_USEDEV_MODE.SDK_TALK_SPEAK_PARAM, stParam)) {
			Func.log_d(Const.LOG_TAG_DAHUA, "Set Talk Speak Param Failed!");
			errMsg = "Error";
			return false;
		}

		nPort = 0;
		return talk();
	}

	public boolean isTalking() {
		return mTalkHandle != 0;
	}

	private boolean talk() {
		///Start talk
		mTalkHandle = INetSDK.StartTalkEx(loginHandle, cbAudioDataCallBack);
		if (0 != mTalkHandle) {
			///Start audio record
			boolean bSuccess = startAudioRecord();
			if (!bSuccess) {
				Func.log_d(Const.LOG_TAG_DAHUA, "Start Audio Record Failed!");
				INetSDK.StopTalkEx(mTalkHandle);
				errMsg = "Error";
				return false;
			} else {
				mOpenAudioRecord = true;
				errMsg = "Error";
			}
		} else {
			INetSDK.StopTalkEx(mTalkHandle);
			Func.log_d(Const.LOG_TAG_DAHUA, "Start Talk Failed!");
			errMsg = "Error";
			return false;
		}
		return true;
	}

	public boolean stopTalk() {
		if(mOpenAudioRecord) {
			stopAudioRecord();
		}

		if(0 != mTalkHandle) {
			///Stop audio talk to the device
			if(INetSDK.StopTalkEx(mTalkHandle)) {
				mTalkHandle = 0;
				errMsg = "Error";
			} else {
				return false;
			}
		}
		return true;
	}

	///Talk callback
	public class AudioDataCallBack implements CB_pfAudioDataCallBack
	{
		public void invoke(long lTalkHandle, byte pDataBuf[], byte byAudioFlag)
		{
			if(mTalkHandle == lTalkHandle)
			{
				///byAudioFlag Audio data home sign, 0:means audio data collected by local audio recording list; 1:means received audio data sent by devie
				if(1 == byAudioFlag)
				{

					///You can use PLAY SDK to decode to get PCM and then encode to other formats if you get a uniform formats.
					IPlaySDK.PLAYInputData(nPort, pDataBuf, pDataBuf.length);
				}
			}
		}
	}

	private boolean startAudioRecord()	{

		boolean bRet = false;

		///Then specify frame length
		int nFrameLength = 320;

		///Then call PLAYSDK library to begin recording audio
		boolean bOpenRet = IPlaySDK.PLAYOpenStream(nPort,null,0,1024*1024) == 0? false : true;
		if(bOpenRet) {
			boolean bPlayRet = IPlaySDK.PLAYPlay(nPort, (Surface)null) == 0? false : true;
			if(bPlayRet) {
				IPlaySDK.PLAYPlaySoundShare(nPort);
				boolean bSuccess = IPlaySDK.PLAYOpenAudioRecord(cbAudioRecord,mTalkFormatList.type[0].nAudioBit,
						mTalkFormatList.type[0].dwSampleRate, nFrameLength, 0) == 0? false : true;
				if(bSuccess) {
					bRet = true;
					Func.log_d(Const.LOG_TAG_DAHUA, "nAudioBit = " + mTalkFormatList.type[0].nAudioBit + "\n" + "dwSampleRate = "
							+ mTalkFormatList.type[0].dwSampleRate + "\n" + "nFrameLength = " + nFrameLength + "\n");
				} else {
					IPlaySDK.PLAYStopSoundShare(nPort);
					IPlaySDK.PLAYStop(nPort);
					IPlaySDK.PLAYCloseStream(nPort);
				}
			} else {
				IPlaySDK.PLAYCloseStream(nPort);
			}
		}

		return bRet;
	}

	private void stopAudioRecord()	{
		mOpenAudioRecord = false;
		IPlaySDK.PLAYCloseAudioRecord();
		IPlaySDK.PLAYStop(nPort);
		IPlaySDK.PLAYStopSoundShare(nPort);
		IPlaySDK.PLAYCloseStream(nPort);
	}

	public class AudioRecordCallBack implements IPlaySDKCallBack.pCallFunction {
		public void invoke(byte[] pDataBuffer,int nBufferLen, long pUserData) {
			try
			{
				///encode
				Func.log_d(Const.LOG_TAG_DAHUA, "AudioRecord send " + nBufferLen);
				byte encode[] = AudioRecord(pDataBuffer);

				///send user's audio data to device.
				long lSendLen = INetSDK.TalkSendData(mTalkHandle, encode);
				if(lSendLen != (long)encode.length) {
					///Error occurred when sending the user audio data to the device.
				}
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}

	byte[] AudioRecord(byte[] pDataBuffer) {
		int DataLength = pDataBuffer.length;
		byte pCbData[] = null;
		pCbData = new byte[8+DataLength];

		pCbData[0] = (byte) 0x00;
		pCbData[1] = (byte) 0x00;
		pCbData[2] = (byte) 0x01;
		pCbData[3] = (byte) 0xF0;
		pCbData[4] = (byte) 0x0C;

		pCbData[5] = 0x02; // 8k
		pCbData[6]=(byte)(DataLength & 0x00FF);
		pCbData[7]=(byte)(DataLength >> 8);
		System.arraycopy(pDataBuffer, 0, pCbData, 8, DataLength);
		return pCbData;
	}

	///Get talk format list，this demo only use PCM.
	public void getCodeType() {
		if(!INetSDK.QueryDevState(loginHandle, FinalVar.SDK_DEVSTATE_TALK_ECTYPE, mTalkFormatList, 4000)) {
			Func.log_d(Const.LOG_TAG_DAHUA, "QueryDevState TalkList Failed!");
			return;
		}
	}

	/// Get Transfer Mode
//	public List getTransferModeList() {
//		ArrayList<String> outputList = new ArrayList<String>();
//		String[] transferModeNames = res.getStringArray(R.array.transfer_mode_array);
//		for (int i = 0; i < transferModeNames.length; i++) {
//			outputList.add(transferModeNames[i]);
//		}
//		return outputList;
//	}

	///Is Transfer Mode
	public boolean isTransfer(){
		return bTransfer;
	}

	///Get Channel Num
//	private int getChannel(){
//		if (camera == null)
//			return 0;
//		return camera.getDeviceInfo().nChanNum;
//	}

	///Get Error Msg
	public String getErrMsg() {
		return errMsg;
	}

	/// Get Channel List
//	public List getChannelList(){
//		if (!channelList.isEmpty()) {
//			return channelList;
//		}
//		for (int i=0;i<getChannel();i++){
//			channelList.add(res.getString(R.string.channel)+(i));
//		}
//		return channelList;
//	}
}