package biz.teko.td.SHUB_APP.Activity;

import static com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_EXPANDED;
import static com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_SETTLING;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.transition.Transition;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import biz.teko.td.SHUB_APP.Activity.SwipeBack.SwipeBackActivityX;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Callback;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomMenuInfoLayout;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomNavigationView;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 20.07.2017.
 */

public abstract class BaseActivity extends SwipeBackActivityX {
	private ActivityInfo info = null;
	private D3Service myService;
	private Callback onBackPressedCallback;
	private boolean isFirstTutorial;

	public boolean isFirstTutorial() {
		return isFirstTutorial;
	}

	public void setFirstTutorial(boolean firstTutorial) {
		isFirstTutorial = firstTutorial;
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
			}
		};
	}

	public void setOnBackPressedCallback(Callback onBackPressedCallback) {
		this.onBackPressedCallback = onBackPressedCallback;
	}

	@Override
	public void onBackPressed() {
		if (onBackPressedCallback == null)
			super.onBackPressed();
		else
			onBackPressedCallback.complete();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onResume()
	{
		getServiceConnection();
		if(null == myService || null == D3Service.d3XProtoConstEvent) {
//			Func.restartD3Service(getApplicationContext(), "Base");}
			Func.restartD3Service(this, "Base");}
		super.onResume();
		PackageManager packageManager = getPackageManager();
		try
		{
			info = packageManager.getActivityInfo(this.getComponentName(), 0);
			((App)this.getApplication()).setPreviousActivity(info.name);
		} catch (PackageManager.NameNotFoundException e)
		{
			e.printStackTrace();
		}
		((App)this.getApplication()).stopActivityTransitionTimer();

	}

	@Override
	protected void onPause()
	{
		super.onPause();
		((App)this.getApplication()).startActivityTransitionTimer(this);
//		overridePendingTransition(0, 0);
//		unbindService(serviceConnection);
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		((App)this.getApplication()).stopActivityTransitionTimerOnDestroy();
	}



	public void setBottomInfoBehaviour(Context context) {
		setBottomInfoBehaviour((NBottomNavigationView) findViewById(R.id.nBottomMenu), this.findViewById(android.R.id.content), context);
	}

	public void setBottomInfoBehaviour(NBottomNavigationView bottomNavigationView, View view, Context context) {
		boolean alarm;
		boolean attention;

		NBottomMenuInfoLayout bottomInfoView = view.findViewById(R.id.nBottomInfoView);
		ViewGroup bottomInfo = view.findViewById(R.id.nBottomInfo);
		View backGround = view.findViewById(R.id.nBottomCritBackground);

		Cursor alarms = getAlarmEventsCursor(context);
		Cursor attentions = getAttentionEventsCursor(context);
		if (null != attentions && attentions.getCount() > 0) {
			//когда меняется attention экран перекрывается
			attention = true;
			bottomInfoView.setTitle(getString(R.string.N_BOTTOM_STATE_CRIT));
			bottomNavigationView.setItemIconTintList(context.getResources().getColorStateList(R.color.n_bottom_nav_icon_color_attention_selector));
		}else{
			attention = false;
		}


		if(null!=alarms && alarms.getCount() > 0){
            alarm = true;
            bottomInfoView.setTitle(getString(R.string.N_BOTTOM_STATE_ALARM));
            bottomNavigationView.setItemIconTintList(context.getResources().getColorStateList(R.color.n_bottom_nav_icon_color_alarm_selector));
        } else {
            alarm = false;
        }
        bottomInfoView.setAttention(attention);
        bottomNavigationView.setAttention(attention);
        bottomInfoView.setAlarm(alarm);
        bottomNavigationView.setAlarm(alarm);
        BottomSheetBehavior<FrameLayout> bottomSheetBehavior = BottomSheetBehavior.from((FrameLayout) findViewById(R.id.ll));


        if (!(attention || alarm)) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            bottomInfoView.setVisibility(View.GONE);
            findViewById(R.id.ll).setVisibility(View.GONE);
            findViewById(R.id.alarms_container).setVisibility(View.GONE);
            bottomNavigationView.setItemIconTintList(context.getResources().getColorStateList(R.color.n_bottom_nav_icon_color_selector));
            bottomNavigationView.invalidate();
			//resizeWorkFrameDown(view, context);
        } else {
			bottomInfoView.setVisibility(View.VISIBLE);
			findViewById(R.id.ll).setVisibility(View.VISIBLE);
			findViewById(R.id.alarms_container).setVisibility(View.VISIBLE);
			bottomNavigationView.invalidate();
			//resizeWorkFrameUp(view, context);

			Callback backPressedCallback = this.onBackPressedCallback;
			if (PrefUtils.getInstance(context).getUseAM()) {
				if (null != bottomInfoView)
					bottomInfoView.setOnItemsClickListener(new NBottomMenuInfoLayout.OnItemClickListener() {
						@Override
						public void onTitleClick() {
							bottomSheetBehavior.setState(STATE_EXPANDED);
						}

						@Override
						public void onCloseClick() {

						}
					});
			}else {
				if (findViewById(R.id.alarms_container) != null)
					findViewById(R.id.alarms_container).setVisibility(View.GONE);
				findViewById(R.id.nButtonClose).setOnClickListener((v) -> {
					bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
				});
				updateList(view.findViewById(R.id.nBottomAttentionList), context, getCritCursor(context));

				if (null != bottomInfoView)
					bottomInfoView.setOnItemsClickListener(new NBottomMenuInfoLayout.OnItemClickListener() {
						@Override
						public void onTitleClick() {
							bottomSheetBehavior.setState(STATE_EXPANDED);
						}

						@Override
					public void onCloseClick() {
						bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
					}
				});
			}

			LinearLayout cont = (LinearLayout) findViewById(R.id.ll_attention_container);
			bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
				@Override
				public void onStateChanged(@NonNull View bottomSheet, int newState) {

					if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
						backGround.setVisibility(View.GONE);
						bottomInfoView.setAlpha(1);
						setOnBackPressedCallback(backPressedCallback);
					} else if (newState == BottomSheetBehavior.STATE_HALF_EXPANDED) {
						bottomSheetBehavior.setState(STATE_EXPANDED);
					} else if (newState == STATE_EXPANDED) {
						bottomInfoView.setAlpha(0);
						setOnBackPressedCallback(() ->
								bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED));
					} else if (newState == STATE_SETTLING) {
						bottomInfoView.setAlpha(1);
					}
				}

				@Override
				public void onSlide(@NonNull View bottomSheet, float slideOffset) {
					backGround.setVisibility(View.VISIBLE);
					backGround.setAlpha(slideOffset);
					bottomNavigationView.animate().translationY(
							bottomNavigationView.getHeight() * slideOffset).setDuration(0).start();
					FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) findViewById(R.id.ll_al_fr).getLayoutParams();
					params.topMargin = Func.dpToPx((1 - slideOffset) * 51, context);
					findViewById(R.id.ll_al_fr).setLayoutParams(params);
					cont.setAlpha((slideOffset));
					if (slideOffset > 0.5) {
						bottomInfoView.setAlpha((2 - slideOffset * 2));
					}
					bottomNavigationView.animate().translationY(bottomNavigationView.getHeight() * slideOffset).setDuration(0).start();
					bottomInfo.animate().translationY(slideOffset).setDuration(0).start();
				}
			});
		}
	}

	private void resizeWorkFrameUp(View view, Context context)
	{
		if (null != view)
		{
			FrameLayout workFrame = (FrameLayout) view.findViewById(R.id.nav_work);
			ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) workFrame.getLayoutParams();
			layoutParams.setMargins(layoutParams.getMarginStart(), 0, layoutParams.getMarginEnd(), Func.dpToPx(51, context));
			workFrame.setLayoutParams(layoutParams);
		}
	}

	private void resizeWorkFrameDown(View view, Context context)
	{
		if(null!=view)
		{
			FrameLayout workFrame = (FrameLayout) view.findViewById(R.id.nav_work);
			ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) workFrame.getLayoutParams();
			layoutParams.setMargins(layoutParams.getMarginStart(), 0, layoutParams.getMarginEnd(), 0);
			workFrame.setLayoutParams(layoutParams);
		}
	}

	public void showCommandResultSnackbar(Context context, int result)
	{
		String[] results;
		if(result >0)
		{
			results = context.getResources().getStringArray(R.array.command_results);
		}else{
			results = context.getResources().getStringArray(R.array.command_results_negative);
			result = Math.abs(result);
		}
		Func.showSnackBar(context, findViewById(R.id.nav_work), context.getString(R.string.ERROR_COMMAND) + " " + (result!=0? (results.length >=result ? results[result - 1] : "") : results[result]));
	}

	public void showSnackBar(Context context, String message){
		Func.showSnackBar(context, findViewById(R.id.nav_work), message);

	}

	private Cursor getAlarmEventsCursor(Context context){
//		return dbHelper.getNewActiveAlarmEvents(System.currentTimeMillis()/1000 - D3Service.EVENTS_TIME_INTERVAL*10);
		return PrefUtils.getInstance(context).getUseAM() ?  DBHelper.getInstance(context).getAlarmsCursor() : DBHelper.getInstance(context).nGetAlarmEventsCursor(0, PrefUtils.getInstance(context).getCurrentSite());
	}


	private Cursor getAttentionEventsCursor(Context context){
		return PrefUtils.getInstance(context).getUseAM() ?
				DBHelper.getInstance(context).nGetCritEventsCursorAMMode(0, PrefUtils.getInstance(context).getCurrentSite()) :
				DBHelper.getInstance(context).nGetAttentionEventsCursor(0, PrefUtils.getInstance(context).getCurrentSite());
	}

	private Cursor getCritCursor(Context context){
		return DBHelper.getInstance(context).nGetCritEventsCursor(0, PrefUtils.getInstance(context).getCurrentSite());
	}

	private void updateList(ListView list, Context context, Cursor cursor)
	{
		NEventsListAdapter adapter = new NEventsListAdapter(context, R.layout.n_events_list_element, cursor, 0, false);
		if(null!=list)
			if(adapter.getCount() > 0) {
				list.setVisibility(View.VISIBLE);
				list.setAdapter(adapter);
			}
			else{
				list.setVisibility(View.GONE);
			}
	}

	public boolean addTransitionListener(boolean b) {
		if(b)
		{
			final Transition transition = getWindow().getSharedElementEnterTransition();


			if (transition != null)
			{
				// There is an entering shared element transition so add a listener to it
				transition.addListener(new Transition.TransitionListener()
				{
					@Override
					public void onTransitionEnd(Transition transition)
					{
						// As the transition has ended, we can now load the full-size image

						// Make sure we remove ourselves as a listener
						transition.removeListener(this);
					}

					@Override
					public void onTransitionStart(Transition transition)
					{
						// No-op
					}

					@Override
					public void onTransitionCancel(Transition transition)
					{
						// Make sure we remove ourselves as a listener
						transition.removeListener(this);
					}

					@Override
					public void onTransitionPause(Transition transition)
					{
						// No-op
					}

					@Override
					public void onTransitionResume(Transition transition)
					{
						// No-op
					}
				});
				return true;
			}
		}

		// If we reach here then we have not added a listener
		return false;
	}

	@SuppressLint("InlinedApi")
	public void hideSystemUI() {
		View decorView = getWindow().getDecorView();
		int uiOptions = decorView.getSystemUiVisibility();
		int newUiOptions = uiOptions;
		newUiOptions |= View.SYSTEM_UI_FLAG_LOW_PROFILE;
		newUiOptions |= View.SYSTEM_UI_FLAG_FULLSCREEN;
		newUiOptions |= View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
		newUiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE;
		newUiOptions |= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
		decorView.setSystemUiVisibility(newUiOptions);
	}

	@SuppressLint("InlinedApi")
	public void showSystemUI() {
		View decorView = getWindow().getDecorView();
		int uiOptions = decorView.getSystemUiVisibility();
		int newUiOptions = uiOptions;
		newUiOptions &= ~View.SYSTEM_UI_FLAG_LOW_PROFILE;
		newUiOptions &= ~View.SYSTEM_UI_FLAG_FULLSCREEN;
		newUiOptions &= ~View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
		newUiOptions &= ~View.SYSTEM_UI_FLAG_IMMERSIVE;
		newUiOptions &= ~View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
		decorView.setSystemUiVisibility(newUiOptions);
	}

	public void setRotationAnimation()
	{
		int rotationAnimation = WindowManager.LayoutParams.ROTATION_ANIMATION_SEAMLESS;
		Window win = getWindow();
		WindowManager.LayoutParams winParams = win.getAttributes();
		winParams.rotationAnimation = rotationAnimation;
		win.setAttributes(winParams);
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleHelper.onAttach(base));
	}
}
