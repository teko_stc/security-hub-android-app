package biz.teko.td.SHUB_APP.Events.Activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.StartUpActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Adapters.UniEventListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardPinActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardPinEnterActivity;

/**
 * Created by td13017 on 25.05.2016.
 */
public class HistoryActivity extends BaseActivity
{
	public UniEventListAdapter eventAdapter;
	public ListView eventList;
	private DBHelper dbHelper;
	private Cursor cursor;
	private int  i;
	private final  static int DEF_POSITION = 3;

	private D3Service myService;
	private ServiceConnection serviceConnection;

	private Spinner spinnerTypes, spinnerClasses;

	public int filter = -1;
	public int objectId = 0;
	private Context context;
	private TextView sText;
	private DatePickerDialog fromDateDialog, toDateDialog;
	private long timeFrom, timeTo;  /*time in millis*/
	private TextView centralText;
	private boolean bound;
	private String currentActivity = null;
	private PrefUtils prefUtils;

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		prefUtils = PrefUtils.getInstance(context);

		getSwipeBackLayout().setEnableGesture(false);



		serviceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		eventList = (ListView) findViewById(R.id.listEvents);
		centralText = (TextView) findViewById(R.id.ha_centraltext);

		final EditText editDateFrom = (EditText) findViewById(R.id.editDateFrom);
		editDateFrom.setInputType(InputType.TYPE_NULL);
		timeFrom = Func.getStartOfDay(System.currentTimeMillis() - 3600*24*1000);
		editDateFrom.setText(Func.getDate(context, timeFrom, "dd.MM.yyyy"));
		final EditText editDateTo = (EditText) findViewById(R.id.editDateTo);
		editDateTo.setInputType(InputType.TYPE_NULL);
		timeTo = Func.getEndOfDay(System.currentTimeMillis());
		editDateTo.setText(Func.getDate(context, timeTo, "dd.MM.yyyy"));

		//init events list
		updateList();

		editDateFrom.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				fromDateDialog.show();
			}
		});

		editDateTo.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				toDateDialog.show();
			}
		});

		Calendar oldCalendar = Calendar.getInstance();
		oldCalendar.setTimeInMillis(timeFrom);

		fromDateDialog = new DatePickerDialog(this, R.style.datepicker, new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				long oldTime = timeFrom;
				timeFrom = Func.getStartOfDay(newDate.getTimeInMillis());
				long dbTime = dbHelper.getEventMinTime()*1000;
				if((oldTime > timeFrom)&&( dbTime > timeFrom)){
					getDataFromServer();
				}
				editDateFrom.setText(Func.getDate(context, timeFrom, "dd.MM.yyyy"));
				updateList();
			}
		}, oldCalendar.get(Calendar.YEAR), oldCalendar.get(Calendar.MONTH), oldCalendar.get(Calendar.DAY_OF_MONTH));

		Calendar newCalendar = Calendar.getInstance();

		toDateDialog = new DatePickerDialog(this, R.style.datepicker, new DatePickerDialog.OnDateSetListener()
		{
			@Override
			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
			{
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				timeTo = Func.getEndOfDay(newDate.getTimeInMillis());
				editDateTo.setText(Func.getDate(context, timeTo, "dd.MM.yyyy"));
				updateList();
			}
		}, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


		if(null!=D3Service.d3XProtoConstEvent)
		{
			/*Выбираем список классов для спиннера в зависимости от ролей*/
			LinkedList<EClass> eClassesList = D3Service.d3XProtoConstEvent.classes;
			EClass[] eClasses = new EClass[eClassesList.size() + 1];
			eClasses[0] = new EClass(-1, getString(R.string.ALL));
			int i = 1;
			for (EClass eClass : eClassesList)
			{
				eClasses[i] = eClass;
				i++;
			}


			spinnerTypes = (Spinner) findViewById(R.id.spinnerTypes);
//			final HistoryClassesSpinnerAdapter historyClassesSpinnerAdapter = new HistoryClassesSpinnerAdapter(this, R.layout.spinner_item_dropdown, eClasses);
//
//			spinnerTypes.setAdapter(historyClassesSpinnerAdapter);
//
//			spinnerTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
//			{
//				@Override
//				public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
//				{
//					filter = historyClassesSpinnerAdapter.getItem(position).id;
//					updateList();
//				}
//
//				@Override
//				public void onNothingSelected(AdapterView<?> parent)
//				{
//				}
//
//			});
		}

		LinearLayout objectsSpinnerLayout =  (LinearLayout) findViewById(R.id.objectsSpinnerLayout);

		final Spinner objectsSpinner = (Spinner) findViewById(R.id.spinnerObjects);
		Site[] sites = dbHelper.getAllSitesArray();
		Site[] spinnerSites;
		if(null!=sites){
			spinnerSites = new Site[sites.length +1];
			int i1 = 1;
			for (Site site:sites){
				spinnerSites[i1] = site;
				i1++;
			}
		}else{
			spinnerSites = new Site[1];
		}
		spinnerSites[0] = new Site(0, getString(R.string.ALL));

//		final HistorySitesSpinnerAdapter historySitesSpinnerAdapter = new HistorySitesSpinnerAdapter(this,  R.layout.spinner_item_dropdown, spinnerSites);
//		objectsSpinner.setAdapter(historySitesSpinnerAdapter);
//		objectsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
//		{
//			@Override
//			public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
//			{
//				objectId = historySitesSpinnerAdapter.getItem(position).id;
//				updateList();
//			}
//
//			@Override
//			public void onNothingSelected(AdapterView<?> parent)
//			{
//
//			}
//		});
		if(((App)this.getApplication()).getBlockedStatus())
		{
			if(D3Service.OPEN_HISTORY_FROM_STATUS_BAR== getIntent().getIntExtra("FROM", -1))
			{
				Intent activityIntent;
				activityIntent = new Intent(getBaseContext(), StartUpActivity.class);
				finish();
				//			activityIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				startActivity(activityIntent);
				overridePendingTransition(R.animator.enter, R.animator.exit);
			}
		}else{
			currentActivity =((App)this.getApplication()).getPreviousActivity();
			if (null != currentActivity)
			{
				if (null != dbHelper.getUserInfo())
				{
					String wizardPinAddActivity = WizardPinActivity.class.getName();
					String wizardPinEnterActivity = WizardPinEnterActivity.class.getName();
					if (currentActivity.equals(wizardPinAddActivity))
					{
						Intent activityIntent;
						activityIntent = new Intent(getBaseContext(), WizardPinActivity.class);
						activityIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(activityIntent);
						overridePendingTransition(R.animator.enter, R.animator.exit);
					}else if(currentActivity.equals(wizardPinEnterActivity))
					{
						Intent activityIntent;
						activityIntent = new Intent(getBaseContext(), WizardPinEnterActivity.class);
						activityIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(activityIntent);
						overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				}
			}
		}
	}

	private void updateList()
	{
		Cursor cursor = updateCursor();
		cursor = null;
		if(null!=cursor && null!=eventAdapter)
		{
			eventAdapter.update(cursor);
		}else{
			eventAdapter = new UniEventListAdapter(context, R.layout.card_event_uni, cursor, 0, 0, -1);;
			if(null!=eventList) eventList.setAdapter(eventAdapter);
		}
		if (null!=eventAdapter && 0!= eventAdapter.getCount())
		{
			centralText.setVisibility(View.GONE);
			eventList.setVisibility(View.VISIBLE);
		} else
		{
			centralText.setVisibility(View.VISIBLE);
			eventList.setVisibility(View.GONE);
		}

	}

	private Cursor updateCursor()
	{
		return  dbHelper.getEventCursorForHistory(objectId, filter, timeFrom, timeTo);
	}

	private BroadcastReceiver eventReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onStop()
	{
		super.onStop();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);

		registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));

		NotificationManager notifManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.cancelAll();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(eventReceiver);
	}

	@Override
	public void onBackPressed()
	{
		AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		adb.setMessage(R.string.EXIT_FROM_APP_MESSAGE);
		adb.setPositiveButton(getResources().getString(R.string.YES), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				finishAffinity();
			}
		});
		adb.setNegativeButton(getResources().getString(R.string.NO), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialogInterface, int i)
			{
				dialogInterface.dismiss();
			}
		});
		adb.show();
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}


	private void getDataFromServer(){
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject message = new JSONObject();
		long tmax = dbHelper.getEventMinTime();
		try
		{
			message.put("tmin", timeFrom/1000);
//			message.put("tmax", timeTo/1000);
			message.put("tmax", tmax == 0?  System.currentTimeMillis()/1000 : tmax);

		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		D3Request d3Request = D3Request.createMessage(userInfo.roles, "EVENTS", message);
		if(d3Request.error == null){
			sendMessageToServer(d3Request);
		}else{
			Func.pushToast(context, d3Request.error, (HistoryActivity) context);
		}
	}

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if (null != myService)
		{
			if (myService.send(d3Request.toString()))
			{
				return true;
			}
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
		}
		return false;
	};
}
