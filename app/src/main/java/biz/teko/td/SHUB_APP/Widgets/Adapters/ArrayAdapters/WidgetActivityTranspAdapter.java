package biz.teko.td.SHUB_APP.Widgets.Adapters.ArrayAdapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Widgets.Classes.Transparency;

/**
 * Created by td13017 on 26.05.2017.
 */

public class WidgetActivityTranspAdapter extends ArrayAdapter<Transparency>
{
	private final Context context;
	private final Transparency[] transparencies;
	private int selectedPosition = 0;
	boolean selected = true;

	public WidgetActivityTranspAdapter(Context context, int resource, Transparency[] transparencies)
	{
		super(context, resource, transparencies);
		this.context = context;
		this.transparencies = transparencies;
	}
	//
	//	public int getCount(){
	//		return sites.length;
	//	}
	//
	public Transparency getSelectedItem(){
		if(selected)
		{
			return transparencies[selectedPosition];
		}else{
			return null;
		}
	}
	//
	public long getSelectedItemId(int position){
		if(selected)
		{
			return selectedPosition;
		}else{
			return -1;
		}
	}

	public Transparency getItem(int position){
		return transparencies[position];
	}

	public long getItemId(int position){
		return position;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandColorLight));
		label.setText(transparencies[position].proc);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		label.setPadding(0, Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandColorLight));
		label.setText(transparencies[position].proc);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;	}
}
