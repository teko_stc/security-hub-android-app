package biz.teko.td.SHUB_APP.SectionsTabs.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ResourceCursorTreeAdapter;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.DevicesTabs.Activities.DevicesActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SectionsTabs.SectionsActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.ZonesTabs.ZonesActivity;

/**
 * Created by td13017 on 10.02.2017.
 */

public class SectionsTreeAdapter extends ResourceCursorTreeAdapter
{
	private final UserInfo userInfo;
	private Context context;
	private DBHelper dbHelper;
	private int collapsedGroupLayoutViewId;
	private int expandedGroupLayoutViewId;
	private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
	private D3Service myService;
	private int sending = 0;
	private int siteId, deviceId;

	public SectionsTreeAdapter(Context context, Cursor cursor, int collapsedGroupLayout, int expandedGroupLayout, int childLayout, int lastChildLayout, int siteId, int deviceId)
	{
		super(context, cursor, collapsedGroupLayout, expandedGroupLayout, childLayout, lastChildLayout);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.siteId = siteId;
		this.deviceId = deviceId;
		userInfo = dbHelper.getUserInfo();
		View view = newGroupView(context, null, false, null);
		collapsedGroupLayoutViewId = view.getId();
		view = newGroupView(context, null, true, null);
		expandedGroupLayoutViewId = view.getId();
	}

	public void update(Cursor cursor, int sending){
		this.changeCursor(cursor);
		this.sending = sending;
	}

	@Override
	protected Cursor getChildrenCursor(Cursor groupCursor)
	{
		int device_id = groupCursor.getInt(4);
		int section_id = groupCursor.getInt(3);
		return dbHelper.getAffectCursorBySiteDeviceSectionId(siteId, device_id, section_id);
	}

	@Override
	protected void bindGroupView(View view, final Context context, Cursor cursor, boolean isExpanded)
	{
		final Section section = new Section(cursor);
		final Device device = dbHelper.getDeviceById(section.device_id);
		final int section_id = section.id;

		TextView name = (TextView) view.findViewById(R.id.sectionName);
		name.setText(section.name);

		TextView sectionIdText  = (TextView) view.findViewById(R.id.sectionId);
		TextView type  = (TextView) view.findViewById(R.id.sectionType);
		TextView sectionArmText = (TextView) view.findViewById(R.id.sectionSt);

		ImageView imageMore = (ImageView) view.findViewById(R.id.imageMore);
		final TextView buttonMore = (TextView) view.findViewById(R.id.buttonMore);
		TextView buttonArm = (TextView) view.findViewById(R.id.buttonArm);
		TextView buttonDisarm = (TextView) view.findViewById(R.id.buttonDisarm);
		TextView buttonFireReset = (TextView) view.findViewById(R.id.buttonFireReset);
		LinearLayout controlLayout = (LinearLayout) view.findViewById(R.id.armDisarmLayout);
		FrameLayout armDisarmFrame = (FrameLayout) view.findViewById(R.id.armDisarmFrame);

		if((userInfo.roles&32768)==0)
		{
			if(null!=armDisarmFrame){
				armDisarmFrame.setVisibility(View.GONE);
			}
		}else{
			switch (section.detector){
				case Func.SECTION_GUARD_TYPE:
				case Func.SECTION_TECH_CONTROL_TYPE:
					break;
				case Func.SECTION_FIRE_TYPE:
				case Func.SECTION_FIRE_DOUBLE_TYPE:
					if(null!=controlLayout){
						controlLayout.setVisibility(View.GONE);
						if(dbHelper.getAlarmStatusForSection(section.device_id, section.id)){
							buttonFireReset.setVisibility(View.VISIBLE);
						}else{
							armDisarmFrame.setVisibility(View.GONE);
						}
					}
					break;
				case Func.SECTION_ALARM_TYPE:
				case Func.SECTION_TECH_TYPE:
				default:
					if(null!=armDisarmFrame){
						armDisarmFrame.setVisibility(View.GONE);
					}
					break;
			}
		}

		String deviceIdText = "";
		if(deviceId == -1){
			if (null != device)
			{
				deviceIdText += " • " + device.getName() + "(" + Integer.toString(device.account) + ")";
			}
		}

		LinkedList<Event> affects = dbHelper.getAffectsBySiteDeviceSectionId(siteId, section.device_id, section_id);
		ImageView imageStatement = (ImageView) view.findViewById(R.id.siteImage);
		LinearLayout sectionStImagesLayout = (LinearLayout) view.findViewById(R.id.smallIconLayout);
		ImageView sectionArmImage = (ImageView) view.findViewById(R.id.sectionArmImage);

		LinearLayout divider = (LinearLayout) view.findViewById(R.id.dividerExp);
		LinearLayout cardLinear = (LinearLayout) view.findViewById(R.id.cardLinearExp);

		String idText = "";
		if (section_id == 0)
		{
			sectionArmText.setVisibility(View.GONE);
			sectionArmImage.setVisibility(View.GONE);
//			if(null!=device && null !=D3Service.d3XProtoConstEvent){
//				DeviceType deviceType = D3Service.d3XProtoConstEvent.getDeviceTypeById(device.type);
//				if(null!=deviceType){
//					imageStatement.setImageResource(deviceType.getMainIcon(context));
//				}
//			}

			type.setVisibility(View.GONE);
			idText += context.getResources().getString(R.string.COMPLEX);

			if (null != buttonMore)
			{
				buttonMore.setText(R.string.STA_BUTTON_COMPONENTS);
				buttonMore.clearFocus();
				Cursor c = dbHelper.getAllZonesCursorForSiteDeviceSection(siteId, section.device_id, section_id, 2);
				if (null != c && c.getCount() != 0)
				{
					c.close();
					buttonMore.setVisibility(View.VISIBLE);
					if (null != affects) if (0 != affects.size())
					{
						if (divider != null)
						{
							divider.setVisibility(View.GONE);
						}
						if (cardLinear != null)
						{
							cardLinear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Func.dpToPx(201, context)));
						}
					}
				} else
				{
					buttonMore.setVisibility(View.GONE);
					if (null != affects) if (0 != affects.size())
					{
						if (divider != null)
						{
							divider.setVisibility(View.GONE);
						}
						if (cardLinear != null)
						{
							cardLinear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Func.dpToPx(154, context)));
						}
					}
				}
			}

			if(null!=armDisarmFrame){
				armDisarmFrame.setVisibility(View.GONE);
			}
		} else
		{
			if(section.detector == Func.SECTION_GUARD_TYPE || section.detector == Func.SECTION_TECH_CONTROL_TYPE )
			{
				sectionArmText.setVisibility(View.VISIBLE);
				String[] armStatements = context.getResources().getStringArray(R.array.arm_statement_titles);
				sectionArmText.setText(armStatements[section.armed>0? 1:0]);
				sectionArmText.setTextColor(section.armed > 0 ? context.getResources().getColor(R.color.brandColorGreen) : context.getResources().getColor(R.color.brandColorDark));

				if (section.armed == 0)
				{
					sectionArmImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_restriction_shield_grey_500_big));
				} else
				{
					sectionArmImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_security_checked_green_500_big_48));
				}
			}else{
				sectionArmText.setVisibility(View.GONE);
				sectionArmImage.setVisibility(View.GONE);
			}


			if(null!=D3Service.d3XProtoConstEvent && null != D3Service.d3XProtoConstEvent.getSTypeById(section.detector)){
				SType sectionType = D3Service.d3XProtoConstEvent.getSTypeById(section.detector);
				String sType = sectionType.caption;
				imageStatement.setImageResource(sectionType.getMainIcon(context));
				type.setText(sType);
			}else{
				type.setText(R.string.SECTION_TYPE_UNKNOWN);
			}

			idText += context.getResources().getString(R.string.PARTITION) + " " + Integer.toString(section_id);

			if (null != buttonMore)
			{
				buttonMore.setVisibility(View.VISIBLE);
				buttonMore.setText(R.string.DEVICES_BIG);
				buttonMore.clearFocus();
			}
			if (null != buttonArm)
			{
				buttonArm.clearFocus();
			}
			if (null != buttonDisarm)
			{
				buttonArm.clearFocus();
			}

			if (null != affects) if (0 != affects.size())
			{
				if (divider != null)
				{
					divider.setVisibility(View.GONE);
				}
				if (cardLinear != null)
				{
					cardLinear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Func.dpToPx(201, context)));
				}
			}
		}

		sectionIdText.setText(idText + deviceIdText);

		ImageView[] imagesSmall = new ImageView[7];
		for (int i = 0; i < 7; i++)
		{
			imagesSmall[i] = new ImageView(context);
			imagesSmall[i].setLayoutParams(new LinearLayout.LayoutParams(Func.dpToPx(24, context), Func.dpToPx(24, context)));
		}

		if (null != affects)
		{
			int imgSmallNum = 0;
			int sabI = 0;
			int malfI = 0;
			int warnI = 0;
			int armI = 0;
			int powI = 0;
			int gprsI = 0;
			int ethI = 0;
			int radioI = 0;

			for (Event affect : affects)
			{
				switch (affect.classId)
				{
					case 0:
						imageStatement.setImageResource(context.getResources().getIdentifier(affect.explainAffectIconBig(), null, context.getPackageName()));
						break;
					case 1:
							if (0 == sabI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								sabI++;
							}
						break;
					case 2:
							if (0 == malfI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								malfI++;
							}
						break;
					case 3:
							if (0 == warnI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								warnI++;
							}
						break;
					case 4:
						break;
					case 5:
						break;
					case 6:
						if (9 != affect.reasonId)
						{
							if (0 == powI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								powI++;
							}
						}
						break;
					case 7:
						if (1 == affect.reasonId)
						{
							switch (affect.detectorId){
								case 21:
									if (0 == gprsI)
									{
										imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
										imgSmallNum++;
										gprsI++;
									}
									break;
								case 22:
									if (0 == ethI)
									{
										imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
										imgSmallNum++;
										ethI++;
									}
									break;
								default:
									if (0 == radioI)
									{
										imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
										imgSmallNum++;
										radioI++;
									}
									break;
							}
						}
						break;
				}
			}
			if(null!=sectionStImagesLayout)
			{
				for (int n = 0; n < imgSmallNum; n++)
				{
					sectionStImagesLayout.addView(imagesSmall[n]);
				}
			}
		}

		if(section.id ==0 && !Func.getRemoteSetRights(userInfo, dbHelper, siteId, section.device_id )){

		}

		if(Func.getRemoteSetRights(userInfo, dbHelper, siteId, section.device_id) || section.id == 0){
			CharSequence[] items = getItemsForCurrentSection(section);
			if(items != null && items.length != 0)
			{
				imageMore.setOnClickListener(new sectionCardMoreOnClick(section, dbHelper, items));
			}else{
				imageMore.setVisibility(View.GONE);
			}
		}else{
			imageMore.setVisibility(View.GONE);
		}

		if(buttonMore!=null)
		{
			buttonMore.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					v.startAnimation(buttonClick);
					Intent intent = new Intent(context, ZonesActivity.class);
					intent.putExtra("site", siteId);
					intent.putExtra("device", section.device_id);
					intent.putExtra("section", section_id);
					intent.putExtra("version", 0);
					((Activity) context).startActivity(intent);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
		}

		if(buttonArm!=null)
			if(sending == 0)
			{
				buttonArm.setEnabled(true);
				buttonArm.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (dbHelper.isDeviceOnline(section.device_id))
						{
							JSONObject commandAddress = new JSONObject();
							try
							{
								commandAddress.put("section", section_id);
								int command_count = Func.getCommandCount(context);
								D3Request d3Request = D3Request.createCommand(userInfo.roles, section.device_id, "ARM", commandAddress, command_count);
								if(d3Request.error == null){
									sendMessageToServer(section, d3Request, true);
								}else{
									Func.pushToast(context, d3Request.error, (Activity) context);
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
						}
					}
				});
			}else{
				buttonArm.setEnabled(false);
			}

		if(buttonDisarm!=null)
			if(sending == 0)
			{
				buttonDisarm.setEnabled(true);
				buttonDisarm.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (dbHelper.isDeviceOnline(section.device_id))
						{
							JSONObject commandAddress = new JSONObject();
							try
							{
								commandAddress.put("section", section_id);
								int command_count = Func.getCommandCount(context);
								D3Request d3Request = D3Request.createCommand(userInfo.roles, section.device_id, "DISARM", commandAddress, command_count);
								if(d3Request.error == null){
									sendMessageToServer(section, d3Request, true);
								}else{
									Func.pushToast(context, d3Request.error, (Activity) context);
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
						}
					}
				});
			}else{
				buttonDisarm.setEnabled(false);
			}
		if(null!=buttonFireReset)
			if(sending == 0)
			{
				buttonFireReset.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (dbHelper.isDeviceOnline(section.device_id))
						{
							final Zone[] allZones = dbHelper.getAllZonesForDeviceSection(section.device_id, section.id);
							if(null!=allZones){
								for (Zone zone : allZones)
								{
									if (dbHelper.getAlarmStatusForZone(zone.device_id, zone.section_id, zone.id))
									{
										JSONObject commandAddress = new JSONObject();
										try
										{
											commandAddress.put("section", zone.section_id);
											commandAddress.put("zone", zone.id);
											int command_count = Func.getCommandCount(context);
											D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "RESET", commandAddress, command_count);
											if (d3Request.error == null)
											{
												sendMessageToServer(section, d3Request, true);
											} else
											{
												Func.pushToast(context, d3Request.error, (Activity) context);
											}
										} catch (JSONException e)
										{
											e.printStackTrace();
										}
									}
								}
							}

//							Zone[] zones = dbHelper.getFireZonesForDeviceSection(section.deviceId, section.id);
////							Zone[] zones = dbHelper.getAllZonesForDeviceSection(section.deviceId, section.id);
// 							if (null != zones)
//							{
//								for (Zone zone : zones)
//								{
//									if (dbHelper.getAlarmStatusForZone(zone.deviceId, zone.section_id, zone.id))
//									{
//										JSONObject commandAddress = new JSONObject();
//										try
//										{
//											commandAddress.put("section", zone.section_id);
//											commandAddress.put("zone", zone.id);
//											int command_count = Func.getCommandCount(context);
//											D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.deviceId, "RESET", commandAddress, command_count);
//											if (d3Request.error == null)
//											{
//												if (sendCommandToServer(d3Request))
//												{
//													sendIntentCommandBeenSend(1);
//												}
//											} else
//											{
//												Func.pushToast(context, d3Request.error, (Activity) context);
//											}
//										} catch (JSONException e)
//										{
//											e.printStackTrace();
//										}
//									}
//								}
//							}
//							{
//								final Zone[] allZones = dbHelper.getAllZonesForDeviceSection(section.deviceId, section.id);
//								final AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
//								adb.setTitle(R.string.ATTENTION);
//								adb.setMessage("Не найдены пожарные датчики, зарегистрированные в зоне.\nВсе равно хотите отправить сброс пожарной тревоги на данный раздел?");
//								adb.setPositiveButton("Сбросить", new DialogInterface.OnClickListener()
//								{
//									@Override
//									public void onClick(DialogInterface dialogInterface, int i)
//									{
//										if(null!=allZones){
//											for (Zone zone : allZones)
//											{
//												if (dbHelper.getAlarmStatusForZone(zone.deviceId, zone.section_id, zone.id))
//												{
//													JSONObject commandAddress = new JSONObject();
//													try
//													{
//														commandAddress.put("section", zone.section_id);
//														commandAddress.put("zone", zone.id);
//														int command_count = Func.getCommandCount(context);
//														D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.deviceId, "RESET", commandAddress, command_count);
//														if (d3Request.error == null)
//														{
//															if (sendCommandToServer(d3Request))
//															{
//																sendIntentCommandBeenSend(1);
//															}
//														} else
//														{
//															Func.pushToast(context, d3Request.error, (Activity) context);
//														}
//													} catch (JSONException e)
//													{
//														e.printStackTrace();
//													}
//												}
//											}
//										}
//									}
//								});
//								adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
//								{
//									@Override
//									public void onClick(DialogInterface dialogInterface, int i)
//									{
//										dialogInterface.dismiss();
//									}
//								});
//								adb.show();
//							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
						}

					}
				});
			}

	}

	private CharSequence[] getItemsForCurrentSection(Section section)
	{
		final String[] menuItemsArray = context.getResources().getStringArray(R.array.section_menu_values);
		List<String> menuItemsList = new ArrayList<>();
		if(0 == section.id){
			if((userInfo.roles& Const.Roles.TRINKET) == 0)
			{
				menuItemsList.add(menuItemsArray[0]);
			}
		}
		if(Func.getRemoteSetRights(userInfo, dbHelper, siteId, section.device_id))
		{
			if (0 == section.id)
			{
				menuItemsList.add(menuItemsArray[1]);
			} else
			{
				menuItemsList.add(menuItemsArray[2]);
				menuItemsList.add(menuItemsArray[3]);
				menuItemsList.add(menuItemsArray[4]);
			}
		}

		final CharSequence[] items = menuItemsList.toArray(new CharSequence[menuItemsList.size()]);
		return items;
	}

	@Override
	protected void bindChildView(View view, final Context context, Cursor cursor, boolean isLastChild)
	{
		TextView affectName = (TextView) view.findViewById(R.id.affectName);
		ImageView affectImage = (ImageView) view.findViewById(R.id.affectImage);
		TextView affectPlace = (TextView) view.findViewById(R.id.affectPlace);
		affectPlace.setVisibility(View.VISIBLE);
		TextView affectDevice = (TextView) view.findViewById(R.id.affectDevice);
		affectDevice.setVisibility(View.GONE);

		final Event affect = dbHelper.getAffect(cursor);

		final String affectExplanation = affect.explainAffectRegDescription(context);
		affectName.setText(Html.fromHtml(affectExplanation));

		String sectionText = "";
		String zoneText = "";
		String place = "";
		if(affect.section == 0){
//			place = dbHelper.getSectionNameById(affect.device, affect.section) + "(" + context.getString(R.string.CONTROLLER) + ")" ;
			if(affect.zone != 0){
				zoneText+=dbHelper.getZoneNameById(affect.device, affect.section, affect.zone) + "(" + affect.zone + ")";
				place += zoneText;
			}else{
				affectPlace.setVisibility(View.GONE);
			}
		}else{
			if(affect.zone ==0){
				sectionText = dbHelper.getSectionNameByIdWithOptions(affect.device, affect.section) + "(" + affect.section + ") - " + context.getString(R.string.WHOLE_SECTION);
				affectPlace.setVisibility(View.GONE);
			}else{
				sectionText += dbHelper.getSectionNameByIdWithOptions(affect.device, affect.section) + "(" + affect.section + ")";
			 	zoneText += dbHelper.getZoneNameById(affect.device, affect.section, affect.zone) + "(" + affect.zone + ")";
				place += zoneText;
			}
		}

		affectPlace.setText(place);
		affectImage.setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
		View divider = (View) view.findViewById(R.id.childDivider);
		View padding = (View) view.findViewById(R.id.childPadding);
		if(isLastChild){
			divider.setVisibility(View.VISIBLE);
			padding.setVisibility(View.VISIBLE);
		}else{
			divider.setVisibility(View.GONE);
			padding.setVisibility(View.GONE);

		}

		final String finalZoneText = zoneText;
		final String finalSectionText = sectionText;
		view.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				Func.showAffectInfo(context, siteId, finalZoneText, finalSectionText, affectExplanation, affect);
			}
		});
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		Cursor cursor = getGroup(groupPosition);
		View v = convertView;
		if (cursor != null)
		{
			if (convertView == null)
			{
				v = newGroupView(context, cursor, isExpanded, parent);
			} else
			{
				if ((isExpanded && convertView.getId() == collapsedGroupLayoutViewId) || (!isExpanded && convertView.getId() == expandedGroupLayoutViewId))
				{
					v = newGroupView(context, cursor, isExpanded, parent);
				}
			}
			bindGroupView(v, context, cursor, isExpanded);
		}
		return v;
	}

	@Override
	public void notifyDataSetChanged() {
		notifyDataSetChanged(false);
	}

	private class sectionCardMoreOnClick implements View.OnClickListener
	{
		private final SectionsActivity activity;
		private Section section;
		private DBHelper dbHelper;
		private CharSequence[] items;

		public sectionCardMoreOnClick(Section section, DBHelper dbHelper, CharSequence[] items)
		{
			this.section = section;
			this.dbHelper = dbHelper;
			activity  = (SectionsActivity) context;
			this.items = items;
		}

		@Override
		public void onClick(View v)
		{
			final UserInfo userInfo = dbHelper.getUserInfo();
			AlertDialog.Builder deviceMenuDialogBuilder = Func.adbForCurrentSDK(context);
			deviceMenuDialogBuilder.setTitle(section.name);

			final String[] menuItemsArray = context.getResources().getStringArray(R.array.section_menu_values);
			if(null != items){
				deviceMenuDialogBuilder.setItems(items, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						String curValue = items[which].toString();
						if(curValue.equals(menuItemsArray[0])){showUSSDRequestDialog(section);}
						else if(curValue.equals(menuItemsArray[1])){showRenameDialog(section, activity);}
						else if(curValue.equals(menuItemsArray[2])){showRenameDialog(section, activity);}
						else if(curValue.equals(menuItemsArray[3])){showChangeSectionTypeDialog(section);}
						else if(curValue.equals(menuItemsArray[4])){showDeleteSectionDialog(section);};
						dialog.dismiss();
					}
				});
				deviceMenuDialogBuilder.show();
			}
		}
	}

	private void showDeleteSectionDialog(final Section section)
	{
		if (dbHelper.isDeviceOnline(section.device_id))
		{
			if (0 == dbHelper.getSiteDeviceArmStatus(siteId, section.device_id))
			{
				if(0 == dbHelper.getZonesCountSection(section.device_id, section.id))
				{
					if (sending == 0)
					{
						AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
						adb.setTitle(R.string.SETA_DELETE_TITLE);
						adb.setMessage(R.string.SETA_DELETE_MESS);
						adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								JSONObject commandAddr = new JSONObject();
								try
								{
									commandAddr.put("index", section.id);
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
								int command_count = Func.getCommandCount(context);
								D3Request d3Request = D3Request.createCommand(userInfo.roles, section.device_id, "SECTION_DEL", commandAddr, command_count);
								if (d3Request.error == null)
								{
									if (sendMessageToServer(section, d3Request, true))
									{
										dialog.dismiss();
									}
								} else
								{
									Func.pushToast(context, d3Request.error, (DevicesActivity) context);
								}
							}
						});
						adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
						{
							@Override
							public void onClick(DialogInterface dialog, int which)
							{
								dialog.dismiss();
							}
						});
						adb.show();
					} else
					{
						Func.nShowMessage(context, context.getString(R.string.EA_CANNOT_SEND_COMMAND_EXIST));
					}
				}else{
					Func.nShowMessage(context, context.getString(R.string.ERROR_DELETE_SECTION_HAVE_ZONES));
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.MESSAGE_DISARM_TO_DELETE_SECTION));
			}
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showUSSDRequestDialog(final Section section)
	{
		if(dbHelper.isDeviceOnline(section.device_id))
		{
			AlertDialog.Builder ussdRequestDialog = Func.adbForCurrentSDK(context);
			AlertDialog addDevDialog = ussdRequestDialog.create();
			addDevDialog.setTitle(R.string.USSD_DIALOG_TITLE);
			addDevDialog.setMessage(context.getString(R.string.USSD_DIALOG_MESSAGE));
			FrameLayout frameLayout = (FrameLayout) new FrameLayout(context);
			frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			frameLayout.setPadding(Func.dpToPx(20, context), 0, Func.dpToPx(20, context), 0);
			final EditText editText = new EditText(context);
			String ussd = "*100#";
			String imsi = dbHelper.getCurrectDeviceImsi(section.device_id);
			if ((null != imsi) && (!imsi.equals("")))
			{
				ussd = Func.getUSSD(Integer.valueOf(imsi.substring(0, 8)));
			}
			editText.setText(ussd);
			try
			{
				Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
				f.setAccessible(true);
				f.set(editText, R.drawable.caa_cursor);
			} catch (Exception ignored)
			{
			}
			editText.setCursorVisible(true);
			editText.isCursorVisible();
			editText.setTextColor(context.getResources().getColor(R.color.md_black_1000));
			editText.setGravity(Gravity.CENTER);
			editText.requestFocus();

			frameLayout.addView(editText);
			addDevDialog.setButton(Dialog.BUTTON_POSITIVE, context.getString(R.string.ADB_USSD), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					String request = editText.getText().toString();
					if ((request != null) && (request.length() != 0))
					{
						JSONObject commandAddr = new JSONObject();
						try
						{
							commandAddr.put("text", request);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						int command_count = Func.getCommandCount(context);
						D3Request d3Request = D3Request.createCommand(userInfo.roles, section.device_id, "USSD", commandAddr, command_count);
						if (d3Request.error == null)
						{
							if (sendMessageToServer(section, d3Request, true))
							{
								dialog.dismiss();
							}
						} else
						{
							Func.pushToast(context, d3Request.error, (Activity) context);
						}
					} else
					{
						Func.pushToast(context, context.getString(R.string.ENTER_USSD), (Activity) context);
					}
				}
			});
			addDevDialog.setButton(Dialog.BUTTON_NEGATIVE, context.getString(R.string.ADB_CANCEL), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
				}
			});
			addDevDialog.setView(frameLayout);
			addDevDialog.show();
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showChangeSectionTypeDialog(final Section section)
	{
		if (dbHelper.isDeviceOnline(section.device_id))
		{
			if (0 == dbHelper.getSiteDeviceArmStatus(siteId, section.device_id))
			{
				final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(context);

				View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
				TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
				title.setText(R.string.STA_SECTION_CHANGE_SELECT_TYPE);

				ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
				listView.setDivider(null);

				LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
				LinkedList<SType> sTypes = new LinkedList<SType>();

				Device device = dbHelper.getDeviceById(section.device_id);
				if (device.canBeSetFromApp())
				{
					for (SType sType : sTypesList)
					{
						if (null != sType)
						{
							if (sType.id != 4)
							{
								sTypes.add(sType);
							}
						}
					}
				} else
				{
					sTypes = sTypesList;
				}
				final SType[] sTypesArray = new SType[sTypes.size()];
				for (int i = 0; i < sTypesArray.length; i++)
				{
					sTypesArray[i] = sTypes.get(i);
				}

				SectionTypesListAdapter sectionTypesListAdapter = new SectionTypesListAdapter(context, R.layout.n_card_for_list, sTypesArray, section.detector);
				listView.setAdapter(sectionTypesListAdapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(final AdapterView<?> parent, View view, final int position, long id)
					{
						if (sending == 0)
						{
							String mess = context.getString(R.string.STA_CHANGE_SECTIONTYPE_QUESTION);
							if (0 != dbHelper.getZonesCountSection(section.device_id, section.id))
							{
								mess = context.getString(R.string.STA_CHANGE_SECTION_TYPE_MESSAGE);
							}
							AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
							adb.setTitle(R.string.TITLE_ATTENTION);
							adb.setMessage(mess);
							adb.setPositiveButton(R.string.ADB_ACCEPT, new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int which)
								{
									SType sType = (SType) parent.getItemAtPosition(position);
									if (sType.id != section.detector)
									{
										JSONObject commandAddr = new JSONObject();
										try
										{
											commandAddr.put("index", section.id);
											commandAddr.put("type", sType.id);
											//																	commandAddr.put("name", newName);
										} catch (JSONException e)
										{
											e.printStackTrace();
										}
										int command_count = Func.getCommandCount(context);
										D3Request d3Request = D3Request.createCommand(userInfo.roles, section.device_id, "SECTION_SET", commandAddr, command_count);
										if (d3Request.error == null)
										{
											sendMessageToServer(section, d3Request, true);
										} else
										{
											Func.pushToast(context, d3Request.error, (Activity) context);
										}
									}
								}
							});
							adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
							{
								@Override
								public void onClick(DialogInterface dialog, int which)
								{
									dialog.dismiss();
								}
							});
							adb.show();
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.EA_CANNOT_SEND_COMMAND_EXIST));
						}
						bottomSheetDialog.dismiss();
					}
				});

				bottomSheetDialog.setContentView(bottomView);

				try
				{
					Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
					mBehaviorField.setAccessible(true);

					final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
					behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
					{
						@Override
						public void onStateChanged(@NonNull View bottomSheet, int newState)
						{
							if (newState == BottomSheetBehavior.STATE_DRAGGING)
							{
								behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
							}
						}

						@Override
						public void onSlide(@NonNull View bottomSheet, float slideOffset)
						{
						}
					});
				} catch (NoSuchFieldException e)
				{
					e.printStackTrace();
				} catch (IllegalAccessException e)
				{
					e.printStackTrace();
				}

				bottomSheetDialog.show();

			} else
			{
				Func.nShowMessage(context, context.getString(R.string.MESSAGE_DISARM_TO_CHANGE_SECTION));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showRenameDialog(final Section section, final Activity activity)
	{
		if(dbHelper.isDeviceOnline(section.device_id))
		{
			if (0 == dbHelper.getSiteDeviceArmStatus(siteId, section.device_id))
			{
				AlertDialog.Builder renameDialogBuilder = Func.adbForCurrentSDK(context);

				if(0 == section.id){
					renameDialogBuilder.setTitle(R.string.ZTA_RENAME_CONTROLLER);
				}else{
					renameDialogBuilder.setTitle(R.string.SCTA_RENAME_SECTION);
				}
				renameDialogBuilder.setMessage(R.string.TA_ENTER_NEW_NAME);

				LinearLayout parentLayout = new LinearLayout(context);
				parentLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
				parentLayout.setOrientation(LinearLayout.VERTICAL);
				parentLayout.setPadding(Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context));

				final EditText editName = new EditText(context);
				editName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				editName.setCursorVisible(true);
				editName.isCursorVisible();
				try
				{
					Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
					f.setAccessible(true);
					f.set(editName, R.drawable.caa_cursor);
				} catch (Exception ignored)
				{
				}
				editName.setGravity(Gravity.CENTER);
				editName.setTextColor(context.getResources().getColor(R.color.md_black_1000));
				editName.setText(section.name);
				editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
				int editNameMaxLength = 32;
				editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
				editName.requestFocus();

				parentLayout.addView(editName);
				renameDialogBuilder.setView(parentLayout);

				renameDialogBuilder.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{

					}
				});

				renameDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						Func.hideKeyboard((Activity) context);
						dialog.dismiss();
					}
				});

				final AlertDialog renameDialog = renameDialogBuilder.create();

				renameDialog.show();
				renameDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						String newName = editName.getText().toString();
						if (!newName.matches(""))
						{
							JSONObject commandAddr = new JSONObject();
							try
							{
								commandAddr.put("index", section.id);
								//										commandAddr.put("type", section.detector);
								commandAddr.put("name", newName);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							int command_count = Func.getCommandCount(context);
							D3Request d3Request = D3Request.createCommand(userInfo.roles, section.device_id, "SECTION_SET", commandAddr, command_count);
							if (d3Request.error == null)
							{
								if (sendMessageToServer(section, d3Request, true))
								{
									Func.hideKeyboard(activity);
									renameDialog.dismiss();
								}

							} else
							{
								Func.pushToast(context, d3Request.error, (Activity) context);
							}
						} else
						{
							Func.pushToast(context, context.getString(R.string.DELA_INCORRECT_NAME_PUSHTOAST), activity);
						}
					}
				});
			}else{
				Func.nShowMessage(context, context.getString(R.string.MESSAGE_DISARM_TO_CHANGE_SECTION));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	public boolean sendMessageToServer(D3Element d3Element, D3Request d3Request, boolean command)
	{
		Activity activity = (SectionsActivity)context;
		if(activity != null){
			SectionsActivity sectionsActivity = (SectionsActivity) activity;
			return sectionsActivity.sendMessageToServer(d3Element, d3Request, command);
		}
		return false;
	}
}
