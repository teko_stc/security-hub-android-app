package biz.teko.td.SHUB_APP.Profile.Fragments;

import static biz.teko.td.SHUB_APP.Utils.PrefUtils.SHARED_MAIN_SCREEN_MODE;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.Nullable;

import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class InterfacePreferenceFragment extends PreferenceFragment {
    private Activity context;

    private DBHelper dbHelper;
    private SharedPreferences sp;
    private CheckBoxPreference servicePreference;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();

        dbHelper = DBHelper.getInstance(context);
        sp = PreferenceManager.getDefaultSharedPreferences(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            addPreferencesFromResource(R.xml.interface_preferences_oreo);

        } else {
            addPreferencesFromResource(R.xml.interface_preferences);
        }
        servicePreference = (CheckBoxPreference) findPreference(SHARED_MAIN_SCREEN_MODE);
        if (null != servicePreference) {
            servicePreference.setChecked(PrefUtils.getInstance(context).getSharedMainScreenMode());
            servicePreference.setOnPreferenceChangeListener((preference, newValue) -> {
                preferenceChangeAction((boolean) newValue);
                return false;
            });
		}
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
        ListView list = view.findViewById(android.R.id.list);
        list.setDivider(new ColorDrawable(Color.TRANSPARENT));
        list.setDividerHeight(0);
    }

    private boolean preferenceChangeAction(boolean b) {
        showWarningDialog(b);
        //Func.startD3Service(context, "Connection Preference Fragment");;
        return true;
    }

    private void showWarningDialog(boolean active) {
        PrefUtils prefUtils = PrefUtils.getInstance(context);
        NDialog dialog = new NDialog(getActivity(), R.layout.n_dialog_question_allow);
        dialog.setTitle(getResources().getString(R.string.shared_main_screen_warning_dialog_text));
        dialog.setPositiveButton(getResources().getString(R.string.shared_main_screen_warning_dialog_ok));
        dialog.setNegativeButton(getResources().getString(R.string.shared_main_screen_warning_dialog_cancel));

        dialog.setOnActionClickListener((int value, boolean b) -> {
            if (value == NActionButton.VALUE_CANCEL) {
                dialog.dismiss();

            } else if (value == NActionButton.VALUE_OK) {
                dialog.dismiss();
                dbHelper.deleteMainBars();
                prefUtils.setSharedMainScreenMode(active);
                servicePreference.setChecked(active);

            }
        });
        dialog.show();
    }
}
