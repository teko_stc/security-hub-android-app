package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.MainTabs.Adapters.MainGridAdapter;
import biz.teko.td.SHUB_APP.R;

public class MainGridFragment extends Fragment implements ClickableGridFrame {
	private OnClickListener onClickListener;
	private Cursor cursor;
	private DBHelper dbHelper;
	private BaseActivity activity;
	private MainGridAdapter mainGridAdapter;
	private int site_id;
	private static MainGridFragment instance;

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateViews(site_id);
		}
	};

	public static MainGridFragment newInstance(int site_id){
		MainGridFragment mainGridFragment = new MainGridFragment();
		if(null==instance){
			instance =  mainGridFragment;
		}
		Bundle b = new Bundle();
		b.putInt("site", site_id);
		mainGridFragment.setArguments(b);
		return mainGridFragment;
	}

	public static MainGridFragment getInstance() {
		return instance;
	}

	public Fragment getFragment() {
		return this;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		final Context context = container.getContext();
		activity = (BaseActivity) context;
		dbHelper = DBHelper.getInstance(context);
		site_id = getArguments().getInt("site");
		final Site site = dbHelper.getSiteById(site_id);
		if (null != site) {
			View view = inflater.inflate(R.layout.fragment_main_grid, container, false);

			LinearLayout addNewElementView = (LinearLayout) view.findViewById(R.id.nButtonMainAdd);
			if(null!=addNewElementView) addNewElementView.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
//					startActivity(new Intent(getBaseContext(), WizardAllActivity.class));
					onClickListener.onNewClickListener();
				}
			});


			LinearLayout buttonMainSiteSelect = (LinearLayout) view.findViewById(R.id.nButtonSiteSelect);
			if(null!=buttonMainSiteSelect) buttonMainSiteSelect.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					onClickListener.onSiteChooseClick(site.id);
				}
			});
			LinearLayout buttonSiteSettings = (LinearLayout) view.findViewById(R.id.nButtonSettings);
			if(null!=buttonSiteSettings) buttonSiteSettings.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
//					updateViews(site.id);
					onClickListener.onViewSettings(site_id);
				}
			});
			TextView siteName = (TextView) view.findViewById(R.id.nTextSiteName);
			siteName.setText(site.name.trim());

			GridView mainGrid = (GridView) view.findViewById(R.id.mainGrid);
			refreshCursor(site.id);

			mainGridAdapter = new MainGridAdapter(context, cursor, false, true, new MainGridAdapter.OnGridElementClickListener() {
				@Override
				public void onElementClick(int id) {
					onClickListener.onBarClick(id, view);
				}

				@Override
				public void onEmptyElementClick() {

				}

				@Override
				public void onElementLongClick(int id) {
					onClickListener.onBarLongClick(id);
				}

				@Override
				public void onRefresh() {
					updateViews(site_id);
				}
			});
			mainGrid.setAdapter(mainGridAdapter);
			MainFragment parent = ((MainFragment)getParentFragment());
			if (parent!= null)
				parent.setOnFragmentRefreshListener(() -> updateViews(site_id));
			return view;
		}

		return null;
	}

	private void refreshCursor(int site_id) {
		cursor = dbHelper.getMainBars(site_id, false);

		if (null == cursor || 0 == cursor.getCount()) {
			dbHelper.fillBars(site_id, false);
			cursor = dbHelper.getMainBars(site_id, false);
		}
	}

	private void updateViews(int site_id){
		refreshCursor(site_id);
		if(null!=mainGridAdapter)mainGridAdapter.update(cursor);
	}

	public interface OnClickListener
	{
		void onNewClickListener();

		void onEmptySharedClickListener();

		void onSiteChooseClick(int site_id);
		void onViewSettings(int site_id);
		void onBarClick(int id, View view);
		void onBarLongClick(int id);
	}

	public void setClickListener(OnClickListener onClickListener){
		this.onClickListener = onClickListener;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		getActivity().registerReceiver(updateReceiver, intentFilter);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(updateReceiver);
	}
}
