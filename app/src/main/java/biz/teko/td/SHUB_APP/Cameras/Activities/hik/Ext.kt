package biz.teko.td.SHUB_APP.Cameras.Activities.hik

    import okhttp3.*

class Ext {
    /*fun Interceptor.Chain.addQueriesToInterceptor(queryPair: Pair<String, String>): Response {

        val requestBuilder = request().url.newBuilder()
        requestBuilder.addQueryParameter(queryPair.first, queryPair.second)
        return requestBuilder.build().let { url ->
            this.proceed(request().newBuilder().url(url).build())
        }
    }*/
    val JSON: MediaType? = MediaType.parse("application/json; charset=utf-8")
    fun addJson(chain: Interceptor.Chain, json: String): Response {
        val body: RequestBody = RequestBody.create(JSON, json)
        val request: Request = Request.Builder()
            .url(chain.request().url())
            .post(body)
            .build()
        return chain.proceed(request)
    }
}