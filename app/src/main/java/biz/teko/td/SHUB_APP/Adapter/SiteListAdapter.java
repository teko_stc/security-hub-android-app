package biz.teko.td.SHUB_APP.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cursoradapter.widget.ResourceCursorAdapter;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 28.07.2016.
 */
public class SiteListAdapter extends ResourceCursorAdapter
{
	Context context;
	Cursor cursor;
	DBHelper dbHelper;
	int layout;
	Site site;

	public SiteListAdapter(Context context, int layout, Cursor c, int flags)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.layout = layout;
		this.cursor = c;
	}

	public void update(Cursor cursor){
		this.changeCursor(cursor);
	}

	private void setTextViewText(View view, int id, String string){
		TextView textView = (TextView) view.findViewById(id);
		if(textView!=null){
			textView.setText(string==null? "" : string);
		}
	}

	private void setRowBackground(View view, int textId, int layoutId, int textColor, int layoutColor){
		LinearLayout pattern = (LinearLayout)  view.findViewById(layoutId);
		TextView textView = (TextView) view.findViewById(textId);
		pattern.setBackgroundColor(context.getResources().getColor(layoutColor));
		textView.setTextColor(context.getResources().getColor(textColor));

	}


	@Override
	public void bindView(View view, final Context context, Cursor cursor)
	{
		dbHelper = DBHelper.getInstance(context);
		site = dbHelper.getSiteWithoutSections(cursor);
		setTextViewText(view, R.id.siteName, site.name);
//		setTextViewText(view, R.id.site, Integer.toString(site.id));
		view.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				switch(event.getAction()){
					case MotionEvent.ACTION_DOWN:
						setRowBackground(v, R.id.siteName, R.id.patternLayout, R.color.md_white_1000, R.color.brandColorDark);
						break;
					case MotionEvent.ACTION_UP:
						setRowBackground(v, R.id.siteName, R.id.patternLayout, R.color.brandColorRed, R.color.brandColorWhite);
						break;
				};
				return false;
			};
		});
	}





	private class openActivityTask extends AsyncTask<Intent, Void,Void>
	{
		private AlertDialog ad;
		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute()
		{
			//			ad = Func.progressDialog(context);
			progressDialog = new ProgressDialog(context);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Получение данных ...");
			progressDialog.show();
		}

		@Override
		protected Void doInBackground(Intent... intents)
		{
			Intent intent = intents[0];
			//LATER
			//								intent.putExtra("Section Id", params.id);
			//								intent.putExtra("Device Id", params.id);
			context.startActivity(intent);
			((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
			((Activity) context).finish();
			return null;
		}

		@Override
		protected void onPostExecute(Void vvoid)
		{
			//			ad.dismiss();
		}
	}

	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint)
	{
		return super.runQueryOnBackgroundThread(constraint);
	}
}
