package biz.teko.td.SHUB_APP.UDP.Entities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import biz.teko.td.SHUB_APP.UDP.LocalConnection;

public class RequestConfig extends RequestBasePincoded {
    public final static byte _command = 4;

    public RequestConfig(int pin) {
        this.command = _command;
        this.magic = LocalConnection.leIntToByteArray(pin);
    }

    public byte[] getBytes() {
        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            outStream.write(magic);
            outStream.write(command);
            return outStream.toByteArray();
        } catch (IOException e) {e.printStackTrace();}
        return null;
    }
}
