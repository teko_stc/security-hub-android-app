package biz.teko.td.SHUB_APP.Wizards.Adapter.Pager;

import android.app.Fragment;
import android.app.FragmentManager;
import androidx.legacy.app.FragmentStatePagerAdapter;

import biz.teko.td.SHUB_APP.Wizards.Fragments.ControllerSetFragment;

/**
 * Created by td13017 on 23.06.2017.
 */

public class ContrSetViewPagerAdapter extends FragmentStatePagerAdapter
{
	private final ControllerSetFragment.OnClickListener onClickListener;
	private int CONTROLLER_SET_PAGES_COUNT = 4;

	public ContrSetViewPagerAdapter(FragmentManager fm, ControllerSetFragment.OnClickListener onClickListener)
	{
		super(fm);
		this.onClickListener = onClickListener;
	}

	@Override
	public Fragment getItem(int position)
	{
		ControllerSetFragment controllerSetFragment = ControllerSetFragment.newInstance(position);
		controllerSetFragment.setListener(onClickListener);
		return controllerSetFragment;
	}

	@Override
	public int getCount()
	{
		return CONTROLLER_SET_PAGES_COUNT;
	}

	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}
}
