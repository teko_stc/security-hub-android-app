package biz.teko.td.SHUB_APP.FaqTab.RecycleGroup;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

import biz.teko.td.SHUB_APP.FaqTab.Faq.FChapter;
import biz.teko.td.SHUB_APP.FaqTab.FPQuestion;

/**
 * Created by td13017 on 08.02.2017.
 */

public class FaqGroup extends ExpandableGroup<FPQuestion>
{
	private String caption;
	private int id;

	public FaqGroup(FChapter fChapter, List<FPQuestion> items)
	{
		super(fChapter.caption, items);
		this.id = fChapter.id;
		this.caption = fChapter.caption;
	}

	public int getChapterId(){
		return this.id;
	}

	public String getChapterCaption(){
		return this.caption;
	}
}
