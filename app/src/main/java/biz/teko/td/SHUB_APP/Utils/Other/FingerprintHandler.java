package biz.teko.td.SHUB_APP.Utils.Other;

/**
 * Created by td13017 on 29.06.2017.
 */

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;

@TargetApi(Build.VERSION_CODES.M)
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

	private final OnAuthListener onAuthListener;
	// You should use the CancellationSignal method whenever your app can no longer process user input, for example when your app goes
	// into the background. If you don’t use this method, then other apps will be unable to access the touch sensor, including the lockscreen!//
	private CancellationSignal cancellationSignal;
	private Context context;


	public interface OnAuthListener{
		void onResult(int result);
	}

	public FingerprintHandler(Context mContext, OnAuthListener onAuthListener) {
		context = mContext;
		this.onAuthListener = onAuthListener;
	}

	//Implement the startAuth method, which is responsible for starting the fingerprint authentication process//
	public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject, boolean activated) {

		cancellationSignal = new CancellationSignal();
		if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
			return;
		}
		if(activated)
		{
			manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
		}
	}

	public void stopListeningAuthentication()
	{
		if (cancellationSignal != null)
		{
			cancellationSignal.cancel();
			cancellationSignal = null;
		}
	}

	@Override
	//onAuthenticationError is called when a fatal error has occurred. It provides the error code and error message as its parameters//
	public void onAuthenticationError(int errMsgId, CharSequence errString) {

		//I’m going to display the results of fingerprint authentication as a series of toasts.
		//Here, I’m creating the message that’ll be displayed if an error occurs//

//		02.08.2019 закомментировано, т.к. непонятно при каких условиях вылетает
// 		Toast.makeText(context, context.getString(R.string.FINGERHANDLER_ERROR_AUTH) + "\n" + errString, Toast.LENGTH_LONG).show();
	}

	@Override
	//onAuthenticationFailed is called when the fingerprint doesn’t match with any of the fingerprints registered on the device//
	public void onAuthenticationFailed() {
		Toast.makeText(context, R.string.FINGERHANDLER_ERROR, Toast.LENGTH_LONG).show();
	}

	@Override
	//onAuthenticationHelp is called when a non-fatal error has occurred. This method provides additional information about the error,
	//so to provide the user with as much feedback as possible I’m incorporating this information into my toast//
	public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
		Toast.makeText(context, context.getString(R.string.FINGERHANDLER_ATTENTION) + "\n" + helpString, Toast.LENGTH_LONG).show();
	}

	@Override
	//onAuthenticationSucceeded is called when a fingerprint has been successfully matched to one of the fingerprints stored on the user’s device//
	public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result)
	{
		if(null!=onAuthListener) onAuthListener.onResult(Const.AUTH_METHOD_FINGER);
//		((WizardPinEnterActivity) context).goNext(0);
	}

}

