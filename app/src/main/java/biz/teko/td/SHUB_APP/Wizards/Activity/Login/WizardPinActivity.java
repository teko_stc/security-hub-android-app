package biz.teko.td.SHUB_APP.Wizards.Activity.Login;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Wizards.Fragments.NWizardPinAddFragment;

/**
 * Created by td13017 on 29.06.2017.
 */

public class WizardPinActivity extends AppCompatActivity {
    private Context context;
    private DBHelper dbHelper;
    private ServiceConnection serviceConnection;
    private D3Service myService;
    private boolean bound;
    private int from;
    private NWizardPinAddFragment addFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_wizard_pin_add);

        context = this;
        dbHelper = DBHelper.getInstance(context);
        from = getIntent().getIntExtra("FROM", 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                if (!pm.isIgnoringBatteryOptimizations(getPackageName())) {
                    Func.showDozeDialog(context);
                }
            }
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (getIntent().getIntExtra("SERVICE", -1) == D3Service.SERVICE_BEEN_STOPED) {
                Func.showServiceStopedDialog(context);
            }
        }
        if (Func.needLicenseAForBuild()) {
            if (!PrefUtils.getInstance(context).getLicenseAgreed()) {
                Func.showLicenseAgreementDialog(context);
            }
        }
        if (from == D3Service.OPEN_FROM_LOGIN) {
            UserInfo userInfo = dbHelper.getUserInfo();
            if (null != userInfo && userInfo.roles != 0 && userInfo.roles < Const.Roles.DOMAIN_ADMIN) {
                showCrossDomainUserDialog();
            }
        }
        setInitialFragment(-1);
    }

    public void setInitialFragment(int savedPin) {
        addFragment = new NWizardPinAddFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("FROM", from);
        if (savedPin != -1)
            bundle.putInt("pin", savedPin);
        addFragment.setArguments(bundle);
        getTransAnim(savedPin)
                .replace(R.id.pinContainer, addFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

    private FragmentTransaction getTransAnim(int savedPin) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (savedPin == -1)
            return transaction;
        return transaction.setCustomAnimations(R.animator.from_left_enter, R.animator.from_left_exit);
    }

    private void showCrossDomainUserDialog() {
        Func.nShowOkMessage(getResources().getString(R.string.CROSS_DOMAIN_ATTENTION_MESS), this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindD3();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(serviceConnection);
    }

    private void bindD3() {
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = getServiceConnection();
        bindService(intent, serviceConnection, 0);
    }

    private ServiceConnection getServiceConnection() {
        return new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
    }

    public D3Service getLocalService() {
        if (bound) {
            return myService;
        } else {
            bindD3();
            if (null != myService) {
                return myService;
            }
            Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
        }
        return null;

    }

    public D3Service getService() {
        return myService;
    }

    public int getSiteIdForForceCode(String typedPin) {
        return dbHelper.getSiteIdForForceCode(typedPin);
    }

    public void addFragment(Fragment fragment, boolean b) {
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(b ? R.animator.enter: R.animator.from_left_enter, b? R.animator.exit : R.animator.from_left_exit)
                .replace(R.id.pinContainer, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }


    public UserInfo getUserInfo() {
        return dbHelper.getUserInfo();
    }

    public void setUserInfoPIN(String newPin) {
        dbHelper.setUserInfoPIN(newPin);
    }

    public void makeToast(String text) {
        Func.pushToast(context, text, this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}
