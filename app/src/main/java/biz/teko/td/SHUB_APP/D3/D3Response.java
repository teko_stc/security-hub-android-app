package biz.teko.td.SHUB_APP.D3;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import biz.teko.td.HexDump;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3DB.Alarm;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.ConfigFileObject;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Extra;
import biz.teko.td.SHUB_APP.D3DB.LocaleD3;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.Statement;
import biz.teko.td.SHUB_APP.D3DB.User;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;

/**
 * Created by td13017 on 31.05.2016.
 */
public class D3Response extends JSONObject
{

	private String regCommandName = "^\\s*\\{\\s*\"([\\w]+)\"\\s*:.*\\}\\s*$";

	public D3Response(String data) throws JSONException
	{
		super(data);
	}


	public String getStringField(String name){
		try
		{
			return this.getString(name);
		} catch (JSONException e)
		{
			return null;
		}
	}

	public JSONObject getDataField(){
		try
		{
			return this.getJSONObject("D");
		} catch (JSONException e)
		{
			return null;
		}
	}

	public Integer getIntegerField(String name){
		try
		{
			return this.getInt(name);
		} catch (JSONException e)
		{
			return null;
		}
	}

	public int getResult(){
		Integer result = getIntegerField("R");
		if (null == result ){
			return  0;
		}
		return  result;
	}

	public Date getDate(){
		Integer result = getIntegerField("T");
		if (null == result){
			return null;
		}
		return new  Date(result*1000L);
	}

	public int getID(){
		 Integer id = getIntegerField("ID");
		 if(null == id){
		 	return  0 ;
		 }
		 return id;
	}

	public String getSession(){
		try
		{
			return this.getDataField().getString("session");
		} catch (JSONException e)
		{
			return null;
		}
	}

	public Integer getCount(){
		try
		{
			return this.getDataField().getInt("count");
		} catch (JSONException e)
		{
			return null;
		}
	}

	public JSONArray getItems(){
		JSONObject d = getDataField();
		int count = this.getCount();
		try
		{
			JSONArray array = d.getJSONArray("items");
			if(count == array.length())
				return  array;
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	/*SITES*/

	public Site[] getSites()
	{
		int count = this.getCount();
		try
		{
			JSONArray sites = getItems();
			if(count!= sites.length()){
				count = sites.length();
			};
			Site[] result = new Site[count];
			for(int i = 0 ; i < count; i++){
				result[i] = siteFromJson(sites.getJSONObject(i));
			}
			return result;
		} catch (JSONException e)
		{
			return null;
		}
	}

	public  Site  getSite(){
		if(getResult() < 0) return  null;
		return siteFromJson(getDataField());
	}

	private Site siteFromJson(JSONObject siteObject){
		try
		{
			JSONArray devicesArray = siteObject.getJSONArray("devices");
			Device[] devices = new Device[devicesArray.length()];
			for(int id = 0; id < devicesArray.length(); id ++){
				JSONObject deviceObject = devicesArray.getJSONObject(id);

				/*get sections fro device*/
				JSONArray sectionsArray = deviceObject.getJSONArray("sections");
				Section[] sections = new Section[sectionsArray.length()];
				for (int ii = 0; ii < sectionsArray.length(); ii++)
				{
					JSONObject sectionObject = sectionsArray.getJSONObject(ii);
					JSONArray zonesArray = sectionObject.getJSONArray("zones");
					Zone[] zones = new Zone[zonesArray.length()];
					for (int y = 0; y < zonesArray.length(); y++)
					{
						JSONObject zoneObject = zonesArray.getJSONObject(y);
						zones[y] = new Zone(zoneObject.getInt("id"),
								zoneObject.getString("name"),
								sectionObject.getInt("id"),
								deviceObject.getInt("id"),
								zoneObject.getInt("physic"),
								zoneObject.getInt("detector"),
								zoneObject.getInt("delay"),
								getI(zoneObject, "uid_type"));
					}
					sections[ii] = new Section(sectionObject.getInt("id"),
							sectionObject.getString("name"),
							deviceObject.getInt("id"),
							zones,
							sectionObject.getInt("detector"),
							0,
							null);
				}
				/*get users for device*/
				JSONArray usersArray = deviceObject.getJSONArray("users");
				User[] users = new User[usersArray.length()];
				for(int iu = 0; iu < usersArray.length(); iu++ ){
					JSONObject userObject = usersArray.getJSONObject(iu);
					JSONArray uSecArray = userObject.getJSONArray("sections");
					String[] uSec = new String[uSecArray.length()];
					for(int si = 0; si < uSecArray.length(); si++){
						uSec[si] = uSecArray.getString(si);
					}
					users[iu] = new User(userObject.getInt("user_index"),
							deviceObject.getInt("id"),
							userObject.getString("name"),
							userObject.getString("comment"),
							userObject.getString("phone"),
							Arrays.toString(uSec));
				}

				/*return current device*/
				devices[id] = new Device(deviceObject.getInt("id"),
						deviceObject.getInt("account"),
						deviceObject.getInt("config_version"),
						sections,
						users,
						deviceObject.getInt("parted"),
						deviceObject.getInt("cluster_id"));
			}

			return new Site(siteObject.getInt("id"),
					siteObject.getInt("contract_id"),
					siteObject.getInt("contract_status"),
					siteObject.getString("name"),
					siteObject.getString("comment"),
					"",
					devices,
					null,
					siteObject.getInt("is_crossing"),
					siteObject.getInt("domain"),
					siteObject.getInt("delegated"),
					siteObject.getInt("type"),
					siteObject.getInt("category"),
					siteObject.getInt("arm_mode"));
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	/*DEVICES */

	public Device getDevice(){
		return deviceFromJson(getDataField());
	}

	private Device deviceFromJson(JSONObject deviceObject)
	{
		JSONArray sectionsArray = null;
		try
		{
			sectionsArray = deviceObject.getJSONArray("sections");
			Section[] sections = new Section[sectionsArray.length()];
			for (int ii = 0; ii < sectionsArray.length(); ii++)
			{
				JSONObject sectionObject = sectionsArray.getJSONObject(ii);
				JSONArray zonesArray = sectionObject.getJSONArray("zones");
				Zone[] zones = new Zone[zonesArray.length()];
				for (int y = 0; y < zonesArray.length(); y++)
				{
					JSONObject zoneObject = zonesArray.getJSONObject(y);
					zones[y] = new Zone(zoneObject.getInt("id"),
							zoneObject.getString("name"),
							sectionObject.getInt("id"),
							deviceObject.getInt("id"),
							zoneObject.getInt("physic"),
							zoneObject.getInt("detector"),
							zoneObject.getInt("delay"),
							getI(zoneObject, "uid_type"));
				}
				sections[ii] = new Section(sectionObject.getInt("id"),
						sectionObject.getString("name"),
						deviceObject.getInt("id"),
						zones,
						sectionObject.getInt("detector"),
						sectionObject.getLong("arm"),
						getExtraShort(sectionObject, Const.EXTRAS.TEMP_THRESHOLDS));
			}
			/*get users for device*/
			JSONArray usersArray = deviceObject.getJSONArray("users");
			User[] users = new User[usersArray.length()];
			for(int iu = 0; iu < usersArray.length(); iu++ ){
				JSONObject userObject = usersArray.getJSONObject(iu);
				JSONArray uSecArray = userObject.getJSONArray("sections");
				String[] uSec = new String[uSecArray.length()];
				for(int si = 0; si < uSecArray.length(); si++){
					uSec[si] = uSecArray.getString(si);
				}
				users[iu] = new User(userObject.getInt("user_index"),
						deviceObject.getInt("id"),
						userObject.getString("name"),
						userObject.getString("comment"),
						userObject.getString("phone"),
						Arrays.toString(uSec));
			}
			return new Device(deviceObject.getInt("id"),
					deviceObject.getInt("account"),
					deviceObject.getInt("config_version"),
					sections,
					users,
					deviceObject.getInt("parted"),
					deviceObject.getInt("cluster_id"));
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private Extra getExtraShort(JSONObject jsonObject, String key)
	{
		try
		{
			return new Extra(key, jsonObject.getJSONObject(key).getString("value"));
		} catch (JSONException e)
		{
			return null;
		}
	}

	public Device[] getDevices()
	{
		int count = getCount();
		try
		{
			JSONArray devicesArray = getItems();
			if(count != devicesArray.length()){
				count = devicesArray.length();
			}
			Device[] devices = new Device[count];
			for(int i =0; i < count; i++){

				JSONObject deviceObject = devicesArray.getJSONObject(i);
				devices[i] = deviceFromJson(deviceObject);
			}
			return devices;
		} catch (JSONException e)
		{
			return null;
		}
	}

	public Script[] getDeviceScripts()
	{
		try
		{
			int result = getResult();
			int count = getCount();
			JSONArray scriptsArray = getItems();
			if(count != scriptsArray.length())count = scriptsArray.length();
			Script[] scripts = new Script[count];
			for(int i=0; i < count; i++)
			{
				JSONObject scriptObject = scriptsArray.getJSONObject(i);
				scripts[i] = new Script(scriptObject.getInt("id"),
						scriptObject.getString("uid"),
						result,
						scriptObject.getInt("bind"),
						scriptObject.getString("name"),
						scriptObject.getString("program"),
						scriptObject.getString("compile_program"),
						scriptObject.getString("params"),
						scriptObject.getString("extra"),
						scriptObject.getInt("enabled")
						);
			}
			return scripts;
		} catch (JSONException e)
		{
			return null;
		}

	}

	public Script getDeviceScript()
	{
		try
		{
			JSONObject scriptObject = getDataField();
			return  new Script(scriptObject.getInt("id"),
					scriptObject.getString("uid"),
					scriptObject.getInt("device_id"),
					scriptObject.getInt("bind"),
					scriptObject.getString("name"),
					scriptObject.getString("program"),
					scriptObject.getString("compile_program"),
					scriptObject.getString("params"),
					scriptObject.getString("extra"),
					scriptObject.getInt("enabled")
			);
		} catch (JSONException e)
		{
			return null;
		}
	}

	public Script[] getLibraryScripts()
	{

		try
		{
			int count = getCount();
			JSONArray scriptsArray = getItems();
			if(count != scriptsArray.length())count = scriptsArray.length();
			Script[] scripts = new Script[count];
			for(int i=0; i < count; i++)
			{
				JSONObject scriptObject = (JSONObject) scriptsArray.getJSONObject(i);
//				scripts[i] = new Script(scriptObject.getInt("id"),
//						scriptObject.getString("uid"));
				D3Response data = new D3Response(scriptObject.getString("data"));
				scripts[i] = new Script(scriptObject.getInt("id"),
						scriptObject.getString("uid"),
						data.getAS("category"),
						data.getAS("name"),
						data.getAS("description_alt"),
						data.getS("source"),
						data.getAS("params"),
						data.getS("extra"),
						data.getAS("firmwares")
				);
			}
			return scripts;
		} catch (JSONException e)
		{
			return null;
		}

	}

	private String getAS(String key)
	{
		try
		{
			return getJSONArray(key).toString();
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private JSONArray getA(String key)
		{
			try
			{
				return getJSONArray(key);
			} catch (JSONException e)
			{
				e.printStackTrace();
				return null;
			}
		}

	private int getI(JSONObject object, String key)
	{
		try
		{
			return object.getInt(key);
		} catch (JSONException e)
		{
			e.printStackTrace();
			return 0;
		}
	}

	private String getS(String key)
	{
		try
		{
			return getString(key);
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public User getUser(){
		JSONObject data =getDataField();
		JSONArray uSecArray = null;
		try
		{
			uSecArray = data.getJSONArray("sections");
			String[] uSec = new String[uSecArray.length()];
			for(int si = 0; si < uSecArray.length(); si++){
				uSec[si] = uSecArray.getString(si);
			}
			return new User(data.getInt("user_index"),
					data.getInt("device_id"),
					data.getString("name"),
					data.getString("comment"),
					data.getString("phone"),
					Arrays.toString(uSec));
		} catch (JSONException e)
		{
			e.printStackTrace();
			return  null;
		}
	}

	public String getUpdatedCommandType (){
		JSONObject jsonObject = getDataField();
		String type = "";
		try
		{
			String data = jsonObject.getString("data");
			if(data!=null){
				Pattern p = Pattern.compile(regCommandName);
				Matcher m = p.matcher(data);
				if(m.matches())
					type = m.group(1);
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return type;
	}

	public Command getCommandFromIPC(){
		JSONObject jsonObject = getDataField();
		try
		{
			int id = jsonObject.getInt("id");
			int result = jsonObject.getInt("result");
			String type = "";
			String data = jsonObject.getString("cmd");
			if(data!=null){
				Pattern p = Pattern.compile(regCommandName);
				Matcher m = p.matcher(data);
				if(m.matches())
					type = m.group(1);
			}
			return new Command(id, type, result);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public Command[] getCommandsFromReference()
	{
		JSONObject jsonObject = getDataField();
		try
		{
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			int count = jsonArray.length();
			Command[] commandsFromReference = new Command[count];
			for(int i = 0; i < count; i++){
				JSONObject jsonObject1 = jsonArray.getJSONObject(i);
				int id = jsonObject1.getInt("id");
				String type = "";
				String data = null;
				switch (App.getContext().getPackageName()){
					case "teko.shub":
					case "uvosrtv.sh":
						data = jsonObject1.getString(jsonObject.getString("name"));
						break;
					default:
						data = jsonObject1.getString("data");
						break;
				}
				if(data!=null){
					Pattern p = Pattern.compile(regCommandName);
					Matcher m = p.matcher(data);
					if(m.matches())
						type = m.group(1);
				}
				commandsFromReference[i] = new Command(id, type);
			}
			return commandsFromReference;
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	public Operator[] getOperatorsFromReference()
	{
		JSONObject jsonObject = getDataField();
		try
		{
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			int count = jsonArray.length();
			if(0 < count){
				Operator[] operators = new Operator[count];
				for (int i = 0; i < count; i++){
					String data;
					switch (App.getContext().getPackageName()){
						case "teko.shub":
						case "uvosrtv.sh":
							data = jsonArray.getJSONObject(i).getString(jsonObject.getString("name"));
							break;
						default:
							data = jsonArray.getJSONObject(i).getString("data");
							break;
					}
					operators[i] = new Operator(jsonArray.getJSONObject(i).getInt("id"), data);
				}
				return operators;
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		return null;
	}


	public String getRefType()
	{
		JSONObject jsonObject = getDataField();
		String refType = "";
		try
		{
			refType = jsonObject.getString("name");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return refType;
	}

	public JSONArray getConfigFilesArray(){
		JSONObject d = getDataField();
		try
		{
			return d.getJSONArray("files");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public ConfigFileObject getConfigfile(){
		ConfigFileObject configFileObject = new ConfigFileObject();
		JSONArray jsonArray = getConfigFilesArray();
		if(0!=jsonArray.length())
		{
			try
			{
				JSONObject jsonObject = jsonArray.getJSONObject(0);
				configFileObject.number = jsonObject.getInt("number");
				String content = jsonObject.getString("content");
				//			for(int i =0 ; i < content.length(); i++){
				//				char c = content.charAt(i);
				//				getNibble(c);
				//			}
				int[] b = HexDump.hexBin(content);
				Log.d("D3CONFIG", HexDump.binHex(b));
				configFileObject.bytes = b;
				return configFileObject;
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	public int getSiteId(){
		JSONObject data = getDataField();
		if(null!=data){
			try
			{
				int site_id = data.getInt("site_id");
				return site_id;
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return -1;
	}

	public Event[] getEvents() {
		// jresp = json_pack("{s:i,s:i,s:{s:i,s:o}}", "R", 1, "T", time(NULL), "D", "count", count, "events", array);
		JSONObject d = getDataField();
		try
		{
			int count = this.getCount();
			JSONArray events = d.getJSONArray("items");
			if (count != events.length()) {
				// error !
				count = events.length();
			}
			Event[] result = new Event[count];
			for (int i = 0; i < count; i++) {
				result[i] = new Event(events.getJSONObject(i));
			}
			return result;
		} catch (JSONException e)
		{
			return null;
		}
	}

	public Statement[] getSiteStatements()
	{
		JSONObject dataField = getDataField();
		try
		{
			JSONArray stateArray = dataField.getJSONArray("items");
			int count = stateArray.length();
			Statement[] statements = new Statement[count];
			for(int i = 0; i  < count; i++){
				JSONObject state = stateArray.getJSONObject(i);
				statements[i] = new Statement(state.getInt("id"), state.getInt("state"));
			}
			return statements;

		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}


	public String getQ()
	{
		return getStringField("Q");
	}

	public int getId()
	{
		JSONObject data = getDataField();
		try
		{
			return data.getInt("id");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	public int getRemovedDeviceId(){
		JSONObject data = getDataField();
		try
		{
			return data.getInt("id");
		} catch (JSONException e)
		{
			e.printStackTrace();
			return 0;
		}
	}

	public int getRole()
	{
		int roles = -1;
		JSONObject data = getDataField();
		try
		{
			return roles = data.getInt("roles");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return roles;
	}

	public int getDomainID(){
		int domain = 0;
		JSONObject data = getDataField();
		try
		{
			return data.getInt("domain_id");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return domain;
	}

	public int getConfigVersion(){
		try
		{
			return getDataField().getInt("config_version");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	/*for section_zone_update*/
	public Zone getFullZone(){
		return getFullZoneFromJson(getDataField());
	}

	public Zone[] getZones()
	{
		JSONObject data = getDataField();
		int zoneCount = getCount();
		JSONArray zonesArray = null;
		try
		{
			zonesArray = data.getJSONArray("zones");
			if(zoneCount!=zonesArray.length()){
				zoneCount=zonesArray.length();
			}
			Zone[] zones = new Zone[zoneCount];
			for(int i =0; i < zoneCount; i++){
				zones[i] = getFullZoneFromJson(zonesArray.getJSONObject(i));
			}
			return  zones;
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	private Zone getFullZoneFromJson(JSONObject jsonObject){
		try
		{
			return new Zone(jsonObject.getInt("zone"),
					jsonObject.getString("name"),
					jsonObject.getInt("section"),
					jsonObject.getInt("device"),
					jsonObject.getInt("physic"),
					jsonObject.getInt("detector"),
					jsonObject.getInt("delay"),
					getI(jsonObject, "uid_type")
					);
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public Zone getZone(){
		JSONObject zoneObject = getDataField();
		try
		{
			return new Zone(zoneObject.getInt("zone"),
					zoneObject.getInt("section"),
					zoneObject.getInt("device"));
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return  null;
	}

	public int getDeviceId(){
		JSONObject jsonObject = getDataField();
		try
		{
			return jsonObject.getInt("device");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return 0;
	}

	public int getSectionId(){
		JSONObject jsonObject = getDataField();
		try
		{
			int id = jsonObject.getInt("device_section");
			return id == -1 ? 0 : id;
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return 0;
	}


	public String getDelegationCode()
	{
		JSONObject jsonObject = getDataField();
		try
		{
			return jsonObject.getString("user_code");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public int getCommandInnerId()
	{
		return getIntegerField("ID");
	}

	public String getSafeSession()
	{
		JSONObject jsonObject = getDataField();
		try
		{
			return jsonObject.getString("session");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public LocaleD3 getLocale()
	{
		JSONObject data = getDataField();
		LocaleD3 localeD3 = new LocaleD3();
		try
		{
			JSONObject localeJson = data.getJSONObject("locale");
			return new LocaleD3(localeJson.getInt("locale_id"), localeJson.getString("iso"), localeJson.getString("description"));
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return localeD3;
	}


	public int getOpID()
	{
		JSONObject jsonObject = getDataField();
		try
		{
			return jsonObject.getInt("op_id");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return -1;
	}

	public Alarm[] getAlarms(){
		int count = getCount();
		try
		{
			JSONArray alarmsArray = getItems();
			if (count != alarmsArray.length()){
				count = alarmsArray.length();
			}
			Alarm[] alarms = new Alarm[count];
			for(int i=0; i<count; i++){
				alarms[i] = alarmFromJson(alarmsArray.getJSONObject(i));
			}
			return alarms;
		}catch (Exception e ){
			return null;
		}
	}

	public Alarm getAlarm(){
		return alarmFromJson(getDataField());
	}

	private Alarm alarmFromJson(JSONObject dataField) {
		try {
			return new Alarm(dataField.getInt("alarm_id"),
					dataField.getInt("device_id"),
					dataField.getInt("state"),
					dataField.getInt("type"),
					dataField.getInt("create_time"),
					getI(dataField, "managed_user"),
					getS("extra"));
		}catch (Exception e){
			return null;
		}
	}

	public Extra getExtra()
	{
		JSONObject data = getDataField();
		if(null!=data){
			try
			{
				return new Extra(data.getInt("device_id"),
						data.getInt("section"),
						data.getInt("zone"),
						data.getString("name"),
						data.getString("value"));
			} catch (JSONException e)
			{
				return null;
			}
		}
		return null;
	}
}
