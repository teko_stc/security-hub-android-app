package biz.teko.td.SHUB_APP.SitesTabs.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ResourceCursorTreeAdapter;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import biz.teko.td.SHUB_APP.Codes.Activities.NLocalUsersActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Delegation.Activities.DelegateListActivity;
import biz.teko.td.SHUB_APP.Delegation.Activities.DelegateSendActivity;
import biz.teko.td.SHUB_APP.DevicesTabs.Activities.DevicesActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptsListActivity;
import biz.teko.td.SHUB_APP.SectionsTabs.SectionsActivity;
import biz.teko.td.SHUB_APP.SitesTabs.SitesActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.ZonesTabs.ZonesActivity;

/**
 * Created by td13017 on 09.02.2017.
 */

public class SitesTreeAdapter extends ResourceCursorTreeAdapter
{

	private final SharedPreferences sp;

	private enum  TYPE
	{
		SITE,
		SITE_DEVICE
	}

	private UserInfo userInfo;
	DBHelper dbHelper;
	Context context;
	private int collapsedGroupLayoutViewId;
	private int expandedGroupLayoutViewId;
	private AlphaAnimation buttonClick = new AlphaAnimation(1F, 0.8F);
	private int sending = 0;

	private boolean showDevices  = true;
	private boolean showSections = true;

	public SitesTreeAdapter(Context context, Cursor cursor, int collapsedGroupLayout, int expandedGroupLayout, int childLayout, int lastChildLayout, boolean showSections, boolean showDevices)
	{
		super(context, cursor, collapsedGroupLayout, expandedGroupLayout, childLayout, lastChildLayout);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();
		View view = newGroupView(context, null, false, null);
		collapsedGroupLayoutViewId = view.getId();
		view = newGroupView(context, null, true, null);
		expandedGroupLayoutViewId = view.getId();
		this.showSections = showSections;
		this.showDevices = showDevices;
		sp = PreferenceManager.getDefaultSharedPreferences(context);

	}


	public void update(Cursor cursor, int sending){
		this.changeCursor(cursor);
		this.sending = sending;
	}

	@Override
	protected Cursor getChildrenCursor(Cursor groupCursor)
	{
		int site_id = groupCursor.getInt(1);
		return dbHelper.getAffectsCursorBySiteId(site_id);
	}

	@Override
	protected void bindGroupView(View view, final Context context, Cursor cursor, boolean isExpanded)
	{
		TextView name = (TextView) view.findViewById(R.id.siteName);
		ImageView imageArm = (ImageView) view.findViewById(R.id.siteArmImage);
		TextView siteArmStatus = (TextView) view.findViewById(R.id.siteId);
		ImageView imageStatement = (ImageView) view.findViewById(R.id.siteImage);


		name.setText(cursor.getString(4));
		final int siteId = cursor.getInt(1);

		String[] armStatements = context.getResources().getStringArray(R.array.arm_statement_titles);
		siteArmStatus.setText(armStatements[dbHelper.getSiteArmStatus(siteId)]);

		final LinkedList<Event> affects = dbHelper.getAffectsBySiteId(siteId);
		LinearLayout imageSmallStLayout = (LinearLayout) view.findViewById(R.id.smallIconLayout);

		int armStatus = dbHelper.getSiteArmStatus(siteId);
		siteArmStatus.setText(armStatements[armStatus]);
		siteArmStatus.setTextColor(armStatus > 0 ? context.getResources().getColor(R.color.brandColorGreen) : context.getResources().getColor(R.color.brandColorDark));
		switch (armStatus){
			case 0:
				imageArm.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_restriction_shield_grey_500_big));
				break;
			case 1:
				imageArm.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_security_checked_green_500_big_48));
				break;
			case 2:
				imageArm.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_partly_armed_big));
				break;
			default:
				break;
		}


		LinearLayout divider = (LinearLayout) view.findViewById(R.id.dividerExp);
		LinearLayout cardLinear = (LinearLayout) view.findViewById(R.id.cardLinearExp);
		if(null!=affects)
			if(0!=affects.size()) {
				if(divider!=null){
					divider.setVisibility(View.GONE);
				}
				if(cardLinear!=null){
					LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) cardLinear.getLayoutParams();
					cardLinear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, layoutParams.height + Func.dpToPx(1, context)));
				}
		 	}

		ImageView[] imagesSmall = new ImageView[8];

		for (int i = 0; i < 7; i++)
		{
			imagesSmall[i] = new ImageView(context);
			imagesSmall[i].setLayoutParams(new LinearLayout.LayoutParams(Func.dpToPx(24, context), Func.dpToPx(24, context)));
		}

		if (null != affects)
		{
			int maxClassId = 4;
			Event affectPriorityMax = null;
			for (Event affect : affects)
			{
				if (affect.classId < 5)
				{
					if ((maxClassId + 1) > affect.classId)
					{
						maxClassId = affect.classId;
						affectPriorityMax = affect;
					}
				}
			}
			int imgSmallNum = 0;
			int alarmI = 0;
			int sabI = 0;
			int malfI = 0;
			int warnI = 0;
			int powI = 0;
			int gprsI = 0;
			int ethI = 0;
			int radioI = 0;

			int stCount = 0;
			String stText = "";


			for (Event affect : affects)
			{
				switch (affect.classId)
				{
					case 0:
						imageStatement.setImageResource(context.getResources().getIdentifier(affect.explainAffectIconBig(), null, context.getPackageName()));
						if(alarmI==0)
						{
							alarmI++;
							if (stCount == 0)
							{
								stCount++;
								stText = context.getString(R.string.ALARM_BIG);
							}
						}
						break;
					case 1:
							if (0 == sabI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								sabI++;

							}
						break;
					case 2:
							if (0 == malfI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								malfI++;
							}
						break;
					case 3:
							if (0 == warnI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
							}
						break;
					case 4:
						break;
					case 5:
						break;
					case 6:
						if(9!=affect.reasonId)
						{
							if (0 == powI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								powI++;
								if(stCount==0){
									stCount++;
									stText = context.getString(R.string.POWER_BIG);
								}else{
									stText += context.getString(R.string.POWER_SMALL);
								}
							}
						}
						break;
					case 7:
						if ((21 == affect.detectorId) || (22 == affect.detectorId))
						{
							if (22 == affect.detectorId)
							{
								if (0 == ethI)
								{
									imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
									imgSmallNum++;
									ethI++;
									if(stCount==0){
										stCount++;
										stText = context.getString(R.string.ETH_BIG);
									}else{
										stText += context.getString(R.string.ETH_SMALL);
									}
								}
							}else{
								if (0 == gprsI)
								{
									imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
									imgSmallNum++;
									gprsI++;
									if(stCount==0){
										stCount++;
										stText = context.getString(R.string.GPRS_BIG);
									}else{
										stText += context.getString(R.string.GPRS_SMALL);
									}
								}
							}
						} else
						{

							if(5!=affect.reasonId)
							{
								if (0 == radioI)
								{
									imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
									imgSmallNum++;
									radioI++;
									if(stCount==0){
										stCount++;
										stText = context.getString(R.string.RADIO_BIG);
									}else{
										stText += context.getString(R.string.RADIO_SMALL);
									}
								}
							}
						}

						break;
					default:
						break;
				}
			}
			if(null!=imageSmallStLayout)
			{
				for (int n = 0; n < imgSmallNum; n++)
				{
					imageSmallStLayout.addView(imagesSmall[n]);
				}
			}
		}

		ImageView imageMore = (ImageView) view.findViewById(R.id.imageMore);
		TextView buttonMore = (TextView) view.findViewById(R.id.buttonMore);
		TextView buttonArm = (TextView) view.findViewById(R.id.buttonArm);
		TextView buttonDisarm = (TextView) view.findViewById(R.id.buttonDisarm);
		TextView buttonRelay = (TextView) view.findViewById(R.id.buttonRelay);
		TextView buttonCodes = (TextView) view.findViewById(R.id.buttonCodes);

		if((userInfo.roles&Const.Roles.DOMAIN_HOZ_ORG)==0)
		{
			if (buttonArm != null)
			{
				buttonArm.setVisibility(View.GONE);
			}
			if (buttonDisarm != null)
			{
				buttonDisarm.setVisibility(View.GONE);
			}
		}
		imageMore.setOnClickListener(new siteCardButtonMoreOnClick(siteId, dbHelper));

		if(buttonMore!=null)
		{
			if(showDevices){
				buttonMore.setText(R.string.DEVICES_TITLE);
				buttonMore.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						v.startAnimation(buttonClick);
						Intent intent = new Intent(context, DevicesActivity.class);
						intent.putExtra("site", siteId);
						((Activity) context).startActivity(intent);
						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
			}else{
				if(showSections)
				{
					buttonMore.setText(R.string.SECTIONS);
					buttonMore.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							v.startAnimation(buttonClick);
							Intent intent = new Intent(context, SectionsActivity.class);
							intent.putExtra("site", siteId);
							intent.putExtra("device", -1);
							((Activity) context).startActivity(intent);
							((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
						}
					});
				}else{
					buttonMore.setText(R.string.BUTTON_DEVICES);
					buttonMore.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							v.startAnimation(buttonClick);
							Intent intent = new Intent(context, ZonesActivity.class);
							intent.putExtra("site", siteId);
							intent.putExtra("device", -1); //if we show only zones instead from sites
							intent.putExtra("section", -1); //if we show only zones instead from sites
							intent.putExtra("version", 0); //0 - if we show zones, 1 - if we show relays
							((Activity) context).startActivity(intent);
							((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
						}
					});
				}
			}

		}

		/*TODO ADD LOGICS FOR COMPLEX SITES*/
		if(buttonArm!=null)
		{
			if(sending==0)
			{
				buttonArm.setEnabled(true);
				buttonArm.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						v.startAnimation(buttonClick);
						Device[] devices = dbHelper.getAllDevicesForSite(siteId);
						if(null!=devices && devices.length >0)
						{
							for (Device device : devices)
							{
								if(dbHelper.getDevicePartedStatusForSite(siteId, device.id)){
									Section[] sections = dbHelper.getGuardSectionsForSiteDeviceWithoutZones(siteId, device.id);
									if(null!=sections)
									{
										for (Section section : sections)
										{
											try
											{
												JSONObject commandAddress = new JSONObject();
												commandAddress.put("section", section.id);
												int command_count = Func.getCommandCount(context);
												D3Request d3Request = D3Request.createCommand(userInfo.roles, device.id, "ARM", commandAddress, command_count);
												if (d3Request.error == null)
												{
													sendMessageToServer(d3Request, true);
												} else
												{
													Func.pushToast(context, d3Request.error, (Activity) context);
												}
											} catch (JSONException e)
											{
												e.printStackTrace();
											}
										}
									}else{
										Func.nShowMessage(context, "Нет охранных разделов на устройстве");
									}
								}else{
									JSONObject commandAddress = new JSONObject();
									int command_count = Func.getCommandCount(context);
									D3Request d3Request = D3Request.createCommand(userInfo.roles, device.id, "ARM", commandAddress, command_count);
									if(d3Request.error == null){
										sendMessageToServer(d3Request, true);
									}else{
										Func.pushToast(context, d3Request.error, (Activity) context);
									}
								}
							}
						}else{
							Func.nShowMessage(context, context.getString(R.string.MESS_NO_DEVICES_ASSIGNED_TO_SITE));
						}
					}
				});
			}else{
				buttonArm.setEnabled(false);
			}
		}

		if(buttonDisarm!=null)
		{
			if(sending == 0)
			{
				buttonDisarm.setEnabled(true);
				buttonDisarm.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						v.startAnimation(buttonClick);
						Device[] devices = dbHelper.getAllDevicesForSite(siteId);
						if(null!=devices)
						{
							for (Device device : devices)
							{
								if(dbHelper.getDevicePartedStatusForSite(siteId, device.id)){
									Section[] sections = dbHelper.getGuardSectionsForSiteDeviceWithoutZones(siteId, device.id);
									if(null!=sections)
									{
										for (Section section : sections)
										{
											try
											{
												JSONObject commandAddress = new JSONObject();
												commandAddress.put("section", section.id);
												int command_count = Func.getCommandCount(context);
												D3Request d3Request = D3Request.createCommand(userInfo.roles, device.id, "DISARM", commandAddress, command_count);
												if (d3Request.error == null)
												{
													sendMessageToServer(d3Request, true);
												} else
												{
													Func.pushToast(context, d3Request.error, (Activity) context);
												}
											} catch (JSONException e)
											{
												e.printStackTrace();
											}
										}
									}else{
										Func.nShowMessage(context, "Нет охранных разделов на устройстве");
									}
								}else{
									JSONObject commandAddress = new JSONObject();
									int command_count = Func.getCommandCount(context);
									D3Request d3Request = D3Request.createCommand(userInfo.roles, device.id, "DISARM", commandAddress, command_count);
									if(d3Request.error == null){
										sendMessageToServer(d3Request, true);
									}else{
										Func.pushToast(context, d3Request.error, (Activity) context);
									}
								}
							}
						}
					}
				});
			}else{
				buttonDisarm.setEnabled(false);
			}
		}

		if(Func.getBooleanSPDefTrue(sp, "pref_show_relays")){
			if(null!=buttonRelay){
				buttonRelay.setVisibility(View.VISIBLE);
				buttonRelay.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						v.startAnimation(buttonClick);
						Intent intent = new Intent(context, ZonesActivity.class);
						intent.putExtra("site", siteId);
						intent.putExtra("device", -1); //if we show only zones instead from sites
						intent.putExtra("section", -1); //if we show only zones instead from sites
						intent.putExtra("version", 1); //0 - if we show zones, 1 - if we show relays
						((Activity) context).startActivity(intent);
						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
			}
		}


		if(null!=buttonCodes){
			if((userInfo.roles&Const.Roles.TRINKET)==0)
			{
				//			if(((userInfo.roles&Const.Roles.ORG_INGEN) != 0)||((userInfo.roles& Const.Roles.DOMAIN_INGEN) != 0)){
				buttonCodes.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						v.startAnimation(buttonClick);
						Intent intent = new Intent(context, NLocalUsersActivity.class);
						intent.putExtra("site", siteId);
						intent.putExtra("device", -1);
						((Activity) context).startActivity(intent);
						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
				//			}else{
				//				buttonCodes.setEnabled(false);
				//			}
			}else{
				buttonCodes.setVisibility(View.GONE);
			}
		}

	}

	@Override
	protected void bindChildView(View view, final Context context, Cursor cursor, boolean isLastChild)
	{
		TextView affectDesc = (TextView) view.findViewById(R.id.affectName);
		TextView affectPlace = (TextView) view.findViewById(R.id.affectPlace);
		affectPlace.setVisibility(View.VISIBLE);
		ImageView affectImage = (ImageView) view.findViewById(R.id.affectImage);
		TextView affectDevice = (TextView) view.findViewById(R.id.affectDevice);

		final Event affect = dbHelper.getAffect(cursor);

		final String affectExplanation = affect.explainAffectRegDescription(context);
		affectDesc.setText(Html.fromHtml(affectExplanation));

		final Device device = dbHelper.getDeviceById(affect.device);
		String deviceDesc = context.getResources().getString(R.string.UNKNOWN_DEVICE);
		if(null!=device){
			affectDevice.setText(device.getName() + "(" + Integer.toString(device.account) + ")");
		}

		String sectionText = "";
		String zoneText = "";
		String placeText = "";
		if(affect.section == 0){
			if(affect.zone != 0){
				zoneText = dbHelper.getZoneNameById(affect.device, affect.section, affect.zone) + "(" + affect.zone + ")";
			}else{
				affectPlace.setVisibility(View.GONE);
			}
			placeText += zoneText;
		}else{
			if(affect.zone ==0){
				sectionText = dbHelper.getSectionNameByIdWithOptions(affect.device, affect.section) + "(" + affect.section + ") "
//						+ "- " + context.getString(R.string.WHOLE_SECTION)
				;
				placeText += sectionText;
			}else{
				zoneText = dbHelper.getZoneNameById(affect.device, affect.section, affect.zone) + "(" + affect.zone + ")";
				sectionText = dbHelper.getSectionNameByIdWithOptions(affect.device, affect.section) + "(" + affect.section + ")";
				placeText += zoneText + " • " + sectionText;
			}
		}
		affectPlace.setText(placeText);
		affectImage.setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
		View divider = (View) view.findViewById(R.id.childDivider);
		View padding = (View) view.findViewById(R.id.childPadding);
		if(isLastChild){
			divider.setVisibility(View.VISIBLE);
			padding.setVisibility(View.VISIBLE);
		}else{
			divider.setVisibility(View.GONE);
			padding.setVisibility(View.GONE);

		}


		final String finalDeviceDesc = deviceDesc;
		final String finalZoneText = zoneText;
		final String finalSectionText = sectionText;
		view.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Func.showAffectInfo(context, -1, finalZoneText, finalSectionText, affectExplanation,  affect );;
			}
		});
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		Cursor cursor = getGroup(groupPosition);
		View v = convertView;
		if (cursor != null)
		{
			if (convertView == null)
			{
				v = newGroupView(context, cursor, isExpanded, parent);
			} else
			{
				if ((isExpanded && convertView.getId() == collapsedGroupLayoutViewId) || (!isExpanded && convertView.getId() == expandedGroupLayoutViewId))
				{
					v = newGroupView(context, cursor, isExpanded, parent);
				}
			}
			bindGroupView(v, context, cursor, isExpanded);
		}
		return v;
	}

	@Override
	public void notifyDataSetChanged() {
		notifyDataSetChanged(false);
	}


	private class siteCardButtonMoreOnClick implements View.OnClickListener
	{
		private final SitesActivity activity;
		private int siteId;
		private  Site site;
		private DBHelper dbHelper;

		public siteCardButtonMoreOnClick(int siteId, DBHelper dbHelper)
		{
			this.siteId = siteId;
			this.dbHelper = dbHelper;
			this.site = dbHelper.getSiteById(siteId);
			activity  = (SitesActivity) context;
		}

		@Override
		public void onClick(View v)
		{
			userInfo = dbHelper.getUserInfo();
			AlertDialog.Builder deviceMenuDialogBuilder = Func.adbForCurrentSDK(context);
			deviceMenuDialogBuilder.setTitle(site.name);

			final String[] menuItemsArray = context.getResources().getStringArray(R.array.site_menu_values);
			List<String> menuItemsList = new ArrayList<>();
			menuItemsList.add(menuItemsArray[0]);
			if(0!=(userInfo.roles& Const.Roles.DOMAIN_LAWYER))
			{
				switch (site.delegated)
				{
					case 0:
						menuItemsList.add(menuItemsArray[1]);
						menuItemsList.add(menuItemsArray[2]);
						if((userInfo.roles&Const.Roles.DOMAIN_ADMIN) !=0){
							menuItemsList.add(menuItemsArray[5]);
						}
						break;
					case 1:
						if(site.domain != userInfo.domain)
						{
							menuItemsList.add(menuItemsArray[4]);
						}else
						{
							menuItemsList.add(menuItemsArray[1]);
							menuItemsList.add(menuItemsArray[2]);
							menuItemsList.add(menuItemsArray[3]);
							if((userInfo.roles&Const.Roles.DOMAIN_ADMIN) !=0){
								menuItemsList.add(menuItemsArray[5]);
							}
						}
						break;
					default:
						break;
				}
			}

			if(Func.needScriptsForBuild()){
				if((!Func.isMasterMode(sp)
					||((Func.isMasterMode(sp)) &&  !Func.getBooleanSPDefFalse(sp, "pref_show_devices")))
						&& (0==(userInfo.roles&Const.Roles.TRINKET))
				){
					menuItemsList.add(menuItemsArray[6]);
				}
			}

			final CharSequence[] items = menuItemsList.toArray(new CharSequence[menuItemsList.size()]);
			deviceMenuDialogBuilder.setItems(items, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, int which)
				{
					String curValue = items[which].toString();
					if(curValue.equals(menuItemsArray[0])){showAboutDialog(site);}
					else if(curValue.equals(menuItemsArray[1])){showRenameDialog(site);}
					else if(curValue.equals(menuItemsArray[2])){goToDelegateSend(site);}
					else if(curValue.equals(menuItemsArray[3])){goToDelegationList(site);}
					else if(curValue.equals(menuItemsArray[4])){showDelegateRevokeDialog(site);}
					else if(curValue.equals(menuItemsArray[5])){showDeleteDialog(site);}
					else if(curValue.equals(menuItemsArray[6])){openScripts(site);}
					dialog.dismiss();
				}
			});

			deviceMenuDialogBuilder.show();
		}
	}

	private void openScripts(Site site)
	{
		Intent intent = new Intent(context, ScriptsListActivity.class);
		intent.putExtra("site_id", site.id);
		intent.putExtra("device_id", -1);
		((Activity) context).startActivity(intent);
		((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
	}

	private void showDeleteDialog(final Site site)
	{
		Device[] devices = dbHelper.getAllDevicesForSite(site.id);
		if(null!=devices){
			if(devices.length > 1){
				/*CANT_DEL*/
				Func.nShowMessage(context, context.getString(R.string.ERROR_CANT_DELETE_SITE_WITH_TWO_OR_MORE_DEVICES));
				return;
			}else if (devices.length == 1){
				/*DEVICE_REVOKE_ASSIGN*/
				if (PrefUtils.getInstance(context).getPinEnabled())
				{
					showDeleteDialogWithPin(site, TYPE.SITE_DEVICE);
				} else
				{
					showDeleteDialogNoPin(site, TYPE.SITE_DEVICE);
				}
				return;
			}
		}
		/*SITE_DEL*/
		if (PrefUtils.getInstance(context).getPinEnabled())
		{
			showDeleteDialogWithPin(site, TYPE.SITE);
		} else
		{
			showDeleteDialogNoPin(site, TYPE.SITE);
		}
	}

	private void showDeleteDialogNoPin(final Site site, final TYPE type)
	{
		final AlertDialog.Builder deleteDialogBuilder = Func.adbForCurrentSDK(context);

		deleteDialogBuilder.setTitle(R.string.STA_DELETE_SITE_TITLE);
		deleteDialogBuilder.setMessage(R.string.STA_DELETE_TITLE_MESS);

		deleteDialogBuilder.setPositiveButton(R.string.DELETE, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
			}
		});
		deleteDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
				Func.hideKeyboard((SitesActivity) context);
			}
		});

		final AlertDialog deleteDialog = deleteDialogBuilder.create();
		deleteDialog.show();
		deleteDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				switch (type){
					case SITE:
						sendSiteDel(site);
						break;
					case SITE_DEVICE:
						sendDeviceRevoke(site);
						break;
				}
				deleteDialog.dismiss();
			}
		});
	}

	private void showDeleteDialogWithPin(final Site site, final TYPE type)
	{
		final AlertDialog.Builder deleteDialogBuilder = Func.adbForCurrentSDK(context);
		deleteDialogBuilder.setTitle(R.string.STA_DELETE_SITE_TITLE);
		deleteDialogBuilder.setMessage(R.string.STA_DELETE_TITLE_MESS);
		View view = (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.custom_pin_enter_dialog, null);
		final EditText pinText = (EditText) view.findViewById(R.id.editTextPIN);
		pinText.requestFocus();
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
		pinText.setOnFocusChangeListener(new View.OnFocusChangeListener()
		{
			@Override
			public void onFocusChange(View view, boolean b)
			{
				if (!b)
				{
					InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
					inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
				}
			}
		});
		final TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.pinError);
		textInputLayout.setErrorEnabled(true);
		deleteDialogBuilder.setView(view);
		deleteDialogBuilder.setPositiveButton(R.string.DELETE, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
			}
		});
		deleteDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
				Func.hideKeyboard((SitesActivity) context);
			}
		});

		final AlertDialog deleteDialog = deleteDialogBuilder.create();
		deleteDialog.show();

		deleteDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (4 != pinText.getText().toString().length())
				{
					textInputLayout.setError(context.getString(R.string.ERROR_WRONG_PIN));
				} else
				{
					String pin = Func.md5(String.valueOf(Integer.parseInt(pinText.getText().toString())));
					String dbPin = dbHelper.getUserInfo().pinCode;
					if (pin.equals(dbPin))
					{
						switch (type){
							case SITE:
								sendSiteDel(site);
								break;
							case SITE_DEVICE:
								sendDeviceRevoke(site);
								break;
						}
						deleteDialog.dismiss();
					} else
					{
						textInputLayout.setError(context.getString(R.string.ERROR_WRONG_PIN));
						pinText.setText("");
					}
				}
			}
		});
	}

	private void sendDeviceRevoke(Site site)
	{
		Device[] devices = dbHelper.getAllDevicesForSite(site.id);
		JSONObject message = new JSONObject();
		try
		{
			message.put("device_id", devices[0].id);
			message.put("domain_id", userInfo.domain);
			message.put("site", site.id);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		D3Request d3Request = D3Request.createMessage(userInfo.roles, "DEVICE_REVOKE_DOMAIN", message);
		if (d3Request.error == null)
		{
			sendMessageToServer(d3Request, false);
		} else
		{
			Func.pushToast(context, d3Request.error, (Activity) context);
		}
		Func.hideKeyboard((SitesActivity) context);
	}

	public void sendSiteDel(Site site){
		JSONObject message = new JSONObject();
		try
		{
			message.put("site", site.id);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		D3Request d3Request = D3Request.createMessage(userInfo.roles, "SITE_DEL", message);
		if (d3Request.error == null)
		{
			sendMessageToServer(d3Request, false);
		} else
		{
			Func.pushToast(context, d3Request.error, (Activity) context);
		}
		Func.hideKeyboard((SitesActivity) context);
	}

	private void showDelegateRevokeDialog(final Site site)
	{
		if(PrefUtils.getInstance(context).getPinEnabled()){

			final AlertDialog.Builder delRefuseDialogBuilder = Func.adbForCurrentSDK(context);
			delRefuseDialogBuilder.setTitle(R.string.DELEGATION_REFUSE_DIALOG_TITLE);
			delRefuseDialogBuilder.setMessage(R.string.DELEGATION_REFUSE_DIALOG_MESS);

			View view = (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.custom_pin_enter_dialog, null);
			final EditText pinText = (EditText) view.findViewById(R.id.editTextPIN);
			int pinMaxLength = 4;
			pinText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(pinMaxLength)});
			pinText.setOnFocusChangeListener(new View.OnFocusChangeListener()
			{
				@Override
				public void onFocusChange(View view, boolean b)
				{
					if (b)
					{
						pinText.setInputType(InputType.TYPE_CLASS_NUMBER);
						InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(pinText, InputMethodManager.SHOW_IMPLICIT);
					}
				}
			});
			final TextInputLayout textInputLayout = (TextInputLayout) view.findViewById(R.id.pinError);
			textInputLayout.setErrorEnabled(true);
			delRefuseDialogBuilder.setView(view);

			delRefuseDialogBuilder.setPositiveButton(R.string.ADB_ACCEPT, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
				}
			});
			delRefuseDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
					Func.hideKeyboard((SitesActivity) context);
				}
			});

			final AlertDialog delRefuseDialog = delRefuseDialogBuilder.create();
			delRefuseDialog.show();
			delRefuseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (4 != pinText.getText().toString().length())
					{
						textInputLayout.setError(context.getString(R.string.ERROR_WRONG_PIN));
					} else
					{
						String pin = Func.md5(String.valueOf(Integer.parseInt(pinText.getText().toString())));
						String dbPin = dbHelper.getUserInfo().pinCode;
						if (pin.equals(dbPin))
						{
							JSONObject message = new JSONObject();
							try
							{
								message.put("site_id", site.id);
								message.put("domain_id", userInfo.domain);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							D3Request d3Request = D3Request.createMessage(userInfo.roles, "DELEGATE_REVOKE", message);
							if (d3Request.error == null)
							{
								sendMessageToServer(d3Request, false);
							} else
							{
								Func.pushToast(context, d3Request.error, (SitesActivity) context);
							}
							delRefuseDialog.dismiss();
							Func.hideKeyboard((SitesActivity) context);
						}else{
							textInputLayout.setError(context.getString(R.string.ERROR_WRONG_PIN));
							pinText.setText("");
						}
					}

				}
			});
		}else{
			final AlertDialog.Builder delRefuseDialogBuilder = Func.adbForCurrentSDK(context);
			delRefuseDialogBuilder.setTitle(R.string.DELEGATION_REFUSE_DIALOG_TITLE);
			delRefuseDialogBuilder.setMessage(R.string.DELEGATION_REFUSE_DIALOG_MESS);
			delRefuseDialogBuilder.setPositiveButton(R.string.ADB_ACCEPT, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
				}
			});
			delRefuseDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
					Func.hideKeyboard((SitesActivity) context);
				}
			});

			final AlertDialog delRefuseDialog = delRefuseDialogBuilder.create();
			delRefuseDialog.show();
			delRefuseDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					JSONObject message = new JSONObject();
					try
					{
						message.put("site_id", site.id);
						message.put("domain_id", userInfo.domain);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					D3Request d3Request = D3Request.createMessage(userInfo.roles, "DELEGATE_REVOKE", message);
					if (d3Request.error == null)
					{
						sendMessageToServer(d3Request, false);
					} else
					{
						Func.pushToast(context, d3Request.error, (Activity) context);
					}
					delRefuseDialog.dismiss();
					Func.hideKeyboard((SitesActivity) context);

				}
			});
		}
	}

	private void goToDelegateSend(Site site)
	{
		Intent intent = new Intent(context, DelegateSendActivity.class);
		intent.putExtra("site", site.id);
		((Activity) context).startActivity(intent);
		((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
	}

	private void goToDelegationList(Site site)
	{
		Intent delegationListActivityintent = new Intent(context, DelegateListActivity.class);
		delegationListActivityintent.putExtra("site", site.id);
		((Activity) context).startActivity(delegationListActivityintent);
		((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
	}

	private void showRenameDialog(final Site site)
	{
		AlertDialog.Builder renameDialogBuilder = Func.adbForCurrentSDK(context);

		renameDialogBuilder.setTitle(R.string.STA_RENAME_SITE);
		renameDialogBuilder.setMessage(R.string.TA_ENTER_NEW_NAME);

		LinearLayout parentLayout = new LinearLayout(context);
		parentLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		parentLayout.setOrientation(LinearLayout.VERTICAL);
		parentLayout.setPadding(Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context));

		final EditText editName = new EditText(context);
		editName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		editName.setCursorVisible(true);
		editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
		editName.isCursorVisible();
		try
		{
			Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
			f.setAccessible(true);
			f.set(editName, R.drawable.caa_cursor);
		} catch (Exception ignored)
		{
		}
		editName.setGravity(Gravity.CENTER);
		editName.setTextColor(context.getResources().getColor(R.color.md_black_1000));
		editName.setText(site.name);
		int editNameMaxLength = 32;
		editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
		editName.requestFocus();

		parentLayout.addView(editName);
		renameDialogBuilder.setView(parentLayout);

		renameDialogBuilder.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

			}
		});

		renameDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				//									Func.hideKeyboard(activity);
				dialog.dismiss();
			}
		});

		final AlertDialog renameDialog = renameDialogBuilder.create();

		renameDialog.show();
		renameDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if (!editName.getText().toString().matches(""))
				{
					String newName = editName.getText().toString();
					//DO CHANGE NAME
					JSONObject message = new JSONObject();
					try
					{
						message.put("id", site.id);
						message.put("name", newName);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					D3Request d3Request = D3Request.createMessage(userInfo.roles, "SITE_SET", message);
					if(d3Request.error == null){
						sendMessageToServer(d3Request, false);
						renameDialog.dismiss();
					}else{
						Func.pushToast(context, d3Request.error, (Activity) context);
					}
				} else
				{
					Func.pushToast(context, context.getString(R.string.DELA_INCORRECT_NAME_PUSHTOAST), (SitesActivity) context);
				}
			}
		});
	}

	private void showAboutDialog(Site site)
	{
		AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		adb.setTitle(R.string.TITLE_OBJECT_INFO);
		String message = "\n";
		message += context.getString(R.string.OBJCET_NAME_DOTS) + " " + site.name + "\n\n";

		message+=context.getString(R.string.DEVICES_DOTS) + "\n";
		Device[] devices = dbHelper.getAllDevicesForSite(site.id);
		if(null!=devices && 0 != devices.length)
		{
			for (Device device : devices)
			{
				message += device.getName() + "(" + Integer.toString(device.account) + ")\n";
			}
		}else{
			message += context.getString(R.string.SITE_ABOUT_NO_DEVICES) + "\n";
		}

		int sectionsCount = dbHelper.getSectionsCount(site.id);
		message += context.getString(R.string.SECTIONS_COUNT_DOTS) + " " +  (sectionsCount) + "\n\n";

		int zonesCount = dbHelper.getZonesCountForSite(site.id);
		message += context.getString(R.string.DEVICES_COUNT_DOTS) + " " + (zonesCount) + "\n\n";

		int oparetorsCount = dbHelper.getOperatorsCount();
		message += context.getString(R.string.USERS_COUNT_DOTS) + " " + oparetorsCount + "\n\n";
		message += context.getString(R.string.DOMAIN_NUMBER) + " " + site.domain + "\n\n";
		switch (site.delegated)
		{
			case 0:
				break;
//			case 1:
//				message += context.getString(R.string.DOMAIN_DELEGATED_FROM) +"\n\n";
//				break;
//			case 2:
//				message += context.getString(R.string.DOMAIN_DELEGATED_TO) + "\n\n";
//				break;
			default:
				message += (site.domain == userInfo.domain) ? context.getString(R.string.DOMAIN_DELEGATED_TO) : context.getString(R.string.DOMAIN_DELEGATED_FROM);
				message += "\n\n";
				break;
		}


		long t = dbHelper.getSiteLastEventTime(site.id);

		final LinkedList<Event> affects = dbHelper.getAffectsBySiteId(site.id);
		int count = 0;
		if (null != affects)
		{
			for (Event affect : affects)
			{
				if ((2 == affect.classId) && (affect.reasonId == 3))
				{
					count++;
				}
			}
		}
		String st = context.getString(R.string.EXIST);
		if (count != 0)
		{
			st = context.getString(R.string.NO_EXIST);
		}
		message += context.getString(R.string.CONNECTION_DOTS) + " " + st + "\n\n";

		message += context.getString(R.string.LAST_CONNECTION_TIME) + " " + (Func.getTimeDateText(context, t)) + "\n\n";

		adb.setMessage(message);
		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		adb.show();
	}

	public void sendMessageToServer(D3Request d3Request, boolean command)
	{
		Activity activity = (SitesActivity)context;
		if(activity != null){
			SitesActivity sitesActivity = (SitesActivity) activity;
			sitesActivity.sendMessageToServer(d3Request, command);
		}
//		return  Func.sendMessageToServer(d3Request, getService(), context, command, sending);
	};

//	public D3Service getService()
//	{
//		Activity activity = (SitesActivity)context;
//		if(activity != null){
//			SitesActivity mainActivity = (SitesActivity) activity;
//			return mainActivity.getLocalService();
//		}
//		return null;
//	}

}
