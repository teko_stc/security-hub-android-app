package biz.teko.td.SHUB_APP.D3.D3XProtoEvent;

import android.content.Context;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

public class AType
{
	@Attribute(name = "id")
	public int id;

	@Attribute(name = "caption")
	public String caption;

	@Attribute(name = "icon")
	public String icon;

	@Attribute(name = "default", required = false)
	public int def;

	@ElementList(name = "icons", entry = "icon", required = false)
	public LinkedList<Icon> icons;

	public int getListIcon(Context context)
	{
		return context.getResources().getIdentifier(this.icons.get(0).src, null, context.getPackageName());
	}

	public int getMainIcon(Context context)
	{
		return context.getResources().getIdentifier(this.icons.get(1).src, null, context.getPackageName());
	}

	public int getBigIcon(Context context)
	{
			return context.getResources().getIdentifier(this.icons.get(2).src, null, context.getPackageName());
	}

}
