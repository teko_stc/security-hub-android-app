package biz.teko.td.SHUB_APP.Wizards.Activity.Compoments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.UserSetViewPagerAdapter;

public class WizardUserSetActivity extends BaseActivity
{
	private Context context;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private int selSiteId;
	private int selDeviceId;
	private int selRegType;
	private ViewPager pager;
	public final  static int PAGER_COUNT  = 3;
	private UserSetViewPagerAdapter adapter;
	private DBHelper dbHelper;
	private UserInfo userInfo;
	private Device device;
	private String deviceSerial;
	private String deviceModel;
	private String selSections = "";
	private String selName;
	private String UID;
	private int UIDFormat;
	private String selPass;
	private LinkedList<Section> selSectionArray;


	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver newUserRegReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(pager.getCurrentItem() == WizardUserSetActivity.PAGER_COUNT - 1)
			{
				Activity activity = (WizardUserSetActivity) context;
				if (null != activity)
				{
					WizardUserSetActivity wizardUserSetActivity = (WizardUserSetActivity) activity;
					int device = intent.getIntExtra("deviceId", -1);
					if(device == wizardUserSetActivity.getDeviceId()){
						wizardUserSetActivity.setNextPage();
					}
				}
			}
		}
	};

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_user_set);
		selSiteId = getIntent().getIntExtra("site_id", -1);
		selDeviceId = getIntent().getIntExtra("device_id", -1);

		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();
		context = this;

		serviceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.USER_SET_TITLE);
		if(-1 != selDeviceId){
			device = dbHelper.getDeviceById(selDeviceId);
			if(null!=device)
			{
				toolbar.setSubtitle(device.getType().caption + "(" + device.account + ")  •  " + getString(R.string.USER_CODE_ADD_TITLE));
				toolbar.setSubtitleTextColor(getResources().getColor(R.color.brandColorWhite));
			}

		}

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		ImageView closeButton = (ImageView) findViewById(R.id.wizCloseButton);
		closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});



		adapter = new UserSetViewPagerAdapter(getSupportFragmentManager());
		pager = (ViewPager) findViewById(R.id.userSetViewPager);
		pager.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true;
			}
		});
		pager.setAdapter(adapter);

	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(newUserRegReceiver, new IntentFilter(D3Service.BROADCAST_USER_ADD));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(newUserRegReceiver);
	}

	@Override
	public void onBackPressed() {
		Func.hideKeyboard((WizardUserSetActivity)context);
		if (pager.getCurrentItem() == 0) {
			super.onBackPressed();
		} else {
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position - 1);
		}
	}

	public void setNextPage()
	{
		if(pager.getCurrentItem() != PAGER_COUNT - 1)
		{
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position + 1);
			refreshViews();
		}else{
			Intent activityIntent = new Intent(context, WizardUserSetFinishActivity.class);
			activityIntent.putExtra("site_id", selSiteId );
			activityIntent.putExtra("device_id", selDeviceId );
			activityIntent.putExtra("sections", selSections);
			activityIntent.putExtra("name", selName);
			startActivity(activityIntent);
			finish();
		}
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	public int getSiteId(){
		return selSiteId;
	}

	public void setSiteID(int siteId){
		this.selSiteId = siteId;
	}

	public int  getDeviceId(){
		return selDeviceId;
	}

	public void  setDeviceid(int device_id){
		this.selDeviceId = device_id;
	}

	public int getRegType(){
		return selRegType;
	}

	public void setRegType(int regType){
		this.selRegType = regType;
	}

	public String getSections(){
		return  selSections;
	}

	public void setSections(String sections){
		this.selSections = sections;
	}

	public void setUID(String UID)
	{
		this.UID = UID;
	}

	public String getUID(){
		return  this.UID;
	}

	public void setUIDFormat(int UIDFormat)
	{
		this.UIDFormat = UIDFormat;
	}

	public int getUIDFormat(){
		return this.UIDFormat;
	}

	public void setName(String name){
		this.selName = name;
	}

	public String getName(){
		return this.selName;
	}

	public void setPass(String pass){
		this.selPass = pass;
	}

	public String getPass(){
		return this.selPass;
	}

	public void setSectionsArray(LinkedList<Section> sectionsArray)
	{
		this.selSectionArray = sectionsArray;
	}

	public LinkedList<Section> getSectionArray()
	{
		return selSectionArray;
	}

	public void refreshViews(){
		adapter.notifyDataSetChanged();
	}

}
