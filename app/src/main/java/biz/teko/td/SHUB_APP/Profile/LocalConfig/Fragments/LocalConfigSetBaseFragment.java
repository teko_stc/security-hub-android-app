package biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.List;

import biz.teko.td.SHUB_APP.Fragments.ProgressDialogFragment;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Activities.LocalConfigNavigation;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.ISetConfig;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Presenters.SetConfigPresenter;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;

abstract class LocalConfigSetBaseFragment extends Fragment implements ISetConfig {
    private final LocalConfigNavigation activity;
    private View layout;
    private final DeviceDTO device;
    private final SocketConnection connection;

    private ProgressDialogFragment progressDialog;
    protected SetConfigPresenter setConfigPresenter;
    protected List<String> stringsAPNs;

    public LocalConfigSetBaseFragment(LocalConfigNavigation activity, DeviceDTO device, SocketConnection connection) {
        this.activity = activity;
        this.device = device;
        this.connection = connection;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setConfigPresenter = new SetConfigPresenter(
                connection, device);
        setConfigPresenter.attachSetActivity(this);
        getLists();
    }


    protected String getReplaceEditText(EditText editText) {
        return getReplaceText(editText.getText().toString());
    }

    protected String getReplaceText(String string) {
        return string.replace(" ", "");
    }

    private void getLists() {
        //stringsAPNs = getIntent().getStringArrayListExtra(ConfigConstants.STRINGS_APN);
    }

    @Override
    public void onResume() {
        super.onResume();
        setConfigPresenter.sendReplyTimer();
    }
    @Nullable
    public LocalConfigNavigation getBaseActivity() {
        return activity;
    }

    public View getLayout() {
        return layout;
    }

    public DeviceDTO getDevice() {
        return device;
    }

    public SocketConnection getConnection() {
        return connection;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //if (!setConfigPresenter.isCancel() && !setConfigPresenter.isAccept())
        //    setConfigPresenter.testModeOff();
        setConfigPresenter.detachSetActivity();
    }

    @Override
    public void showProgressBar() {
        progressDialog = new ProgressDialogFragment();
        progressDialog.setCancelable(false);
        progressDialog.show(getChildFragmentManager(), "progressBar");
    }

    @Override
    public void dismissProgressBar(boolean successful) {
        progressDialog.dismiss();
        if (!successful)
            Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void close(int result) {
        activity.runOnUiThread(activity::openDeviceList);
    }

    public void onBackPressed()
    {
        activity.finish();
        setConfigPresenter.cancel();
    }
}
