package biz.teko.td.SHUB_APP.D3DB;

/**
 * Created by td13017 on 02.02.2017.
 */
public class AddressElement
{
	public String name;
	public int code;
	public int parent_code;
	public String addr_type;
	public String canonical;

	public AddressElement(String name, int code, int parent_code, String addr_type, String canonical){
		this.name = name;
		this.code = code;
		this.parent_code = parent_code;
		this.addr_type = addr_type;
		this.canonical = canonical;
	}
}
