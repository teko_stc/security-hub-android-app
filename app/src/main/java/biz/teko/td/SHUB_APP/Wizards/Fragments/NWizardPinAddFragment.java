package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDotsView;
import biz.teko.td.SHUB_APP.Utils.NViews.NKeyPadView;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardPinActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardWelcomeActivity;

public class NWizardPinAddFragment extends Fragment {
    private int enteredCount;
    private int pin;
    private WizardPinActivity activity;
    private NDotsView dotsView;
    private NKeyPadView keypad;
    private NActionButton next;
    private NActionButton logout;
    private int from;
    private int typedPin, savedPin;
    private LinearLayout backLayout;
    private Context context;

    private OnBackPressedCallback backCallback = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            onBackPressed();
        }
    };


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        View view = inflater.inflate(R.layout.n_fragment_wizar_pin_add, container, false);
        setup(view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (WizardPinActivity) getActivity();
        activity.getOnBackPressedDispatcher().addCallback(backCallback);
        from = getArguments().getInt("FROM");
        savedPin = getArguments().getInt("pin", -1);
        setView();
        bind();
    }

    private void setView() {
        if (from == D3Service.OPEN_FROM_RESET_PIN)
            logout.setVisibility(View.GONE);
        else
            backLayout.setVisibility(View.GONE);
        if (savedPin != -1)
            setSavedPin();
    }

    private void setup(View view) {
        dotsView = view.findViewById(R.id.nPinDots);
        keypad = view.findViewById(R.id.nPinKeyPad);

        next = view.findViewById(R.id.nPinSet);
        logout = view.findViewById(R.id.nPinLogout);

        backLayout = view.findViewById(R.id.nButtonBack);
        backLayout.setOnClickListener(v -> onBackPressed());
    }

    private void bind() {
        if (null != next) {
            next.setDisable(savedPin == -1);
            next.setOnButtonClickListener(action -> {
                typedPin = pin;
                int forcedId = activity.getSiteIdForForceCode(Func.md5(String.valueOf(typedPin)));
                if (0 == forcedId) {
                    Bundle bundle = new Bundle();
                    bundle.putInt("pin", typedPin);
                    if (from == D3Service.OPEN_HISTORY_FROM_STATUS_BAR) {
                        bundle.putInt("FROM", D3Service.OPEN_FROM_ADD_PIN_AND_HISTORY_STATUS_BAR);
                    } else if (from == D3Service.OPEN_FROM_RESET_PIN) {
                        bundle.putInt("FROM", D3Service.OPEN_FROM_RESET_PIN);
                    } else {
                        bundle.putInt("FROM", D3Service.OPEN_FROM_ADD_PIN);
                    }
                    NWizardPinEnterFragment fragment = new NWizardPinEnterFragment();
                    fragment.setArguments(bundle);
                    activity.addFragment(fragment, true);
                } else {
                    Func.pushToast(activity, getString(R.string.SFCF_ERROR_DUPLICATE), (WizardPinActivity) activity);
                    pin = 0;
                    enteredCount = 0;
                }
            });
        }
        if (null != logout) {
            logout.setOnButtonClickListener(action -> logOut());
        }
        if (null != keypad) {
            keypad.setFPVisibility(View.GONE);
            keypad.setOnKeyClickListener(value -> {
                switch (value) {
                    case NKeyPadView.KEY_BS:
                        dotsView.decrease();
                        pin /= 10;
                        if (null != next) next.setDisable(true);
                        break;
                    case NKeyPadView.KEY_FP:
                        NDialog dialog = new NDialog(activity, R.layout.n_dialog_fingerprint);
                        dialog.setTitle(getResources().getString(R.string.FINGERPRINT_AUTH_TITLE));
                        dialog.setSubTitle(getResources().getString(R.string.FIGERPRINT_AUTH_MESS));
                        dialog.setOnActionClickListener((v, b) -> dialog.dismiss());
                        dialog.show();
                        break;
                    default:
                        if (dotsView.increase()) {
                            pin = pin * 10 + value;
                        }
                        if (dotsView.getCount() == dotsView.getTotalCount()) {
                            if (null != next) next.setDisable(false);
                        }
                        break;
                }
            });
        }
    }

    private void setSavedPin() {
        pin = savedPin;
        dotsView.setCount(4);
    }

    private void showBackDialog() {

        NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_vertical_small);
        nDialog.setTitle(getResources().getString(R.string.SPSA_PIN_ENTER_DIALOG_MESS));
        nDialog.setPositiveButton(getResources().getString(R.string.N_BUTTON_CONTINUE), () -> {
            nDialog.dismiss();
            back();
        });
        nDialog.setNegativeButton(getResources().getString(R.string.N_BUTTON_CANCEL), nDialog::dismiss);

        nDialog.show();
    }

    private void onBackPressed() {
        if (from == D3Service.OPEN_FROM_LOGIN) {
            logOut();
        } else
            showBackDialog();
    }

    private void back() {
        if (from == D3Service.OPEN_FROM_RESET_PIN)
            activity.finish();
        else
            activity.finishAffinity();
    }

    public void logOut() {
        NDialog dialog = new NDialog(activity, R.layout.n_dialog_question_logout);
        dialog.setOptionCheck(true);
        dialog.setTitle(getResources().getString(R.string.PA_EXIT_DIALOG_TITLE_1) + " " + getResources().getString(R.string.application_name) + " " + getResources().getString(R.string.PA_EXIT_DIALOG_TITLE_2));
        dialog.setOnActionClickListener((value, b) -> {
            switch (value) {
                case NActionButton.VALUE_OK:
                    dialog.dismiss();
                    Intent manualLogoutIntent  = new Intent(D3Service.BROADCAST_CONNECTION_DROP);
                    manualLogoutIntent.putExtra("AppName", context.getPackageName());
                    manualLogoutIntent.putExtra("base", true);
                    manualLogoutIntent.putExtra("saveData", !b);
                    manualLogoutIntent.putExtra("from", "manual logout");
                    context.sendBroadcast(manualLogoutIntent);
                    break;
                case NActionButton.VALUE_CANCEL:
                    dialog.dismiss();
                    break;
            }
        });
        dialog.show();
    }
}
