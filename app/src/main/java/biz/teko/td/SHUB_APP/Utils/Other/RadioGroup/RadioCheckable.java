package biz.teko.td.SHUB_APP.Utils.Other.RadioGroup;

import android.view.View;

public interface RadioCheckable extends Cheсkable
{
	void addOnCheckChangeListener(OnCheckedChangeListener onCheckedChangeListener);
	void removeOnCheckChangeListener(OnCheckedChangeListener onCheckedChangeListener);

	public static interface OnCheckedChangeListener {
		void onCheckedChanged(View radioGroup, boolean isChecked);
	}
}
