package biz.teko.td.SHUB_APP.Relays_N.Activities;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class RelaySettingsActivity extends BaseActivity
{
	private LinearLayout buttonBack;
	private NMenuListElement nNameView;
	private NMenuListElement nDeleteView;
	private int zone_id;
	private int section_id;
	private int device_id;
	private Context context;
	private DBHelper dbHelper;
	private Zone relay;
	private BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			bindView();
		}
	};
	private boolean bound;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private int sending = 0;
	private UserInfo userInfo;

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_relay_settings);
		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();

		zone_id = getIntent().getIntExtra("zone", -1);
		section_id = getIntent().getIntExtra("section",-1);
		device_id = getIntent().getIntExtra("device", -1);


		setupView();
		bindView();

	}

	private void bindView()
	{
		relay = dbHelper.getZoneById(device_id, section_id, zone_id);

		if(null!=buttonBack)buttonBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});
		if(null!=relay)
		{
			if (null != nNameView)
			{
				nNameView.setTitle(relay.name);
				if(maySet()){
					nNameView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NRenameActivity.class);
							intent.putExtra("type", Const.ZONE);
							intent.putExtra("device", relay.device_id);
							intent.putExtra("section", relay.section_id);
							intent.putExtra("zone", relay.id);
							intent.putExtra("name", relay.name);
							intent.putExtra("transition",true);
							startActivity(intent, getTransitionOptions(nNameView).toBundle());
						}
					});
				}else{
					nNameView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Func.nShowMessage(context, getString(R.string.N_NO_PERMISSION));
						}
					});
				}
			}
			if (null != nDeleteView) {
				if(maySystemSet()){
					nDeleteView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NDeleteActivity.class);
							intent.putExtra("type", Const.ZONE);
							intent.putExtra("device", relay.device_id);
							intent.putExtra("section", relay.section_id);
							intent.putExtra("zone", relay.id);
							intent.putExtra("name", relay.name);
							intent.putExtra("transition",true);
							startActivityForResult(intent, Const.REQUEST_DELETE, getTransitionOptions(nDeleteView).toBundle());
						}
					});
				}else{
					nDeleteView.setVisibility(View.GONE);
				}
			}
		}
	}

	private boolean maySet()
	{
		return Func.nGetRemoteSystemSetRights(userInfo);
	}

	private boolean maySystemSet()
	{
		return Func.nGetRemoteDeviceSetRights(userInfo, dbHelper.getDeviceById(device_id));
	}


	private void setupView()
	{
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		nNameView = (NMenuListElement) findViewById(R.id.nMenuName);
		nDeleteView = (NMenuListElement) findViewById(R.id.nMenuDelete);
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((RelaySettingsActivity)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	public void  onResume(){
		super.onResume();
		bindView();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
//		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
//		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		registerReceiver(updateReceiver, intentFilter);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		bindD3();
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(updateReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
}
