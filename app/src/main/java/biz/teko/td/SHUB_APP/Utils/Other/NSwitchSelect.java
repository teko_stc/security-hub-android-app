package biz.teko.td.SHUB_APP.Utils.Other;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Switch;

public class NSwitchSelect extends Switch
{
	public NSwitchSelect(Context context)
	{
		super(context);
	}

	public NSwitchSelect(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public NSwitchSelect(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	public NSwitchSelect(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	public void setSelected(boolean selected)
	{
//		super.setSelected(selected);
		setChecked(selected);
	}
}
