package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Utils.Dir.Const;

/**
 * Created by td13017 on 16.02.2017.
 */

public class Operator
{
	public int id, active, domain, org, roles;
	public String name, login, contacts;

	public Operator() {}

	public Operator(int id, String name){
		this.id = id;
		this.name = name;
	}

	public Operator(int id, String name, String login, String contacts, int active, int domain, int org, int roles){
		this.id = id;
		this.name = name;
		this.login = login;
		this.contacts = contacts;
		this.active = active;
		this.domain = domain;
		this.org = org;
		this.roles = roles;
	}

	public Operator(Cursor c){
		this.id = c.getInt(1);
		this.name = c.getString(2);
		this.login = c.getString(3);
		this.contacts = c.getString(4);
		this.active = c.getInt(5);
		this.domain = c.getInt(6);
		this.org = c.getInt(7);
		this.roles = c.getInt(8);
	}

	public Operator(JSONObject operatorObject)
	{
		if(null!=operatorObject)
		try
		{
			this.id = operatorObject.getInt("id");
			this.name = operatorObject.getString("name");
			if(operatorObject.has("login"))
			{
				this.login = operatorObject.getString("login");
			}
			this.contacts = operatorObject.getString("contacts");
			this.active = operatorObject.getInt("active");
			this.domain = operatorObject.getInt("domain");
			this.org = operatorObject.getInt("org");
			this.roles = operatorObject.getInt("roles");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public int getRole(){
		int role = 0;
		if(((this.roles)& Const.Roles.DOMAIN_OPER)>0){
			role = 3;
			if(((this.roles)&Const.Roles.DOMAIN_HOZ_ORG)>0){
				role = 2;
			}
		} else if(((this.roles)&Const.Roles.DOMAIN_HOZ_ORG)>0){
			role = 3;
		}
		if((this.roles & Const.Roles.DOMAIN_ADMIN) > 0){
			role = 1;
		}
		return role;
	}

//	public String getRole(){
//		String role = App.getContext().getString(R.string.OPERATOR_UNKNOWN);
//		if(((this.roles)& Const.Roles.DOMAIN_OPER)>0){
//			role = rolesString[2];
//			if(((this.roles)&Const.Roles.DOMAIN_HOZ_ORG)>0){
//				role = rolesString[1];
//			}
//		} else if(((this.roles)&Const.Roles.DOMAIN_HOZ_ORG)>0){
//			role = rolesString[2];
//		}
//		if((this.roles & Const.Roles.DOMAIN_ADMIN) > 0){
//			role = rolesString[0];
//		}
//		return role;
//	}

	public int getRoleInt() {
		if (((this.roles) & 8192) > 0) {
			if (((this.roles) & 32768) > 0) {
				if (((this.roles) & 16384) > 0){
					return 0;
				} else {
					return 1;
				}
			} else {
				return 2;
			}
		} else {
			return 3;
		}
	}
}