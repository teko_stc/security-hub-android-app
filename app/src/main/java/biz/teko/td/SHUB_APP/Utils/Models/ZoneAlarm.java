package biz.teko.td.SHUB_APP.Utils.Models;

import android.content.Context;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.Icon;
import biz.teko.td.SHUB_APP.R;

public class ZoneAlarm
{
	public int id, detector;
	public String caption, icon, sectionmask;
	public LinkedList<Icon> icons;


	public ZoneAlarm (int id, int detector, String caption, String icon, String sectionmask){
		this.id = id;
		this.detector = detector;
		this.caption = caption;
		this.icon = icon;
		this.sectionmask = sectionmask;
	}


	public ZoneAlarm(int id, int detector, String caption, String icon, String sectionmask, LinkedList<Icon> icons)
	{
		this.id = id;
		this.detector = detector;
		this.caption = caption;
		this.icon = icon;
		this.sectionmask = sectionmask;
		this.icons = icons;
	}

	public int getListIcon(Context context)
	{
		try
		{
			return context.getResources().getIdentifier(this.icons.get(0).src, null, context.getPackageName());
		}catch (Exception e ){
			return R.drawable.n_image_class_alarm_common_list_selector;
		}
	}

	public int getMainIcon(Context context)
	{
		try
		{
			return context.getResources().getIdentifier(this.icons.get(1).src, null, context.getPackageName());
		}catch (Exception e){
			return R.drawable.n_image_class_alarm_common_list_selector;
		}
	}

	public int getBigIcon(Context context)
	{
		try
		{
			return context.getResources().getIdentifier(this.icons.get(2).src, null, context.getPackageName());
		}catch (Exception e){
			return R.drawable.n_image_class_alarm_common_list_selector;
		}
	}
}
