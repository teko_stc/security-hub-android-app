package biz.teko.td.SHUB_APP.Wizards.Activity.Controller;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Other.DEPager;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.ImagesViewPagerAdapter;

public class WizardControllerBindFinishActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private int site_id, device_id;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private ImagesViewPagerAdapter adapter;
	private DEPager pager;
	private View indicator1, indicator2, indicator3, indicator4;
	private ImageView buttonRight, buttonLeft;
	private String deviceSerial, deviceModel;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_controller_bind_finish);

		context = this;
		dbHelper = DBHelper.getInstance(context);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		site_id = getIntent().getIntExtra("site_id", -1);
		device_id = getIntent().getIntExtra("device_id", -1);
		Device device = dbHelper.getDeviceById(device_id);
		if(null!=device){
			deviceSerial = Integer.toString(device.account);
			deviceModel = device.getName();
		}

		if(-1 != site_id){
			toolbar.setTitle(dbHelper.getSiteNameById(site_id));
		}
		toolbar.setSubtitle(R.string.WIZ_CONTR_BIND_TITLE);
		toolbar.setSubtitleTextColor(getResources().getColor(R.color.brandColorWhite));
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		serviceConnection = new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};


		adapter = new ImagesViewPagerAdapter(((WizardControllerBindFinishActivity) context).getSupportFragmentManager());
		pager = (DEPager) findViewById(R.id.imagesViewPager);
		pager.setPagingEnabled(false);
		pager.setAdapter(adapter);

		indicator1 = (View) findViewById(R.id.indicator1);
		indicator2 = (View) findViewById(R.id.indicator2);
		indicator3 = (View) findViewById(R.id.indicator3);
		indicator4 = (View) findViewById(R.id.indicator4);
		updateIndicators(0);

		buttonLeft = (ImageView) findViewById(R.id.imagesButtonLeft);
		buttonLeft.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				goLeft();
			}

			private void goLeft()
			{
				if(pager.getCurrentItem() != 0){
					int position = pager.getCurrentItem();
					pager.setCurrentItem(position - 1);
					updateIndicators(position - 1);
				}
			}
		});
		buttonRight = (ImageView) findViewById(R.id.imagesButtonRight);
		buttonRight.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				goRight();
			}

			private void goRight()
			{
				if(pager.getCurrentItem() != 4)
				{
					int position = pager.getCurrentItem();
					pager.setCurrentItem(position + 1);
					updateIndicators(position + 1);
				}
			}
		});

		final Button contrSetupEndButton = (Button) findViewById(R.id.wizContrSetupEndButton);
		contrSetupEndButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	private void updateIndicators(int i)
	{
		if((null!=indicator1)&&(null!=indicator2)&&(null!=indicator3)&&(null!=indicator4)&&(null!=buttonLeft)&&(null!=buttonRight)){
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			int resizeValue = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 25, metrics);
			int defaultValue = (int) TypedValue.applyDimension(
					TypedValue.COMPLEX_UNIT_DIP, 15, metrics);
			switch (i) {
				case 0:
					indicator1.getLayoutParams().height = resizeValue;
					indicator1.getLayoutParams().width = resizeValue;
					indicator1.requestLayout();

					indicator2.getLayoutParams().height = defaultValue;
					indicator2.getLayoutParams().width = defaultValue;
					indicator2.requestLayout();

					indicator3.getLayoutParams().height = defaultValue;
					indicator3.getLayoutParams().width = defaultValue;
					indicator3.requestLayout();

					indicator4.getLayoutParams().height = defaultValue;
					indicator4.getLayoutParams().width = defaultValue;
					indicator4.requestLayout();

					buttonLeft.setVisibility(View.INVISIBLE);

					break;

				case 1:
					indicator1.getLayoutParams().height = defaultValue;
					indicator1.getLayoutParams().width = defaultValue;
					indicator1.requestLayout();

					indicator2.getLayoutParams().height = resizeValue;
					indicator2.getLayoutParams().width = resizeValue;
					indicator2.requestLayout();

					indicator3.getLayoutParams().height = defaultValue;
					indicator3.getLayoutParams().width = defaultValue;
					indicator3.requestLayout();

					indicator4.getLayoutParams().height = defaultValue;
					indicator4.getLayoutParams().width = defaultValue;
					indicator4.requestLayout();

					buttonLeft.setVisibility(View.VISIBLE);

					break;

				case 2:
					indicator1.getLayoutParams().height = defaultValue;
					indicator1.getLayoutParams().width = defaultValue;
					indicator1.requestLayout();

					indicator2.getLayoutParams().height = defaultValue;
					indicator2.getLayoutParams().width = defaultValue;
					indicator2.requestLayout();

					indicator3.getLayoutParams().height = resizeValue;
					indicator3.getLayoutParams().width = resizeValue;
					indicator3.requestLayout();

					indicator4.getLayoutParams().height = defaultValue;
					indicator4.getLayoutParams().width = defaultValue;
					indicator4.requestLayout();

					buttonRight.setVisibility(View.VISIBLE);

					break;

				case 3:
					indicator1.getLayoutParams().height = defaultValue;
					indicator1.getLayoutParams().width = defaultValue;
					indicator1.requestLayout();

					indicator2.getLayoutParams().height = defaultValue;
					indicator2.getLayoutParams().width = defaultValue;
					indicator2.requestLayout();

					indicator3.getLayoutParams().height = defaultValue;
					indicator3.getLayoutParams().width = defaultValue;
					indicator3.requestLayout();

					indicator4.getLayoutParams().height = resizeValue;
					indicator4.getLayoutParams().width = resizeValue;
					indicator4.requestLayout();

					buttonRight.setVisibility(View.INVISIBLE);

					break;
			}
		}
	}
}
