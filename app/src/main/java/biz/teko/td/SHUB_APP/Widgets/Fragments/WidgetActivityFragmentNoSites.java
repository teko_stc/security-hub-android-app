package biz.teko.td.SHUB_APP.Widgets.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 24.05.2017.
 */

public class WidgetActivityFragmentNoSites extends Fragment
{
	Context context;
	DBHelper dbHelper;
	View view;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);

		view = inflater.inflate(R.layout.widget_fragment_no_sites, container, false);
		TextView centralText = (TextView) view.findViewById(R.id.widget_central_text);

		if(dbHelper.getUserInfo() == null){
			centralText.setText(R.string.ERROR_WIDGET_NOT_AUTHORIZE);
		}else{
			centralText.setText(R.string.ERROR_WIDGET_NO_CONNECTED_OBJECTS);
		}

		return view;
	}
}
