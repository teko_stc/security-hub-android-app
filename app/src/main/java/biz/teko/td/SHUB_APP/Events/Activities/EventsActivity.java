/*
package biz.teko.td.SHUB_APP.Events.Activities;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Adapter.HistoryClassesSpinnerAdapter;
import biz.teko.td.SHUB_APP.Adapter.HistorySitesSpinnerAdapter;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NEAListElement;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class EventsActivity extends BaseActivity
{
	private boolean bound;
	private D3Service myService;
	private ServiceConnection serviceConnection;


	private Context context;
	private DBHelper dbHelper;
	private PrefUtils prefUtils;
	private UserInfo userInfo;
	private FrameLayout filterFrame;
	private FrameLayout filterSetFrame;
	private FrameLayout filterLayout;


	private boolean visible;
	private LinearLayout buttonFilter;
	private int initHeight;
	private int targetHeight;
	private ListView eventList;
	private NEventsListAdapter eventAdapter;
	private ImageView buttonImageFilter;
	private Filter filter;
	private NEAListElement filterSite;
	private NEAListElement filterClass;
	private NEAListElement filterTo;
	private NEAListElement filterFrom;
	private DatePickerDialog fromDateDialog;
	private DatePickerDialog toDateDialog;
	private boolean animationEnd;

	private class Filter{
		public int site;
		public int eClass;
		public long from;
		public long to;

		public Filter(){
			this.site = 0;
			this.eClass = -1;
			this.from = Func.getStartOfDay(System.currentTimeMillis() - 3600*24*1000);
			to  = Func.getEndOfDay(System.currentTimeMillis());
		}
	}

	private void updateBottomHelper(){
		setBottomInfoBehaviour(context);
	}

	private BroadcastReceiver eventReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
			updateBottomHelper();
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getSwipeBackLayout().setEnableGesture(false);

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		this.prefUtils = PrefUtils.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();

		setContentView(R.layout.n_activity_events);
		setBottomNavigation(R.id.bottom_navigation_item_messages, context);

		serviceConnection = getServiceConnection();

		setup();
		bind();

	}

	private void setup()
	{
		filter = new Filter();

		filterLayout = (FrameLayout) findViewById(R.id.nEventsFilterLayout);
		filterFrame = (FrameLayout) findViewById(R.id.nEventsFilter);
		filterSetFrame = (FrameLayout) findViewById(R.id.nEventsFilterFrame);

		buttonFilter = (LinearLayout) findViewById(R.id.nButtonShow);
		buttonImageFilter = (ImageView) findViewById(R.id.nImageShow);
//		targetHeight = filterSetFrame.getLayoutParams().height;

		filterSite = (NEAListElement) findViewById(R.id.nSelectorEventFilterSite);
		filterClass = (NEAListElement) findViewById(R.id.nSelectorEventFilterClass);
		filterFrom = (NEAListElement) findViewById(R.id.nSelectorEventFilterFrom);
		filterTo = (NEAListElement) findViewById(R.id.nSelectorEventFilterTo);

		eventList = (ListView) findViewById(R.id.nListEvents);
	}

	private void bind()
	{
		if(null!=buttonFilter)buttonFilter.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				resizeFilter();
			}
		});

		if(null!=filterFrame)filterFrame.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				resizeFilter();
			}
		});

		if(null!=filterFrom)
		{
			filterFrom.setTitle(Func.getDate(context, filter.from, "dd.MM.yyyy"));
			Calendar oldCalendar = Calendar.getInstance();
			oldCalendar.setTimeInMillis(filter.from);
			fromDateDialog = new DatePickerDialog(this, R.style.datepicker, new DatePickerDialog.OnDateSetListener()
			{
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
				{
					Calendar newDate = Calendar.getInstance();
					newDate.set(year, monthOfYear, dayOfMonth);
					long oldTime = filter.from;
					filter.from = Func.getStartOfDay(newDate.getTimeInMillis());
					long dbTime = dbHelper.getEventMinTime()*1000;
					if((oldTime > filter.from)&&( dbTime > filter.from)){
						getDataFromServer();
					}
					filterFrom.setTitle(Func.getDate(context, filter.from, "dd.MM.yyyy"));
					updateList();
				}
			}, oldCalendar.get(Calendar.YEAR), oldCalendar.get(Calendar.MONTH), oldCalendar.get(Calendar.DAY_OF_MONTH));
			filterFrom.setOnElementClickListener(new NEAListElement.OnElementClickListener()
			{
				@Override
				public void onClick()
				{
					fromDateDialog.show();
				}
			});
		}

		if(null!=filterTo)
		{
			filterTo.setTitle(Func.getDate(context, filter.to, "dd.MM.yyyy"));
			Calendar newCalendar = Calendar.getInstance();
			toDateDialog = new DatePickerDialog(this, R.style.datepicker, new DatePickerDialog.OnDateSetListener()
			{
				@Override
				public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
				{
					Calendar newDate = Calendar.getInstance();
					newDate.set(year, monthOfYear, dayOfMonth);
					filter.to = Func.getEndOfDay(newDate.getTimeInMillis());
					filterTo.setTitle(Func.getDate(context, filter.to, "dd.MM.yyyy"));
					updateList();
				}
			}, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
			filterTo.setOnElementClickListener(new NEAListElement.OnElementClickListener()
			{
				@Override
				public void onClick()
				{
					toDateDialog.show();
				}
			});
		}



		if(null!=filterClass){
			filterClass.setOnElementClickListener(new NEAListElement.OnElementClickListener()
			{
				@Override
				public void onClick()
				{
					showBottomClassSelectDialog();

				}
			});
		}

		if(null!=filterSite){
			filterSite.setOnElementClickListener(new NEAListElement.OnElementClickListener()
			{
				@Override
				public void onClick()
				{
					showBottomSiteSelectDialog();
				}
			});
		}

		updateList();
	}

	private void resizeFilter()
	{
		if(!visible){

			resizeWithAnimation(filterLayout, 500, 1);
			visible = true;
			if(null!=buttonImageFilter) buttonImageFilter.setImageDrawable(getResources().getDrawable(R.drawable.ic_stair_up_inverted));
		}else{
			resizeWithAnimation(filterLayout, 500, 0);
			visible = false;
			if(null!=buttonImageFilter) buttonImageFilter.setImageDrawable(getResources().getDrawable(R.drawable.ic_stair_down_inverted));
		}
	}

	private void updateList()
	{
		Cursor cursor = updateCursor();
		if(null!=cursor && null!=eventAdapter)
		{
			eventAdapter.update(cursor);
		}else{
			eventAdapter = new NEventsListAdapter(context, R.layout.n_events_list_element, cursor, 0, true);;
			if(null!=eventList) eventList.setAdapter(eventAdapter);
		}
		if (null!=eventAdapter && 0!= eventAdapter.getCount())
		{
			eventList.setVisibility(View.VISIBLE);
		} else
		{
			eventList.setVisibility(View.GONE);
		}

	}

	private Cursor updateCursor()
	{
		return  dbHelper.getEventCursorForHistory(filter.site, filter.eClass, filter.from, filter.to);
	}
	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}


	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);

		registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));

		NotificationManager notifManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.cancelAll();

		updateBottomHelper();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(eventReceiver);
	}



	private void showBottomSiteSelectDialog()
	{
		final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(context);

		View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
		TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.N_EVENTS_CHOOSE_SITE);
		ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);

		Site[] sites = dbHelper.getAllSitesArray();
		Site[] sitesArray;
		if(null!=sites){
			sitesArray = new Site[sites.length +1];
			int i1 = 1;
			for (Site site:sites){
				sitesArray[i1] = site;
				i1++;
			}
		}else{
			sitesArray = new Site[1];
		}
		sitesArray[0] = new Site(0, getString(R.string.ALL));


		if(null!=sitesArray) {
			final HistorySitesSpinnerAdapter historySitesAdapter = new HistorySitesSpinnerAdapter(this,  R.layout.spinner_item_dropdown, sitesArray, filter.site);
			listView.setAdapter(historySitesAdapter);
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					Site site = (Site) parent.getItemAtPosition(position);
					bottomSheetDialog.dismiss();
					filter.site = site.id;
					updateList();
					filterSite.setTitle(site.name);
				}
			});
		}

		bottomSheetDialog.setContentView(bottomView);

		bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener()
		{
			@Override
			public void onShow(DialogInterface dialog)
			{
				FrameLayout bottomSheet = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

				if (null != bottomSheet)
				{
					BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
					behavior.setHideable(false);
				}
			}
		});

		try {
			Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
			mBehaviorField.setAccessible(true);

			final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
			behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
				@Override
				public void onStateChanged(@NonNull View bottomSheet, int newState) {
					if (newState == BottomSheetBehavior.STATE_DRAGGING) {
						behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
					}
				}

				@Override
				public void onSlide(@NonNull View bottomSheet, float slideOffset) {
				}
			});
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		bottomSheetDialog.show();
	}

	private void showBottomClassSelectDialog()
	{
		final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(context);

		View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
		TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.N_EVENTS_CHOOSE_CLASS);
		ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
		LinkedList<EClass> eClassesList = D3Service.d3XProtoConstEvent.classes;
		LinkedList<EClass> eClasses = new LinkedList<>();

		eClasses.add(new EClass(-1, getString(R.string.N_EVENTS_ALL_CLASSES)));
		for (EClass eClass : eClassesList)
		{
			if(eClass.id != Const.CLASS_CONNECTIVITY
					&& eClass.id != Const.CLASS_SUPPLY
					&& eClass.id != Const.CLASS_INNER
					&& eClass.id != Const.CLASS_DEBUG
			)
			{
				eClasses.add(eClass);
			}
		}
		EClass[] eClassesArray = eClasses.toArray(new EClass[eClasses.size()]);
		if(null!=eClassesArray) {
			final HistoryClassesSpinnerAdapter historyClassesAdapter = new HistoryClassesSpinnerAdapter(this, R.layout.n_card_for_list, eClassesArray, filter.eClass);
			listView.setAdapter(historyClassesAdapter);
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					EClass eClass = (EClass) parent.getItemAtPosition(position);
					bottomSheetDialog.dismiss();
					filter.eClass = eClass.id;
					filterClass.setTitle(eClass.caption);
					updateList();
				}
			});
		}

		bottomSheetDialog.setContentView(bottomView);

		bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener()
		{
			@Override
			public void onShow(DialogInterface dialog)
			{
				FrameLayout bottomSheet = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

				if (null != bottomSheet)
				{
					BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
					behavior.setHideable(false);
				}
			}
		});

		try {
			Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
			mBehaviorField.setAccessible(true);

			final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
			behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
				@Override
				public void onStateChanged(@NonNull View bottomSheet, int newState) {
					if (newState == BottomSheetBehavior.STATE_DRAGGING) {
						behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
					}
				}

				@Override
				public void onSlide(@NonNull View bottomSheet, float slideOffset) {
				}
			});
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}

		bottomSheetDialog.show();
	}



	private void resizeWithAnimation(final View view, int duration, int type) {
		int initHeight =0;
		int targetHeight = 0;

		int availableWidth = getParentWidth(filterSetFrame);

		int widthSpec = View.MeasureSpec.makeMeasureSpec(availableWidth, View.MeasureSpec.AT_MOST);
		int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

		filterSetFrame.measure(widthSpec, heightSpec);
		int measuredHeight = filterSetFrame.getMeasuredHeight();

		switch (type){
			case 0:
				filterSetFrame.setVisibility(View.GONE);
				initHeight = measuredHeight;
				targetHeight = filterFrame.getHeight();
				break;
			case 1:
				filterSetFrame.setVisibility(View.VISIBLE);
				initHeight = filterFrame.getHeight();
				targetHeight = measuredHeight;
				break;
		}

		int finalTargetHeight = targetHeight;
		int finalInitHeight = initHeight;

		final int distance = targetHeight - initHeight;

		view.getLayoutParams().height = initHeight;
		view.setVisibility(View.VISIBLE);



		Animation animation = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if(!animationEnd)
				{
					if (interpolatedTime == 1 && finalTargetHeight == 0)
					{
						view.setVisibility(View.GONE);
					}
					view.getLayoutParams().height = (int) (finalInitHeight + distance * interpolatedTime);
					view.requestLayout();
					if(interpolatedTime == 1){
						animationEnd  = true;
						LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						view.setLayoutParams(layoutParams);
					}
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		animation.setAnimationListener(new Animation.AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{
				animationEnd = false;
			}

			@Override
			public void onAnimationEnd(Animation animation) {}

			@Override
			public void onAnimationRepeat(Animation animation) {}
		});

		animation.setDuration(duration);
		view.startAnimation(animation);
	}

	private int getParentWidth(View viewToScale)
	{
		final ViewParent parent = viewToScale.getParent();
		if (parent instanceof View) {
			final int parentWidth = ((View) parent).getWidth();
			if (parentWidth > 0) {
				return parentWidth;
			}
		}

		throw new IllegalStateException("View to scale must have parent with measured width");
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}


	private void getDataFromServer(){
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject message = new JSONObject();
		long tmax = dbHelper.getEventMinTime();
		try
		{
			message.put("tmin", filter.from/1000);
			//			message.put("tmax", timeTo/1000);
			message.put("tmax", tmax == 0?  System.currentTimeMillis()/1000 : tmax);

		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		D3Request d3Request = D3Request.createMessage(userInfo.roles, "EVENTS", message);
		if(d3Request.error == null){
			sendMessageToServer(d3Request);
		}else{
			Func.pushToast(context, d3Request.error, (HistoryActivity) context);
		}
	}

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if (null != myService)
		{
			if (myService.send(d3Request.toString()))
			{
				return true;
			}
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
		}
		return false;
	};

	@Override
	public void onBackPressed()
	{
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.N_LOGOUT_Q));
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_OK:
						finishAffinity();
						break;
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}

			}
		});
		nDialog.show();
	}
}
*/
