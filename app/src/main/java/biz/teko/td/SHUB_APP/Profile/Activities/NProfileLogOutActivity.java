package biz.teko.td.SHUB_APP.Profile.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;

public class NProfileLogOutActivity extends BaseActivity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_profile_logout);

		LinearLayout back = (LinearLayout) findViewById(R.id.nButtonBack);
		if(null!=back)back.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});

		NWithAButtonElement exit  = (NWithAButtonElement) findViewById(R.id.nButtonDelete);
		if(null!=exit){
			exit.setCheck(true);
			exit.setOnChildClickListener((action, option) -> {
				Func.log_d("D3BaseActivity", "LOGOUT");
				Intent manualLogoutIntent  = new Intent(D3Service.BROADCAST_CONNECTION_DROP);
				manualLogoutIntent.putExtra("AppName", getPackageName());
				manualLogoutIntent.putExtra("base", true);
				manualLogoutIntent.putExtra("saveData", !option);
				manualLogoutIntent.putExtra("from", "manual logout");
				sendBroadcast(manualLogoutIntent);
			});
		}

//		NActionButton exit = (NActionButton) findViewById(R.id.nImageAction);
//		if(null!=exit)exit.setOnButtonClickListener(new NActionButton.OnButtonClickListener()
//		{
//			@Override
//			public void onButtonClick(int action)
//			{
//				Func.log_d("D3BaseActivity", "LOGOUT");
//				Intent manualLogoutIntent  = new Intent(D3Service.BROADCAST_CONNECTION_DROP);
//				manualLogoutIntent.putExtra("AppName", getPackageName());
//				manualLogoutIntent.putExtra("base", true);
//				manualLogoutIntent.putExtra("saveData", b);
//				sendBroadcast(manualLogoutIntent);
//			}
//		});

	}
}
