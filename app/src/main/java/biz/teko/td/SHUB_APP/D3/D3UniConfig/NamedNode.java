package biz.teko.td.SHUB_APP.D3.D3UniConfig;

import org.simpleframework.xml.Attribute;

/**
 * Created by td13017 on 29.06.2016.
 */
public class NamedNode extends DescribedNode{
	@Attribute(name="name")
	public String Name;
}
