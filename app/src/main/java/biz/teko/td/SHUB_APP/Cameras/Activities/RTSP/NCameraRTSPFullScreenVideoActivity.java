package biz.teko.td.SHUB_APP.Cameras.Activities.RTSP;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import org.videolan.libvlc.MediaPlayer;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.VLC.VLCListener;
import biz.teko.td.SHUB_APP.Cameras.VLC.VLCModule;
import biz.teko.td.SHUB_APP.R;

public class NCameraRTSPFullScreenVideoActivity extends BaseActivity {
    private VLCModule vlcModule;
    private ProgressBar progress;
    private SurfaceView videoView;
    private FrameLayout videoViewParent;
    private MediaPlayer mediaPlayer;
    private String link;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_camera_rtsp_full);
        this.context = this;
        link = getIntent().getStringExtra("link");
        setup();
    }

    private void setup() {
        progress = (ProgressBar) findViewById(R.id.pb_search);
        videoView = (SurfaceView) findViewById(R.id.nVideoView);
        videoViewParent = (FrameLayout) findViewById(R.id.nVideoViewParent);
    }

    private void createVLCModule() {
        vlcModule = new VLCModule(videoView, videoViewParent, context, link);
        vlcModule.setFull(true);
        mediaPlayer = vlcModule.getMediaPlayer();
        vlcModule.setListener(new VLCModule.ISizeClickListener() {
            @Override
            public void fullscreen() {

            }

            @Override
            public void windowed() {
                finish();
            }
        });
    }

    private void bind() {
        showVideoLoading();
        createVLCModule();
        mediaPlayer.play();
        vlcModule.setEventListener(new VLCListener() {
            @Override
            public void onComplete() {
                showVideoFrame();
                resizeVideoView();
            }

            @Override
            public void onError() {
            }
        });
    }

    private void resizeVideoView() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        vlcModule.getVout().setWindowSize(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    private void showVideoLoading() {
        if (null != videoViewParent) {
            videoViewParent.setVisibility(View.INVISIBLE);
            videoViewParent.setAlpha(0);
        }
        if (null != progress) progress.setVisibility(View.VISIBLE);
    }

    private void showVideoFrame() {
        if (null != videoViewParent) {
            videoViewParent.setVisibility(View.VISIBLE);
            videoViewParent.setAlpha(1);
        }
        if (null != progress) progress.setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideSystemUi();
        bind();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (null != mediaPlayer) mediaPlayer.stop();
    }

    @Override
    public void onBackPressed() {
        if (null != mediaPlayer) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onBackPressed();
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUi() {
        videoView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LOW_PROFILE
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
        );
    }
}
