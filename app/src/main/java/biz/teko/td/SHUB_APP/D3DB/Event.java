package biz.teko.td.SHUB_APP.D3DB;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Pair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.MatchResult;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.D3XProtoConstEvent;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DPhysic;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EDetector;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EReason;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EStatement;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 15.06.2016.
 */
public class Event
{
	public static  final int TEMP_NO_VALUE = -999;
	public static final int NO_LINK = 0;
	public static final int LINK_GEO = 1;
	public static final int LINK_VIDEO = 2;
	public static final int LINK_VIDEO_FAILED = 3;

	private static final String EXPLANAITION_ERROR = "Ошибка расшифровки ивента";

	public static int[] icons = {R.drawable.ic_action_messages_white};
//	String regex = "(\\$([\\w]+))?\\?(\\w)\\[(\\d+),(\\d+)\\]";
	String regex = "\\?(\\w)\\[(\\d+),(\\d+)(,([\\w]+))?\\]";
	String regex1 = "<([^<]*)\\{([\\w\\%]+)(?>@([\\w]+))?\\}([^>]*)>";
	//	public static int[] sound = {R.};

	public int id;
	public int device;
	public int section;
	public int zone;
	public int active;
	public int classId;   //класс события
	public int detectorId;
	public int reasonId;
	public long time;
	public long localtime;
	public int channel;
	public int reviewStatus;
	public String data = "";
	public int affect;
	public String jdata = "";

	public boolean exist = true;
	public boolean notify = false;
	public boolean inc = true;

	public Event() {}

	public Event(int id, int device, int section, int zone, int active, int classId, int detectorId, int reasonId, long time, long localtime, int channel, int reviewStatus, String data, int affect, String jdata)
	{
		this.id = id;
		this.device = device;
		this.section = section;
		this.zone = zone;
		this.active = active;
		this.classId = classId;
		this.detectorId = detectorId;
		this.reasonId = reasonId;
		this.time = time;
		this.localtime = localtime;
		this.channel = channel;
		this.reviewStatus = reviewStatus;
		this.data = data;
		this.affect = affect;
		this.jdata = jdata;
	}

	public Event(Cursor cursor){
		this.id = cursor.getInt(1);
		this.device = cursor.getInt(2);
		this.section = cursor.getInt(3);
		this.zone = cursor.getInt(4);
		this.active = cursor.getInt(5);
		this.classId = cursor.getInt(6);
		this.detectorId = cursor.getInt(7);
		this.reasonId = cursor.getInt(8);
		this.time = cursor.getLong(9);
		this.localtime = cursor.getLong(10);
		this.channel = cursor.getInt(11);
		this.reviewStatus = cursor.getInt(12);
		this.data = cursor.getString(13);
		this.affect = cursor.getInt(15);
		this.jdata = cursor.getString(16);
	}

	/*event constructor for fast icon found by */

	public Event(JSONObject object) {
		this.id = getI(object, "id");
		this.device = getI(object, "device");
		this.section = getI(object, "section");
		this.zone = getI(object, "zone");
		this.active = getI(object, "active");
		this.classId = getI(object, "class");
		this.detectorId = getI(object, "detector");
		this.reasonId = getI(object, "reason");
		this.time = getL(object, "time");
		this.localtime = getL(object, "localtime");
		this.channel = getI(object, "channel");
	//			this.reviewStatus = object.getInt("review_state");
	//			this.data = object.getString("data");
		this.affect = getI(object, "affect");
		this.jdata = getS(object, "jdata");
	}

	public int getI(JSONObject jsonObject, String name){
		try
		{
			return jsonObject.getInt(name);
		} catch (JSONException e)
		{
			return -1;
		}
	}

	public long getL(JSONObject jsonObject, String name){
		try
		{
			return jsonObject.getLong(name);
		} catch (JSONException e)
		{
			return -1;
		}
	}


	public String getS(JSONObject jsonObject, String name){
		try
		{
			return jsonObject.getString(name);
		} catch (JSONException e)
		{
			return null;
		}
	}

	//D3XProtoEvent
	public String explainEventDescription(Context context){
		if(null!=D3Service.d3XProtoConstEvent)
		{
			D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
			EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
			if (null != eClass)
			{
				EDetector eDetector = eClass.getDetectorById(this.detectorId);
				if (null != eDetector)
				{
					EReason eReason = eDetector.getReasonById(this.reasonId);
					if (null != eReason)
					{
						EStatement eStatement = eReason.getStatementById(this.active);
						if (null != eStatement)
						{
							return eStatement.description;
						}

					}
//					else
//					{
//						EStatement eStatement = eDetector.getStatementById(this.active);
//						if (null != eStatement)
//						{
//							return eStatement.description;
//						}
//					}

				}
				EReason eReason = eClass.getReasonById(this.reasonId);
				if (null != eReason)
				{
					EStatement eStatement = eReason.getStatementById(this.active);
					if (null != eStatement)
					{
						return eStatement.description;
					}
				}
				EStatement eStatement = eClass.getStatementById(this.active);
				if (null != eStatement)
				{
					return eStatement.description;
				}
				return  eClass.description;
			}
			return context.getString(R.string.UNKNOWN_EVENT_CLASS);

		}
		return Integer.toString(this.classId) + " + " + Integer.toString(this.detectorId) + " + " + Integer.toString(this.reasonId);
	}

	public Pair<String, String> explainEventDescriptionWithClass(Context context){
		String c = context.getString(R.string.UNKNOWN_EVENT_CLASS);
		String d = this.classId + " + " + this.detectorId + " + " + this.reasonId;
		if(null!=D3Service.d3XProtoConstEvent)
		{
			D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
			EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
			if (null != eClass)
			{
				c = eClass.description;
				EDetector eDetector = eClass.getDetectorById(this.detectorId);
				if (null != eDetector)
				{
					EReason eReason = eDetector.getReasonById(this.reasonId);
					if (null != eReason)
					{
						EStatement eStatement = eReason.getStatementById(this.active);
						if (null != eStatement)
						{
							return new Pair<>(c, eStatement.description);
						}

					}
				}
				EReason eReason = eClass.getReasonById(this.reasonId);
				if (null != eReason)
				{
					EStatement eStatement = eReason.getStatementById(this.active);
					if (null != eStatement)
					{
						return  new Pair<>(c, eStatement.description);
					}
				}
				EStatement eStatement = eClass.getStatementById(this.active);
				if (null != eStatement)
				{
					return  new Pair<>(c, eStatement.description);
				}
			}
		}
		return new Pair<>(c, d);
	}

	/*25.06.2018*/
	public String explainAffectDescription(Context context){
		if(null!=D3Service.d3XProtoConstEvent)
		{
			D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
			EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
			if (null != eClass)
			{
				EDetector eDetector = eClass.getDetectorById(this.detectorId);
				if (null != eDetector)
				{
					EReason eReason = eDetector.getReasonById(this.reasonId);
					if (null != eReason)
					{
						return eReason.affect;
					}

				}
				EReason eReason = eClass.getReasonById(this.reasonId);
				if(null!=eReason)
				{
					return eReason.affect;
				}
				return eClass.caption + context.getString(R.string.EVENT_UNKNOWN_REASON);

			}else{
				return context.getString(R.string.AFFECT_UNKNOWN);
			}
		}
		return Integer.toString(this.classId) + " + " + Integer.toString(this.detectorId) + " + " + Integer.toString(this.reasonId);
	}

	/*25.06.2018 */
	public String explainAffectRegDescription(Context context)
	{
		final DBHelper dbHelper = DBHelper.getInstance(context);
		String description = explainAffectDescription(context);
		final String jdata = this.jdata;
		final int reason = this.reasonId;
		final int device = this.device;

		final CallbackMatcher callbackMatcher = new CallbackMatcher(regex1);
		description = callbackMatcher.replaceMatches(description, matchResult -> {
			String operator = matchResult.group(2);
			try
			{
				String dataFromJData = "";
				switch (operator)
				{
					case "user_id":
						dataFromJData = dbHelper.getUserNameById(device, reason);
						break;
					case "charging":
						dataFromJData = new JSONObject(jdata).getString(operator);
						dataFromJData = Func.getBatteryStatus(context, dataFromJData);
						break;
					case "operator_id":
						dataFromJData = new JSONObject(jdata).getString(operator);
						dataFromJData = dbHelper.getOperatorNameByEventData(dataFromJData);
						break;
					case "command_id":
						dataFromJData = new JSONObject(jdata).getString(operator);
						String[] commands = App.getContext().getResources().getStringArray(R.array.command_types);
						int command_id = Integer.valueOf(dataFromJData);
						Command command = dbHelper.getCommandById(command_id);
						dataFromJData+=" " + commands[command.explainCommand()];
						break;
					case "status":
						dataFromJData = new JSONObject(jdata).getString(operator);
						int result = Integer.valueOf(dataFromJData);
						String[] results = App.getContext().getResources().getStringArray(R.array.command_results);
						if( result <= 0)
						{
							result=-result;
							results = App.getContext().getResources().getStringArray(R.array.command_results_negative);
						}
						dataFromJData = result <= results.length ? (result!=0? results[result - 1] : results[result]) : (App.getContext().getResources().getString(R.string.UNKNOWN_COMAND_FAIL_REASON)) ;
						break;
					case "physic":
						dataFromJData = new JSONObject(jdata).getString(operator);
						if(null!=D3Service.d3XProtoConstEvent){
							DPhysic dPhysic = D3Service.d3XProtoConstEvent.getPhysicById(Integer.valueOf(dataFromJData));
							if(null!=dPhysic){
								dataFromJData = dPhysic.caption;
							}
						}
					default:
						dataFromJData = new JSONObject(jdata).getString(operator);
						break;
				}
				return matchResult.group(1) + dataFromJData + matchResult.group(4);
			} catch (JSONException e)
			{
				e.printStackTrace();
				return "";
			}
		});

		return description.equals("")?context.getString(R.string.NO_DATA) : description;
	}

	public String explainEventRegDescription(Context context){
		final DBHelper dbHelper = DBHelper.getInstance(context);
		String description = explainEventDescription(context);
		final String jdata = this.jdata;
		final int reason = this.reasonId;
		final int device = this.device;
		final CallbackMatcher.Callback callback = matchResult -> {
			String operator = matchResult.group(2);
			try
			{
				String dataFromJData = ";";
				switch (operator)
				{
					case "user_id":
						if((D3Service.getRole()&Const.Roles.TRINKET)!=0){
							return "";
						}
						dataFromJData = dbHelper.getUserNameById(device, reason);
						break;
					case "user":
						int user_id = new JSONObject(jdata).getInt(operator);
						dataFromJData = user_id + " - " + dbHelper.getUserNameById(device, user_id);
						break;
					case "charging":
						dataFromJData = new JSONObject(jdata).getString(operator);
						dataFromJData = Func.getBatteryStatus(context, dataFromJData);
						break;
					case "operator_id":
						dataFromJData = new JSONObject(jdata).getString(operator);
						if(((D3Service.getRole()&Const.Roles.TRINKET)!=0) && (D3Service.getUserId() != Integer.valueOf(dataFromJData))){
							dataFromJData = "";
							break;
						}
						dataFromJData = dbHelper.getOperatorNameByEventData(dataFromJData);
						break;
					case "command_id":
						dataFromJData = new JSONObject(jdata).getString(operator);
						String[] commands = context.getResources().getStringArray(R.array.command_types);
						int command_id = Integer.valueOf(dataFromJData);
						Command command = dbHelper.getCommandById(command_id);
						if(null!=command)
						{
							dataFromJData += " " + commands[command.explainCommand()];
						}
						break;
					case "status":
						dataFromJData = new JSONObject(jdata).getString(operator);
						try
						{
							int result = Integer.valueOf(dataFromJData);
							String[] results = context.getResources().getStringArray(R.array.command_results);
							if( result <= 0)
							{
								result=-result;
								results = context.getResources().getStringArray(R.array.command_results_negative);
							}
							dataFromJData = result <= results.length ?
									(result!=0? results[result - 1] : results[result])
									: (App.getContext().getResources().getString(R.string.UNKNOWN_COMAND_FAIL_REASON)) ;
						}catch (NumberFormatException e){
							e.printStackTrace();
						}
						break;
					case "physic":
						dataFromJData = new JSONObject(jdata).getString(operator);
						if(null!=D3Service.d3XProtoConstEvent){
							DPhysic dPhysic = D3Service.d3XProtoConstEvent.getPhysicById(Integer.valueOf(dataFromJData));
							if(null!=dPhysic){
								dataFromJData = dPhysic.caption;
							}
						}
						break;
					case "reason_id":
						if((D3Service.getRole()&Const.Roles.TRINKET)!=0){
							return "";
						}
						dataFromJData = Integer.toString(reason);
						break;
					case "reboot_reason":
						int rebootReason = new JSONObject(jdata).getInt("reason");
						String[] reasons = context.getResources().getStringArray(R.array.reboot_reasons);
						dataFromJData = rebootReason <=reasons.length ? (reasons[rebootReason]): (reasons[reasons.length]);
						break;
					case "sections":
						JSONArray uSecArray = new JSONObject(jdata).getJSONArray("sections");
						String[] uSec = new String[uSecArray.length()];
						for(int si = 0; si < uSecArray.length(); si++){
							uSec[si] = uSecArray.getString(si);
						}
						dataFromJData = Arrays.toString(uSec);
						break;
					case "opencpu":
						int reason1 = new JSONObject(jdata).getInt("0");
						reasons = context.getResources().getStringArray(R.array.modem_debug_results);
						dataFromJData = reasons[reason1];
						break;
					default:
						dataFromJData = new JSONObject(jdata).getString(operator);
						break;
				}
				return matchResult.group(1) + dataFromJData + matchResult.group(4);
			} catch (JSONException e)
			{
				e.printStackTrace();
				return "";
			}
		};

		final CallbackMatcher callbackMatcher = new CallbackMatcher(regex1);
		description = callbackMatcher.replaceMatches(description, callback);

		return description;
	}

	public Pair<String, String> explainEventRegDescriptionWithClass(Context context){
		final DBHelper dbHelper = DBHelper.getInstance(context);
		Pair<String, String> dsc = explainEventDescriptionWithClass(context);
		String description = dsc.second;
		final String jdata = this.jdata;
		final int reason = this.reasonId;
		final int device = this.device;
		final CallbackMatcher.Callback callback = matchResult -> {
			String operator = matchResult.group(2);
			try
			{
				String dataFromJData = ";";
				switch (operator)
				{
					case "user_id":
						if((D3Service.getRole()&Const.Roles.TRINKET)!=0){
							return "";
						}
						dataFromJData = dbHelper.getUserNameById(device, reason);
						break;
					case "user":
						int user_id = new JSONObject(jdata).getInt(operator);
						dataFromJData = user_id + " - " + dbHelper.getUserNameById(device, user_id);
						break;
					case "charging":
						dataFromJData = new JSONObject(jdata).getString(operator);
						dataFromJData = Func.getBatteryStatus(context, dataFromJData);
						break;
					case "operator_id":
						dataFromJData = new JSONObject(jdata).getString(operator);
						if(((D3Service.getRole()&Const.Roles.TRINKET)!=0) && (D3Service.getUserId() != Integer.valueOf(dataFromJData))){
							dataFromJData = "";
							break;
						}
						dataFromJData = dbHelper.getOperatorNameByEventData(dataFromJData);
						break;
					case "command_id":
						dataFromJData = new JSONObject(jdata).getString(operator);
						String[] commands = context.getResources().getStringArray(R.array.command_types);
						int command_id = Integer.valueOf(dataFromJData);
						Command command = dbHelper.getCommandById(command_id);
						if(null!=command)
						{
							dataFromJData += " " + commands[command.explainCommand()];
						}
						break;
					case "status":
						dataFromJData = new JSONObject(jdata).getString(operator);
						try
						{
							int result = Integer.valueOf(dataFromJData);
							String[] results = context.getResources().getStringArray(R.array.command_results);
							if( result <= 0)
							{
								result=-result;
								results = context.getResources().getStringArray(R.array.command_results_negative);
							}
							dataFromJData = result <= results.length ?
									(result!=0? results[result - 1] : results[result])
									: (App.getContext().getResources().getString(R.string.UNKNOWN_COMAND_FAIL_REASON)) ;
						}catch (NumberFormatException e){
							e.printStackTrace();
						}
						break;
					case "physic":
						dataFromJData = new JSONObject(jdata).getString(operator);
						if(null!=D3Service.d3XProtoConstEvent){
							DPhysic dPhysic = D3Service.d3XProtoConstEvent.getPhysicById(Integer.valueOf(dataFromJData));
							if(null!=dPhysic){
								dataFromJData = dPhysic.caption;
							}
						}
						break;
					case "reason_id":
						if((D3Service.getRole()&Const.Roles.TRINKET)!=0){
							return "";
						}
						dataFromJData = Integer.toString(reason);
						break;
					case "reboot_reason":
						int rebootReason = new JSONObject(jdata).getInt("reason");
						String[] reasons = context.getResources().getStringArray(R.array.reboot_reasons);
						dataFromJData = rebootReason <=reasons.length ? (reasons[rebootReason]): (reasons[reasons.length]);
						break;
					case "sections":
						JSONArray uSecArray = new JSONObject(jdata).getJSONArray("sections");
						String[] uSec = new String[uSecArray.length()];
						for(int si = 0; si < uSecArray.length(); si++){
							uSec[si] = uSecArray.getString(si);
						}
						dataFromJData = Arrays.toString(uSec);
						break;
					case "opencpu":
						int reason1 = new JSONObject(jdata).getInt("0");
						reasons = context.getResources().getStringArray(R.array.modem_debug_results);
						dataFromJData = reasons[reason1];
						break;
					default:
						dataFromJData = new JSONObject(jdata).getString(operator);
						break;
				}
				return matchResult.group(1) + dataFromJData + matchResult.group(4);
			} catch (JSONException e)
			{
				e.printStackTrace();
				return "";
			}
		};

		final CallbackMatcher callbackMatcher = new CallbackMatcher(regex1);
		description = callbackMatcher.replaceMatches(description, callback);

		return new Pair<>(dsc.first, description);
	}

	//for simplity data in notifications
	public String explainEventRegDescriptionLite(Context context){
		final DBHelper dbHelper = DBHelper.getInstance(context);
		String description = explainEventDescription(context);
		final String jdata = this.jdata;
		final int reason = this.reasonId;
		final int device = this.device;
		//\?(\w)\[(([^,]+),)?(\d+),(\d+)\]
		//\?([\w]+)\[(([\w]+),)?(\d+),(\d+)\]
		final CallbackMatcher.Callback callback = new CallbackMatcher.Callback()
		{
			public String foundMatch(MatchResult matchResult)
			{
				String operator = matchResult.group(2);
				try
				{
					String dataFromJData = ";";
					switch (operator)
					{
						case "user_id":
							if((D3Service.getRole()&Const.Roles.TRINKET)!=0){
								return "";
							}
							dataFromJData = dbHelper.getUserNameById(device, reason);
							break;
						case "user":
							int user_id = new JSONObject(jdata).getInt(operator);
							dataFromJData = user_id + " - " + dbHelper.getUserNameById(device, user_id);
							break;
						case "charging":
							dataFromJData = new JSONObject(jdata).getString(operator);
							dataFromJData = Func.getBatteryStatus(context, dataFromJData);
							break;
						case "operator_id":
							dataFromJData = new JSONObject(jdata).getString(operator);
							if(((D3Service.getRole()&Const.Roles.TRINKET)!=0) && (D3Service.getUserId() != Integer.valueOf(dataFromJData))){
								dataFromJData = "";
								break;
							}
							dataFromJData = dbHelper.getOperatorNameByEventData(dataFromJData);
							break;
						case "command_id":
							dataFromJData = "";
							break;
						case "status":
							dataFromJData = new JSONObject(jdata).getString(operator);
							try
							{
								int result = Integer.valueOf(dataFromJData);
								String[] results = context.getResources().getStringArray(R.array.command_results);
								if( result <= 0)
								{
									result=-result;
									results = context.getResources().getStringArray(R.array.command_results_negative);
								}
								dataFromJData = result <= results.length ?
										(result!=0? results[result - 1] : results[result])
										: (App.getContext().getResources().getString(R.string.UNKNOWN_COMAND_FAIL_REASON)) ;
							}catch (NumberFormatException e){
								e.printStackTrace();
							}
							break;
						case "physic":
							dataFromJData = new JSONObject(jdata).getString(operator);
							if(null!=D3Service.d3XProtoConstEvent){
								DPhysic dPhysic = D3Service.d3XProtoConstEvent.getPhysicById(Integer.valueOf(dataFromJData));
								if(null!=dPhysic){
									dataFromJData = dPhysic.caption;
								}
							}
							break;
						case "reason_id":
							if((D3Service.getRole()&Const.Roles.TRINKET)!=0){
								return "";
							}
							dataFromJData = Integer.toString(reason);
							break;
						case "reboot_reason":
							int rebootReason = new JSONObject(jdata).getInt("reason");
							String[] reasons = context.getResources().getStringArray(R.array.reboot_reasons);
							dataFromJData = rebootReason <=reasons.length ? (reasons[rebootReason]): (reasons[reasons.length]);
							break;
						case "sections":
							JSONArray uSecArray = new JSONObject(jdata).getJSONArray("sections");
							String[] uSec = new String[uSecArray.length()];
							for(int si = 0; si < uSecArray.length(); si++){
								uSec[si] = uSecArray.getString(si);
							}
							dataFromJData = Arrays.toString(uSec);
							break;
						case "opencpu":
							int reason = new JSONObject(jdata).getInt("0");
							reasons = context.getResources().getStringArray(R.array.modem_debug_results);
							dataFromJData = reasons[reason];
							break;
						default:
							dataFromJData = new JSONObject(jdata).getString(operator);
							break;
					}
					return !dataFromJData.equals("") ? matchResult.group(1) + dataFromJData + matchResult.group(4) : "";
				} catch (JSONException e)
				{
					e.printStackTrace();
					Func.log_e(D3Service.LOG_TAG, "Event reg description error " + id);
					return "";
				}
			}
		};

		final CallbackMatcher callbackMatcher = new CallbackMatcher(regex1);
		description = callbackMatcher.replaceMatches(description, callback);

		return description;
	}

	public String explainEventClass(){
		if(null!=D3Service.d3XProtoConstEvent)
		{
			D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
			EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
			if (null == eClass)
			{
				return Integer.toString(this.classId) + " + " + Integer.toString(this.detectorId) + " + " + Integer.toString(this.reasonId);
			} else
			{
				return eClass.description;
			}
		}
		return Integer.toString(this.classId) + " + " + Integer.toString(this.detectorId) + " + " + Integer.toString(this.reasonId);
	}

	public String explainColor(){
		D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
		EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
		if(null!=eClass)
		{
			EDetector eDetector = eClass.getDetectorById(this.detectorId);
			if(null!=eDetector)
			{
				EReason eReason = eDetector.getReasonById(this.reasonId);
				if(null!=eReason)
				{
					EStatement eStatement = eReason.getStatementById(this.active);
					if(null!=eStatement)
					{
						return eStatement.color;
					}else{
						return "#FFFFFF";
					}
				}else{
					return "#FFFFFF";
				}
			}else{
				return "#FFFFFF";
			}
		}else{
			return "#FFFFFF";
		}
	}

	public String explainAffectIconBig(){
		if(null!=D3Service.d3XProtoConstEvent)
		{
			D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
			EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
			if (null != eClass)
			{
				EDetector eDetector = eClass.getDetectorById(this.detectorId);
				if (null != eDetector)
				{
					EReason eReason = eDetector.getReasonById(this.reasonId);
					if (null != eReason)
					{
						return  eReason.iconBig;
					}
				}
				EReason eReason = eClass.getReasonById(this.reasonId);
				if (null != eReason)
				{
					return eReason.iconBig;
				}
				return eClass.iconBig;
			}
		}
		return "@drawable/ic_question_grey_500_24dip";
	}

	public String explainAffectIconSmall(){
		if(null!=D3Service.d3XProtoConstEvent)
		{
			D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
			EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
			if (null != eClass)
			{
				EDetector eDetector = eClass.getDetectorById(this.detectorId);
				if (null != eDetector)
				{
					EReason eReason = eDetector.getReasonById(this.reasonId);
					if (null != eReason)
					{
						return  eReason.iconSmall;
					}
				}
				EReason eReason = eClass.getReasonById(this.reasonId);
				if (null != eReason)
				{
					return eReason.iconSmall;
				}
				return eClass.iconSmall;
			}
		}
		return "@drawable/ic_question_grey_500_24dip";
	}

	public String explainIcon(){
		if(null!=D3Service.d3XProtoConstEvent)
		{
			D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
			EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
			if (null != eClass)
			{
				EDetector eDetector = eClass.getDetectorById(this.detectorId);
				if (null != eDetector)
				{
					EReason eReason = eDetector.getReasonById(this.reasonId);
					if (null != eReason)
					{
						EStatement eStatement = eReason.getStatementById(this.active);
						if (null != eStatement)
						{
							return eStatement.icon;
						}
					}
				}
				EReason eReason = eClass.getReasonById(this.reasonId);
				if (null != eReason)
				{
					EStatement eStatement = eReason.getStatementById(this.active);
					if (null != eStatement)
					{
						return eStatement.icon;
					}
				}
				EStatement eStatement = eClass.getStatementById(this.active);
				if (null != eStatement)
				{
					return eStatement.icon;
				}
			}
		}
		return "@drawable/ic_question_grey_500_24dip";
	}


	/*Проверка показывать событие или нет*/
	public boolean isShouldBeDisplayedAtAll(DBHelper dbHelper, int roles, SharedPreferences sp){
		if((15 == this.classId) && ((1 == this.reasonId) || (0 == this.reasonId) || (2 == this.reasonId)) ||
				(7 == this.classId && 21 == this.detectorId && 1 == this.reasonId ))
		{
			return false;
		}
		else if((roles& Const.Roles.TRINKET)==0)
		{
			if((0 == this.classId) && (this.reasonId == 11)) {
				if (Func.getBooleanSPDefFalse(sp, "pref_forced"))
				{
					return false;
				}
			}
			else if ((!Func.debug())
					&&((6 == this.classId) && (this.section == 0) && (this.reasonId == 9) && (this.active == 1)))
			{
				if ((null != this.jdata) && (!(this.jdata).equals("")))
				{
					try
					{
						JSONObject jdataObject = new JSONObject(this.jdata);
						int newBatteryLevel = jdataObject.getInt("%");
						if ((newBatteryLevel > 15) && (newBatteryLevel != 100))
						{
							String currentDeviceBatteryState = dbHelper.getCurrentDeviceBatteryState(this.device);
							if (null != currentDeviceBatteryState)
							{
								if (Math.abs(Integer.valueOf(currentDeviceBatteryState) - newBatteryLevel) < 10)
								{
									return false;
								}
							}
						}
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					return true;
				} else
				{
					return false;
				}
			}
			return true;
		}else //for trinket role
			if((this.classId == 4)
					||(this.classId == 5 && (this.reasonId == 5 || this.reasonId == 26))
					||(this.classId == 15 && this.reasonId ==10)
					|| (this.classId == 3 && this.reasonId == 5)
					|| (this.classId == 8 && this.reasonId == 6)
					){
				return true;
			}else {
				return false;
			}
	}

	public boolean displayAffect(int roles){
		if((15 == this.classId) && ((1 == this.reasonId) || (0 == this.reasonId) || (2 == this.reasonId))){
			return false;
		}else if(0!=(roles& Const.Roles.TRINKET)){
			return false;
		}
		return true;
	}

	public  boolean isShowable(SharedPreferences sp){
		EClass eClass = D3Service.d3XProtoConstEvent.getClassById(this.classId);
		if(eClass!=null ){
			if((this.classId < 5 && (Func.getBooleanSPDefTrue(sp, "notif_class_status_" + eClass.id)))
				||((this.classId > 4)&&(Func.getBooleanSPDefFalse(sp, "notif_class_status_" + eClass.id)))){
				return true;
			}
		}
		return false;
	}

	//describe event device_section_zone for events list
	public String getEventLocation(Context context)
	{
		String eventLocalString = context.getResources().getString(R.string.UNKNOWN);
		DBHelper dbHelper = DBHelper.getInstance(context);
		if(this.section == 0){
			if(this.zone == 0 || this.zone == 100)
			{
				if(this.classId == 4){
					if(!this.jdata.equals("")){
						eventLocalString  = this.secNamesJDataWithOptions(context);
					}else{
						eventLocalString = context.getString(R.string.EVENTS_UNKNOWN_SECTIONS);
					}
				}else
				{
					Device device = dbHelper.getDeviceById(this.device);
					if(null!=device){
						DeviceType dType = device.getType();
						switch (dType.id){
							case Const.DEVICETYPE_SH:
							case Const.DEVICETYPE_SH_1:
							case Const.DEVICETYPE_SH_1_1:
							case Const.DEVICETYPE_SH_2:
							case Const.DEVICETYPE_SH_4G:
								eventLocalString = device.getName() + " " + device.account;
								break;
							default:
								if(this.zone == 0){
									eventLocalString = dbHelper.getSectionNameByIdWithOptions(this.device, this.section) + "(" + context.getString(R.string.CONTROLLER) + ")";
								}else{
									eventLocalString = dbHelper.getZoneNameById( this.device, this.section, this.zone) + " • " + dbHelper.getSectionNameByIdWithOptions(this.device, this.section);
								}
								break;
						}
					}
				}
			}else{
				eventLocalString = dbHelper.getZoneNameById( this.device, this.section, this.zone) + " • " + dbHelper.getSectionNameByIdWithOptions(this.device, this.section);
			}
		}else{
			if(this.zone ==0){
				eventLocalString = dbHelper.getSectionNameByIdWithOptions(this.device, this.section);
			}else{
				eventLocalString = dbHelper.getZoneNameById( this.device, this.section, this.zone) + " • " + dbHelper.getSectionNameByIdWithOptions(this.device, this.section);
			}
		}
		return eventLocalString;
	}

	/*DEPRECATED*/
	//describe event device_section_zone for events info dialog
	public String getEventLocationWithNum(Context context)
	{
		String eventLocalString = context.getResources().getString(R.string.UNKNOWN);
		DBHelper dbHelper = DBHelper.getInstance(App.getContext());
		if(this.section == 0){
			if(this.zone == 0)
			{
				if(this.classId == 4){
					if(!this.jdata.equals("")){
						eventLocalString  = this.sectionsFromJData(context);
					}else{
						eventLocalString = context.getString(R.string.EVENTS_UNKNOWN_SECTIONS);
					}
				}else
				{
					eventLocalString = dbHelper.getSectionNameByIdWithOptions(this.device, this.section) + "(" + context.getString(R.string.CONTROLLER) + ")";
				}
			}else{
				eventLocalString = dbHelper.getZoneNameById( this.device, this.section, this.zone) + "(" + this.zone + ")" + " • " + dbHelper.getSectionNameByIdWithOptions(this.device, this.section) + "(" + this.section + ")" ;
			}
		}else{
			if(this.zone ==0){
				eventLocalString = dbHelper.getSectionNameByIdWithOptions(this.device, this.section) + "(" + this.section + ")";
			}else{
				eventLocalString = dbHelper.getZoneNameById( this.device, this.section, this.zone) + "(" + this.zone + ")" + " • " + dbHelper.getSectionNameByIdWithOptions(this.device, this.section) + "(" + this.section + ")";
			}
		}
		return eventLocalString;
	}

	public String sectionsFromJData(Context context)
	{
		String deviceLocalStringNF = "";
		DBHelper dbHelper = DBHelper.getInstance(context);
		try
		{
			JSONObject jdataObject = new JSONObject(this.jdata);
			JSONArray sectionsArray = jdataObject.getJSONArray("sections");
			if(null!=sectionsArray && sectionsArray.length() !=0){
				if(sectionsArray.length() > 1)
				{
					deviceLocalStringNF = context.getString(R.string.EVENTS_SECTIONS);
				}else{
					deviceLocalStringNF = context.getString(R.string.EVENTS_SECTION);
				}
				for(int i = 0; i < sectionsArray.length(); i++){
					int id = sectionsArray.getInt(i);
					deviceLocalStringNF += " " + dbHelper.getSectionNameByIdWithOptions(this.device, id) + "(" + String.valueOf(id) + "),";
				}
				deviceLocalStringNF = deviceLocalStringNF.substring(0, deviceLocalStringNF.length() - 1);
			}else{
				deviceLocalStringNF = context.getString(R.string.EVENTS_UNKNOWN_SECTIONS);
			}

		} catch (JSONException e)
		{
			deviceLocalStringNF = context.getString(R.string.EVENTS_UNKNOWN_SECTIONS);
			e.printStackTrace();
		}
		return deviceLocalStringNF;
	}

	public String getEventLocationForTts(Context context)
	{
		String eventLocalString = null;
		DBHelper dbHelper = DBHelper.getInstance(context);
		if(this.section == 0){
			if(this.zone == 0 || this.zone == 100)
			{
				if(this.classId == 4){
					if(!this.jdata.equals("")){
						eventLocalString  = this.secNamesJDataWithOptions(context);
					}
				}else
				{
					Device device = dbHelper.getDeviceById(this.device);
					if(null!=device){
						DeviceType dType = device.getType();
						switch (dType.id){
							case Const.DEVICETYPE_SH:
							case Const.DEVICETYPE_SH_1:
							case Const.DEVICETYPE_SH_1_1:
							case Const.DEVICETYPE_SH_2:
							case Const.DEVICETYPE_SH_4G:
								eventLocalString = device.getName() + " " + device.account;
								break;
							default:
								if(this.zone==0){
									eventLocalString = dbHelper.getSectionNameByIdWithOptions(this.device, this.section) + "(" + context.getString(R.string.CONTROLLER) + ")";
								}else{
									eventLocalString = dbHelper.getZoneNameById( this.device, this.section, this.zone);
								}
								break;
						}
					}
				}
			}else{
				eventLocalString = dbHelper.getZoneNameById( this.device, this.section, this.zone);
			}
		}else{
			if(this.zone == 0){
				eventLocalString = dbHelper.getSectionNameByIdWithOptions(this.device, this.section) + "(" + this.section + ")";
			}else{
				eventLocalString = dbHelper.getZoneNameById( this.device, this.section, this.zone);
			}
		}
		return eventLocalString;
	}

	public String secNamesJDataWithOptions(Context context)
	{
		String deviceLocalStringNF = "";
		DBHelper dbHelper = DBHelper.getInstance(context);
		try
		{
			JSONObject jdataObject = new JSONObject(this.jdata);
			JSONArray sectionsArray = jdataObject.getJSONArray("sections");
			if(null!=sectionsArray && sectionsArray.length() !=0){
				if(sectionsArray.length() > 1)
				{
					deviceLocalStringNF = context.getString(R.string.EVENTS_SECTIONS);
				}else{
					deviceLocalStringNF = context.getString(R.string.EVENTS_SECTION);
				}
				for(int i = 0; i < sectionsArray.length(); i++){
					int id = sectionsArray.getInt(i);
					deviceLocalStringNF += " " + dbHelper.getSectionNameByIdWithOptions(this.device, id) + ",";
				}
				deviceLocalStringNF = deviceLocalStringNF.substring(0, deviceLocalStringNF.length() - 1);
			}else{
				deviceLocalStringNF = context.getString(R.string.EVENTS_UNKNOWN_SECTIONS);
			}

		} catch (JSONException e)
		{
			deviceLocalStringNF = context.getString(R.string.EVENTS_UNKNOWN_SECTIONS);
			e.printStackTrace();
		}
		return deviceLocalStringNF;
	}

	public String secNamesJData(Context context)
	{
		String deviceLocalStringNF = "";
		DBHelper dbHelper = DBHelper.getInstance(context);
		try
		{
			JSONObject jdataObject = new JSONObject(this.jdata);
			JSONArray sectionsArray = jdataObject.getJSONArray("sections");
			if(null!=sectionsArray && sectionsArray.length() !=0){
				for(int i = 0; i < sectionsArray.length(); i++){
					int id = sectionsArray.getInt(i);
					deviceLocalStringNF += dbHelper.getSectionNameByIdWithOptions(this.device, id) + ", ";
				}
				deviceLocalStringNF = deviceLocalStringNF.substring(0, deviceLocalStringNF.length() - 2);
			}else{
				deviceLocalStringNF = context.getString(R.string.EVENTS_UNKNOWN_SECTIONS);
			}
		} catch (JSONException e)
		{
			deviceLocalStringNF = context.getString(R.string.EVENTS_UNKNOWN_SECTIONS);
			e.printStackTrace();
		}
		return deviceLocalStringNF;
	}

	public int getBearer(){
		try
		{
			JSONObject jsonObject=  new JSONObject(this.jdata);
			return jsonObject.getInt("bearer");
		}catch (JSONException e){
			e.printStackTrace();
		}
		return 0;
	}


	public String getLevel(){
		try
		{
			JSONObject jsonObject=  new JSONObject(this.jdata);
			return jsonObject.getString("%") + " %";
		}catch (JSONException e){
			e.printStackTrace();
			try
			{
				JSONObject jsonObject=  new JSONObject(this.jdata);
				return jsonObject.getString("db") + " dB";
			}catch (JSONException ee){
				ee.printStackTrace();
			}
		}
		return null;
	}

	public String getTemperatureString(){
		try
		{
			JSONObject jsonObject = new JSONObject(this.jdata);
			return jsonObject.getString("celsius");

		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public int getTemperatureValue()
	{
		try
		{
			JSONObject jsonObject = new JSONObject(this.jdata);
			return jsonObject.getInt("celsius");

		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return TEMP_NO_VALUE;
	}

	public String getDescription()
	{
		return null;
	}

	public int getNotificationIcon(Context context){
		return context.getResources().getIdentifier(this.explainIconFromSet(3), null, context.getPackageName());
	}

	public int getListIcon(Context context)
	{
		return  context.getResources().getIdentifier(this.explainIconFromSet(0), null, context.getPackageName());
	}

	public int getMainIcon(Context context)
	{
		return  context.getResources().getIdentifier(this.explainIconFromSet(1), null, context.getPackageName());
	}

	public int getBigIcon(Context context)
	{
		return  context.getResources().getIdentifier(this.explainIconFromSet(2), null, context.getPackageName());
	}


	public int getAffectIcon(Context context){
		return  context.getResources().getIdentifier(this.explainIcon(), null, context.getPackageName());
	}


	public String explainIconFromSet(int i)
	{
		if(null!=D3Service.d3XProtoConstEvent)
		{
			D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
			EClass eClass = d3XProtoConstEvent.getClassById(this.classId);
			if (null != eClass)
			{
				EDetector eDetector = eClass.getDetectorById(this.detectorId);
				if (null != eDetector)
				{
					EReason eReason = eDetector.getReasonById(this.reasonId);
					if (null != eReason)
					{
						EStatement eStatement = eReason.getStatementById(this.active);
						if (null != eStatement)
						{
							if(null!=eStatement.icons)
								return eStatement.icons.size() > i ? eStatement.icons.get(i).src: eStatement.icons.get(0).src;
						}
					}
//					else{
//						EStatement eStatement = eDetector.getStatementById(this.active);
//						if (null != eStatement)
//						{
//							if(null!=eStatement.icons)
//								return eStatement.icons.size() > i ? eStatement.icons.get(i).src: eStatement.icons.get(0).src;
//						}
//					}
				}
				EReason eReason = eClass.getReasonById(this.reasonId);
				if (null != eReason)
				{
					EStatement eStatement = eReason.getStatementById(this.active);
					if (null != eStatement)
					{
						if(null!=eStatement.icons)
							return eStatement.icons.size() > i ? eStatement.icons.get(i).src: eStatement.icons.get(0).src;
					}
				}
				EStatement eStatement = eClass.getStatementById(this.active);
				if (null != eStatement)
				{
					if(null!=eStatement.icons)
						return eStatement.icons.size() > i ? eStatement.icons.get(i).src: eStatement.icons.get(0).src;
				}
			}
		}
		return null;
	}

	public int getLink(){
		if ((this.classId == 11) && (this.reasonId == 1))
		{
			if(null!=this.jdata && !this.jdata.equals("")){
				return LINK_GEO;
			}
		}else if(((this.classId == 2)||(this.classId == 1)||(this.classId == 0))
				&&((this.jdata !=null)
				&&(!this.jdata.equals("")))){
			if(this.jdata.contains("url")){
				return LINK_VIDEO;
			}else if(this.jdata.contains("notification")){
				return LINK_VIDEO_FAILED;
			}
		}
		return NO_LINK;
	}

	public String getSectionName(Context context)
	{
		String section = null;
		DBHelper dbHelper = DBHelper.getInstance(context);
		if(this.classId == 4){
			if(!this.jdata.equals("")){
				section  = this.secNamesJData(context);
			}
		}else
		{
			if(this.section!=0) {
				section = dbHelper.getSectionNameById(this.device, this.section);
			}
		}
		return section;
	}
}
