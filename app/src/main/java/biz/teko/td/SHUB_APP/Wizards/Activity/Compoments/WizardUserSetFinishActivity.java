package biz.teko.td.SHUB_APP.Wizards.Activity.Compoments;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;

public  class WizardUserSetFinishActivity extends BaseActivity
{
	private int selSiteId;
	private int selDeviceId;
	private DBHelper dbHelper;
	private Context context;
	private UserInfo userInfo;
	private ServiceConnection serviceConnection;
	private Device device;
	private String deviceSerial;
	private String deviceModel;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_user_set_finish);
		context= this;
		selSiteId = getIntent().getIntExtra("site_id", -1);
		selDeviceId = getIntent().getIntExtra("device_id", 0);
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();
		context = this;

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.USER_SET_TITLE);
		if(-1 != selDeviceId){
			device = dbHelper.getDeviceById(selDeviceId);
			if(null!=device){
				deviceSerial = Integer.toString(device.account);
				deviceModel = device.getName();
			}
			toolbar.setSubtitle(deviceModel + "(" + deviceSerial + ")  •  " + getString(R.string.USER_CODE_ADD_TITLE));
			toolbar.setSubtitleTextColor(getResources().getColor(R.color.brandColorWhite));

		}

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		Button finishButton = (Button) findViewById(R.id.wizUserFinishExit);
		Button repeatButton = (Button) findViewById(R.id.wizUserFinishRepeat);

		repeatButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
				Intent activityIntent = new Intent(getBaseContext(), WizardUserSetActivity.class);
				activityIntent.putExtra("site_id", selSiteId);
				activityIntent.putExtra("device_id", selDeviceId);
				startActivity(activityIntent);
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		finishButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}
}
