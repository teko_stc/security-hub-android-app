package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua;

import android.Manifest;
import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.BuildConfig;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaLiveModel;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaP2PLoginModule;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaSnapshotModule;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaTalkModule;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NetSDKLib;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopInfoLayout;

public  class NCameraDahuaActivity extends BaseActivity
{
	private Context context;
	private Application app;
	private DBHelper dbHelper;
	private NDahuaHelper nDahuaHelper;
	private LinearLayout back;
	private TextView title;
	private LinearLayout settings;
	private String sn;
	private NTopInfoLayout topInfoFrame;
	private Camera camera;
	private NDahuaP2PLoginModule p2pLoginModule;
	private SurfaceView streamView;
	private NActionButton screenshotButton;
	private NActionButton speakButton;
	private NActionButton recordButton;

	private boolean logging = false;
	private NDahuaLiveModel liveModule;
	private NDahuaTalkModule talkModule;

	private boolean isVideoTalking = false;
	private  boolean record = false;
	private NActionButton recordsButton;
	private boolean permissions;
	private AsyncTask<String, Integer, Boolean> loginTask;
	private VideoTalkTask task;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_dahua_activity);

		context = this;
		app = getApplication();
		dbHelper = DBHelper.getInstance(context);
		nDahuaHelper = NDahuaHelper.getInstance(context);

		String sn = getIntent().getStringExtra("cameraId");
//		long l_handle = getIntent().getLongExtra("cameraHandle", 0);
		if(null!=sn && null==camera) {
			camera = dbHelper.getDahuaCameraBySn(sn);
			p2pLoginModule = nDahuaHelper.getP2PModule(sn);
			if(null!=p2pLoginModule) {
				camera.setLoginHandle(p2pLoginModule.getLoginHandle());
				setModules();
			}
//			camera.setLoginHandle(l_handle);
		}

		setup();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		nDahuaHelper.init();
		bind();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if(!permissions)
		{
			if(null!=talkModule && talkModule.isTalking()){
				talkModule.stopTalk();
			}
			if (null != liveModule && liveModule.isRealPlaying())
			{
				liveModule.stopRealPlay();
//				new Handler(Looper.getMainLooper()).post(() -> streamView.setVisibility(View.INVISIBLE));
				new Handler(Looper.getMainLooper()).post(() -> 		{
					if(null!=topInfoFrame) topInfoFrame.setVideoVisibility(NTopInfoLayout.VIDEO_LOADING);
				});
			}
		}
		permissions = false;
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		if (null != liveModule && liveModule.isRealPlaying())
		{
			liveModule.stopRealPlay();
//			new Handler(Looper.getMainLooper()).post(() -> streamView.setVisibility(View.INVISIBLE));
			new Handler(Looper.getMainLooper()).post(() -> 		{
				if(null!=topInfoFrame) topInfoFrame.setVideoVisibility(NTopInfoLayout.VIDEO_LOADING);
			});
		}
		if (null != p2pLoginModule && p2pLoginModule.isServiceStarted())
		{
			if (record) switchRecord();
			nDahuaHelper.getP2PModule(camera.sn).logout();
			nDahuaHelper.getP2PModule(camera.sn).stopP2pService();

//			p2pLoginModule.logout();
//			p2pLoginModule.stopP2pService();
		}

		if(null!=loginTask && loginTask.getStatus() == AsyncTask.Status.RUNNING)
			try
			{
				loginTask.cancel(true);
			}catch (Exception ignored){
				Func.log_d(Const.LOG_TAG_DAHUA, "login task cancel exception");
			};
	}

	private void bind()
	{
		setTopFrame();
		setDahuaLive();

		if(null!=screenshotButton) {
			screenshotButton.setDisable(false);
			screenshotButton.setOnButtonClickListener(action -> makeScreeshot());
		}
		if(null!=speakButton) {
			speakButton.setDisable(false);
			speakButton.setOnButtonHoldListener(is_pressed -> talk(is_pressed));
		}
		if(null!=recordButton) {
			recordButton.setDisable(false);
			recordButton.setOnButtonClickListener(action -> record());
		}
		if(null!=recordsButton) {
			recordsButton.setDisable(false);
			recordsButton.setOnButtonClickListener(action -> records());
		}
	}

	private void setDahuaLive(){
		if(null!=camera)
			if(liveModule!=null){
				if(!initVideo()){
					loginAndPlay();
				}
//				if (liveModule.isRealPlaying())
//				{
//					initVideo();
//				}else{
//					if(!startVideoStream(true))
//						loginAndPlay();
//				}
			}else{
				loginAndPlay();
			}
	}

	private void loginAndPlay(){
		NetSDKLib.getInstance().init();
		p2pLoginModule = new NDahuaP2PLoginModule();
		if(null!=topInfoFrame) topInfoFrame.setVideoVisibility(NTopInfoLayout.VIDEO_LOADING);
		if(!(logging || null!=p2pLoginModule && p2pLoginModule.getLoginHandle() != 0)){
			logging = true;
			loginTask = p2pLoginModule.startLoginTask(camera, (camera) -> {
				logging = false;
				if(null!=camera)
				{
					setDahuaLive();
				}else{
					if(p2pLoginModule.isServiceStarted()){
						p2pLoginModule.logout();
						p2pLoginModule.stopP2pService();
					}
					setDahuaLive();
				}
			});
		}
	}

	private boolean initVideo()
	{
		Func.log_d(Const.LOG_TAG_DAHUA, "callback login task camera" + " " + this.camera.sn);
		setModules();
		if (null!= streamView)
		{
			setSurface();

			if (!liveModule.isRealPlaying())
			{
				return startVideoStream(true);
			}
//			else
//			{
//				liveModule.stopRealPlay();
//				if (p2pLoginModule.isServiceStarted()) {
//					p2pLoginModule.logout();
//					p2pLoginModule.stopP2pService();
//				}
//			}
		}
		return false;
	}

	private void setSurface()
	{
		streamView.getHolder().addCallback(new SurfaceHolder.Callback()
		{
			@Override
			public void surfaceCreated(@NonNull SurfaceHolder holder)
			{
				liveModule.initSurfaceView(streamView);
				topInfoFrame.setVideoVisibility(NTopInfoLayout.VIDEO_SUCCESS);
			}

			@Override
			public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height)
			{

			}

			@Override
			public void surfaceDestroyed(@NonNull SurfaceHolder holder)
			{

			}
		});
	}

	private boolean startVideoStream(boolean audio)
	{
		Func.log_d(Const.LOG_TAG_DAHUA, "start play live video from d_activity" + " " + this.camera.sn);
		if(liveModule.isRealPlaying()) {
			liveModule.stopRealPlay();
		}
		liveModule.startPlay(0, audio ? 1 : 0 , streamView);
		if (!liveModule.isRealPlaying())
		{
			Func.log_d(Const.LOG_TAG_DAHUA, "live video failed");
//			Func.pushToast(context, getString(R.string.N_DAHUA_LIVE_STREAM_ERROR));
			return false;
		}
		return true;
	}

	private void setModules()
	{
//		if(null!=p2pLoginModule){
			liveModule = new NDahuaLiveModel(context, camera, p2pLoginModule.getLoginHandle());
			talkModule = new NDahuaTalkModule(this, p2pLoginModule.getLoginHandle());
//		}
	}

	/*RECORDS*/
	private void records()
	{
		Intent intent = new Intent(context, NCameraDahuaRecordsActivity.class);
		intent.putExtra("cameraId", camera.sn);
		startActivity(intent);
		overridePendingTransition(R.animator.enter, R.animator.exit);
	}

	/*RECORD*/
	private void record()
	{
		if(null!=liveModule){
			if(!(logging || null==p2pLoginModule || p2pLoginModule.getLoginHandle() == 0))
			{
				switchRecord();
			}else{
				Func.pushToast(context, getString(R.string.N_DAHUA_CONNECTION_ERROR));
			}
		}else{
			Func.pushToast(context, getString(R.string.N_DAHUA_OFFLINE_ERROR));
		}
	}

	private void switchRecord()
	{
//		boolean access;
//		if(Build.VERSION.SDK_INT > 23) {
//			access = checkAndGrandPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE});
//		}else {
//			access = checkAndGrandPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE});
//		}
		if (checkAndGrandPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}))
		{
			if(liveModule.isRealPlaying())
			{
				record = !record;
				boolean result = liveModule.record(record);
				if (result)
				{
					if (record)
					{
						screenshotButton.setDisable(true);
						speakButton.setDisable(true);
					} else
					{
						screenshotButton.setDisable(false);
						speakButton.setDisable(false);
					}
				} else
				{
					record = !record;
				}
				recordButton.setSelection(record);
			}else{
				Func.pushToast(context, getString(R.string.N_DAHUA_RECORD_FAILED_TRY_AGAIN));
				startVideoStream(true);
			}
		}
	}

	/*talk*/
	private void talk(boolean b)
	{
		if(null!=camera && null!=liveModule)
		{
			if(!(logging || null==p2pLoginModule || p2pLoginModule.getLoginHandle() == 0))
			{
				if (checkAndGrandPermissions(new String[]{android.Manifest.permission.RECORD_AUDIO}))
				{
					//				talkModule.setLoginHandle(p2pLoginModule.getLoginHandle());
//					if(liveModule.isRealPlaying()) liveModule.stopRealPlay();
//					if(null!=task){
//						task.cancel(true);
//					}else {
//						task = new VideoTalkTask();
//						task.execute();
//					}
					task = new VideoTalkTask(b);
					task.execute();
				}
			}
		}
	}

	private class VideoTalkTask extends AsyncTask<String, Integer, Boolean>
	{
		private final boolean is_pressed;

		public VideoTalkTask(boolean b) {
			this.is_pressed = b;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			return videoTalk(is_pressed);
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (!result) {
				Func.pushToast(context, getString(R.string.N_DAHUA_UNABLE_FUNCTION));
				return;
			}
			if (!isVideoTalking && is_pressed) {
				screenshotButton.setDisable(true);
				recordButton.setDisable(true);

				isVideoTalking = true;
				if (!liveModule.isRealPlaying()) {
					Func.pushToast(context, getString(R.string.N_DAHUA_ERROR_NO_CONNECTION));
				}else if (!talkModule.isTalking()) {
					Func.pushToast(context, getString(R.string.N_DAHUA_ERROR_NO_CONNECTION));
				}
			} else {
				isVideoTalking = false;
				screenshotButton.setDisable(false);
				recordButton.setDisable(false);
			}

		}
	}

	private boolean videoTalk(boolean b) {
		if (!isVideoTalking && b) {
//			if(!b) return true;
			if(talkModule.startClientTalk()){
				liveModule.setOpenSound(false);
				startVideoStream(false);
				if (talkModule.isTalking()) {
					return true;
				}
			}
			return false;
		}else{
			stopTalk();
			return true;
		}
	}

	private void stopTalk() {
		talkModule.stopTalk();
		liveModule.setOpenSound(true);
		startVideoStream(true);
	}

	/*SCREENSHOT*/
	private void makeScreeshot()
	{
		boolean access;
		if(Build.VERSION.SDK_INT > 23) {
			access = checkAndGrandPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE});
		}else {
			access = checkAndGrandPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE});
		}
		if(access){
			if(!(logging || p2pLoginModule.getLoginHandle() != 0)){
				logging = true;
				p2pLoginModule.startLoginTask(camera, camera -> {
					logging  = false;
					if(null!=camera){
						getBitmap();
					}
				});
			}else{
				getBitmap();
			}
		}
	}

	private void getBitmap()
	{
		NDahuaSnapshotModule nDahuaSnapshotModule = new NDahuaSnapshotModule(context, camera);
		nDahuaSnapshotModule.setSnapRevCallBack((lLoginID, pBuf, RevLen, EncodeType, CmdSerial) -> {
			Func.log_d(Const.LOG_TAG_DAHUA, "callback snapshot task camera" + " " + camera.sn);
			byte[] data = new byte[RevLen];
			System.arraycopy(pBuf, 0, data, 0, RevLen);
			saveImage(Func.getImage(data));
//			nDahuaSnapshotModule.stopCapturePicture(0);
		});
		if(!nDahuaSnapshotModule.remoteCapturePicture(0)){
			Func.log_d(Const.LOG_TAG_DAHUA, "Не удалось сохранить скриншот");
		};
	}

	private void saveImage(Bitmap bitmap) {
		if (android.os.Build.VERSION.SDK_INT >= 29) {
			ContentValues values = contentValues();
			values.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/" + getString(R.string.application_name));
			values.put(MediaStore.Images.Media.IS_PENDING, true);

			Uri uri = this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			if (uri != null) {
				try {
					saveImageToStream(bitmap, this.getContentResolver().openOutputStream(uri));
					values.put(MediaStore.Images.Media.IS_PENDING, false);
					this.getContentResolver().update(uri, values, null, null);
					Func.showSnackBar(context, findViewById(android.R.id.content), getString(R.string.N_DAHUA_SCREENSHOT_SAVED), "Отправить", () -> {
						sendScreenshot(uri);
					});
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

			}
		} else {
			File directory = new File(Func.getMediaFilesOldDirectory(context));

			if (!directory.exists()) {
				directory.mkdirs();
			}
			String fileName = System.currentTimeMillis() + ".png";
			File file = new File(directory, fileName);
			try {
				saveImageToStream(bitmap, new FileOutputStream(file));
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
				this.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
				Func.showSnackBar(context, findViewById(android.R.id.content), getString(R.string.N_DAHUA_SCREENSHOT_SAVED), getString(R.string.N_BUTTON_SEND), () -> {
					sendScreenshot(FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", file));
				});
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}

		}
	}

	private void sendScreenshot(Uri uri)
	{
		if (uri != null) {
			Intent shareIntent = new Intent();
			shareIntent.setAction(Intent.ACTION_SEND);
			shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
			shareIntent.setDataAndType(uri, getContentResolver().getType(uri));
			shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
			startActivity(Intent.createChooser(shareIntent, getString(R.string.N_DAHUA_CHOOSE_APP)));
		}
	}

	private ContentValues contentValues() {
		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
		values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
			values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
		}
		return values;
	}

	private void saveImageToStream(Bitmap bitmap, OutputStream outputStream) {
		if (outputStream != null) {
			try {
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
				outputStream.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private boolean checkAndGrandPermissions(String[] ps)
	{
		permissions =  true;
		List<String> permissions = new ArrayList<>();
		for(String p:ps){
			if(ActivityCompat.checkSelfPermission(this, p)
					!= PackageManager.PERMISSION_GRANTED){
				permissions.add(p);
			}
		}
		if(permissions.size() > 0) {
			ActivityCompat.requestPermissions((NCameraDahuaActivity) context, permissions.toArray(new String[permissions.size()]), 123);
			return false;
		}
		return true;
	}


	private void setTopFrame()
	{
		if(null!=topInfoFrame){
			if(null!=camera){
				topInfoFrame.setTitle(camera.name);
				topInfoFrame.setOnTopNavigationListener(new NTopInfoLayout.OnTopNavigationListener()
				{
					@Override
					public void favoriteChanged()
					{

					}

					@Override
					public void backPressed()
					{
						onBackPressed();
					}

					@Override
					public void setOptions()
					{
						Intent intent = new Intent(context, NCameraDahuaSettings.class);
						intent.putExtra("cameraId", camera.sn);
						startActivityForResult(intent, Const.REQUEST_DELETE);
						overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
			}
		}
	}

	private void setup(){

		topInfoFrame = (NTopInfoLayout) findViewById(R.id.nTopInfoFrame);
		streamView = topInfoFrame.getSurfaceView();
		screenshotButton = (NActionButton) findViewById(R.id.nDahuaScreenshotButton);
		speakButton = (NActionButton) findViewById(R.id.nDahuaSpeakButton);
		recordButton = (NActionButton) findViewById(R.id.nDahuaRecordButton);
		recordsButton = (NActionButton) findViewById(R.id.nDahuaRecordsButton);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Const.REQUEST_DELETE) {
			if (resultCode == RESULT_OK) {
				finish();
			}
		}
	}

}
