package biz.teko.td.SHUB_APP.Cameras.Activities.IV;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.exoplayer2.ui.PlayerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.ExoPlayerVideoHandler;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 22.03.2018.
 */

public class NCameraRecordViewActivity extends BaseActivity
{
	private static final String BASE_URL = "https://openapi-alpha-eu01.ivideon.com";
	private static final String URL_COMMAND = "/media_storages";
	private static final String URL_PARTNER = "/ivideon";
	private static final String URL_REQUEST = "/event_clip?";
	private static final String URL_REQUEST_KEY = "url=";
	private static final String URL_PART_2 = "&partner_id=ivideon&app_id=";


	private Context context;
	private DBHelper dbHelper;
	private int eventId;
	private PlayerView playerView;
	private ImageView fullScreenIcon;
	private Event event;
	private boolean destroyVideo = true;
	private LinearLayout close;
	private TextView title;
	private TextView recordDecs;
	private TextView recordSite;
	private TextView recordSection;
	private TextView recordZone;
	private TextView recordDate;
	private TextView recordTime;
	private ImageView recordImage;
	private FrameLayout buttonCopyCode;
	private FrameLayout buttonShareCode;
	private String url;
	private String accessToken = "";

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record_view);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		eventId = getIntent().getIntExtra("event", -1);

		accessToken = dbHelper.getUserInfoIVToken();

		setup();
		bind();

	}

	private void setup()
	{
		close = (LinearLayout) findViewById(R.id.nButtonClose);
		title = (TextView) findViewById(R.id.nTextTitle);

		/*set event data*/
		recordDecs = (TextView) findViewById(R.id.recordEventDescText);
		recordSite = (TextView) findViewById(R.id.recordEventSiteText);
		recordSection = (TextView) findViewById(R.id.recordEventSectionText);
		recordZone = (TextView) findViewById(R.id.recordEventZoneText);
		recordDate = (TextView) findViewById(R.id.recordEventDateText);
		recordTime = (TextView) findViewById(R.id.recordEventTimeText);

		recordImage = (ImageView) findViewById(R.id.recordImageEvent);

		buttonCopyCode = (FrameLayout) findViewById(R.id.record_copy_code_button);
		buttonShareCode = (FrameLayout) findViewById(R.id.record_copy_share_button);
	}

	private void bind()
	{
		if(null!= close) { close.setOnClickListener(v -> onBackPressed()); }
		if(null!=title) title.setText(getResources().getString(R.string.CAMERA_TITLE_RECORD_VIEW));

		event = dbHelper.getEventById(eventId);

		if(null!=event)
		{
			setUrl();

			if (null != recordDecs) recordDecs.setText(event.explainEventRegDescription(context));
			if (null != recordSite) recordSite.setText(dbHelper.getSiteNameByDeviceSection(event.device, event.section));
			recordSection.setText(dbHelper.getSectionNameByIdWithOptions(event.device, event.section));
			recordZone.setText(dbHelper.getZoneNameById(event.device, event.section, event.zone));
			recordDate.setText(Func.getServerDateText(context, event.time));
			recordTime.setText(Func.getServerTimeText(context, event.time));
			recordImage.setImageResource(context.getResources().getIdentifier(event.explainIcon(), null, context.getPackageName()));

			buttonCopyCode.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(null!=url)
					{
						ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
						ClipData clip = ClipData.newPlainText("record_url", url);
						clipboard.setPrimaryClip(clip);
						Func.pushToast(context, getString(R.string.COPIED_IN_BUFFER), (NCameraRecordViewActivity) context);
					}
				}
			});

			buttonShareCode.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(null!=url && !url.equals(""))
					{
						Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
						sharingIntent.setType("text/plain");
						sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.CAMERA_URL_SHARE_SUBJECT));
						sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, url);
						startActivity(Intent.createChooser(sharingIntent, getString(R.string.DELEGATION_SHARE_TITLE)));
					}else{
						Func.pushToast(context, getString(R.string.CAMERA_CANT_FIND_VIDEO_URL_MESSAGE), (NCameraRecordViewActivity) context);
					}
				}
			});
		}
	}

	private void setUrl()
	{
		try
		{
			url =  new JSONObject(event.jdata).getString("url");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		destroyVideo = true;

		/*set video*/
		playerView = (PlayerView) findViewById(R.id.record_video_view);
		fullScreenIcon = (ImageView) findViewById(R.id.exo_fullscreen_icon);
		fullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_enter_48_brand));

		try
		{
			JSONObject jsonObject = new JSONObject(event.jdata);
			String url = jsonObject.getString("url");
			String videoUrl = "";
			try {
				Uri uri = Uri.parse(url);
				String a = uri.getAuthority();
//				if(null!=uri.getQueryParameter("expire_at")&&  Integer.parseInt(uri.getQueryParameter("expire_at"))<=(System.currentTimeMillis()/1000)){
//
//				}
				videoUrl = URLEncoder.encode(("https://" + uri.getAuthority() + uri.getPath()).split("\\?")[0], StandardCharsets.UTF_8.toString());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			url = BASE_URL + URL_COMMAND + URL_PARTNER + URL_REQUEST + URL_REQUEST_KEY
					+ videoUrl
					+ URL_PART_2
			//+ BuildConfig.IV_APP_ID
			;

			Map<String, String> headersMap = new HashMap<>();
			headersMap.put("Authorization", "Bearer " + accessToken);

			ExoPlayerVideoHandler.getInstance().prepareExoPlayerForUri(context, Uri.parse(url), playerView, new ExoPlayerVideoHandler.OnVideoListener()
			{
				@Override
				public void OnLoaded()
				{

				}
			}, headersMap);

			String finalUrl = url;

			fullScreenIcon.setOnClickListener(new View.OnClickListener(){
				@Override
				public void onClick(View v){
					destroyVideo = false;
					Intent intent = new Intent(getBaseContext(), CameraFullScreenVideoActivity.class);
					intent.putExtra("url", finalUrl);
					startActivity(intent);
					overridePendingTransition(R.animator.from_down_enter, R.animator.from_down_exit);
				}
			});
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if(destroyVideo)
		{
			ExoPlayerVideoHandler.getInstance().releaseVideoPlayer();
		}
	}
}
