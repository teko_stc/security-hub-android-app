package biz.teko.td.SHUB_APP.Wizards.Activity.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import biz.teko.td.SHUB_APP.Activity.MainNavigationActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerSetActivity;

/**
 * Created by td13017 on 07.06.2017.
 */

public class WizardRegistrationFinishActivity extends AppCompatActivity
{
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_register_finish);

		Toolbar toolbar = findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.WIZ_WELC_REGISTRATION);

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		Button buttonShSetup = findViewById(R.id.regContrSetup);
		Button buttonShSetupSkip = findViewById(R.id.regContrSetupSkip);

		buttonShSetup.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent activityIntent = new Intent(getBaseContext(), WizardControllerSetActivity.class);
				startActivity(activityIntent);
				overridePendingTransition(R.animator.enter, R.animator.exit);
			}
		});

		buttonShSetupSkip.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent activityIntent = new Intent(getBaseContext(), MainNavigationActivity.class);
				startActivity(activityIntent);
				finish();
				overridePendingTransition(R.animator.enter, R.animator.exit);
			}
		});

	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleHelper.onAttach(base));
	}
}
