package biz.teko.td.SHUB_APP.Cameras.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceScreen;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.lang.reflect.Field;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Cameras.Adapters.QualitiesListAdapter;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.Cameras.Quality;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;

/**
 * Created by td13017 on 28.02.2018.
 */

public class CameraSettingsFragment extends PreferenceFragment
{
	private String cameraId;
	private Context context;
	private Preference bindingPreference;
	private DBHelper dbHelper;
	private WebHelper webHelper;
	private Camera camera;
	private Preference qualityListPreference;



	public static CameraSettingsFragment newInstance(String id){
		CameraSettingsFragment cameraSettingsFragment = new CameraSettingsFragment();
		Bundle b = new Bundle();
		b.putString("cameraId", id);
		cameraSettingsFragment.setArguments(b);
		return cameraSettingsFragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		context = getActivity();
		dbHelper = DBHelper.getInstance(context);
		webHelper = WebHelper.getInstance();

		addPreferencesFromResource(R.xml.camera_settings_preferences);

		cameraId = getArguments().getString("cameraId");
		camera = dbHelper.getIVCameraById(cameraId);

		PreferenceScreen preferenceScreen = getPreferenceScreen();

		PreferenceCategory cameraSettingsCategory = new PreferenceCategory(context);
		cameraSettingsCategory.setTitle(R.string.CAMERA_SETTING_CAT_SETTINGS);
		preferenceScreen.addPreference(cameraSettingsCategory);

		Preference renamePreference = new Preference(context);
		renamePreference.setTitle(R.string.CAMERA_SETTINGS_RENAME);
		renamePreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
				camera = dbHelper.getIVCameraById(cameraId);
				AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
				adb.setPositiveButton(R.string.CAMERA_SETTINGS_REN, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{

					}
				});
				adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{

					}
				});

				final AlertDialog ad = adb.create();

				View view = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_camera_rename, null);
				LinearLayout dialogFrame = (LinearLayout) view.findViewById(R.id.cameraRenameDialogFrame);
//				dialogFrame.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Func.dpToPx(200, context)));
				final EditText editName = (EditText) view.findViewById(R.id.editCameraName);
				editName.setText(camera.name);
				editName.setCursorVisible(true);
				editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
				editName.isCursorVisible();
				try
				{
					Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
					f.setAccessible(true);
					f.set(editName, R.drawable.caa_cursor);
				} catch (Exception ignored)
				{
				}
				editName.setTextColor(context.getResources().getColor(R.color.md_black_1000));
				int editNameMaxLength = 32;
				editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
				editName.requestFocus();

				ad.setView(view);
				ad.show();

				ad.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						ad.dismiss();
					}
				});
				ad.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						String newName = editName.getText().toString();
						if((!newName.equals(""))&&(!newName.equals(camera.name))){
							webHelper.renameCamera(camera.id, newName, dbHelper.getUserInfoIVToken());
							ad.dismiss();
						}else{
							Func.pushToast(context, getString(R.string.CAMERA_MESSAGE_ENTER_NEW_NAME), (Activity)context);
						}
					}
				});

				return false;
			}
		});
		cameraSettingsCategory.addPreference(renamePreference);

		bindingPreference = new Preference(context);
		bindingPreference.setTitle(R.string.CAMERA_BIND_SET);
		cameraSettingsCategory.addPreference(bindingPreference);

		PreferenceCategory videoSettingsCategory = new PreferenceCategory(context);
		videoSettingsCategory.setTitle(R.string.CAMERA_SETTINGS_CATEGORY_STREAM);
		preferenceScreen.addPreference(videoSettingsCategory);

		qualityListPreference = new Preference(context);
		qualityListPreference.setTitle(R.string.CAMERA_SETTINGS_VIDEO_Q);
		String[] qualities = getResources().getStringArray(R.array.stream_qualities_status_list);
		qualityListPreference.setSummary(qualities[camera.quality + 1]);
		qualityListPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
		{
			@Override
			public boolean onPreferenceClick(Preference preference)
			{
//				BottomSheetForQuality bottomSheetForQuality = new BottomSheetForQuality().newInstance(cameraId);
//				bottomSheetForQuality.show(((CameraSettingsActivity)context).getSupportFragmentManager(), "tagQuality");


				final NBottomSheetDialog bottomSheetDialog =new NBottomSheetDialog(context);

				View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
				TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
				title.setText(R.string.CAMERAA_SET_QUALITY_TITLE);

				ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
				listView.setDivider(null);

				String[] qualities_status = getResources().getStringArray(R.array.stream_qualities_status_list);
				String[] qualities_value = getResources().getStringArray(R.array.stream_qualities_value_list);

				final Quality[] qualities = new Quality[qualities_status.length];
				for(int i = 0; i < qualities_status.length; i++ ){
					qualities[i] = new Quality(qualities_status[i], qualities_value[i]);
				}

				QualitiesListAdapter qualitiesListAdapter = new QualitiesListAdapter(context, R.layout.n_card_for_list, qualities, camera.quality);
				listView.setAdapter(qualitiesListAdapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id)
					{
						Quality quality = (Quality) parent.getItemAtPosition(position);
						camera.quality = Integer.valueOf(quality.value);
						dbHelper.setIVCameraQuality(camera.id, quality.value);
						setQSummary(quality.name);
						bottomSheetDialog.dismiss();
					}
				});

				bottomSheetDialog.setContentView(bottomView);

				try {
					Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
					mBehaviorField.setAccessible(true);

					final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
					behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
						@Override
						public void onStateChanged(@NonNull View bottomSheet, int newState) {
							if (newState == BottomSheetBehavior.STATE_DRAGGING) {
								behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
							}
						}

						@Override
						public void onSlide(@NonNull View bottomSheet, float slideOffset) {
						}
					});
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}

				bottomSheetDialog.show();

				return false;
			}
		});
		videoSettingsCategory.addPreference(qualityListPreference);

		CheckBoxPreference cameraSoundPreference = new CheckBoxPreference(context);
		cameraSoundPreference.setTitle(R.string.CAMERA_SETTINGS_AUDIO_SETTINGS);
		cameraSoundPreference.setSummaryOn(R.string.CAMERA_SETTINGS_AUDIO_ON);
		cameraSoundPreference.setSummaryOff(R.string.CAMERA_SETTIGNS_AUDIO_OFF);

		cameraSoundPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener()
		{
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue)
			{
				if((boolean) newValue){
					dbHelper.setIVCameraStreamSound(camera.id, true);
				}else{
					dbHelper.setIVCameraStreamSound(camera.id, false);
				}
				return true;
			}
		});
		videoSettingsCategory.addPreference(cameraSoundPreference);
		if(camera.audio.equals("both")){
			cameraSoundPreference.setChecked(true);
		}else{
			cameraSoundPreference.setChecked(false);
		}
	}

	public void setQSummary(String s){
		qualityListPreference.setSummary(s);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if(null==dbHelper.getZonesByIVCameraId(cameraId))
		{
			bindingPreference.setSummary(R.string.CAMERA_STATUS_UNBINDED);
		}else{
			bindingPreference.setSummary(R.string.CAMERA_STATUS_BINDED);

		}

	}
}
