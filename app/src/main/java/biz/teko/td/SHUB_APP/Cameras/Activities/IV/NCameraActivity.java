package biz.teko.td.SHUB_APP.Cameras.Activities.IV;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.ui.PlayerView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.Cameras.ExoPlayerVideoHandler;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;

/**
 * Created by td13017 on 15.09.2017.
 */

public class NCameraActivity extends AppCompatActivity
{
	private DBHelper dbHelper;
	private UserInfo userInfo;

	private Context context;

	private String url = "";
	private static final String baseURL = "https://openapi-alpha-eu01.ivideon.com/cameras/";
	private String cameraId = "";
	private final String url2 = "/live_stream?access_token=";
	private final String url3 = "&q=";
	private final String url4 = "&streams=";
	private final String url5 = "&format=hls";
	private String access_token = "";

	private Camera camera;
	private boolean exoFullScreen;
	private Dialog fullScreenDialog;
	private ImageView fullScreenIcon;
	private PlayerView playerView;
	private boolean destroyVideo = true;

	private NEventsListAdapter recordsAdapter;
	private ListView recordsList;
	private RelativeLayout cameraBindedLayout;

	private NWithAButtonElement emptyView;

	private final BroadcastReceiver recordsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{

			updateList();

		}
	};

	private void updateList()
	{
		Cursor cursor = updateCursor();
		if(null!=cursor && null!= recordsAdapter){
			recordsAdapter.update(cursor);
		}else{
			recordsAdapter = new NEventsListAdapter(context, R.layout.n_events_list_element, cursor,  0, false);
			if(null!=recordsList)recordsList.setAdapter(recordsAdapter);
		}
		if(null!=recordsAdapter && recordsAdapter.getCount() > 0){
			showEmptyFrame(View.GONE);
			recordsList.setVisibility(View.VISIBLE);
		}else{
			showEmptyNoRecordsFrame(View.VISIBLE);
			recordsList.setVisibility(View.GONE);
		}
	}

	private Cursor updateCursor()
	{
		return dbHelper.getRecordsCursorForCamera(camera.id);
	}

	private LinearLayout back;
	private TextView title;
	private LinearLayout settings;
	private ProgressBar progress;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);
		this.context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();
		access_token = dbHelper.getUserInfoIVToken();

		cameraId = getIntent().getStringExtra("cameraId");

		setup();
	}

	private void setup()
	{
		back = findViewById(R.id.nButtonClose);
		title = findViewById(R.id.nTextTitle);
		settings = findViewById(R.id.nButtonSettings);

		cameraBindedLayout = findViewById(R.id.cameraBindedLayout);
		recordsList = findViewById(R.id.recordsList);

		emptyView = findViewById(R.id.noElementsView);

		playerView = findViewById(R.id.nVideoViewExo);
		fullScreenIcon = findViewById(R.id.exo_fullscreen_icon);
		progress = findViewById(R.id.pb_search);
	}

	private void bind()
	{
		camera = dbHelper.getIVCameraById(cameraId);

		if(null!=settings){
			if((userInfo.roles& Const.Roles.DOMAIN_ADMIN) == 0)
			{
				settings.setVisibility(View.GONE);
			}else
			{
				settings.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						Intent intent = new Intent(getBaseContext(), NCameraSettingsActivity.class);
						intent.putExtra("cameraId", cameraId);
						startActivity(intent);
						overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
			}
		}

		if(null!=title) title.setText(camera.name);
		if(null!=back) back.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});
		if(null!=emptyView){
			emptyView.setOnChildClickListener(new NWithAButtonElement.OnChildClickListener()
			{
				@Override
				public void onChildClick(int action, boolean option)
				{
					Intent intent = new Intent(getBaseContext(), NCameraBindActivity.class);
					intent.putExtra("cameraId", camera.id);
					intent.putExtra("from", Const.OPEN_IV_FROM_CAMERA);
					startActivity(intent, ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle());
				}
			});
		}
		if(null!=cameraBindedLayout) {
			cameraBindedLayout.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent = new Intent(getBaseContext(), NCameraBindListActivity.class);
					intent.putExtra("cameraId", camera.id);
					intent.putExtra(Const.OPEN_IV_KEY_FROM, Const.OPEN_IV_FROM_CAMERA);
					startActivity(intent, ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle());
				}
			});
		}

		destroyVideo = true;
		url = getAddress();

		if(null!=fullScreenIcon)
		{
			fullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_enter_48_brand));
			if (url != null && playerView != null)
			{
				ExoPlayerVideoHandler.getInstance().prepareExoPlayerForUri(this, Uri.parse(url), playerView, new ExoPlayerVideoHandler.OnVideoListener()
				{
					@Override
					public void OnLoaded()
					{
						playerView.setVisibility(View.VISIBLE);
						if(null!=progress) progress.setVisibility(View.GONE);
					}
				}, null);
				fullScreenIcon.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						destroyVideo = false;
						Intent intent = new Intent(getBaseContext(), CameraFullScreenVideoActivity.class);
						intent.putExtra("url", url);
						startActivity(intent);
						overridePendingTransition(R.animator.from_down_enter, R.animator.from_down_exit);
					}
				});
			}
		}

		if(null!= dbHelper.getZonesByIVCameraId(cameraId)){
			cameraBindedLayout.setVisibility(View.VISIBLE);
			updateList();
		}else{
			showEmptyFrame(View.VISIBLE);
			cameraBindedLayout.setVisibility(View.GONE);
		}
	}

	private void showEmptyNoRecordsFrame(int visibility)
	{
		if(null!=emptyView){
			emptyView.setVisibility(visibility);
			emptyView.setTitle(getString(R.string.CAMERA_NO_ALERTS));
			emptyView.setVisibitilyAction(View.GONE);
		}
	}

	private void showEmptyFrame(int visibility)
	{
		if(null!=emptyView){
			emptyView.setVisibility(visibility);
			emptyView.setTitle(getString(R.string.CAMERA_NOT_BINDED_TEXT));
			if((userInfo.roles& Const.Roles.DOMAIN_ADMIN) == 0)
			{
				emptyView.setVisibitilyAction(View.GONE);
			}else{
				emptyView.setVisibitilyAction(View.VISIBLE);
			}
		}
	}

	private String getAddress()
	{
		String url = baseURL + cameraId + url2 + access_token;
		if(camera.quality != -1 ){
			url += url3 + camera.quality;
		}
		if(null!=camera.audio && !camera.audio.equals("both")){
			url += url4 + camera.audio;
		}
		return url;
	}

	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(recordsReceiver, new IntentFilter(D3Service.BROADCAST_NEW_RECORD));
		bind();
		//setRotationAnimation();
	}

	@Override
	public void onPause() {
		super.onPause();
		unregisterReceiver(recordsReceiver);

		if(destroyVideo)
		{
			ExoPlayerVideoHandler.getInstance().releaseVideoPlayer();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
	}

	@Override
	public void onDestroy(){
		super.onDestroy();
	}

}
