package biz.teko.td.SHUB_APP.HTTP;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public  interface WebApi
{
	@GET("log/?")
	Call<ResponseBody> setLogs(@Query("log") String log, @Query("token") String token);
}
