package biz.teko.td.SHUB_APP.D3DB;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

/**
 * Created by td13017 on 30.06.2016.
 */


// 26.09.2018 dont using now
// DIFFERENT ACTIVITIES WITH DB STARTING BY BROADCAST SIGNAL

public class DBNewEventReceiver extends BroadcastReceiver
{
	DBHelper dbHelper;
	@Override
	public void onReceive(Context context, Intent intent)
	{
		DBOldEventsDeleteTask dbOldEventsDeleteTask = new DBOldEventsDeleteTask(context);
		dbOldEventsDeleteTask.execute();
	}

	public class DBOldEventsDeleteTask extends AsyncTask<Object, Object, Void>
	{

		private Context context;

		public DBOldEventsDeleteTask(Context context){
			this.context=context;
		};

		@Override
		protected Void doInBackground(Object... params)
		{
//			Log.e("D3NewEventReceiver", "Delete old events");
			dbHelper = DBHelper.getInstance(context);
			dbHelper.deleteOldEvents();
			return null;
		}
	}
}
