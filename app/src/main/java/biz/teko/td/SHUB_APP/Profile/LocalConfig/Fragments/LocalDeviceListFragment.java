package biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.Fragments.ProgressDialogFragment;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Activities.LocalConfigNavigation;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Adapters.DeviceLocalConfigListAdapter;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.ILoadDevice;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Presenters.LocalDeviceListPresenter;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.UdpDevice;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;
import biz.teko.td.SHUB_APP.Utils.Callback;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDotsView;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

public class LocalDeviceListFragment extends Fragment implements DeviceLocalConfigListAdapter.OnDeviceClickListener, ILoadDevice {
    private LocalConfigNavigation activity;
    private SocketConnection socketConnection;

    private final DeviceLocalConfigListAdapter mAdapter = new DeviceLocalConfigListAdapter();
    private final HashMap<Integer, DeviceConfigAndIp> devices = new HashMap<>();
    private LocalDeviceListPresenter presenter;
    private ProgressDialogFragment progressDialog;
    private ProgressBar pBLoading;
    private TextView tVLoading;
    private TextView tVLoadingTimeout;
    private ImageView updateButton;

    private String deviceName, deviceSn;
    private ConnectivityManager connection;
    public LocalDeviceListFragment() {
        activity = (LocalConfigNavigation) getActivity();
        this.socketConnection = activity.getConnection();
    }
    public LocalDeviceListFragment(LocalConfigNavigation activity, SocketConnection socketConnection) {
        this.activity = activity;
        this.socketConnection = socketConnection;
    }

    //TODO: Нужна локализация

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        activity =(LocalConfigNavigation) getActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        activity.setOnBackPressedCallback(this::onBackPressed);
    }
    public void setSocketConnection(SocketConnection socketConnection) {
        this.socketConnection = socketConnection;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_profile_device_for_apn_select, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        SwipeRefreshLayout srl = (SwipeRefreshLayout) activity.findViewById(R.id.srl);
        srl.setOnRefreshListener(()->  {
            startConnection();
            startLoading();
            srl.setRefreshing(false);
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        connection = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        /*String apn1 = getIntent().getStringExtra("apn1");
        if (apn1 == null || apn1.trim().length() == 0) {
            apn1 = "internet";
        }
        String apn2 = getIntent().getStringExtra("apn2");
        if (apn2 == null || apn2.trim().length() == 0) {
            apn2 = "internet";
        }*/
    }
//TODO

    public void onBackPressed() {
        //TODO
        activity.finish();
        activity.overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }

    @Override
    public void onResume() {
        startLoading();
        startConnection();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (presenter != null)
            presenter.cancelSurvey();
    }


    private void initView() {
        RecyclerView mRecyclerView = (RecyclerView) activity.findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnDeviceClickListener(this);
        updateButton = (ImageView) activity.findViewById(R.id.iv_update_button);
        pBLoading = (ProgressBar) activity.findViewById(R.id.pb_search);
        tVLoading  = (TextView) activity.findViewById(R.id.tv_search);
        tVLoadingTimeout  = (TextView) activity.findViewById(R.id.tv_search_timeout);
        updateButton.setOnClickListener((v)->{
            startConnection();
            startLoading();
        } );
        NTopToolbarView toolbar = (NTopToolbarView) activity.findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);
    }


    private boolean checkWiFiConnection() {
        return connection.getActiveNetworkInfo() != null && connection.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }
    private void showNoDevicesFoundDialog(){
        NDialog dialog = new NDialog(activity, R.layout.n_dialog_question_repeat);
        dialog.setTitle(getResources().getString(R.string.ERROR_NO_LOCAL_DEVICES_FOUND));
        dialog.setOnActionClickListener((int value, boolean b ) -> {
            if  (value == NActionButton.VALUE_CANCEL) {
                dialog.dismiss();
            }
            else if  (value == NActionButton.VALUE_OK){
                dialog.dismiss();
                startLoading();
                startConnection();
            }
        });
        dialog.show();
    }

    private void startConnection() {
        if (checkWiFiConnection()) {
            if (presenter == null)
                presenter = new LocalDeviceListPresenter(socketConnection);
            presenter.setOnLoadComplete( ()-> {stopLoading(true);});
            presenter.startSearchingForDevices(this);
            presenter.startSurvey();
        } else {
            stopLoading(false);
            activity.showNoWifiDialog();
        }
    }
    @Override
    public void onDestroy() {
        if (presenter!= null)
            presenter.detach();
        presenter = null;
        super.onDestroy();
    }

    private void recyclerUpdate(HashMap<Integer, DeviceConfigAndIp> devices) {
        if (devices.size() == 0 && mAdapter.getItemCount() == 0)
            return;
        ArrayList<Device> _devices = new ArrayList<>();
        for (Map.Entry<Integer, DeviceConfigAndIp> device : devices.entrySet()) {
            _devices.add(new Device(device.getKey(), device.getValue().getConfig(),
                    device.getValue().getIp(), device.getValue().version));
        }
        activity.runOnUiThread(() -> mAdapter.setDevices(_devices));
    }

    @Override
    public void onDeviceClick(Device device) {
        enterPinAlertDialog(device);
    }

    @Override
    public void startConfigChooseActivity(String ip, int pin) {
        presenter.cancelSurvey();
        DeviceDTO deviceDTO = new DeviceDTO(ip, pin, deviceSn, deviceName);
        deviceDTO.setTestMode(true);
        activity.openDeviceConfig(deviceDTO);
    }

    @Override
    public void updateList(UdpDevice udpDevice) {
        Func.log_v(Const.LOCALCONFIG_LOG_TAG, udpDevice.serial + " " + udpDevice.config);
        if (!devices.containsKey(udpDevice.serial)) {
            devices.put(udpDevice.serial, new DeviceConfigAndIp(udpDevice.config, udpDevice.ip, Double.valueOf(udpDevice.version)));
            recyclerUpdate(devices);
        }
    }

    public void enterPinAlertDialog(Device device) {
        NDialog dialog = new NDialog(activity, R.layout.n_dialog_with_pin);
        dialog.setTitle("Security Hub" + " S/N " + device.account);
        dialog.setSubTitle(getResources().getString(R.string.LOCAL_CONFIG_PIN_DIALOG));

        NDotsView dotsView = dialog.findViewById(R.id.nPinDots);
        NEditText pinEditText = dialog.findViewById(R.id.editPin);
        AtomicReference<String> shared = new AtomicReference<>("");
        Func.setDotsPinEditText(pinEditText, shared, dotsView);
        Callback callback = () -> {
            String pin = shared.get();
            if (pin.length() > 3) {
                Func.hideEditTextKeyboard(pinEditText);
                presenter.sendSendSurveyRequestWithInterval(
                        LocalDeviceListPresenter.DEVICE_CONFIGURATION_REQUEST_TIMEOUT);
                //presenter.localConnectionSendSurveyRequest();
                presenter.sendTestModeOn(device, Integer.parseInt(pin));
                deviceName = "Security Hub";
                deviceSn = "S/N " + device.account;
                dialog.dismiss();
                pinEditText.setText("");
                dotsView.setCount(0);
            } else {
                Func.pushToast(activity, getResources().getString(R.string.PN_ENTER_ENTER_PN), activity);
            }
        };
        pinEditText.setOnEditorActionListener((v, actionId, event) -> {
            if(actionId == EditorInfo.IME_ACTION_DONE){
                callback.complete();
                return true;
            }
            return false;
        });
        dialog.setOnActionClickListener((value, b) -> {
            switch (value){
                case NActionButton.VALUE_CANCEL:
                    Func.hideEditTextKeyboard(pinEditText);
                    dialog.dismiss();
                    break;
                case NActionButton.VALUE_OK:
                    callback.complete();
                    break;
            }
        });
        dialog.show();
    }

    @Override
    public void showProgressBar() {
        progressDialog = new ProgressDialogFragment();
        progressDialog.setCancelable(false);
        progressDialog.show(getChildFragmentManager(), "progressBar");
    }

    public void stopLoading(boolean timeout ) {
        pBLoading.setVisibility(View.GONE);
        tVLoading.setVisibility(View.GONE);
        if (timeout) {
            if (devices.size() == 0) {
                showNoDevicesFoundDialog();
            }
            tVLoadingTimeout.setVisibility(View.VISIBLE);
        }
    }
    public void startLoading() {
        tVLoadingTimeout.setVisibility(View.GONE);
        pBLoading.setVisibility(View.VISIBLE);
        tVLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgressBar(boolean successful) {
        progressDialog.dismiss();

        if (!successful)
            Toast.makeText(activity, getResources().getString(R.string.D3_ERR_CONTRACTOR_NOT_FOUND), Toast.LENGTH_SHORT).show();
    }

    private static class DeviceConfigAndIp {
        private final int config;
        private final double version;
        private final String ip;

        public DeviceConfigAndIp(int config, String ip, double version) {
            this.config = config;
            this.ip = ip;
            this.version = version;
        }

        public int getConfig() {
            return config;
        }

        public String getIp() {
            return ip;
        }
    }

}
