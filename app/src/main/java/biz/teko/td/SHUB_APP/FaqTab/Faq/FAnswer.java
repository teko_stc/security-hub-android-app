package biz.teko.td.SHUB_APP.FaqTab.Faq;

import org.simpleframework.xml.Attribute;

/**
 * Created by td13017 on 11.02.2017.
 */

public class FAnswer
{
	@Attribute(name="id")
	public int id;

	@Attribute(name="caption")
	public String caption;

	@Attribute(name="link", required = false)
	public String link;
}
