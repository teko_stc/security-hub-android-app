package biz.teko.td.SHUB_APP.DevicesTabs.Activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.DevicesTabs.Adapters.DevicesViewPagerAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SitesTabs.SitesActivity;
import biz.teko.td.SHUB_APP.SlidingTabsLib.SlidingTabLayout;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class DevicesActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private int siteId;
	private String siteName;
	private CharSequence[] titles;
	private final int numoftabs = 2;
	private DevicesViewPagerAdapter adapter;
	private ViewPager pager;
	private SlidingTabLayout tabs;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private boolean fabShown  = false;
	private final int sending = 0;


	private final BroadcastReceiver connectionRepairReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int status = intent.getIntExtra("status", -1);
			if(status == 1)
			{
				setStatusConnected();
			}
		}
	};

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_devices);

		context = this;
		dbHelper = DBHelper.getInstance(this);

		siteId= getIntent().getIntExtra("site", -1);
		if(-1!=siteId)
			siteName = dbHelper.getSiteNameById(siteId);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(siteName);
		toolbar.setSubtitle(getString(R.string.DEVICES_TITLE));
		toolbar.setSubtitleTextColor(getResources().getColor(R.color.brandColorWhite));

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		// Creating The SitesViewPagerAdapter and Passing Fragment Manager, titles fot the Tabs and Number Of Tabs.
		titles = new CharSequence[]{getString(R.string.DEVICES_TITLE), getString(R.string.SUBTITLE_EVENTS)};
		adapter =  new DevicesViewPagerAdapter(getSupportFragmentManager(), titles, numoftabs, siteId);

		// Assigning ViewPager View and setting the adapter
		pager = (ViewPager) findViewById(R.id.devicesPager);
		pager.setAdapter(adapter);

		// Assiging the Sliding Tab Layout View
		tabs = (SlidingTabLayout) findViewById(R.id.devicesTabs);
		tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

		// Setting Custom ColorX for the Scroll bar indicator of the Tab View
		tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
			@Override
			public int getIndicatorColor(int position) {
				return getResources().getColor(R.color.tabsScrollColor);
			}
		});

		// Setting the ViewPager For the SlidingTabsLayout
		tabs.setViewPager(pager);
	}

	public void onDestroy()
	{
		super.onDestroy();
	}

	public void onStop()
	{
		super.onStop();
	}

	public void onStart()
	{
		super.onStart();
	}

	public void  onResume(){
		super.onResume();
		registerReceiver(connectionRepairReceiver, new IntentFilter(D3Service.BROADCAST_CONNECTION_STATUS_CHANGE));
		bindD3();
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(connectionRepairReceiver);
		unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
				setStatusConnected();
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	public boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	}


	private void setStatusConnected(){
		if(null!=myService)
		{
			final RelativeLayout noConnLayout = (RelativeLayout) findViewById(R.id.noConnLayout);
			if (myService.getStatus())
			{
				if (null != noConnLayout) noConnLayout.setVisibility(View.GONE);
				pager.setVisibility(View.VISIBLE);
			} else
			{
				if (null != noConnLayout) noConnLayout.setVisibility(View.VISIBLE);
				pager.setVisibility(View.GONE);
			}
		}
	}


	public void setFabStatus(boolean status){
		fabShown = status;
	}

	@Override
	public void onBackPressed()
	{
		if(fabShown){
			adapter.closeFab();
		}else
		{
//			super.backPressed();
			Intent intent = new Intent(getBaseContext(), SitesActivity.class);
			startActivity(intent);
			overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);

		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
	}

}
