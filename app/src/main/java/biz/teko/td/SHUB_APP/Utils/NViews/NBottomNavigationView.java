package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import biz.teko.td.SHUB_APP.R;

public class NBottomNavigationView extends BottomNavigationView
{
	private static final int[] STATE_INVERTED = {R.attr.state_invert};
	private static final int[] STATE_ALARM = {R.attr.state_alarm};
	private static final int[] STATE_ATTENTION = {R.attr.state_attention};

	private final Context context;
	private boolean inverted;
	private boolean alarm;
	private boolean attention;

	public NBottomNavigationView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setupView();
		bindView();
	}

	private void bindView()
	{
		setInverted(inverted);
	}

	private void setupView()
	{

	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FrameState, 0 , 0);
		try
		{
			inverted = a.getBoolean(R.styleable.FrameState_state_invert, false);
		}finally
		{
			a.recycle();
		}
	}



	public void setInverted(boolean inverted){
		this.inverted = inverted;
		refreshDrawableState();
	}

	public void setAlarm(boolean alarm){
		this.alarm = alarm;
		refreshDrawableState();
	}

	public void setAttention(boolean attention){
		this.attention = attention;
		refreshDrawableState();
	}



	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 3);
		if(inverted) mergeDrawableStates(drawableState, STATE_INVERTED);
		if(alarm) mergeDrawableStates(drawableState, STATE_ALARM);
		if(attention) mergeDrawableStates(drawableState, STATE_ATTENTION);
		return drawableState;
	}
}
