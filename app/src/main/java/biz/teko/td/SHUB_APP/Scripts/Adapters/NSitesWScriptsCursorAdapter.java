package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;

public class NSitesWScriptsCursorAdapter extends RecyclerView.Adapter
{
	private final DBHelper dbHelper;

	private Cursor cursor;

	private NSiteScriptsCursorAdapter.OnElementClickListener onElementClickListener;

	public void setOnElementClickListener(NSiteScriptsCursorAdapter.OnElementClickListener onElementClickListener){
		this.onElementClickListener = onElementClickListener;
	}

//	@NonNull
//	@Override
//	public ScriptAndSitesVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//		return ScriptAndSitesVH.newInstance(parent);
//	}
//
//	@Override
//	public void onBindViewHolder(@NonNull ScriptAndSitesVH holder, int position) {
//		cursor.moveToPosition(position);
//		Site site =new Site(cursor);
//
//		if(null!=site){
//			TextView nameText = (TextView) holder.itemView.findViewById(R.id.nTextTitle);
//			nameText.setText(site.name);
//
//			RecyclerView scriptsList = holder.itemView.findViewById(R.id.nListScripts);
//			Cursor c = dbHelper.getDeviceScriptsCursorBySiteId(site.id);
//			if(null!=c && c.getCount() > 0 && null!=scriptsList){
//				NSiteScriptsCursorAdapter scriptsCursorAdapter =
//						new NSiteScriptsCursorAdapter(holder.itemView.getContext(),c);
//				scriptsCursorAdapter.setOnElementClickListener((device_id, script_id) -> {
//					if(null!=onElementClickListener) onElementClickListener.onClick(device_id, script_id);
//				});
//				scriptsList.setAdapter(scriptsCursorAdapter);
//				//Func.setDynamicHeight(scriptsList);
//			}
//
//		}
////		TextView bindText = (TextView) view.findViewById(R.id.scriptBind);
////		TextView enableText = (TextView) view.findViewById(R.id.scriptEnable);
////
////		final Script deviceScript = new Script(cursor);
////		if(null!=deviceScript){
////			nameText.setText(deviceScript.name);
////			bindText.setText(dbHelper.getZoneNameById(deviceScript.device, deviceScript.bind));
////			enableText.setText(deviceScript.enabled == 1? context.getString(R.string.SCRIPTS_STATEMENT_ACTIVE):context.getString(R.string.SCRIPT_STATEMENT_LOAD_IN_DEVICE));
////		}
////		view.setOnClickListener(new View.OnClickListener()
////		{
////			@Override
////			public void onClick(View view)
////			{
////				ScriptsListActivity scriptsListActivity = (ScriptsListActivity) context;
////				scriptsListActivity.setSelScript(deviceScript.id);
////				scriptsListActivity.getLibraryScripts(false, deviceScript.device);
////
////			}
////		});
//	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		return null;
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
	{

	}

	@Override
	public int getItemCount() {
		return cursor.getCount();
	}

	public interface OnElementClickListener{
		void onClick(int id);
	}

	public NSitesWScriptsCursorAdapter(Context context,  Cursor cursor)
	{
		super();
		this.dbHelper = DBHelper.getInstance(context);
		this.cursor = cursor;
	}


	public void update(Cursor cursor)
	{
		this.cursor = (cursor);
	}
}
