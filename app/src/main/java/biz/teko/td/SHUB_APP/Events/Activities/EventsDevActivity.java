package biz.teko.td.SHUB_APP.Events.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapter;
import biz.teko.td.SHUB_APP.R;

public  class EventsDevActivity extends BaseActivity
{
	private FrameLayout filterFrame;
	private boolean visible;
	private FrameLayout nInputFrame;
	private boolean animationEnd;

	private NEventsListAdapter eventAdapter;
	private Context context;
	private AdapterView eventList;
	private DBHelper dbHelper;
	private LinearLayout back;

	private BroadcastReceiver eventReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};
	private int site_id;
	private int device_id;
	private TextView textEmpty;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_events_dev);
		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);

		setup();
		bind();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(eventReceiver);
	}

	private void setup()
	{
//		filterFrame = (FrameLayout) findViewById(R.id.nFilterFrame);
//		nInputFrame = (FrameLayout) findViewById(R.id.nInputFrame);

		eventList = (ListView) findViewById(R.id.nListEvents);
		textEmpty = (TextView) findViewById(R.id.nTextEmpty);

		back = (LinearLayout) findViewById(R.id.nButtonBack);
	}

	private void bind()
	{
		site_id = getIntent().getIntExtra("site", -1);
		device_id = getIntent().getIntExtra("device", -1);

		updateList();

		if(null!=back)back.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});
	}

	private void updateList()
	{
		Cursor cursor = updateCursor();
		if(null!=cursor && null!=eventAdapter)
		{
			eventAdapter.update(cursor);
		}else{
			eventAdapter = new NEventsListAdapter(context, R.layout.n_events_list_element, cursor, 0, false);;
			if(null!=eventList) eventList.setAdapter(eventAdapter);
		}
		if (null!=eventAdapter && 0!= eventAdapter.getCount())
		{
			if(null!=eventList)eventList.setVisibility(View.VISIBLE);
			if(null!=textEmpty) textEmpty.setVisibility(View.GONE);
		} else
		{
			if(null!=eventList)eventList.setVisibility(View.GONE);
			if(null!=textEmpty)textEmpty.setVisibility(View.VISIBLE);
		}

	}

	private Cursor updateCursor()
	{
		return  dbHelper.getDebugEventsCursor(site_id, device_id);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}
}
