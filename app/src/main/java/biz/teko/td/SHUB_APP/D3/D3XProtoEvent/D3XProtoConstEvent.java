package biz.teko.td.SHUB_APP.D3.D3XProtoEvent;

import android.content.Context;
import android.content.res.Resources;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.InputStream;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 05.08.2016.
 */
public class D3XProtoConstEvent
{
	@ElementList(name = "classes", entry = "class")
	public LinkedList<EClass> classes;

	@ElementList(name = "physics", entry = "physic", required = false)
	public LinkedList<DPhysic> physics;

	@ElementList(name = "detectortypes", entry = "detectortype", required = false)
	public LinkedList<DType> detectortypes;

	@ElementList(name = "alarmtypes", entry = "alarmtype", required = false)
	public LinkedList<AType> alarmtypes;

	@ElementList(name = "sectiontypes", entry = "sectiontype", required = false)
	public LinkedList<SType> sectiontypes;

	@ElementList(name = "zonetypes", entry = "zonetype", required = false)
	public LinkedList<ZType> zonetypes;

	@ElementList(name = "inputtypes", entry = "inputtype", required = false)
	public LinkedList<DWIType> dwitypes;

	@ElementList(name = "outputtypes", entry = "outputtype", required = false)
	public LinkedList<DWOType>  dwotypes;

	@ElementList(name = "iotypes", entry = "iotype", required = false)
	public LinkedList<IOType>  iotypes;

	@ElementList(name = "devicetypes", entry = "devicetype", required = false)
	public LinkedList<DeviceType>  devicetypes;


	public EClass getClassById(int id){
		LinkedList<EClass> classes = this.classes;
		for(EClass eClass:classes){
			if(eClass.id == id){
				return  eClass;
			}
		}
		return null;
	}
	public DPhysic getPhysicById(int id){
		LinkedList<DPhysic> physics = this.physics;
		for(DPhysic physic:physics){
			if(physic.id == id){
				return  physic;
			}
		}
		return null;
	}
	public DType getDetectorType(int id){
		LinkedList<DType> detectortypes = this.detectortypes;
		for(DType dType :detectortypes){
			if(dType.id == id){
				return dType;
			}
		}
		return null;
	}

	public AType getAlarmType(int id){
		LinkedList<AType> alarmtypes = this.alarmtypes;
		for(AType aType :alarmtypes){
			if(aType.id == id){
				return aType;
			}
		}
		return null;
	}


	public SType getSTypeById(int id){
		LinkedList<SType> sTypes = this.sectiontypes;
		for(SType sType:sTypes){
			if(sType.id == id){
				return  sType;
			}
		}
		return null;
	}

	public ZType getZTypeById(int id){
		LinkedList<ZType> zTypes = this.zonetypes;
		for(ZType zType:zTypes){
			if(zType.id == id){
				return  zType;
			}
		}
		return null;
	}

	public DeviceType getDeviceTypeById(int id){
		LinkedList<DeviceType> deviceTypes = this.devicetypes;
		for(DeviceType deviceType:deviceTypes){
			if(deviceType.id == id){
				return  deviceType;
			}
		}
		return null;
	}

	//get from xml
	public static D3XProtoConstEvent load(Context context){
		Resources resources = context.getResources();
		InputStream inputStream = resources.openRawResource(R.raw.x_proto_event_descript);
		Serializer serializer = new Persister();
		try
		{
			return serializer.read(D3XProtoConstEvent.class, inputStream);
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}


	public String getDescription(int classId, int detectorId, int reasonId, int active) {
		EClass eClass = getClassById(classId);
		if (null != eClass) {
			EDetector eDetector = eClass.getDetectorById(detectorId);
			if (null != eDetector) {
				EReason eReason = eDetector.getReasonById(reasonId);
				if (null != eReason) {
					EStatement eStatement = eReason.getStatementById(active);
					if (eStatement != null)
						return eStatement.description;
				}
			}
			EReason eReason = eClass.getReasonById(reasonId);
			if (null != eReason) {
				EStatement eStatement = eReason.getStatementById(active);
				if (eStatement != null)
					return eStatement.description;
			}
		}
		return "";
	}

	public String getAffectIconBig(int classId, int detectorId, int reasonId ){
		EClass eClass = getClassById(classId);
		if(null!=eClass){
			EDetector eDetector = eClass.getDetectorById(detectorId);
			if(null!=eDetector){
				EReason eReason = eDetector.getReasonById(reasonId);
				if(null!=eReason){
					return eReason.iconBig;
				}
			}
			EReason eReason = eClass.getReasonById(reasonId);
			if(null!=eReason){
				return eReason.iconBig;
			}
		}
		return "@drawable/ic_question_grey_500_24dip";
	}

	public DWIType getDWIType(int setZoneType)
	{
		LinkedList<DWIType> dwiTypes = this.dwitypes;
		for(DWIType dwiType:dwiTypes){
			if(dwiType.id == setZoneType){
				return  dwiType;
			}
		}
		return null;
	}

	public DWOType getDWOType(int setZoneType)
	{
		LinkedList<DWOType> dwoTypes = this.dwotypes;
		for(DWOType dwoType:dwoTypes){
			if(dwoType.id == setZoneType){
				return  dwoType;
			}
		}
		return null;
	}

	public DWOType getDWOTypeByDetector(int setZoneDetector)
	{
		LinkedList<DWOType> dwoTypes = this.dwotypes;
		for(DWOType dwoType:dwoTypes){
			if(dwoType.detector == setZoneDetector){
				return  dwoType;
			}
		}
		return null;
	}
}
