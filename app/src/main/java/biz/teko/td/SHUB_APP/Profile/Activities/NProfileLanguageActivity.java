package biz.teko.td.SHUB_APP.Profile.Activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.LocaleD3;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Profile.Adapters.NLanguageListAdapter;
import biz.teko.td.SHUB_APP.Profile.Models.Language;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 08.05.2018.
 */

public class NProfileLanguageActivity extends BaseActivity
{
    private Context context;
    private ServiceConnection serviceConnection;
    private D3Service myService;
    private boolean bound;
    private int sending = 0;
    private DBHelper dbHelper;
    private boolean changeDomain = false;
    private SharedPreferences sharedPreferences;
    private LinearLayout backButton, dotsButton;
    private TextView titleToolbar;
    private LinearLayout close;
    private LinearLayout more;
    private TextView title;
    private ListView languageList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_profile_language);
        this.context = this;
        this.dbHelper = DBHelper.getInstance(context);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);


        setup();
        bind();
    }

    private void bind()
    {
        if(null!=title) title.setText(getResources().getString(R.string.PROF_LANG_TITLE));
        if(null!=close) close.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });
        if(null!=more)more.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*show domain lang dialog*/
                PopupMenu popupMenu = new PopupMenu(context, v);
                popupMenu.getMenuInflater().inflate(R.menu.language_menu, popupMenu.getMenu());
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
                {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {
                        switch (item.getItemId()) {
                            case R.id.language_domain_setting:
                                UserInfo userInfo = dbHelper.getUserInfo();
                                String curLang = PrefUtils.getInstance(context).getCurrentLang();
                                if((userInfo.roles & Const.Roles.DOMAIN_ADMIN)!=0){
                                    if(userInfo.locale.equals(curLang)){
                                        openChangeDomainLangDialog(getString(R.string.DOMAIN_LANG_DIALOG_TITLE), getString(R.string.DOMAIN_LANG_DIALOG_SAME));
                                    }else
                                    {
                                        openChangeDomainLangDialog(getString(R.string.DOMAIN_LANG_DIALOG_TITLE), getString(R.string.DOMAIN_LANG_DIALOG_DIFF));
                                    }
                                }else{
                                    Func.nShowMessage(context, getString(R.string.DOMAIN_LANG_DIALOG_NO_RULES));
                                }
                                return true;
                            default:
                                return false;
                        }
                    }
                });
                popupMenu.show();
            }
        });

        if(null!=languageList){
            String curLang = PrefUtils.getInstance(context).getCurrentLang();
            final String[] lang_name = getResources().getStringArray(R.array.pref_language_select);
            String[] lang_value = getResources().getStringArray(R.array.pref_language_values);

            final Language[] languages = new Language[lang_name.length];
            for(int i = 0; i < languages.length; i++ ){
                languages[i] = new Language(lang_name[i], lang_value[i]);
            }
            NLanguageListAdapter NLanguageListAdapter = new NLanguageListAdapter(context, R.layout.n_lang_list_element, languages, curLang);
            NLanguageListAdapter.setOnElementClickListener(new NLanguageListAdapter.OnElementClickListener()
            {
                @Override
                public void onClick(Language language)
                {
                    if(!curLang.equals(language.value)){
                        UserInfo userInfo = dbHelper.getUserInfo();
                        if (((userInfo.roles & Const.Roles.DOMAIN_ADMIN) != 0))
                        {
                            NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
                            nDialog.setTitle(getResources().getString(R.string.DOMAIN_LANG_QUESTION));
                            nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
                            {
                                @Override
                                public void onActionClick(int value, boolean b)
                                {
                                    switch (value){
                                        case NActionButton.VALUE_OK:
                                            changeDomain = true;
                                            nDialog.dismiss();
                                            changeLanguage(language);
                                            break;
                                        case NActionButton.VALUE_CANCEL:
                                            nDialog.dismiss();
                                            changeLanguage(language);
                                            break;
                                    }
                                }
                            });
                            nDialog.show();
                        }else{
                            changeLanguage(language);
                        }
                    }else{
                        changeLanguage(language);
                    }
                }
            });
            languageList.setAdapter(NLanguageListAdapter);
        }
    }

    private void setup()
    {
        close = (LinearLayout) findViewById(R.id.nButtonBack);
        more = (LinearLayout) findViewById(R.id.nButtonMore);
        title = (TextView) findViewById(R.id.nTextTitle);
        languageList = (ListView) findViewById(R.id.nLanguageList);

    }

    private void changeLanguage(Language language)
    {
        LocaleHelper.setLocale(getBaseContext(), language.value);
        PrefUtils.getInstance(context).setCurrentLang(language.value);

        Func.log_d(D3Service.LOG_TAG , "Current lang: " + PrefUtils.getInstance(context).getCurrentLang());
        Func.updateContext(context); // уже есть в Func.startD3Service, а он перезапускается далее асинхронно
        new asynTask1(NProfileLanguageActivity.this, language.value, changeDomain).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.language_menu, menu);
        return true;
    }

    private void openChangeDomainLangDialog(String title, String message)
    {
        NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
        nDialog.setTitle(message);
        nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
        {
            @Override
            public void onActionClick(int value, boolean b)
            {
                switch (value){
                    case NActionButton.VALUE_OK:
                        Intent intent = new Intent(getBaseContext(), NProfileDomainLanguageAcitivity.class);
                        startActivityForResult(intent, D3Service.CHOOSE_LANGUAGE);
                        overridePendingTransition(R.animator.enter, R.animator.exit);
                        nDialog.dismiss();
                        break;
                    case NActionButton.VALUE_CANCEL:
                        nDialog.dismiss();
                        break;
                }
            }
        });
        nDialog.show();
    }

    public class asynTask1 extends AsyncTask<String, Void, Boolean>
    {

        private String lang;
        private boolean changeDomain;
        private NDialog progressDialog;
        private Timer timer;
        private boolean wait;

        public asynTask1(NProfileLanguageActivity activity, String lang, boolean changeDomain)
        {
            context = activity;
            this.lang = lang;
            this.changeDomain = changeDomain;
        }

        @Override
        protected void onPostExecute(Boolean result)
        {

            super.onPostExecute(result);
            if(wait){
                wait = false;
                if(null!=timer)timer.cancel();
                progressDialog.dismiss();
            }
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            onBackPressed();
            overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
        }

        @Override
        protected void onPreExecute()
        {

            super.onPreExecute();
            progressDialog = new NDialog(context, R.layout.n_dialog_progress);
            progressDialog.setTitle(getResources().getString(R.string.DIALOG_WAIT_TITLE));
            progressDialog.show();
            wait = true;
            timer = new Timer();
            TimerTask timerTask = new TimerTask()
            {
                public void run()
                {
                    wait = false;
                    if(null!=progressDialog)progressDialog.dismiss();
                }
            };
            timer.schedule(timerTask, 10000);
        }

        @Override
        protected Boolean doInBackground(String... params)
        {

            Func.restartD3ServiceForced(context, "ProfileLangActivity");
//            Func.startD3Service(context, "ProfileLangActivity");
//            ((App) getApplication()).newContext();
            if(changeDomain)
            {
                UserInfo userInfo = dbHelper.getUserInfo();
                if (!((userInfo.roles & Const.Roles.DOMAIN_ADMIN) == 0))
                {
                    LocaleD3 locale = dbHelper.getLocale(lang);
                    if (null != locale)
                    {
                        JSONObject jsonObject = new JSONObject();
                        try
                        {
                            jsonObject.put("locale_id", locale.id);
                            nSendMessageToServer(null, "SET_LOCALE", jsonObject, false);
                        } catch (JSONException e)
                        {
                            e.printStackTrace();
                        }
                    }
                }
            }
            return null;
        }
    }

    public void  onResume(){
        super.onResume();

        bindD3();
    }

    public void onPause(){
        super.onPause();
        if(bound)unbindService(serviceConnection);
    }

    private void bindD3(){
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = getServiceConnection();
        bindService(intent, serviceConnection, 0);
    }

    private ServiceConnection getServiceConnection(){
        return new ServiceConnection()
        {
            public void onServiceConnected(ComponentName name, IBinder binder)
            {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name)
            {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
    }

    private D3Service getLocalService()
    {
        if(bound){
            return myService;
        }else{
            bindD3();
            if(null!=myService){
                return myService;
            }
            Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
        }
        return null;

    }

    private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
    {
        return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
    };
    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }
}
