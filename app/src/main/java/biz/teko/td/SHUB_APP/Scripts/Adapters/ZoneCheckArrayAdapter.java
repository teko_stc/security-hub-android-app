package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;

public class ZoneCheckArrayAdapter extends ArrayAdapter
{
	private final Context context;
	private final Zone[] zones;
	private final LayoutInflater layoutInflater;


	public ZoneCheckArrayAdapter(Context context, int resource, Zone[] zones)
	{
		super(context, resource);
		this.context = context;
		this.zones = zones;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.card_for_section_zone_check, parent, false);
		}
		final Zone zone = zones[position];
		TextView zoneName = (TextView) view.findViewById(R.id.sectionName);
		TextView zoneId = (TextView) view.findViewById(R.id.sectionId);
		zoneName.setText(zone.name);
		zoneId.setText(context.getString(R.string.ZONES_CHECK_LIST_SUBTITILE)+ " " +  zone.id);

		CheckBox zoneCheck = (CheckBox) view.findViewById(R.id.sectionCheck);

		zoneCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				if(isChecked){
					zone.checked = true;
				}else{
					zone.checked = false;
				}
			}
		});

		if(zone.checked){
			zoneCheck.setChecked(true);
		}else{
			zoneCheck.setChecked(false);
		}
		return view;
	}

	public Zone[] getZones(){
		return zones;
	}

	public int getCount() {
		return zones.length;
	}

	public Zone getItem(int position){
		return zones[position];
	}

	public long getItemId(int position){
		return position;
	}
}
