package biz.teko.td.SHUB_APP.MainTabs.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.NPresetValueButton;

public class SectionInGroupSelectRecyclerAdapter extends RecyclerView.Adapter<SectionInGroupSelectRecyclerAdapter.SectionInGroupVH> {
	private final Context context;
	private final Section[] sections;
	private final boolean shared;
	private final List<Site> sites;
	private OnSectionSelectlistener onSectionSelectListener;
	private Site curSite;

	public SectionInGroupSelectRecyclerAdapter(@NonNull Context context, Section[] sections, OnSectionSelectlistener onSectionSelectListener, boolean shared, List<Site> sites) {

		//super(context, layout);
		this.context = context;
		this.sections = sections;
		this.onSectionSelectListener = onSectionSelectListener;
		this.shared = shared;
		this.sites = sites;
	}

	public Site getCurSite() {
		return curSite;
	}

	public void setCurSite(Site curSite) {
		if (curSite == null) {
			for (Section section : sections) {
				section.checked = false;
			}
			notifyDataSetChanged();
		}
		this.curSite = curSite;
	}

	public void setOnSectionSelectListener(OnSectionSelectlistener onSectionSelectListener) {
		this.onSectionSelectListener = onSectionSelectListener;
	}

	public Section getItem(int position) {
		return sections[position];
	}

	@NonNull
	@Override
	public SectionInGroupVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return SectionInGroupVH.newInstance(parent);
	}

	@Override
	public void onBindViewHolder(@NonNull SectionInGroupVH holder, int position) {

		NPresetValueButton elementView = holder.itemView.findViewById(R.id.nPresetButton);
		if (null != elementView) {
			final Section section = sections[position];
			elementView.setTitle(section.name);
			SType sType = section.getType();
			if (null != sType) {
				elementView.setSubTitle(sType.caption);
				elementView.setImage(sType.getListIcon(context));
			} else {
				elementView.setImage(R.drawable.n_image_section_common_list_selector);
			}
			elementView.setUncheckable(true);
			elementView.setChecked(section.checked);
			TextView siteTv = elementView.findViewById(R.id.text_view_site);
			if (shared) {
				if (curSite != null && curSite.id != sites.get(position).id) {
					//elementView.setChecked(true);
					siteTv.setActivated(true);
					elementView.getImageSwitcher().setSelected(true);
					elementView.getSubTitleTextView().setActivated(true);
					elementView.getTitleTextView().setActivated(true);
				} else {
					siteTv.setActivated(false);
					elementView.getImageSwitcher().setSelected(false);
					elementView.getSubTitleTextView().setActivated(false);
					elementView.getTitleTextView().setActivated(false);
				}

				siteTv.setTextColor(context.getResources().getColorStateList(R.color.n_site_select_text_color_selector));
				siteTv.setVisibility(View.VISIBLE);
				siteTv.setText(sites.get(position).name);
			}

			elementView.setOnClickListener(view1 -> {
				section.checked = view1.isSelected();
				if (null != onSectionSelectListener)
					onSectionSelectListener.callback(sections, view1, position);
			});
		}
	}

	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return sections.length;
	}

	public Section[] getSections() {
		return this.sections;
	}

	public interface OnSectionSelectlistener {
		void callback(Section[] sections, View view, int position);
	}

	public static class SectionInGroupVH extends RecyclerView.ViewHolder {
		private SectionInGroupVH(@NonNull View itemView) {
			super(itemView);
		}

		public static SectionInGroupVH newInstance(ViewGroup parent) {
			return new SectionInGroupVH(LayoutInflater.from(parent.getContext())
					.inflate(R.layout.n_group_sections_set_list_element, parent, false));
		}
	}
}
