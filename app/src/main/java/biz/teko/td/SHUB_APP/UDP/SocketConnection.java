package biz.teko.td.SHUB_APP.UDP;

import android.net.DhcpInfo;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import biz.teko.td.SHUB_APP.Profile.LocalConfig.UdpDevice;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.ReplyConfig;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.ReplyConfig1h;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.ReplyConfig1hNg;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.ReplyConfigNg;
import biz.teko.td.SHUB_APP.UDP.Entities.RequestConfig;
import biz.teko.td.SHUB_APP.UDP.Entities.RequestReset;
import biz.teko.td.SHUB_APP.UDP.Entities.RequestSurvey;
import biz.teko.td.SHUB_APP.UDP.Entities.SendConfig;
import biz.teko.td.SHUB_APP.UDP.Entities.TestMode;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;
import biz.teko.td.SHUB_APP.Utils.Callback;
import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class SocketConnection {

	private static final int PORT = 22239;
	private static final int BUFFER_SIZE = 800;

	private static final Charset UTF8_CHARSET = StandardCharsets.UTF_8;
	private static final Charset UCS_CHARSET = Charset.forName("UNICODE");
	private static final Charset CP1251_CHARSET = Charset.forName("Windows-1251");
	private static final Charset LATIN_CHARSET = StandardCharsets.ISO_8859_1;
	private static final Charset ASCII_CHARSET = StandardCharsets.US_ASCII;
	private static ReplyConfig config;
	private static ReplyConfigNg configNg;
	private static ReplyConfig1h config1h;
	private static ReplyConfig1hNg config1hNg;
	private final PublishSubject<UdpDevice> deviceSubject = PublishSubject.create();
	private final PublishSubject<Boolean> sendConfigSubject = PublishSubject.create();
	private final int ip;
	private final DhcpInfo dhcp;
	private PublishSubject<HashMap<String, String>> configSubject = PublishSubject.create();
	private Observable<UdpData> udpDataObservable;
	private Disposable replyDisposable;
	private DatagramSocket udpSocket;

	public SocketConnection(DhcpInfo dhcp) {
		this.ip = dhcp.ipAddress;
		this.dhcp = dhcp;
	}

	public static byte[] deleteZeros(byte[] bytes) {
		List<Byte> list = new ArrayList<>();
		for (byte aByte : bytes) list.add(aByte);
		list.removeAll(Arrays.asList((byte) 0));
		bytes = new byte[list.size()];
		for (int i = 0; i < list.size(); i++)
			bytes[i] = list.get(i);
		return bytes;
	}

	public static String decodeUTF8(byte[] bytes) {
		return new String(deleteZeros(bytes), UTF8_CHARSET);
	}

	public static String decodeUNICODE(byte[] bytes) {
		return new String(bytes, UCS_CHARSET);
	}

	public static String decodeCP1251(byte[] bytes) {
		return new String(bytes, CP1251_CHARSET);
	}

	public static String decodeLatin(byte[] bytes) {
		return new String(bytes, LATIN_CHARSET);
	}

	public static String decodeASCIIn(byte[] bytes) {
		return new String(bytes, ASCII_CHARSET);
	}

	public static int byteArrayToInt(byte[] b) {
		if (b.length == 8) {
			return b[0] << 56 | (b[1] & 0xff) << 48 | (b[2] & 0xff) << 40 | (b[3] & 0xff) << 32 | b[4] << 24 | (b[5] & 0xff) << 16 | (b[6] & 0xff) << 8 | (b[7] & 0xff);
		} else {
			if (b.length == 4)
				return b[0] << 24 | (b[1] & 0xff) << 16 | (b[2] & 0xff) << 8 | (b[3] & 0xff);
			else if (b.length == 2)
				return 0x00 << 24 | 0x00 << 16 | (b[0] & 0xff) << 8 | (b[1] & 0xff);
		}
		return 0;
	}

	public static byte[] leIntToByteArray(int i) {
		final ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putInt(i);
		return bb.array();
	}

	public static byte[] leShortToByteArray(short i) {
		final ByteBuffer bb = ByteBuffer.allocate(Short.SIZE / Byte.SIZE);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putShort(i);
		return bb.array();
	}

	public static byte[] invertByte(byte[] bytes) {
		for (int i = 0; i < bytes.length / 2; i++) {
			byte temp = bytes[i];
			bytes[i] = bytes[bytes.length - i - 1];
			bytes[bytes.length - i - 1] = temp;
		}
		return bytes;
	}

	public ReplyConfigNg getConfigNg() {
		return configNg;
	}

	public ReplyConfig getConfig() {
		return config;
	}

	public ReplyConfig1h getConfig1h() {
		return config1h;
	}

	public ReplyConfig1hNg getConfig1hNg() {
		return config1hNg;
	}

	public PublishSubject<Boolean> getSendConfigSubject() {
		return sendConfigSubject;
	}

	public PublishSubject<UdpDevice> getDeviceSubject() {
		return deviceSubject;
	}

	public Subject<HashMap<String, String>> getConfigSubject() {
		return configSubject;
	}

	private Observable<UdpData> createUdpSubject(DatagramSocket udpSocket) {
		return Observable.create(
				emitter -> {
					try {
						while (!emitter.isDisposed()) {
							byte[] rcvBuffer = new byte[SocketConnection.BUFFER_SIZE];
							DatagramPacket datagramPacket = new DatagramPacket(rcvBuffer, rcvBuffer.length);
							udpSocket.receive(datagramPacket);
							emitter.onNext(new UdpData(datagramPacket, rcvBuffer));
						}
					} catch (Throwable th) {
						if (!emitter.isDisposed())
							emitter.onError(th);
					}
				}
		);
	}

	public void initialize() {
		try {
			udpSocket = new DatagramSocket(null);
			System.err.println("NEW SOCKET");
			udpSocket.setBroadcast(true);
			udpSocket.setReuseAddress(true);

			udpSocket.bind(new InetSocketAddress(PORT));
			udpDataObservable = createUdpSubject(udpSocket);
			configSubject = PublishSubject.create();
			replyDisposable = receive();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		replyDisposable.dispose();
		udpSocket.close();
	}

	public boolean isClosed() {
		return udpSocket.isClosed();
	}

	public boolean isConnected() {
		return udpSocket.isConnected();
	}

	private Disposable receive() {
		return udpDataObservable
				.subscribeOn(Schedulers.io())
				.subscribe(udpData -> {
					DatagramPacket receiveData = udpData.datagramPacket;
					byte[] start = udpData.bytes;
					if ((start[0] & 0xff) == 255 && receiveData.getLength() > 10) {
						InetAddress IPAddress = receiveData.getAddress();
						if (!IPAddress.equals(getMyAddress())) {
							byte[] sn_byte = new byte[4];
							System.arraycopy(start, 5, sn_byte, 0, sn_byte.length);
							int serial = ByteBuffer.wrap(invertByte(sn_byte)).getInt();
							byte[] config_byte = new byte[4];
							System.arraycopy(start, 9, config_byte, 0, config_byte.length);
							int config = ByteBuffer.wrap((config_byte)).getInt();

							String version = start[4] + "." + start[3];

							int testMode = start[19];

							deviceSubject.onNext(new UdpDevice(testMode, IPAddress.getHostAddress(), serial, config, version));
						}
					}
					if ((start[0] & 0x7F) == RequestConfig._command && receiveData.getLength() > 0) {
						int hubVersion = -1;
						HashMap<String, String> map = new HashMap<>();
						if (receiveData.getLength() == ConfigConstants.hubVersion2) {
							hubVersion = 2;
							byte[] dataHub2 = new byte[ConfigConstants.hubVersion2 - 1];
							System.arraycopy(start, 1, dataHub2, 0, dataHub2.length);
							config = new ReplyConfig(dataHub2);
							map = config.getMap();
						} else if (receiveData.getLength() == ConfigConstants.hubVersion1) {
							hubVersion = 1;
							byte[] dataHub1 = new byte[ConfigConstants.hubVersion1 - 1];
							System.arraycopy(start, 1, dataHub1, 0, dataHub1.length);
							config1h = new ReplyConfig1h(dataHub1);
							map = config1h.getMap();
						} else if (receiveData.getLength() == ConfigConstants.hubVersion3) {
							hubVersion = 3;
							byte[] dataHub3 = new byte[ConfigConstants.hubVersion3 - 2];
							System.arraycopy(start, 1, dataHub3, 0, dataHub3.length);
							configNg = new ReplyConfigNg(dataHub3);
							map = configNg.getMap();
						} else if (receiveData.getLength() == ConfigConstants.hubVersion4) {
							hubVersion = 4;
							byte[] dataHub4 = new byte[ConfigConstants.hubVersion3 - 1];
							System.arraycopy(start, 1, dataHub4, 0, dataHub4.length);
							config1hNg = new ReplyConfig1hNg(dataHub4);
							map = config1hNg.getMap();
						}
						if (hubVersion != -1) {
							map.put(ConfigConstants.HUB_VERSION, String.valueOf(hubVersion));
							if (!configSubject.hasComplete()) {
								configSubject.onNext(map);
							}
						}
					}
					if ((start[0] & 0x7F) == SendConfig._command && !receiveData.getAddress().equals(getMyAddress())) {
						sendConfigSubject.onNext(true);
					}
				});
	}

	private InetAddress getMyAddress() throws IOException {
		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((ip >> k * 8) & 0xFF);
		return InetAddress.getByAddress(quads);
	}

	public void sendUdpPacket(byte[] buf, String ip) {
		try {
			DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(ip), PORT);
			Completable.fromAction(() -> udpSocket.send(packet))
					.subscribeOn(Schedulers.io())
					.subscribe(() -> {
					}, throwable -> {
					}).isDisposed();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void sendUdpPacketWithCallback(byte[] buf, String ip, Callback stateChangeListener) {
		try {
			DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(ip), PORT);
			Completable.fromAction(() -> udpSocket.send(packet))
					.subscribeOn(Schedulers.io())
					.subscribe(() -> {
						stateChangeListener.complete();
					}, throwable -> {
						stateChangeListener.complete();
					}).isDisposed();
		} catch (Exception e) {
			stateChangeListener.complete();
			e.printStackTrace();
		}
	}
	public void sendUdpPacketWithErrorCallback(byte[] buf, String ip, Callback onError) {
		try {
			DatagramPacket packet = new DatagramPacket(buf, buf.length, InetAddress.getByName(ip), PORT);
			Completable.fromAction(() -> udpSocket.send(packet))
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(() -> {}, throwable -> {
						onError.complete();
					}).isDisposed();
		} catch (Exception e) {
			e.printStackTrace();
			onError.complete();
		}
	}

	public void sendReset(String ip, int pin) {
		sendUdpPacket(new RequestReset(pin).getBytes(), ip);
	}
	public void sendResetWithErrorCallback(String ip, int pin, Callback callback) {
		sendUdpPacketWithErrorCallback(new RequestReset(pin).getBytes(), ip, callback);
	}




	public void sendConfig(String ip, int pin, byte[] data) {
		sendUdpPacket(new SendConfig(data, pin).getBytes(), ip);
	}

	public void sendRequestConfig(String ip, int pin) {
		sendUdpPacket(new RequestConfig(pin).getBytes(), ip);
	}

	public void sendTestModeOff(String ip, int pin) {
		sendUdpPacket(new TestMode(TestMode.TestModeState.Off, pin).getBytes(), ip);
	}

	public void sendTestModeOffWithCallback(String ip, int pin, Callback callback) {
		sendUdpPacketWithCallback(new TestMode(TestMode.TestModeState.Off, pin).getBytes(), ip, callback);
	}

	public void sendTestModeOn(String ip, int pin) {
		sendUdpPacket(new TestMode(TestMode.TestModeState.On, pin).getBytes(), ip);
	}

	public void sendSurveyRequest() {
		sendUdpPacket(new RequestSurvey().getBytes(), "255.255.255.255");
	}

	private InetAddress getBroadcastAddress() throws IOException {
		int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
		byte[] quads = new byte[4];
		for (int k = 0; k < 4; k++)
			quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
		return InetAddress.getByAddress(quads);
	}

	private static class UdpData {
		DatagramPacket datagramPacket;
		byte[] bytes;

		public UdpData(DatagramPacket datagramPacket, byte[] bytes) {
			this.datagramPacket = datagramPacket;
			this.bytes = bytes;
		}
	}

}
