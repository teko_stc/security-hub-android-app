package biz.teko.td.SHUB_APP.Receivers;

import android.app.ActivityManager;
import android.app.ActivityOptions;
import android.app.NotificationManager;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Widgets.NormalWidget;
import biz.teko.td.SHUB_APP.Widgets.WidgetConfigActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardLoginActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardWelcomeActivity;

/**
 * Created by td13017 on 15.02.2017.
 */

public class D3ConnectionDropReceiver extends BroadcastReceiver {
	private D3Service d3Service;

	public D3ConnectionDropReceiver(D3Service d3Service) {
		this.d3Service = d3Service;
	}

	private static final int CONNECTION_DROP = 17;

	@Override
	public void onReceive(Context context, Intent intent) {
		String responseName = intent.getStringExtra("AppName");
		String from = intent.getStringExtra("from");
		Func.log_d(D3Service.LOG_TAG, "Catched connection drop in " + null != responseName ? responseName : "" + null != from ? " " + " from " + from : "");
		if (responseName != null) {
			if (responseName.equals(context.getPackageName())) {
				ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
				ComponentName cn;
				if (am.getAppTasks().size() == 0 && !(intent.getBooleanExtra("base", false))) {
					d3Service.dropCredentials(false);
					if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O  || PrefUtils.getInstance(context).getConnDropNotifMode()) {
						d3Service.showConnectionDropNotification();
					}
					((D3Service) context).stopConnection();
					return;
				}
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
					cn = am.getAppTasks().get(0).getTaskInfo().topActivity;
				} else {
					cn = am.getRunningTasks(1).get(0).topActivity;
				}
				String currentActivity = cn.getClassName();
				if ((!currentActivity.equals(WizardLoginActivity.class.getName()))
						&& (!currentActivity.equals(WizardWelcomeActivity.class.getName()))) {
					Intent intentLoginPage = new Intent(context, WizardWelcomeActivity.class);
					intentLoginPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

					if(intent.getBooleanExtra("base", false))//intent from manual logout
					{
						//drop credentials & stop service
						Func.log_d(D3Service.LOG_TAG, "Drop credentials from receiver " + null!=responseName? responseName:"");

						((D3Service) context).dropCredentials(intent.getBooleanExtra("saveData", true));//manual logout

						NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
						mNotificationManager.cancel(1);

						AppWidgetManager manager = AppWidgetManager.getInstance(context);
						final int[] appWidgetIds = manager.getAppWidgetIds(new ComponentName(context, NormalWidget.class));
						SharedPreferences sp = context.getSharedPreferences(WidgetConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
						for (int i = 0; i < appWidgetIds.length; ++i)
						{
							NormalWidget.updateWidget(context, manager, sp, appWidgetIds[i]);
						}
					}else {
						d3Service.dropCredentials(false);
						if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O  || PrefUtils.getInstance(context).getConnDropNotifMode()){
							d3Service.showConnectionDropNotification();
						}
						intentLoginPage.putExtra("SERVICE", CONNECTION_DROP);

					}

					((D3Service) context).stopConnection();

					context.getApplicationContext().startActivity(intentLoginPage, ActivityOptions.makeCustomAnimation(context.getApplicationContext(), R.animator.from_left_enter, R.animator.from_left_exit).toBundle());
				}
			}
		}
	}
}
