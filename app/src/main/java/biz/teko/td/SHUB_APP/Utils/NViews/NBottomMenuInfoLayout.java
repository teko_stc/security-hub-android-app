package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class NBottomMenuInfoLayout extends LinearLayout
{
	private static final int[] STATE_INVERTED = {R.attr.state_invert};
	private static final int[] STATE_ALARM = {R.attr.state_alarm};
	private static final int[] STATE_ATTENTION = {R.attr.state_attention};

	private final Context context;
	private int layout;
	private boolean alarm;
	private boolean attention;
	private boolean inverted;
	private TextView titleView;
	private String title;
	private OnItemClickListener onItemClickListener;
	private FrameLayout buttonClose;
	private FrameLayout titleFrame;

	public void setOnItemsClickListener(OnItemClickListener onItemClickListener){
		this.onItemClickListener = onItemClickListener;
	}

	public void nSetAlpha(float slideOffset)
	{
		if(null!=buttonClose && null!=titleFrame) {
			buttonClose.setAlpha(slideOffset);
			titleFrame.setAlpha(1-slideOffset);
			if(slideOffset == 1){
				titleFrame.setVisibility(GONE);
				buttonClose.setVisibility(VISIBLE);
			}else if(slideOffset == 0){
				titleFrame.setVisibility(VISIBLE);
				buttonClose.setVisibility(GONE);
			}else{
				titleFrame.setVisibility(VISIBLE);
				buttonClose.setVisibility(VISIBLE);
			}
		}
	}

	public interface OnItemClickListener
	{
		void onTitleClick();
		void onCloseClick();
	}

	public NBottomMenuInfoLayout(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setup();
		bind();
	}

	private void setup()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
		layoutInflater.inflate(layout, this, true);

		titleView = (TextView) findViewById(R.id.nBottomStateText);
		titleFrame = (FrameLayout) findViewById(R.id.nTitleFrame);
		buttonClose = (FrameLayout) findViewById(R.id.nButtonClose);
	}

	private void bind()
	{
		setTitleView();
		setButtonClose();
	}

	private void setButtonClose()
	{
		if(null!=buttonClose){
			buttonClose.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					onItemClickListener.onCloseClick();
				}
			});
		}
	}

	public void setInverted(boolean inverted){
		this.inverted = inverted;
		refreshDrawableState();
	}

	public void setAlarm(boolean alarm){
		this.alarm = alarm;
		refreshDrawableState();
	}

	public void setAttention(boolean attention){
		this.attention = attention;
		refreshDrawableState();
	}


	private void setTitleView()
	{
		if(null!= titleView){
			if(null!=title)titleView.setText(title);
			titleView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(null!= onItemClickListener) onItemClickListener.onTitleClick();
				}
			});
		}
	}

	public void setTitle(String title){
		this.title = title;
		setTitleView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NBottomMenuInfoLayout, 0 , 0);
		try
		{
			layout = a.getResourceId(R.styleable.NBottomMenuInfoLayout_NBottomMenuInfoLayout, 0);
		}finally
		{
			a.recycle();
		}
	}



	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 3);
		if(inverted) mergeDrawableStates(drawableState, STATE_INVERTED);
		if(alarm) mergeDrawableStates(drawableState, STATE_ALARM);
		if(attention) mergeDrawableStates(drawableState, STATE_ATTENTION);
		return drawableState;
	}
}
