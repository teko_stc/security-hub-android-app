package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Activity.DomainUsers.WizardDomainUserSetActivity;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.PresetRadioGroup;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.PresetValueButton;

public class DomainUserSetFragment extends Fragment
{
	private static final int POSITION_NAME = 0;
	private static final int POSITION_LOGIN = 1;
	private static final int POSITION_PASS = 2;
	private static final int POSITION_ROLE = 3;
	private static final int POSITION_FINISH = 4;

	private int position;
	private String roleTitle;
	private Context context;
	private ViewGroup container;
	private WizardDomainUserSetActivity activity;
	private DBHelper dbHelper;
	private View v;
	private boolean changed = false;
	private int role = 0;

	public static Fragment newInstance(int position)
	{
		DomainUserSetFragment domainUserSetFragment = new DomainUserSetFragment();
		Bundle b = new Bundle();
		b.putInt("position", position);
		domainUserSetFragment.setArguments(b);
		return domainUserSetFragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		position = getArguments().getInt("position");
		int layout;
		context = container.getContext();
		this.container = container;
		activity = (WizardDomainUserSetActivity) getActivity();
		dbHelper = DBHelper.getInstance(context);

		switch (position){
			case POSITION_NAME:
				layout = R.layout.fragment_domainuser_set_1;
				v = inflater.inflate(layout, container, false);
				setFragmentOne(v);
				break;
			case POSITION_LOGIN:
				layout = R.layout.fragment_domainuser_set_2;
				v = inflater.inflate(layout, container, false);
				setFragmentTwo(v);
				break;
			case POSITION_PASS:
				layout = R.layout.fragment_domainuser_set_3;
				v = inflater.inflate(layout, container, false);
				setFragmentThree(v);
				break;
			case POSITION_ROLE:
				layout = R.layout.fragment_domainuser_set_4;
				v = inflater.inflate(layout, container, false);
				setFragmentFour(v);
				break;
			case POSITION_FINISH:
				layout = R.layout.fragment_domainuser_set_5;
				v = inflater.inflate(layout, container, false);
				setFragmentFive(v);
				break;
			default:
				break;
		}

		return v;
	}

	private void setFragmentFive(View v)
	{
		if((activity).getCurrentItem() == POSITION_FINISH)
		{
			animateEntry();

			TextView nameText = (TextView) v.findViewById(R.id.wizDUMessageText);
			TextView roleText = (TextView) v.findViewById(R.id.wizDUMessageText1);

			nameText.setText(activity.getUserName());
			roleText.setText(activity.getRoleTitle());

			TextView buttonCont = (TextView) v.findViewById(R.id.wizDUButtonContinue);
			buttonCont.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					activity.goNext();
				}
			});
		}
	}

	private void setFragmentFour(View v)
	{
		if(activity.getCurrentItem() == POSITION_ROLE)
		{
			animateEntry();

			TextView buttonCancel = (TextView) v.findViewById(R.id.wizDUButtonCancel);
			final TextView buttonCont = (TextView) v.findViewById(R.id.wizDUButtonContinue);

			final PresetRadioGroup rolesRadioGroup = (PresetRadioGroup) v.findViewById(R.id.roles_radio_group);
			rolesRadioGroup.setOnCheckedChangeListener(new PresetRadioGroup.OnCheckedChangeListener()
			{
				@Override
				public void onCheckedChanged(View radioGroup, View radioButton, boolean isChecked, int checkedId)
				{
					switch( ((PresetValueButton)radioButton).getPosition()){
						case 1:
							role = 129024;
							break;
						case 2:
							role = 40960;
							break;
						case 3:
							role = 8192;
							break;

					};

					if(null!=buttonCont) buttonCont.setEnabled(true);
					roleTitle = ((PresetValueButton)radioButton).getTitle();
				}
			});

			buttonCancel.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					activity.goPrevious();
				}
			});

			buttonCont.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					if(0 != role)
					{
						activity.setUserRole(role);
						activity.setRoleTitle(roleTitle);
						activity.sendUserRegRequest();
					}
				}
			});

			final PresetValueButton button2 = (PresetValueButton) v.findViewById(R.id.user_radio_button);
			button2.setOnClickListener(new View.OnClickListener()			{
				@Override
				public void onClick(View view)
				{
				}
			});
			final PresetValueButton button3 = (PresetValueButton) v.findViewById(R.id.guest_radio_button);
			button3.setOnClickListener(new View.OnClickListener()			{
				@Override
				public void onClick(View view)
				{
				}
			});
		}
	}

	private void setFragmentThree(View v)
	{
		if(activity.getCurrentItem() == POSITION_PASS)
		{
			animateEntry();

			final TextView buttonCancel = (TextView) v.findViewById(R.id.wizDUButtonCancel);
			final TextView buttonCont = (TextView) v.findViewById(R.id.wizDUButtonContinue);

			final EditText editPass = (EditText) v.findViewById(R.id.wizDUEditPass);
			if(null!=editPass)editPass.requestFocus();
			final EditText editRepeatPass = (EditText) v.findViewById(R.id.wizDUEditPasRepeat);
			final TextInputLayout inputPassLayout = (TextInputLayout) v.findViewById(R.id.wizDUPassTextInput);
			final TextInputLayout inputRepeatLayout = (TextInputLayout) v.findViewById(R.id.wizDUPassRepeatTextInput);
			editPass.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
				{

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
				{
					if (changed && charSequence.length() == 0)
					{
						inputPassLayout.setError(" ");
					} else
					{
						inputPassLayout.setError("");
					}
					inputRepeatLayout.setError("");
				}

				@Override
				public void afterTextChanged(Editable editable)
				{

				}
			});

			editPass.setOnTouchListener(new View.OnTouchListener()
			{
				@Override
				public boolean onTouch(View view, MotionEvent motionEvent)
				{
					if(!changed) changed=!changed;
					return  false;
				}
			});

			editPass.setOnKeyListener(new View.OnKeyListener()
			{

				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && keyCode == KeyEvent.KEYCODE_ENTER)
					{
						editPass.clearFocus();
						editRepeatPass.requestFocus();
						return true;
					}
					return false;
				}
			});

			editRepeatPass.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
				{

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
				{
					if (changed && charSequence.length() == 0)
					{
						inputRepeatLayout.setError(" ");
					} else
					{
						inputRepeatLayout.setError("");
					}
					inputPassLayout.setError("");
				}

				@Override
				public void afterTextChanged(Editable editable)
				{

				}
			});

			editRepeatPass.setOnKeyListener(new View.OnKeyListener()
			{

				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && keyCode == KeyEvent.KEYCODE_ENTER)
					{
						buttonCont.callOnClick();
						return true;
					}
					return false;
				}
			});



			buttonCancel.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					activity.goPrevious();
				}
			});

			buttonCont.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					changed = true;
					String pass = editPass.getText().toString();
					String repeatPass = editRepeatPass.getText().toString();
					if (pass.length() > 5)
					{
						if(!repeatPass.equals("")){
							if(pass.equals(repeatPass)){
								activity.setUserPass(Func.md5(pass));
								activity.goNext();
							}else{
								Func.nShowMessage(context,getString(R.string.ERROR_PASS_DONT_MATCH));
							}
						}else{
							editRepeatPass.requestFocus();
							inputRepeatLayout.setError(" ");
						}

					} else
					{
						editPass.requestFocus();
						inputPassLayout.setError(getString(R.string.ERROR_NOT_ENOUGH_SYMBOLS));
					}
				}
			});
		}
	}

	private void setFragmentTwo(View v)
	{
		if(activity.getCurrentItem() == POSITION_LOGIN)
		{
			animateEntry();

			TextView buttonCancel = (TextView) v.findViewById(R.id.wizDUButtonCancel);
			final TextView buttonCont = (TextView) v.findViewById(R.id.wizDUButtonContinue);

			final EditText editLogin = (EditText) v.findViewById(R.id.wizDUEditLogin);
			if(null!=editLogin)editLogin.requestFocus();
			final TextInputLayout textInputLayout = (TextInputLayout) v.findViewById(R.id.wizDULoginTextInput);
			editLogin.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
				{

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
				{
					if (changed && charSequence.length() == 0)
					{
						textInputLayout.setError(" ");
					} else
					{
						textInputLayout.setError("");
					}
				}

				@Override
				public void afterTextChanged(Editable editable)
				{

				}
			});

			editLogin.setOnTouchListener(new View.OnTouchListener()
			{
				@Override
				public boolean onTouch(View view, MotionEvent motionEvent)
				{
					if(!changed) changed=!changed;
					return  false;
				}
			});

			editLogin.setOnKeyListener(new View.OnKeyListener()
			{

				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && keyCode == KeyEvent.KEYCODE_ENTER)
					{
						buttonCont.callOnClick();
						return true;
					}
					return false;
				}
			});


			buttonCancel.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					activity.goPrevious();
				}
			});

			buttonCont.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					changed = true;
					if (!editLogin.getText().toString().equals(""))
					{
						activity.setUserLogin(editLogin.getText().toString());
						activity.goNext();
					} else
					{
						editLogin.requestFocus();
						textInputLayout.setError(" ");
					}
				}
			});
		}
	}

	private void setFragmentOne(View v)
	{
		final String name = "";

		animateEntry();
		TextView buttonCancel = (TextView) v.findViewById(R.id.wizDUButtonCancel);
		final TextView buttonCont = (TextView) v.findViewById(R.id.wizDUButtonContinue);

		final EditText editName = (EditText) v.findViewById(R.id.wizDUEditName);
		final TextInputLayout textInputLayout = (TextInputLayout) v.findViewById(R.id.wizDUNameTextInput);
		if(null!=editName) editName.requestFocus();
		editName.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{
				if(charSequence.length() == 0)
				{
					textInputLayout.setError(" ");
				}else{
					textInputLayout.setError("");
				}
			}

			@Override
			public void afterTextChanged(Editable editable)
			{

			}
		});

		editName.setOnKeyListener(new View.OnKeyListener()
		{

			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && keyCode == KeyEvent.KEYCODE_ENTER)
				{
					buttonCont.callOnClick();
					return true;
				}
				return false;
			}
		});

		buttonCancel.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				activity.goPrevious();
			}
		});

		buttonCont.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if(!editName.getText().toString().equals("")){
					activity.setUserName(editName.getText().toString());
					activity.goNext();
				}else{
					editName.requestFocus();
					textInputLayout.setError(" ");
				}

			}
		});

	}

	private void animateEntry()
	{
		TextView title = (TextView) v.findViewById(R.id.wizDUTitleText);
		Animation slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);

		if (title.getVisibility() == View.INVISIBLE) {
			title.setVisibility(View.VISIBLE);
			title.startAnimation(slideUp);
		}

		FrameLayout frameOne = (FrameLayout) v.findViewById(R.id.wizDUFrame1);
		Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);
		frameOne.startAnimation(fadeIn);
	}


}
