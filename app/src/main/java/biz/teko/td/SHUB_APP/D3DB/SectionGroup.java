package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

import biz.teko.td.SHUB_APP.Utils.Dir.Const;

public class SectionGroup
{
	public int id;
	public String name;
	public Section[] sections;

	public SectionGroup(){};

	public SectionGroup(int id, String name){
		this.id = id;
		this.name = name;
	}

	public SectionGroup(int id, String name, Section[] sections){
		this.id = id;
		this.name = name;
		this.sections = sections;
	}

	public SectionGroup(Cursor c)
	{
		this.id = c.getInt(0);
		this.name = c.getString(1);
	}

	public int getArmed()
	{
		if(null!=this.sections && sections.length > 0){
			int count = 0;
			for(Section section:sections){
				if(section.armed > 0) count ++;
			}
			if (count > 0){
				if(count == this.sections.length) return Const.STATUS_ARMED;
				return Const.STATUS_PARTLY_ARMED;
			}
		}
		return Const.STATUS_DISARMED;
	}
}
