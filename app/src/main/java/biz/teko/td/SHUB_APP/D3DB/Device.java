package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class Device extends  D3Element
{
	public int account, config_version, type, cluster;
	public int parted = 0;
	public User[] users;
	public String site;
	public boolean enabled = true;
	public int armed = 0;
	public String ip;
	public Section[] sections;
	private double version;

	public Device(){}

	public Device(int id, int account, int config_version, int type, int cluster) {
		this.id = id;
		this.account = account;
		this.config_version = config_version;
		this.type = type;
		this.cluster = cluster;
	}

	public Device(int account,int config_version, String ip, double version) {
		this.account = account;
		this.config_version = config_version;
		this.ip = ip;
		this.version = version;
	}

	public Device(int id, int account, int config_version, Section[] sections, User[] users, int parted, int cluster){
		this.id = id;
		this.account = account;
		this.config_version = config_version;
		this.sections = sections;
		this.users= users;
		this.parted = parted;
		this.cluster = cluster;
	}

	public Device(Cursor cursor)
	{
		this.id = cursor.getInt(1);
		this.account = cursor.getInt(2);
		this.config_version = cursor.getInt(3);
		this.type = cursor.getInt(5);
	}

	public String getName(){
		if(null!= D3Service.d3XProtoConstEvent){
			DeviceType deviceType = D3Service.d3XProtoConstEvent.getDeviceTypeById(this.type);
			if(null!=deviceType){
				return deviceType.caption;
				//				imageStatement.setImageResource(context.getResources().getIdentifier(deviceType.icons.get(0).src, null, context.getPackageName()));
			}
		}
		return App.getContext().getResources().getString(R.string.UNKNOWN_DEVICE_MODEL);
	}

	public DeviceType getType(){
		if(null!= D3Service.d3XProtoConstEvent){
			return  D3Service.d3XProtoConstEvent.getDeviceTypeById(this.type);
		}
		return null;
	}

	//23.09.2020
	public String getVersion(){
		byte[] b = Func.intToByteArray(this.config_version);
		return b[3] + "." + b[2];
	}

	public boolean canBeSetFromApp()
	{
		return (Const.DEVICETYPE_SH <= this.type && this.type <= Const.DEVICETYPE_SH_4G);
	}

	public double getDeviceVersion() {
		return version;
	}

}
