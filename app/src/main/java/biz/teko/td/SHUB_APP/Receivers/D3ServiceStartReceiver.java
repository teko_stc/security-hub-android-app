package biz.teko.td.SHUB_APP.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 09.06.2016.
 */
public class D3ServiceStartReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent) {
		Func.log_v(D3Service.LOG_TAG, "Receive restart broadcast intent");
		Func.restartD3Service(context, "D3ServiceStartReceiver");
//		Func.startD3Service(context, "D3ServiceStartReceiver");

	}
}
