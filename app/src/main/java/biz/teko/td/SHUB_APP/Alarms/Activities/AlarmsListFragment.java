package biz.teko.td.SHUB_APP.Alarms.Activities;

import static android.app.Activity.RESULT_OK;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NAlarmsTopView;
import biz.teko.td.SHUB_APP.Utils.Other.NListView;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class AlarmsListFragment extends Fragment {
    private NAlarmsTopView alarmsTopView;
    private NListView critList;
    private DBHelper dbHelper;
    private final BroadcastReceiver alarmsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != alarmsTopView) {
                updateAlarmsView();
            }
        }
    };
    private NEventsListAdapter critAdapter;
    private final BroadcastReceiver eventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != critList) {
                updateCritList();
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.n_activity_alarms_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dbHelper = DBHelper.getInstance(getContext());
        setup();
    }

    private void setup() {
        alarmsTopView = (NAlarmsTopView) getActivity().findViewById(R.id.nAlarmsTopView);
        critList = (NListView) getActivity().findViewById(R.id.nCritList);
    }

    private void bind() {
        if (null != alarmsTopView) {
            updateAlarmsView();
        }
        if (null != critList) {
            updateCritList();
        }
    }

    private void updateAlarmsView() {
        alarmsTopView.setOnViewClickListener(getActivity()::onBackPressed);
        alarmsTopView.setAlarms(dbHelper.getAlarmsList());
        alarmsTopView.bind();
    }

    private void updateCritList() {
        Cursor cursor = updateCritCursor();
        if (null == critAdapter) {
            critAdapter = new NEventsListAdapter(getActivity(), R.layout.n_events_list_element, cursor, 0, false);
            critList.setAdapter(critAdapter);
        } else {
            critAdapter.update(cursor);
        }
        if (critAdapter.getCount() > 0) {
            critList.setVisibility(View.VISIBLE);
        } else {
            critList.setVisibility(View.GONE);
        }
    }

    private Cursor updateCritCursor() {
        return dbHelper.nGetCritEventsCursorAMMode(0, PrefUtils.getInstance(getActivity()).getCurrentSite());
    }

    @Override
    public void onResume() {
        super.onResume();
        bind();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(D3Service.BROADCAST_ALARM_ADD_UPD_RMV);
        intentFilter.addAction(D3Service.BROADCAST_ALARM_OPEN_CLOSE);
        getActivity().registerReceiver(alarmsReceiver, intentFilter);
        getActivity().registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_AFFECT));
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(alarmsReceiver);
        getActivity().unregisterReceiver(eventReceiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.REQUEST_SHOW_LIST_ELEMENT) {
            if (resultCode == RESULT_OK) {
                Func.nShowSuccessMessage(getActivity());
            }

            bind();
        }
    }
}
