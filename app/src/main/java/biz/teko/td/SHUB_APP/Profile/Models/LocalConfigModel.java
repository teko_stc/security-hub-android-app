package biz.teko.td.SHUB_APP.Profile.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class LocalConfigModel implements Parcelable {
    private String title;
    private String type;
    private String value;
    private String description;

    public LocalConfigModel(String title, String type) {
        this.title = title;
        this.type = type;
    }

    private LocalConfigModel(String title, String type, String value, String description) {
        this.title = title;
        this.type = type;
        this.value = value;
        this.description = description;
    }

    private LocalConfigModel(Parcel in) {
        title = in.readString();
        type = in.readString();
        value = in.readString();
        description = in.readString();
    }

    public static final Creator<LocalConfigModel> CREATOR = new Creator<LocalConfigModel>() {
        @Override
        public LocalConfigModel createFromParcel(Parcel source) {
            String title = source.readString();
            String type = source.readString();
            String value = source.readString();
            String description = source.readString();
            return new LocalConfigModel(title, type, value, description);
        }

        @Override
        public LocalConfigModel[] newArray(int size) {
            return new LocalConfigModel[size];
        }
    };
    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(type);
        parcel.writeString(value);
        parcel.writeString(description);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
