package biz.teko.td.SHUB_APP.SitesTabs.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONObject;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.Adapter.NRListAdapter;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Delegation.Activities.DelegateAddActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SitesTabs.Adapters.SitesTreeAdapter;
import biz.teko.td.SHUB_APP.SitesTabs.SitesActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerSetActivity;


/**
 * Created by td13017 on 07.02.2017.
 */

public class SitesTabFragment extends Fragment
{
	DBHelper dbHelper;
	Context context;
	private View v;
	private int previousItem = -1;
	private int currentItem;
	private ExpandableListView expandableListView;
	private SitesTreeAdapter sitesTreeAdapter;
	private int sending = 0;
	private FrameLayout fabLayout;
	private FrameLayout fabLayout1;
	private FrameLayout fabLayout2;
	private FloatingActionButton fab;
	private FloatingActionButton fab1;
	private FloatingActionButton fab2;
	private TextView fabText1;
	private TextView fabText2;
	private boolean isFABOpen = false;

	private AlertDialog.Builder nrDialogBuilder;
	private AlertDialog nrDialog;
	private NRListAdapter nrListAdapter;
	private ListView nrList;
	private LinkedList<Event> eventsNRList = new LinkedList<>();

	private int eCount = 0;

	private BroadcastReceiver revokeResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result > 0){
				Func.pushToast(context, getString(R.string.DELEGATE_REVOKE_SUCCESS), (SitesActivity) context);
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (SitesActivity) context);
			}
		}
	};

	private BroadcastReceiver siteSetResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result > 0){
				Func.showSnackBar(context,  getActivity().findViewById(android.R.id.content), getString(R.string.TOAST_SUCC_CHANGED));
			}else{
				Func.showSnackBar(context, getActivity().findViewById(android.R.id.content), Func.handleResult(context, result));
			}
		}
	};

	private BroadcastReceiver affectsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(sitesTreeAdapter!=null){
				Cursor sitesCursor  = dbHelper.getAllSitesCursor();
				sitesTreeAdapter.update(sitesCursor, sending);
			}
		}
	};

	private BroadcastReceiver sitesUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(sitesTreeAdapter!=null){
				Cursor sitesCursor  = dbHelper.getAllSitesCursor();
				sitesTreeAdapter.update(sitesCursor, sending);
			}
		}
	};

	private BroadcastReceiver notReadyReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int eventId = intent.getIntExtra("eventId", -1);
			Event event = dbHelper.getEventById(eventId);
			if(null!=event)
			{
				eventsNRList.add(event);
				showNotReadyMessage(context);
			}
		}
	};

	private BroadcastReceiver siteDeleteResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result > 0){
				Func.showSnackBar(context, getActivity().findViewById(android.R.id.content),getString(R.string.SITE_DELETE_SUCCESS));
			}else{
				Func.showSnackBar(context, getActivity().findViewById(android.R.id.content),Func.handleResult(context, result));
			}
		}
	};

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, getActivity().findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};


	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(final Context context, Intent intent)
		{
			sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, getActivity().findViewById(android.R.id.content), Func.handleResult(context, result));
						} else
						{
							Func.showSnackBar(context, getActivity().findViewById(android.R.id.content),context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, getActivity().findViewById(android.R.id.content), context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						sending = 0;
						break;
					default:
						sending = 0;
						break;
				}
			}
			Cursor cursor = dbHelper.getAllSitesCursor();
			sitesTreeAdapter.update(cursor, sending);
			if(null!=fab){
				if(sending == 0){
					fab.setEnabled(true);
				}else{
					fab.setEnabled(false);
				}
			}
		}
	};

	@Override
	public void onResume()
	{
		super.onResume();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		getActivity().registerReceiver(sitesUpdateReceiver, intentFilter);

		getActivity().registerReceiver(revokeResultReceiver, new IntentFilter(D3Service.BROADCAST_DELEGATE_REVOKE_RESULT));
		getActivity().registerReceiver(affectsReceiver, new IntentFilter(D3Service.BROADCAST_AFFECT));
		getActivity().registerReceiver(notReadyReceiver, new IntentFilter(D3Service.BROADCAST_SITE_NOT_READY));
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		getActivity().registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		getActivity().registerReceiver(siteSetResultReceiver, new IntentFilter(D3Service.BROADCAST_SITE_SET));
		getActivity().registerReceiver(siteDeleteResultReceiver, new IntentFilter(D3Service.BROADCAST_SITE_DEL));
		getActivity().registerReceiver(siteDeleteResultReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_REVOKE_DOMAIN));

		if(expandableListView!=null){
			if(sitesTreeAdapter!=null){
				Cursor cursor = dbHelper.getAllSitesCursor();
				sitesTreeAdapter.update(cursor, sending);
			}
		}
		eventsNRList.clear();
	}

	@Override
	public void onPause()
	{
		super.onPause();

		getActivity().unregisterReceiver(revokeResultReceiver);
		getActivity().unregisterReceiver(affectsReceiver);
		getActivity().unregisterReceiver(sitesUpdateReceiver);
		getActivity().unregisterReceiver(notReadyReceiver);
		getActivity().unregisterReceiver(commandResultReceiver);
		getActivity().unregisterReceiver(commandSendReceiver);
		getActivity().unregisterReceiver(siteSetResultReceiver);
		getActivity().unregisterReceiver(siteDeleteResultReceiver);
	}


	@SuppressLint("RestrictedApi")
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		v = inflater.inflate(R.layout.tab_main_sites, container, false);
		dbHelper = DBHelper.getInstance(context);
		context = container.getContext();
		UserInfo userInfo = dbHelper.getUserInfo();

		expandableListView = (ExpandableListView) v.findViewById(R.id.sitesList);
		expandableListView.setGroupIndicator(null);

		Cursor sitesCursor  = dbHelper.getAllSitesCursor();

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		boolean showDevices = Func.getBooleanSPDefFalse(sp, "pref_show_devices");
		boolean showSections = Func.getBooleanSPDefFalse(sp, "pref_show_sections");
		if(((userInfo.roles& Const.Roles.TRINKET)==0) && !App.getMode()){
			showDevices = false;
			showSections = false;
		}
		if(null!=sitesCursor){
			int expandedLayout = R.layout.card_site_expanded;
			if(!showDevices && ((userInfo.roles&Const.Roles.TRINKET)==0)){
				expandedLayout = R.layout.card_site_expanded_relay;
			}
			sitesTreeAdapter = new SitesTreeAdapter(context, sitesCursor, R.layout.card_site, expandedLayout, R.layout.card_affect, R.layout.card_affect, showSections, showDevices);
			expandableListView.setAdapter(sitesTreeAdapter);
		}

		if(sitesCursor.getCount() == 1){
			expandableListView.expandGroup(0);
		}


//		expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//			@Override
//			public void onGroupExpand(int groupPosition) {
//				eCount ++;
//				currentItem = groupPosition;
//				if(previousItem == -1){
//					expandableListView.setBackgroundColor(getResources().getColor(R.color.brandColorLightPattern));
//				}
////				if(groupPosition != previousItem && previousItem!= -1)
////					expandableListView.collapseGroup(previousItem );
//				previousItem = groupPosition;
//			}
//		});
//		expandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener()
//		{
//			@Override
//			public void onGroupCollapse(int groupPosition)
//			{
//				eCount --;
//				if(eCount  == 0){
//					expandableListView.setBackgroundColor(getResources().getColor(R.color.brandColorWhite));
//				}
//			}
//		});


		fabLayout = (FrameLayout) v.findViewById(R.id.fabLayout);
		fabLayout1 = (FrameLayout) v.findViewById(R.id.fabLayout3);
		fabLayout2 = (FrameLayout) v.findViewById(R.id.fabLayout4);
		fab = (FloatingActionButton) v.findViewById(R.id.fab);
		fab1 = (FloatingActionButton) v.findViewById(R.id.fab3);
//		fab1.setEnabled(false);
		fab2 = (FloatingActionButton) v.findViewById(R.id.fab4);
		fabText1 = (TextView) v.findViewById(R.id.fabText3);
		fabText2 = (TextView) v.findViewById(R.id.fabText4);

		if(((userInfo.roles&Const.Roles.DOMAIN_LAWYER)==0)&& ((userInfo.roles&Const.Roles.ORG_LAWYER)==0)){
			fabLayout.setVisibility(View.GONE);
			fab.setVisibility(View.GONE);
		}else
		{
			fab.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if (!isFABOpen)
					{
						showFABMenu();
					} else
					{
						closeFABMenu();
					}
				}
			});
			expandableListView.setOnScrollListener(new AbsListView.OnScrollListener()
			{
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState)
				{

				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
				{
					if (0 != firstVisibleItem && fab.isShown())
					{
						fab.hide();
						fab1.hide();
						fab2.hide();
						fabText1.setVisibility(View.INVISIBLE);
						fabText2.setVisibility(View.INVISIBLE);
						fabLayout.setClickable(false);
					}
					if (0 == firstVisibleItem && !fab.isShown())
					{
						fab.show();
						fab1.show();
						fab2.show();
						if(isFABOpen)
							closeFABMenu();
					}
				}
			});
			fab2.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					Intent intent = new Intent(context, WizardControllerSetActivity.class);
					intent.putExtra("from", 1);
					((Activity) context).startActivity(intent);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
			fab1.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					Intent intent = new Intent(context, DelegateAddActivity.class);
					((Activity) context).startActivity(intent);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
		}

		expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
		{
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
			{
				return false;
			}
		});

		expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener()
		{
			@Override
			public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id)
			{
				parent.collapseGroup(groupPosition);
				return false;
			}
		});

		return v;
	}



	private void showFABMenu(){

		isFABOpen=true;
		setFabInMain();
		fab.animate().rotation(-45);
		fabLayout.setClickable(true);
		fabLayout.setBackgroundColor(getResources().getColor(R.color.brandTransparentWhite50));
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		fabLayout1.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 65, metrics));
		fabLayout2.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 125, metrics));
		fabText1.setVisibility(View.VISIBLE);
		fabText2.setVisibility(View.VISIBLE);
	}

	public void closeFABMenu(){
		isFABOpen=false;
		setFabInMain();
		fab.animate().rotation(0);

		fabText1.setVisibility(View.INVISIBLE);
		fabText2.setVisibility(View.INVISIBLE);
		fabLayout1.animate().translationY(0);
		fabLayout2.animate().translationY(0);
		fabLayout.setClickable(false);
		fabLayout.setBackgroundColor(getResources().getColor(R.color.brandTransparentWhite00));
	}

	private void setFabInMain(){
		Activity activity = getActivity();
		if(activity != null){
			SitesActivity sitesActivity = (SitesActivity) activity;
			sitesActivity.setFabStatus(isFABOpen);
		}
	}

	public void showNotReadyMessage(final Context context)
	{
		if((null!=nrDialog)&&(nrDialog.isShowing())){
			//updateAdapter
			if(nrListAdapter != null){
				nrListAdapter.notifyDataSetChanged();
			}
		}else
		{
			nrDialogBuilder = Func.adbForCurrentSDK(context);
			nrDialog = nrDialogBuilder.create();
			nrDialog.setTitle(R.string.TITLE_FAILED);
			nrDialog.setMessage(getString(R.string.MESS_NOT_READY));

			nrList = new ListView(context);

			UserInfo userInfo = dbHelper.getUserInfo();
			if((userInfo.roles&Const.Roles.DOMAIN_HOZ_ORG)!=0)
			{
				nrDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ADB_RETRY), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						UserInfo userInfo = dbHelper.getUserInfo();
						if ((eventsNRList != null) && (eventsNRList.size() != 0))
						{
							LinkedList<Integer> deviceIds = new LinkedList<>();
							deviceIds.add(eventsNRList.get(0).device);
							for (Event event : eventsNRList)
							{
								boolean compare = false;
								for (int deviceId : deviceIds)
								{
									if (event.device == deviceId)
									{
										compare = true;
									}
								}
								if (!compare)
								{
									deviceIds.add(event.device);
								}
							}
							LinkedList<Integer> armedDevicesIds = new LinkedList<>();
							String armedDevicesString = "";
							for (int deviceId : deviceIds)
							{
								if (!dbHelper.isDeviceOnline(deviceId))
								{
									armedDevicesIds.add(deviceId);
									Device device = dbHelper.getDeviceById(deviceId);
									if (null != device)
									{
										armedDevicesString += device.getName() + "(" + Integer.toString(device.account) + "),";
									}
								}
							}
							if (armedDevicesIds.size() == 0)
							{
								for (int deviceId : deviceIds)
								{
									JSONObject commandAddress = new JSONObject();
										int command_count = Func.getCommandCount(context);
										D3Request d3Request = D3Request.createCommand(userInfo.roles, deviceId, "ARM", commandAddress, command_count);
										if (d3Request.error == null)
										{
											sendMessageToServer(d3Request, true);
										} else
										{
											Func.pushToast(context, d3Request.error, (Activity) context);
									}
								}
							} else
							{
								Func.nShowMessage(context, getString(R.string.ERROR_CANNOT_ARM_SITES_NOT_READY) + armedDevicesString.substring(0, armedDevicesIds.size() - 1));
							}
							eventsNRList.clear();
							nrListAdapter.notifyDataSetChanged();
						}
					}
				});
			}

			nrDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.ADB_CANCEL), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
					if(null!=eventsNRList && 0 != eventsNRList.size())
					{
						eventsNRList.clear();
					}
				}
			});

			nrListAdapter = new NRListAdapter(context, eventsNRList);
			nrList.setAdapter(nrListAdapter);
			FrameLayout frameLayout = (FrameLayout) new FrameLayout(context);
			frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			frameLayout.addView(nrList);

			nrDialog.setView(frameLayout);
			nrDialog.show();
		}
	}

	public void sendMessageToServer(D3Request d3Request, boolean command)
	{
		Activity activity = getActivity();
		if(activity != null){
			SitesActivity sitesActivity = (SitesActivity) activity;
			sitesActivity.sendMessageToServer(d3Request, command);
		}
	};

//	public D3Service getService()
//	{
//		Activity activity = getActivity();
//		if(activity != null){
//			SitesActivity mainActivity = (SitesActivity) activity;
//			return mainActivity.getLocalService();
//		}
//		return null;
//	}

}
