package biz.teko.td.SHUB_APP.Activity;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.R;

public class PopupFragment extends Fragment {

	public static PopupFragment newInstance(int classId, int detectorId, int reasonId, int activeId, String data, int additionalCount) {
		PopupFragment fragment = new PopupFragment();
		Bundle b = new Bundle();
		b.putInt("eventClass", classId);
		b.putInt("eventDetector", detectorId);
		b.putInt("eventReason", reasonId);
		b.putInt("eventActive", activeId);
		b.putString("eventData", data);
		b.putInt("addCount", additionalCount);
		fragment.setArguments(b);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		PowerManager pm = (PowerManager) getActivity().getApplicationContext().getSystemService(Context.POWER_SERVICE);
		PowerManager.WakeLock mWakeLock = pm.newWakeLock((PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "YourServie");
		mWakeLock.acquire();
		mWakeLock.release();

		int classId = getArguments().getInt("eventClass", -1);
		int detectorId = getArguments().getInt("eventDetector", -1);
		int reasonId = getArguments().getInt("eventReason", -1);
		int activeId = getArguments().getInt("eventActive", -1);
		String data = getArguments().getString("eventData");
		int additionalCount = getArguments().getInt("addCount", 0);

		View view = inflater.inflate(R.layout.card_popup, container, false);

		ImageView eventImage = (ImageView) view.findViewById(R.id.eventImage);
		TextView eventDescription = (TextView) view.findViewById(R.id.eventDescription);
		TextView eventAdditional = (TextView) view.findViewById(R.id.eventAdditional);

//		eventImage.setImageResource(getActivity().getResources().getIdentifier(affect.decribeAffectIconSmall(), null, context.getPackageName()));

//
//		WindowManager mWindowManager = (WindowManager) getActivity().getSystemService(WINDOW_SERVICE);
//		WindowManager.LayoutParams mLayoutParams = new WindowManager.LayoutParams(
//				ViewGroup.LayoutParams.WRAP_CONTENT,
//				ViewGroup.LayoutParams.WRAP_CONTENT, 0, 0,
//				WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
//				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
//						| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
//						| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
///* | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON */,
//				PixelFormat.RGBA_8888);
//
//		mWindowManager.addView(view, mLayoutParams);
		return view;
	}
}