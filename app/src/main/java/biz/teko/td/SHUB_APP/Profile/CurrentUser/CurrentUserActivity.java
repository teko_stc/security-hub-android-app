package biz.teko.td.SHUB_APP.Profile.CurrentUser;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 22.08.2017.
 */

public class CurrentUserActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;

	private BroadcastReceiver operatorChangedReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", -99);
			if(result > 0){
				Func.pushToast(context, getString(R.string.PUA_SUCCESS_CHANGED), (Activity) context);
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
			}
		}
	};

	private BroadcastReceiver operatorUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			setUserInfoTable();
		}
	};

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_current_user);

		context = this;
		dbHelper = DBHelper.getInstance(context);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.TITLE_CURRENT_USER);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		serviceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});

		setUserInfoTable();

		if(!Func.isAxiosConfig()) getFragmentManager().beginTransaction().replace(R.id.prefContainer, new CurrentUserPreferenceFragment()).commit();
	}

	private void setUserInfoTable()
	{
		final UserInfo userInfo = dbHelper.getUserInfo();

		TextView userName = (TextView) findViewById(R.id.textUserName);
		TextView userLogin = (TextView) findViewById(R.id.textUserLogin);
		TextView userRoles = (TextView) findViewById(R.id.textUserRoles);
		TextView userObjects = (TextView) findViewById(R.id.textUserObjectsCount);

		userName.setText(getString(R.string.NAME_DOTS) + " " + dbHelper.getOperatorNameById(userInfo.id) + "\n");
		userLogin.setText(getString(R.string.LOGIN_DOTS) + " " + userInfo.userLogin + "\n");
		userRoles.setText(getString(R.string.ROLES_DOTS) + " " + getResources().getStringArray(R.array.user_roles)[userInfo.getRoleValue()] + "\n");
		userObjects.setText(getString(R.string.OBJECT_COUNT_DOTS) + " " + dbHelper.getSitesCount());
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
		registerReceiver(operatorChangedReceiver, new IntentFilter(D3Service.BROADCAST_OPERATOR_SET));
		registerReceiver(operatorUpdateReceiver, new IntentFilter(D3Service.BROADCAST_OPERATORS));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(operatorChangedReceiver);
		unregisterReceiver(operatorUpdateReceiver);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}
}
