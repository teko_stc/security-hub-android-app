package biz.teko.td.SHUB_APP.D3.D3XProtoEvent;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

public class DType
{
	@Attribute(name = "id")
	public int id;

	@Attribute(name = "caption")
	public String caption;

	@ElementList(name = "alarmtypes", entry = "alarmtype", required = false)
	public LinkedList<AType> alarmtypes;

	@Attribute(name = "sectionmask", required = false)
	public String sectionmask;

	@Attribute(name = "sectiondefault", required = false)
	public int sectiondefault;

	@Attribute(name = "default", required = false)
	public int def;

	public AType getAlarmType(int id){
		LinkedList<AType> alarmtypes = this.alarmtypes;
		if(null!=alarmtypes){
			for(AType aType :alarmtypes){
				if(aType.id == id){
					return aType;
				}
			}
		}
		return null;
	}

	public  AType getDefaultAType()
	{
		LinkedList<AType> alarmtypes = this.alarmtypes;
		if(null!=alarmtypes)
			for(AType aType :alarmtypes){
				if(aType.def == 1){
					return aType;
				}
			}
		return null;
	}
}
