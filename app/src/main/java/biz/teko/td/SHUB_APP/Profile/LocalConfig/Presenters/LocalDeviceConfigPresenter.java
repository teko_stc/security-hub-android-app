package biz.teko.td.SHUB_APP.Profile.LocalConfig.Presenters;

import android.content.res.Resources;
import android.os.Bundle;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.ILoadConfigList;
import biz.teko.td.SHUB_APP.Profile.Models.LocalConfigModel;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class LocalDeviceConfigPresenter {
    private DeviceDTO deviceDTO;
    private final SocketConnection localConnection;
    private ILoadConfigList iLoadConfigListActivity = null;
    private final PublishSubject<Boolean> resetSubject = PublishSubject.create();
    private final PublishSubject<Boolean> idleSubject = PublishSubject.create();
    private final CompositeDisposable disposables = new CompositeDisposable();
    private Disposable timerDisposable;
    private boolean isCancel, isAccept, isReset;

    public LocalDeviceConfigPresenter(SocketConnection connectionProvider) {
        localConnection = connectionProvider;
    }

    public void attachDeviceAndActivity(ILoadConfigList iLoadConfigListActivity, DeviceDTO device) {
        this.deviceDTO = device;
        this.iLoadConfigListActivity = iLoadConfigListActivity;
        disposeTimerChooseConditional();
    }

    public void registerMovement() {
        idleSubject.onNext(true);
    }

    public void sendTestModeOff() {
        localConnection.sendTestModeOff(deviceDTO.getIp(), deviceDTO.getPin());
        deviceDTO.setTestMode(false);
    }
    public void sendTestModeOn() {
        localConnection.sendTestModeOn(deviceDTO.getIp(), deviceDTO.getPin());
        deviceDTO.setTestMode(true);
    }

    public void sendConfig() {
        //if (!localConnection.getConfigSubject().hasComplete()) {
            iLoadConfigListActivity.showProgressBar();
            setConfigDisposable();
            localConnection.sendRequestConfig(deviceDTO.getIp(), deviceDTO.getPin());
            disposables.add(timerDisposable = getTimerObservable()
                    .subscribe(aLong -> errorClose()));
        //}
    }

    private void errorClose() {
        iLoadConfigListActivity.close();
        iLoadConfigListActivity.showToast(R.string.D3_ERR_CONTRACTOR_NOT_FOUND);
    }

    private void setConfigDisposable() {
        disposables.add(localConnection.getConfigSubject()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(map -> {
                    if (map.isEmpty()) {
                        errorClose();
                    } else {
                        timerDisposable.dispose();
                        iLoadConfigListActivity.dismissProgressBar(true);
                        iLoadConfigListActivity.setConfigValues(map);
                    }
                }));
    }

    private void disposeTimerChooseConditional() {
        disposables.add(localConnection.getDeviceSubject().subscribe(udpDevice -> {
            if (udpDevice.ip.equals(deviceDTO.getIp())) {
                if (udpDevice.mode == 0) {
                    if (timerDisposable != null && !timerDisposable.isDisposed())
                        timerDisposable.dispose();
                    //localConnectionStopSession();
                } else if (udpDevice.mode == 1) {
                    resetSubject.onNext(true);
                }
            }
        }));
    }

    private void sendConfig(byte[] data, DeviceDTO device) {
        disposables.add(localConnection.getSendConfigSubject()
                .subscribeOn(Schedulers.io())
                .subscribe(isSend -> {
                    if (isSend) {
                        isCancel = true;
                        isAccept = true;
                        sendTestModeOff();
                        iLoadConfigListActivity.close();
                    }
                }));
        localConnection.sendConfig(device.getIp(), device.getPin(),  data);
        setProgressAndTimer(device);
    }

    private void setProgressAndTimer(DeviceDTO device) {
        if(null!=iLoadConfigListActivity)
        {
            iLoadConfigListActivity.showProgressBar();
            timerDisposable = getTimerObservable().subscribe(aLong -> {
                if (null != iLoadConfigListActivity)
                    iLoadConfigListActivity.dismissProgressBar(false);
                localConnection.sendTestModeOff(device.getIp(), device.getPin());
                device.setTestMode(false);
            });
        }
    }

    public void setDialogConfig(Bundle configBundle, String configType, int hubVersion, DeviceDTO deviceDTO) {

        if (hubVersion == 2) {
            localConnection.getConfig().setNewAnotherConfig(configBundle, configType);
            sendConfig(localConnection.getConfig().getData(), deviceDTO);
        } else if (hubVersion == 1) {
            localConnection.getConfig1h().setNewAnotherConfig(configBundle, configType);
            sendConfig(localConnection.getConfig1h().getData(), deviceDTO);
        } else if (hubVersion == 3) {
            localConnection.getConfigNg().setNewAnotherConfig(configBundle, configType);
            sendConfig(localConnection.getConfigNg().getData(), deviceDTO);
        } else if (hubVersion == 4) {
            localConnection.getConfig1hNg().setNewAnotherConfig(configBundle, configType);
            sendConfig(localConnection.getConfig1hNg().getData(), deviceDTO);
        }
    }

    private Observable<Long> getTimerObservable() {
        return Observable
                .timer(20, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    public void detach() {
        iLoadConfigListActivity = null;
        disposables.clear();
        disposables.dispose();
    }

    public boolean isCancel() {
        return isCancel;
    }

    public boolean isAccept() {
        return isAccept;
    }

    public boolean isStartShake() {
        return localConnection.isConnected();
    }

    public void setCancel(boolean cancel) {
        isCancel = cancel;
    }

    public boolean isReset() {
        return isReset;
    }

    public boolean isDialogValues(String type) {
        return Arrays.asList(ConfigConstants.stringsWithDialog).contains(type);
    }

    public boolean isBooleanValues(String type) {
        return Arrays.asList(ConfigConstants.stringsWithDialogBoolean).contains(type);
    }

    public void addAPN2(Resources resources, List<LocalConfigModel> configs) {
        configs.add(3, new LocalConfigModel(resources.getString(R.string.N_APN2_SERVER), ConfigConstants.APN2_SERVER));
        configs.add(4, new LocalConfigModel(resources.getString(R.string.N_APN2_LOGIN), ConfigConstants.APN2_LOGIN));
        configs.add(5, new LocalConfigModel(resources.getString(R.string.N_APN2_PASSWORD), ConfigConstants.APN2_PASSWORD));
    }

    public void addTemp(Resources resources, List<LocalConfigModel> configs) {
        int count = 1;
        for (int i = 0; i < ConfigConstants.temperatures.length - 1; i = i + 2) {
            configs.add(new LocalConfigModel(resources.getString(R.string.MIN_TEMPERATURE_SEC)
                    + " " + count, ConfigConstants.temperatures[i]));
            configs.add(new LocalConfigModel(resources.getString(R.string.MAX_TEMPERATURE_SEC)
                    + " " + count, ConfigConstants.temperatures[i + 1]));
            count++;
        }
    }
}
