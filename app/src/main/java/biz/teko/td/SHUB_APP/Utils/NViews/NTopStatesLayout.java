package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.Events.Adapters.NAffectsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public  class NTopStatesLayout extends LinearLayout
{
	public static  final int[] STATE_NO_SOCKET = {R.attr.state_no_socket};
	public static  final int[] STATE_ERROR_SOCKET = {R.attr.state_error_socket};
	public static  final int[] STATE_NO_LAN = {R.attr.state_no_lan};

	public static  final int[] STATE_BATTERY_HIGH = {R.attr.state_battery_high};
	public static  final int[] STATE_BATTERY_MID = {R.attr.state_battery_mid};
	public static  final int[] STATE_BATTERY_LOW = {R.attr.state_battery_low};
	private static final int[] STATE_BATTERY_EMPTY ={R.attr.state_battery_empty}; ;
	public static  final int[] STATE_BATTERY_NO = {R.attr.state_battery_no};
	public static  final int[] STATE_BATTERY_ERROR = {R.attr.state_battery_error};
	public static  final int[] STATE_R_BATTERY_HIGH = {R.attr.state_r_battery_high};
	public static  final int[] STATE_R_BATTERY_MID = {R.attr.state_r_battery_mid};
	public static  final int[] STATE_R_BATTERY_LOW = {R.attr.state_r_battery_low};
	private static final int[] STATE_R_BATTERY_EMPTY ={R.attr.state_r_battery_empty}; ;
	public static  final int[] STATE_R_BATTERY_NO = {R.attr.state_r_battery_no};
	public static  final int[] STATE_R_BATTERY_ERROR = {R.attr.state_r_battery_error};

	public static  final int[] STATE_LEVEL_TOP = {R.attr.state_level_top};
	public static  final int[] STATE_LEVEL_HIGH = {R.attr.state_level_high};
	public static  final int[] STATE_LEVEL_MID = {R.attr.state_level_mid};
	public static  final int[] STATE_LEVEL_LOW = {R.attr.state_level_low};

	public static  final int[] STATE_SIM_EXIST = {R.attr.state_sim_exist};
	public static  final int[] STATE_NO_SIGNAL_SIM = {R.attr.state_sim_no_signal};

	public static  final int[] STATE_SIM_1_EXIST = {R.attr.state_sim_1_exist};
	public static  final int[] STATE_NO_SIGNAL_SIM_1 = {R.attr.state_sim_1_no_signal};

	public static  final int[] STATE_SIM_2_EXIST = {R.attr.state_sim_2_exist};
	public static  final int[] STATE_NO_SIGNAL_SIM_2 = {R.attr.state_sim_2_no_signal};

	private final Context context;
	private int layout;

	private boolean no_socket;
	private boolean error_socket;
	private boolean no_lan;
	private boolean battery_high;
	private boolean battery_mid;
	private boolean battery_low;
	private boolean battery_empty;
	private boolean battery_no;
	private boolean battery_error;
	private boolean r_battery_high;
	private boolean r_battery_mid;
	private boolean r_battery_low;
	private boolean r_battery_empty;
	private boolean r_battery_no;
	private boolean r_battery_error;
	private boolean level_top;
	private boolean level_high;
	private boolean level_mid;
	private boolean level_low;
	private boolean sim_exist;
	private boolean sim_no_signal;
	private boolean sim1_exist;
	private boolean sim1_no_signal;
	private boolean sim2_exist;
	private boolean sim2_no_signal;

	private String socketTitle;
	private String socketSubTitle;
	private String batteryTitle;
	private String batterySubTitle;
	private String rBatteryTitle;
	private String rBatterySubTitle;
	private String levelTitle;
	private String levelSubTitle;
	private String lanTitle;
	private String lanSubTitle;
	private String simTitle;
	private String sim1Title;
	private String sim2Title;
	private String simSubTitle;
	private String sim1SubTitle;
	private String sim2SubTitle;
	private String simImsi;
	private String sim1Imsi;
	private String sim2Imsi;
	private String tempTitle;
	private String tempSubTitle;
	private String tempOutTitle;
	private String tempOutSubTitle;

	private EType type;
	private LinearLayout socketFrame;
	private TextView socketTitleView;
	private TextView socketSubTitleView;
	private LinearLayout batteryFrame;
	private TextView batteryTitleView;
	private TextView batterySubTitleView;
	private LinearLayout rBatteryFrame;
	private TextView rBatteryTitleView;
	private TextView rBatterySubTitleView;
	private LinearLayout lanFrame;
	private TextView lanTitleView;
	private TextView lanSubTitleView;
	private LinearLayout radioFrame;
	private TextView levelTitleView;
	private TextView levelSubTitleView;
	private LinearLayout simFrame;
	private TextView simTitleView;
	private TextView simImsiView;
	private TextView simSubTitleView;
	private LinearLayout sim1Frame;
	private TextView sim1TitleView;
	private TextView sim1ImsiView;
	private TextView sim1SubTitleView;
	private LinearLayout sim2Frame;
	private TextView sim2TitleView;
	private TextView sim2ImsiView;
	private TextView sim2SubTitleView;
	private LinearLayout tempFrame;
	private TextView tempTitleView;
	private TextView tempSubTitleView;
	private LinearLayout tempOutFrame;
	private TextView tempOutTitleView;
	private TextView tempOutSubTitleView;
	private LinkedList<Event> affects;
	private ListView affectsList;

	public void setSimImsi(String simImsi)
	{
		this.simImsi = simImsi;
	}

	public void setSim1Imsi(String sim1Imsi)
	{
		this.sim1Imsi = sim1Imsi;
	}

	public void setSim2Imsi(String sim2Imsi)
	{
		this.sim2Imsi = sim2Imsi;
	}

	public void setLanTitle(String lanTitle)
	{
		this.lanTitle = lanTitle;
	}

	public void setLanSubTitle(String lanSubTitle)
	{
		this.lanSubTitle = lanSubTitle;
	}

	public enum EType{
		device, device_sh_2, zone, zone_with_bu, zone_temp
	}

	public void setEType(EType type)
	{
		this.type = type;
		setTypeSet();
	}

	private void setTypeSet()
	{
		switch (type){
			case device:
				if(null!=radioFrame)radioFrame.setVisibility(GONE);
				if(null!=sim1Frame)sim1Frame.setVisibility(GONE);
				if(null!=sim2Frame)sim2Frame.setVisibility(GONE);
				if(null!=rBatteryFrame)rBatteryFrame.setVisibility(GONE);
				break;
			case device_sh_2:
				if(null!=radioFrame)radioFrame.setVisibility(GONE);
				if(null!=simFrame)simFrame.setVisibility(GONE);
				if(null!=rBatteryFrame)rBatteryFrame.setVisibility(GONE);
				break;
			case zone_temp:
				if(null!=tempOutFrame) tempOutFrame.setVisibility(VISIBLE);
			case zone:
				if(null!=rBatteryFrame)rBatteryFrame.setVisibility(GONE);
			case zone_with_bu:
				if(null!=socketFrame)socketFrame.setVisibility(GONE);
				if(null!=lanFrame)lanFrame.setVisibility(GONE);
				if(null!=simFrame)simFrame.setVisibility(GONE);
				if(null!=sim1Frame)sim1Frame.setVisibility(GONE);
				if(null!=sim2Frame)sim2Frame.setVisibility(GONE);
			default:
				break;
		}
	}

	public EType getType()
	{
		return type;
	}

	public NTopStatesLayout(Context context)
	{
		super(context);
		this.context = context;
	}

	public NTopStatesLayout(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context  = context;
		parseAttributes(attrs);
		setup();
	}

	private void setup()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
		layoutInflater.inflate(layout, this, true);

		socketFrame = (LinearLayout) findViewById(R.id.nSocketFrame);
		socketTitleView = (TextView) findViewById(R.id.nTextSocketState);
		socketSubTitleView = (TextView) findViewById(R.id.nTextSocketTime);

		batteryFrame = (LinearLayout) findViewById(R.id.nBatteryFrame);
		batteryTitleView = (TextView)findViewById(R.id.nTextBatteryState);
		batterySubTitleView = (TextView) findViewById(R.id.nTextBatteryTime);

		rBatteryFrame = (LinearLayout) findViewById(R.id.nRBatteryFrame);
		rBatteryTitleView = (TextView) findViewById(R.id.nTextRBatteryState);
		rBatterySubTitleView = (TextView) findViewById(R.id.nTextRBatteryTime);

		lanFrame = (LinearLayout) findViewById(R.id.nLanFrame);
		lanTitleView = (TextView) findViewById(R.id.nLanState);
		lanSubTitleView = (TextView) findViewById(R.id.nLanTime);

		radioFrame = (LinearLayout) findViewById(R.id.nRadioFrame);
		levelTitleView = (TextView) findViewById(R.id.nRadioState);
		levelSubTitleView = (TextView) findViewById(R.id.nRadioTime);

		simFrame = (LinearLayout) findViewById(R.id.nSimFrame);
		simTitleView = (TextView) findViewById(R.id.nSimState);
		simImsiView = (TextView) findViewById(R.id.nSimIMSI);
		simSubTitleView = (TextView) findViewById(R.id.nSimTime);

		sim1Frame = (LinearLayout) findViewById(R.id.nSim1Frame);
		sim1TitleView = (TextView) findViewById(R.id.nSim1State);
		sim1ImsiView = (TextView) findViewById(R.id.nSim1IMSI);
		sim1SubTitleView = (TextView) findViewById(R.id.nSim1Time);

		sim2Frame = (LinearLayout) findViewById(R.id.nSim2Frame);
		sim2TitleView = (TextView) findViewById(R.id.nSim2State);
		sim2ImsiView = (TextView) findViewById(R.id.nSim2IMSI);
		sim2SubTitleView = (TextView) findViewById(R.id.nSim2Time);

		tempFrame = (LinearLayout) findViewById(R.id.nTempFrame);
		tempTitleView = (TextView) findViewById(R.id.nTempState);
		tempSubTitleView = (TextView) findViewById(R.id.nTempTime);

		tempOutFrame = (LinearLayout) findViewById(R.id.nTempOutFrame);
		tempOutTitleView = (TextView) findViewById(R.id.nTempOutState);
		tempOutSubTitleView = (TextView) findViewById(R.id.nTempOutTime);

		affectsList = (ListView) findViewById(R.id.nListAffects);

	}

	public void bind()
	{
		setSocketView();
		setBatteryView();
		setRBatteryView();
		setLanView();
		setLevelVeiw();
		setSimView();
		setSim1View();
		setSim2View();
		setTempView();
		setTempOutView();
		setAffectsFrame();
	}

	private void setAffectsFrame()
	{
		if(null!=affects && null!=affectsList){
			NAffectsListAdapter adapter = new NAffectsListAdapter(context, R.layout.n_affects_list_element, affects);
			affectsList.setAdapter(adapter);
			Func.setDynamicHeight(affectsList);
		}
	}

	private void setSocketView()
	{
		setSocketTitleView();
		setSocketSubTitleView();
	}

	private void setSocketSubTitleView()
	{
		if(null!=socketSubTitleView){
			if(null!=socketSubTitle){
				socketSubTitleView.setText(socketSubTitle);
				socketSubTitleView.setVisibility(VISIBLE);
			}else{
				socketSubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setSocketTitleView()
	{
		if(null!=socketTitleView){
			if(null!=socketTitle){
				socketTitleView.setText(socketTitle);
			}else{
				socketTitleView.setText(R.string.N_SOCKET_OK);
			}
		}
	}

	private void setBatteryView()
	{
		setBatteryTitleView();
		setBatterySubTitleView();
	}

	private void setBatteryTitleView()
	{
		if(null!=batteryTitleView){
			if(null!=batteryTitle){
				batteryTitleView.setText(batteryTitle);
			}else{
				batteryTitleView.setText(R.string.N_BATT_OK);
			}
		}
	}

	private void setBatterySubTitleView()
	{
		if(null!=batterySubTitleView){
			if(null!=batterySubTitle){
				batterySubTitleView.setText(batterySubTitle);
				batterySubTitleView.setVisibility(VISIBLE);
			}else{
				batterySubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setRBatteryView()
	{
		setRBattertyTitleView();
		setRBatterySubTitleView();
	}

	private void setRBattertyTitleView()
	{
		if(null!=rBatteryTitleView){
			if(null!=rBatteryTitle){
				rBatteryTitleView.setText(rBatteryTitle);
			}else{
				rBatteryTitleView.setText(R.string.N_R_BATT_OK);
			}
		}
	}

	private void setRBatterySubTitleView()
	{
		if(null!=rBatterySubTitleView){
			if(null!=rBatterySubTitle){
				rBatterySubTitleView.setText(rBatterySubTitle);
				rBatterySubTitleView.setVisibility(VISIBLE);
			}else{
				rBatterySubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setLanView()
	{
		setLanTitleView();
		setLanSubTitleView();
	}

	private void setLanTitleView()
	{
		if(null!=lanTitleView){
			if(null!=lanTitle){
				lanTitleView.setText(lanTitle);
			}else{
				lanTitleView.setText(R.string.N_LAN_OK);
			}
		}
	}

	private void setLanSubTitleView()
	{
		if(null!=lanSubTitleView){
			if(null!=lanSubTitle){
				lanSubTitleView.setText(lanSubTitle);
				lanSubTitleView.setVisibility(VISIBLE);
			}else{
				lanSubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setLevelVeiw()
	{
		setLevelTitleView();
		setLevelSubTitleView();
	}

	private void setLevelTitleView()
	{
		if(null!= levelTitleView){
			if(null!=levelTitle){
				levelTitleView.setText(levelTitle);
			}else{
				levelTitleView.setText(R.string.N_LEVEL_UNKNOWN);
			}
		}
	}

	private void setLevelSubTitleView()
	{
		if(null!= levelSubTitleView){
			if(null!=levelSubTitle)
			{
				levelSubTitleView.setText(levelSubTitle);
				levelSubTitleView.setVisibility(VISIBLE);
			}else{
				levelSubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setSimView()
	{
		setSimTitleView();
		setSimSubTitleView();
		setSimImsiView();
	}

	private void setSimTitleView()
	{
		if(null!=simTitleView){
			if(null!=simTitle){
				simTitleView.setText(simTitle);
			}else{
				simTitleView.setText(R.string.N_SIM_NO);
			}
		}
	}

	private void setSimSubTitleView()
	{
		if(null!=simSubTitleView){
			if(null!=simSubTitle){
				simSubTitleView.setText(simSubTitle);
				simSubTitleView.setVisibility(VISIBLE);
			}else{
				simSubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setSimImsiView()
	{
		if(null!=simImsiView){
			if(null!=simImsi){
				simImsiView.setText(simImsi);
				simImsiView.setVisibility(VISIBLE);
			}else{
				simImsiView.setVisibility(GONE);
			}
		}
	}

	private void setSim1View()
	{
		setSim1TitleView();
		setSim1SubTitleView();
		setSim1ImsiView();
	}

	private void setSim1TitleView()
	{
		if(null!=sim1TitleView){
			if(null!=sim1Title){
				sim1TitleView.setText(sim1Title);
			}else{
				sim1TitleView.setText(R.string.N_SIM_NO);
			}
		}
	}

	private void setSim1SubTitleView()
	{
		if(null!=sim1SubTitleView){
			if(null!=sim1SubTitle){
				sim1SubTitleView.setText(sim1SubTitle);
				sim1SubTitleView.setVisibility(VISIBLE);
			}else{
				sim1SubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setSim1ImsiView()
	{
		if(null!=sim1ImsiView){
			if(null!=sim1Imsi){
				sim1ImsiView.setText(sim1Imsi);
				sim1ImsiView.setVisibility(VISIBLE);
			}else{
				sim1ImsiView.setVisibility(GONE);
			}
		}
	}

	private void setSim2View()
	{
		setSim2TitleView();
		setSim2SubTitleView();
		setSim2ImsiView();
	}

	private void setSim2TitleView()
	{
		if(null!=sim2TitleView){
			if(null!=sim2Title){
				sim2TitleView.setText(sim2Title);
			}else{
				sim2TitleView.setText(R.string.N_SIM_NO);
			}
		}
	}

	private void setSim2SubTitleView()
	{
		if(null!=sim2SubTitleView){
			if(null!=sim2SubTitle){
				sim2SubTitleView.setText(sim2SubTitle);
				sim2SubTitleView.setVisibility(VISIBLE);
			}else{
				sim2SubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setSim2ImsiView()
	{
		if(null!=sim2ImsiView){
			if(null!=sim2Imsi){
				sim2ImsiView.setText(sim2Imsi);
				sim2ImsiView.setVisibility(VISIBLE);
			}else{
				sim2ImsiView.setVisibility(GONE);
			}
		}
	}

	private void setTempView(){
		setTempTitleView();
		setTempSubTitleView();
	}

	private void setTempTitleView()
	{
		if(null!= tempTitleView){
			if(null!=tempTitle){
				tempTitleView.setText(tempTitle);
			}else{
				tempTitleView.setText(R.string.N_TEMP_NO_DATA);
			}
		}
	}

	private void setTempSubTitleView()
	{
		if(null!=tempSubTitleView){
			if(null!=tempSubTitle){
				tempSubTitleView.setText(tempSubTitle);
				tempSubTitleView.setVisibility(VISIBLE);
			}else{
				tempSubTitleView.setVisibility(GONE);
			}
		}
	}

	private void setTempOutView(){
		setTempOutTitleView();
		setTempOutSubTitleView();
	}

	private void setTempOutTitleView()
	{
		if(null!= tempOutTitleView){
			if(null!=tempOutTitle){
				tempOutTitleView.setText(tempOutTitle);
			}else{
				tempOutTitleView.setText(R.string.N_TEMP_NO_DATA);
			}
		}
	}

	private void setTempOutSubTitleView()
	{
		if(null!=tempOutSubTitleView){
			if(null!=tempOutSubTitle){
				tempOutSubTitleView.setText(tempOutSubTitle);
				tempOutSubTitleView.setVisibility(VISIBLE);
			}else{
				tempOutSubTitleView.setVisibility(GONE);
			}
		}
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NTopStatesLayout, 0, 0);
		try{
			layout = a.getResourceId(R.styleable.NTopStatesLayout_NTopStateLayout, 0);
		}finally
		{
			a.recycle();
		}
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 19);

		if(no_socket) mergeDrawableStates(drawableState, STATE_NO_SOCKET);
		if(error_socket) mergeDrawableStates(drawableState, STATE_ERROR_SOCKET);
		if(no_lan) mergeDrawableStates(drawableState, STATE_NO_LAN);
		if(battery_high) mergeDrawableStates(drawableState, STATE_BATTERY_HIGH);
		if(battery_mid) mergeDrawableStates(drawableState, STATE_BATTERY_MID);
		if(battery_low) mergeDrawableStates(drawableState, STATE_BATTERY_LOW);
		if(battery_empty) mergeDrawableStates(drawableState, STATE_BATTERY_EMPTY);
		if(battery_no) mergeDrawableStates(drawableState, STATE_BATTERY_NO);
		if(battery_error) mergeDrawableStates(drawableState, STATE_BATTERY_ERROR);
		if(r_battery_high) mergeDrawableStates(drawableState, STATE_R_BATTERY_HIGH);
		if(r_battery_mid) mergeDrawableStates(drawableState, STATE_R_BATTERY_MID);
		if(r_battery_low) mergeDrawableStates(drawableState, STATE_R_BATTERY_LOW);
		if(r_battery_empty) mergeDrawableStates(drawableState, STATE_R_BATTERY_EMPTY);
		if(r_battery_no) mergeDrawableStates(drawableState, STATE_R_BATTERY_NO);
		if(r_battery_error) mergeDrawableStates(drawableState, STATE_R_BATTERY_ERROR);
		if(level_top) mergeDrawableStates(drawableState, STATE_LEVEL_TOP);
		if(level_high) mergeDrawableStates(drawableState, STATE_LEVEL_HIGH);
		if(level_mid) mergeDrawableStates(drawableState, STATE_LEVEL_MID);
		if(level_low) mergeDrawableStates(drawableState, STATE_LEVEL_LOW);
		if(sim_exist) mergeDrawableStates(drawableState, STATE_SIM_EXIST);
		if(sim_no_signal) mergeDrawableStates(drawableState, STATE_NO_SIGNAL_SIM);
		if(sim1_exist) mergeDrawableStates(drawableState, STATE_SIM_1_EXIST);
		if(sim1_no_signal) mergeDrawableStates(drawableState, STATE_NO_SIGNAL_SIM_1);
		if(sim2_exist) mergeDrawableStates(drawableState, STATE_SIM_2_EXIST);
		if(sim2_no_signal) mergeDrawableStates(drawableState, STATE_NO_SIGNAL_SIM_2);

		return  drawableState;
	}

	public void setStates(LinkedList<Event> affects, int zone_id)
	{
		this.affects = new LinkedList<>();

		LinkedList<String> batteryList = new LinkedList<>();
		LinkedList<String> rBatteryList= new LinkedList<>();
		LinkedList<String> socketList = new LinkedList<>();
		LinkedList<String> lanList = new LinkedList<>();
		LinkedList<String> levelList = new LinkedList<>();
		LinkedList<String> simList = new LinkedList<>();
		LinkedList<String> sim1List = new LinkedList<>();
		LinkedList<String> sim2List = new LinkedList<>();
		LinkedList<String> tempList = new LinkedList<>();
		LinkedList<String> tempOutList = new LinkedList<>();

		boolean sim_exist = false;
		boolean sim1_exist = false;
		boolean sim2_exist = false;

		boolean sim_no_signal = false;
		boolean sim1_no_signal = false;
		boolean sim2_no_signal = false;

		long sim_time = 0;
		long sim1_time = 0;
		long sim2_time = 0;

		if(null!=affects){
			for(Event event:affects){
				switch (event.classId){
					case Const.CLASS_MALFUNCTION:
						case Const.REASON_MAFL_BATT_LOW:
							setBattery_error(true);
							batteryList.add(event.explainAffectRegDescription(context));
							setBatterySubTitle(Func.getServerTimeDateTextShort(context, event.time));
							break;
						case Const.REASON_MAFL_BATT_NO:
							setBattery_no(true);
							batteryList.add(event.explainAffectRegDescription(context));
							setBatterySubTitle(Func.getServerTimeDateTextShort(context, event.time));
							break;
					case Const.CLASS_SUPPLY:
						switch (event.reasonId){
							case Const.REASON_BATTERY_NORESERVE:
								setR_battery_no(true);
								rBatteryList.add(event.explainAffectRegDescription(context));
								setSocketSubTitle(Func.getServerTimeDateTextShort(context, event.time));
								break;
							case Const.REASON_BATTERY_SUPPLY:
								this.affects.add(event);
							case Const.REASON_SOCKET_FAULT:
								setNo_socket(true);
								socketList.add(event.explainAffectRegDescription(context));
								setSocketSubTitle(Func.getServerTimeDateTextShort(context, event.time));
								break;
							case Const.REASON_BATTERY_MAINSUNDER:
							case Const.REASON_BATTERY_MAINOVER:
							case Const.REASON_BATTERY_RESERVE:
								setError_socket(true);
								socketList.add(event.explainEventRegDescription(context));
								setSocketSubTitle(Func.getServerTimeDateTextShort(context, event.time));
								break;
							case Const.REASON_BATTERY_LEVEL:
								batteryList = analyzeBatteryLevel(batteryList, event);
								break;
							case Const.REASON_BATTERY_BATTLOW:
							case Const.REASON_BATTERY_ERROR:
								setBattery_error(true);
								batteryList.add(event.explainAffectRegDescription(context));
								setBatterySubTitle(Func.getServerTimeDateTextShort(context, event.time));
								break;
							case Const.REASON_BATTERY_RESBATTLEVEL:
								analyzeResBatteryLevel(event);
								break;
							case Const.REASON_BATTERY_RESBATTLOW:
							case Const.REASON_BATTERY_RESBATTERROR:
								setR_battery_error(true);
								rBatteryList.add(event.explainAffectRegDescription(context));
								setrBatterySubTitle(Func.getServerTimeDateTextShort(context, event.time));
							default:
								break;
						}
						break;
					case Const.CLASS_CONNECTIVITY:
						switch (event.reasonId){
							case Const.REASON_CONNECTIVITY_UNAVAILABLE:
								if(event.detectorId == Const.DETECTOR_MODEM){
									sim_no_signal = true;
//									setSim_no_signal(true);
									simList.add(event.explainAffectRegDescription(context));
									sim_time = Math.max(event.time, sim_time);
									sim1_time = Math.max(event.time, sim1_time);
									sim2_time = Math.max(event.time, sim2_time);
//									setSimSubTitle(Func.getServerTimeDateTextShort(event.time));
//									setSim1SubTitle(Func.getServerTimeDateTextShort(event.time));
//									setSim2SubTitle(Func.getServerTimeDateTextShort(event.time));
								}else if(event.detectorId == Const.DETECTOR_GPRS_1)
								{
									sim1_no_signal = true;
//									setSim1_no_signal(true);
									sim1List.add(event.explainAffectRegDescription(context));
									sim1_time = Math.max(event.time, sim1_time);
//									setSim1SubTitle(Func.getServerTimeDateTextShort(event.time));
								}else if(event.detectorId == Const.DETECTOR_GRPS_2){
									sim2_no_signal = true;
//									setSim2_no_signal(true);
									sim2List.add(event.explainAffectRegDescription(context));
									sim2_time = Math.max(event.time, sim1_time);
//									setSim2SubTitle(Func.getServerTimeDateTextShort(event.time));
								}else if (event.detectorId == Const.DETECTOR_ETHERNET){
									setNo_lan(true);
									lanList.add(event.explainAffectRegDescription(context));
									setLanSubTitle(Func.getServerTimeDateTextShort(context, event.time));
								}
								break;
							case Const.REASON_CONNECTIVITY_NOCARRIER:
							case Const.REASON_CONNECTIVITY_NOANSWER:
								this.affects.add(event);
								break;
							case Const.REASON_CONNECTIVITY_LEVEL:
								if(event.detectorId == Const.DETECTOR_MODEM){
									switch (event.getBearer()){
										case 1:
											sim1List.add(event.explainAffectRegDescription(context));
//											setSim1SubTitle(Func.getServerTimeDateTextShort(event.time));
											sim1_time = Math.max(event.time, sim1_time);
											break;
										case 2:
											sim2List.add(event.explainAffectRegDescription(context));
//											setSim2SubTitle(Func.getServerTimeDateTextShort(event.time));
											sim2_time = Math.max(event.time, sim2_time);
											break;
										default:
											simList.add(event.explainAffectRegDescription(context));
											sim_time = Math.max(event.time, sim_time);
//											setSimSubTitle(Func.getServerTimeDateTextShort(event.time));
											break;
									}
								}else if(event.detectorId == Const.DETECTOR_GPRS_1)
								{
									sim1List.add(event.explainAffectRegDescription(context));
									sim1_time = Math.max(event.time, sim1_time);
//									setSim1SubTitle(Func.getServerTimeDateTextShort(event.time));
								}else if(event.detectorId == Const.DETECTOR_GRPS_2){
									sim2List.add(event.explainAffectRegDescription(context));
									sim2_time = Math.max(event.time, sim2_time);
//									setSim2SubTitle(Func.getServerTimeDateTextShort(event.time));
								}else{
									levelList = analyzeConnectivityLevel(levelList, event);
								}
								break;
							case Const.REASON_CONNECTIVITY_IMSI:
								if(event.detectorId == Const.DETECTOR_MODEM){
									switch (event.getBearer()){
										case 1:
											sim1_exist = true;
//											setSim1_exist(true);
											setSim1Imsi(event.explainAffectRegDescription(context));
											sim1_time = Math.max(event.time, sim1_time);
//											setSim1SubTitle(Func.getServerTimeDateTextShort(event.time));
											break;
										case 2:
											sim2_exist = true;
//											setSim2_exist(true);
											setSim2Imsi(event.explainAffectRegDescription(context));
											sim2_time = Math.max(event.time, sim2_time);
//											setSim2SubTitle(Func.getServerTimeDateTextShort(event.time));
											break;
										default:
											sim_exist = true;
//											setSim_exist(true);
											setSimImsi(event.explainAffectRegDescription(context));
											sim_time = Math.max(event.time, sim_time);
//											setSimSubTitle(Func.getServerTimeDateTextShort(event.time));
											break;
									}
								}else if(event.detectorId == Const.DETECTOR_GPRS_1)
								{
									sim1_exist = true;
//									setSim1_exist(true);
									setSim1Imsi(event.explainAffectRegDescription(context));
									sim1_time = Math.max(event.time, sim1_time);
//									setSim1SubTitle(Func.getServerTimeDateTextShort(event.time));
								}else if(event.detectorId == Const.DETECTOR_GRPS_2){

									sim2_exist = true;
//									setSim2_exist(true);
									setSim2Imsi(event.explainAffectRegDescription(context));
									sim2_time = Math.max(event.time, sim2_time);
//									setSim2SubTitle(Func.getServerTimeDateTextShort(event.time));
								}
								break;
							case Const.REASON_CONNECTIVITY_JAM:
								this.affects.add(event);
									break;
							default:
								break;
						}
						break;
					case Const.CLASS_TELEMETRY:
						switch (event.reasonId){
							case Const.REASON_TEMPERATURE:
								if(event.zone == zone_id)
								{
									tempList.add(event.explainAffectRegDescription(context));
									setTempSubTitle(Func.getServerTimeDateTextShort(context, event.time));
								}else{
									tempOutList.add(event.explainAffectRegDescription(context));
									setTempOutSubTitle(Func.getServerTimeDateTextShort(context, event.time));
								}
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}
			}
		}

		if(batteryList.size() > 0){
			String _s = "";
			for(String b: batteryList){
				_s+=b + "\n";
			}
			setBatteryTitle(_s.substring(0, _s.length()-1));
		}else{
			setBatteryTitle(null);
		}

		if(rBatteryList.size() > 0){
			String _s = "";
			for(String b: rBatteryList){
				_s+=b + "\n";
			}
			setrBatteryTitle(_s.substring(0, _s.length()-1));
		}else{
			setrBatteryTitle(null);
		}

		if(socketList.size() > 0){
			String _s = "";
			for(String s: socketList){
				_s+=s + "\n";
			}
			setSocketTitle(_s.substring(0, _s.length()-1));
		}else{
			setSocketTitle(null);
		}

		if(lanList.size() > 0){
			String _s = "";
			for(String s: lanList){
				_s+=s + "\n";
			}
			setLanTitle(_s.substring(0, _s.length()-1));
		}else{
			setLanTitle(null);
		}

		if(levelList.size() > 0){
			String _s = "";
			for(String s: levelList){
				_s+=s + "\n";
			}
			setLevelTitle(_s.substring(0, _s.length()-1));
		}else{
			setLevelTitle(null);
		}


		setSim_exist(sim_exist);
		if(sim_exist){
			setSim_no_signal(sim_no_signal);
			if(simList.size() > 0){
				String _s = "";
				for(String s: simList){
					_s+=s + "\n";
				}
				setSimTitle(_s.substring(0, _s.length()-1));
			}else{
				setSimTitle(getContext().getString(R.string.N_NO_LEVEL_DATA));
			}
			setSimSubTitle(Func.getServerTimeDateTextShort(context, sim_time));
		}else{
			setSimTitle(null);
			setSimSubTitle(null);
		}

		setSim1_exist(sim1_exist);
		if(sim1_exist){
			setSim1_no_signal(sim1_no_signal || sim_no_signal);
			if(sim1_no_signal || sim_no_signal){
				setSim1Title(getContext().getString(R.string.N_NO_MOBILE_CONNECTION));
			}else if(sim1List.size() > 0){
				String _s = "";
				for(String s: sim1List){
					_s+=s + "\n";
				}
				setSim1Title(_s.substring(0, _s.length()-1));
			}else{
				setSim1Title(getContext().getString(R.string.N_NO_LEVEL_DATA));
			}
			setSim1SubTitle(Func.getServerTimeDateTextShort(context, sim1_time));
		}else{
			setSim1Title(null);
			setSim1SubTitle(null);
		}

		setSim2_exist(sim2_exist);
		if(sim2_exist){
			setSim2_no_signal(sim2_no_signal || sim_no_signal);
			if(sim2_no_signal || sim_no_signal){
				setSim2Title(getContext().getString(R.string.N_NO_MOBILE_CONNECTION));
			}else if(sim2List.size() > 0){
				String _s = "";
				for(String s: sim2List){
					_s+=s + "\n";
				}
				setSim2Title(_s.substring(0, _s.length()-1));
			}else{
				setSim2Title(getContext().getString(R.string.N_NO_LEVEL_DATA));
			}
			setSim2SubTitle(Func.getServerTimeDateTextShort(context, sim2_time));
		}else{
			setSim2Title(null);
			setSim2SubTitle(null);
		}

		if(tempList.size() > 0){
			String _s = "";
			for(String s: tempList){
				_s+=s + "\n";
			}
			setTempTitle(_s.substring(0, _s.length()-1));
		}else{
			setTempTitle(null);
		}

		if(tempOutList.size() > 0){
			String _s = "";
			for(String s: tempOutList){
				_s+=s + "\n";
			}
			setTempOutTitle(_s.substring(0, _s.length()-1));
		}else{
			setTempOutTitle(null);
		}
	}

	public LinkedList<String> analyzeBatteryLevel(LinkedList<String> list, Event event)
	{
		String level = event.getLevel();
		if(null!=level){
			try{
				int levelI  = Integer.parseInt(level.substring(0, level.indexOf(" ")));

				if(levelI < 10){
					setBattery_empty(true);
				}else if(levelI < 50){
					setBattery_low(true);
				}else if(levelI < 90){
					setBattery_mid(true);
				}else{
					setBattery_high(true);
				}
			}catch (Exception exception){
				setBattery_mid(true);
			}
		}
		setBatterySubTitle(Func.getServerTimeDateTextShort(context, event.time));
		list.add(event.explainAffectRegDescription(context));
		return list;
	}

	public void analyzeResBatteryLevel(Event event)
	{
		String level = event.getLevel();
		if(null!=level){
			try{
				int levelI  = Integer.parseInt(level.substring(0, level.indexOf(" ")));

				if(levelI < 10){
					setR_battery_empty(true);
				}else if(levelI < 50){
					setR_battery_low(true);
				}else if(levelI < 90){
					setR_battery_mid(true);
				}else{
					setR_battery_high(true);
				}
			}catch (Exception exception){
				setR_battery_mid(true);
			}
		}
		setrBatteryTitle(event.explainAffectRegDescription(context));
		setrBatterySubTitle(Func.getServerTimeDateTextShort(context, event.time));
	}

	public LinkedList<String > analyzeConnectivityLevel(LinkedList<String> list, Event event)
	{
		String level = event.getLevel();
		if(null!=level){
			try{
				int levelI  = Integer.parseInt(level.substring(0, level.indexOf(" ")));
				if(levelI < 10){
					setLevel_low(true);
				}else if(levelI < 50){
					setLevel_mid(true);
				}else if(levelI < 90){
					setLevel_high(true);
				}else{
					setLevel_top(true);
				}
			}catch (Exception exception){
				setLevel_mid(true);
			}
		}
		setLevelSubTitle(Func.getServerTimeDateTextShort(context, event.time));
		list.add(event.explainAffectRegDescription(context));
		return list;
	}



	public void setSim2SubTitle(String sim2SubTitle)
	{
		this.sim2SubTitle = sim2SubTitle;
	}

	public String getSim2SubTitle()
	{
		return sim2SubTitle;
	}

	public void setSim1SubTitle(String sim1SubTitle)
	{
		this.sim1SubTitle = sim1SubTitle;
	}

	public String getSim1SubTitle()
	{
		return sim1SubTitle;
	}

	public void setSimSubTitle(String simSubTitle)
	{
		this.simSubTitle = simSubTitle;
	}

	public String getSimSubTitle()
	{
		return simSubTitle;
	}

	public void setSim2Title(String sim2Title)
	{
		this.sim2Title = sim2Title;
	}

	public String getSim2Title()
	{
		return sim2Title;
	}

	public void setSim1Title(String sim1Title)
	{
		this.sim1Title = sim1Title;
	}

	public String getSim1Title()
	{
		return sim1Title;
	}

	public void setSimTitle(String simTitle)
	{
		this.simTitle = simTitle;
	}

	public String getSimTitle()
	{
		return simTitle;
	}

	public void setLevelTitle(String levelTitle)
	{
		this.levelTitle = levelTitle;
	}

	public String getLevelTitle()
	{
		return levelTitle;
	}

	public void setrBatteryTitle(String rBatteryTitle)
	{
		this.rBatteryTitle = rBatteryTitle;
	}

	public String getrBatteryTitle()
	{
		return rBatteryTitle;
	}

	public void setBatteryTitle(String batteryTitle)
	{
		this.batteryTitle = batteryTitle;
	}

	public String getBatteryTitle()
	{
		return batteryTitle;
	}

	public void setSocketTitle(String socketTitle)
	{
		this.socketTitle = socketTitle;
	}

	public String getSocketTitle()
	{
		return socketTitle;
	}

	public void setSocketSubTitle(String socketSubTitle)
	{
		this.socketSubTitle = socketSubTitle;
	}

	public void setBatterySubTitle(String batterySubTitle)
	{
		this.batterySubTitle = batterySubTitle;
	}

	public void setrBatterySubTitle(String rBatterySubTitle)
	{
		this.rBatterySubTitle = rBatterySubTitle;
	}

	public void setLevelSubTitle(String levelSubTitle)
	{
		this.levelSubTitle = levelSubTitle;
	}

	public void setTempTitle(String tempTitle)
	{
		this.tempTitle = tempTitle;
	}

	public void setTempSubTitle(String tempSubTitle)
	{
		this.tempSubTitle = tempSubTitle;
	}

	public void setTempOutTitle(String tempOutTitle)
	{
		this.tempOutTitle = tempOutTitle;
	}

	public void setTempOutSubTitle(String tempOutSubTitle)
	{
		this.tempOutSubTitle = tempOutSubTitle;
	}

	public String getSocketSubTitle()
	{
		return socketSubTitle;
	}

	public String getBatterySubTitle()
	{
		return batterySubTitle;
	}

	public String getrBatterySubTitle()
	{
		return rBatterySubTitle;
	}

	public String getLevelSubTitle()
	{
		return levelSubTitle;
	}

	public String getTempTitle()
	{
		return tempTitle;
	}

	public String getTempSubTitle()
	{
		return tempSubTitle;
	}

	/* STATES */

	public void setNo_socket(boolean no_socket)
	{
		this.no_socket = no_socket;
		refreshDrawableState();
	}

	public void setError_socket(boolean error_socket)
	{
		this.error_socket = error_socket;
		refreshDrawableState();
	}

	public void setBattery_high(boolean battery_high)
	{
		this.battery_high = battery_high;
		refreshDrawableState();
	}

	public void setBattery_mid(boolean battery_mid)
	{
		this.battery_mid = battery_mid;
		refreshDrawableState();
	}

	public void setBattery_low(boolean battery_low)
	{
		this.battery_low = battery_low;
		refreshDrawableState();
	}

	public void setBattery_empty(boolean battery_empty)
	{
		this.battery_empty = battery_empty;
		refreshDrawableState();
	}

	public void setBattery_no(boolean battery_no)
	{
		this.battery_no = battery_no;
		refreshDrawableState();
	}

	public void setBattery_error(boolean battery_error)
	{
		this.battery_error = battery_error;
		refreshDrawableState();
	}

	public void setR_battery_high(boolean r_battery_high)
	{
		this.r_battery_high = r_battery_high;
		refreshDrawableState();
	}

	public void setR_battery_mid(boolean r_battery_mid)
	{
		this.r_battery_mid = r_battery_mid;
		refreshDrawableState();
	}

	public void setR_battery_low(boolean r_battery_low)
	{
		this.r_battery_low = r_battery_low;
		refreshDrawableState();
	}

	public void setR_battery_empty(boolean r_battery_empty)
	{
		this.r_battery_empty = r_battery_empty;
		refreshDrawableState();
	}

	public void setR_battery_no(boolean r_battery_no)
	{
		this.r_battery_no = r_battery_no;
		refreshDrawableState();
	}

	public void setR_battery_error(boolean r_battery_error)
	{
		this.r_battery_error = r_battery_error;
		refreshDrawableState();
	}

	public void setLevel_top(boolean level_top)
	{
		this.level_top = level_top;
		refreshDrawableState();
	}

	public void setLevel_high(boolean level_high)
	{
		this.level_high = level_high;
		refreshDrawableState();
	}

	public void setLevel_mid(boolean level_mid)
	{
		this.level_mid = level_mid;
		refreshDrawableState();
	}

	public void setLevel_low(boolean level_low)
	{
		this.level_low = level_low;
		refreshDrawableState();
	}

	public void setNo_lan(boolean no_lan)
	{
		this.no_lan = no_lan;
	}

	public void setSim_exist(boolean sim_exist)
	{
		this.sim_exist = sim_exist;
		refreshDrawableState();
	}

	public void setSim_no_signal(boolean sim_no_signal)
	{
		this.sim_no_signal = sim_no_signal;
		refreshDrawableState();
	}

	public void setSim1_exist(boolean sim1_exist)
	{
		this.sim1_exist = sim1_exist;
		refreshDrawableState();
	}

	public void setSim1_no_signal(boolean sim1_no_signal)
	{
		this.sim1_no_signal = sim1_no_signal;
		refreshDrawableState();
	}

	public void setSim2_exist(boolean sim2_exist)
	{
		this.sim2_exist = sim2_exist;
		refreshDrawableState();
	}

	public void setSim2_no_signal(boolean sim2_no_signal)
	{
		this.sim2_no_signal = sim2_no_signal;
		refreshDrawableState();
	}
}
