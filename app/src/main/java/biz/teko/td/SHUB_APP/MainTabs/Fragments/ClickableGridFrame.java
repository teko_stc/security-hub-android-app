package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import androidx.fragment.app.Fragment;

public interface ClickableGridFrame {
    void setClickListener(MainGridFragment.OnClickListener onClickListener);

    Fragment getFragment();
}
