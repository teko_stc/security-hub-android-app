package biz.teko.td.SHUB_APP.Application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.multidex.MultiDex;
import biz.teko.td.SHUB_APP.Billing.BillingHelper;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.Utils.ActivityLifecycleCall;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 20.07.2017.
 */

public class App extends Application
{
	public static final String FIREBASE_TOKEN = "firebase_token";
	public static final String DEFAULT_ISO = "en";
	private static final String DEFAULT_BLOCK_TIME_VALUE = "20";

	private static WeakReference<Context> context;
	private static LinkedList<Snackbar> snackBarList;
	private  String previousActivity;
	private WebHelper webHelper;
	private DBHelper dbHelper;
	private static SharedPreferences sp;
	private static boolean master;
	private BillingHelper billingHelper;
	private PrefUtils prefUtils;




	public static LinkedList<Snackbar> getSnackBarQueue(){
		//queue refresh when transition timer starts
		if(snackBarList == null){
			snackBarList = new LinkedList<Snackbar>();
		}
		return snackBarList;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		newContext();

		webHelper = WebHelper.getInstance();
		dbHelper = DBHelper.getInstance(getContext());
		prefUtils = PrefUtils.getInstance(getContext());
//		NDahuaHelper.getInstance(getContext());

		registerActivityLifecycleCallbacks(new ActivityLifecycleCall());
		AndroidThreeTen.init(this);

		sp = PreferenceManager.getDefaultSharedPreferences(getContext());

		FirebaseMessaging.getInstance().getToken()
				.addOnCompleteListener(task -> {
					try
					{
						String refreshedToken = task.getResult();
						SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
						preferences.edit().putString(FIREBASE_TOKEN, refreshedToken).apply();
						Func.log_d("D3ServiceLog", "Refreshed token: " + refreshedToken);
					}catch (Exception e){}
				});
	}

	public static boolean getMode(){
		return  Func.isMasterMode(sp);
	}

	public void newContext(){
		context = new WeakReference<Context>(this);
	}

	public static void updateContext()
	{
		context = new WeakReference<Context>(getContext());
	}

	public static Context getContext(){
		return context.get();
	}

	public static DBHelper getDBHelper()
	{
		return DBHelper.getInstance(getContext());
	}

	private Timer mActivityTransitionTimer;
	private TimerTask mActivityTransitionTimerTask;
	private boolean foreGround = false;
	private boolean blocked = true;

	private long MAX_ACTIVITY_TRANSITION_TIME_MS = 5000;

	public void startActivityTransitionTimer(final Context context) {
		if(null!=getSnackBarQueue()) getSnackBarQueue().clear();

		foreGround = false;
		Func.log_v("D3ServiceLog", "GO BACKGROUND");
		if (Func.getBooleanSPDefFalse(sp, "all_events_were_seen"))
		{
//            long time = dbHelper.getEventMaxTime();
			long time = System.currentTimeMillis()/1000;
//            sp.edit().putLong("last_activity_closed_time", time == 0 ? (System.currentTimeMillis() - D3Service.EVENTS_TIME_INTERVAL)/1000 : time).apply();
			sp.edit().putLong("last_activity_closed_time", time).apply();
			Func.log_v("D3ServiceLog", "SAVE LAST ACTIVITY CLOSED TIME:" + time);
		}

		sp.edit().putLong("app_pause_time", System.currentTimeMillis()/1000).apply();

		int mult = Integer.valueOf(sp.getString("prof_blocking_time", DEFAULT_BLOCK_TIME_VALUE));
		if(mult!=20)
		{
			MAX_ACTIVITY_TRANSITION_TIME_MS = mult * 1000;
			if (MAX_ACTIVITY_TRANSITION_TIME_MS == 0)
			{
				MAX_ACTIVITY_TRANSITION_TIME_MS = 500;
			}

			this.mActivityTransitionTimer = new Timer();
			this.mActivityTransitionTimerTask = new TimerTask()
			{
				public void run()
				{
					blocked = true;
					Func.log_v("D3ServiceLog", "BLOCKED");
					((Activity) context).setResult(Activity.RESULT_CANCELED);
					((Activity) context).finishAffinity();
				}
			};
			this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask, MAX_ACTIVITY_TRANSITION_TIME_MS);
		}
	}

	public void stopActivityTransitionTimer() {
		if (this.mActivityTransitionTimerTask != null) {
			this.mActivityTransitionTimerTask.cancel();
		}

		if (this.mActivityTransitionTimer != null) {
			this.mActivityTransitionTimer.cancel();
		}
		blocked = false;
		Func.log_v("D3ServiceLog", "UNBLOCKED/STILL UNBLOCKED");
		foreGround = true;
		Func.log_v("D3ServiceLog", "GO FOREGROUND");
	}

	public void stopActivityTransitionTimerOnDestroy() {
		if (this.mActivityTransitionTimerTask != null) {
			this.mActivityTransitionTimerTask.cancel();
		}

		if (this.mActivityTransitionTimer != null) {
			this.mActivityTransitionTimer.cancel();
		}
	}


	public boolean getStatus(){
		return foreGround;
	}

	public boolean getBlockedStatus(){
		return blocked;
	}

	public String getPreviousActivity(){
		if(!blocked)
		{
			return previousActivity;
		}
		return null;
	}

	public void setPreviousActivity(String previousActivity){
		this.previousActivity = previousActivity;
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleHelper.onAttach(base));
		MultiDex.install(this);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		LocaleHelper.onAttach(this);
	}

}
