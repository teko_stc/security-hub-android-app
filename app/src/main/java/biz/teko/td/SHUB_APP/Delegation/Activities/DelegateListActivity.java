package biz.teko.td.SHUB_APP.Delegation.Activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Delegation.Adapters.NDomainsAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;

/**
 * Created by td13017 on 12.04.2018.
 */

public class DelegateListActivity extends BaseActivity
{
	private DelegateListActivity context;
	private DBHelper dbHelper;
	private int siteId;
	private final boolean editable = false;
	private NDomainsAdapter adapter;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private final int sending = 0;
	private ListView list;

	private final BroadcastReceiver revokeResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result > 0){
				Func.pushToast(context, getString(R.string.DELEGATE_REVOKE_SUCCESS), (DelegateListActivity) context);
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (DelegateListActivity) context);
			}
		}
	};

	private final BroadcastReceiver delegatedToListUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};

	private Cursor cursor;
	private LinearLayout buttonAdd;
	private LinearLayout buttonBack;
	private Site site;
	private UserInfo userInfo;
	private TextView empty;


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_delegate_list);

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();
		this.siteId = getIntent().getIntExtra("site", 0);

		setView();
		bindView();
	}

	private void bindView()
	{
		if(null!=buttonAdd){
			if(mayControl()){
				buttonAdd.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						Intent intent = new Intent(getBaseContext(), DelegateSendActivity.class);
						intent.putExtra("site", site.id);
						startActivity(intent);
						overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				});
			}else{
				buttonAdd.setVisibility(View.GONE);
			}
		}
		buttonBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		site = dbHelper.getSiteById(siteId);
		if(null!=site)
		{
			updateList();
		}
	}

	private void setView()
	{
		buttonAdd = (LinearLayout) findViewById(R.id.nButtonAdd);
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		list = (ListView) findViewById(R.id.delegatedToList);
		empty = (TextView) findViewById(R.id.nTextEmpty);
	}

	private boolean mayControl()
	{
		return Func.nGetRemoteSystemAdminRights(userInfo);
	}

	private void updateList()
	{
		refreshCursor();
		if(null!=adapter){
			adapter.update(cursor);
		}else{
			adapter = new NDomainsAdapter(context, R.layout.n_domains_list_element_view, cursor, 0, siteId, new NDomainsAdapter.OnItemClickListener()
			{
				@Override
				public void onClick(int id)
				{
					NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
					nDialog.setTitle(context.getString(R.string.MESSAGE_REVOKE_DELEGATION) + " " + dbHelper.getSiteNameById(siteId) + " " + context.getString(R.string.MESSAGE_2_REVOKE_DELEGATION) + " " + id);
					nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
					{
						@Override
						public void onActionClick(int value, boolean b)
						{
							switch (value){
								case NActionButton.VALUE_OK:
									JSONObject message = new JSONObject();
									try
									{
										message.put("site_id", siteId);
										message.put("domain_id", id);
										nSendMessageToServer(null, D3Request.DELEGATE_REVOKE, message, false);
									} catch (JSONException e)
									{
										e.printStackTrace();
									}
									nDialog.dismiss();
									break;
								case NActionButton.VALUE_CANCEL:
									nDialog.dismiss();
									break;
							}
						}
					});
					nDialog.show();
				}
			});
			list.setAdapter(adapter);
		}
		if(null!=adapter && adapter.getCount() > 0 ){
			list.setVisibility(View.VISIBLE);
			if(null!=empty) empty.setVisibility(View.GONE);
		}else{
			list.setVisibility(View.GONE);
			if(null!=empty)empty.setVisibility(View.VISIBLE);
		}
	}

	public void refreshCursor()
	{
		cursor = dbHelper.getDelegateToDomainCursorForSite(siteId);
	}


	@Override
	protected void onResume()
	{
		super.onResume();
		bindD3();
		registerReceiver(revokeResultReceiver, new IntentFilter(D3Service.BROADCAST_DELEGATE_REVOKE_RESULT));
		registerReceiver(delegatedToListUpdateReceiver, new IntentFilter(D3Service.BROADCAST_SITE_DELEGATES));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(revokeResultReceiver);
		unregisterReceiver(delegatedToListUpdateReceiver);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	}


}
