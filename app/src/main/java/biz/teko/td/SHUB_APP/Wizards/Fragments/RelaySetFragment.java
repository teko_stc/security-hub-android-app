package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Other.InputFilterMinMax;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.PresetRadioGroup;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.PresetValueButton;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardRelaySetActivity;

public class RelaySetFragment extends Fragment
{
	private int position;
	private Context context;
	private ViewGroup container;
	private WizardRelaySetActivity activity;
	private DBHelper dbHelper;
	private View v;
	private D3Service myService;
	private int setDeviceId;
	private int min = 1;
	private int max = 2;
	private static final int RELAY_TYPE = 3;
	private static final int AUTO_RELAY_TYPE = 6;
	private int setRelayNumber;
	private String setExitName;
	private int setRelayConType;
	private String setUID;
	private int setRelayOutputNum;
	private int detector = 0;
	private int sending = 0;

	public static RelaySetFragment newInstance(int position){
		RelaySetFragment relaySetFragment = new RelaySetFragment();
		Bundle b = new Bundle();
		b.putInt("position", position);
		relaySetFragment.setArguments(b);
		return relaySetFragment;
	}

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				((WizardRelaySetActivity) getActivity()).refreshViews();
			}
		}
	};

	@Override
	public void onResume()
	{
		super.onResume();
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(commandSendReceiver);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		position = getArguments().getInt("position");
		int layout;
		context = container.getContext();
		this.container = container;
		activity = (WizardRelaySetActivity) getActivity();
		dbHelper = DBHelper.getInstance(context);
		switch (position)
		{
			case 0:
				if(null!=activity){
					setRelayConType = activity.getRelayConType();
				}
				layout = (0 == setRelayConType) ? R.layout.fragment_relay_set_3_1 : R.layout.fragment_relay_set_3 ;
				v = inflater.inflate(layout, container, false);
				setFragmentThree(v);
				break;
			case 1:
				if(null!=activity){
					setRelayConType = activity.getRelayConType();
				}
				layout = 0 == setRelayConType ? R.layout.fragment_relay_set_2_1 : R.layout.fragment_relay_set_2;
				v = inflater.inflate(layout, container, false);
				setFragmentFour(v);
				break;
			case 2:
				if(null!=activity){
					setRelayConType = activity.getRelayConType();
				}
				layout = R.layout.fragment_relay_set_5;
				v = inflater.inflate(layout, container, false);
				setFragmentFiveScripts();
			default:
				break;
		}

		return v;
	}


	public void setFragmentThree(View fragmentTwo)
	{
		final WizardRelaySetActivity activity = (WizardRelaySetActivity) getActivity();
		setDeviceId = activity.getDeviceId();
		setRelayConType = activity.getRelayConType();

		if(0 == setRelayConType){
			/*WIRELESS*/
			Button buttonInteractive = (Button) v.findViewById(R.id.wizRelayStartInteractive);

			boolean interactive = dbHelper.getDeviceInteractiveStatus(setDeviceId);
			if(interactive){
				if(null!=buttonInteractive)
				{
					buttonInteractive.setText(R.string.WIZ_INTERACTIVE_STATEMENT_ON);
					buttonInteractive.setEnabled(false);
				}
			}else{
				if(null!=buttonInteractive){
					buttonInteractive.setText(R.string.WIZ_INTERACTIVE_TURN_ON_ZONE);
					buttonInteractive.setEnabled(true);
				}
			}

			if(null!=buttonInteractive){
				buttonInteractive.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						nSendMessageToServer(dbHelper.getDeviceById(setDeviceId), Command.INTERACTIVE, new JSONObject(), true);
					}
				});
			}

		}else{
			/*WIRED*/
			final EditText relayNameEdit = (EditText) v.findViewById(R.id.wizRelayName);
			if(null!=relayNameEdit){
				relayNameEdit.requestFocus();
				relayNameEdit.setOnEditorActionListener(new TextView.OnEditorActionListener()
				{
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
					{
						if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
						{
							Func.hideKeyboard(activity);
							return true;
						}
						return false;
					}
				});
			}

			final Button next = (Button) v.findViewById(R.id.wizRelayNextButton);
			if(null!=next)
			{
				next.setText(R.string.WIZ_GONEXT);
				next.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (relayNameEdit != null)
						{
							String relayName = relayNameEdit.getText().toString();
							if (!relayName.equals(""))
							{
								activity.setRelayName(relayName);
								activity.setNextPage();
							} else
							{
								Func.nShowMessage(context, context.getString(R.string.WIZ_RELAY_ENTER_NAME));
							}
						}
//						if (dbHelper.isDeviceOnline(setDeviceId))
//						{
//							if (0 == dbHelper.getDeviceArmStatus(setDeviceId))
//							{
//
//							} else
//							{
//								Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_ZONE_ADD));
//							}
//						} else
//						{
//							Func.nShowMessage(context, context.getString(R.string.DTA_NO_CONNECTION_DEVICE));
//						}
					}
				});
			}
		}
	}

	public void setFragmentFour(View fragmentThree)
	{
		final WizardRelaySetActivity activity = (WizardRelaySetActivity) getActivity();
		setDeviceId = activity.getDeviceId();
		setExitName = activity.getRelayName();
		setRelayConType = activity.getRelayConType();

		final Button buttonNext = (Button) v.findViewById(R.id.wizRelayNextButton);
		if(!Func.needScriptsForBuild()) buttonNext.setText(context.getString(R.string.WIZ_ZONE_SET_BUTTON_REGISTER));

		if(setRelayConType == 0){
			/*WIRELESS*/
			TextView relayType = (TextView) v.findViewById(R.id.wizZoneType);
			final ImageView relayImage = (ImageView) v.findViewById(R.id.wizZoneFoundImage);

			setUID = activity.getUID();

			if(null!=setUID && !setUID.equals("")){
				if (null != D3Service.d3XProtoConstEvent)
				{
					String ss =setUID.substring(setUID.length()-4);
					String le = "";
					for(int i = ss.length() -2; i >=0; i-=2){
						le += ss.substring(i, i+2);
					}
					int type = Integer.parseInt(le, 16);
					ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(type);
					if(null!=zType){
						if(null!=relayType)relayType.setText(zType.caption);
						if(null!=relayImage) relayImage.setImageResource(context.getResources().getIdentifier(zType.img, null, context.getPackageName()));
					}
				}
			}

			final EditText relayNameEdit= (EditText) v.findViewById(R.id.wizRelayName);
			if(null!=relayNameEdit){
				relayNameEdit.requestFocus();
				relayNameEdit.setOnEditorActionListener(new TextView.OnEditorActionListener()
				{
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
					{
						if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
						{
							Func.hideKeyboard(activity);
							return true;
						}
						return false;
					}
				});
			}

			if(null!=buttonNext){
				buttonNext.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{

						if(Func.needScriptsForBuild()){
							if(null!=relayNameEdit){
								String relayName =relayNameEdit.getText().toString();
								if(!relayName.equals("")){
									setExitName = relayName;
									activity.setRelayName(setExitName);
									activity.setNextPage();
								}else{
									Func.nShowMessage(context, context.getString(R.string.WIZ_RELAY_ENTER_NAME));
								}
							}
						}else{
							if(null!=relayNameEdit){
								String relayName =relayNameEdit.getText().toString();
								if(!relayName.equals("")){
									setExitName = relayName;
									sendSetRelayCommand();
								}else{
									Func.nShowMessage(context, context.getString(R.string.WIZ_RELAY_ENTER_NAME));
								}
							}
						}
					}
				});
			}

		}else{
			/*WIRED*/
			Device device = dbHelper.getDeviceById(setDeviceId);

			if(null!=device)
			{
				switch (device.type){
					case Const.DEVICETYPE_SH_2:
					case Const.DEVICETYPE_SH_4G:
						max = 6;
						break;
					default:
						max =2;
						break;
				}
			}

			final EditText editOutputNumber = (EditText) v.findViewById(R.id.wizRelayNumber);

			if(null != editOutputNumber)
			{
				editOutputNumber.setFilters(new InputFilter[]{new InputFilterMinMax(min, max)});
			}
			if(null!=editOutputNumber){
				editOutputNumber.requestFocus();
				editOutputNumber.setOnEditorActionListener(new TextView.OnEditorActionListener()
				{
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
					{
						if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
						{
							editOutputNumber.clearFocus();
							Func.hideKeyboard(activity);
							return true;
						}
						return false;
					}
				});
			}

			if(null!=buttonNext)
			buttonNext.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						if(Func.needScriptsForBuild())
						{
							if (null != editOutputNumber)
							{
								String editOutput = editOutputNumber.getText().toString();
								if (null != editOutput && !editOutput.equals(""))
								{
									setRelayOutputNum = Integer.valueOf(editOutput);
									activity.setRelayOutput(setRelayOutputNum);
									activity.setNextPage();
								} else
								{
									Func.nShowMessage(context, context.getString(R.string.WIZ_RELAY_ENTER_EXIT_NUMBER));
								}
							}
						}else{
							if (null != editOutputNumber)
							{
								String editOutput = editOutputNumber.getText().toString();
								if (null != editOutput && !editOutput.equals(""))
								{
									setRelayOutputNum = Integer.valueOf(editOutput);
									sendSetRelayCommand();
								} else
								{
									Func.nShowMessage(context, context.getString(R.string.WIZ_RELAY_ENTER_EXIT_NUMBER));
								}
							}
						}
					}
				});

		}
	}

	private void setFragmentFiveScripts()
	{

		final WizardRelaySetActivity activity = (WizardRelaySetActivity) getActivity();
		setDeviceId = activity.getDeviceId();
		setExitName = activity.getRelayName();
		setRelayConType = activity.getRelayConType();
		setUID = activity.getUID();
		setRelayOutputNum = activity.getRelayOutput();
		final Button buttonNext = (Button) v.findViewById(R.id.wizRelayNextButton);
		if(null!= buttonNext)
			buttonNext.setEnabled(false);

		PresetRadioGroup typeRadioGroup = (PresetRadioGroup) v.findViewById(R.id.relay_type_radio_group);
		PresetValueButton manualRadioButton  = (PresetValueButton) v.findViewById(R.id.relay_manual_button);
		PresetValueButton autoRadioButton  = (PresetValueButton) v.findViewById(R.id.relay_auto_button);

		if(null!=typeRadioGroup)
			typeRadioGroup.setOnCheckedChangeListener(new PresetRadioGroup.OnCheckedChangeListener()
			{
				@Override
				public void onCheckedChanged(View radioGroup, View radioButton, boolean isChecked, int checkedId)
				{
					switch( ((PresetValueButton)radioButton).getPosition()){
						case 1:
							detector = 23;

							break;
						case 2:
							detector = 13;
							break;

					};
					if(null!= buttonNext)
						buttonNext.setEnabled(true);
				}
			});

		if(null!=manualRadioButton)
			manualRadioButton.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{

				}
			});
		if(null!=autoRadioButton)
			autoRadioButton.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{

				}
			});

		if(null!=buttonNext)
		{
			buttonNext.setText(context.getString(R.string.WIZ_ZONE_SET_BUTTON_REGISTER));
			buttonNext.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					sendSetRelayCommand();
				}
			});
		}

	}

	private void sendSetRelayCommand()
	{
		JSONObject commandAddr = new JSONObject();
		try
		{
			//								commandAddr.put("uid",  setUID);
			commandAddr.put("uid",  setRelayConType == 0 ? setUID : (String.format("%02X", 5) + String.format("%02X", setRelayOutputNum) + (detector == 13 ? String.format("%02X", AUTO_RELAY_TYPE) : String.format("%02X", RELAY_TYPE))));
			commandAddr.put("name", setExitName);
			commandAddr.put("detector", detector == 0? 23 : detector);
			nSendMessageToServer(dbHelper.getDeviceById(setDeviceId), Command.ZONE_SET, commandAddr, true);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};


	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}


	public D3Service getLocalService()
	{
		WizardRelaySetActivity activity = (WizardRelaySetActivity) getActivity();
		if(activity != null){
			return activity.getLocalService();
		}
		return null;
	}

}
