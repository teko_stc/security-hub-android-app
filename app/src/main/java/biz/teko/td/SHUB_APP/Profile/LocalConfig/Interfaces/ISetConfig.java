package biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces;


public interface ISetConfig {
    void showProgressBar();
    void dismissProgressBar(boolean successful);
    void close(int resultOk);
}
