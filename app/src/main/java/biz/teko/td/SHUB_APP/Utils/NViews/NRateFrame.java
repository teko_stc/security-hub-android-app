package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.R;

public class NRateFrame extends LinearLayout
{
	private OnRateClickListener onRateClickListener;
	private LinearLayout close;

	public interface OnRateClickListener{
		void onRateClick(int value);
		void onBack();
	}

	public void setOnRateClickListener(OnRateClickListener onRateClickListener){
		this.onRateClickListener = onRateClickListener;
	}

	private Context context;
	private int layout;
	private LinearLayout starsFrame;
	private FrameLayout rootView;
	private NRateStar[] stars;

	public NRateFrame(@NonNull Context context)
	{
		super(context);
	}

	public NRateFrame(Context context, int layout){
		super(context);
		this.context = context;
		this.layout = layout;

		setup();
		bind();
	}

	public NRateFrame(Context context, AttributeSet attr){
		super(context, attr);
		this.context = context;
		parseAttributes(attr);
		setup();
		bind();;
	}

	private void parseAttributes(AttributeSet attr)
	{
		TypedArray a = getContext().obtainStyledAttributes(attr, R.styleable.NRateFrame, 0, 0);
		try{
			layout = a.getResourceId(R.styleable.NRateFrame_NRateFrameLayout, 0);
		}finally
		{
			a.recycle();
		}
	}

	private void setup()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		layoutInflater.inflate(layout, this, true);

		rootView = (FrameLayout) findViewById(R.id.nRootView);
		starsFrame = findViewById(R.id.nStarFrame);
		close = (LinearLayout) findViewById(R.id.nButtonBack);
	}

	private void bind()
	{
		if(null!=starsFrame){
			stars = new NRateStar[5];
			for(int i=0; i < 5; i++){
				stars[i] = new NRateStar(context, R.layout.n_rate_star_view);
				stars[i].setPosition(i);
				int finalI = i;
				stars[i].setOnStarClickListener(() -> {
					if(null!=onRateClickListener) onRateClickListener.onRateClick(finalI);
				});
				starsFrame.addView(stars[i]);
			}
		}
		if(null!=close){
			close.setOnClickListener(v -> {
				if(null!=onRateClickListener) {
					onRateClickListener.onBack();
				}
			});
		}
	}

	public void setRating(int value){
		if(null!=starsFrame && null!=stars && stars.length > 0){
			for(int i=0; i < 5; i++){
				stars[i].setChecked(i<=value);
			}
		}
	}


}
