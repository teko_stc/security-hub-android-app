package biz.teko.td.SHUB_APP.Utils.NViews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import io.reactivex.subjects.PublishSubject;

public class NStartTutorialView extends FrameLayout {
    private static final int DEVICE_CLICK_TUTORIAL = 7;
    private static final int DEVICES_CLICK_TUTORIAL = 6;
    private static final int LAST_TUTORIAL = 13;
    private static final int ALL_STATES_TUTORIAL = 9;
    private Context context;
    private NActionButton buttonYes, buttonNext, buttonSkip;
    private Paint focusPaint;
    private RectF rect;
    private final PublishSubject<Integer> tutorialObservable = PublishSubject.create();
    private View focusView;
    private ConstraintLayout dialog;
    private TextView infoView;
    private Activity activity;
    private float circleX = 0, circleY = 0, circleRadius;
    private float rectX1 = 0, rectY1 = 0, rectX2 = 0, rectY2 = 0, rectRadius;
    private int hintStep, pressedWidth, flipperHeight, textWidth;
    private boolean isRectFocus, isFlipperLeft;
    private NextActivityListener nextActivityListener;
    private SkipListener skipListener;
    private LinearLayout pressedCircleLayout, pressedRectLayout;
    private String dialogText;
    private ImageView flipperImage;

    public interface NextActivityListener {
        void nextActivity();
    }

    public interface SkipListener {
        void skip();
    }

    public void setSkipListener(SkipListener skipListener) {
        this.skipListener = skipListener;
    }

    public void setNextActivityListener(NextActivityListener nextActivityListener) {
        this.nextActivityListener = nextActivityListener;
    }

    public NStartTutorialView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public NStartTutorialView(Context context, Activity activity, int hintStep) {
        super(context);
        this.context = context;
        this.activity = activity;
        this.hintStep = hintStep;
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        setup();
        bind();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        setupFocusPaint();
        if (isRectFocus)
            drawFocusRect(canvas);
        else
            drawFocusCircle(canvas);
    }

    private void drawFocusCircle(Canvas canvas) {
        if (hintStep == DEVICES_CLICK_TUTORIAL) {
            drawCirclePressedLayout();
            canvas.drawCircle(circleX, circleY, circleRadius - pressedWidth, focusPaint);
        } else {
            canvas.drawCircle(circleX, circleY, circleRadius, focusPaint);
        }
    }

    private void drawCirclePressedLayout() {
        pressedCircleLayout.setVisibility(VISIBLE);
        pressedCircleLayout.setX(circleX - circleRadius);
        pressedCircleLayout.setY(circleY - circleRadius);
    }

    private void drawFocusRect(Canvas canvas) {
        pressedWidth = Func.dpToPx(4.7f, context);
        drawRectPressedLayout();
        rect = new RectF(
                rectX1 + pressedWidth,
                rectY1 + pressedWidth,
                rectX2 - pressedWidth,
                rectY2 - pressedWidth
        );
        canvas.drawRoundRect(rect, rectRadius, rectRadius, focusPaint);
    }

    private void drawRectPressedLayout() {
        pressedRectLayout.setVisibility(VISIBLE);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) pressedRectLayout.getLayoutParams();
        layoutParams.width = (int) (rectX2 - rectX1);
        layoutParams.height = (int) (rectY2 - rectY1);
        pressedRectLayout.setLayoutParams(layoutParams);
        pressedRectLayout.setX(rectX1);
        pressedRectLayout.setY(rectY1);
    }

    private void setupFocusPaint() {
        focusPaint = new Paint();
        focusPaint.setColor(Color.TRANSPARENT);
        focusPaint.setAntiAlias(true);
        focusPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
    }

    private void setDialogGravity(View view, int b) {
        LayoutParams params = (LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.START | b;
        view.setLayoutParams(params);
    }

    private void setup() {
        flipperHeight = Func.dpToPx(24, context);
        isRectFocus = (hintStep == DEVICE_CLICK_TUTORIAL);
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutInflater.inflate(R.layout.n_start_tutorial_base_layout, this, true);
        //activity.getWindow().getDecorView().setSystemUiVisibility(SYSTEM_UI_FLAG_LOW_PROFILE);
        setupView();
        setTutorialObservable();
        showTutorial(hintStep);
    }

    private void setNextStep() {
        hintStep++;
        tutorialObservable.onNext(hintStep);
    }

    private void setTutorialObservable() {
        tutorialObservable.onNext(hintStep);
        setupListeners();
        tutorialObservable.subscribe(this::showTutorial).isDisposed();
    }

    private void showTutorial(int hintStep) {
        switch (hintStep) {
            case 1: startMainTutorial(); break;
            case 2: addMainTutorial(); break;
            case 3: siteNameTutorial(); break;
            case 4: settingTutorial(); break;
            case 5: addToolbarTutorial(); break;
            case 6: allDevicesTutorial(); break;
            case 7: deviceClickTutorial(); break;
            case 8: deviceFavouriteTutorial(); break;
            case 9: allStatesTutorial(); break;
            case 10: sectionsTutorial(); break;
            case 11: eventsTutorial(); break;
            case 12: camerasTutorial(); break;
            case 13: bottomSettingTutorial(); break;
        }
    }

    private void bottomSettingTutorial() {
        setTextWidth(190);
        isFlipperLeft = false;
        dialogText = getResources().getString(R.string.N_TUTORIAL_13);
        buttonNext.setTitle(getResources().getString(R.string.ADB_OK));
        focusView = activity.findViewById(R.id.bottom_navigation_item_menu);
        setCoords(focusView.getWidth() / 2, focusView.getHeight() / 2, 90,
                isHasBottomNavPanel() ? 49.5f : 28,
                253,
                true);
    }

    private void camerasTutorial() {
        setTextWidth(148);
        isFlipperLeft = false;
        dialogText = getResources().getString(R.string.N_TUTORIAL_12);
        focusView = activity.findViewById(R.id.bottom_navigation_item_cameras);
        setCoords(focusView.getWidth() / 2, focusView.getHeight() / 2, 90,
                isHasBottomNavPanel() ? 35.5f : 14,
                146,
                true);
    }

    private void eventsTutorial() {
        setTextWidth(159);
        isFlipperLeft = true;
        dialogText = getResources().getString(R.string.N_TUTORIAL_11);
        focusView = activity.findViewById(R.id.bottom_navigation_item_messages);
        setCoords(focusView.getWidth() / 2, focusView.getHeight() / 2, 90,
                isHasBottomNavPanel() ? 49.5f : 28.5f,
                227,
                true);
    }

    private void sectionsTutorial() {
        setTextWidth(194);
        isFlipperLeft = false;
        dialogText = getResources().getString(R.string.N_TUTORIAL_10);
        nextVisibility();
        focusView = activity.findViewById(R.id.nDevicesTabLayout);
        focusView.post(() -> {
            int[] points = new int[2];
            focusView.getLocationOnScreen(points);
            setCircleCoord(points, (int) (focusView.getWidth() * 1.5), focusView.getHeight() / 2);
            setDialogViewTop(36, getTopMargin(23), 168);
            invalidate();
        });
    }

    private void allStatesTutorial() {
        setTextWidth(233);
        isFlipperLeft = false;
        dialogText = getResources().getString(R.string.N_TUTORIAL_9);
        setRadius(112);
        focusView = activity.findViewById(R.id.nFrameSmallIcon);
        setCoords(focusView.getWidth() / 2, focusView.getHeight() / 2, 38, 18, 159, false);
    }

    private void deviceFavouriteTutorial() {
        setTextWidth(160);
        isFlipperLeft = false;
        dialogText = getResources().getString(R.string.N_TUTORIAL_8);
        nextVisibility();
        setRadius(44);
        focusView = activity.findViewById(R.id.nButtonFavorite);
        focusView.post(() -> {
            int[] points = new int[2];
            focusView.getLocationOnScreen(points);
            setCircleCoord(points, focusView.getWidth() / 2, focusView.getHeight() / 2);
            setDialogViewTop(39, getTopMargin(19f), 172);
            invalidate();
        });
    }

    private void deviceClickTutorial() {
        setTextWidth(242);
        isFlipperLeft = true;
        dialogText = getResources().getString(R.string.N_TUTORIAL_7);
        startAnimation(pressedRectLayout);
        focusView = activity.findViewById(R.id.nPagerDevices);
        setCoords(
                Func.dpToPx(11, context),
                Func.dpToPx(16, context),
                35,
                48f,
                104,
                false,
                getResources().getDisplayMetrics().widthPixels - Func.dpToPx(21, context),
                Func.dpToPx(65, context)
        );
    }

    private void allDevicesTutorial() {
        setTextWidth(238);
        isFlipperLeft = true;
        dialogText = getResources().getString(R.string.N_TUTORIAL_6);
        startAnimation(pressedCircleLayout);
        pressedCircleLayout.setClickable(true);
        buttonNext.setVisibility(GONE);
        focusView = activity.findViewById(R.id.bottom_navigation_item_devices);
        setCoords(focusView.getWidth() / 2, focusView.getHeight() / 2,
                64,
                isHasBottomNavPanel() ? 65.5f : 46.5f,
                131,
                true);
    }

    private void addToolbarTutorial() {
        setTextWidth(224);
        isFlipperLeft = true;
        dialogText = getResources().getString(R.string.N_TUTORIAL_5);
        focusView =  ( activity).findViewById(R.id.nImageAdd);
        setCoords(focusView.getWidth() / 2, focusView.getHeight() / 2, 54, 25f, 72, false);
    }

    private void addMainTutorial() {
        //?
        if (null != DBHelper.getInstance(context).getEmptyBar(PrefUtils.getInstance(context).getCurrentSite(), false)) {
            setTextWidth(195);
            isFlipperLeft = true;
            dialogText = getResources().getString(R.string.N_TUTORIAL_2);
            focusView = (activity).findViewById(R.id.mainGrid);
            View image = (activity).findViewById(R.id.plus);
            setCoords(focusView.getWidth() / 4, Func.dpToPx(64, context) + image.getHeight() / 2, 64, 33f, 140, false);
            invalidate();
        } else {
            setNextStep();
        }
    }

    private void settingTutorial() {
        setRadius(62);
        setTextWidth(172);
        isFlipperLeft = false;
        dialogText = getResources().getString(R.string.N_TUTORIAL_4);
        focusView =  (activity).findViewById(R.id.nButtonSettingsImage);
        setCoords(focusView.getWidth() / 2, focusView.getHeight() / 2,
                78, 25f, 245, false);
    }

    private void siteNameTutorial() {
        setTextWidth(220);
        isFlipperLeft = false;
        dialogText = getResources().getString(R.string.N_TUTORIAL_3);
        focusView = ( activity).findViewById(R.id.nButtonSiteSelect);
        setRadiusPx(focusView.getWidth() / 2 + Func.dpToPx(20, context));
        setCoords(focusView.getWidth() / 2, focusView.getHeight() / 2,
                33, 35f, 87, false);
    }

    private void startMainTutorial() {
        setTextWidth(239);
        isFlipperLeft = true;
        dialogText = getResources().getString(R.string.N_TUTORIAL_1);
        focusView = activity.findViewById(R.id.bottom_navigation_item_favorited);
        focusView.post(() -> {
            int[] points = new int[2];
            focusView.getLocationOnScreen(points);
            setCircleCoord(points, focusView.getWidth() / 2, focusView.getHeight() / 2);
            setDialogViewBottom(
                    64,
                    getBottomMargin(isHasBottomNavPanel() ? 50.5f : 27.5f),
                    87);
            invalidate();
        });
    }

    private int getTopMargin(float value) {
        int fromDialog = Func.dpToPx(value, context);
        if (isRectFocus)
            return (int) Math.abs(rectY2 + fromDialog);
        return (int) Math.abs(circleY + circleRadius + fromDialog);
    }

    private int getBottomMargin(float value) {
        int height = getResources().getDisplayMetrics().heightPixels;
        int margin = Func.dpToPx(value, context);
        if (isHasBottomNavPanel()) {
            View view = activity.findViewById(R.id.nBottomMenu);
            return (view.getHeight() + margin + Func.getNavBarHeight(context));
        }
        return (int) Math.abs(height - circleY - circleRadius - margin);
    }

    private boolean isHasBottomNavPanel() {
        return circleY < getResources().getDisplayMetrics().heightPixels;
    }

    private void setDialogViewBottom(int left, int bottom, int flipperLeft) {
        setDialogGravity(dialog, Gravity.BOTTOM);
        setDialogPosition(dialog, left, 0, bottom);
        setFlipperImageBottom(flipperLeft, bottom - flipperHeight);
        infoView.setText(dialogText);
    }

    private void setFlipperImageBottom(int left, int bottom) {
        setDialogGravity(flipperImage, Gravity.BOTTOM);
        flipperImage.setImageResource(isFlipperLeft ? R.drawable.ic_flipper_left_bottom : R.drawable.ic_flipper_right_bottom);
        setDialogPosition(flipperImage, left, 0, bottom);
    }

    private void setDialogViewTop(int left, int top, int flipperLeft) {
        setDialogGravity(dialog, Gravity.TOP);
        setDialogPosition(dialog, left, top, 0);
        setFlipperImageTop(flipperLeft, top - flipperHeight);
        infoView.setText(dialogText);
    }

    private void setFlipperImageTop(int left, int top) {
        setDialogGravity(flipperImage, Gravity.TOP);
        flipperImage.setImageResource(isFlipperLeft ? R.drawable.ic_flipper_left_top : R.drawable.ic_flipper_right_top);
        setDialogPosition(flipperImage, left, top, 0);
    }

    private void setDialogPosition(View view, int left, int top, int bottom) {
        infoView.setWidth(textWidth);
        MarginLayoutParams params = (MarginLayoutParams) view.getLayoutParams();
        params.leftMargin = Func.dpToPx(left, context);
        if (top == 0)
            params.bottomMargin = bottom;
        else
            params.topMargin = top;
        view.setLayoutParams(params);
    }

    private void setCoords(int x, int y, int dialogLeft, float dialogH, int flipperLeft, boolean isBottom,  int... endXY) {
        focusView.post(() -> {
            int[] points = new int[2];
            focusView.getLocationOnScreen(points);
            if (isRectFocus)
                setRectCoord(points, x, y, endXY);
            else
                setCircleCoord(points, x, y);
            if (isBottom)
                setDialogViewBottom(dialogLeft, getBottomMargin(dialogH), flipperLeft);
            else
                setDialogViewTop(dialogLeft, getTopMargin(dialogH), flipperLeft);
            invalidate();
        });
    }

    private void setRectCoord(int[] points, int x, int y, int[] endXY) {
        rectX1 = points[0] + x;
        rectY1 = points[1] + y;
        rectX2 = rectX1 + endXY[0];
        rectY2 = rectY1 + endXY[1];
    }

    private void setCircleCoord(int[] points, int x, int y) {
        circleX = points[0] + x;
        circleY = points[1] + y;
    }

    private void setupView() {
        flipperImage = findViewById(R.id.flipperImage);
        dialog = findViewById(R.id.dialogTutorial);
        infoView = findViewById(R.id.infoTutorial);
        buttonYes = findViewById(R.id.nDialogButtonOK);
        buttonNext = findViewById(R.id.nDialogButtonNext);
        buttonSkip = findViewById(R.id.nDialogButtonSkip);
        pressedCircleLayout = findViewById(R.id.circleLayout);
        pressedRectLayout = findViewById(R.id.roundRectLayout);
        setAliasLayouts();
        pressedWidth = Func.dpToPx(4.7f, context);
        if (isRectFocus) {
            rectRadius = Func.dpToPx(28, context);
            clickVisibility();
        } else {
            setRadius(62);
        }
    }

    private void setAliasLayouts() {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        pressedCircleLayout.setLayerPaint(paint);
        pressedRectLayout.setLayerPaint(paint);
    }

    private void setRadius(int radius) {
        circleRadius = Func.dpToPx(radius, context);
    }

    private void setRadiusPx(int radius) {
        circleRadius = radius;
    }

    private void clickVisibility() {
        buttonYes.setVisibility(View.GONE);
        buttonSkip.setVisibility(View.GONE);
        buttonNext.setVisibility(View.GONE);
    }

    private void nextVisibility() {
        buttonYes.setVisibility(View.GONE);
        buttonSkip.setVisibility(View.GONE);
        buttonNext.setVisibility(View.VISIBLE);
    }

    private void bind() {
        ((ViewGroup) activity.getWindow().getDecorView()).addView(this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        return true;
    }

    private void setupListeners() {
        pressedCircleLayout.setOnClickListener(view -> {
            if (hintStep == DEVICES_CLICK_TUTORIAL)
                layoutClick(view);
        });
        pressedRectLayout.setOnClickListener(this::layoutClick);
        buttonSkip.setOnButtonClickListener(action -> finishTutorial());
        buttonYes.setOnButtonClickListener(action -> {
            nextVisibility();
            setNextStep();
        });
        buttonNext.setOnButtonClickListener(action -> {
            if (hintStep == ALL_STATES_TUTORIAL)
                nextActivityListener.nextActivity();
            else if (hintStep == LAST_TUTORIAL)
                finishTutorial();
            else
                setNextStep();
        });
    }

    private void startAnimation(LinearLayout layout) {
        AnimationDrawable mAnimationDrawable = (AnimationDrawable) layout.getBackground();
        mAnimationDrawable.start();
    }

    private void layoutClick(View view) {
        nextActivityListener.nextActivity();
        this.setVisibility(GONE);
        invalidate();
    }

    private void setTextWidth(int width) {
        textWidth = Func.dpToPx(width, context);
    }

    public void finishTutorial() {
        PrefUtils.getInstance(context).setStartTutorial(false);
        ((ViewGroup) activity.getWindow().getDecorView()).removeView(this);
        if (skipListener != null)
            skipListener.skip();
    }
}
