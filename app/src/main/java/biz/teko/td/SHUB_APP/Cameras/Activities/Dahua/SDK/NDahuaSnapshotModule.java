package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK;

import android.content.Context;
import android.content.res.Resources;

import com.company.NetSDK.CB_fSnapRev;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.SNAP_PARAMS;
import com.company.PlaySDK.Constants;
import com.company.PlaySDK.IPlaySDK;

import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NDahuaSnapshotModule
{

	private Resources res;
	private Camera camera;

	public NDahuaSnapshotModule(Context context, Camera camera) {
		res = context.getResources();
		this.camera = camera;
	}

	public boolean localCapturePicture(int nPort, String picFileName) {
		if (IPlaySDK.PLAYCatchPicEx(nPort, picFileName, Constants.PicFormat_JPEG) == 0) {
			Func.log_d("localCapturePicture Failed!");
			return false;
		}
		return true;
	}

	public boolean remoteCapturePicture(int chn) { return remoteCapturePicture(camera.getLoginHandle(), chn); }

	public boolean remoteCapturePicture(long m_hLoginHandle, int chn) { return snapPicture(m_hLoginHandle, chn, 0, 0); }

	public boolean timerCapturePicture(long m_hLoginHandle, int chn) {
		return snapPicture(m_hLoginHandle, chn, 1, 2);
	}

	public boolean stopCapturePicture(int chn) {
		return stopCapturePicture(camera.getLoginHandle(), chn);
	}

	public boolean stopCapturePicture(long m_hLoginHandle, int chn) {
		return snapPicture(m_hLoginHandle, chn, -1, 0);
	}

	private boolean snapPicture(long m_hLoginHandle, int chn, int mode, int interval) {
		SNAP_PARAMS stuSnapParams = new SNAP_PARAMS();
		stuSnapParams.Channel = chn;
		stuSnapParams.mode = mode;
		stuSnapParams.Quality = 3;
		stuSnapParams.InterSnap = interval;
		stuSnapParams.CmdSerial = 0;

		if(0!=m_hLoginHandle) {
			if (!INetSDK.SnapPictureEx(m_hLoginHandle, stuSnapParams)) {
				Func.log_d("SnapPictureEx Failed!");
				return false;
			}
		}else{
			return false;
		}
		return true;
	}

	public void setSnapRevCallBack(CB_fSnapRev snapReceiveCB){
		INetSDK.SetSnapRevCallBack(snapReceiveCB);
	}
}
