package biz.teko.td.SHUB_APP.UDP.Entities;

public abstract class RequestBasePincoded {
    protected byte command;
    protected byte[] magic = new byte[4];
    public abstract byte[] getBytes();
}
