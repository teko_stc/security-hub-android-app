package biz.teko.td.SHUB_APP.Profile.CurrentUser;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;

/**
 * Created by td13017 on 22.08.2017.
 */

public class CurrentUserPreferenceFragment extends PreferenceFragment
{
	private Activity context;
	private DBHelper dbHelper;
	private D3Service myService;
	private int sending = 0;

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.current_user_preferences);
		context = getActivity();
		dbHelper  = DBHelper.getInstance(context);
		final UserInfo userInfo = dbHelper.getUserInfo();
		final CurrentUserActivity currentUserActivity = (CurrentUserActivity) context;

		Preference prof_change_name = (Preference) findPreference("prof_change_name");
		Preference prof_change_pass = (Preference) findPreference("prof_change_pass");
		Preference prof_exit = (Preference) findPreference("prof_exit");

		if((userInfo.roles & Const.Roles.DOMAIN_ADMIN)==0 && (userInfo.roles&Const.Roles.DOMAIN_HOZ_ORG)==0){
			prof_change_name.setEnabled(false);
			prof_change_pass.setEnabled(false);
		}

		if(null!=prof_exit){
		prof_exit.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				NDialog nDialog = new NDialog(context, R.layout.n_dialog_question);
				nDialog.setTitle("Выйти из учетной записи?");
				nDialog.setOnActionClickListener(new NDialog.OnActionClickListener() {
					@Override
					public void onActionClick(int value, boolean b) {
						switch (value){
							case NActionButton.VALUE_CANCEL:
								nDialog.dismiss();
								break;
								case NActionButton.VALUE_OK:
//									Func.log_d("D3BaseActivity", "LOGOUT");
//									Intent manualLogoutIntent  = new Intent(D3Service.BROADCAST_CONNECTION_DROP);
//									manualLogoutIntent.putExtra("AppName", context.getPackageName());
//									manualLogoutIntent.putExtra("base", true);
//									context.sendBroadcast(manualLogoutIntent);
									break;
						}
					}
				});
				nDialog.show();
				return false;
			}
		});
	}

		if(null!=prof_change_name){
			prof_change_name.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					final String operatorName = dbHelper.getOperatorNameById(userInfo.id);
					AlertDialog.Builder pwChangeDBuilder = Func.adbForCurrentSDK(context);
					pwChangeDBuilder.setTitle(R.string.PA_NAME_CHANGE_TITLE);

					View view = context.getLayoutInflater().inflate(R.layout.dialog_change_name, null);
					final EditText editName = (EditText) view.findViewById(R.id.edit_user_name);
					editName.setText(operatorName);
					editName.setSelection(operatorName.length());

					editName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
					editName.setPadding(0, Func.dpToPx(20, context), 0, Func.dpToPx(20, context));
					editName.setCursorVisible(true);
					editName.setSingleLine();
					//set edittext cursor drawable
					try
					{
						Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
						f.setAccessible(true);
						f.set(editName, R.drawable.caa_cursor);
					} catch (Exception ignored)
					{
					}
					editName.setGravity(Gravity.CENTER);

					pwChangeDBuilder.setView(view);

					pwChangeDBuilder.setPositiveButton(R.string.PA_CHANGE_BUTTON, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{

						}
					});
					pwChangeDBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							dialog.dismiss();
						}
					});

					final AlertDialog ad = pwChangeDBuilder.create();
					ad.show();
					ad.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View view)
						{
							if (0 != editName.getText().length())
							{
								UserInfo userInfo = dbHelper.getUserInfo();
								if (!editName.getText().toString().equals(operatorName))
								{
									JSONObject message = new JSONObject();
									try
									{
										Operator operator = dbHelper.getCurrentOperator(userInfo);
										message.put("id", operator.id);
										message.put("name", editName.getText().toString().trim());
									} catch (JSONException e)
									{
										e.printStackTrace();
									}
									D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_SET", message);
									if(d3Request.error == null){
										sendMessageToServer(d3Request);
										ad.dismiss();
										Func.hideKeyboard((Activity) context);
									}else{
										Func.pushToast(context, d3Request.error, (Activity) context);
									}
								} else
								{
									Func.pushToast(context, getString(R.string.PA_ENTERED_NAME_COMPARE), currentUserActivity);
								}
							} else
							{
								Func.pushToast(context, getString(R.string.PA_ERROR_ENTER_NAME), currentUserActivity);
							}
						}
					});
					return false;
				}
			});
		}

		if(null!=prof_change_pass){
			prof_change_pass.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener()
			{
				@Override
				public boolean onPreferenceClick(Preference preference)
				{
					AlertDialog.Builder pwChangeDBuilder = Func.adbForCurrentSDK(context);
					pwChangeDBuilder.setTitle(R.string.PA_PW_CHANGE);

					View view = context.getLayoutInflater().inflate(R.layout.dialog_change_pass, null);


					final EditText editOldPW = (EditText) view.findViewById(R.id.edit_old_pw);
					final EditText editNewPW = (EditText) view.findViewById(R.id.edit_new_pw);
					;

					editOldPW.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
					editOldPW.setPadding(0, Func.dpToPx(20, context), 0, Func.dpToPx(20, context));
					editOldPW.setCursorVisible(true);
					editOldPW.setSingleLine();
					//set edittext cursor drawable
					try
					{
						Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
						f.setAccessible(true);
						f.set(editOldPW, R.drawable.caa_cursor);
					} catch (Exception ignored)
					{
					}
					//				editOldPW.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
					//				editOldPW.setFilters(new InputFilter[]{filter});
					editOldPW.setGravity(Gravity.CENTER);
					editOldPW.setTransformationMethod(new Func.AsteriskPasswordTransformationMethod());
					final EditText finalEditNewPW = editNewPW;
					editOldPW.setOnKeyListener(new View.OnKeyListener()
					{

						public boolean onKey(View v, int keyCode, KeyEvent event)
						{
							// If the event is a key-down event on the "enter" button
							if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
							{
								// Perform action on Enter key press
								editOldPW.clearFocus();
								finalEditNewPW.requestFocus();
								return true;
							}
							return false;
						}
					});

					editNewPW.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
					editNewPW.setPadding(0, Func.dpToPx(20, context), 0, Func.dpToPx(20, context));
					editNewPW.setCursorVisible(true);
					editNewPW.setSingleLine();
					try
					{
						Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
						f.setAccessible(true);
						f.set(editNewPW, R.drawable.caa_cursor);
					} catch (Exception ignored)
					{
					}
					//				editNewPW.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
					//				editNewPW.setFilters(new InputFilter[]{filter});
					editNewPW.setTransformationMethod(new Func.AsteriskPasswordTransformationMethod());
					editNewPW.setGravity(Gravity.CENTER);

					final Button buttonOk = new Button(context);
					buttonOk.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
					buttonOk.setBackgroundResource(R.drawable.rectangle_white);
					buttonOk.setText(R.string.SITE_NAME_CHANGE);
					buttonOk.setTextColor(getResources().getColor(R.color.md_black_1000));
					buttonOk.setTypeface(null, Typeface.BOLD);
					buttonOk.setOnTouchListener(new View.OnTouchListener()
					{
						@Override
						public boolean onTouch(View v, MotionEvent event)
						{
							switch (event.getAction())
							{
								case MotionEvent.ACTION_DOWN:
									buttonOk.setTextColor(context.getResources().getColor(R.color.brandColorDark));
									break;
								case MotionEvent.ACTION_UP:
									buttonOk.setTextColor(context.getResources().getColor(R.color.md_black_1000));
									break;
							}
							return false;
						}
					});

					pwChangeDBuilder.setView(view);

					pwChangeDBuilder.setPositiveButton(R.string.PA_CHANGE_BUTTON, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{

						}
					});
					pwChangeDBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialog, int which)
						{
							dialog.dismiss();
						}
					});

					final AlertDialog ad = pwChangeDBuilder.create();
					ad.show();
					ad.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View view)
						{
							if (0 != editOldPW.getText().length())
							{
								UserInfo userInfo = dbHelper.getUserInfo();
								if (0 == (userInfo.userPassword).compareTo(Func.md5(editOldPW.getText().toString())))
								{
									if (0 != editNewPW.getText().length())
									{
										String s = editNewPW.getText().toString();
										final String passHash = Func.md5(s);
										JSONObject message = new JSONObject();
										try
										{
											Operator operator = dbHelper.getCurrentOperator(userInfo);
											message.put("id", operator.id);
											message.put("password", passHash);
										} catch (JSONException e)
										{
											e.printStackTrace();
										}
										D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_SET", message);
										if(d3Request.error == null){
											sendMessageToServer(d3Request);
											ad.dismiss();
											Func.hideKeyboard((Activity) context);
										}else{
											Func.pushToast(context, d3Request.error, (Activity) context);
										}
									} else
									{
										Func.pushToast(context, getString(R.string.PA_PASS_CHANGE_ENTER_NEW_PASS), currentUserActivity);
									}
								} else
								{
									Func.pushToast(context, getString(R.string.PA_PASS_CHANGE_WRONG_PASS), currentUserActivity);
								}
							} else
							{
								Func.pushToast(context, getString(R.string.PA_PASS_CHANGE_ENTER_OLD_PASS), currentUserActivity);
							}
						}
					});
					return false;
				}
			});
		}
	}


	private boolean sendCommandToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			myService = getService();
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				} else
				{
					sendIntentCommandBeenSend(-1);
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			myService = getService();
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};


	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	public D3Service getService()
	{
		Activity activity = getActivity();
		if(activity != null){
			CurrentUserActivity currentUserActivity = (CurrentUserActivity) activity;
			return currentUserActivity.getLocalService();
		}
		return null;
	}

}
