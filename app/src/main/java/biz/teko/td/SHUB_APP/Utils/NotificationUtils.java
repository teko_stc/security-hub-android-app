package biz.teko.td.SHUB_APP.Utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.os.Build;

import androidx.annotation.RequiresApi;

import static android.content.Context.NOTIFICATION_SERVICE;

public class NotificationUtils
{
	public static final String CONN_DROP_NOTIF_CHANNEL = "conn_drop_notif_channel";
	public static final String SERVICE_NOTIF_CHANNEL = "service_notif_channel";

	private static NotificationUtils notificationUtils;
	private static NotificationManager notificationService;
	private static PrefUtils prefUtils;
	private final Context context;

	public NotificationUtils(Context context){
		this.context = context.getApplicationContext();
	}

	public static final NotificationUtils getInstance(Context context){
		if(null == notificationUtils){
			notificationUtils = new NotificationUtils(context);
			prefUtils = PrefUtils.getInstance(context);
			notificationService = ((NotificationManager) context.getSystemService(NOTIFICATION_SERVICE));
		}
		return notificationUtils;
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public void createClassDefaultChannel(int classId, String caption)
	{
		/*При наличии автоматически пропускает и только имя обновляет*/
		String channelId = getClassChannelId(classId);
		NotificationChannel mChannel = getClassChannel(Integer.valueOf(channelId));
		if(prefUtils.getImportNotifChannels()){
			if(null==mChannel){
				if (classId < 4)
				{
					mChannel = new NotificationChannel(channelId, caption, NotificationManager.IMPORTANCE_HIGH);
				} else
				{
					mChannel = new NotificationChannel(channelId, caption, NotificationManager.IMPORTANCE_NONE);
				}
				createNotificationChannel(classId, mChannel);
			}else{
				saveChannelParamsToPrefs(classId, mChannel);
			}
		}else{
			mChannel = new NotificationChannel(channelId, caption, prefUtils.getNotificationChannelImportance(classId));
			mChannel = setChannelParamsFromPrefs(classId, mChannel);
			createNotificationChannel(classId, mChannel);
		}
//		NotificationChannel mChannel = getClassChannel(Integer.valueOf(channelId));
//		if(null == mChannel)
//		{
//			if (classId < 4)
//			{
//				mChannel = new NotificationChannel(channelId, caption, NotificationManager.IMPORTANCE_HIGH);
//			} else
//			{
//				mChannel = new NotificationChannel(channelId, caption, NotificationManager.IMPORTANCE_NONE);
//			}
//			mChannel = setChannelParamsFromPrefs(classId, mChannel);
//		}else{
//			mChannel = new NotificationChannel(channelId, caption, mChannel.getImportance());
//			mChannel.enableVibration(mChannel.shouldVibrate());
//		}

	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public void deleteClassChannel(int classId)
	{
		notificationService.deleteNotificationChannel(getClassChannelId(classId));
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public void createChannel(String id, String title, int importance)
	{
		createNotificationChannel(new NotificationChannel(id, title, importance));
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public void createChannelNoVibro(String id, String title, int importance)
	{
		NotificationChannel mChannel = new NotificationChannel(id, title, importance);
		mChannel.setVibrationPattern(new long[]{ 0 });
		mChannel.enableVibration(true);
		createNotificationChannel(mChannel);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public NotificationChannel getClassChannel(int classId)
	{
		return  notificationService.getNotificationChannel(getClassChannelId(classId));
	}

	public String getClassChannelId(int classId)
	{
		return Integer.toString(prefUtils.getNotificationChannelId(classId));
	}

	private void setClassChannelId(int classId, int id){
		prefUtils.setNotificationChannelId(classId, id);
	}

	public void updateClassChannelID(int classId)
	{
		prefUtils.updateNotificationChannelCounter();
		setClassChannelId(classId, prefUtils.getNotificationChannelCounter());
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void createNotificationChannel(NotificationChannel notificationChannel)
	{
		notificationService.createNotificationChannel(notificationChannel);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public void createNotificationChannel(int classId, NotificationChannel notificationChannel)
	{
		/*save prefs for user before create channel*/
		saveChannelParamsToPrefs(classId, notificationChannel);
		notificationService.createNotificationChannel(notificationChannel);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void saveChannelParamsToPrefs(int classId, NotificationChannel notificationChannel)
	{
		prefUtils.setNotificationChannelImportance(classId, notificationChannel.getImportance());
		prefUtils.setNotificationChannelSound(classId, notificationChannel.getSound());
		prefUtils.setNotificationChannelVibrate(classId, notificationChannel.shouldVibrate());
		prefUtils.setNotificationChannelLight(classId, notificationChannel.shouldShowLights());
		prefUtils.setNotificationChannelBadge(classId, notificationChannel.canShowBadge());
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private NotificationChannel setChannelParamsFromPrefs(int classId, NotificationChannel mChannel)
	{
//		mChannel.setImportance(prefUtils.getNotificationChannelImportance(classId));
		mChannel.setSound(prefUtils.getNotificationChannelSound(classId), new AudioAttributes.Builder()
				.setUsage(AudioAttributes.USAGE_NOTIFICATION)
				.setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
				.build());
		mChannel.enableVibration(prefUtils.getNotificationChannelVibrate(classId));
		mChannel.enableLights(prefUtils.getNotificationChannelLight(classId));
		mChannel.setShowBadge(prefUtils.getNotificationChannelBadge(classId));
		return mChannel;
	}

	public void removeChannelParamsFromPrefs(int classId)
	{
		prefUtils.removeNotificationChannelImportance(classId);
		prefUtils.removeNotificationChannelSound(classId);
		prefUtils.removeNotificationChannelVibrate(classId);
		prefUtils.removeNotificationChannelLight(classId);
		prefUtils.removeNotificationChannelBadge(classId);
	}

	/*PERSISTENCE SERVICE*/

	@RequiresApi(api = Build.VERSION_CODES.O)
	public NotificationChannel getServiceNotifChannel()
	{
		return notificationService.getNotificationChannel(getServiceNotifChannelId());
	}

	public String getServiceNotifChannelId(){
		return Integer.toString(prefUtils.getServiceNotifChannelId());
	}

	public void updateServiceNotifChannelId(){
		prefUtils.updateNotificationChannelCounter();
		prefUtils.setServiceNotificationChannelId(prefUtils.getNotificationChannelCounter());
//		prefUtils.setServiceChannelIdCounter(prefUtils.getServiceChannelIdCounter() + 1)
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public void deleteServiceNotifChannel()
	{
		notificationService.deleteNotificationChannel(getServiceNotifChannelId());
	}

	/*CONNECTION DROP*/

	public String getConnectionDropNotifChannelId(){
		return Integer.toString(prefUtils.getConnDropNotifChannelId());
	}

	public void updateConnectionDropNotifChannelId(){
		prefUtils.updateNotificationChannelCounter();
		prefUtils.setConnDropNotifChannelId(prefUtils.getNotificationChannelCounter());
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public void deleteConnectionDropNotifChannel()
	{
		notificationService.deleteNotificationChannel(getConnectionDropNotifChannelId());
	}
}
