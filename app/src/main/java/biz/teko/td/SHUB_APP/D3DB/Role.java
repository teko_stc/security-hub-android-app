package biz.teko.td.SHUB_APP.D3DB;

/**
 * Created by td13017 on 01.02.2017.
 */
public class Role
{
	int org;
	int domain;
	int role;

	public Role(int org, int domain,  int role){
		this.org = org;
		this.domain = domain;
		this.role = role;
	}
}
