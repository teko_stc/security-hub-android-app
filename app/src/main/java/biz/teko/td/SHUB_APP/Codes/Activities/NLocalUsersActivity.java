package biz.teko.td.SHUB_APP.Codes.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Codes.Adapters.NLocalUsersListAdapter;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardUserSetActivity;

public class NLocalUsersActivity extends BaseActivity {
    private Context context;
    private DBHelper dbHelper;
    private int device_id;
    private NLocalUsersListAdapter codesListAdapter;

    private ListView codesList;
    private Device device;
    private TextView title;
    private LinearLayout back;
    private LinearLayout add;
    private TextView empty;

    private Cursor cursor;

    private int sending = 0;

    private BroadcastReceiver updateListReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
           updateCodesList();
        }
    };


    private BroadcastReceiver commandResultReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
        }
    };


    private BroadcastReceiver commandSendReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, Intent intent) {
            sending = intent.getIntExtra("command_been_send", 0);
            if (sending != 1) {
                switch (sending) {
                    case 0:
                        int result = intent.getIntExtra("result", 1);
                        if (result < 1) {
                            Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
                        } else {
                            Func.pushToast(context, context.getString(R.string.COMMAND_BEEN_SEND), (Activity) context);
                        }
                        break;
                    case -1:
                        Func.pushToast(context, context.getString(R.string.COMMAND_NOT_BEEN_SEND), (Activity) context);
                        sending = 0;
                        break;
                    default:
                        sending = 0;
                        break;
                }
            }
            updateCodesList();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        context = this;
        dbHelper = DBHelper.getInstance(context);
        device_id = getIntent().getIntExtra("device", -1);

        device = dbHelper.getDeviceById(device_id);

        setup();
    }

    private void setup()
    {
        back = (LinearLayout) findViewById(R.id.nButtonBack);
        add = (LinearLayout) findViewById(R.id.nButtonAdd);
        title = (TextView) findViewById(R.id.nTextTitle);
        empty  = (TextView) findViewById(R.id.operatorsEmpty);

        codesList = (ListView) findViewById(R.id.codesList);
    }

    private void bind()
    {
        if(null!=title){
            if(null!=device) title.setText(device.getName() + " (" + device.account + ")");
        }
        if(null!=back) back.setOnClickListener(v->onBackPressed());
        if(null!=add) add.setOnClickListener(v -> openCodeAddActivity());

        updateCodesList();
    }

    private void openCodeAddActivity()
    {
        Intent intent = new Intent(getBaseContext(), WizardUserSetActivity.class);
        intent.putExtra("device_id", device.id);
        startActivityForResult(intent, Const.ADD_LOCAL_USER);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            updateCodesList();
        }
    }

    private void updateCodesList()
    {
        updateCursor();

        if(null== codesListAdapter){
            codesListAdapter = new NLocalUsersListAdapter(context, R.layout.card_user, cursor, 0, device_id);
            if(null!= codesList){
                codesList.setAdapter(codesListAdapter);
            }
        }else{
            codesListAdapter.update(cursor);
        }

        if(null== cursor || cursor.getCount() == 0 ){
            empty.setVisibility(View.VISIBLE);
        }else{
            empty.setVisibility(View.GONE);
        }

    }

    private void updateCursor()
    {
        cursor = dbHelper.getUsersCursorForDevice(device_id);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceivers();
    }

    @Override
    protected void onResume() {
        super.onResume();
        bind();
        registerReceivers();
    }

    private void registerReceivers()
    {
        registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
        registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
        registerReceiver(updateListReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_USER_UPD));
        registerReceiver(updateListReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_ADD));
        registerReceiver(updateListReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_USER_RMV));
    }

    private void unregisterReceivers()
    {
        unregisterReceiver(commandSendReceiver);
        unregisterReceiver(commandResultReceiver);
        unregisterReceiver(updateListReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }
}
