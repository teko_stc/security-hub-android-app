package biz.teko.td.SHUB_APP.Profile.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class ProfileMasterActivity extends BaseActivity
{
	private Context context;
	private SharedPreferences sp;
	private boolean master;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_master);

		context = this;
		sp  = PreferenceManager.getDefaultSharedPreferences(context);
		master = Func.isMasterMode(sp);

		TextView button = (TextView) findViewById(R.id.masterSwitchButton);
		ImageView close = (ImageView) findViewById(R.id.closeButton);


		if(null!=button){
			button.setText(master?getString(R.string.MASTER_MODE_OFF):getString(R.string.MASTER_MODE_ON));
			button.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					sp.edit().putBoolean("prof_master_set", !master).apply();
//					sendBroadcast(new Intent(D3Service.BROADCAST_CHANGE_MASTER_MODE));
					Intent intent = new Intent();
					setResult(RESULT_OK, intent);
					onBackPressed();
				}
			});
		}

		if(null!=close){
			close.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					onBackPressed();
				}
			});
		}
	}
}
