package biz.teko.td.SHUB_APP.Relays_N.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Events.Activities.EventsStateActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptActivity;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptsLibraryActivity;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomInfoLayout;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopLayout;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class RelayActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private Zone relay;
	private NTopLayout topFrame;
	private NBottomInfoLayout bottomFrame;
	private String title;
	private int zone_id;
	private int section_id;
	private int device_id;
	private boolean transition;
	private int transition_start;
	private int site_id;
	private MainBar mainBar;
	private boolean favorited;
	private int sending = 0;
	private boolean bound;
	private D3Service myService;
	private ServiceConnection serviceConnection;

	private UserInfo userInfo;
	private String iso;

	private NDialog materialDialog;
	private boolean wait;
	private Timer timer;

	private boolean binded;

	private BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(Const.ACTION_RMV == intent.getIntExtra("action", -1)
					&& zone_id == intent.getIntExtra("zone", -1)
					&& section_id == intent.getIntExtra("section", -1)
					&& device_id == intent.getIntExtra("device", -1)){
				onBackPressed();
			}else {
				bind();
			}
		}
	};

	private BroadcastReceiver libraryScriptsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			goToScriptSet();
		}
	};

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_relay);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();
		iso = PrefUtils.getInstance(context).getCurrentLang();

		title  = getIntent().getStringExtra("bar_title");
		zone_id = getIntent().getIntExtra("zone", -1);
		section_id = getIntent().getIntExtra("section", -1);
		device_id = getIntent().getIntExtra("device", -1);
		site_id = getIntent().getIntExtra("site", -1);

		transition = getIntent().getBooleanExtra("transition", false);
		transition_start = getIntent().getIntExtra("transition_start", 0);

		addTransitionListener(transition);

		topFrame = (NTopLayout) findViewById(R.id.nTopFrame);
		bottomFrame = (NBottomInfoLayout) findViewById(R.id.nBottomFrame);


//		favorited = dbHelper.getFavoriteStatusForRelay(site_id, relay);
		bind();
	}

	private void bind()
	{
		binded = true;
		relay = dbHelper.getZoneById(device_id, section_id, zone_id);

		if(null!=relay)
		{
			mainBar = getRefreshedMainBar();
			if (null != mainBar) {
				favorited = true;
			} else
				favorited = false;
			setTopFrame();
			setBottomFrame();
		}else{
			onBackPressed();
		}
	}

	private void setBottomFrame()
	{
		//Section section = null;
		Device device = null;

		if(null!=bottomFrame){
			switch (relay.id){
				case 0:
					if(relay.section_id != 0){
						//section = dbHelper.getDeviceSectionById(relay.device_id, relay.section_id);
						break;
					}
				case 100:
					device = dbHelper.getDeviceById(relay.device_id);
					break;
				default:
					device = dbHelper.getDeviceById(relay.device_id);
					//section = dbHelper.getDeviceSectionById(relay.device_id, relay.section_id);
//					relay = dbHelper.getZoneById(relay.device_id, relay.section_id, relay.id);
					break;
			}
			int model = relay.getModelId();
			if(0!=model){
				if(model != Const.SENSORTYPE_WIRED_OUTPUT)
				{
					ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(model);
					if (null != zType && null!=zType.icons)
					{
						bottomFrame.setModel(zType.caption_e);
						bottomFrame.setModelImage(zType.getListIcon(context));
					}
				}else{
					DWOType dwoType = relay.getDWOType();
					if (null != dwoType && null != dwoType.icons)
					{
						bottomFrame.setType(dwoType.caption);
					}
					int dwoId = relay.getWIOId();
					if(0!=dwoId){
						bottomFrame.setZoneIO(Integer.toString(dwoId));
					}
				}
			}
			DType detector = relay.getDType();
			if(null!=detector) {
				bottomFrame.setAlarm(detector.caption);
			}
//			bottomFrame.setZoneId(Integer.toString(relay.id));
			bottomFrame.setZoneId(null);

//			bottomFrame.setSectionName(null!=section ? (section.id == 0 ? "Не в разделе" : section.name): null);
			bottomFrame.setDeviceName(null!= device? device.getName()  + " " + Integer.toString(device.account): null);
			bottomFrame.setSiteName(dbHelper.getSiteNameByDeviceSection(relay.device_id, relay.section_id));
		}
	}

	private void setTopFrame()
	{
		if (null != topFrame)
		{

			topFrame.setNormal(false);
			topFrame.setTurnedOn(false);

			if(Const.SENSORTYPE_WIRED_OUTPUT == relay.getModelId()){
				topFrame.setType(NTopLayout.Type.relay_wired);
				if(Const.DETECTOR_AUTO_RELAY == relay.detector){
					if(dbHelper.getScriptBindedStatusForRelay(relay)){
						topFrame.setType(NTopLayout.Type.relay_wired_auto_binded);
					}else{
						topFrame.setType(NTopLayout.Type.relay_wired_auto);
					}
				}else if(Const.DETECTOR_SIREN == relay.detector){
					topFrame.setType(NTopLayout.Type.relay_wired_siren);
				}
			}else{
				if(relay.getModelId() == Const.ZONETYPE_8731){
					topFrame.setType(NTopLayout.Type.relay_socket);
				}else if(relay.getModelId() == Const.DETECTOR_SZO){
					topFrame.setType(NTopLayout.Type.relay_szo);
				}else {
					topFrame.setType(NTopLayout.Type.relay_wireless);
				}
			}

			topFrame.setFavorited(favorited);
			topFrame.setTransition(this, transition, transition_start);
			topFrame.setTitle(relay.name);
			topFrame.setBigImage(getBigImage());

			topFrame.setAuto(Const.DETECTOR_AUTO_RELAY == relay.detector);

			if(Const.DETECTOR_MANUAL_RELAY == relay.detector){
				topFrame.setControlType(NTopLayout.ControlType.control);
			}else{
				if(dbHelper.getScriptBindedStatusForRelay(relay)){
					topFrame.setScripted(true);

				}else{
					topFrame.setScripted(false);
//					topFrame.setControlType(NTopLayout.ControlType.none);
				}
				topFrame.setControlType(NTopLayout.ControlType.script);
				Script script = dbHelper.getDeviceScriptsByBind(relay.device_id, relay.id);
				topFrame.setScript(script);
			}
			topFrame.setOnActionListener(new NTopLayout.OnActionListener()
			{
				@Override
				public void onFavoriteChanged()
				{
					if(!favorited){
						addOnMain();
					}else{
						if(null!=mainBar){
							if(dbHelper.deleteMainBar(mainBar)) {
								favorited = false;
								topFrame.setFavorited(favorited);
							}
						}
					}
				}

				@Override
				public void backPressed()
				{
					onBackPressed();
				}

				@Override
				public void onSettingsPressed()
				{
					Intent intent = new Intent(context, RelaySettingsActivity.class);
					intent.putExtra("zone", relay.id);
					intent.putExtra("section", relay.section_id);
					intent.putExtra("device", relay.device_id);
					startActivityForResult(intent, Const.REQUEST_DELETE);
					overridePendingTransition(R.animator.enter, R.animator.exit);
				}

				@Override
				public void onControlPressed(int value)
				{
					JSONObject commandAddress = new JSONObject();
					switch (value){
						case NActionButton.VALUE_ON:
							try
							{
								commandAddress.put("section", relay.section_id);
								commandAddress.put("zone", relay.id);
								commandAddress.put("state", 1);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							break;
						case NActionButton.VALUE_OFF:
							try
							{
								commandAddress.put("section", relay.section_id);
								commandAddress.put("zone", relay.id);
								commandAddress.put("state", 0);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							break;
						default: // for script values
							try
							{
								commandAddress.put("section", relay.section_id);
								commandAddress.put("zone", relay.id);
								commandAddress.put("state", value);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							break;
					}
					nSendMessageToServer(relay, Command.SWITCH, commandAddress, true);
				}

				@Override
				public void onLinkPressed(int id)
				{
					getLibraryScripts(relay);
				}

				@Override
				public void onStateClick(int type)
				{
					Intent intent = new Intent(context, EventsStateActivity.class);
					intent.putExtra("type", type);
					intent.putExtra("zone", relay.id);
					intent.putExtra("section", relay.section_id);
					intent.putExtra("device", relay.device_id);
					startActivity(intent);
				}

			});
			LinkedList<Event> affects = dbHelper.getAffectsByZoneId(site_id, relay.device_id, relay.section_id, relay.id);
//			if(null!=affects)
				topFrame.setStates(affects, relay, false);

			topFrame.bindView();
		}
	}


	private int getBigImage()
	{
		int bigImage = R.drawable.ic_relay_big;

		if(relay.getModelId() == Const.SENSORTYPE_WIRED_OUTPUT){
			DWOType dwoType = relay.getDWOType();
			if (null != dwoType && null != dwoType.icons)
			{
				bigImage = dwoType.getBigIcon(context);
			}
		}else{
			ZType zType  = relay.getModel();
			if(null!=zType && null!=zType.icons){
				bigImage = zType.getBigIcon(context);
			}
		}
		switch (relay.detector){
			case Const.DETECTOR_AUTO_RELAY:
				//set sctipt icon only if it exist. if not - set auto overlay
				int sctiptIcon = 0;
				if(dbHelper.getScriptBindedStatusForRelay(relay)){
					Script script = dbHelper.getDeviceScriptsByBind(relay.device_id, relay.id);
					if(null!=script)sctiptIcon = script.getIcon();
				}
				if(0!=sctiptIcon){
					bigImage =sctiptIcon;
				}
			default:
				break;
		}
		return bigImage;
	}

	private void addOnMain() {
		boolean shared = PrefUtils.getInstance(context).getSharedMainScreenMode();
		Cursor cursor = dbHelper.getMainBars(site_id, shared);
		if (null == cursor || 0 == cursor.getCount()) {
			dbHelper.fillBars(site_id, shared);
		}
		MainBar mainBar = dbHelper.getEmptyBar(site_id, shared);
		if (null != mainBar) {
			mainBar.type = MainBar.Type.CONTROL;
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("device", relay.device_id);
				jsonObject.put("section", relay.section_id);
				jsonObject.put("zone", relay.id);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
			mainBar.element_id = jsonObject.toString();
			if (dbHelper.addNewBar(mainBar, shared)) {
				favorited = true;
				topFrame.setFavorited(favorited);
				this.mainBar = getRefreshedMainBar();
			} else {
				Func.pushToast(context, "DB ERROR", (RelayActivity) context);
			};
		}else{
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
			nDialog.setTitle(getString(R.string.N_ERROR_NO_SPACE_ON_MAIN));
			nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
			{
				@Override
				public void onActionClick(int value, boolean b)
				{
					switch (value){
						case NActionButton.VALUE_CANCEL:
							nDialog.dismiss();
							break;
					}
				}
			});
			nDialog.show();
		}
	}

	private MainBar getRefreshedMainBar()
	{
		return dbHelper.getMainBarForRelay(site_id, relay);
	}

	public void getLibraryScripts(Zone relay)
	{
		((Activity)context).runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				materialDialog = new NDialog(context, R.layout.n_dialog_progress);
				materialDialog.setTitle(getString(R.string.DIALOG_WAIT_TITLE));
				materialDialog.show();
			}
		});
		wait = true;

		sendScriptsRequest(relay.device_id);

		timer = new Timer();
		timer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				wait = false;
				context.sendBroadcast(new Intent(D3Service.BROADCAST_LIBRARY_SCRIPTS));
			}
		}, 15000);
	}

	private void sendScriptsRequest(int device_id)
	{
		Device device  = dbHelper.getDeviceById(device_id);
		JSONObject message = new JSONObject();
		try
		{
			message.put("config_version", device.config_version);
			if(!nSendMessageToServer(null, D3Request.LIBRARY_SCRIPTS, message, false)){
				wait = false;
				context.sendBroadcast(new Intent(D3Service.BROADCAST_LIBRARY_SCRIPTS));
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	public void goToScriptSet(){
		if (materialDialog != null && materialDialog.isShowing())
		{
			materialDialog.dismiss();
			if (wait)
			{
				if (null != timer) timer.cancel();
				wait = false;
				if(!dbHelper.getScriptBindedStatusForRelay(relay))
				{
					openScriptsLibrary();
				}else{
					openDeviceScript();
				}
			}else{
				Func.nShowMessage(context, context.getString(R.string.SCRIPT_ERROR_CANT_LOAD_LIB));
			}
		}
	}

	private void openDeviceScript()
	{
		int script_id = dbHelper.getBindedScriptIdForRelay(relay);
		if(-1 != script_id)
		{
			Intent deviceScriptsIntent = new Intent(getBaseContext(), ScriptActivity.class);
			deviceScriptsIntent.putExtra("script_id", script_id);
			deviceScriptsIntent.putExtra("site_id", site_id);
			startActivity(deviceScriptsIntent);
		}else{
			Func.nShowMessage(context, context.getString(R.string.SCRIPT_SET_ERROR));
		}
	}

	private void openScriptsLibrary(){
		Intent intent = new Intent(getBaseContext(), ScriptsLibraryActivity.class);
		intent.putExtra("device_id", relay.device_id);
		intent.putExtra("site_id", site_id);
		intent.putExtra("bind", relay.id);
		startActivity(intent);
	}

	public void  onResume(){
		super.onResume();
		if(!binded)bind();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		registerReceiver(updateReceiver, intentFilter);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		registerReceiver(libraryScriptsReceiver, new IntentFilter(D3Service.BROADCAST_LIBRARY_SCRIPTS));
		bindD3();
	}

	public void onPause(){
		super.onPause();
		binded = false;
		unregisterReceiver(updateReceiver);
		unregisterReceiver(libraryScriptsReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				finish();
			}
		}
	}
}
