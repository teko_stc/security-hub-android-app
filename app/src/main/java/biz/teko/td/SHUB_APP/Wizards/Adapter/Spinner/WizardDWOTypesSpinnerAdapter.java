package biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class WizardDWOTypesSpinnerAdapter extends ArrayAdapter<DWOType>
{
	private final Context context;
	private final DWOType[] dwoTypes;

	public WizardDWOTypesSpinnerAdapter(@NonNull Context context, int resource, DWOType[] dwoTypes)
	{
		super(context, resource, dwoTypes);
		this.context = context;
		this.dwoTypes = dwoTypes;
	}

	public int getCount(){
		return dwoTypes.length;
	}

	public DWOType getItem(int position){
		return dwoTypes[position];
	}

	public long getItemId(int position){
		return position;
	}

	@Override
	public int getPosition(@Nullable DWOType item)
	{
		return super.getPosition(item);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		label.setText(dwoTypes[position].caption);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		label.setText(dwoTypes[position].caption);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}
}
