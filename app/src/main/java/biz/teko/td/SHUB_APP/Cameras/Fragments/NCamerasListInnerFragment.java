package biz.teko.td.SHUB_APP.Cameras.Fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.NDahuaHelper;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaP2PLoginModule;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaSnapshotModule;
import biz.teko.td.SHUB_APP.Cameras.Adapters.NCamerasCursorAdapter;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Utils.Other.NGridView;

public class NCamerasListInnerFragment extends Fragment
{
	private Context context;
	private DBHelper dbHelper;

	private LinearLayout ivFrame;
	private NGridView ivGrid;
	private LinearLayout rtspFrame;
	private NGridView rtspGrid;
	private LinearLayout dahuaFrame;
	private NGridView dahuaGrid;

	private final int col_count = 2;

	private FrameLayout progress;


	private NCamerasCursorAdapter rtspGridAdapter;
	private NCamerasCursorAdapter ivGridAdapter;
	private NCamerasCursorAdapter dahuaGridAdapter;

	private Cursor cursorRTSP;
	private Cursor cursorIV;
	private Cursor cursorDahua;

	private boolean iv_loaded = false;
	private boolean rtsp_loaded = false;
	private NWithAButtonElement emptyFrame;
	private OnElementClickListener onElementClicklistener;
	private Camera[] dahuaCameras;
	private NDahuaHelper nDahuaHelper;

//	HashMap<String, NDahuaP2PLoginModule> nDahuaP2PLoginModules= new HashMap<>();;
	HashMap<String, Long> nDahuaHandlers= new HashMap<>();;
	HashMap<String, NDahuaSnapshotModule> nDahuaSnapshotModules = new HashMap<>();;
	private AsyncTask[] loginTasks;
	private boolean to_c;

	public void setOnElementClickListener(OnElementClickListener onElementClickListener){
		this.onElementClicklistener = onElementClickListener;
	}

	public interface OnElementClickListener
	{
		void onNewClick();
		void getIVCameras();
		void onCameraClick(Camera camera);
		void onCameraLongClick(Camera camera);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.n_fragment_cameras, container, false);
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		nDahuaHelper = NDahuaHelper.getInstance(context);

		setup(v);
		bind(v);
		return v;
	}

	private void setup(View v)
	{
		ivFrame = (LinearLayout) v.findViewById(R.id.nIVCamerasFrame);
		ivGrid = (NGridView) v.findViewById(R.id.nGridIVCameras);
		rtspFrame = (LinearLayout) v.findViewById(R.id.nRTSPCamerasFrame);
		rtspGrid = (NGridView) v.findViewById(R.id.nGridRTSPCameras);
		dahuaFrame = (LinearLayout) v.findViewById(R.id.nDahuaCamerasFrame);
		dahuaGrid = (NGridView) v.findViewById(R.id.nGridDahuaCameras);
		progress = (FrameLayout) v.findViewById(R.id.nCamerasWaitProgress);
		emptyFrame = (NWithAButtonElement) v.findViewById(R.id.noElementsView);
	}

	private void bind(View v)
	{
		NCamerasListFragment parent = (NCamerasListFragment) getParentFragment();
		if(null!= parent)
			parent.setOnFragmentRefreshListener((b, cType) -> {
					updateLists(cType);
				});
		if(null!=emptyFrame){
			emptyFrame.setTitle(getString(R.string.N_CAMERAS_NO_CAMERAS));
			emptyFrame.setOnChildClickListener((action, option) -> {
				if(null!=onElementClicklistener) onElementClicklistener.onNewClick();
			});
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		nDahuaHelper.init();
		to_c = false;
//		iv_loaded = false;
//		rtsp_loaded = false;
		loadData();
	}

//	@Override
//	public void onDestroy()
//	{
//		super.onDestroy();
//		if(null!=modules && modules.length > 0){
//			for(NDahuaP2PLoginModule module :modules){
//				module.logout();
//				module.stopP2pService();
//			}
//		}
//	}

	private void loadData()
	{
		getIVCameras();
		getDahuaPreview();
		updateLists(Camera.CType.DEFAULT);
	}

	private void getDahuaPreview()
	{
		dahuaCameras = dbHelper.getAllDahuaCameras();
		if(null!=dahuaCameras && dahuaCameras.length > 0){
			for(int i = 0; i < dahuaCameras.length; i++){
				Camera camera = dahuaCameras[i];
				NDahuaP2PLoginModule p2pLoginModule = nDahuaHelper.getP2PModule(camera.sn);
				if(null==p2pLoginModule) {
					p2pLoginModule = new NDahuaP2PLoginModule();
				}else{
					if(p2pLoginModule.isServiceStarted()){
						p2pLoginModule.logout();
						p2pLoginModule.stopP2pService();
					}
				}
				p2pLoginModule.startLoginTask(camera, (r_camera) -> {
					if(null!=r_camera)
					{
						Func.log_d(Const.LOG_TAG_DAHUA, "callback login task camera" + " " + camera.sn);
						NDahuaSnapshotModule nDahuaSnapshotModule = new NDahuaSnapshotModule(context, camera);
						nDahuaSnapshotModule.setSnapRevCallBack((lLoginID, pBuf, RevLen, EncodeType, CmdSerial) -> {
							Func.log_d(Const.LOG_TAG_DAHUA, "callback snapshot task camera" + " " + camera.sn);
							byte[] data = new byte[RevLen];
							System.arraycopy(pBuf, 0, data, 0, RevLen);
							dbHelper.setDahuaCameraPreview(camera, data);
							context.sendBroadcast(new Intent(NDahuaHelper.BROADCAST_GET_DAHUA_PREVIEW));
						});
						nDahuaSnapshotModule.remoteCapturePicture(0);
						nDahuaSnapshotModules.put(camera.sn, nDahuaSnapshotModule);
					}
//					else{
//						if(p2pLoginModule.isServiceStarted()){
//							p2pLoginModule.logout();
//							p2pLoginModule.stopP2pService();
//						}
//					}
				});
//				i++;
				nDahuaHelper.addP2PModule(camera.sn, p2pLoginModule);
//				nDahuaP2PLoginModules.put(camera.sn, p2pLoginModule);
			}
		}
	}

	private void stopDahuaProcesses(Camera camera){
		if(null!=nDahuaHelper.getNDahuaP2PLoginModules()){
			for(Map.Entry<String, NDahuaP2PLoginModule> p2pModule: nDahuaHelper.getNDahuaP2PLoginModules().entrySet()){
				if(null==camera ||camera.type!= Camera.CType.DAHUA ||  !camera.sn.equals(p2pModule.getKey())) {
					if(p2pModule.getValue().isServiceStarted()){
						p2pModule.getValue().logout();
						p2pModule.getValue().stopP2pService();
					}
				}
			}
		}
	}

	private void getIVCameras()
	{
		if(null!=onElementClicklistener) onElementClicklistener.getIVCameras();
	}

	private void updateLists(Camera.CType cType)
	{
		switch (cType){
			case IV:
				updateIVlist();
				break;
			case RTSP:
				updateRTSPList();
				break;
			case DAHUA:
				updateDahuaList();
				break;
			default:
				updateIVlist();
				updateRTSPList();
				updateDahuaList();
				break;
		}
	}

	private void updateDahuaList()
	{
		cursorDahua = dbHelper.getDahuaCamerasCursor();
		if(null!=cursorDahua){
			if(null==dahuaGridAdapter){
				dahuaGridAdapter = getAdapter(Camera.CType.DAHUA);
				dahuaGrid.setAdapter(dahuaGridAdapter);
			}else{
				dahuaGridAdapter.update(cursorDahua);
			}
			dahuaGrid.setDynamicHeight(2);
		}
		//		dahua_loaded = true;
		updateVisiblity();
	}

	private void updateRTSPList()
	{
//		rtspGrid.setNumColumns(col_count);
		cursorRTSP = dbHelper.getRTSPCamerasCursor();
		if(null!=cursorRTSP){
			if(null==rtspGridAdapter){
				rtspGridAdapter = getAdapter(Camera.CType.RTSP);
				rtspGrid.setAdapter(rtspGridAdapter);
			}else{
				rtspGridAdapter.update(cursorRTSP);
			}
			rtspGrid.setDynamicHeight(2);
		}
//		rtsp_loaded = true;
		updateVisiblity();
	}

	private void updateIVlist()
	{
//		ivGrid.setNumColumns(col_count);
		cursorIV = dbHelper.getIVCamerasCursor();
		if(null!=cursorIV){
			if(null==ivGridAdapter){
				ivGridAdapter = getAdapter(Camera.CType.IV);
				ivGrid.setAdapter(ivGridAdapter);
			}else{
				ivGridAdapter.update(cursorIV);
			}
			ivGrid.setDynamicHeight(2);
		}
		updateVisiblity();
	}

	private NCamerasCursorAdapter getAdapter(Camera.CType cType)
	{
		return new NCamerasCursorAdapter(context, R.layout.n_camera_grid_element, getCursor(cType), cType, 0,
				new NCamerasCursorAdapter.OnCamerasElementClickListener()
					{
						@Override
						public void onClick(Camera camera)
						{
							if(null!=onElementClicklistener) {
								to_c = true;
//								if(camera.type == Camera.CType.DAHUA && null!=nDahuaHandlers && nDahuaHandlers.size() > 0) {
//									long l_handle = nDahuaHandlers.get(camera.sn);
//									camera.setLoginHandle(l_handle);
//								}
								stopDahuaProcesses(camera);
								onElementClicklistener.onCameraClick(camera);
							}
						}

						@Override
						public void onLongClick(Camera camera)
						{
							if(null!=onElementClicklistener) onElementClicklistener.onCameraLongClick(camera);
						}

						@Override
						public void onLoaded()
						{
							if(null!=ivGrid) ivGrid.setDynamicHeight(2);
						}
					});
	}

	private Cursor getCursor(Camera.CType cType)
	{
		switch (cType){
			case IV:
				return cursorIV;
			case RTSP:
				return cursorRTSP;
			case DAHUA:
				return cursorDahua;
			default:
				return null;
		}
	}

	private void updateVisiblity()
	{
		ivFrame.setVisibility(getIVExist() ? View.VISIBLE : View.GONE);
		rtspFrame.setVisibility(getRTSPExist() ? View.VISIBLE : View.GONE);
		dahuaFrame.setVisibility(getDahuaExist() ? View.VISIBLE : View.GONE);
//		progress.setVisibility(loaded() ? View.GONE : View.VISIBLE);
		progress.setVisibility(View.GONE);
		emptyFrame.setVisibility(getIVExist() || getRTSPExist() || getDahuaExist() ? View.GONE : View.VISIBLE);
//		emptyFrame.setVisibility(loaded() && !getIVExist() && !getRTSPExist() ? View.VISIBLE : View.GONE);
//		emptyFrame.setVisibility(View.GONE);
	}

//	private boolean loaded()
//	{
//		return iv_loaded && rtsp_loaded;
//	}

	private boolean getDahuaExist() {
		return  (null!=cursorDahua && cursorDahua.getCount() > 0);
	}

	private boolean getRTSPExist()
	{
		return  (null!=cursorRTSP && cursorRTSP.getCount() >0);
	}

	private boolean getIVExist()
	{
		return (null!=cursorIV && cursorIV.getCount() > 0);
	}



	@Override
	public void onPause()
	{
		super.onPause();
		if(!to_c)stopDahuaProcesses(null);
	}
}
