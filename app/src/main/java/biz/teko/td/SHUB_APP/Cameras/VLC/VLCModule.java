package biz.teko.td.SHUB_APP.Cameras.VLC;

import android.annotation.TargetApi;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.util.ArrayList;

import biz.teko.td.SHUB_APP.Utils.Other.VideoControllerView;

public class VLCModule implements IVLCVout.OnNewVideoLayoutListener,
        IVLCVout.Callback {
    private final SurfaceView surfaceView;
    private final FrameLayout surfaceContainer;
    private final Context context;
    private final String url;
    private MediaPlayer mediaPlayer;
    private IVLCVout vout;
    private VideoControllerView controller;
    private LibVLC libVLC;
    private VLCListener eventListener;
    private boolean isFull = false;
    private int mVideoHeight = 0;
    private int mVideoWidth = 0;
    private ISizeClickListener listener;

    public void setEventListener(VLCListener eventListener) {
        this.eventListener = eventListener;
    }

    public void setListener(ISizeClickListener listener) {
        this.listener = listener;
    }

    public void playerNull() {
        mediaPlayer = null;
    }

    public interface ISizeClickListener {
        void fullscreen();
        void windowed();
    }

    public VLCModule(SurfaceView surfaceView, FrameLayout surfaceContainer, Context context, String url) {
        this.surfaceView = surfaceView;
        this.surfaceContainer = surfaceContainer;
        this.context = context;
        this.url = url;
        setLibVLC();
        setMediaPlayer(getMedia());
    }

    private void setLibVLC() {
        ArrayList<String> options = new ArrayList<>();
//        options.add("--aout=opensles");
        options.add("--audio-time-stretch");
        options.add("-vvv");
        libVLC = new LibVLC(context, options);
    }

    private Media getMedia() {
        return new Media(libVLC, Uri.parse(url));
    }

    private void setMediaPlayer(Media media) {
        mediaPlayer = new MediaPlayer(libVLC);
        vout = mediaPlayer.getVLCVout();
        vout.setVideoView(surfaceView);
        controller = new VideoControllerView(context);
        controller.setMediaPlayer(control);
        controller.setAnchorView(surfaceContainer);
        vout.attachViews();
        surfaceView.setOnClickListener(v -> controller.show());
        mediaPlayer.setMedia(media);
        mediaPlayer.setEventListener(event -> {
            switch (event.type) {
                case MediaPlayer.Event.Playing:
                    eventListener.onComplete();
                    break;
                case MediaPlayer.Event.EncounteredError:
                    eventListener.onError();
                    break;
            }
        });
    }

    public IVLCVout getVout() {
        return vout;
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void setFull(boolean full) {
        isFull = full;
        controller.setFullscreen(isFull);
    }

    private final VideoControllerView.MediaPlayerControl control = new VideoControllerView.MediaPlayerControl() {
        @Override
        public void start() {
            if (null != mediaPlayer) mediaPlayer.play();
        }

        @Override
        public void pause() {
            if (null != mediaPlayer) mediaPlayer.pause();
        }

        @Override
        public int getDuration() {
            return 0;
        }

        @Override
        public int getCurrentPosition() {
            return 0;
        }

        @Override
        public void seekTo(int pos) {
        }

        @Override
        public boolean isPlaying() {
            if (null != mediaPlayer) {
                return mediaPlayer.isPlaying();
            }
            return false;
        }

        @Override
        public int getBufferPercentage() {
            return 0;
        }

        @Override
        public boolean canPause() {
            return true;
        }

        @Override
        public boolean isFullScreen() {
            return controller.isFullscreen();
        }

        @Override
        public void toggleFullScreen(ViewGroup mAnchor) {
            if (listener != null) {
                if (!isFull)
                    listener.fullscreen();
                else
                    listener.windowed();
            }
        }
    };

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onNewVideoLayout(IVLCVout vlcVout, int width, int height,
                                 int visibleWidth, int visibleHeight,
                                 int sarNum, int sarDen) {

        mVideoWidth = width;
        mVideoHeight = height;
    }

    @Override
    public void onSurfacesCreated(IVLCVout vlcVout) {
    }

    public void onSurfacesDestroyed(IVLCVout vlcVout) {
    }
}
