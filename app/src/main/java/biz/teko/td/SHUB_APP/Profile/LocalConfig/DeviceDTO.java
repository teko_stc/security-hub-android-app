package biz.teko.td.SHUB_APP.Profile.LocalConfig;

public class DeviceDTO {
    private final String ip;
    private final int pin;
    private final String serial;
    private final String name;

    private boolean isTestMode = false;

    public DeviceDTO(String ip, int pin, String serial, String name) {
        this.ip = ip;
        this.pin = pin;
        this.serial = serial;
        this.name = name;
    }


    public String getIp() {
        return ip;
    }

    public int getPin() {
        return pin;
    }

    public String getSerial() {
        return serial;
    }

    public String getName() {
        return name;
    }

    public boolean isTestMode() {
        return isTestMode;
    }

    public void setTestMode(boolean testMode) {
        isTestMode = testMode;
    }
}
