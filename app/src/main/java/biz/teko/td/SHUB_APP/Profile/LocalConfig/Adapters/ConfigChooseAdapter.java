package biz.teko.td.SHUB_APP.Profile.LocalConfig.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import biz.teko.td.SHUB_APP.Profile.Models.LocalConfigModel;
import biz.teko.td.SHUB_APP.R;

public class ConfigChooseAdapter extends RecyclerView.Adapter<ConfigChooseViewHolder> {
    private final List<LocalConfigModel> configs;
    private OnConfigClickListener listener;

    public interface OnConfigClickListener
    {
        void onConfigClick(String type, View view);
    }

    public ConfigChooseAdapter(List<LocalConfigModel> configs) {
        this.configs = configs;
    }

    public void setOnConfigClickListener(OnConfigClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ConfigChooseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ConfigChooseViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.n_config_choose_list_element, parent, false),
                listener, parent.getContext());
    }

    @Override
    public void onBindViewHolder(@NonNull ConfigChooseViewHolder holder, int position) {
        holder.bind(configs.get(position));
    }

    @Override
    public int getItemCount() {
        return configs.size();
    }
}
