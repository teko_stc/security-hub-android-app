package biz.teko.td.SHUB_APP.Receivers;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.Widgets.NormalWidget;
import biz.teko.td.SHUB_APP.Widgets.WidgetConfigActivity;

/**
 * Created by td13017 on 24.05.2017.
 */

public class WidgetUpdateReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		final int[] appWidgetIds = manager.getAppWidgetIds(new ComponentName(context, NormalWidget.class));
		SharedPreferences sp = context.getSharedPreferences(WidgetConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
		DBHelper dbHelper = DBHelper.getInstance(context);

		for (int i = 0; i < appWidgetIds.length; ++i)
		{
			NormalWidget.updateWidget(context, manager, sp, appWidgetIds[i]);
		}
	}
}
