package biz.teko.td.SHUB_APP.ZonesTabs.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import biz.teko.td.SHUB_APP.ZonesTabs.Fragments.ZonesHistoryTabFragment;
import biz.teko.td.SHUB_APP.ZonesTabs.Fragments.ZonesTabFragment;

/**
 * Created by td13017 on 10.02.2017.
 */

public class ZonesViewPagerAdapter extends FragmentStatePagerAdapter
{
	private final int siteId;
	private final int deviceId;
	private final int sectionId;
	private final int zoneOrRelay;
	private CharSequence Titles[];
	private int NumOfTabs;

	public ZonesViewPagerAdapter(FragmentManager fm, CharSequence[] titles, int num, int siteId, int deviceId, int sectionId, int zoneOrRelay)
	{
		super(fm);
		this.Titles = titles;
		this.NumOfTabs = num;
		this.siteId = siteId;
		this.deviceId = deviceId;
		this.sectionId = sectionId;
		this.zoneOrRelay = zoneOrRelay; // 0 - zones, 1- relays
	}

	@Override
	public Fragment getItem(int position)
	{
		if(position == 0){
			return ZonesTabFragment.newInstance(siteId, deviceId, sectionId, zoneOrRelay);
		}else{
			return ZonesHistoryTabFragment.newInstance(siteId, deviceId, sectionId, zoneOrRelay);
		}
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		return Titles[position];
	}

	@Override
	public int getCount()
	{
		return NumOfTabs;
	}
}
