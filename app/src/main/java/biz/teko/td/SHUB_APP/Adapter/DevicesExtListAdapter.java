//package biz.teko.td.SHUB_APP.Adapter;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.database.Cursor;
//import android.graphics.Typeface;
//import android.support.v4.widget.ResourceCursorAdapter;
//import android.text.InputFilter;
//import android.util.Log;
//import android.util.TypedValue;
//import android.view.Gravity;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageButton;
//import android.widget.ImageView;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import org.json.JSONException;
//
//import java.lang.reflect.Field;
//import java.util.LinkedList;
//
//import biz.teko.td.SHUB_APP.Utils.Dir.Func;
//import biz.teko.td.SHUB_APP.OldServerLib.ElementActivity;
//import biz.teko.td.SHUB_APP.D3.D3AbsQueueElement;
//import biz.teko.td.SHUB_APP.D3.D3Response;
//import biz.teko.td.SHUB_APP.D3.D3Service;
//import biz.teko.td.SHUB_APP.D3DB.Affect;
//import biz.teko.td.SHUB_APP.D3DB.DBHelper;
//import biz.teko.td.SHUB_APP.D3DB.Device;
//import biz.teko.td.SHUB_APP.D3DB.UserInfo;
//import biz.teko.td.SHUB_APP.R;
//
///**
// * Created by td13017 on 21.09.2016.
// */
//public class DevicesExtListAdapter extends ResourceCursorAdapter
//{
//	private static final String DETECTOR_LIST_CHAGED = "Detector list been changed";
//	Context context;
//	int site;
//	int secId;
//	int layout;
//	Cursor cursor;
//	private DBHelper dbHelper;
//	private D3Service service;
//	private D3Service myService;
//	private ImageView imageSab;
//	private ImageView imageMalf;
//	private ImageView imageAtt;
//	private ImageView imageArm;
//	private ImageView imagePow;
//	private ImageView imageConn;
//	private ImageView imageConnEth;
//	private ImageView imageRadioConn;
//	private ImageView[] imagesSmall;
//
//
//	public DevicesExtListAdapter(Context context, int layout, int site, int secId, Cursor c, int flags)
//	{
//		super(context, layout, c, flags);
//		this.context = context;
//		this.cursor = c;
//		this.site = site;
//		this.secId = secId;
//	}
//
//	private void setTextViewText(View view, Context context, int id, String string){
//		TextView textView = (TextView) view.findViewById(id);
//		if(null!=textView){
//			textView.setText(string);
//		}
//	}
//
//	public void update(Cursor cursor){
//		this.changeCursor(cursor);
//	}
//
//	@Override
//	public void bindView(View view, Context context, Cursor cursor)
//	{
//		dbHelper = DBHelper.newInstance(context);
//		Device device = dbHelper.getZone(cursor);
//		setTextViewText(view, context, R.id.textDeviceName, device.name);
//		setTextViewText(view, context, R.id.textDeviceNumber, "Датчик номер " + device.id);
//		setTextViewText(view, context, R.id.textDeviceBattery, "Уровень заряда: " + dbHelper.getZoneBatteryLevel(device));
//
//		LinearLayout deviceStatementsLayout = (LinearLayout) view.findViewById(R.id.deviceStatementImageLayout);
//
////		imageSab = new ImageView(context);
////		imageSab.setLayoutParams(new ViewGroup.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
////		deviceStatementsLayout.addView(imageSab);
////		imageMalf = new ImageView(context);
////		imageMalf.setLayoutParams(new ViewGroup.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
////		deviceStatementsLayout.addView(imageMalf);
////		imageAtt = new ImageView(context);
////		imageAtt.setLayoutParams(new ViewGroup.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
////		deviceStatementsLayout.addView(imageAtt);
////		imageArm = new ImageView(context);
////		imageArm.setLayoutParams(new ViewGroup.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
////		deviceStatementsLayout.addView(imageArm);
////		imagePow = new ImageView(context);
////		imagePow.setLayoutParams(new ViewGroup.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
////		deviceStatementsLayout.addView(imagePow);
////		imageConn = new ImageView(context);
////		imageConn.setLayoutParams(new ViewGroup.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
////		deviceStatementsLayout.addView(imageConn);
////		imageConnEth = new ImageView(context);
////		imageConnEth.setLayoutParams(new ViewGroup.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
////		deviceStatementsLayout.addView(imageConnEth);
////		imageRadioConn = new ImageView(context);
////		imageRadioConn.setLayoutParams(new ViewGroup.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
////		deviceStatementsLayout.addView(imageRadioConn);
////
//
//		LinkedList<Affect> affects = dbHelper.getAffectsByZoneId(site, secId, device.id);
//
//		imagesSmall = new ImageView[8];
//
//		for(int i = 0; i < 8; i++)
//		{
//			imagesSmall[i] = new ImageView(context);
//			imagesSmall[i].setLayoutParams(new LinearLayout.LayoutParams(Func.dpToPx(20, context), Func.dpToPx(20, context)));
//		}
//		int imgSmallNum = 0;
//		int al = 0;
//		int sab = 0;
//		int mal = 0;
//
//		if(null!=affects)
//		{
//			for (Affect affect : affects)
//			{
//				switch (affect.classId)
//				{
//					case 0:
//						imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
//						imgSmallNum++;
//						al++;
//						break;
//					case 1:
//						imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
//						imgSmallNum++;
//						sab++;
//						break;
//					case 2:
//						imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
//						imgSmallNum++;
//						mal++;
//						break;
//					case 3:
//						imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
//						imgSmallNum++;
//						mal++;
//						break;
//					case 4:
////						imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
////						imgSmallNum++;
//						break;
//					case 6:
//						imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
//						imgSmallNum++;
//						mal++;
//						break;
//					case 7:
////						if ((21 == affect.detectorId) || (22 == affect.detectorId))
////						{
////							if (21 == affect.detectorId)
////							{
////								//											imageConn.setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
////								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
////								imgSmallNum++;
////							}
////							if (22 == affect.detectorId)
////							{
////								//											imageConnEth.setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
////								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
////								imgSmallNum++;
////							}
////						} else
////						{
////							//										imageRadioConn.setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
//						imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
//						imgSmallNum++;
//						mal++;
////						}
//						break;
//				}
//			}
//		}
//		String state = "Датчик в норме";
//		if(al>0){
//			state = "Тревога";
//			if(sab>0){
//				state += " , саботаж";
//				if(mal>0){
//					 state+=" , неисправность";
//				}
//			}
//			else{
//				if(mal>0){
//					state+=" , неисправность";
//				}
//			}
//		}else{
//			if(sab>0){
//				state = "Саботаж";
//				if(mal>0){
//					state+=" , неисправность";
//				}
//			}else{
//				if(mal>0){
//					state ="Неисправность";
//				}
//			}
//		}
//		setTextViewText(view, context, R.id.textDeviceAffects, state);
//
//		for(int n = 0; n< imgSmallNum;n++){
//			deviceStatementsLayout.addView(imagesSmall[n]);
//		}
//
//		ImageButton deviceButtonMore = (ImageButton) view.findViewById(R.id.deviceMenuButton);
//		deviceButtonMore.setOnClickListener(new siteCardButtonMoreOnClick(device, dbHelper));
//
//	}
//
//	private class siteCardButtonMoreOnClick implements View.OnClickListener
//	{
//		private final ElementActivity activity;
//		private Device device;
//		private DBHelper dbHelper;
//
//		public siteCardButtonMoreOnClick(Device device, DBHelper dbHelper)
//		{
//			this.device = device;
//			this.dbHelper = dbHelper;
//			activity  = (ElementActivity) context;
//		}
//
//		@Override
//		public void onClick(View v)
//		{
//			AlertDialog.Builder deviceMenuDialogBuilder = Func.adbForCurrentSDK(context);
//			final AlertDialog deviceMenuDialog = deviceMenuDialogBuilder.create();
//
//			LinearLayout parentLayout = new LinearLayout(context);
//			parentLayout.setOrientation(LinearLayout.VERTICAL);
//			parentLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//			parentLayout.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
//			parentLayout.setPadding(Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context));
//
//			TextView titleText = new TextView(context);
//			titleText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//			titleText.setPadding(0, 0, 0, Func.dpToPx(12, context));
//			titleText.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//			titleText.setTextColor(context.getResources().getColor(R.color.brandColorDark));
//			titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
//			titleText.setGravity(Gravity.CENTER);
//			titleText.setText(device.name);
//			parentLayout.addView(titleText);
//
//			final Button buttonRenameDetector = new Button(context);
//			buttonRenameDetector.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1f));
//			buttonRenameDetector.setPadding(0, 0, 0, Func.dpToPx(12, context));
//			buttonRenameDetector.setText(R.string.DELA_DEVICE_RENAME_BUTTON_TEXT);
//			buttonRenameDetector.setTypeface(null, Typeface.BOLD);
//			buttonRenameDetector.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//			buttonRenameDetector.setGravity(Gravity.CENTER_VERTICAL);
//			buttonRenameDetector.setBackgroundResource(R.drawable.rectangle_white);
//			buttonRenameDetector.setOnTouchListener(new View.OnTouchListener()
//			{
//				@Override
//				public boolean onTouch(View v, MotionEvent event)
//				{
//					switch (event.getAction())
//					{
//						case MotionEvent.ACTION_DOWN:
//							buttonRenameDetector.setTextColor(context.getResources().getColor(R.color.md_white_1000));
//							buttonRenameDetector.setBackgroundColor(context.getResources().getColor(R.color.brandColorDark));
//							break;
//						case MotionEvent.ACTION_UP:
//							buttonRenameDetector.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//							buttonRenameDetector.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
//							break;
//					}
//					return false;
//				}
//			});
//			buttonRenameDetector.setOnClickListener(new View.OnClickListener()
//			{
//				@Override
//				public void onClick(View v)
//				{
//					AlertDialog.Builder renameDialogBuilder = Func.adbForCurrentSDK(context);
//					final AlertDialog renameDialog = renameDialogBuilder.create();
//
//					LinearLayout parentLayout = new LinearLayout(context);
//					parentLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//					parentLayout.setOrientation(LinearLayout.VERTICAL);
//					parentLayout.setPadding(Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context));
//					parentLayout.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//
//					TextView titleText = new TextView(context);
//					titleText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//					titleText.setPadding(0, 0, 0, Func.dpToPx(12, context));
//					titleText.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//					titleText.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//					titleText.setTypeface(null, Typeface.BOLD);
//					titleText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
//					titleText.setText(R.string.DELA_DETECTOR_RENAME_DIALOG_TITLE);
//					parentLayout.addView(titleText);
//
//					final EditText editName = new EditText(context);
//					editName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//					editName.setCursorVisible(true);
//					editName.isCursorVisible();
//					try
//					{
//						Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
//						f.setAccessible(true);
//						f.set(editName, R.drawable.caa_cursor);
//					} catch (Exception ignored)
//					{
//					}
//					editName.setGravity(Gravity.CENTER);
//					editName.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//					editName.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//					editName.setText(device.name);
//					int editNameMaxLength = 30;
//					editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
//					editName.requestFocus();
//
//					parentLayout.addView(editName);
//
//					LinearLayout buttonLayout = new LinearLayout(context);
//					buttonLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//					buttonLayout.setOrientation(LinearLayout.HORIZONTAL);
//					buttonLayout.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//
//					final Button buttonOk = new Button(context);
//					buttonOk.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
//					buttonOk.setBackgroundResource(R.drawable.rectangle_white);
//					buttonOk.setText(R.string.SITE_NAME_CHANGE);
//					buttonOk.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//					buttonOk.setTypeface(null, Typeface.BOLD);
//					buttonOk.setOnTouchListener(new View.OnTouchListener()
//					{
//						@Override
//						public boolean onTouch(View v, MotionEvent event)
//						{
//							switch (event.getAction())
//							{
//								case MotionEvent.ACTION_DOWN:
//									buttonOk.setTextColor(context.getResources().getColor(R.color.brandColorDark));
//									break;
//								case MotionEvent.ACTION_UP:
//									buttonOk.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//									break;
//							}
//							return false;
//						}
//					});
//					buttonOk.setOnClickListener(new View.OnClickListener()
//					{
//						@Override
//						public void onClick(View v)
//						{
//							if (!editName.getText().toString().matches(""))
//							{
//								String newName = editName.getText().toString();
//								final UserInfo userInfo = dbHelper.getUserInfo();
//								myService = getService();
//								myService.addInNormalQueue(new D3AbsQueueElement(D3AbsQueueElement.REQ_TYPE.JSON, "{\"S\":\"" + userInfo.getSession() + "\",\"Q\":\"SITE_SET\",\"D\":{\"site\":{\"id\":" + site + ",\"sections\":[{\"id\":" + secId + ",\"zones\":[{\"id\":" + device.id + ",\"name\":\"" + newName + "\"}]}]}}}")
//								{
//									@Override
//									public void receiver(String data)
//									{
//										if (null != data)
//										{
//											try
//											{
//												D3Response d3Response = new D3Response(data);
//												if (1 == d3Response.getResult())
//												{
//													myService.addInNormalQueue(new D3AbsQueueElement(D3AbsQueueElement.REQ_TYPE.JSON, "{\"S\":\"" + userInfo.getSession() + "\",\"Q\":\"SITE_GET\",\"D\":{\"site\":" + site + "}}")
//													{
//														@Override
//														public void receiver(String data)
//														{
////															if (null != data)
////															{
////																Log.d("D3CAA", "site_info:" + data);
////																D3Response d3_site_response = null;
////																try
////																{
////																	d3_site_response = new D3Response(data);
////																	if (0 != d3_site_response.getResult())
////																	{
////																		final Site site = d3_site_response.getSiteFull();
////																		if (null != site)
////																		{
////																			dbHelper.addSite(site);
////																			Section[] sections = site.sections;
////																			dbHelper.addSectionsOfSite(site.id, sections);
////																			for (Section section : sections)
////																			{
////																				Device[] devices = section.devices;
////																				dbHelper.addDevicesOfSection(site.id, section.id, devices);
////																			}
////																			context.sendBroadcast(new Intent(D3Service.BROADCAST_UPDATE_SITES));
////																			activity.runOnUiThread(new Runnable()
////																			{
////																				@Override
////																				public void run()
////																				{
////																					///TIMEOUT NAME CHANGE
////																				}
////																			});
////																			renameDialog.dismiss();
////																			Func.pushToast(context, context.getString(R.string.DELA_RENAME_SUCCESS_PUSH), activity);
////																			///SUCCESS
////																		}
////																	} else
////																	{
////																		Func.pushToast(context, context.getString(R.string.DELA_SERVER_ERROR_PUSH), activity);
////																	}
////																} catch (JSONException e)
////																{
////																	e.printStackTrace();
////																	Func.pushToast(context, context.getString(R.string.DELA_SERVER_ERROR_PUSH), activity);
////																}
////															}
//
//														}
//													});
//												} else
//												{
//													Func.pushToast(context, context.getString(R.string.DELA_GET_ERROR_PUSH), activity);
//
//													//TRY AGAIN
//												}
//											} catch (JSONException e)
//											{
//												e.printStackTrace();
//											}
//											;
//
//										} else
//										{
//											Func.pushToast(context, context.getString(R.string.DELA_SERVER_CONN_ERROR_PUSH), activity);
//										}
//									}
//								});
//								Func.hideKeyboard(activity);
//							} else
//							{
//								Func.pushToast(context, context.getString(R.string.DELA_INCORRECT_NAME_PUSHTOAST), activity);
//							}
//						}
//					});
//					buttonLayout.addView(buttonOk);
//
//					final Button buttonCancel = new Button(context);
//					buttonCancel.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
//					buttonCancel.setBackgroundResource(R.drawable.rectangle_white);
//					buttonCancel.setText(context.getString(R.string.ADD_DETECTOR_DIALOG_CANCEL));
//					buttonCancel.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//					buttonCancel.setTypeface(null, Typeface.BOLD);
//					buttonCancel.setOnTouchListener(new View.OnTouchListener()
//					{
//						@Override
//						public boolean onTouch(View v, MotionEvent event)
//						{
//							switch (event.getAction())
//							{
//								case MotionEvent.ACTION_DOWN:
//									buttonCancel.setTextColor(context.getResources().getColor(R.color.brandColorDark));
//									break;
//								case MotionEvent.ACTION_UP:
//									buttonCancel.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//									break;
//							}
//							return false;
//						}
//					});
//					buttonCancel.setOnClickListener(new View.OnClickListener()
//					{
//						@Override
//						public void onClick(View v)
//						{
//							Func.hideKeyboard(activity);
//							editName.clearFocus();
//							renameDialog.dismiss();
//						}
//					});
//					buttonLayout.addView(buttonCancel);
//					parentLayout.addView(buttonLayout);
//
//					renameDialog.setView(parentLayout);
//
//					deviceMenuDialog.dismiss();
//					renameDialog.show();
//				}
//			});
//
//			final Button buttonDeleteDetector = new Button(context);
//			buttonDeleteDetector.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1f));
//			buttonDeleteDetector.setPadding(0, 0, 0, Func.dpToPx(12, context));
//			buttonDeleteDetector.setText(R.string.DELA_DELETE_ZONE);
//			buttonDeleteDetector.setTypeface(null, Typeface.BOLD);
//			buttonDeleteDetector.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//			buttonDeleteDetector.setGravity(Gravity.CENTER_VERTICAL);
//			buttonDeleteDetector.setBackgroundResource(R.drawable.rectangle_white);
//			buttonDeleteDetector.setOnTouchListener(new View.OnTouchListener()
//			{
//				@Override
//				public boolean onTouch(View v, MotionEvent event)
//				{
//					switch (event.getAction())
//					{
//						case MotionEvent.ACTION_DOWN:
//							buttonDeleteDetector.setTextColor(context.getResources().getColor(R.color.md_white_1000));
//							buttonDeleteDetector.setBackgroundColor(context.getResources().getColor(R.color.brandColorDark));
//							break;
//						case MotionEvent.ACTION_UP:
//							buttonDeleteDetector.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//							buttonDeleteDetector.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
//							break;
//					}
//					return false;
//				}
//			});
//			buttonDeleteDetector.setOnClickListener(new View.OnClickListener()
//			{
//				@Override
//				public void onClick(View v)
//				{
//					LinkedList<Affect> affects = dbHelper.getAffectsByZoneId(site, secId, device.id);
//					int count = 0;
//					if(null!=affects)
//					{
//						for (Affect affect : affects)
//						{
//							if ((4 == affect.classId) && (affect.reasonId == 0))
//							{
//								count++;
//							}
//						}
//					}
//					if(0==count){
//						UserInfo userInfo = dbHelper.getUserInfo();
//						myService = getService();
//						myService.addInNormalQueue(new D3AbsQueueElement(D3AbsQueueElement.REQ_TYPE.JSON, "{\"S\":\"" + userInfo.getSession() + "\",\"Q\":\"COMMAND\",\"D\":{\"command\":{\"REGISTER\":" + ((0x80) | (device.id)) + "},\"site\":" + site + "}}")
//						{
//							@Override
//							public void receiver(String data)
//							{
//								if (null != data)
//								{
//									Log.d("D3ELEMENT", data);
//								} else
//								{
//									Log.d("D3ELEMENT", "RECEIVED NULL");
//								}
//							}
//						});
//						deviceMenuDialog.dismiss();
//						Func.hideKeyboard(activity);
//						Func.nShowMessage(context, context.getString(R.string.EA_NEW_DEVICE_ADD_MESS_DEVICE_WAITING_MODE));
//					}else{
//						deviceMenuDialog.dismiss();
//						Func.nShowMessage(context, context.getString(R.string.DELA_DISARM_MESSAGE));
//					}
//
//				}
//			});
//
//			parentLayout.addView(buttonDeleteDetector);
//			parentLayout.addView(buttonRenameDetector);
//
//			deviceMenuDialog.setView(parentLayout);
//			deviceMenuDialog.show();
//		}
//	}
//
//
//
//	public D3Service getService()
//	{
////		Activity activity = (ElementActivity)context;
////		if(activity != null){
////			ElementActivity elementActivity = (ElementActivity) activity;
////			return elementActivity.getLocalService();
////		}
////		return service;
//		return null;
//	}
//}
