package biz.teko.td.SHUB_APP.Scripts.Activities;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.LinkedList;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Adapters.ScriptsShortListAdapter;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;

public class ScriptsCategoryActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private String category;
	private ListView scriptsList;
	private LinkedList<Script> scriptsArray;
	private HashMap<String, String> scriptsMap;
	private ScriptsShortListAdapter scriptsAdapter;
	private SharedPreferences sp;
	private String iso;
	private int deviceId;
	private int siteId;
	private int bind;
	private Device device;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scripts_category);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		category = getIntent().getStringExtra("category");
		deviceId = getIntent().getIntExtra("device_id", -1);
		siteId = getIntent().getIntExtra("site_id", -1);
		bind = getIntent().getIntExtra("bind", -1);

		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));

		device = dbHelper.getDeviceById(deviceId);

		if (null!=category){
			TextView categoryText = (TextView) findViewById(R.id.categoryName);
			if(null!=categoryText) categoryText.setText(Func.getScriptDataForCurrLang(category, iso).substring(1).replace("/", ": "));
//			category="/" + category.replace(": ", "/");
		}

		TextView backButton = (TextView) findViewById(R.id.scriptBackButton);
		backButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		animateEntry();

		scriptsList = (ListView) findViewById(R.id.scriptsList);
		scriptsList.setDivider(null);
		updateList();

	}

	private void updateList()
	{
		refreshList();
		refreshViewVisiblity();
	}

	private void refreshList(){
		refreshArray();
		refreshAdapter();
	}

	private void refreshArray(){
		scriptsArray = dbHelper.getLibraryScriptsByCategory(category, device!=null? device.config_version:-1);
//		if(null!=scriptsArray)
//		{
//			scriptsMap = new HashMap<String, String>();
//			for (Script script : scriptsArray)
//			{
//				scriptsMap.put(Func.getScriptDataForCurrLang(script, iso), script);
//			}
//		}
	}

	private void refreshAdapter()
	{
		if(null!=scriptsArray)
		{
			scriptsAdapter = new ScriptsShortListAdapter(context, R.layout.card_script_library_list, scriptsArray);
			if (null != scriptsList && null != scriptsAdapter) {
				scriptsList.setAdapter(scriptsAdapter);
				scriptsList.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long l)
					{
						Script script = (Script) parent.getItemAtPosition(position);
						if(null!=category){
							//SHOW DIALOG
//							AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
//							adb.setTitle(Func.getScriptDataForCurrLang(script.nameLib, iso));
//							adb.setMessage(Func.getScriptDataForCurrLang(script.description, iso));
//							adb.show();
							View sharedView = view.findViewById(R.id.textName);
							String transitionName = getString(R.string.transition_script_name);


							ActivityOptions transitionActivityOptions = null;
							if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP)
							{
								transitionActivityOptions = ActivityOptions.makeSceneTransitionAnimation((ScriptsCategoryActivity)context, sharedView, transitionName);
							}
							Intent intent = new Intent(getBaseContext(), ScriptInfoActivity.class);
							intent.putExtra("script", script.libId);
							intent.putExtra("device_id", deviceId);
							intent.putExtra("site_id", siteId);
							intent.putExtra("bind", bind);
							startActivityForResult(intent, D3Service.ADD_NEW_SCRIPT,  transitionActivityOptions.toBundle());
						}
					}
				});
			}
		}
	}

	private void refreshViewVisiblity()
	{
		if(null!= scriptsList){
			if(null!=scriptsAdapter && scriptsAdapter.getCount() != 0)
			{
				scriptsList.setVisibility(View.VISIBLE);
				//				noScriptsText.setVisibility(View.GONE);
			}else{
				scriptsList.setVisibility(View.GONE);
				//				noScriptsText.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	private void animateEntry()
	{
		LinearLayout title = (LinearLayout) findViewById(R.id.scriptsHeaderFrame);
		Animation slideUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);

		if (title.getVisibility() == View.INVISIBLE) {
			title.setVisibility(View.VISIBLE);
			title.startAnimation(slideUp);
		}

		FrameLayout frameOne = (FrameLayout) findViewById(R.id.scriptsInfoFrame);
		Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fade_in);
		frameOne.startAnimation(fadeIn);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.ADD_NEW_SCRIPT){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
}
