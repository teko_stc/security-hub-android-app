package biz.teko.td.SHUB_APP.Utils.Device

import biz.teko.td.SHUB_APP.D3DB.Zone
import biz.teko.td.SHUB_APP.Utils.Models.DeviceInfo

class DeviceConfigHelper {
    fun checkIfController(deviceInfo: DeviceInfo): Boolean {
        val zone: Zone = deviceInfo.zone
        if (zone.section_id == 0 &&
            (zone.id == 0 || zone.id == 100)
        ) {
            return true
        }
        return false
    }
}