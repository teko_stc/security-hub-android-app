package biz.teko.td.SHUB_APP.Zones_N.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Events.Activities.EventsStateActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptActivity;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptsLibraryActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomInfoLayout;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopLayout;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class ZoneActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private Zone zone;
	private boolean transition;
	private int transition_start;
	private String title;
	private int zone_id;
	private int section_id;
	private int device_id;
	private int site_id;
	private NTopLayout topFrame;
	private boolean favorited;
	private MainBar mainBar;
	private NBottomInfoLayout bottomFrame;

	private int model;
	private DType detector;
	private AType alarm;
	private ZType zType;
	private DWIType dwiType;
	private DWOType dwoType;

	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private int sending = 0;
	private int dwoId = 0;

	private boolean binded;

	private NDialog materialDialog;
	private boolean wait;
	private Timer timer;

	private BroadcastReceiver libraryScriptsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			goToScriptSet();
		}
	};

	private BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(Const.ACTION_RMV == intent.getIntExtra("action", -1)
				&& zone_id == intent.getIntExtra("zone", -1)
				&& section_id == intent.getIntExtra("section", -1)
				&& device_id == intent.getIntExtra("device", -1)){
				onBackPressed();
			}else {
				bind();
			}
		}
	};

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};
	private PrefUtils prefUtils;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_zone);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		prefUtils = PrefUtils.getInstance(context);

		title  = getIntent().getStringExtra("bar_title");
		zone_id = getIntent().getIntExtra("zone", -1);
		section_id = getIntent().getIntExtra("section", -1);
		device_id = getIntent().getIntExtra("device", -1);
		site_id = getIntent().getIntExtra("site", -1);

		transition = getIntent().getBooleanExtra("transition",false);
		transition_start = getIntent().getIntExtra("transition_start", 0);

//		favorited = dbHelper.getFavoriteStatusForZone(site_id, zone);

		addTransitionListener(transition);

		setup();
		bind();

	}

	private void setup()
	{
		topFrame = (NTopLayout) findViewById(R.id.nTopFrame);
		bottomFrame = (NBottomInfoLayout) findViewById(R.id.nBottomFrame);
	}

	private void bind()
	{
		binded = true;
		zone = dbHelper.getZoneById(device_id, section_id, zone_id);
		if(null!=zone)
		{
			model = zone.getModelId();
			detector = zone.getDType();
			if (0 != model)
			{
				switch (model)
				{
					case Const.SENSORTYPE_WIRED_OUTPUT:
						dwoType = zone.getDWOType();
						dwoId = zone.getWIOId();
						break;
					case Const.SENSORTYPE_WIRED_INPUT:
						dwiType = zone.getDWIType();
						if (null != dwiType)
						{
							detector = dwiType.getDetectorType(zone.getDetector());
							if (null != detector)
							{
								alarm = detector.getAlarmType(zone.getAlarm());
							}
						}
						dwoId = zone.getWIOId();
						break;
					default:
						zType = D3Service.d3XProtoConstEvent.getZTypeById(model);
						if (null != zType)
						{
							detector = zType.getDetectorType(zone.getDetector());
							if (null != detector)
							{
								alarm = detector.getAlarmType(zone.getAlarm());
							}
						}
						break;
				}
			}

			mainBar = getRefreshedMainBar();
			if (null != mainBar) {
				favorited = true;
			} else
				favorited = false;
			setTopFrame();
			setBottomFrame();
		}else{
			onBackPressed();
		}
	}

	private void setBottomFrame()
	{
		Section section = null;
		Device device = null;

		if(null!=bottomFrame){
			switch (zone.id){
				case 0:
					if(zone.section_id != 0){
						section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
						break;
					}
				case 100:
					device = dbHelper.getDeviceById(zone.device_id);
					break;
				default:
					device = dbHelper.getDeviceById(zone.device_id);
					section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
//					zone = dbHelper.getZoneById(zone.device_id, zone.section_id, zone.id);
					break;
			}
			if(0!=model){
				switch (model) {
					case Const.SENSORTYPE_WIRED_OUTPUT:
						if (null != dwoType)
						{
							bottomFrame.setType(dwoType.caption);
						}
						if(0!=dwoId){
							bottomFrame.setZoneIO(Integer.toString(dwoId));
						}
						break;
					case Const.SENSORTYPE_WIRED_INPUT:
						if(null!=dwiType){
							bottomFrame.setType(dwiType.caption);
						}
						if(0!=dwoId){
							bottomFrame.setZoneIO(Integer.toString(dwoId));
						}
						break;
					default:
						if(null!=zType)
						{
							bottomFrame.setType(zType.caption);
							bottomFrame.setModel(zType.caption_e);
							bottomFrame.setModelImage(zType.getListIcon(context));
						}
						break;
				}

			}

			if(null!=detector) bottomFrame.setDetector(detector.caption);
			if(null!=alarm) bottomFrame.setAlarm(alarm.caption);

//			bottomFrame.setZoneId(Integer.toString(zone.id));
			bottomFrame.setZoneId(null);

			bottomFrame.setSectionName(null!=section ? (section.id == 0 ? getString(R.string.N_ZONE_NOT_IN_SECTION) : section.name ): null);

			bottomFrame.setDeviceName(null!= device? device.getName() + " " + Integer.toString(device.account): null);

			bottomFrame.setSiteName(dbHelper.getSiteNameByDeviceSection(zone.device_id, zone.section_id));
		}
	}

	private void setTopFrame()
	{
		if (null != topFrame)
		{
			topFrame.setType(NTopLayout.Type.zone_wireless);

			int bigImage = R.drawable.n_image_device_common_big_selector;
			switch (zone.getDetector()){
				case Const.DETECTOR_KEYPAD:
				case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
					bigImage = R.drawable.ic_keypad_big;
					break;
				default:
					break;
			}

			if(0!=model){
				switch (model) {
					case Const.SENSORTYPE_WIRED_OUTPUT:
						if (null != dwoType)
						{
							if(null!=dwoType.icons){
								bigImage = dwoType.getBigIcon(context);
							}
						}
						topFrame.setType(NTopLayout.Type.zone_wired);
						break;
					case Const.SENSORTYPE_WIRED_INPUT:
						if(null!=dwiType){
							if(null!=dwiType.icons){
								bigImage = dwiType.getBigIcon(context);
								DType dType = dwiType.getDetectorType(zone.getDetector());
								if(null!=dType){
									AType aType = dType.getAlarmType(zone.getAlarm());
									if(null!=aType && null!=aType.icons){
										bigImage = aType.getBigIcon(context);
									}
								}
							}
						}
						topFrame.setType(NTopLayout.Type.zone_wired);
						break;
					default:
						if(model == Const.ZONETYPE_3731){
							topFrame.setType(NTopLayout.Type.zone_temp);
						}
						if(null!=alarm && null!=alarm.icons)
						{
							bigImage = alarm.getBigIcon(context);
						}else{
							if(null!=zType && null!=zType.icons)bigImage = zType.getBigIcon(context);

						}
						break;
				}

			}
			topFrame.setBigImage(bigImage);
			setZoneArmStatusAndControl();
			topFrame.setTransition(this, transition, transition_start);
			topFrame.setDelayed(zone.delay>0);
			topFrame.setFavorited(favorited);
			topFrame.setTitle(zone.name);

//			topFrame.setArmed(dbHelper.getZoneArmStatus(zone));
			topFrame.setOnActionListener(new NTopLayout.OnActionListener()
			{
				@Override
				public void onFavoriteChanged()
				{
					if(!favorited){
						addOnMain();
					}else{
						if(null!=mainBar){
							if(dbHelper.deleteMainBar(mainBar)) {
								favorited = false;
								topFrame.setFavorited(favorited);
							}
						}
					}
				}

				@Override
				public void backPressed()
				{
					onBackPressed();
				}

				@Override
				public void onSettingsPressed()
				{
					Intent intent = new Intent(context, ZoneSettingsActivity.class);
					intent.putExtra("zone", zone.id);
					intent.putExtra("section", zone.section_id);
					intent.putExtra("device", zone.device_id);
					startActivityForResult(intent, Const.REQUEST_DELETE);
					overridePendingTransition(R.animator.enter, R.animator.exit);
				}

				@Override
				public void onControlPressed(int value)
				{
					JSONObject commandAddress = new JSONObject();
					switch (value)
					{
						case NActionButton.VALUE_RESET:
							try
							{
								commandAddress = new JSONObject();
								commandAddress.put("section", zone.section_id);
								commandAddress.put("zone", zone.id);
								nSendMessageToServer(dbHelper.getDeviceById(zone.device_id), Command.RESET, commandAddress, true);
								dbHelper.closeEventDroppedStatus(zone);
								bind();
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							break;
						default://for scripted
							try
							{
								commandAddress.put("section", zone.section_id);
								commandAddress.put("zone", zone.id);
								commandAddress.put("state", value);
								nSendMessageToServer(dbHelper.getDeviceById(zone.device_id), Command.SWITCH, commandAddress, true);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							break;
					}
				}

				@Override
				public void onLinkPressed(int id)
				{
					getLibraryScripts(zone);
				}

				@Override
				public void onStateClick(int type)
				{
					switch (type){
						case Const.STATE_TYPE_DELAY:
							Func.nShowMessage(context, getString(R.string.N_STATE_DELAY_MESSAGE));
							break;
						case Const.STATE_TYPE_DELAY_ARM:
							Func.nShowMessage(context, getString(R.string.N_STATE_DELAY_ARM_MESSAGE_ZONE));
							break;
						default:
							Intent intent = new Intent(context, EventsStateActivity.class);
							intent.putExtra("type", type);
							intent.putExtra("zone", zone.id);
							intent.putExtra("section", zone.section_id);
							intent.putExtra("device", zone.device_id);
							startActivity(intent);
							break;
					}
				}

			});

			/*if section or zone been alarmed ib last 10 min & not dropped*/
			long max_time = dbHelper.getAlarmEventMaxTimeForZone(zone);
			boolean been_alarmed = System.currentTimeMillis()/1000 - max_time <= Const.SECTION_ZONE_TO_DROP_ALARM_TIME;


			LinkedList<Event> affects = dbHelper.getAffectsByZoneId(site_id, zone.device_id, zone.section_id, zone.id);
			topFrame.setStates(affects, zone, been_alarmed);

			topFrame.bindView();
		}
	}

	public void getLibraryScripts(Zone zone)
	{
		((Activity)context).runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				materialDialog = new NDialog(context, R.layout.n_dialog_progress);
				materialDialog.setTitle(getString(R.string.DIALOG_WAIT_TITLE));
				materialDialog.show();
			}
		});
		wait = true;

		sendScriptsRequest(zone.device_id);

		timer = new Timer();
		timer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				wait = false;
				context.sendBroadcast(new Intent(D3Service.BROADCAST_LIBRARY_SCRIPTS));
			}
		}, 15000);
	}

	private void sendScriptsRequest(int device_id)
	{
		Device device  = dbHelper.getDeviceById(device_id);
		JSONObject message = new JSONObject();
		try
		{
			message.put("config_version", device.config_version);
			if(!nSendMessageToServer(null, D3Request.LIBRARY_SCRIPTS, message, false)){
				wait = false;
				context.sendBroadcast(new Intent(D3Service.BROADCAST_LIBRARY_SCRIPTS));
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	public void goToScriptSet(){
		if (materialDialog != null && materialDialog.isShowing())
		{
			materialDialog.dismiss();
			if (wait)
			{
				if (null != timer) timer.cancel();
				wait = false;
				if(!dbHelper.getScriptBindedStatusForRelay(zone))
				{
					openScriptsLibrary();
				}else{
					openDeviceScript();
				}
			}else{
				Func.nShowMessage(context, context.getString(R.string.SCRIPT_ERROR_CANT_LOAD_LIB));
			}
		}
	}

	private void openDeviceScript()
	{
		int script_id = dbHelper.getBindedScriptIdForRelay(zone);
		if(-1 != script_id)
		{
			Intent deviceScriptsIntent = new Intent(getBaseContext(), ScriptActivity.class);
			deviceScriptsIntent.putExtra("script_id", script_id);
			deviceScriptsIntent.putExtra("site_id", site_id);
			startActivity(deviceScriptsIntent);
		}else{
			Func.nShowMessage(context, context.getString(R.string.SCRIPT_SET_ERROR));
		}
	}

	private void openScriptsLibrary(){
		Intent intent = new Intent(getBaseContext(), ScriptsLibraryActivity.class);
		intent.putExtra("device_id", zone.device_id);
		intent.putExtra("site_id", site_id);
		intent.putExtra("bind", zone.id);
		startActivity(intent);
	}

	private void setZoneArmStatusAndControl()
	{
		switch (zone.getDetector()){
			case Const.DETECTOR_SIREN:
			case Const.DETECTOR_SZO:
//				if(dbHelper.getScriptBindedStatusForRelay(zone)){
//					topFrame.setScripted(true);
//
//				}else{
//					topFrame.setScripted(false);
//				}
//				topFrame.setControlType(NTopLayout.ControlType.script);
//				break;
			case Const.DETECTOR_LAMP:
			case Const.DETECTOR_BIK:
			case Const.DETECTOR_KEYPAD:
			case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
				break;
			default:
				Section section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
				if(null!=section){
					SType sType = section.getType();
					if(null!=sType){
						switch (sType.id){
							case Const.SECTIONTYPE_GUARD:
								if(section.armed > 0){
									topFrame.setArmed(Const.STATUS_ARMED);
								}else{
									topFrame.setArmed(Const.STATUS_DISARMED);
								}
								break;
							case Const.SECTIONTYPE_TECH_CONTROL:
								if(section.armed > 0){
									topFrame.setNoControl(false);
								}else{
									topFrame.setNoControl(true);
								}
								break;
							case Const.SECTIONTYPE_FIRE:
							case Const.SECTIONTYPE_FIRE_DOUBLE:
								/*только для пожарных датчиков в пожарном разделе открываем кнопку сброса*/
								if(zone.getDetector() == Const.DETECTOR_FIRE){
									topFrame.setControlType(NTopLayout.ControlType.reset_fire);
								}
								break;
							case Const.SECTIONTYPE_ALARM:
							case Const.SECTIONTYPE_TECH:
								/*открываем кнопку сброса для всех датчиков в неснимаемых разделах*/
								topFrame.setControlType(NTopLayout.ControlType.reset);
								break;
							default:
								break;
						}
					}else{
						if(section.armed > 0){
							topFrame.setArmed(Const.STATUS_ARMED);
						}else{
							topFrame.setArmed(Const.STATUS_DISARMED);
						}
					}
				}
				break;
		}
	}

	private void addOnMain() {
		boolean shared = PrefUtils.getInstance(context).getSharedMainScreenMode();
		Cursor cursor = dbHelper.getMainBars(site_id, shared);
		if (null == cursor || 0 == cursor.getCount()) {
			dbHelper.fillBars(site_id, shared);
		}
		MainBar mainBar = dbHelper.getEmptyBar(site_id, shared);
		if (null != mainBar) {
			mainBar.type = MainBar.Type.DEVICE;
			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("device", zone.device_id);
				jsonObject.put("section", zone.section_id);
				jsonObject.put("zone", zone.id);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
			mainBar.element_id = jsonObject.toString();
			if (dbHelper.addNewBar(mainBar, shared)) {
				favorited = true;
				topFrame.setFavorited(favorited);
				this.mainBar = getRefreshedMainBar();
			} else {
				Func.pushToast(context, "DB ERROR", (ZoneActivity) context);
			};
		}else{
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
			nDialog.setTitle(getString(R.string.N_ERROR_NO_SPACE_ON_MAIN));
			nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
			{
				@Override
				public void onActionClick(int value, boolean b)
				{
					switch (value){
						case NActionButton.VALUE_CANCEL:
							nDialog.dismiss();
							break;
					}
				}
			});
			nDialog.show();
		}
	}

	private MainBar getRefreshedMainBar()
	{
		return dbHelper.getMainBarForZone(site_id, zone);
	}

	public void  onResume(){
		super.onResume();
		if(!binded)bind();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		registerReceiver(updateReceiver, intentFilter);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		registerReceiver(libraryScriptsReceiver, new IntentFilter(D3Service.BROADCAST_LIBRARY_SCRIPTS));
		bindD3();
	}

	public void onPause(){
		super.onPause();
		binded = false;
		unregisterReceiver(updateReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
		unregisterReceiver(libraryScriptsReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				finish();
			}
		}
	}
}
