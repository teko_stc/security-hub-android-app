package biz.teko.td.SHUB_APP.D3DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Models.Pair;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 08.06.2016.
 */
public class DBHelper extends SQLiteOpenHelper
{

	private static final String DATABASE_NAME_1 = "shub_database_";
	private static final String DATABASE_NAME_3 = "shub_database";
	private static final String DATABASE_NAME_2 = ".db";


	private static final int DATABASE_VERSION = 812;

	private static final long ONE_DAY_LONG = 684000;
	private static final long TIME_CONST = 978307200;
	private static final int AUTO_RELAY_TYPE = 13;
	private static final int SIREN_TYPE = 14;
	private static final int LAMP_TYPE = 15;
	private static final int RELAY_TYPE = 23;
	private static final int THERMOSTAT_TYPE = 27;


	private static DBHelper instance;
	private static SQLiteDatabase dbRead;
	private static SQLiteDatabase dbWrite;
	private static String LOG_TAG = "D3DataBase";
	private Context context;
	private boolean domainLocale;

//	public static DBHelper getInstance(Context context){
//		/**
//		 * use the application context as suggested by CommonsWare.
//		 * this will ensure that you don't accidentally leak an Activities
//		 * context (see this article for more information:
//		 * http://developer.android.com/resources/articles/avoiding-memory-leaks.html)
//		 */
//		if (instance == null) {
//			instance = new DBHelper(context.getApplicationContext());
//			dbRead = instance.getReadableDatabase();
//			dbWrite = instance.getWritableDatabase();
//		}
//		return instance;
//	}

	public static  DBHelper getInstance(Context context){
		/**
		 * use the application context as suggested by CommonsWare.
		 * this will ensure that you don't accidentally leak an Activities
		 * context (see this article for more information:
		 * http://developer.android.com/resources/articles/avoiding-memory-leaks.html)
		 */
		if(instance == null){
			if(PrefUtils.getInstance(context).getImportSettings()){ /*TODO убрать после обновления, когда импорт уже не будет нужен*/
				instance = new DBHelper(context.getApplicationContext());
				Func.log_d(D3Service.LOG_TAG, "OLD DBDBDBDBDBDBDBDB");
			}else{
				instance = new DBHelper(context.getApplicationContext(), PrefUtils.getInstance(context).getCurrentUserId());
				Func.log_d(D3Service.LOG_TAG, "NEW DBDBDBDBDBDBDBDB");
			}

			dbRead = instance.getReadableDatabase();
			dbWrite = instance.getWritableDatabase();
		}
		return instance;
	}

	public static DBHelper updateDBInstance(Context context){
		if(null!=instance){
			UserInfo userInfo = instance.getUserInfo();
			if(null!=userInfo && userInfo.id != PrefUtils.getInstance(context).getCurrentUserId()){
				instance.deleteUserInfo();
				Log.d(D3Service.LOG_TAG, " updateDBInstance - drop current user id and other userinfo from db"  + (null!=userInfo  ? userInfo.userLogin  : "null"));
			}
		}
		instance = new DBHelper(context.getApplicationContext(), PrefUtils.getInstance(context).getCurrentUserId());
		Func.log_d(D3Service.LOG_TAG, "NEW DBDBDBDBDBDBDBDB");
		dbRead = instance.getReadableDatabase();
		dbWrite = instance.getWritableDatabase();
		return instance;
	}

	/*TODO убрать после обновления, когда импорт уже не будет нужен*/
	/*13.05.2021 in 1.0.016 to import old data, use only once*/
	public void importFromDefault(){
		execSQL(dbWrite, "ATTACH DATABASE ? as old_db", new String[]{context.getDatabasePath("shub_database.db").toString()});
		copyData();
		execSQL(dbWrite, "DETACH DATABASE old_db");
	}

	private void copyData()
	{
		execSQL(dbWrite, "INSERT INTO `user_info` SELECT * FROM old_db.`user_info`");
		execSQL(dbWrite, "INSERT INTO `events` SELECT * FROM old_db.`events`");
		execSQL(dbWrite, "INSERT INTO `sites` SELECT * FROM old_db.`sites`");
		execSQL(dbWrite, "INSERT INTO `devices` SELECT * FROM old_db.`devices`");
		execSQL(dbWrite, "INSERT INTO `device_site` SELECT * FROM old_db.`device_site`");
		execSQL(dbWrite, "INSERT INTO `users` SELECT * FROM old_db.`users`");
		execSQL(dbWrite, "INSERT INTO `addresses` SELECT * FROM old_db.`addresses`");
		execSQL(dbWrite, "INSERT INTO  `address_elements` SELECT * FROM old_db.`address_elements`");
		execSQL(dbWrite, "INSERT INTO  `zones` SELECT * FROM old_db.`zones`");
		execSQL(dbWrite, "INSERT INTO  `site_delegates` SELECT * FROM old_db.`site_delegates`");
		execSQL(dbWrite, "INSERT INTO  `operators` SELECT * FROM old_db.`operators`");
		execSQL(dbWrite, "INSERT INTO  `commands` SELECT * FROM old_db.`commands`");
		execSQL(dbWrite, "INSERT INTO  `clusters` SELECT * FROM old_db.`clusters`");
		execSQL(dbWrite, "INSERT INTO  `cameras_iv` SELECT * FROM old_db.`cameras_iv`");
		execSQL(dbWrite, "INSERT INTO  `cameras_rtsp` SELECT * FROM old_db.`cameras_rtsp`");
		execSQL(dbWrite, "INSERT INTO  `locales` SELECT * FROM old_db.`locales`");
		execSQL(dbWrite, "INSERT INTO  `scripts` SELECT * FROM old_db.`scripts`");
		execSQL(dbWrite, "INSERT INTO  `device_scripts` SELECT * FROM old_db.`device_scripts`");
		execSQL(dbWrite, "INSERT INTO  `firmware_scripts` SELECT * FROM old_db.`firmware_scripts`");
		execSQL(dbWrite, "INSERT INTO  `section_groups` SELECT * FROM old_db.`section_groups`");
		execSQL(dbWrite, "INSERT INTO  `section_group_sections` SELECT * FROM old_db.`section_group_sections`");
		execSQL(dbWrite, "INSERT INTO  `main_bars` SELECT * FROM old_db.`main_bars`");
		execSQL(dbWrite, "INSERT INTO  `alarms` SELECT * FROM old_db.`alarms`");
		execSQL(dbWrite, "INSERT INTO  `alarm_events` SELECT * FROM old_db.`alarm_events`");

	}

	public DBHelper(Context context)
	{
		super(context, DATABASE_NAME_3 + DATABASE_NAME_2, null, DATABASE_VERSION);
		this.context = context;
	}

	public DBHelper(Context context, int user_id)
	{
		super(context, getDBName(user_id), null, DATABASE_VERSION);
		this.context = context;
	}

	private static String getDBName(int user_id)
	{
		return DATABASE_NAME_1 + user_id + DATABASE_NAME_2;
	}

	public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
		super(context, name, factory, version, errorHandler);
	}

	private static void logSql(String sql, Exception e) {
		Log.w(LOG_TAG, "DB Exception in query '" + sql + "'\n", e);
	}

	private static void logSql(String sql, String[] args, Exception e) {
		String str;
		try {
			str = String.format(sql.replace("%", "%%").replace("?", "%s"), args);
		}
		catch (Exception e_str) {
			str = "(bad format)";
		}
		logSql(str, e);
	}

	public static Cursor rawQuery(SQLiteDatabase db, String sql, String[] selectionArgs) {
		try {
			return db.rawQuery(sql, selectionArgs);
		}
		catch (Exception e) {
			logSql(sql, selectionArgs, e);
		}
		return null;
	}

	public static Cursor rawQuery(SQLiteDatabase db, String sql) {
		return rawQuery(db, sql, null);
	}


	public static boolean execSQL(SQLiteDatabase db, String sql, String[] bindArgs) {
		try {
			db.execSQL(sql, bindArgs);
			return true;
		}
		catch (Exception e) {
			logSql(sql, bindArgs, e);
		}
		return false;
	}

	public static boolean execSQL(SQLiteDatabase db, String sql) {
		try {
			db.execSQL(sql);
			return true;
		}
		catch (Exception e) {
			logSql(sql, e);
		}
		return false;
	}

	public static String capitalize(String string) {
		return string.substring(0, 1).toUpperCase() + string.substring(1);
	}

	public static long lastInsertedRowId(SQLiteDatabase db) {
		long id = 0;
		Cursor cursor = db.rawQuery("SELECT last_insert_rowid()", null);
		if (cursor.moveToFirst()) {
			id = cursor.getLong(0);
		}
		cursor.close();
		return id;
	}

	@Override
	public void onOpen(SQLiteDatabase db)
	{
		super.onOpen(db);
		db.execSQL("PRAGMA foreign_keys=ON");
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		createUserInfoTable(db);
		createEventsTable(db);
		createSitesTable(db);
		createDevicesTable(db);
		createDeviceSitesTable(db);
		createUsersTable(db);
		createAddressesTable(db);
		createAddressElemetsTable(db);
		createZonesTable(db);
		createSiteDelegatesTable(db);
		createOperatorsTable(db);
		createCommandsTable(db);
		createClustersTable(db);
		createCamerasIVTable(db);
		createCamerasRTSPTable(db);
		createLocalesTable(db);
		createScriptsTable(db);
		createDeviceScriptsTable(db);
		//17.10.2019
		createDeviceFirmwareTable(db);
		createMainBarsTable(db);
		createSectionGroupsTable(db);
		createSectionGroupsSectionsTable(db);
		//20.10.2020
		createAlarmsTable(db);
		createAlarmEventsTable(db);
		createExtraTable(db);
		createCamerasDahuaTable(db);
	}

	private void createExtraTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB devicesExtra");
		execSQL(db, "CREATE TABLE `devices_extra` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`zone` INTEGER NOT NULL UNIQUE, " +
				"`name` TEXT, " +
				"`value` TEXT, " +
				"CONSTRAINT fk_extra_device_section_zone FOREIGN KEY (`zone`) " +
				"REFERENCES `zones` (`_id`) ON DELETE CASCADE)");
	}

	private void createAlarmEventsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB SectionGroupSections");
		execSQL(db, "CREATE TABLE `alarm_events` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`alarm_id` INTEGER NOT NULL," +
				"`event_id` INTEGER NOT NULL, " +
				"UNIQUE (`alarm_id`, `event_id`))"
		);
	}

	private void createAlarmsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB SectionGroupSections");
		execSQL(db, "CREATE TABLE `alarms` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`alarm_id` INTEGER NOT NULL," +
				"`device_id` INTEGER NOT NULL, " +
				"`state` INTEGER NOT NULL DEFAULT 0, " +
				"`type` INTEGER, " +
				"`create_time` LONG NOT NULL DEFAULT 0, " +
				"`managed_user` INTEGER, " +
				"`extra` TEXT, " +
				"`actual` INTEGER NOT NULL DEFAULT 1)"
		);
	}

	private void createSectionGroupsSectionsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB SectionGroupSections");
		execSQL(db, "CREATE TABLE `section_group_sections` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`group_id` INTEGER NOT NULL," +
				"`device_id` INTEGER NOT NULL, " +
				"`section_id` INTEGET NOT NULL ," +
				"CONSTRAINT fk_group_id FOREIGN KEY (`group_id`) " +
				"REFERENCES `section_groups` (`_id`) ON DELETE CASCADE)");
	}

	private void createSectionGroupsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB SectionGroups");
		execSQL(db, "CREATE TABLE `section_groups` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`name` TEXT)");
	}

	private void createDeviceFirmwareTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB FirmwareScripts");
		execSQL(db, "CREATE TABLE `firmware_scripts` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`firmware` INTEGER," +
				"`script_id` INTEGER," +
				" UNIQUE (`firmware`, `script_id`))");
	}

	private void createDeviceScriptsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB DeviceScripts");
		execSQL(db, "CREATE TABLE `device_scripts` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`script_id` INTEGER ," +
				"`script_uid` TEXT, " +
				"`device` INTEGER, " +
				"`bind` INTEGER, " +
				"`name` TEXT, " +
				"`program` TEXT, " +
				"`compile_program` TEXT, " +
				"`params` TEXT, " +
				"`extra` TEXT, " +
				"`enabled` INTEGER," +
				"`actual` INTEGER NOT NULL DEFAULT 1)");
	}

	private void createLocalesTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB Locales");
		execSQL(db, "CREATE TABLE `locales` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`locale_id` INTEGER," +
				"`iso` TEXT," +
				"`description` TEXT)");
	}

	private void createCamerasIVTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB Cameras_IV");
		execSQL(db, "CREATE TABLE `cameras_iv` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`iv_id` TEXT," +
				"`iv_name` TEXT," +
				"`camera_status` INTEGER," +
				"`camera_quality` INTEGER," +
				"`camera_audio` TEXT," +
				"`actual` INTEGER NOT NULL DEFAULT 1)");
	}

	private void createCamerasRTSPTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB Cameras_RTSP");
		execSQL(db, "CREATE TABLE `cameras_rtsp` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`name` TEXT," +
				"`link` TEXT," +
				"`actual` INTEGER NOT NULL DEFAULT 1)");
	}

	private void createCamerasDahuaTable(SQLiteDatabase db){
		Log.d(LOG_TAG, "onCreate DB Cameras_Dahua");
		execSQL(db, "CREATE TABLE `cameras_dahua` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`name` TEXT," +
				"`sn` TEXT," +
				"`login` TEXT," +
				"`pass` TEXT," +
				"`preview` BLOB," +
				"`actual` INTEGER NOT NULL DEFAULT 1)");
	}

	private void createClustersTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB Clusters");
		execSQL(db, "CREATE TABLE `clusters` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`cluster_id` INTEGER," +
				"`cluster_system` INTEGER," +
				"`cluster_cluster` INTEGER," +
				"`cluster_name` TEXT)");
	}

	private void createCommandsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB Commands");
		execSQL(db, "CREATE TABLE `commands` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`command_id` INTEGER," +
				"`command_inner_id` INTEGER," +
				"`command_type` TEXT," +
				"`command_result` INTEGER)");
	}

	private void createOperatorsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB Operators");
		execSQL(db, "CREATE TABLE `operators` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`operator_id` INTEGER NOT NULL UNIQUE, " +
				"`operator_name` TEXT, " +
				"`operator_login` TEXT, " +
				"`operator_contacts` TEXT, " +
				"`operator_active` INTEGER, " +
				"`operator_domain` INTEGER, " +
				"`operator_org` INTEGER, " +
				"`operator_roles` INTEGER," +
				"`operator_actual` INTEGER NOT NULL DEFAULT 1)");
	}

	private void createSiteDelegatesTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB SiteDelegates");
		execSQL(db, "CREATE TABLE `site_delegates` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`site_id` INTEGER," +
				"`domain` INTEGER," +
				"`domain_name` TEXT, " +
				"`actual` INTEGER NOT NULL DEFAULT 1, " +
				"`permissions` INTEGER NOT NULL DEFAULT 0)");
	}

	private void createZonesTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB Zones");
		execSQL(db, "CREATE TABLE `zones` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`zone_id` INTEGER, " +
				"`zone_name` TEXT, " +
				"`zone_section_id` INTEGER, " +
				"`zone_device_id` INTEGER," +
				"`zone_actual` INTEGER, " +
				"`zone_camera` TEXT, " +
				"`zone_physic` INTEGER, " +
				"`zone_detector` INTEGER," +
				"`zone_armed` LONG NOT NULL DEFAULT 0," +
				"`zone_delay` INTEGER NOT NULL DEFAULT 0," +
				"`uid_type` INTEGER NOT NULL DEFAULT 0)");
	}

	private void createAddressElemetsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB AddressEleentsTable");
		execSQL(db, "CREATE TABLE `address_elements` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`site_id` INTEGER," +
				"`name` TEXT," +
				"`code` INTEGER," +
				"`parent_code` INTEGER," +
				"`addr_type` TEXT," +
				"`canonical` TEXT," +
				"`apartment_number` TEXT)");
	}

	private void createAddressesTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB AddressTable");
		execSQL(db, "CREATE TABLE `addresses` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`site_id` INTEGER," +
				"`house_number` TEXT," +
				"`apartment_number` TEXT)");
	}

	private void createUsersTable(SQLiteDatabase db)
	{
		execSQL(db, "CREATE TABLE `users` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`user_id` INTEGER," +
				"`user_device_id` INTEGER," +
				"`user_name` TEXT," +
				"`user_comment` TEXT," +
				"`user_phone` TEXT," +
				"`user_sections` TEXT)");
	}

	private void createDeviceSitesTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB DeviceSiteTable");
		execSQL(db, "CREATE TABLE `device_site` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`device_site_id` INTEGER," +
				"`device_device_id` INTEGER," +
				"`device_section_id` INTEGER," +
				"`device_site_actual` INTEGER NOT NULL DEFAULT 1," +
				"`device_site_parted` INTEGER NOT NULL DEFAULT 0)");
		Log.d(LOG_TAG, "onCreate DB UsersTable");
	}

	private void createDevicesTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB DevicesTable");
		execSQL(db, "CREATE TABLE `devices` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`device_id` INTEGER NOT NULL UNIQUE," +
				"`device_hw_id` INTEGER," +
				"`device_config_version` INTEGER," +
				"`device_actual` INTEGER," +
				"`device_type` INTEGER NOT NULL DEFAULT 0," +
				"`device_cluster` INTEGER NOT NULL DEFAULT 0)");
	}

	private void createSitesTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB SitesTable");
		execSQL(db, "CREATE TABLE `sites` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`site_id` INTEGER," +
				"`site_contract_id` INTEGER NOT NULL DEFAULT 0, " +
				"`site_contract_status` INTEGER NOT NULL DEFAULT 0, " +
				"`site_name` TEXT, " +
				"`site_comment` TEXT, " +
				"`site_extra` TEXT, " +
				"`site_is_crossing` INTEGER, " +
				"`site_domain` INTEGER, " +
				"`site_actual` INTEGER, " +
				"`site_category` INTEGER, " +
				"`site_arm_mode` INTEGER," +
				"`site_delegated` INTEGER," +
				"`site_type` INTEGER," +
				"`site_force_code` INTEGER)");
	}

//	private void createUserRolesTable(SQLiteDatabase db)
//	{
//		Log.d(LOG_TAG, "onCreate DB RolesTable");
//		execSQL(db, "CREATE TABLE `user_roles` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
//				"`user_id` INTEGER, " +
//				"`org` INTEGER, " +
//				"`domain` INTEGER, " +
//				"`role` INTEGER, " +
//				"`role_actual` INTEGER);");
//	}

	private void createUserInfoTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB UserTable");
		execSQL(db, "CREATE TABLE `user_info` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`user_id` INTEGER, " +
				"`user_login` TEXT, " +
				"`user_password` TEXT, " +
				"`roles` INTEGER NOT NULL DEFAULT 0, " +
				"`domain` INTEGER NOT NULL DEFAULT 0, " +
				"`server_time` LONG NOT NULL DEFAULT 0, " +
				"`user_pin` TEXT," +
				"`iv_token` TEXT," +
				"`locale_iso` TEXT);");
	}

	private void createEventsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB EventTable");
		execSQL(db, "CREATE TABLE `events` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				" `event_id` INTEGER NOT NULL UNIQUE," +
				"`event_device` INTEGER," +
				"`event_section` INTEGER, " +
				"`event_zone` INTEGER, " +
				"`event_active` INTEGER, " +
				"`event_class_id` INTEGER, " +
				"`event_detector_id` INTEGER, " +
				"`event_reason_id` INTEGER, " +
				"`event_time` LONG, " +
				"`event_local_time` LONG, " +
				"`event_channel` INTEGER, " +
				"`event_review_status` INTEGER, " +
				"`event_comment` TEXT, " +
				"`event_visiblity` INTEGER NOT NULL DEFAULT 1, " +
				"`event_affect` INTEGER, "+
				"`event_jdata` TEXT," +
				"`affect_visiblity` INTEGER NOT NULL DEFAULT 1, " +
				"`receive_time` LONG, " +
				"`dropped` INTEGER NOT NULL DEFAULT 0);");
	}

	private void createScriptsTable(SQLiteDatabase db)
	{
		Log.d(LOG_TAG, "onCreate DB Scripts");
		execSQL(db, "CREATE TABLE `scripts` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"`script_id` INTEGER NOT NULL, " +
				"`script_uid` TEXT, " +
				"`category` TEXT, " +
				"`name` TEXT, " +
				"`description` TEXT, " +
				"`source` TEXT, " +
				"`params` TEXT, " +
				"`extra` TEXT, " +
				"`firmware` TEXT," +
				"`actual` INTEGER NOT NULL DEFAULT 1," +
				"UNIQUE (`script_id`))");
		Log.d(LOG_TAG, "onCreate DB DeviceScripts");
	}

	private void createMainBarsTable(SQLiteDatabase db)
	{
		//02.03.2020
		Log.d(LOG_TAG, "onCreate DB MainBars");
		execSQL(db, "CREATE TABLE `main_bars` (" +
				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
				"`type` TEXT," +
				"`subtype` TEXT," +
				"`site_id` INTEGER NOT NULL DEFAULT 0," +
				"`element_id` TEXT," +
				"`x` INTEGER, " +
				"`y` INTEGER)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d(LOG_TAG, "onUpdate DB");

		dropCamerasDahuaTable(db);
		createCamerasDahuaTable(db);

		//29.04.2021 add top&bottom temp for section
//		execSQL(db, "ALTER TABLE `zones` ADD COLUMN `bottom_temp`");

		//01.11.2021 temp limits
		createExtraTable(db);


//		//TODO
//		// OPEN 25.01.2020 add receive time 1.0.009
//		execSQL(db, "ALTER TABLE `events` ADD COLUMN `receive_time` LONG");
//		execSQL(db, "ALTER TABLE `events` ADD COLUMN `dropped` INTEGER NOT NULL DEFAULT 0");
//		execSQL(db, "ALTER TABLE `events` RENAME TO `events_tmp`");
//		createEventsTable(db);
//		execSQL(db, "INSERT INTO `events` (`_id`, " +
//				"`event_id`, `event_device`, `event_section`, `event_zone`, `event_active`," +
//				"`event_class_id`, `event_detector_id`, `event_reason_id`, `event_time`, `event_local_time`, " +
//				"`event_channel`, `event_review_status`, `event_comment`, `event_visiblity`, `event_affect`, `event_jdata`, `affect_visiblity`, `receive_time`, `dropped`) " +
//				"SELECT * FROM `events_tmp`");
//		execSQL(db, "DROP TABLE `events_tmp`");
//
//
//		//TODO
//		// OPEN 21.12.2020
//		execSQL(db, "ALTER TABLE `devices` RENAME TO `devices_tmp`");
//		createDevicesTable(db);
//		execSQL(db, "INSERT INTO `devices` SELECT * FROM `devices_tmp`");
//		execSQL(db, "DROP TABLE `devices_tmp`");
//
//		//TODO
//		// OPEN 26.10.2020
//		createAlarmsTable(db);
//		createAlarmEventsTable(db);
//
//		//TODO
//		// OPEN 07.10.2020 rtsp cameras
//		createCamerasRTSPTable(db);
//
//		//TODO
//		// OPEN 06.10.2020 set actual for cameras_iv
//		execSQL(db, "ALTER TABLE `cameras` ADD COLUMN `actual` INTEGER NOT NULL DEFAULT 1");
//		execSQL(db, "ALTER TABLE `cameras` RENAME TO `cameras_tmp`");
//		createCamerasIVTable(db);
//		execSQL(db, "INSERT INTO `cameras_iv` SELECT * FROM `cameras_tmp`");
//		execSQL(db, "DROP TABLE `cameras_tmp`");
//
//		//TODO
//		// OPEN 01.10.2020
//		createDeviceFirmwareTable(db);
//		createMainBarsTable(db);
//		createSectionGroupsTable(db);
//		createSectionGroupsSectionsTable(db);
//
//		//TODO
//		// OPEN 27.09.2020 set default for actual&parted
//		execSQL(db, "ALTER TABLE `device_site` RENAME TO `device_sites_tmp`");
//		createDeviceSitesTable(db);
//		execSQL(db, "INSERT INTO `device_site` SELECT * FROM `device_site_tmp`");
//		execSQL(db, "DROP TABLE `device_site_tmp`");
//
//
//		/*-----*/
//
//		//23.07.2020
////		dropSectionGroups(db);
//		createSectionGroupsTable(db);
////		dropSectionGroupSections(db);
//		createSectionGroupsSectionsTable(db);
//
//		//19.06.2020
////		dropMainBars(db);
//		createMainBarsTable(db);
//
//		//10.06.2020 unique key firmware+script_id
//		execSQL(db, "ALTER TABLE `firmware_scripts` RENAME TO `firmware_scripts_tmp`");
//		createDeviceFirmwareTable(db);
//		execSQL(db, "INSERT INTO `firmware_scripts` SELECT * FROM `firmware_scripts_tmp`");
//		execSQL(db, "DROP TABLE `firmware_scripts_tmp`");
//
//		// TODO open for 04.06.2020 for uvo msk 1.1.001
//		//04.06.2020 for uvo msk 1.1.001
//		execSQL(db, "ALTER TABLE `device_scripts` RENAME TO `device_scripts_tmp`");
//		createDeviceScriptsTable(db);
//		execSQL(db, "INSERT INTO `device_scripts` SELECT * FROM `device_scripts_tmp`");
//		execSQL(db, "DROP TABLE `device_scripts_tmp`");
//
//		//19.05.2020 unique key operator_id
//		execSQL(db, "ALTER TABLE `operators` RENAME TO `operators_tmp`");
//		createOperatorsTable(db);
//		execSQL(db, "DROP TABLE `operators_tmp`");
//
//		//09.04.2020 0.9.074 unique key script_id
//		execSQL(db, "ALTER TABLE `scripts` RENAME TO `scripts_tmp`");
//		createScriptsTable(db);
//		execSQL(db, "INSERT INTO `scripts` SELECT * FROM `scripts_tmp`");
//		execSQL(db, "DROP TABLE `scripts_tmp`");

		// TODO open for 04.06.2020 for uvo msk 1.1.001
		//09.04.2020 0.9.072
//		execSQL(db, "ALTER TABLE `events` ADD COLUMN `affect_visiblity` INTEGER NOT NULL DEFAULT 1");
//		execSQL(db, "ALTER TABLE `events` RENAME TO `events_tmp`");
//		createEventsTable(db);
//		execSQL(db, "INSERT INTO `events` (`_id`, " +
//				"`event_id`, `event_device`, `event_section`, `event_zone`, `event_active`," +
//				"`event_class_id`, `event_detector_id`, `event_reason_id`, `event_time`, `event_local_time`, " +
//				"`event_channel`, `event_review_status`, `event_comment`, `event_visiblity`, `event_affect`, `event_jdata`, `affect_visiblity`) " +
//				"SELECT * FROM `events_tmp`");
//		execSQL(db, "DROP TABLE `events_tmp`");


		// TODO open for 04.06.2020 for uvo msk 1.1.001
		/*23.10.2018*/
//		execSQL(db, "ALTER TABLE `zones` ADD COLUMN `zone_delay` INTEGER NOT NULL DEFAULT 0");

		// TODO open for 04.06.2020 for uvo msk 1.1.001
		//24.10.2019 0.9.040
//		execSQL(db, "ALTER TABLE `zones` ADD COLUMN `uid_type` INTEGER NOT NULL DEFAULT 0");

		//17.10.2019
//		execSQL(db, "DROP TABLE `scripts`");
//		createScriptsTable(db);

		// TODO open for 04.06.2020 for uvo msk 1.1.001
//		COMMENT 16.08.2019
// 		execSQL(db, "ALTER TABLE `user_info` ADD COLUMN `locale_iso` TEXT");


//		COMMENT 16.08.2019
//		execSQL(db, "ALTER TABLE `user_info` RENAME TO `user_info_tmp`");
//		execSQL(db, "CREATE TABLE `user_info` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
//				"`user_id` INTEGER, " +
//				"`user_login` TEXT, " +
//				"`user_password` TEXT, " +
//				"`roles` INTEGER NOT NULL DEFAULT 0, " +
//				"`domain` INTEGER NOT NULL DEFAULT 0, " +
//				"`server_time` LONG NOT NULL DEFAULT 0, " +
//				"`user_pin` TEXT," +
//				"`iv_token` TEXT," +
//				"`locale_iso` TEXT);");
//		execSQL(db, "INSERT INTO `user_info` (`_id`," +
//				"`user_id`," +
//				"`user_login`," +
//				"`user_password`," +
//				"`roles`," +
//				"`domain`," +
//				"`server_time`," +
//				"`user_pin`," +
//				"`iv_token`," +
//				"`locale_iso`) " +
//				"SELECT * FROM `user_info_tmp`");
//		execSQL(db, "DROP TABLE `user_info_tmp`");

//		COMMENT 16.08.2019
//		execSQL(db, "ALTER TABLE `devices` ADD COLUMN `device_cluster` INTEGER NOT NULL DEFAULT 0");

//		----------


//		/*TODO add `locale` into user_info 21.05.2018*/
//		execSQL(db, "ALTER TABLE `user_info` ADD COLUMN `locale_iso` TEXT");

//		/*TODO open in next release to add iv_token ADDED*/
//		execSQL(db, "ALTER TABLE `user_info` ADD COLUMN `iv_token` TEXT");
//	/*TODO next release 2 change column in user_info ADDED*/
//	execSQL(db, "ALTER TABLE `user_info` RENAME TO `user_info_tmp`");
//	execSQL(db, "CREATE TABLE `user_info` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
//			"`user_id` INTEGER, " +
//			"`user_login` TEXT, " +
//			"`user_password` TEXT, " +
//			"`roles` INTEGER, " +
//			"`domain` INTEGER, " +
//			"`server_time` LONG, " +
//			"`user_pin` TEXT," +
//			"`iv_token` TEXT, " +
//			"`locale_iso` TEXT);");
//	execSQL(db, "INSERT INTO `user_info` (`_id`,`user_id`,`user_login`,`user_password`, `roles`, `domain`, `server_time`,`user_pin`, `iv_token`) " +
//			"SELECT * FROM `user_info_tmp`");
//	execSQL(db, "DROP TABLE `user_info_tmp`");
//
////		/*TODO add locales table 31.05.2018 */
////		execSQL(db, "CREATE TABLE `locales` (" +
////				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
////				"`locale_id` INTEGER," +
////				"`iso` TEXT," +
////				"`description` TEXT)");
//
//		/*TODO OPEN add devices table 09.06.2018*/
//		Log.d(LOG_TAG, "onCreate DB DevicesTable");
//		execSQL(db, "CREATE TABLE `devices` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`device_id` INTEGER," +
//				"`device_hw_id` INTEGER," +
//				"`device_config_version` INTEGER," +
//				"`device_actual` INTEGER NOT NULL DEFAULT 1," +
//				"`device_type` INTEGER NOT NULL DEFAULT 0)");
//
//		/*TODO OPEN 27.06.2018*/
//		execSQL(dbWrite, "DROP TABLE `users`");
//		Log.d(LOG_TAG, "onCreate DB UsersTable");
//		execSQL(db, "CREATE TABLE `users` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`user_id` INTEGER," +
//				"`user_device_id` INTEGER," +
//				"`user_name` TEXT," +
//				"`user_comment` TEXT," +
//				"`user_phone` TEXT," +
//				"`user_sections` TEXT)");
//
//
//
//		/*TODO next release 2 change device to zone ADDED*/
//		execSQL(db, "CREATE TABLE `zones` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
//				"`zone_id` INTEGER, " +
//				"`zone_name` TEXT, " +
//				"`zone_section_id` INTEGER, " +
//				"`zone_site_id` INTEGER, " +
//				"`zone_actual` INTEGER, " +
//				"`zone_battery` INTEGER," +
//				"`zone_camera` TEXT)");
//		execSQL(db, "INSERT INTO `zones` (`_id`, `zone_id`, `zone_name`, `zone_section_id`, `zone_site_id`, `zone_actual`, `zone_battery`) SELECT * FROM `devices`");
//		execSQL(db, "DROP TABLE `devices`");
//
//		/*TODO  add in zones type and detector 21.05.2018*/
//		execSQL(db, "ALTER TABLE `zones` ADD COLUMN `zone_type` INTEGER");
//		execSQL(db, "ALTER TABLE `zones` ADD COLUMN `zone_detector` INTEGER");
//
//		/*TODO OPEN `zones` rename zone_type and add zone_detector_id instead of zone_battery 18.06.2018*/
//		execSQL(db, "ALTER TABLE `zones` RENAME TO `zones_tmp`");
//		execSQL(db, "CREATE TABLE `zones` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
//				"`zone_id` INTEGER, " +
//				"`zone_name` TEXT, " +
//				"`zone_section_id` INTEGER, " +
//				"`zone_device_id` INTEGER," +
//				"`zone_site_id` INTEGER, " +
//				"`zone_actual` INTEGER, " +
//				"`zone_camera` TEXT, " +
//				"`zone_physic` INTEGER, " +
//				"`zone_detector` INTEGER)");
//		execSQL(db, "INSERT INTO `zones` (`_id`, `zone_id`, `zone_name`, `zone_section_id`," +
//				"`zone_site_id`, `zone_actual`, `zone_device_id`, `zone_camera`, `zone_physic`, `zone_detector`) SELECT * FROM `zones_tmp`");
//		execSQL(db, "DROP TABLE `zones_tmp`");
//
//		/*TODO  OPEN 25.06.2018 delete `zone_site_id` from `zones`*/
//		execSQL(db, "ALTER TABLE `zones` RENAME TO `zones_tmp`");
//		execSQL(db, "CREATE TABLE `zones` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
//				"`zone_id` INTEGER, " +
//				"`zone_name` TEXT, " +
//				"`zone_section_id` INTEGER, " +
//				"`zone_device_id` INTEGER," +
//				"`zone_actual` INTEGER, " +
//				"`zone_camera` TEXT, " +
//				"`zone_physic` INTEGER, " +
//				"`zone_detector` INTEGER)");
//		execSQL(db, "INSERT INTO `zones` (`_id`, `zone_id`, " +
//				"`zone_section_id`, `zone_device_id`, `zone_actual`, `zone_camera`, `zone_physic`, `zone_detector`) " +
//				"SELECT (`_id`, `zone_id`, `zone_section_id`, `zone_device_id`, `zone_actual`, `zone_camera`, " +
//				"`zone_physic`, `zone_detector`) FROM `zones_tmp`");
//		execSQL(db, "DROP TABLE `zones_tmp`");
//
//		/*TODO 17.07.2018*/
//		execSQL(db, "ALTER TABLE `zones` ADD COLUMN `zone_armed` LONG NOT NULL DEFAULT 0");
//
//
//		/*TODO next release add site_delegated colums in sites ADDED*/
//		execSQL(db, "ALTER TABLE `sites` ADD COLUMN `site_delegated` INTEGER");
//
//		/*TODO OPEN `site` add contract_id, contract_status, type, category, arm_mode 18.06.2018 */
//		execSQL(db, "ALTER TABLE `sites` RENAME TO `sites_tmp`");
//		execSQL(db, "CREATE TABLE `sites` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`site_id` INTEGER," +
//				"`site_contract_id` INTEGER NOT NULL DEFAULT 0, " +
//				"`site_contract_status` INTEGER NOT NULL DEFAULT 0, " +
//				"`site_name` TEXT, " +
//				"`site_comment` TEXT, " +
//				"`site_extra` TEXT, " +
//				"`site_is_crossing` INTEGER, " +
//				"`site_domain` INTEGER, " +
//				"`site_actual` INTEGER, " +
//				"`site_category` INTEGER, " +
//				"`site_arm_mode` INTEGER," +
//				"`site_delegated` INTEGER," +
//				"`site_type` INTEGER," +
//				"`site_force_code` INTEGER)");
//		execSQL(db, "INSERT INTO `sites` (`_id`, `site_id`, `site_contract_id`, `site_contract_status`, `site_name`, " +
//				"`site_comment`, `site_extra`, `site_is_crossing`, `site_domain`, `site_actual`, `site_category`, `site_arm_mode`, `site_delegated`) SELECT * FROM `sites_tmp`");
//		execSQL(db, "DROP TABLE `sites_tmp`");
//
//		execSQL(db, "ALTER TABLE `sites` ADD COLUMN `site_type` INTEGER");
//
//
//		/*TODO next release 2 change event_device to event_zone ADDED*/
//		execSQL(db, "ALTER TABLE `events` RENAME TO `events_tmp`");
//		execSQL(db, "CREATE TABLE `events` (" +
//				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`event_id` INTEGER SECONDARY KEY, " +
//				"`event_site` INTEGER," +
//				"`event_section` INTEGER, " +
//				"`event_zone` INTEGER, " +
//				"`event_active` INTEGER, " +
//				"`event_class_id` INTEGER, " +
//				"`event_detector_id` INTEGER, " +
//				"`event_reason_id` INTEGER, " +
//				"`event_time` LONG, " +
//				"`event_local_time` LONG, " +
//				"`event_channel` INTEGER, " +
//				"`event_review_status` INTEGER, " +
//				"`event_comment` TEXT, " +
//				"`event_visiblity` INTEGER, " +
//				"`event_affect` INTEGER, "+
//				"`event_jdata` TEXT);");
//		execSQL(db, "INSERT INTO `events` (`_id`, `event_id`, `event_site`, " +
//				"`event_section`, `event_zone`, `event_active`, `event_class_id`, `event_detector_id`, `event_reason_id`, " +
//				"`event_time`, `event_local_time`, `event_channel`, `event_review_status`, `event_comment`, `event_visiblity`, `event_affect`, `event_jdata`) SELECT * FROM `events_tmp`");
//		execSQL(db, "DROP TABLE `events_tmp`");
//
// 		/*TODO OPEN `events` add `event_device` column 18.06.2018 */
//		execSQL(db, "ALTER TABLE `events` RENAME TO `events_tmp`");
//		execSQL(db, "CREATE TABLE `events` (" +
//				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`event_id` INTEGER SECONDARY KEY, " +
//				"`event_site` INTEGER," +
//				"`event_device` INTEGER," +
//				"`event_section` INTEGER, " +
//				"`event_zone` INTEGER, " +
//				"`event_active` INTEGER, " +
//				"`event_class_id` INTEGER, " +
//				"`event_detector_id` INTEGER, " +
//				"`event_reason_id` INTEGER, " +
//				"`event_time` LONG, " +
//				"`event_local_time` LONG, " +
//				"`event_channel` INTEGER, " +
//				"`event_review_status` INTEGER, " +
//				"`event_comment` TEXT, " +
//				"`event_visiblity` INTEGER, " +
//				"`event_affect` INTEGER, "+
//				"`event_jdata` TEXT)");
//		execSQL(db, "INSERT INTO `events` (`_id`, `event_id`, `event_site`, `event_section`, `event_zone`, `event_active`,`event_class_id`, `event_detector_id`, " +
//				"`event_reason_id`, `event_time`, `event_local_time`, `event_channel`, `event_review_status`, `event_comment`, `event_visiblity`," +
//				"`event_affect`, `event_jdata`) SELECT * FROM `events_tmp`");
//		execSQL(db, "DROP TABLE `events_tmp`");
//
//
//		/*TODO OPEN DELETE event_site 22.06.2018*/
//		execSQL(db, "ALTER TABLE `events` RENAME TO `events_tmp`");
//		execSQL(db, "CREATE TABLE `events` (" +
//				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`event_id` INTEGER SECONDARY KEY, " +
//				"`event_device` INTEGER," +
//				"`event_section` INTEGER, " +
//				"`event_zone` INTEGER, " +
//				"`event_active` INTEGER, " +
//				"`event_class_id` INTEGER, " +
//				"`event_detector_id` INTEGER, " +
//				"`event_reason_id` INTEGER, " +
//				"`event_time` LONG, " +
//				"`event_local_time` LONG, " +
//				"`event_channel` INTEGER, " +
//				"`event_review_status` INTEGER, " +
//				"`event_comment` TEXT, " +
//				"`event_visiblity` INTEGER, " +
//				"`event_affect` INTEGER, "+
//				"`event_jdata` TEXT)");
//		execSQL(db, "INSERT INTO `events` " +
//				"(`_id`, " +
//				"`event_id`, " +
//				"`event_device`," +
//				" `event_section`, " +
//				" `event_zone`, " +
//				"`event_active`," +
//				"`event_class_id`, " +
//				"`event_detector_id`, " +
//				"`event_reason_id`, " +
//				" `event_time`, " +
//				" `event_local_time`, " +
//				" `event_channel`, " +
//				"`event_review_status`, `event_comment`, `event_visiblity`, " +
//				"`event_affect`, " +
//				"`event_jdata`) " +
//				"SELECT (`_id`, " +
//				"`event_id`, `event_device`, " +
//				" `event_section`, `event_zone`, " +
//				" `event_active`, `event_class_id`, " +
//				" `event_detector_id`, `event_reason_id`, " +
//				"`event_time`, `event_local_time`, " +
//				"`event_channel`, `event_review_status`," +
//				" `event_comment`, `event_visiblity`," +
//				"`event_affect`, " +
//				"`event_jdata`) FROM `events_tmp`");
//		execSQL(db, "DROP TABLE `events_tmp`");
//
//
//		/*TODO next release 2 create site_delegates table ADDED*/
//		execSQL(db, "CREATE TABLE `site_delegates` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`site_id` INTEGER," +
//				"`domain` INTEGER," +
//				"`domain_name` TEXT)");
//		/*TODO 13.07.2018 */
//		execSQL(db, "ALTER TABLE `site_delegates` ADD COLUMN `actual` INTEGER NOT NULL DEFAULT 1");
//
//		/*TODO 17.07.2018*/
//		execSQL(db, "ALTER TABLE `site_delegates` ADD COLUMN `permissions` INTEGER NOT NULL DEFAULT 0");
//
//
//		/*TODO OPEN 22.06.2018 add device_site table to compare sites and devices*/
//		execSQL(db, "DROP TABLE `device_site`");
//		execSQL(db, "CREATE TABLE `device_site` (`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`device_site_id` INTEGER," +
//				"`device_device_id` INTEGER," +
//				"`device_section_id` INTEGER," +
//				"`device_site_actual` INTEGER," +
//				"`device_site_parted` INTEGER)");
//
//		/*TODO OPEN 22.06.2018 delete affects table*/
//		execSQL(db, "DROP TABLE `affects`");


//		/* TODO next release 2 add new columns in cameras ADDED*/
//		Log.d(LOG_TAG, "onCreate DB Cameras");
//		execSQL(db, "CREATE TABLE `cameras_iv` (" +
//				"`_id` INTEGER PRIMARY KEY AUTOINCREMENT," +
//				"`iv_id` TEXT," +
//				"`iv_name` TEXT," +
//				"`camera_status` INTEGER," +
//				"`camera_quality` INTEGER," +
//				"`camera_audio` TEXT)");

//		execSQL(db, "ALTER TABLE `cameras_iv` ADD COLUMN `camera_quality` INTEGER");
//		execSQL(db, "ALTER TABLE `cameras_iv` ADD COLUMN `camera_audio` TEXT");
//		Log.d(LOG_TAG, "onUpdate DB CamerasTable");


//		Log.d(LOG_TAG, "onUpdate DB RolesTable");
//		execSQL(db, "DROP TABLE `user_roles`");
//		Log.d(LOG_TAG, "onUpdate DB EventTable");
//		execSQL(db, "ALTER TABLE `events` ADD COLUMN `event_jdata` TEXT");
//		execSQL(db, "DROP TABLE `events`");
//		Log.d(LOG_TAG, "onUpdate DB BarsTable");
//		execSQL(db, "DROP TABLE `bar_params`");
//		Log.d(LOG_TAG, "onUpdate DB SitesTable");
//		execSQL(db, "ALTER TABLE `sites` ADD COLUMN `site_force_code` INTEGER");
//		execSQL(db, "DROP TABLE `sites`");
//		Log.d(LOG_TAG, "onUpdate DB AddressesTable");
//		execSQL(db, "DROP TABLE `addresses`");
//		Log.d(LOG_TAG, "onUpdate DB AddressElementsTable");
//		execSQL(db, "DROP TABLE `address_elements`");
//		Log.d(LOG_TAG, "on Update DB Affect");
//		execSQL(db, "ALTER TABLE `affects` ADD COLUMN `event_jdata` TEXT");
//		Log.d(LOG_TAG, "on Update DB Operators");
//		execSQL(db, "DROP TABLE `operators`");
//		Log.d(LOG_TAG, "on Update DB Commands");
//		execSQL(db, "DROP TABLE `commands`");
//		Log.d(LOG_TAG, "on Update DB Clusters");
//		execSQL(db, "DROP TABLE `clusters`");

//		execSQL(db, "DROP TABLE `user_info`");
//		execSQL(db, "DROP TABLE `events`");
//		execSQL(db, "DROP TABLE `sites`");
//		execSQL(db, "DROP TABLE `devices`");
//		execSQL(db, "DROP TABLE `addresses`");
//		execSQL(db, "DROP TABLE `address_elements`");
//		execSQL(db, "DROP TABLE `cameras_iv`");
//		execSQL(db, "DROP TABLE `clusters`");
//		execSQL(db, "DROP TABLE `commands`");
//		execSQL(db, "DROP TABLE `device_site`");
//		execSQL(db, "DROP TABLE `locales`");
//		execSQL(db, "DROP TABLE `operators`");
//		execSQL(db, "DROP TABLE `site_delegates`");
//		execSQL(db, "DROP TABLE `user_roles`");
//		execSQL(db, "DROP TABLE `users`");
//		execSQL(db, "DROP TABLE `zones`");
//
//		onCreate(db);
	}

	private void dropCamerasDahuaTable(SQLiteDatabase db)
	{
		execSQL(db, "DROP TABLE `cameras_dahua`");
	}

	private void dropSectionGroupSections(SQLiteDatabase db){
		execSQL(db, "DROP TABLE `section_group_sections`");
	}

	private void dropSectionGroups(SQLiteDatabase db){
		execSQL(db, "DROP TABLE `section_groups`");
	}

	private void dropMainBars(SQLiteDatabase db)
	{
		execSQL(db,"DROP TABLE `main_bars`");
	}

	/*
		USER INFO
	 */
	public void deleteUserInfo()
	{
		execSQL(dbWrite, "DELETE FROM `user_info`");
	}

	public long addUserInfo(UserInfo userInfo) {
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `user_info`");
		if (null!=cursor){
			if(0 == cursor.getCount())
			{
				execSQL(dbWrite, "INSERT INTO `user_info` (`user_id`, `user_login`, `user_password`, `roles`, `domain`, `server_time`,`user_pin`) " +
						"VALUES (?, ?, ?, ?, ?, ?, ?)",
						new String[]{Integer.toString(userInfo.id), userInfo.userLogin, userInfo.userPassword, Integer.toString(userInfo.roles), Integer.toString(userInfo.domain), Long.toString(userInfo.serverTime), userInfo.pinCode});
			}else{
				cursor.moveToFirst();
				int id = cursor.getInt(0);
				execSQL(dbWrite,"UPDATE `user_info` " +
						"SET " +
						"`user_id`=" + Integer.toString(userInfo.id) +
						", `user_login`='" + String.valueOf(userInfo.userLogin) + "'" +
						", `user_password`='" + String.valueOf(userInfo.userPassword) + "'" +
						", `roles`=" + Integer.toString(userInfo.roles) +
						", `user_pin`='" + null!=userInfo.pinCode? String.valueOf(userInfo.pinCode)  : null + "'" +
						" WHERE `_id`=" + Integer.toString(id));
			}
			cursor.close();
			return lastInsertedRowId(dbWrite);
		}
		return 0;
	}

	public UserInfo getUserInfo()
	{
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `user_info`");
		if(null!=cursor){
			if(cursor.getCount()>0){
				cursor.moveToFirst();
				UserInfo u = new UserInfo(cursor.getInt(1),
						cursor.getString(2), cursor.getString(3),
						cursor.getInt(4), cursor.getInt(5),
						cursor.getLong(6), cursor.getString(7),
						cursor.getString(8), cursor.getString(9));
				cursor.close();
//				dropTempUserInfo();
				return u;
			}
			cursor.close();
//			cursor = rawQuery(dbRead, "SELECT * FROM `user_info_tmp`");
//			if(null!=cursor){
//				if(cursor.getCount()>0){
//					cursor.moveToFirst();
//					UserInfo u = new UserInfo(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7), cursor.getString(8), cursor.getString(9));
//					cursor.close();
//					return u;
//				}
//				cursor.close();
//			}
		}
		return null;
	}

	public UserInfo getTempUserInfo(){
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `user_info_tmp`");
		if(null!=cursor){
			if(cursor.getCount()>0){
				cursor.moveToFirst();
				UserInfo u = new UserInfo(cursor.getInt(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4), cursor.getInt(5), cursor.getLong(6), cursor.getString(7), cursor.getString(8), cursor.getString(9));
				cursor.close();
				return u;
			}
			cursor.close();
		}
		return null;
	}

	public void dropTempUserInfo(){
		if(null != rawQuery(dbRead, "SELECT * FROM `user_info_tmp`"))
			execSQL(dbWrite, "DELETE FROM `user_info_tmp`");
	}

	public void setUserInfoPIN(String pin){
		execSQL(dbWrite, "UPDATE `user_info` SET `user_pin`='" + String.valueOf(pin) + "'");

	}

	public long setUserParams(String login, String pass, int id, int roles, int domain, String locale){
		addUserInfo(new UserInfo(id, login, pass, System.currentTimeMillis()/1000));
		boolean r = execSQL(dbWrite, "UPDATE `user_info` SET `user_id`=" + Integer.toString(id) + ", `roles`=" + Integer.toString(roles) + ", `domain`=" + Integer.toString(domain) + ", `locale_iso`='" + String.valueOf(locale) + "'");
		return lastInsertedRowId(dbWrite);
	}

	public long setUserParams(int id, int roles, int domain, String locale){
		boolean r = execSQL(dbWrite, "UPDATE `user_info` SET `user_id`=" + Integer.toString(id) + ", `roles`=" + Integer.toString(roles) + ", `domain`=" + Integer.toString(domain) + ", `locale_iso`='" + String.valueOf(locale) + "'");
		return lastInsertedRowId(dbWrite);
	}

	public void setUserInfoSafeSession(String hex){

	}

	public String getUserInfoSafeSession(){
		return "";
	}


	/*
	* IV TOKEN
	* */

	public void resetIVAllData(){
		delUserInfoIVToken();
		deleteAllCameraBindings();
		deleteAllCameras();
	}

	public void resetIVCameras(){
		deleteAllCameraBindings();
		deleteAllCameras();
	}

	public void setUserInfoIVToken(String userInfoIVToken)
	{
		execSQL(dbWrite, "UPDATE `user_info` SET `iv_token`='" + String.valueOf(userInfoIVToken) + "'");
	}

	public void delUserInfoIVToken()
	{
		execSQL(dbWrite, "UPDATE `user_info` SET `iv_token`='" + String.valueOf("") + "'");
	}

	public String getUserInfoIVToken(){
		Cursor c =  rawQuery(dbRead, "SELECT * FROM `user_info`");
		if(c!=null){
			if(c.getCount() != 0){
				c.moveToFirst();
				return c.getString(8);
			}
		}
		return null;
	}
	/*
		USER ROLES
	*/


	public void addNewRole(int user_id, int roles){
		execSQL(dbWrite, "UPDATE `user_info` SET `roles`=" + Integer.toString(roles) + " WHERE `user_id`=" + Integer.toString(user_id));
	}


	/*
	* DEVICES
	*
	* */

	public long addDevice(Device device){
		Cursor cursor = getDeviceCursorById(device.id);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.close();
				updateDeviceInfoAndActual(device, 1);
				addZonesForDevice(device);
				addUsersForDevice(device);
				return lastInsertedRowId(dbWrite);
			}
		}
		addNewDevice(device);
		addZonesForDevice(device);
		addUsersForDevice(device);
		return lastInsertedRowId(dbWrite);
	}
	/*25.06.2018*/
	public long addDevices(Device[] devices)
	{
		Cursor cursor = getAllDevicesCursor();
		if(null!=cursor){
			if(0!=cursor.getCount()){
//				for (Device device:devices){
//					updateDeviceInfoAndActual(device, 0);
//					updateZonesActualForDevice(device, 0);
//				}
//				deleteDevicesWithEventsByActual(1);
//				deleteZonesWithActual(1);
//				updateAllDevicesActual(1);
//				updateAllZonesActual(1);
				for(Device device:devices){
					Cursor c = getDeviceCursorById(device.id);
					if(null!=c){
						if(0 == c.getCount()){
							addNewDevice(device);
						}else{
							updateDeviceInfoAndActual(device,1);
						}
						addZonesForDevice(device);
						addUsersForDevice(device);
						c.close();
					}
				}
				cursor.close();
				return lastInsertedRowId(dbWrite);
			}
			cursor.close();
		}
		for (Device device : devices)
		{
			addNewDevice(device);
			addZonesForDevice(device);
			addUsersForDevice(device);
		}
		return lastInsertedRowId(dbWrite);
	}

	private void addUsersForDevice(Device device)
	{
		deleteDeviceUsers(device.id);
		addNewUsers(device.users);
	}

	public void  deleteDeviceUsers(int device_id){
		execSQL(dbWrite, "DELETE FROM `users` WHERE `user_device_id`=" + Integer.toString(device_id));
	}

	public void deleteDeviceUser(int device_id, int index){
		execSQL(dbWrite, "DELETE FROM `users` WHERE `user_device_id`=" + Integer.toString(device_id) +
		" AND `user_id`=" + Integer.toString(index));
	}

	private  void addNewUsers(User[] users){
		for(User user:users){
			addNewUser(user);
		}
	}

	public void addUser(User user){
		Cursor cursor = getCurrentUserCursorForDevice(user.id, user.device);
		if(null!=cursor){
			if(0 != cursor.getCount()){
				updateUser(user);
			}else{
				addNewUser(user);
			}
			cursor.close();
		}
	}

	public void  updateUser(User user){
		execSQL(dbWrite, "UPDATE `users` SET `user_name`='" + String.valueOf(user.name) + "', " +
				"`user_comment`='" + String.valueOf(user.comment) + "', " +
				"`user_phone`='" + String.valueOf(user.phone) + "', " +
				"`user_sections`='" + String.valueOf(user.sections) + "' " +
				"WHERE `user_id`=" + Integer.toString(user.id) + " AND `user_device_id`=" + Integer.toString(user.device));
	}

	public User getDeviceUserById(int user, int device){
		Cursor c = getCurrentUserCursorForDevice(user, device);
		if(null!=c){
			if(c.getCount() > 0){
				c.moveToFirst();
				return new User(c);
			}
			c.close();
		}
		return  null;
	}

	public Cursor getCurrentUserCursorForDevice(int user_id, int device_id){
		return rawQuery(dbRead, "SELECT * FROM `users` WHERE `user_id`=" + Integer.toString(user_id) + " AND `user_device_id`=" + Integer.toString(device_id));
	}

	public void addNewUser(User user){
		execSQL(dbWrite, "INSERT INTO `users` (`user_id`, `user_device_id`, `user_name`, `user_comment`, `user_phone`, `user_sections`) VALUES (?, ?, ?, ?, ?, ?)",
				new String[]{
						Integer.toString(user.id),
						Integer.toString(user.device),
						user.name,
						user.comment,
						user.phone,
						user.sections
				});
	}

	public Cursor getUsersCursorForDevice(int device_id)
	{
		return rawQuery(dbRead, "SELECT * FROM `users` WHERE `user_device_id`=" + Integer.toString(device_id));
	}

	public Cursor getUsersCursorForSite(int site_id)
	{
		return rawQuery(dbRead, "SELECT * FROM `users` " +
				"INNER JOIN " +
				"(SELECT DISTINCT `device_device_id` FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id) +") " +
				"ON `device_device_id` = `users`.`user_device_id` ORDER BY `user_id`");
	}

	public String getUserNameById(int device_id, int user_id)
	{
		Cursor c = rawQuery(dbRead, "SELECT `user_name` FROM `users` WHERE `user_device_id`=" + Integer.toString(device_id) +
				" AND `user_id`=" + Integer.toString(user_id));
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				String name = c.getString(0);
				c.close();
				return name;
			}
		}
		return context.getString(R.string.UNKNOWN_USER_CODE);
	}

	private void addNewDevice(Device device)
	{
		execSQL(dbWrite, "INSERT INTO `devices` (`device_id`, `device_hw_id`, `device_config_version`, `device_type`, `device_cluster`)" +
				" VALUES (?, ?, ?, ?, ?)", new String[]{
				Integer.toString(device.id),
				Integer.toString(device.account),
				Integer.toString(device.config_version),
				Integer.toString(Func.getDeviceType(device.config_version, device.cluster)),
				Integer.toString(device.cluster)
		});
	}

	public Device[] getAllDevices(){
		Cursor c = getAllDevicesCursor();
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				Device[] devices = new Device[c.getCount()];
				int i = 0;
				devices[0] = new Device(c);
				while(c.moveToNext()){
					i++;
					devices[i] = new Device(c);
					devices[i].site = getSiteNameByDeviceSection(devices[i].id, 0);
				}
				c.close();
				return devices;
			}
			c.close();
		}
		return null;
	}

	public Device[] getAllSHDevices(){
		Cursor c = getAllSHDevicesCursor();
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				Device[] devices = new Device[c.getCount()];
				int i = 0;
				devices[0] = new Device(c);
				while(c.moveToNext()){
					i++;
					devices[i] = new Device(c);
//					devices[i].site = getSiteNameByDeviceSection(devices[i].id, 0);
//					devices[i].enabled = isDeviceOnline(devices[i].id);
//					devices[i].armed = getDeviceArmStatus(devices[i].id);
				}
				c.close();
				return devices;
			}
			c.close();
		}
		return null;
	}

	public Cursor getAllSHDevicesCursor()
	{
		return rawQuery(dbRead, "SELECT DISTINCT `devices`.*, `device_site`.`device_site_id` FROM `devices`" +
				" LEFT JOIN `device_site` ON `devices`.`device_id` = `device_site`.`device_device_id`" +
				" WHERE (`device_config_version`>>8)&0xff = 1 " +
				" AND `device_site`.`device_site_id` NOT NULL " +
				" ORDER BY `device_site`.`device_site_id`");
	}

	public Device[] getAllSHDevicesForSite(int site_id){
		Cursor c = getAllSHDevicesCursorForSite(site_id);
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				Device[] devices = new Device[c.getCount()];
				int i = 0;
				devices[0] = new Device(c);
				while(c.moveToNext()){
					i++;
					devices[i] = new Device(c);
					devices[i].site = getSiteNameByDeviceSection(devices[i].id, 0);
					devices[i].enabled = isDeviceOnline(devices[i].id);
					devices[i].armed = getDeviceArmStatus(devices[i].id);
				}
				c.close();
				return devices;
			}
			c.close();
		}
		return null;
	}

	public Cursor getAllSHDevicesCursorForSite(int site_id)
	{
		return rawQuery(dbRead, "SELECT DISTINCT `devices`.`_id`, `devices`.`device_id`, `device_hw_id`, `device_config_version`, `device_actual`, `device_type`" +
				" FROM `devices`" +
				" INNER JOIN  `device_site` " +
				" ON `devices`.`device_id`=`device_site`.`device_device_id` " +
				"WHERE ((`devices`.`device_config_version`>>8)&0xff = 1) AND `device_site`.`device_site_id`=" + Integer.toString(site_id));
	}

	public Cursor getAllDevicesCursor()
	{
		return rawQuery(dbRead, "SELECT * FROM `devices`");
	}

	public int getDevicesCount(){
		Cursor c = rawQuery(dbRead, "SELECT COUNT(*) FROM `devices` INNER JOIN `device_site` ON `devices`.`device_id`=`device_site`.`device_device_id`");
		if(null!=c){
			if(c.getCount() > 0)
			{
				c.moveToFirst();
				int count = c.getInt(0);
				c.close();
				return count;
			}
			c.close();
		}
		return 0;
	}

	public Cursor getDeviceCursorById(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `devices` WHERE `device_id`=" + Integer.toString(id));
	}

	public Cursor getNAllElementsCursorSiteOrder(int site_id)
	{
		return  rawQuery(dbRead, "SELECT DISTINCT `zones`.*, `devices`.`device_hw_id`, `devices`.`device_config_version`, `device_site_id` FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`,`device_site_id` FROM `device_site`) " +
				"ON `zones`.`zone_device_id`=`device_device_id` " +
				" LEFT JOIN `devices` ON `device_id`=`zone_device_id`" +
				"WHERE ((`zone_section_id`> 0 AND `zone_id`!=0) OR (`zone_section_id`=0 AND `zone_id`!=100))" +
				" AND NOT (((`devices`.`device_config_version`>>8)&0xff = 1) AND `zone_id`>100)" +
				" ORDER BY CASE " +
				"WHEN `device_site_id`=" + Integer.toString(site_id) + " THEN -1 ELSE `device_site_id` END," +
				"`zone_section_id`," +
				"CASE WHEN `zone_id`=" + Integer.toString(100) + " THEN -1 ELSE `zone_id` END"
		);
	}

	public Cursor getNAllElementsCursorSiteOrderFilteredWithAllControllers(int site_id, String filter) {
		return rawQuery(dbRead, "SELECT DISTINCT `zones`.*, `devices`.`device_hw_id`, `devices`.`device_config_version`, `device_site_id` FROM `zones` " +
						"INNER JOIN  " +
						"(SELECT DISTINCT `device_device_id`,`device_site_id` FROM `device_site`) " +
						"ON `zones`.`zone_device_id`=`device_device_id` " +
						" LEFT JOIN `devices` ON `device_id`=`zone_device_id`" +
						"WHERE ((`zone_section_id`> 0 AND `zone_id`!=0) OR (`zone_section_id`=0 AND `zone_id`!=100))" +
						" AND ((`zone_section_id`= 0 AND (`zone_id`= 0 OR `zone_id`= 100))" +
						" OR  (`zone_name` LIKE ? " +
						" OR `device_site_id` IN ( SELECT `site_id` FROM `sites`  WHERE `site_name` LIKE ?)))" +
						" AND NOT (((`devices`.`device_config_version`>>8)&0xff = 1) AND `zone_id`>100)" +
						" ORDER BY CASE " +
						"WHEN `device_site_id`=" + Integer.toString(site_id) + " THEN -1 ELSE `device_site_id` END," +
						"`zone_section_id`," +
						"CASE WHEN `zone_id`=" + Integer.toString(100) + " THEN -1 ELSE `zone_id` END",
				new String[]{"%" + filter + "%", "%" + filter + "%"}
		);
	}

	public Cursor getNAllDevicesCursorBySiteId(int site_id) {
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `zones` " +
				" INNER JOIN  " +
				" (SELECT DISTINCT `device_device_id`,`device_site_id` FROM `device_site`) " +
				" ON `zones`.`zone_device_id`=`device_device_id` " +
				" WHERE `zone_id` NOT LIKE " + Integer.toString(0) +
				" ORDER BY CASE " +
				" WHEN `device_site_id`=" + Integer.toString(site_id) + " THEN -1 ELSE `device_site_id` END," +
				" `zone_section_id`," +
				" CASE WHEN `zone_id`=" + Integer.toString(100) + " THEN -1 ELSE `device_site_id` END"
		);
	}

	public Cursor getNAllSectionsCursorBySiteId(int site_id)
	{
		return  rawQuery(dbRead, "SELECT DISTINCT * FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`,`device_site_id` FROM `device_site`) " +
				"ON `zones`.`zone_device_id`=`device_device_id` " +
				"WHERE `zone_id`=" + Integer.toString(0) +
				" AND `zone_section_id`!=" + Integer.toString(0) +
				" ORDER BY CASE " +
				"WHEN `device_site_id`=" + Integer.toString(site_id) + " THEN -1 ELSE `device_site_id` END," +
				"`zone_section_id`"
		);
	}

	public Cursor getNAllSectionsCursorBySiteIdFiltered(int site_id, String filter) {
		String sql = "SELECT DISTINCT * FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`,`device_site_id` FROM `device_site`) " +
				"ON `zones`.`zone_device_id`=`device_device_id` " +
				"WHERE (`zone_name` LIKE ?" +
				"OR `device_site_id` IN ( SELECT `site_id` FROM `sites`  WHERE `site_name` LIKE ?))" +
				" AND `zone_id`=" + Integer.toString(0) +
				" AND `zone_section_id`!=" + Integer.toString(0) +
				" ORDER BY CASE " +
				"WHEN `device_site_id`=" + Integer.toString(site_id) + " THEN -1 ELSE `device_site_id` END," +
				"`zone_section_id`";
		return rawQuery(dbRead, sql, new String[]{"%" + filter + "%", "%" + filter + "%"});
	}

	public Cursor getNAllDevicesCursor() {
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`,`device_site_id` FROM `device_site`) " +
				"ON `zones`.`zone_device_id`=`device_device_id` " +
				"WHERE `zone_id` NOT LIKE " + Integer.toString(0) +
				" ORDER BY `device_site_id`, `zone_section_id`, `zone_id`"
		);
	}

	public Cursor getNDevicesCursorBySiteId(int siteId)
	{
		return  rawQuery(dbRead, "SELECT DISTINCT * FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id` FROM `device_site` WHERE `device_site_id`=" + Integer.toString(siteId) + ") " +
				"ON `zones`.`zone_device_id`=`device_device_id` " +
				"WHERE `zone_id` NOT LIKE " + Integer.toString(0) +
				" ORDER BY `zone_section_id`, `zone_id`"
		);
	}

	public Cursor getDevicesCursorBySiteId(int siteId)
	{
		return  rawQuery(dbRead, "SELECT DISTINCT * FROM `devices` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id` FROM `device_site` WHERE `device_site_id`=" + Integer.toString(siteId) + ") " +
				"ON `devices`.`device_id`=`device_device_id`");
	}

	public Device getDeviceById(int device)
	{
		Cursor c = getDeviceCursorById(device);
		if(null != c){
			if(0 != c.getCount()){
				c.moveToFirst();
				return new Device(c.getInt(1), c.getInt(2), c.getInt(3), c.getInt(5), c.getInt(6));
			}
			c.close();
		}
		return null;
	}

//	public void deleteDeviceById(int device_id){
//		deleteEventsForDevice(device_id);
//		execSQL(dbWrite, "DELETE FROM `devices` WHERE `device_id`=" + Integer.toString(device_id));
//	}

	private void deleteDevicesWithEventsByActual(int actual)
	{
		Cursor c = rawQuery(dbRead,"SELECT `device_id` FROM `devices` WHERE `device_actual`=" + Integer.toString(actual));
		if(null != c ){
			c.moveToFirst();
			if(c.getCount() > 0){
				deleteEventsForDevice(c.getInt(0));
				while(c.moveToNext()){
					deleteEventsForDevice(c.getInt(0));
				}
			}
			c.close();
		}
		execSQL(dbWrite, "DELETE FROM `devices` WHERE `device_actual`=" + Integer.toString(actual));
	}

	private void updateDeviceInfoAndActual(Device device, int i)
	{
		execSQL(dbWrite, "UPDATE `devices` SET `device_hw_id`=" + Integer.toString(device.account) +
		", `device_config_version`=" + Integer.toString(device.config_version) +
		", `device_type`=" + Integer.toString(Func.getDeviceType(device.config_version, device.cluster)) +
		", `device_cluster`=" + Integer.toString(device.cluster) +
		", `device_actual`=" + Integer.toString(i) +
		" WHERE `device_id`=" + Integer.toString(device.id));
	}

	public void updateAllDevicesActual(int actual){
		execSQL(dbWrite, "UPDATE `devices` SET `device_actual`=" + Integer.toString(actual));
	}

	public int getDeviceTypeById(int device_id){
		Cursor cursor = rawQuery(dbRead, "SELECT `device_type` FROM `devices` WHERE `device_id`=" + Integer.toString(device_id) );
		int type = -1;
		if(null!=cursor){
			if(0 != cursor.getCount()){
				cursor.moveToFirst();
				type = cursor.getInt(0);
			}
			cursor.close();
		}
		return type;
	}


	/*
		SITES
	*/

	/*TODO 19.06.2018 add or update one site with sections and zones*/
	public void addSite(Site site){
		Cursor c = getSiteCursorById(site.id);
		if(null!=c)
		{
			if (0 == c.getCount())
			{
				addNewSite(site);
			} else
			{
				updateSiteInfoAndActual(site, 1);
			}
			if (site.devices.length > 0)
			{
				for (Device device : site.devices)
				{
					for (Section section : device.sections)
					{
						addSiteDeviceSection(site.id, device.id, section.id, device.parted);
					}
					;
				}
			} else
			{
				deleteDeviceSiteForSite(site.id);
			}
			addAddressForSite(site);
			c.close();
		}
	}

	/*TODO 19.06.2018 add or update array of sites with sections and zones*/
	public long addSites(Site[] sites){
		Cursor cursor = getAllSitesCursor();
		if(null!=cursor){
			if(0!=cursor.getCount())
			{
				updateAllSiteActual(0);
				updateAllDeviceSiteActual(0);
				for(Site site:sites){
					Cursor c = getSiteCursorById(site.id);
					if(null!=c)
					{
						if (0 == c.getCount())
						{
							addNewSite(site);
						} else
						{
							updateSiteInfoAndActual(site, 1);
						}
						for (Device device : site.devices)
						{
							for (Section section : device.sections)
							{
								addSiteDeviceSection(site.id, device.id, section.id, device.parted);
							}
							;
						}
						addAddressForSite(site);
						c.close();
					}
				}
				deleteSitesWithActual(0);
				deleteDeviceSiteWithActual(0);

				cursor.close();
				return lastInsertedRowId(dbWrite);
			}
		}
		/*add new site
		* add new device_site
		 */
		for(Site site:sites){
			addNewSite(site);
			for(Device device:site.devices){
				for(Section section:device.sections)
				{
					addSiteDeviceSection(site.id, device.id, section.id, device.parted);
				};
			}
			addAddressForSite(site);
		}

		return 0;
	}

	/*25.06.2018*/
	public long addNewSite(Site site){
		if(execSQL(dbWrite, "INSERT INTO `sites` (`site_id`," +
				"`site_contract_id`," +
				"`site_contract_status`," +
				"`site_name`," +
				"`site_comment`," +
				"`site_extra`," +
				"`site_is_crossing`," +
				"`site_actual`," +
				"`site_category`," +
				"`site_domain`," +
				"`site_delegated`," +
				"`site_type`," +
				"`site_arm_mode`)" +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				new String[]{
					Integer.toString(site.id),
					Integer.toString(site.contract_id),
					Integer.toString(site.contract_status),
					site.name,
					site.comment,
					site.extra,
					Integer.toString(site.is_crossing),
					Integer.toString(1),
					Integer.toString(site.category),
					Integer.toString(site.domain),
					Integer.toString(site.delegated),
					Integer.toString(site.type),
					Integer.toString(site.arm_mode)
				})){
			return  lastInsertedRowId(dbWrite);
		}
		return 0;
	}

	public void updateSiteInfoAndActual(Site site, int actual){
		execSQL(dbWrite, "UPDATE `sites` SET `site_name`='" + String.valueOf(site.name) +
				"', `site_contract_id`=" + Integer.toString(site.contract_id) +
				", `site_contract_status`=" + Integer.toString(site.contract_status) +
				", `site_comment`=" + String.valueOf(DatabaseUtils.sqlEscapeString(site.comment)) +
				", `site_extra`=" + String.valueOf(DatabaseUtils.sqlEscapeString(site.extra)) +
				", `site_is_crossing`=" + Integer.toString(site.is_crossing) +
				", `site_actual`=" + Integer.toString(actual) +
				", `site_domain`=" + Integer.toString(site.domain) +
				", `site_delegated`=" + Integer.toString(site.delegated) +
				", `site_category`=" + Integer.toString(site.category) +
				", `site_arm_mode`=" + Integer.toString(site.arm_mode) +
				" WHERE `site_id`=" + Integer.toString(site.id));
	}

//	/*TODO Временно
//	* Надо исправить пакет SITE_UPD
//	* Добавить в него
//	* и унифицировать с getSite()
//	* */
//	public void updateSiteInfoAndActualForSiteUpdate(Site site, int actual){
//		execSQL(dbWrite, "UPDATE `sites` SET `site_name`='" + String.valueOf(site.name) +
//				"', `site_config_id`=" + Integer.toString(site.configId) +
//				", `site_config_version`=" + Integer.toString(site.configVersion) +
//				", `site_comment`='" + String.valueOf(site.comment) +
//				"', `site_extra`='" + String.valueOf(site.extra) +
//				"', `site_is_crossing`=" + Integer.toString(site.is_crossing) +
//				", `site_actual`=" + Integer.toString(actual) +
//				", `site_secured_id`=" + Integer.toString(site.secured_site_id) +
//				" WHERE `site_id`=" + Integer.toString(site.id));
//	}

	/*25.06.2018*/
	public void updateAllSiteActual(int actual){
		execSQL(dbWrite, "UPDATE `sites` SET `site_actual`=" + Integer.toString(actual));
	}

	/*25.06.2018*/
	public void updateAllDeviceSiteActual(int actual){
		execSQL(dbWrite, "UPDATE `device_site` SET `device_site_actual`=" + Integer.toString(actual));
	}

	/*25.06.2018*/
	public void updateSiteDeviceSectionActual(int site_id, int device_id, int section_id, int actual, int parted){
		execSQL(dbWrite, "UPDATE `device_site` SET `device_site_actual`=" + Integer.toString(actual) +
		" WHERE (`device_site_id`=" + Integer.toString(site_id) +
		") AND (`device_device_id`=" + Integer.toString(device_id) +
		") AND (`device_section_id`=" + Integer.toString(section_id) +
		") AND (`device_site_parted`=" + Integer.toString(parted) + ")");
	}


	/*25.06.2018*/
	public void deleteSitesWithActual(int actual){
		execSQL(dbWrite, "DELETE FROM `sites` WHERE `site_actual`=" + Integer.toString(actual));
	}

	/*25.06.2018*/
	public void deleteDeviceSiteWithActual(int actual){
		execSQL(dbWrite, "DELETE FROM `device_site` WHERE `device_site_actual`=" + Integer.toString(actual));
	}

	public  void deleteDeviceSiteForSite(int site_id){
		execSQL(dbWrite, "DELETE FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id));

	}

	/*25.06.2018*/
	public Cursor getAllSitesCursor()
	{
		return rawQuery(dbRead, "SELECT * FROM `sites` ORDER BY `site_id`");
	}


	/*25.06.2018*/
	public Site[] getAllSitesArray()
	{
		Cursor c = getAllSitesCursor();
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				Site[] sites = new Site[c.getCount()];
				int i = 0;
				sites[i] = getSiteWithoutSections(c);
				while(c.moveToNext()){
					i++;
					sites[i]=getSiteWithoutSections(c);
				}
				c.close();
				return sites;
			}
		}
		return null;
	}

	/*25.06.2018*/
	public Site getSiteWithoutSections(Cursor cursor)
	{
		if(null!= cursor){
			return new Site(cursor);
		}
		return null;
	}

	/*25.06.2018*/
	public String getSiteNameById(int site_id)
	{
		Cursor cursor = rawQuery(dbRead, "SELECT `site_name` FROM `sites` WHERE `site_id`=" + Integer.toString(site_id));
		//		Cursor c = getSiteCursorById(site_id);
		//		c.moveToFirst();
		//		String s = c.getString(4);
		String s = context.getString(R.string.SITE_NULL);
		if(null!=cursor)
		{
			if(0!=cursor.getCount())
			{
				cursor.moveToFirst();
				s = cursor.getString(0);
			}
			cursor.close();
		}
		return s;
	}

	/*20.07.2018 NEW*/
	public String getSiteNameByDeviceSection(int device_id, int section_id){
		Cursor cursor = rawQuery(dbRead, "SELECT `site_name` FROM `sites` INNER JOIN (SELECT DISTINCT `device_site_id` FROM `device_site` " +
				"WHERE `device_device_id`="+ Integer.toString(device_id)+ " AND `device_section_id`=" + Integer.toString(section_id) + ") " +
				"ON `device_site_id`=`sites`.`site_id`");
		if(null!= cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				String name = cursor.getString(0);
				while (cursor.moveToNext()){
					name += ", " + cursor.getString(0);
				}
				cursor.close();
				return name;
			}
		}
		return context.getString(R.string.SITE_NULL);
	}

	/*25.06.2018 OLD*/
//	public String getSiteNameByDeviceSectionId(int device_id, int section_id)
//	{
//		Cursor cursor = rawQuery(dbRead, "SELECT `site_name` FROM `sites` INNER JOIN  (SELECT DISTINCT `device_site_id` FROM `device_site` " +
//				"WHERE `device_device_id`=" + Integer.toString(device_id) + " AND `device_section_id`=" + Integer.toString(section_id) + ")" +
//				" ON `sites`.`site_id`=`device_site_id`");
//		String s = context.getString(R.string.SITE_NULL);
//		if(null!=cursor)
//		{
//			if(0!=cursor.getCount())
//			{
//				cursor.moveToFirst();
//				s = cursor.getString(0);
//			}
//			cursor.close();
//		}
//		return s;
//	}

	/*25.06.2018*/
	public int getSitesCount()
	{
		Cursor c = rawQuery(dbRead, "SELECT COUNT(`_id`) FROM `sites`");
		if(null!=c){
			if(0!=c.getCount())
			{
				c.moveToFirst();
				int count = c.getInt(0);
				c.close();
				return count;
			}
			return 0;
		}
		return 0;
	}

	/*25.06.2018*/
	public void deleteAllSitesAndStructure()
	{
		execSQL(dbWrite, "DELETE FROM `sites`");
		execSQL(dbWrite, "DELETE FROM `devices`");
		execSQL(dbWrite, "DELETE FROM `devices_extra`");
		execSQL(dbWrite, "DELETE FROM `zones`");
		execSQL(dbWrite, "DELETE FROM `addresses`");
		execSQL(dbWrite, "DELETE FROM `address_elements`");
		execSQL(dbWrite, "DELETE FROM `events`");
		execSQL(dbWrite, "DELETE FROM `device_site`");
		execSQL(dbWrite, "DELETE FROM `site_delegates`");
		execSQL(dbWrite, "DELETE FROM `scripts`");
		execSQL(dbWrite, "DELETE FROM `device_scripts`");
		execSQL(dbWrite, "DELETE FROM `firmware_scripts`");
		execSQL(dbWrite, "DELETE FROM `main_bars`");
	}

	/*25.06.2018*/
	public void deleteSiteWithStructure(int site_id)
	{
		execSQL(dbWrite, "DELETE FROM `sites` WHERE `site_id`=" + Integer.toString(site_id));
		execSQL(dbWrite, "DELETE FROM `addresses` WHERE `site_id`=" + Integer.toString(site_id));
		execSQL(dbWrite, "DELETE FROM `address_elements` WHERE `site_id`=" + Integer.toString(site_id));
		execSQL(dbWrite, "DELETE FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id));
		execSQL(dbWrite, "DELETE FROM `site_delegates` WHERE `site_id`=" + Integer.toString(site_id));
		execSQL(dbWrite, "DELETE FROM `main_bars` WHERE `site_id`=" + Integer.toString(site_id));

		/*TODO !!!!!*/
//		execSQL(dbWrite, "DELETE FROM `zones` WHERE `zone_site_id`=" + Integer.toString(site_id));
//		execSQL(dbWrite, "DELETE FROM `affects` WHERE `site_id`=" + Integer.toString(site_id));
	}

	public int[] getSiteIds()
	{
		Cursor cursor = rawQuery(dbRead, "SELECT `site_id` FROM `sites`");
		if(null!=cursor){
			if(0!=cursor.getCount()){
				int[] ids =new int[cursor.getCount()];
				cursor.moveToFirst();
				int i = 0;
				ids[i] = cursor.getInt(0);
				i++;
				while(cursor.moveToNext()){
					ids[i] = cursor.getInt(0);
					i++;
				}
				cursor.close();
				return ids;
			}
			return null;
		}
		return null;
	}

	/*25.06.2018*/
	public Cursor getSiteCursorById(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `sites` WHERE site_id =" + Integer.toString(id));
	}

	/*25.06.2018*/
	public Site getSiteById(int id){
		Cursor cursor = getSiteCursorById(id);
		if(null != cursor){
			if(0!= cursor.getCount()){
				cursor.moveToFirst();
				Site site = getSiteWithoutSections(cursor);
				cursor.close();
				return site;
			}
		}
		return null;
	}

	public void updateSiteForceCode(int site_id, String forceCode){
		execSQL(dbWrite, "UPDATE `sites` SET `site_force_code`='" + String.valueOf(forceCode)
				+ "' WHERE `site_id`=" + Integer.toString(site_id)
		);
	}

	/*25.06.2018*/
	public int getSiteIdForForceCode(String forceCode){
		Cursor c = rawQuery(dbRead, "SELECT `site_id` FROM `sites` WHERE `site_force_code`='" + String.valueOf(forceCode) + "'");
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				int id = c.getInt(0);
				c.close();
				return id;
			}
		}
		return 0;
	}

	public Site getSiteByDeviceSection(int device_id, int section_id) {
		Cursor cursor = getSitesCursorForDeviceSection(device_id, section_id);
		if (null != cursor) {
			int count = cursor.getCount();
			if (0 != count) {
				cursor.moveToFirst();
				Site site = new Site(cursor);
				cursor.close();
				return site;
			}
		}
		return null;
	}

	public Site getSiteByDeviceId(int device_id) {
		Cursor cursor = getSitesCursorForDevice(device_id);
		if (null != cursor) {
			int count = cursor.getCount();
			if (0 != count) {
				cursor.moveToFirst();
				Site site = new Site(cursor);
				cursor.close();
				return site;
			}
		}
		return null;
	}


	/*25.06.2018*/
	public Site[] getSitesByDeviceSection(int device_id, int section_id) {
		Cursor cursor = getSitesCursorForDeviceSection(device_id, section_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0 != count){
				cursor.moveToFirst();
				Site[] sites = new Site[count];
				for(int i =0 ; i < count; i++){
					sites[i] = new Site(cursor);
					cursor.moveToNext();
				}
				cursor.close();
				return sites;
			}
		}
		return null;
	}

	/*25.06.2018*/
	private Cursor getSitesCursorForDeviceSection(int device_id, int section_id) {
		return rawQuery(dbRead, "SELECT * FROM `sites` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_site_id` FROM `device_site` " +
				"WHERE `device_device_id`=" + Integer.toString(device_id) + " AND `device_section_id`=" + Integer.toString(section_id) + ") " +
				"" +
				"ON `sites`.`site_id`=`device_site_id`");
	}

	private Cursor getSitesCursorForDevice(int device_id) {
		return rawQuery(dbRead, "SELECT * FROM `sites` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_site_id` FROM `device_site` " +
				"WHERE `device_device_id`=" + Integer.toString(device_id) + ") " +
				"" +
				"ON `sites`.`site_id`=`device_site_id`");
	}

	/*
		ADDRESSES
	 */

	public long addAddressForSite(Site site) {
		Cursor c = getAddressCursorForSite(site);
		if (null != c) {
			deleteAddressForSite(site);
			c.close();
		}
		addNewAddressForSite(site);
		return  0;
	}

	public  Cursor getAddressCursorForSite(Site site){
		return rawQuery(dbRead, "SELECT * FROM `addresses` WHERE `site_id`=" + Integer.toString(site.id) );
	}

	public void deleteAddressForSite(Site site){
		execSQL(dbWrite, "DELETE FROM `addresses` WHERE `site_id`=" + Integer.toString(site.id));
	}

	public long addNewAddressForSite(Site site){
		Address address = site.address;
		if(null!=address)
		{
			if (execSQL(dbWrite, "INSERT INTO `addresses` (`site_id`, `house_number`, `apartment_number`) VALUES (?, ?, ?)", new String[]{Integer.toString(site.id), String.valueOf(address.house_number), String.valueOf(address.apartment_number)}))
			{
				Cursor c = getAddressElementsCursorForSite(site);
				if (null != c)
				{
					deleteAddressElementsForSite(site);
					c.close();
				}
				addNewAddressElementsForSite(site.id, address);
				return lastInsertedRowId(dbWrite);
			}
		}
		return 0;
	}


	public long addNewAddressElementsForSite(int site_id, Address address){
		AddressElement[] addressElements = address.addressElements;
		for(AddressElement addressElement:addressElements)
		{
			if (execSQL(dbWrite, "INSERT INTO `address_elements` (`site_id`, `name`, `code`, `parent_code`, `addr_type`, `canonical`) VALUES (?, ?, ?, ?, ?, ?)",
					new String[]{Integer.toString(site_id),
							String.valueOf(addressElement.name),
							Integer.toString(addressElement.code),
							Integer.toString(addressElement.parent_code),
							String.valueOf(addressElement.addr_type),
							String.valueOf(addressElement.canonical)
					}))
			{
				return lastInsertedRowId(dbWrite);
			}
		}
		return 0;
	}

	private Cursor getAddressElementsCursorForSite(Site site)
	{
		return rawQuery(dbRead, "SELECT * FROM `address_elements` WHERE `site_id`=" + Integer.toString(site.id));
	}

	public void deleteAddressElementsForSite(Site site){
		execSQL(dbWrite, "DELETE FROM `address_elements` WHERE `site_id`=" + Integer.toString(site.id));
	}


	/*
		ADD NEW SECTIONS AND ZONES
	*/

	/*25.06.2018*/
	public long addZonesForDevice(Device device){
		Cursor cursor = getAllZonesCursorForDevice(device.id);
		if(null!=cursor)
		{
			if(0!=cursor.getCount()){
				updateAllZonesActual(0);
				updateAllZonesActualForDevice(device.id, 1);
				for(Section section:device.sections){
					Cursor curs = getSectionCursorById(device.id, section.id);
					if(null!=curs){
						if(0 == curs.getCount()){
							addNewSectionForDevice(section);
						}else{
							updateSectionAndActualForDevice(section, 0);
						};
						addExtra((D3Element) section);
						curs.close();
					}
					for(Zone zone:section.zones){
						Cursor c = getZoneCursorById(device.id, zone.section_id, zone.id);
						if(null!=c)
						{
							if (0 == c.getCount())
							{
								addNewZoneForDevice(zone);
							} else
							{
								updateZoneAndActualForDevice(zone, 0);
							}
							c.close();
						}
					}
				}
				deleteZonesWithActual(1);
				updateAllZonesActual(1);
				cursor.close();
				return lastInsertedRowId(dbWrite);
			}
		}
		for(Section section:device.sections){
			addNewSectionForDevice(section);
			for(Zone zone:section.zones)
			{
				addNewZoneForDevice(zone);
			}
		}
		updateAllZonesActual(1);
		return 0;
	}

	/*25.06.2018*/
	public void updateAllZonesActual(int actual){
		execSQL(dbWrite, "UPDATE `zones` SET `zone_actual`="  + Integer.toString(actual));
	}

	/*25.06.2018*/
	public void updateAllZonesActualForDevice(int device_id, int actual){
		execSQL(dbWrite, "UPDATE `zones` SET `zone_actual`="  + Integer.toString(actual) +
		" WHERE `zone_device_id`=" + Integer.toString(device_id));
	}

	/*25.06.2018*/
	public void updateZoneAndActualForDevice(Zone zone, int actual){
		execSQL(dbWrite, "UPDATE `zones` SET `zone_actual`= " + Integer.toString(actual) +", `zone_name`='" + String.valueOf(zone.name) + "', " +
				"`zone_detector`=" + Integer.toString(zone.detector) + ", " +
				"`zone_physic`=" + Integer.toString(zone.physic) + "," +
				"`uid_type`=" + Integer.toString(zone.uid_type) + "," +
				"`zone_delay`=" + Integer.toString(zone.delay)+ " WHERE " +
				"((`zone_device_id`="+ Integer.toString(zone.device_id) +
				")&(`zone_section_id`="+ Integer.toString(zone.section_id) +
				")&(`zone_id`="+ Integer.toString(zone.id) +"))");
	}

	/*25.06.2018*/
	public void updateSectionAndActualForDevice(Section section, int actual){
		execSQL(dbWrite, "UPDATE `zones` SET `zone_actual`= " + Integer.toString(actual) +", `zone_name`='" + String.valueOf(section.name) + "', " +
				"`zone_detector`=" + Integer.toString(section.detector) + "," +
				"`zone_armed`=" + Long.toString(section.armed) + " WHERE " +
				"((`zone_device_id`="+ Integer.toString(section.device_id) +
				")&(`zone_section_id`="+ Integer.toString(section.id) +
				")&(`zone_id`="+ Integer.toString(0) +"))");
	}

	/*25.06.2018*/
	public void deleteZonesWithActual(int actual){
		execSQL(dbWrite,"DELETE FROM `zones` WHERE `zone_actual`="+ Integer.toString(actual));
	}

	/*25.06.2018*/
	public long addNewZoneForDevice(Zone zone){
		if (execSQL(dbWrite, "INSERT INTO `zones` " +
						"(`zone_id`, " +
						"`zone_name`, " +
						"`zone_section_id`," +
						"`zone_actual`, " +
						"`zone_device_id`, " +
						"`zone_physic`," +
						"`zone_detector`," +
						"`zone_delay`," +
						"`uid_type`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
				new String[]{Integer.toString(zone.id),
						zone.name,
						Integer.toString(zone.section_id),
						Integer.toString(0),
						Integer.toString(zone.device_id),
						Integer.toString(zone.physic),
						Integer.toString(zone.detector),
						Integer.toString(zone.delay),
						Integer.toString(zone.uid_type)
				}))
		{
			return lastInsertedRowId(dbWrite);
		}
		return 0;
	}

	public long addNewSectionForDevice(Section section){
		if (execSQL(dbWrite, "INSERT INTO `zones` " +
						"(`zone_id`, " +
						"`zone_name`, " +
						"`zone_section_id`," +
						"`zone_actual`, " +
						"`zone_device_id`, " +
						"`zone_physic`," +
						"`zone_detector`, " +
						"`zone_armed`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
				new String[]{Integer.toString(0),
						section.name,
						Integer.toString(section.id),
						Integer.toString(0),
						Integer.toString(section.device_id),
						Integer.toString(-1),
						Integer.toString(section.detector),
						Long.toString(section.armed)
				}))
		{
			return lastInsertedRowId(dbWrite);
		}
		return 0;
	}


	/*25.06.2018*/
	public void updateZonesActualForDevice(Device device, int actual){
		execSQL(dbWrite, "UPDATE `zones` SET `zone_actual`= " + Integer.toString(actual) + " WHERE " +
				"`zone_device_id`=" + Integer.toString(device.id));
	}

	public void deleteZonesWithActualForDevice(Device device, int actual){
		execSQL(dbWrite, "DELETE FROM `zones` WHERE `zone_actual`= " + Integer.toString(actual) + " AND " +
				"`zone_device_id`=" + Integer.toString(device.id));
	}

	/*25.06.2018*/
	public long addZone(Zone zone){
		Cursor c = getZoneCursorById(zone.device_id, zone.section_id, zone.id);
		if(null!=c){
			if(0!=c.getCount()){
				updateZone(zone);
			}else{
				addNewZoneForDevice(zone);
			}
			c.close();
		}
		return 0;
	};

//	public long addSection(Zone zone){
//		/*set -1 cause its section*/
//		zone.physic = -1;
//		Cursor c = getZoneCursorById(zone.device_id, zone.section_id, zone.id);
//		if(null!=c){
//			if(0!=c.getCount()){
//				updateZone(zone);
//			}else{
//				addNewZoneForDevice(zone);
//			}
//		}
//		return 0;
//	}

	/*25.06.2018*/
	public long updateZone(Zone zone){
		if (execSQL(dbWrite, "UPDATE `zones` SET " +
				"`zone_name`='" + zone.name + "', " +
				"`zone_detector`=" + Integer.toString(zone.detector) +
				" , `zone_physic`=" + Integer.toString(zone.physic) +
				" , `zone_delay`=" + Integer.toString(zone.delay) +
				" , `uid_type`=" + Integer.toString(zone.uid_type) +
				" WHERE (`zone_id`=" + Integer.toString(zone.id) + ")" +
				"&(`zone_section_id`=" + Integer.toString(zone.section_id) +")" +
				"&(`zone_device_id`=" + Integer.toString(zone.device_id) + ")"))
		{
			return lastInsertedRowId(dbWrite);
		}
		return 0;
	};

	/*25.06.2018*/
	public String getSectionNameByIdWithOptions(int device_id, int sectionId){
		Cursor c  = rawQuery(dbRead, "SELECT `zone_name` FROM `zones` " +
				"WHERE (`zone_section_id`=" +  Integer.toString(sectionId) + ")" +
				"&(`zone_device_id`=" + Integer.toString(device_id) +")" +
				"&(`zone_id`=" + Integer.toString(0) +") ");
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				String s = c.getString(0);
				c.close();
				if(s.equals(""))
					return  context.getString(R.string.NO_NAME);
				return s;
			}
		}
		return  context.getString(R.string.UNKNOWN_SECTION);
	}

	/*28.10.2020*/
	public String getSectionNameById(int device_id, int sectionId){
		Cursor c  = rawQuery(dbRead, "SELECT `zone_name` FROM `zones` " +
				"WHERE (`zone_section_id`=" +  Integer.toString(sectionId) + ")" +
				"&(`zone_device_id`=" + Integer.toString(device_id) +")" +
				"&(`zone_id`=" + Integer.toString(0) +") ");
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				String s = c.getString(0);
				c.close();
				return s;
			}
		}
		return  null;
	}

	//29.09.2020
	public String nGetSectionNameById(int device_id, int sectionId){
		Cursor c  = rawQuery(dbRead, "SELECT `zone_name` FROM `zones` " +
				"WHERE (`zone_section_id`=" +  Integer.toString(sectionId) + ")" +
				"&(`zone_device_id`=" + Integer.toString(device_id) +")" +
				"&(`zone_id`=" + Integer.toString(0) +") ");
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				String s = c.getString(0);
				c.close();
				if(s.equals(""))
					return  null;
				return s;
			}
		}
		return null;
	}

	/*25.06.2018*/
	public Cursor getSectionsCursorForSiteWithoutDevice(int site_id){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_id`=" + Integer.toString(0) +
				" AND `zone_section_id` > " + Integer.toString(0) +
				" ORDER BY `zone_section_id` ");
	}

	public Cursor getSectionsCursorForSite(int site_id){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_id`=" + Integer.toString(0) +
				" ORDER BY `zone_section_id` ");
	}

	public Cursor getSectionsCursorForSiteWithoutController(int site_id) {
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_id`=" + Integer.toString(0) +
				" AND `zone_section_id`>0" +
				" ORDER BY `zone_section_id` ");
	}

	public Cursor getSectionsCursorWithoutController() {
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site`) " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_id`=" + Integer.toString(0) +
				" AND `zone_section_id`>0" +
				" ORDER BY `zone_device_id`, `zone_section_id` ");
	}

	public Cursor getGuardSectionsCursorForSiteWithoutController(int site_id) {
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_id`=" + Integer.toString(0) +
				" AND `zone_section_id`>0" +
				" AND `zone_detector`=1" +
				" ORDER BY `zone_section_id` ");
	}

	public Cursor getGuardSectionsCursorWithoutController() {
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site`) " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_id`=" + Integer.toString(0) +
				" AND `zone_section_id`>0" +
				" AND `zone_detector`=1" +
				" ORDER BY `zone_device_id`, `zone_section_id`");
	}

	public Section[] getSectionsForSite(int site_id) {
		Cursor cursor = getSectionsCursorForSiteWithoutController(site_id);
		if (null != cursor) {
			int count = cursor.getCount();
			if (0 != count) {
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getSections() {
		Cursor cursor = getSectionsCursorWithoutController();
		if (null != cursor) {
			int count = cursor.getCount();
			if (0 != count) {
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()) {
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getGuardSectionsForSite(int site_id) {
		Cursor cursor = getGuardSectionsCursorForSiteWithoutController(site_id);
		if (null != cursor) {
			int count = cursor.getCount();
			if (0 != count) {
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getGuardSections() {
		Cursor cursor = getGuardSectionsCursorWithoutController();
		if (null != cursor) {
			int count = cursor.getCount();
			if (0 != count) {
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()) {
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}


	public Cursor getNotBindedZonesForSite(int site_id) {
		String query = "SELECT * FROM `zones` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_id`>" + Integer.toString(0) +
				" AND `zone_section_id`>" + Integer.toString(0) +
				" AND (`zone_camera` IS NULL OR `zone_camera` LIKE '" + String.valueOf("") + "')";
		return rawQuery(dbRead, query);
	}

	public Cursor getAllRelaysCursor() {
		String query = "SELECT DISTINCT `zones`.*, `device_site`.`device_site_id`, `devices`.`device_config_version` FROM `zones` " +
				"LEFT  JOIN `device_site` ON `device_site`.`device_device_id` = `zones`.`zone_device_id` " +
				"LEFT JOIN `devices` ON `devices`.`device_id` = `zones`.`zone_device_id` " +
				"WHERE`zone_id`>0 " +
				" AND NOT (`zone_id` > 99 AND ((`device_config_version`>>8)&0xff=1))";

		query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
				+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
				+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
//						+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
//						+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
				+ "))";
		query += " ORDER BY `zone_device_id`, `zone_section_id`, `zone_id` ";
		return rawQuery(dbRead, query);
	}

	public Cursor getAllZonesCursorForSite(int site_id, int zoneOrRelay) {
		String query = "SELECT DISTINCT `zones`.*, `device_site`.`device_site_id`, `devices`.`device_config_version` FROM `zones` " +
				"LEFT  JOIN `device_site` ON `device_site`.`device_device_id` = `zones`.`zone_device_id` " +
				"LEFT JOIN `devices` ON `devices`.`device_id` = `zones`.`zone_device_id` " +
				"WHERE `device_site`.`device_site_id`= " + Integer.toString(site_id) +
				" AND `zone_id`>0 " +
				" AND NOT (`zone_id` > 99 AND ((`device_config_version`>>8)&0xff=1))";
		switch (zoneOrRelay) {
			case 0:
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
//						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
//						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ")";
				break;
			case 1:
				query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
//						+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
//						+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
						+ "))";
				break;
			case 2:
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ") AND (`zone_section_id`!=" + Integer.toString(0)
						+ ")";
				break;
			default:
				break;
		}
		query += " ORDER BY `zone_device_id`, `zone_section_id`, `zone_id` ";
		return rawQuery(dbRead, query);
	}

	public Cursor getAllZonesCursor(int zoneOrRelay) {
		String query = "SELECT DISTINCT `zones`.*, `device_site`.`device_site_id`, `devices`.`device_config_version` FROM `zones` " +
				"LEFT  JOIN `device_site` ON `device_site`.`device_device_id` = `zones`.`zone_device_id` " +
				"LEFT JOIN `devices` ON `devices`.`device_id` = `zones`.`zone_device_id` " +
				"WHERE`zone_id`>0 " +
				" AND NOT (`zone_id` > 99 AND ((`device_config_version`>>8)&0xff=1))";
		switch (zoneOrRelay) {
			case 0: //zone
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
//						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
//						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ")";
				break;
			case 1: //relay
				query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
//						+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
//						+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
						+ "))";
				break;
			case 2: // detector (zone + siren + lamp)
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ") AND (`zone_section_id`!=" + Integer.toString(0)
						+ ")";
				break;
			default:
				break;
		}
//		query += " ORDER BY `zone_section_id` "; /*'zone_id`, `zone_device_id`";*/
		query += " ORDER BY `zone_device_id`, `zone_section_id`, `zone_id` ";
		return rawQuery(dbRead, query);
	}

	public Cursor getDevicesCountForSite(int site_id) {
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" ORDER BY `zone_section_id` ");
	}

	/*25.06.2018*/
	public Cursor getAllZonesCursorForDevice(int device_id) {
		return rawQuery(dbRead, "SELECT * FROM `zones` WHERE " +
				"`zone_device_id`=" + Integer.toString(device_id) +

				" ORDER BY `zone_section_id` ");
	}

	public Cursor getAllSectionsCursorForDeviceWithoutControllerByType(int device_id, int type){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"WHERE `zone_section_id`>" + Integer.toString(0) +
				" AND `zone_device_id`=" + Integer.toString(device_id) +
				(type != 0?(" AND `zone_detector`=" + Integer.toString(1)):"")+
				" AND `zone_id`=" + Integer.toString(0) +
				" ORDER BY `zone_section_id` ");
	}

	/*TODO закрыто до тех пор, пока не появится деление устройства на несколько объектов
	* использовался в getAllSectionsForSiteDeviceWithoutControllerByType для получения списка разделов доступных для скрипта*/
	public Cursor getAllSectionsCursorForSiteDeviceWithoutControllerByType(int site_id, int device_id, int type){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_section_id`>" + Integer.toString(0) +
				" AND `zone_device_id`=" + Integer.toString(device_id) +
				(type != 0?(" AND `zone_detector`=" + Integer.toString(1)):"")+
				" AND `zone_id`=" + Integer.toString(0) +
				" ORDER BY `zone_section_id` ");
	}

	/*27.09.2020*/
	public Cursor getAllGuardSectionsCursorForDeviceWithoutController(int device_id){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				" WHERE `zone_section_id`>0" +
				" AND `zone_device_id`=" + Integer.toString(device_id) +
				" AND ((`zone_detector`=1) || (`zone_detector`=0))" +
				" AND `zone_id`=0" +
				" ORDER BY `zone_section_id` ");
	}

	/*25.06.2018*/
	public Cursor getAllGuardSectionsCursorForSiteDeviceWithoutController(int site_id, int device_id){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				" WHERE `zone_section_id`>0" +
				" AND `zone_device_id`=" + Integer.toString(device_id) +
				" AND ((`zone_detector`=1) || (`zone_detector`=0))" +
				" AND `zone_id`=0" +
				" ORDER BY `zone_section_id` ");
	}

	/*28.06.2018*/
	public Cursor getAllSectionsCursorForSiteDeviceWithoutControllerKnownType(int site_id, int device_id){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_section_id`>" + Integer.toBinaryString(0) +
				" AND `zone_device_id`=" + Integer.toString(device_id) +
				" AND `zone_id`=" + Integer.toString(0) +
				" AND `zone_detector`>" + Integer.toString(0) +
				" ORDER BY `zone_section_id` ");
	}

	/*27.09.2020*/
	public Cursor getAllSectionsCursorForDeviceWithoutControllerKnownType(int device_id){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				" WHERE `zone_section_id`>" + Integer.toBinaryString(0) +
				" AND `zone_device_id`=" + Integer.toString(device_id) +
				" AND `zone_id`=0" +
				" AND `zone_detector`>0" +
				" ORDER BY `zone_section_id` ");
	}

	public Cursor getSectionsCursorForSiteDeviceWithoutControllerAndCurrentKnownType(int site_id, int device_id, int section_id){
		return rawQuery(dbRead, "SELECT * FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id`" +
				" WHERE `zone_section_id`>" + Integer.toBinaryString(0) +
				" AND `zone_section_id`!=" + Integer.toString(section_id) +
				" AND `zone_device_id`=" + Integer.toString(device_id) +
				" AND `zone_id`=" + Integer.toString(0) +
				" AND `zone_detector`>" + Integer.toString(0) +
				" ORDER BY `zone_section_id` ");
	}

	/*new 21.06.2018*/
	/*TODO*/
//	public LinkedList<Section> getAllSectionsForDevice(int device_id)
//	{
//		Cursor cursor = getAllSectionCursorForDevice(device_id);
//		LinkedList<Section> sections = new LinkedList<Section>();
//		if (null != cursor)
//		{
//			cursor.moveToFirst();
//			if(cursor.getCount()!=0)
//			{
//				Section section = new Section(cursor);
//				section.zones = getAllZonesForSiteDeviceSection(section);
//				sections.add(section);
//				while (cursor.moveToNext())
//				{
//					section = new Section(cursor);
//					section.zones = getAllZonesForSiteDeviceSection(section);
//					sections.add(section);
//				}
//				cursor.close();
//				return sections;
//			}
//		}
//		return null;
//	}

	/*25.06.2018*/
//	private Cursor getAllSectionCursorForDevice(int device_id)
//	{
//		return rawQuery(dbRead, "SELECT * FROM `zones` WHERE (`zone_device_id`=" + Integer.toString(device_id) + ")&(`zone_id`>" + Integer.toString(0) + ")");
//	}

	/*25.06.2018*/
	public int getSectionsCount(int site_id)
	{
		Cursor cursor = getSectionsCursorForSiteWithoutDevice(site_id);
		if (null != cursor)
		{
			int count = cursor.getCount();
			cursor.close();
			return count;
		}
		return 0;
	}


	/*25.06.2018*/
	public int getDevicesCount(int site_id){
		Cursor cursor = rawQuery(dbRead, "SELECT DISTINCT `device_device_id` " +
				"FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id));
		if(null!=cursor){
			int count = cursor.getCount();
			cursor.close();
			return count;
		}
		return 0;
	}


	/*new 21.06.2018*/
	/*TODO*/
	public Zone[] getAllZonesForSiteDeviceSection(Section section)
	{
//		Cursor cursor = getAllZonesCursorForSiteDeviceSection(section.site_id, section.device_id, section.id);
//		if(null!=cursor){
//			cursor.moveToFirst();
//			Zone[] d = new Zone[cursor.getCount()];
//			if(0!=cursor.getCount())
//			{
//				d[0] = new Zone(cursor);
//				int i = 0;
//				while (cursor.moveToNext())
//				{
//					d[i] = new Zone(cursor);
//					i++;
//				}
//			}
//			cursor.close();
//			return d;
//		}
		return null;
	}

	/*25.06.2018*/
	public Cursor getSectionsCursorForSiteDevice(int site_id, int device_id){
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `zones`" +
				" INNER JOIN  (SELECT `device_device_id`, `device_section_id` FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id) + ")" +
				" ON `zones`.`zone_device_id`=`device_device_id` AND `zones`.`zone_section_id`=`device_section_id` " +
				"WHERE `zone_device_id`=" +Integer.toString(device_id) +
				" AND `zone_id` =" + Integer.toString(0) +
				" ORDER BY `zone_section_id`");
	}

	public Cursor getSectionsCursorForDevice(int device_id){
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `zones`" +
				"WHERE `zone_device_id`=" +Integer.toString(device_id) +
				" AND `zone_id` =" + Integer.toString(0) +
				" ORDER BY `zone_section_id`");
	}

	public Cursor getSectionsCursorForSiteDeviceNoCurrent(int site_id, int device_id, int section_id){
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `zones`" +
				" INNER JOIN  (SELECT `device_device_id`, `device_section_id` FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id) + ")" +
				" ON `zones`.`zone_device_id`=`device_device_id` AND `zones`.`zone_section_id`=`device_section_id` " +
				"WHERE `zone_device_id`=" +Integer.toString(device_id) +
				" AND `zone_id` =" + Integer.toString(0) +
				" AND `zone_section_id` !=" + Integer.toString(section_id) +
				" ORDER BY `zone_section_id`");
	}

	/*25.06.2018*/
	public Cursor getAllZonesCursorForSiteDeviceSection(int site_id, int device_id, int section_id, int zoneOrRelay){
		//0 - zones, 1 - relay, 2 - all
		String query = "SELECT * FROM `zones`" +
				" INNER JOIN  (SELECT `device_device_id`, `device_section_id` FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id) + ")" +
				" ON `zones`.`zone_device_id`=`device_device_id` AND `zones`.`zone_section_id`=`device_section_id` " +
				" WHERE (`zone_device_id`=" + Integer.toString(device_id) +
				")AND(`zone_section_id`=" + Integer.toString(section_id) +
				") AND (`zone_id`>" + Integer.toString(0) + ")";
		switch (zoneOrRelay){
			case 0:
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ")";
				break;
			case 1:
				query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
						+ "))";
				break;
			default:
				break;
		}
		query += " ORDER BY `zone_id`";
		return rawQuery(dbRead, query);
	}

	public Cursor getAllZonesCursorForSiteDevice(int site_id, int device_id, int zoneOrRelay){
		String query = "SELECT * FROM `zones`" +
				" INNER JOIN  (SELECT `device_device_id`, `device_section_id` FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id) + ")" +
				" ON `zones`.`zone_device_id`=`device_device_id` AND `zones`.`zone_section_id`=`device_section_id` " +
				" WHERE (`zone_device_id`=" + Integer.toString(device_id) +
				") AND (`zone_id`>" + Integer.toString(0) + ")";
		switch (zoneOrRelay){
			case 0:
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ")";
				break;
			case 1:
				query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
						+ "))";
				break;
			default:
				break;
		}
		query += " ORDER BY `zone_section_id`,`zone_id`";
		return rawQuery(dbRead,  query);
	}

	public Cursor getAllZonesCursorForDeviceSection(int device_id, int section_id, int zoneOrRelay){
		String query = "SELECT * FROM `zones`" +
				" WHERE (`zone_device_id`=" + Integer.toString(device_id) +
				") AND (`zone_section_id`=" + Integer.toString(section_id) +
				") AND(`zone_id`>" + Integer.toString(0) + ")";
		switch (zoneOrRelay){
			case 0:
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ")";
				break;
			case 1:
				query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
						+ "))";
				break;
			default:
				break;
		}
		query += " ORDER BY `zone_section_id`,`zone_id`";
		return rawQuery(dbRead,  query);
	}

	//23.12.2020
	public Cursor getAllZonesCursorForDeviceSectionNoController(int device_id, int section_id, int zoneOrRelay){
		String query = "SELECT `zones`.*, `devices`.`device_config_version` FROM `zones`" +
				" LEFT JOIN `devices` ON `devices`.`device_id`=`zones`.`zone_device_id`" +
				" WHERE (`zone_device_id`=" + Integer.toString(device_id) +
				") AND (`zone_section_id`=" + Integer.toString(section_id) +
				") AND (`zone_id`>" + Integer.toString(0) + " AND NOT (((`device_config_version`>>8)&0xff=1) AND `zone_id`>100))" +
				" AND NOT(`zone_id`=100 AND `zone_section_id`=0)";
		switch (zoneOrRelay){
			case 0:
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ")";
				break;
			case 1:
				query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
						+ "))";
				break;
			default:
				break;
		}
		query += " ORDER BY `zone_section_id`,`zone_id`";
		return rawQuery(dbRead,  query);
	}

	public Cursor getAllZonesCursorForDevice(int device_id, int zoneOrRelay){
		String query = "SELECT * FROM `zones`" +
				" WHERE (`zone_device_id`=" + Integer.toString(device_id) +
				") AND(`zone_id`>" + Integer.toString(0) + ")";
		switch (zoneOrRelay){
			case 0:
				query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
						+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
						+ ")";
				break;
			case 1:
				query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
						+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
						+ "))";
				break;
			default:
				break;
		}
		query += " ORDER BY `zone_section_id`,`zone_id`";
		return rawQuery(dbRead,  query);
	}

	public Cursor getAllZonesCursorForDeviceWithType(int device_id, int zoneOrRelay, int zType, int bind){
		String query = "SELECT * FROM `zones`" +
				" WHERE (`zone_device_id`=" + Integer.toString(device_id) +
				") AND(`zone_id`>" + Integer.toString(0) + ")" +
				" AND (`zone_id`!= " + Integer.toString(100) + ")"
				+(bind == -1? "":" AND (`zone_id`!="+ Integer.toString(bind) +")" )
				+(zType == 0? "":" AND(`zone_detector`=" + Integer.toString(zType) + ")");
		if(zType==0){
			switch (zoneOrRelay){
				case 0:
					query += " AND (`zone_detector`!=" + Integer.toString(RELAY_TYPE)
							+ ") AND (`zone_detector`!=" + Integer.toString(AUTO_RELAY_TYPE)
							+ ") AND (`zone_detector`!=" + Integer.toString(THERMOSTAT_TYPE)
							+ ") AND (`zone_detector`!=" + Integer.toString(SIREN_TYPE)
							+ ") AND (`zone_detector`!=" + Integer.toString(LAMP_TYPE)
							+ ")";
					break;
				case 1:
					query += " AND ((`zone_detector`=" + Integer.toString(RELAY_TYPE)
							+ ") OR (`zone_detector`=" + Integer.toString(AUTO_RELAY_TYPE)
							+ ") OR (`zone_detector`=" + Integer.toString(THERMOSTAT_TYPE)
							+ ") OR (`zone_detector`=" + Integer.toString(SIREN_TYPE)
							+ ") OR (`zone_detector`=" + Integer.toString(LAMP_TYPE)
							+ "))";
					break;
				default:
					break;
			}
		}
		query += " ORDER BY `zone_section_id`,`zone_id`";
		return rawQuery(dbRead,  query);
	}

	public Zone[] getAllZonesForDevice(int device_id){
		Cursor cursor  = getAllZonesCursorForDevice(device_id, 0);
		if(null!=cursor){
			if(0!=cursor.getCount());
			cursor.moveToFirst();
			Zone[] zones = new Zone[cursor.getCount()];
			int i = 0;
			zones[i] = new Zone(cursor);
			while (cursor.moveToNext()) {
				i++;
				zones[i] = new Zone(cursor);
			}
			return zones;
		}
		return null;
	}

	public Zone[] getAllRelays() {
		Cursor cursor = getAllRelaysCursor();
		if (null != cursor) {
			if (0 != cursor.getCount()) {
				cursor.moveToFirst();
				Zone[] zones = new Zone[cursor.getCount()];
				int i = 0;
				zones[i] = new Zone(cursor);
				while (cursor.moveToNext()) {
					i++;
					zones[i] = new Zone(cursor);
				}
				return zones;
			}
		}
		return null;
	}

	public Zone[] getAllRelaysForSite(int site_id) {
		Cursor cursor = getAllZonesCursorForSite(site_id, 1);
		if (null != cursor) {
			if (0 != cursor.getCount()) {
				cursor.moveToFirst();
				Zone[] zones = new Zone[cursor.getCount()];
				int i = 0;
				zones[i] = new Zone(cursor);
				while (cursor.moveToNext())
				{
					i++;
					zones[i] = new Zone(cursor);
				}
				return zones;
			}
		}
		return null;
	}

	public Zone[] getAllZonesForSite(int site_id){
		Cursor cursor  = getAllZonesCursorForSite(site_id, 0);
		if(null!=cursor){
			if(0!=cursor.getCount())
			{
				cursor.moveToFirst();
				Zone[] zones = new Zone[cursor.getCount()];
				int i = 0;
				zones[i] = new Zone(cursor);
				while (cursor.moveToNext()) {
					i++;
					zones[i] = new Zone(cursor);
				}
				return zones;
			}
		}
		return null;
	}

	public Zone[] getAllZonesArray() {
		Cursor cursor = getAllZonesCursor(0);
		if (null != cursor) {
			if (0 != cursor.getCount()) {
				cursor.moveToFirst();
				Zone[] zones = new Zone[cursor.getCount()];
				int i = 0;
				zones[i] = new Zone(cursor);
				while (cursor.moveToNext()) {
					i++;
					zones[i] = new Zone(cursor);
				}
				return zones;
			}
		}
		return null;
	}

	public Zone[] getAllZonesForSiteWithoutController(int site_id) {
		Cursor cursor = getAllZonesCursorForSite(site_id, 2);
		if (null != cursor) {
			if (0 != cursor.getCount()) {
				cursor.moveToFirst();
				Zone[] zones = new Zone[cursor.getCount()];
				int i = 0;
				zones[i] = new Zone(cursor);
				while (cursor.moveToNext())
				{
					i++;
					zones[i] = new Zone(cursor);
				}
				return zones;
			}
		}
		return null;
	}

	public Zone[] getAllZonesForDeviceWithType(int device_id, int zType, int bind){
		Cursor cursor  = getAllZonesCursorForDeviceWithType(device_id, 0, zType, bind);
		if(null!=cursor){
			if(0!=cursor.getCount())
			{
				cursor.moveToFirst();
				Zone[] zones = new Zone[cursor.getCount()];
				int i = 0;
				zones[i] = new Zone(cursor);
				while (cursor.moveToNext())
				{
					i++;
					zones[i] = new Zone(cursor);
				}
				cursor.close();
				return zones;
			}
		}
		return null;
	}


	public Zone[] getAllZonesForSiteDevice(int site_id, int device_id){
		Cursor cursor  = getAllZonesCursorForSiteDevice(site_id, device_id, 0);
		if(null!=cursor){
			if(0!=cursor.getCount())
			{
				cursor.moveToFirst();
				Zone[] zones = new Zone[cursor.getCount()];
				int i = 0;
				zones[i] = new Zone(cursor);
				while (cursor.moveToNext())
				{
					i++;
					zones[i] = new Zone(cursor);
				}
				cursor.close();
				return zones;
			}
			cursor.close();
		}
		return null;
	}

	public Zone[] getFireZonesForDeviceSection(int device_id, int section_id){
		Cursor cursor = getAllFireZonesForSection(device_id, section_id);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				Zone[] zones = new Zone[cursor.getCount()];
				cursor.moveToFirst();
				int i = 0;
				zones[0] = new Zone(cursor);
				while (cursor.moveToNext()){
					i++;
					zones[i] = new Zone(cursor);
				}
				cursor.close();
				return zones;
			}
			cursor.close();
		}
		return null;
	}

	public Zone[] getAllZonesForDeviceSection(int device_id, int section_id){
		Cursor cursor = getAllZonesCursorForDeviceSection(device_id, section_id, 0);
		if(null!=cursor){
			int count =cursor.getCount();
			if(0!=count){
				Zone[] zones = new Zone[count];
				cursor.moveToFirst();
				int i = 0;
				zones[0] = new Zone(cursor);
				while (cursor.moveToNext()){
					i++;
					zones[i] = new Zone(cursor);
				}
				cursor.close();
				return zones;
			}
			cursor.close();
		}
		return null;
	}

	public Cursor getAllFireZonesForSection(int device_id, int section_id){
		return rawQuery(dbRead, "SELECT * FROM `zones` WHERE" +
				" `zone_device_id`= " + Integer.toString(device_id) +
		" AND `zone_section_id`=" + Integer.toString(section_id) +
		" AND `zone_id`>" + Integer.toString(0) +
		" AND `zone_detector`=" + Integer.toString(1));
	}

	public Section[] getGuardSectionsForSiteDeviceWithoutZones(int site_id, int device_id)
	{
		Cursor cursor = getAllGuardSectionsCursorForSiteDeviceWithoutController(site_id, device_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getGuardSectionsForDeviceWithoutZones(int device_id)
	{
		Cursor cursor = getAllGuardSectionsCursorForDeviceWithoutController(device_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getSectionsForSiteDeviceWithoutControllerKnownType(int site_id, int device_id)
	{
		Cursor cursor = getAllSectionsCursorForSiteDeviceWithoutControllerKnownType(site_id, device_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getSectionsForDeviceWithoutControllerKnownType(int device_id)
	{
		Cursor cursor = getAllSectionsCursorForDeviceWithoutControllerKnownType(device_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getSectionsForSiteDeviceWithoutControllerAndCurrentKnownType(int site_id, int device_id, int section_id)
	{
		Cursor cursor = getSectionsCursorForSiteDeviceWithoutControllerAndCurrentKnownType(site_id, device_id, section_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getAllSectionsForSiteDeviceWithoutControllerByType(int device_id, int type)
	{
		Cursor cursor = getAllSectionsCursorForDeviceWithoutControllerByType(device_id, type);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getAllSectionsForSiteDeviceWithController(int site_id, int device_id)
	{
		Cursor cursor = getSectionsCursorForSiteDevice(site_id, device_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getAllSectionsForDeviceWithController(int device_id)
	{
		Cursor cursor = getSectionsCursorForDevice(device_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Section[] getAllSectionsForSiteDeviceWithControllerNoCurrent(int site_id, int device_id, int section_id)
	{
		Cursor cursor = getSectionsCursorForSiteDeviceNoCurrent(site_id, device_id, section_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Section[] sections = new Section[count];
				cursor.moveToFirst();
				int i = 0;
				sections[i] = new Section(cursor);
				while (cursor.moveToNext()){
					i++;
					sections[i] = new Section(cursor);
				}
				cursor.close();
				return sections;
			}
			cursor.close();
		}
		return null;
	}

	public Zone getZone(Cursor cursor)
	{
		if(null!=cursor){
			return new Zone(cursor);
		}
		return null;
	}


	public Zone[] getZonesNotBindedToCam(int site_id)
	{
		Cursor c = getNotBindedZonesForSite(site_id);
		if(c!= null){
			if(0!=c.getCount()){
				c.moveToFirst();
				Zone[] zones = new Zone[c.getCount()];
				int i = 0;
				zones[i] = getZone(c);
				while(c.moveToNext()){
					i++;
					zones[i]= getZone(c);
				}
				c.close();
				return zones;
			}
			c.close();
		}
		return null;
	}

	/*TODO Add device*/
	public Cursor getSiteDeviceSectionByid(int site_id, int device_id, int section_id)
	{
		return rawQuery(dbRead, "SELECT * FROM `zones`" +
				" WHERE ((`zone_section_id`=" + Integer.toString(section_id) + ")" +
				"&(`zone_site_id`=" + Integer.toString(site_id) + ")" +
				"&(`zone_device_id`=" + Integer.toString(device_id) + ")" +
				"&(`zone_id`="+  Integer.toString(0) +"))");
	}

	/*25.06.2018 для проверки есть ли уже раздел с таким номером*/
	public Cursor getDeviceSectionCursorByid(int device_id, int section_id)
	{
		return rawQuery(dbRead, "SELECT `zones`.*,`devices_extra`.`name`, `devices_extra`.`value` FROM `zones` " +
				"LEFT JOIN `devices_extra` " +
				"ON (`devices_extra`.`zone` = `zones`.`_id`)" +
				"WHERE ((`zone_device_id`=" + Integer.toString(device_id) + ")&" +
				"(`zone_section_id`=" + Integer.toString(section_id) + ")&" +
				"(`zone_id`=" + Integer.toString(0) + "))");
	}

	public Section getDeviceSectionById(int device_id, int section_id){
		Cursor cursor = getDeviceSectionCursorByid(device_id, section_id);
		if(null!= cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				Section section = new Section(cursor);
				cursor.close();
				return section;
			}
		}
		return null;
	}

	public Section getDeviceSectionByIdWithExtra(int device_id, int section_id){
		Cursor cursor = getDeviceSectionCursorByid(device_id, section_id);
		if(null!= cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				Section section = new Section(cursor, Const.EXTRAS.TEMP_THRESHOLDS);
				cursor.close();
				return section;
			}
		}
		return null;
	}

	/*25.06.2018 используем при добавлении зон, нет необходимости делать выборку по сайтам*/
	public Cursor getZoneCursorById(int device_id, int section_id, int zone_id)
	{
		return rawQuery(dbRead, "SELECT * FROM `zones` WHERE " +
				"((`zone_id`=" + Integer.toString(zone_id) + ")" +
				"&(`zone_section_id`=" + Integer.toString(section_id) + ")" +
				"&(`zone_device_id`=" + Integer.toString(device_id) + ")" +
				")");
	}

	/**/
	public Cursor getSectionCursorById(int device_id, int section_id)
	{
		return rawQuery(dbRead, "SELECT * FROM `zones` WHERE " +
				"((`zone_id`=" + Integer.toString(0) + ")" +
				"&(`zone_section_id`=" + Integer.toString(section_id) + ")" +
				"&(`zone_device_id`=" + Integer.toString(device_id) + ")" +
				")");
	}

	/*25.06.2018*/
	public Cursor getAllDevicesCursorForSite(int site_id)
	{
		return rawQuery(dbRead, "SELECT * FROM `devices`" +
				" INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id` FROM `device_site` " +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ")" +
				" ON `devices`.`device_id`=`device_device_id`");
	}

	public Device[] getAllDevicesForSite(int site_id){
		Cursor cursor = getAllDevicesCursorForSite(site_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				Device[] devices = new Device[count];
				cursor.moveToFirst();
				int i = 0;
				devices[i] = new Device(cursor);
				while (cursor.moveToNext()){
					i++;
					devices[i] = new Device(cursor);
				}
				cursor.close();
				return devices;
			}
			cursor.close();
		}
		return null;
	}

	public Device[] getAllDevicesForSiteWithRemoteSet(int site_id){
		Cursor cursor = getAllDevicesCursorForSite(site_id);
		if(null!=cursor){
			int count = cursor.getCount();
			if(0!=count){
				LinkedList<Device> devices = new LinkedList<>();
				cursor.moveToFirst();
				int i = 0;
				Device device = new Device(cursor);
				if(device.canBeSetFromApp() && !getDevicePartedStatusForSite(site_id, device.id)){
					devices.add(device);
				}
				while (cursor.moveToNext()){
					device = new Device(cursor);
					if(device.canBeSetFromApp() && !getDevicePartedStatusForSite(site_id, device.id))
					{
						devices.add(device);
					}
				}
				cursor.close();
				if(0!=devices.size())
				{
					Device[] devicesArray = new Device[devices.size()];
					int i1 = 0;
					for(Device device1: devices){
						devicesArray[i1] = device1;
						i1++;
					}
					return devicesArray;
				}
			}
			cursor.close();
		}
		return null;
	}

	/*25.06.2018*/
	public void deleteDataForZone(Zone zone)
	{
		if(zone.id > 0){
			deleteZone(zone);
			deleteEventsForZone(zone);
			checkMainBarsToDelete(zone);
		}else{
			deleteSectionAndZones(zone);
			deleteDeviceSite(zone);
			deleteEventsForSection(zone);
			Section section =new Section(zone.device_id, zone.section_id);
			checkMainBarsToDelete(section);
			deleteSectionGroupSection(section);
		}
	}

	/*27.09.2020*/
	/*удаляем элементы главной при удалении элементов системы из текущей учетки,
	 так как база не успевает отработать до возврата на главную*/
	private void checkMainBarsToDelete(D3Element d3Element)
	{
		if(null!=d3Element){
			MainBar mainBar = null;
			if(d3Element instanceof Zone){
				Zone zone = (Zone) d3Element;
				switch (zone.detector){
					case Const.DETECTOR_AUTO_RELAY:
					case Const.DETECTOR_MANUAL_RELAY:
					case Const.DETECTOR_THERMOSTAT:
						mainBar = getMainBarForRelay(zone);
						break;
					default:
						mainBar = getMainBarForZone(zone);
						break;
				}
			}else if(d3Element instanceof Section){
				Section section = (Section) d3Element;
				mainBar = getMainBarForSection(section);
			}else if(d3Element instanceof Device){
				Device device = (Device) d3Element;
				mainBar = getMainBarForDevice(device);
			}
			if(null!=mainBar){
				deleteMainBar(mainBar);
			}
		}
	}

	public void deleteDeviceSite(Zone zone)
	{
		if(zone.section_id == 0){
			checkMainBarsToDelete(getDeviceById(zone.device_id));
		}
		execSQL(dbWrite, "DELETE FROM `device_site` WHERE `device_device_id`=" + Integer.toString(zone.device_id) + " AND `device_section_id`="  + Integer.toString(zone.section_id));
	}

	/*25.06.2018*/
	public void deleteZone(Zone zone)
	{
		execSQL(dbWrite, "DELETE FROM `zones` WHERE (`zone_id`=" + Integer.toString(zone.id) + ")&(`zone_section_id`=" + Integer.toString(zone.section_id) + ")&(`zone_device_id`=" + Integer.toString(zone.device_id) + ")");
	}

	/*25.06.2018*/
	private void deleteSectionAndZones(Zone zone)
	{
		execSQL(dbWrite, "DELETE FROM `zones` WHERE (`zone_section_id`=" + Integer.toString(zone.section_id) + ")&(`zone_device_id`=" + Integer.toString(zone.device_id) + ")");
	}

	/*25.06.2018*/
	private void deleteEventsForZone(Zone zone)
	{
		execSQL(dbWrite, "DELETE FROM `events`" +
				" WHERE " +
				"(`event_zone`=" + Integer.toString(zone.id) + ")&" +
				"(`event_section`=" + Integer.toString(zone.section_id) + ")&" +
				"(`event_device`=" + Integer.toString(zone.device_id) + ")");
	}

	/*25.06.2018*/
	private void deleteEventsForSection(Zone zone)
	{
		execSQL(dbWrite, "DELETE FROM `events` " +
				"WHERE " +
				"(`event_section`=" + Integer.toString(zone.section_id) + ")&" +
				"(`event_device`=" + Integer.toString(zone.device_id) + ")");
	}

	private void deleteEventsForDevice(int device_id){
		execSQL(dbWrite, "DELETE FROM `events` " +
				"WHERE " +
				"(`event_device`=" + Integer.toString(device_id) + ")");
	}

	/*25.06.2018*/
	public String getZoneNameById(int deviceId, int sectionId, int zoneId)
	{
		Cursor  c = rawQuery(dbRead, "SELECT `zone_name` FROM `zones` WHERE `zone_device_id`=" + Integer.toString(deviceId) +
				" AND `zone_section_id`=" + Integer.toString(sectionId) +
				" AND `zone_id`=" + Integer.toString(zoneId));
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				String name =  c.getString(0);
				c.close();
				if(!name.equals("")){
					return name;
				}else{
					return context.getString(R.string.NO_NAME);
				}
			}
		}
		return context.getString(R.string.UNKNOWN_ZONE);
	}

	public Zone getZoneById(int deviceId, int sectionId, int zoneId)
	{
		Cursor  c = rawQuery(dbRead, "SELECT * FROM `zones` WHERE `zone_device_id`=" + Integer.toString(deviceId) +
				" AND `zone_section_id`=" + Integer.toString(sectionId) +
				" AND `zone_id`=" + Integer.toString(zoneId));
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				Zone zone = new Zone(c);
				c.close();
				return zone;
			}
		}
		return null;
	}

	public Zone getZoneById(int deviceId, int zoneId)
	{
		Cursor  c = rawQuery(dbRead, "SELECT * FROM `zones` WHERE `zone_device_id`=" + Integer.toString(deviceId) +
				" AND `zone_id`=" + Integer.toString(zoneId));
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				Zone zone = new Zone(c);
				c.close();
				return zone;
			}
		}
		return null;
	}

	public String getZoneNameById(int deviceId, int zoneId)
	{
		Cursor  c = rawQuery(dbRead, "SELECT `zone_name` FROM `zones` WHERE `zone_device_id`=" + Integer.toString(deviceId) +
				" AND `zone_id`=" + Integer.toString(zoneId));
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				String name =  c.getString(0);
				c.close();
				if(!name.equals("")){
					return name;
				}else{
					return context.getString(R.string.NO_NAME);
				}
			}
		}
		return context.getString(R.string.UNKNOWN_ZONE);
	}

	/*
	* OPERATIONS WITH AFFECTS
	*
	* */


	/*25.06.2018*/
	public Cursor getAffectsCursorBySiteId(int site_id){
		return rawQuery(dbRead, "SELECT * FROM `events` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section` WHERE" +
				"(" +
				"(" +
				"(`event_class_id`<" + Integer.toString(5) + ")" +
				"OR((`event_class_id`=" + Integer.toString(5) + ")&" +
				"((`event_reason_id`=" + Integer.toString(24) +") OR (`event_reason_id`=" + Integer.toString(27) +"))" +
				")" +
				"OR(" +
				"			(`event_class_id`=" + Integer.toString(6) + ")" +
				"			&((`event_reason_id`=" + Integer.toString(3) +") OR (`event_reason_id`=" + Integer.toString(4) +") OR (`event_reason_id`=" + Integer.toString(7) +"))" +
				"			)" +
				"OR((`event_class_id`=" + Integer.toString(7) + ")&(`event_reason_id`=" + Integer.toString(1) +"))" +
				"OR((`event_class_id`=" + Integer.toString(8) +")&(`event_reason_id`=" + Integer.toString(6) + "))" +
				")" +
				"&(`event_affect`=" + Integer.toString(1) + ")"+
				")" +
				"&(`affect_visiblity`=" + Integer.toString(1) + ") " +

				" ORDER BY `event_class_id` ");
	}

	/*25.06.2018*/
	public LinkedList<Event> getAffectsBySiteId(int site_id){
		Cursor cursor = getAffectsCursorBySiteId(site_id);
		//maybe return affects always
		if(null!=cursor)
		{
			LinkedList<Event> affects = new LinkedList<Event>();
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	/*25.06.2018*/
	public Event getAffect(Cursor cursor){
		if(null!=cursor){
			return new Event(cursor);
		}
		return null;
	}

	/*25.06.2018*/
	public Cursor getOnlyAlarmAffectsCursor(){
		return  rawQuery(dbRead, "SELECT * FROM `events` WHERE (`event_class_id`=" + Integer.toString(0) + ")&(`event_affect`=" + Integer.toString(1) + ")");
	}

	public boolean getAlarmStatusForSection(int device_id, int section_id){
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` WHERE" +
				" `event_device`=" + Integer.toString(device_id) +
		" AND `event_section`=" + Integer.toString(section_id) +
		" AND " +
				"(`event_class_id`=" + Integer.toString(0)+
				" OR `event_class_id`=" + Integer.toString(1) +
				" OR `event_class_id`=" + Integer.toString(2) +
				" OR `event_class_id`=" + Integer.toString(3) +
				")" +
		" AND `event_affect`=" + Integer.toString(1)
//		+" AND (`event_visiblity`=" + Integer.toString(1) + ") "
		);
		if(null!= c ){
			if(0 != c.getCount()){
				c.close();
				return true;
			}
		}
		return false;
	}

	public boolean getAlarmStatusForZone(int device_id, int section_id, int zone_id){
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `events` WHERE" +
				" `event_device`=" + Integer.toString(device_id) +
		" AND `event_section`=" + Integer.toString(section_id) +
		" AND `event_zone`=" + Integer.toString(zone_id) +
		" AND " +
				"(`event_class_id`=" + Integer.toString(0)+
//				" OR `event_class_id`=" + Integer.toString(1) +
				" OR `event_class_id`=" + Integer.toString(2) +
				" OR `event_class_id`=" + Integer.toString(3) +
				")" +
		" AND `event_affect`=" + Integer.toString(1)
//		+" AND (`event_visiblity`=" + Integer.toString(1) + ") "
		);;
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.close();
				return true;
			}
		}
		return false;
	}

	/*25.06.2018*/
	public Cursor getAffectCursorBySiteDeviceSectionId(int site_id, int device_id, int section_id){
		return rawQuery(dbRead, "SELECT * FROM `events` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section` " +
				"WHERE" +
				"(`event_device` = " + Integer.toString(device_id) +")" +
				"&((`event_section` =" + Integer.toString(section_id) +")" +
				"OR " +
				"((`event_section` =" + Integer.toString(0) +")&" +
				"(`event_class_id`="+ Integer.toString(4) +")" +
				"))" +
				"&(`event_affect`=" + Integer.toString(1) + ") " +
				"&(`affect_visiblity`=" + Integer.toString(1) + ") " +
				"& NOT(`event_class_id`=2 AND `event_reason_id`=3) " +
//				"&(`event_class_id` > 0) " +
				"ORDER BY `event_class_id` ");
	}

	/*25.06.2018*/
	public Cursor getAffectCursorByDeviceId(int device_id){
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `events` " +
				"WHERE" +
				"`event_device` =" + Integer.toString(device_id) +
				" AND " +
				" ((`event_section` = 0 AND (`event_zone` = 100 OR `event_zone`=0))" +
				" OR (`event_class_id`=8 AND `event_reason_id`=6))" +
				" AND `event_affect`=1" +
				" ORDER BY `event_class_id` ");
	}

	/*23.09.2020*/
	public Cursor getExtAffectCursorByDeviceId(int device_id){
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `events` " +
				"WHERE" +
				"(`event_device` =" + Integer.toString(device_id) +")" +
				" AND (" +
				"(" +
				"(`event_section` =0) AND ((`event_zone` = 100 OR `event_zone`=0)" +
				" OR" +
				" (`event_class_id`=7 AND `event_detector_id`=21))" +
				") " +
				"OR (`event_class_id`=8 AND `event_reason_id`=6)" +
				")" +
				" AND (`event_affect`=1)" +
				" ORDER BY `event_class_id` ");
	}

	public Cursor getAffectCursorBySiteDeviceId(int site_id, int device_id){
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `events` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section`" +
				"WHERE" +
				"(`event_device` =" + Integer.toString(device_id) +")&" +
				"(`event_affect`=" + Integer.toString(1) + ")&" +
				"((`event_class_id` < " + Integer.toString(5) +  ") OR " +
				"((`event_class_id`=" + Integer.toString(7) + ")&(`event_reason_id`=" + Integer.toString(1) +")) OR " +
				"((`event_class_id`=" + Integer.toString(8) +")&(`event_reason_id`=" + Integer.toString(6) + ")) OR " +
				"(`event_class_id`=" + Integer.toString(5) + " AND (`event_reason_id`=" + Integer.toString(24) + " OR `event_reason_id`=" + Integer.toString(27)+ ")) OR" +
				"(`event_class_id`=" + Integer.toString(6) + " AND (`event_reason_id`=" + Integer.toString(3) + " OR `event_reason_id`=" + Integer.toString(4) + " OR `event_reason_id`=" + Integer.toString(7) + ")) OR " +
				"(`event_class_id`=" + Integer.toString(7) + " AND `event_reason_id`=" + Integer.toString(1) + ")) " +
				"&(`affect_visiblity`=" + Integer.toString(1) + ") " +
				"ORDER BY `event_class_id` ");
	}

	public Cursor nGetAffectsCursorBySiteId(int site_id){
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `events` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section`" +
				"WHERE " +
				"(`event_affect`=" + Integer.toString(1) + ")&" +
				"((`event_class_id` < " + Integer.toString(5) +  ") OR " +
				"((`event_class_id`=" + Integer.toString(7) + ")&(`event_reason_id`=" + Integer.toString(1) +")) OR " +
				"((`event_class_id`=" + Integer.toString(8) +")&(`event_reason_id`=" + Integer.toString(6) + ")) OR " +
				"(`event_class_id`=" + Integer.toString(5) + " AND (`event_reason_id`=" + Integer.toString(24) + " OR `event_reason_id`=" + Integer.toString(27)+ ")) OR" +
				"(`event_class_id`=" + Integer.toString(6) + " AND (`event_reason_id`=" + Integer.toString(3) + " OR `event_reason_id`=" + Integer.toString(4) + " OR `event_reason_id`=" + Integer.toString(7) + ")) OR " +
				"(`event_class_id`=" + Integer.toString(7) + " AND `event_reason_id`=" + Integer.toString(1) + ")" +
				" OR (`event_class_id`=8 AND `event_reason_id`=6)) " +
				"&(`affect_visiblity`=" + Integer.toString(1) + ") " +
				"ORDER BY `event_class_id` ");
	}

	public Cursor nGetAffectsCursorBySiteId4Farid(int site_id){
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `events` " +
				"INNER JOIN  (SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section`" +
				"WHERE " +
				"(`event_affect`=" + Integer.toString(1) + ")&" +
				"((`event_class_id`<2) OR " +
				"(`event_class_id`=2 AND `event_reason_id`!=3) OR " +
				"(`event_class_id`>2 AND `event_class_id` < 5) OR " +
				"(`event_class_id` =2 AND `event_reason_id`=3 AND `event_section`=0 AND (`event_zone`= 0 OR `event_zone`=100))  OR " +
				"((`event_class_id`=" + Integer.toString(7) + ")&(`event_reason_id`=" + Integer.toString(1) +")) OR " +
				"((`event_class_id`=" + Integer.toString(8) +")&(`event_reason_id`=" + Integer.toString(6) + ")) OR " +
				"(`event_class_id`=" + Integer.toString(5) + " AND (`event_reason_id`=" + Integer.toString(24) + " OR `event_reason_id`=" + Integer.toString(27)+ ")) OR" +
				"(`event_class_id`=" + Integer.toString(6) + " AND (`event_reason_id`=" + Integer.toString(3) + " OR `event_reason_id`=" + Integer.toString(4) + " OR `event_reason_id`=" + Integer.toString(7) + ")) OR " +
				"(`event_class_id`=" + Integer.toString(7) + " AND `event_reason_id`=" + Integer.toString(1) + ")" +
				" OR (`event_class_id`=8 AND `event_reason_id`=6)) " +
				"&(`affect_visiblity`=" + Integer.toString(1) + ") " +
				"ORDER BY `event_class_id` ");
	}



	/*25.06.2018*/
	public LinkedList<Event> getAffectsBySiteDeviceSectionId(int site_id, int device_id, int section_id){
		Cursor cursor = getAffectCursorBySiteDeviceSectionId(site_id, device_id, section_id);
		//maybe return affects always
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
		}
		return null;
	}

	/*25.06.2018*/
	public LinkedList<Event> getAffectsByDeviceId(int device_id){
		Cursor cursor = getAffectCursorByDeviceId(device_id);
		//maybe return affects always
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	/*23.09.2020 to get affects from inner modules like modem, e.t.c*/
	public LinkedList<Event> getExtAffectsByDeviceId(int device_id){
		Cursor cursor = getExtAffectCursorByDeviceId(device_id);
		//maybe return affects always
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	public LinkedList<Event> getAffectsBySiteDeviceId(int site_id, int device_id){
		Cursor cursor = getAffectCursorBySiteDeviceId(site_id, device_id);
		//maybe return affects always
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	public LinkedList<Event> nGetAffectsBySiteId(int site_id){
		Cursor cursor = nGetAffectsCursorBySiteId4Farid(site_id);
		//maybe return affects always
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	//25.06.2018
	// get all affects for zone without sections exclude (logic in adapter, detect excluding by another way) + exclude signal level
	public Cursor getAffectCursorBySiteDeviceSectionZoneId(int site_id, int device_id, int section_id, int zone_id){
		return rawQuery(dbRead, "SELECT DISTINCT `events`.*, `device_site`.`device_site_id`, `devices`.`device_hw_id` FROM `events` " +
				"LEFT JOIN `device_site` ON `device_device_id`=`events`.`event_device`" +
				"LEFT JOIN `devices` ON `devices`.`device_id`=`events`.`event_device`" +
				"WHERE `device_site`.`device_site_id`=" + Integer.toString(site_id) +
				" AND `event_device`=" + Integer.toString(device_id) +
				" AND " +
				" (" +
					"(" +
						"`event_section` =" + Integer.toString(section_id) +
						" AND `event_zone`="+ Integer.toString(zone_id) +
					") " +
					" OR (`event_zone`=" + Integer.toString(zone_id+100) +" AND `event_class_id`=10 AND `event_reason_id`=4)" +
					"OR (`event_section` =0 AND `event_class_id`=4)" +
					" OR (`event_section`=0 AND (`event_zone`=0 OR `event_zone`=100) AND `event_class_id`=2 AND `event_reason_id`=3)" +
				")" +
				" AND `event_affect`=1" +
				" AND `affect_visiblity`=1 " +
				" ORDER BY `event_class_id`"
		);
	}

	public Cursor nGetAffectCursorByDeviceSectionZoneId(int device_id, int section_id, int zone_id){
		return rawQuery(dbRead, "SELECT * FROM `events` " +
				"WHERE" +
				" `event_device`=" + Integer.toString(device_id) +
				" AND (`event_zone`="+ Integer.toString(zone_id) + " AND`event_section` =" + Integer.toString(section_id) +
				" OR `event_section`=0 AND (`event_zone`=0 OR `event_zone`=100) AND `event_class_id`=2 AND `event_reason_id`=3)" +
				"AND(`event_affect`=1)" +
				"AND(`affect_visiblity`=1) " +
				" ORDER BY `event_class_id`"
		);
	}

	//23.09.2020
	public Cursor nGetAffectCursorForController(int device_id){
		return rawQuery(dbRead, "SELECT * FROM `events` " +
				"WHERE" +
				"(`event_device`=" + Integer.toString(device_id) + ")" +
				"&(`event_zone`=0  OR `event_zone`=100)" +
				"&(`event_section`=0)" +
				"&(`event_affect`=1)" +
				"&(`affect_visiblity`=1) " +
				" ORDER BY `event_class_id`"
		);
	}

	public Cursor nGetAffectCursorByDeviceSectionId(int device_id, int section_id){
		return rawQuery(dbRead, "SELECT * FROM `events` " +
				"WHERE" +
				"(`event_device`=" + Integer.toString(device_id) + ")" +
				"&(`event_section` =" + Integer.toString(section_id) +")" +
				"&(`event_affect`=1)" +
				"&(`affect_visiblity`=1) " +
				" ORDER BY `event_class_id`"
		);
	}

	public Cursor getAffectCursorByDeviceSectionZoneId(int device_id, int section_id, int zone_id){
		return rawQuery(dbRead, "SELECT * FROM `events` " +
				"WHERE" +
				"`event_device`=" + Integer.toString(device_id) +
				" AND (`event_section` =" + Integer.toString(section_id) +" AND `event_zone`="+ Integer.toString(zone_id)
				+" OR `event_section`=0 AND (`event_zone`=0 OR `event_zone`=100) AND `event_class_id`=2 AND `event_reason_id`=3) " +
				" AND `event_affect`=1 " +
				" AND `affect_visiblity`=1" +
				" ORDER BY `event_class_id`"
		);
	}

	//25.06.2018 get all affects for zone with section from arm exclude
	public Cursor getAffectCursorBySiteDeviceSectionZoneIdWithExclude(int site_id, int device_id, int section_id, int zone_id){
		return rawQuery(dbRead, "SELECT * FROM `events` " +
				"INNER JOIN  (SELECT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section` " +
				"WHERE" +
				"(`event_device`=" + Integer.toString(device_id) + ")&" +
				"("+

				"((`event_section` =" + Integer.toString(section_id) +")&(`event_zone`="+ Integer.toString(zone_id) +"))"+
				"OR " +
				"(((`event_section` =" + Integer.toString(section_id) +")OR(`event_section` =" + Integer.toString(0) +"))&(`event_class_id`="+ Integer.toString(4) +"))" +
				"OR " +
				"((`event_section` =" + Integer.toString(section_id) + ")&(`event_class_id`="+ Integer.toString(5) +")&(`event_reason_id`="+ Integer.toString(27) +"))" +

				")" +
				"&(`event_affect`=" + Integer.toString(1) + ")" +
				"&(`affect_visiblity`=" + Integer.toString(1) + ") " +
				"  ORDER BY `event_class_id`"
		);
	}

	/*25.06.2018*/
	public LinkedList<Event> getAffectsByZoneId(int siteId, int deviceId, int sectionId, int zoneId){
		Cursor cursor = getAffectCursorBySiteDeviceSectionZoneId(siteId, deviceId, sectionId, zoneId);
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	public LinkedList<Event> nGetAffectsByZone(int deviceId, int sectionId, int zoneId){
		//для контроллера аффекты могут быьт в зонах 0 и 100
		Cursor cursor = (0==sectionId && 0==zoneId) ? nGetAffectCursorForController(deviceId) : nGetAffectCursorByDeviceSectionZoneId(deviceId, sectionId, zoneId);
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	public LinkedList<Event> nGetAffectsBySection(int deviceId, int sectionId){
		Cursor cursor = nGetAffectCursorByDeviceSectionId(deviceId, sectionId);
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	public LinkedList<Event> getAffectsListOnlyByZoneId(int deviceId, int sectionId, int zoneId){
		Cursor cursor = getAffectCursorByDeviceSectionZoneId(deviceId, sectionId, zoneId);
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	/*TODO доделать логику 05,07,2018*/
	public boolean getSiteDeviceSectionsArmStatus(int site_id, int device_id)
	{
//		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `affects` " +
//				"WHERE (`affect_site_id`=" + site_id + ")&" +
//				"(`event_class_id`=" + Integer.toString(4) + ")&" +
//				"(`event_zone`=" + Integer.toString(0) + ")");
//		if(null!=cursor){
//			if(0!=cursor.getCount()){
//				cursor.close();
//				return true;
//			}
//		}
		return false;
	}

	public String getCurrentDeviceBatteryState(int deviceId)
	{
		String currentControllerBatterStatus = null;

		Cursor cursor = getCurrentDeviceBatteryStateCursor(deviceId);
		if(null!=cursor){
			cursor.moveToFirst();
			if(cursor.getCount() > 0){
				String jdata = cursor.getString(16);
				if(null != jdata && !jdata.equals("")){
					try
					{
						JSONObject jsonObject = new JSONObject(jdata);
						int level = jsonObject.getInt("%");
						return Integer.toString(level);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
				}
				cursor.close();
			}
			cursor.close();
		}
		return currentControllerBatterStatus;
	}

	private Cursor getCurrentDeviceBatteryStateCursor(int deviceId)
	{

		return rawQuery(dbRead, "SELECT * " +
				"FROM `events` " +
				"WHERE (" +
				"(`event_device`=" + Integer.toString(deviceId) + ")&" +
				"(`event_class_id`=" + Integer.toString(6) +")&" +
				"(`event_section`=" + Integer.toString(0) + ")&" +
				"(`event_reason_id`=" + Integer.toString(9) + ")&" +
				"(`event_visiblity`=" + Integer.toString(1) + ")&" +
				"(`event_active`=" + Integer.toString(1) + "))" +
				" ORDER BY `event_id` DESC LIMIT 1");
	}

	public boolean getZoneRegisterStatus(Zone zone)
	{
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `events` WHERE" +
				"((`event_device`=" + Integer.toString(zone.device_id) + ")" +
				"&(`event_section`=" + Integer.toString(zone.section_id) +")" +
				"&(`event_zone`=" + Integer.toString(zone.id) + ")" +
				"&(`event_class_id`=" + Integer.toString(5) + ")" +
				"&(`event_reason_id`=" + Integer.toString(24) + ")" +
				"&(`event_affect`=" + Integer.toString(1) + "))");
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.close();
				return false;
			}
			cursor.close();
		}
		return true;
	}

	/*
		COMMANDS
	 */

	public long addCommandById(Command command){
		Cursor cursor = getCommandCursorById(command.id);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				updateRecentCommand(command);
			}else{
				addNewCommand(command);
			}
			cursor.close();
		}
		return 0;
	}


	private long addNewCommand(Command command)
	{
		if(execSQL(dbWrite, "INSERT INTO `commands` (`command_id`, `command_inner_id`, `command_type`) VALUES (?, ?, ?)",
				new String[]{
						Integer.toString(command.id),
						Integer.toString(command.inner_id),
						command.type
				})){
			return lastInsertedRowId(dbWrite);
		}
		return 0;
	}

	private void updateRecentCommand(Command command)
	{
		execSQL(dbWrite, "UPDATE `commands` SET  `command_type`='" + String.valueOf(command.type) +
				"' " +
				(command.result > -1 ? (", `command_result`=" + Integer.toString(command.result)): "") +
				" WHERE `command_id`=" + Integer.toString(command.id));
	}

	public Command getCommandById(int id){
		Cursor cursor = getCommandCursorById(id);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				Command command = new Command(cursor);
				cursor.close();
				return command;
			}
			cursor.close();
			return null;
		}
		return null;
	}

	private Cursor getCommandCursorById(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `commands` WHERE `command_id`=" + Integer.toString(id));
	}

	public int getLastCommand(){
		Cursor cursor = rawQuery(dbRead, "SELECT MAX(`command_inner_id`) FROM `commands`");
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				int i = cursor.getInt(0);
				cursor.close();
				return i;
			}
			cursor.close();
		}
		return 1;
	}

	/*
		EVENTS
	 */

	/*22.06.2018*/
	public Event getEventById(int id)
	{
		Cursor c =  rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_id`=" + Integer.toString(id));
		if(null!=c){
			if(c.getCount()!=0){
				c.moveToFirst();
				Event event = getEvent(c);
				c.close();
				return event;
			}
			c.close();
		}
		return null;
	}

	/*22.06.2018*/
	public long addAffect(Event event, int visiblity, int event_visiblity){
		Cursor c =  rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_id`=" + Integer.toString(event.id));
		if(null!=c){
			if(0!=c.getCount()){
				execSQL(dbWrite, "UPDATE `events` SET `event_affect`=" + Integer.toString(event.affect)+
				", `affect_visiblity`=" + Integer.toString(visiblity) +
				", `event_visiblity`=" + Integer.toString(event_visiblity) +
				" WHERE `event_id`=" + Integer.toString(event.id));
			}else {
				addNewAffect(event, visiblity, event_visiblity);
			}
			c.close();
		}
		return  lastInsertedRowId(dbWrite);
	}

	public long addNewAffect(Event event, int visiblity, int event_visiblity)
	{
		if (execSQL(dbWrite,
				"INSERT INTO `events` (`event_id`, `event_device`, `event_section`, `event_zone`, `event_active`, `event_class_id`, `event_detector_id`, `event_reason_id`, `event_time`, " +
						"`event_local_time`, `event_channel`, `event_review_status`, `event_comment`, `event_visiblity`, `affect_visiblity`, `event_affect`, `event_jdata`, `receive_time`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				new String[]{
						Integer.toString(event.id),
						Integer.toString(event.device),
						Integer.toString(event.section),
						Integer.toString(event.zone),
						Integer.toString(event.active),
						Integer.toString(event.classId),
						Integer.toString(event.detectorId),
						Integer.toString(event.reasonId),
						Long.toString(event.time),
						Long.toString(event.localtime),
						Integer.toString(event.channel),
						Integer.toString(event.reviewStatus),
						event.data,
						Integer.toString(event_visiblity),
						Integer.toString(visiblity),
						Integer.toString(event.affect),
						event.jdata,
						Long.toString(System.currentTimeMillis())
				}
		)) {
			return lastInsertedRowId(dbWrite);
		}
		return 0;
	}

	public long addNewEvent(Event event, int visiblity)
	{
		if (execSQL(dbWrite,
				"INSERT INTO `events` (`event_id`, `event_device`, `event_section`, `event_zone`, `event_active`, `event_class_id`, `event_detector_id`, `event_reason_id`, `event_time`, `event_local_time`, `event_channel`, `event_review_status`, `event_comment`, `event_visiblity`, `event_affect`, `event_jdata`, `receive_time`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				new String[]{
						Integer.toString(event.id),
						Integer.toString(event.device),
						Integer.toString(event.section),
						Integer.toString(event.zone),
						Integer.toString(event.active),
						Integer.toString(event.classId),
						Integer.toString(event.detectorId),
						Integer.toString(event.reasonId),
						Long.toString(event.time),
						Long.toString(event.localtime),
						Integer.toString(event.channel),
						Integer.toString(event.reviewStatus),
						event.data,
						Integer.toString(visiblity),
						Integer.toString(event.affect),
						event.jdata,
						Long.toString(System.currentTimeMillis())
				}
		)) {
			return lastInsertedRowId(dbWrite);
		}
		return 0;
	}


	public Cursor 	getAllEventsCursorFromDB(){
		return rawQuery(dbRead, "SELECT * FROM `events`");
	}

	public Cursor 	getAllActualEventsCursorFromDB(){
		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_visiblity`=1 ORDER BY `event_id` DESC");
	}

	public boolean getEventsExistence()
	{
		Cursor cursor = getAllEventsCursorFromDB();
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.close();
				return true;
			}
			cursor.close();
		}
		return false;
	}

	public Cursor getAllEventsCursorForCurrentFilter(ArrayList<Integer> classes){
		String query = "SELECT * FROM `events`";
		if(classes != null)
		{
			if(classes.size() !=0 )
			{
				query += " WHERE (";
				for (Integer currentClass : classes)
				{
					query+="(`event_class_id`=" + Integer.toString(currentClass) + ") OR";
				}
				query = query.substring(0, query.length()-3) +
						")& (`event_visiblity`=1)" +
						" ORDER BY `event_id` DESC";
				return  rawQuery(dbRead, query);
			}
		}
		return null;
	}

	/*DEPRECATED NO MORE NEED 22.06.2018 DONT SHOW RELAYS  SEPARATELY FROM OTHER ZONES*/
	public Cursor getRelesEventsForSiteCursorWithFilter(int site_id, ArrayList<Integer> classes){
//		String query = "SELECT * FROM `events` WHERE ((`event_site`=" + Integer.toString(site_id) + ")&(`event_section` = " + Integer.toString(0) + ")&(`event_zone`>" + Integer.toString(0) + ")";
//
//		if(classes != null)
//		{
//			if(classes.size() !=0 )
//			{
//				query+="&(";
//				for (Integer currentClass : classes)
//				{
//					query+="(`event_class_id`=" + Integer.toString(currentClass) + ") OR";
//				}
//				query = query.substring(0, query.length()-3) + ")";
//				query+=")&(`event_visiblity`=" + Integer.toString(1) + ") ORDER BY `event_id` DESC";
//				return  rawQuery(dbRead, query);
//			}
//
//		}
		return null;
	}

	/*25.06.2018*/
	public Cursor getEventsForSiteWithFilter(int site_id, ArrayList<Integer> classes, boolean showRelays, int zoneOrRelay){
		String query = "SELECT * FROM `events` INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`, `device_section_id` " +
				"FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section` " +
				"WHERE ";

		if(showRelays){
			switch (zoneOrRelay){
				case 0:
					query += "(`event_detector_id`!=" + Integer.toString(RELAY_TYPE) + ")&";
					query += "(`event_detector_id`!=" + Integer.toString(AUTO_RELAY_TYPE) + ")&";
					query += "(`event_detector_id`!=" + Integer.toString(THERMOSTAT_TYPE) + ")&";
					query += "(`event_detector_id`!=" + Integer.toString(SIREN_TYPE) + ")&";
					query += "(`event_detector_id`!=" + Integer.toString(LAMP_TYPE) + ")&";
					break;
				case 1:
					query += "((`event_detector_id`=" + Integer.toString(RELAY_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(AUTO_RELAY_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(THERMOSTAT_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(SIREN_TYPE) + ")";
					query += "OR (`event_detector_id`=" + Integer.toString(LAMP_TYPE) + "))&";
					break;
			}
		}
		if(classes != null)
		{
			if(classes.size() !=0 )
			{
				query+="(";
				for (Integer currentClass : classes)
				{
					query+="(`event_class_id`=" + Integer.toString(currentClass) + ") OR";
				}
				query = query.substring(0, query.length()-3) + ")";
				query+="&(`event_visiblity`=" + Integer.toString(1) + ") ORDER BY `event_id` DESC";
				return  rawQuery(dbRead, query);
			}

		}
		return null;
	}


	public Cursor getEventsForSiteDeviceWithFilter(int site_id, int device_id, ArrayList<Integer> classes, boolean showRelays, int zoneOrRelay){
		String query = "SELECT * FROM `events` INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) +
				") ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section` " +
				"WHERE " +
				"(`event_device`=" + Integer.toString(device_id) + ")"
//				+ "&(NOT((`event_section`=" + Integer.toString(0) + ")&(`event_zone`>" + Integer.toString(0) + ")))"
				;
		if(showRelays){
			switch (zoneOrRelay){
				case 0:
					query += "&(`event_detector_id`!=" + Integer.toString(RELAY_TYPE) + ")";
					query += "&(`event_detector_id`!=" + Integer.toString(AUTO_RELAY_TYPE) + ")";
					query += "&(`event_detector_id`!=" + Integer.toString(THERMOSTAT_TYPE) + ")";
					query += "&(`event_detector_id`!=" + Integer.toString(SIREN_TYPE) + ")";
					query += "&(`event_detector_id`!=" + Integer.toString(LAMP_TYPE) + ")";
					break;
				case 1:
					query += "&((`event_detector_id`=" + Integer.toString(RELAY_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(AUTO_RELAY_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(THERMOSTAT_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(SIREN_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(LAMP_TYPE) + "))";
					break;
			}
		}
		if(classes != null)
		{
			if(classes.size() !=0 )
			{
				query+="&(";
				for (Integer currentClass : classes)
				{
					query+="(`event_class_id`=" + Integer.toString(currentClass) + ") OR";
				}
				query = query.substring(0, query.length()-3) + ")";
				query+="&(`event_visiblity`=" + Integer.toString(1) + ") ORDER BY `event_id` DESC";
				return  rawQuery(dbRead, query);
			}

		}
		return null;
	}

	/*DEPRECATED NO MORE NEED 22.06.2018 DONT SHOW RELAYS  SEPARATELY FROM OTHER ZONES*/
	public Cursor getAllZoneWithoutReleEventsForSiteWithFilter(int site_id, ArrayList<Integer> classes){
//		String query = "SELECT * FROM `events` WHERE (" +
//				"(`event_site`=" + Integer.toString(site_id) + ")&(NOT(`event_section`=" + Integer.toString(0) +")&(`event_zone` >" + Integer.toString(0) + "))";
//
//		if(classes != null)
//		{
//			if(classes.size() !=0 )
//			{
//				query+="&(";
//				for (Integer currentClass : classes)
//				{
//					query+="(`event_class_id`=" + Integer.toString(currentClass) + ") OR";
//				}
//				query = query.substring(0, query.length()-3) + ")";
//				query+="&(`event_visiblity`=" + Integer.toString(1) + ") ORDER BY `event_id` DESC";
//				return  rawQuery(dbRead, query);
//			}
//
//		}
		return null;
	}

	public Cursor getEventsForSiteDeviceSectionWithFilter(int site_id, int device_id, int section_id, ArrayList<Integer> classes, boolean showRelays, int zoneOrRelay){
		String query = "SELECT * FROM `events` WHERE " +
				"(`event_device`=" + Integer.toString(device_id) + ")&" +
				"(`event_section` =" +  Integer.toString(section_id)+ " )";
		if(showRelays){
			switch (zoneOrRelay){
				case 0:
					query += "&(`event_detector_id`!=" + Integer.toString(RELAY_TYPE) + ")";
					query += "&(`event_detector_id`!=" + Integer.toString(AUTO_RELAY_TYPE) + ")";
					query += "&(`event_detector_id`!=" + Integer.toString(THERMOSTAT_TYPE) + ")";
					query += "&(`event_detector_id`!=" + Integer.toString(SIREN_TYPE) + ")";
					query += "&(`event_detector_id`!=" + Integer.toString(LAMP_TYPE) + ")";
					break;
				case 1:
					query += "&((`event_detector_id`=" + Integer.toString(RELAY_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(AUTO_RELAY_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(THERMOSTAT_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(SIREN_TYPE) + ")";
					query += " OR (`event_detector_id`=" + Integer.toString(LAMP_TYPE) + ")";
					query += ")";
					break;
			}
		}

		if(classes != null)
		{
			if(classes.size() !=0 )
			{
				query+="&(";
				for (Integer currentClass : classes)
				{
					query+="(`event_class_id`=" + Integer.toString(currentClass) + ") OR";
				}
				query = query.substring(0, query.length()-3) + ")";
				query+="&(`event_visiblity`=" + Integer.toString(1) + ") " +
						"ORDER BY `event_id` DESC";
				return  rawQuery(dbRead, query);
			}

		}
		return null;
	}


	public Cursor getAllEventsCursorByClass(int class_id){
		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=" + Integer.toString(class_id) + " ORDER BY `event_id` DESC");
	}
	public Cursor getAllEventsCursorByDetector(int detector_id){
		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_detector_id`=" + Integer.toString(detector_id) + " ORDER BY `event_id` DESC");
	}
//	public Cursor getAllEventsCursorByActiveState(int active){
//		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_active`=? ORDER BY `event_id` DESC ", new String[]{Integer.toString(active)});
//	}

	public Event getEvent(Cursor cursor)
	{
		if (null!=cursor)
		{
			return new Event(cursor);
		}
		return null;
	}

//	public Event getEventLast(Cursor cursor)
//	{
//		if (null!=cursor)
//		{
//			cursor.moveToFirst();
//			return new Event(cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7), cursor.getInt(8), cursor.getLong(9), cursor.getLong(10), cursor.getInt(11), cursor.getInt(12), cursor.getString(13));
//		}
//		return null;
//	}
//
//	public LinkedList<Event> getEventFromDB(Cursor cursor){
//		if (null!=cursor)
//		{
//			cursor.moveToFirst();
//			LinkedList<Event> events = new LinkedList<>();
//			while(cursor.moveToNext()){
//				events.add(new Event(cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getInt(5), cursor.getInt(6), cursor.getInt(7), cursor.getInt(8), cursor.getLong(9), cursor.getLong(10), cursor.getInt(11), cursor.getInt(12), cursor.getString(13)));
//			}
////			cursor.close();
//			return events;
//		}
//		return null;
//	}

	public int getEventMaxId(){
		Cursor cursor = rawQuery(dbRead, "SELECT MAX(`event_id`) FROM `events`");
		if (null!=cursor){
			cursor.moveToFirst();
			int i =  cursor.getInt(0);
			cursor.close();
			return i;

		}
		return 0 ;
	}
	public int getEventMinId(){
		Cursor cursor = rawQuery(dbRead, "SELECT MIN(`event_id`) FROM `events` WHERE `event_affect`=" + Integer.toString(0)
		+ " AND `event_class_id`!=4" );
		if (null!=cursor){
			cursor.moveToFirst();
			int i =  cursor.getInt(0);
			cursor.close();
			return i;

		}
		return 0 ;
	}


	public void updateAllEventsAffectStatus(int i)
	{
		execSQL(dbWrite, "UPDATE `events` SET `event_affect`=" + Integer.toString(i));
	}

	public void updateEventAffectStatus(int id, int i)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_id`=" + Integer.toString(id));
		if(null!= c){
			c.moveToFirst();
			if(0 != c.getCount()){
				execSQL(dbWrite, "UPDATE `events` SET `event_affect`=" + Integer.toString(i) + " WHERE `event_id`=" + Integer.toString(id));
			}
			c.close();
		}
	}



	public long getEventMaxTime(){
		Cursor cursor = rawQuery(dbRead, "SELECT MAX(`event_time`) FROM `events`");
		if (null!=cursor){
			cursor.moveToFirst();
			long i =  cursor.getInt(0);
			cursor.close();
			return i;

		}
		return 0 ;
	}

	public long getEventMinTime(){
		Cursor cursor = rawQuery(dbRead, "SELECT MIN(`event_time`) FROM `events` WHERE `event_affect`=" + Integer.toString(0)
				+ " AND `event_class_id`!=4" );
		if (null!=cursor){
			cursor.moveToFirst();
			long i =  cursor.getInt(0);
			cursor.close();
			return i;

		}
		return 0 ;
	}

	public void deleteOldEvents(){
		execSQL(dbWrite, "DELETE FROM `events` WHERE `event_time` <" + Long.toString(System.currentTimeMillis()/1000 - 3600*24*31));
	}

	//	public void reselAffectStatusForEvents()
	//	{
	//		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `affects`");
	//		if(null!= cursor)
	//		{
	//			cursor.moveToFirst();
	//			execSQL(dbWrite, "DELETE FROM `affects`");
	//			cursor.close();
	//		}
	//	}

	/*25.06.2018*/
	public long getSiteLastEventTime(int site_id)
	{
		Cursor c = rawQuery(dbRead, "SELECT `event_local_time` FROM `events` " +
				"INNER JOIN  (SELECT `device_device_id`, `device_section_id`  FROM `device_site` " +
				"WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section` WHERE `event_visiblity` ="
				 + Integer.toString(1) + " ORDER BY `_id` DESC");
		if(null!=c)
		{
			if(0!=c.getCount())
			{
				c.moveToFirst();
				long siteLastEventTime = c.getLong(0);
				c.close();
				return siteLastEventTime;
			}
			c.close();
		}
		return 0;
	}

	/*25.06.2018*/
	public long getDeviceLastEventTime(int site_id, int device_id){
	Cursor c = rawQuery(dbRead, "SELECT `event_local_time` FROM `events` " +
			"INNER JOIN  " +
			"(SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` WHERE `device_site_id`= " + Integer.toString(site_id) + ") " +
			"ON `device_device_id`=`events`.`event_device`AND `device_section_id`=`events`.`event_section` WHERE " +
			"`event_device`=" + Integer.toString(device_id) +
			" ORDER BY `_id` DESC");
		if(null!=c)
		{
			if(0!=c.getCount())
			{
				c.moveToFirst();
				long sectionLastEventTime = c.getLong(0);
				c.close();
				return sectionLastEventTime;
			}
			c.close();
		}
		return 0;
	}

	/*25.06.2018*/
	public long getSectionLastEventTime(int device_id, int section_id){
		Cursor c = rawQuery(dbRead, "SELECT DISTINCT `event_local_time` FROM `events` " +
				"WHERE (`event_visiblity`=" + Integer.toString(1) +")&" +
				"(`event_device`=" + Integer.toString(device_id) + ")&" +
				"((`event_section`="+ Integer.toString(section_id) +")" +
				" OR " +
				"((`event_class_id`=" + Integer.toString(4) + " ) " +
				"& (`event_section`= " + Integer.toString(0) + ")))" +
				" ORDER BY `_id` DESC");
		if(null!=c)
		{
			if(0!=c.getCount())
			{
				c.moveToFirst();
				long sectionLastEventTime = c.getLong(0);
				c.close();
				return sectionLastEventTime;
			}
			c.close();
		}
		return 0;
	}

	/*25.06.2018*/
	public long getZoneLastEventTime(int device_id, int section_id, int zone_id){
		Cursor c = rawQuery(dbRead, "SELECT `event_local_time` FROM `events` " +
				"WHERE (`event_visiblity`=" + Integer.toString(1) +")&" +
				"(`event_device`=" + Integer.toString(device_id) + ")&" +
				"(`event_section`=" + Integer.toString(section_id)+ ")&" +
				"(`event_zone`=" + Integer.toString(zone_id)+ ")" +
//				"((`event_section`="+ Integer.toString(section_id) +")&((`event_zone`="+ Integer.toString(zone_id) + ")OR(`event_class_id`=" + Integer.toString(4) + "))) " +
//				"OR ((`event_class_id`= " + Integer.toString(4) + ")&(`event_section`=" + Integer.toString(0) + "))" +
//				")" +
				" ORDER BY `_id` DESC");
		if(null!=c)
		{
			if(0!=c.getCount())
			{
				c.moveToFirst();
				long zoneLastEventTime = c.getLong(0);
				c.close();
				return zoneLastEventTime;
			}
			c.close();
		}
		return 0;
	}

	public Cursor getDebugEventsCursor (){
		return rawQuery(dbRead, "SELECT * FROM `events` " +
				"WHERE `event_class_id`=9 OR `event_class_id`=15 " +
				"ORDER BY `event_id` DESC");
	}

	public Cursor getDebugEventsCursor (int site, int device){
		return rawQuery(dbRead, "SELECT DISTINCT `events`.*, `device_site`.`device_site_id` FROM `events` " +
				"LEFT JOIN `device_site` ON `device_site`.`device_device_id` = `events`.`event_device`" +
				"WHERE (`event_class_id`=9 OR `event_class_id`=15) " +
				" AND `device_site`.`device_site_id`=" + Integer.toString(site) +
				" AND `event_device`=" + Integer.toString(device) +
				" ORDER BY `event_id` DESC");
	}

	public Cursor getEventCursorForHistory(int objectId, int filter, long timeFrom, long timeTo)
	{
		String query = "";
		if(objectId != 0){
			query = "SELECT DISTINCT * FROM `events` " +
					"INNER JOIN " +
					"(SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
					"WHERE `device_site_id`=" + Integer.toString(objectId) + ")" +
					" ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section`" +
					" WHERE (`event_time` >" + (timeFrom/1000) + ")&(`event_time`<" + (timeTo/1000) +
					")&(`event_visiblity`= " + Integer.toString(1) + ")";
		}else{
			query = "SELECT * FROM `events`" +
					" WHERE (`event_time` >" + (timeFrom/1000) + ")&(`event_time`<" + (timeTo/1000) +
					")&(`event_visiblity`= " + Integer.toString(1) + ")";
		}
		if(filter != -1){
			query += "&(`event_class_id`=" + Integer.toString(filter) + ")";
		}
		query += " AND (`event_class_id`!=6)";
		query += " AND (`event_class_id`!=7)";
		query += " AND (`event_class_id`!=15)";
		query += " AND (`event_class_id`!=9)";
		query += " AND NOT (`event_class_id`=10 AND `event_reason_id`=4)";
		query +=" ORDER BY `event_id` DESC";
		return rawQuery(dbRead, query);
	}

	public ArrayList<Long> getEventTimes(long timeFrom){
		ArrayList<Long> eventTimes = new ArrayList<>();
		String query = "SELECT DISTINCT strftime('%s', date(datetime(event_time, 'unixepoch'))) FROM `events` "
				+ " WHERE `event_time` < " + timeFrom / 1000
				+ " AND `event_visiblity` = 1 "
				+ " AND `event_class_id` != 6 "
				+ " AND `event_class_id` != 7 "
				+ " AND `event_class_id` != 15 "
				+ " AND `event_class_id` != 9 "
				+ " AND NOT (`event_class_id`=10 AND `event_reason_id`=4) "
				+ " ORDER BY `event_time` DESC ";
		Cursor c = dbRead.rawQuery(query, null);
		if(c != null && c.moveToFirst()){
			do{
				eventTimes.add(c.getLong(0) * 1000);
			}while(c.moveToNext());
		}

		return eventTimes;
	}

	public Cursor getEventCursorForHistoryLocalTime(int objectId, int filter, long timeFrom, long timeTo)
	{
		String query = "";
		if(objectId != 0){
			query = "SELECT DISTINCT * FROM `events` " +
					"INNER JOIN " +
					"(SELECT DISTINCT `device_device_id`, `device_section_id` FROM `device_site` " +
					"WHERE `device_site_id`=" + Integer.toString(objectId) + ")" +
					" ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section`" +
					" WHERE (`event_local_time` >" + (timeFrom/1000 - TIME_CONST ) + ")&(`event_local_time`<" + (timeTo/1000-TIME_CONST) +
					")&(`event_visiblity`= " + Integer.toString(1) + ")";
		}else{
			query = "SELECT * FROM `events`" +
					" WHERE (`event_local_time` >" + (timeFrom/1000 - TIME_CONST ) + ")&(`event_local_time`<" + (timeTo/1000-TIME_CONST) +
					")&(`event_visiblity`= " + Integer.toString(1) + ")";
		}
		if(filter != -1){
			query += "&(`event_class_id`=" + Integer.toString(filter) + ")";
		}
		query +=" ORDER BY `event_id` DESC";
		return rawQuery(dbRead, query);
	}

	public void updateForceEventsVisiblity(int i)
	{
		execSQL(dbWrite, "UPDATE `events` SET `event_visiblity`=" + Integer.toString(i)
				+ " WHERE (`event_class_id`=" + Integer.toString(0)
				+ ")&(`event_reason_id`=" + Integer.toString(11) +")");
	}

	public String getCurrectDeviceImsi(int device_id)
	{
//		Cursor c = rawQuery(dbRead,  "SELECT `event_comment` FROM `events` WHERE (`event_device`=" + device_id + ")" +
//				"&(`event_class_id`=" + Integer.toString(7) + ")" +
//				"&(`event_reason_id`=" + Integer.toString(6) + ")" +
//				"&(`event_active`=" + Integer.toString(1) + ")");
//		if(null!=c){
//			if(c.getCount() != 0)
//			{
//				c.moveToFirst();
//				String data = c.getString(0);
//				if (data.length() != 0)
//				{
//					c.close();
//					return data;
//				}
//			}
//			c.close();
//		}
		return null;
	}

	private Cursor getCurrentUserId()
	{
		return rawQuery(dbRead, "SELECT `user_id` FROM `user_info`");
	}

	/*
		ALL TABLES
	 */

	public void  saveAllDataInDB(){

		Cursor c = getCurrentUserId();
		if(null!=c){
			c.close();
			Cursor cursor = rawQuery(dbRead, "SELECT * FROM `user_info`");
			if(null!= cursor)
			{
				execSQL(dbWrite, "DELETE FROM `user_info`");
				cursor.close();
			}
		}
		return;
	}

	public void deleteAllDataFromDB(){
		Func.log_d(LOG_TAG, "Delete all data from DB");

		Cursor cursor;
		cursor = rawQuery(dbRead, "SELECT * FROM `events`");
		if(null!= cursor){
			execSQL(dbWrite, "DELETE FROM `events`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `user_info`");
		if(null!= cursor)
		{
			execSQL(dbWrite, "DELETE FROM `user_info`");
			cursor.close();
		}
//		cursor = rawQuery(dbRead, "SELECT * FROM `user_roles`");
//		if(null!= cursor)
//		{
//			execSQL(dbWrite, "DELETE FROM `user_roles`");
//			cursor.close();
//		}
		cursor = rawQuery(dbRead, "SELECT * FROM `sites`");
		if(null!= cursor)
		{
			execSQL(dbWrite, "DELETE FROM `sites`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `devices`");
		if(null!= cursor)
		{
			execSQL(dbWrite, "DELETE FROM `devices`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `device_site`");
		if(null!= cursor)
		{
			execSQL(dbWrite, "DELETE FROM `device_site`");
			cursor.close();
		}
		cursor = rawQuery(dbRead, "SELECT * FROM `zones`");
		if(null!= cursor)
		{
			execSQL(dbWrite, "DELETE FROM `zones`");
			cursor.close();
		}
//		cursor = rawQuery(dbRead, "SELECT * FROM `bar_params`");
//		if(null!= cursor)
//		{
//			cursor.moveToFirst();
//			execSQL(dbWrite, "DELETE FROM `bar_params`");
//			cursor.close();
//		}
		cursor = rawQuery(dbRead, "SELECT * FROM `operators`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `operators`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `clusters`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `clusters`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `commands`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `commands`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `cameras_iv`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `cameras_iv`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `cameras_rtsp`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `cameras_rtsp`");
			cursor.close();
		}

//		cursor = rawQuery(dbRead, "SELECT * FROM `device_site`");
//		if(null!= cursor)
//		{
//			cursor.moveToFirst();
//			execSQL(dbWrite, "DELETE FROM `device_site`");
//			cursor.close();
//		}

		cursor = rawQuery(dbRead, "SELECT * FROM `site_delegates`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `site_delegates`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `scripts`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `scripts`");
			cursor.close();
		}

		cursor = rawQuery(dbRead, "SELECT * FROM `device_scripts`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `device_scripts`");
			cursor.close();
		}
		cursor = rawQuery(dbRead, "SELECT * FROM `main_bars`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `main_bars`");
			cursor.close();
		}
		cursor = rawQuery(dbRead, "SELECT * FROM `section_groups`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `section_groups`");
			cursor.close();
		}
		cursor = rawQuery(dbRead, "SELECT * FROM `section_group_sections`");
		if(null!= cursor)
		{
			cursor.moveToFirst();
			execSQL(dbWrite, "DELETE FROM `section_group_sections`");
			cursor.close();
		}

		//20.10.2020
		execSQL(dbWrite, "DELETE FROM `alarms`");
		execSQL(dbWrite, "DELETE FROM `alarm_events`");
		execSQL(dbWrite, "DELETE FROM `devices_extra`");
	}

	/*
	* OPERATORS
	* */

	public void addOperators(LinkedList<Operator> operators)
	{
		if(getOperatorsCount() > 0){
			UserInfo userInfo = getUserInfo();
			if(null!=userInfo && 0!=userInfo.domain){
				if(null!=operators)
				{
					if (operators.size() > 0)
					{
						updateDomainOperatorsActual(userInfo.domain, 0);
						for (Operator operator : operators)
						{
							Cursor cursor = getOperatorCursorById(operator.id);
							if (null != cursor)
							{
								if (cursor.getCount() > 0)
								{
									updateOperatorFullWithActual(operator, 1);
								} else
								{
									addNewOperator(operator);
								}
								cursor.close();
							}
						}
						deleteOperatorsWithActual(0);
					}else{
						deleteAllDomainOperators(userInfo.domain);
					}
				}
			}
		}else{
			for (Operator operator : operators)
			{
				addNewOperator(operator);
			}
		}
	}

	private void deleteAllDomainOperators(int domain)
	{
		execSQL(dbWrite, "DELETE FROM `operators`");
	}

	private void deleteOperatorsWithActual(int i)
	{
		execSQL(dbWrite, "DELETE FROM `operators` WHERE `operator_actual` !=" + Integer.toString(1));
	}

	private void updateDomainOperatorsActual(int domain, int actual)
	{
		/*domain = 0 update all
		domain = X update X domain only
		0 - not actual
		1 - actual
		* */
		execSQL(dbWrite, "UPDATE `operators` SET `operator_actual`=" + actual +
				" WHERE `operator_domain` =" + domain);
	}

	private long addNewOperator(Operator operator)
	{
		if(execSQL(dbWrite, "INSERT INTO `operators` (`operator_id`, `operator_name`, `operator_login`, " +
						"`operator_contacts`, `operator_active`, " +
						"`operator_domain`, `operator_org`, " +
						"`operator_roles`) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
				new String[]{
						Integer.toString(operator.id),
						operator.name,
						operator.login !=null ?
						operator.login : "",
						operator.contacts != null ? operator.contacts:"",
						Integer.toString(operator.active),
						Integer.toString(operator.domain),
						Integer.toString(operator.org),
						Integer.toString(operator.roles)
				}))
			return lastInsertedRowId(dbWrite);
		return 0;
	}

	public long updateOperator(Operator operator){
		execSQL(dbWrite, "UPDATE `operators` SET `operator_name`='" + String.valueOf(operator.name) + "' WHERE `operator_id`=" + Integer.toString(operator.id));
		return  lastInsertedRowId(dbWrite);
	}

	public long updateOperatorFullWithActual(Operator operator, int actual){

		execSQL(dbWrite, "UPDATE `operators` SET " + "`operator_name`='" + operator.name + "', `operator_login`='" + operator.login + "', `operator_contacts`='" + operator.contacts + "', `operator_active`=" + operator.active + ", `operator_domain`=" + operator.domain + ", `operator_org`=" + operator.org + ", `operator_roles`=" + operator.roles + ", `operator_actual`=" + actual + " WHERE `operator_id`=" + operator.id);
		return  lastInsertedRowId(dbWrite);
	}



	public void addOperatorByReference(Operator operator)
	{
		Cursor c = getOperatorCursorById(operator.id);
		if(c!=null)
		{
			if (c.getCount() != 0)
			{
				updateOperator(operator);
			}else{
				addNewOperator(operator);
			}
			c.close();
		}else{
			addNewOperator(operator);
		}
	}

	public Cursor getOperatorCursorById(int id){
		return rawQuery(dbRead, "SELECT * FROM `operators` WHERE `operator_id`=" + Integer.toString(id));
	}

	public Operator getCurrentOperator(UserInfo userInfo)
	{
		Operator operator = new Operator();
		Cursor c = getOperatorCursorById(userInfo.id);
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				operator = new Operator(c);
				c.close();
				return operator;
			}
		}
		return operator;
	}

	public Operator getOperatorsNoCurent(UserInfo userInfo)
	{
		Operator operator = new Operator();
		Cursor c = getOperatorsCursorNoCurrent(userInfo.userLogin, userInfo.domain);
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				operator = new Operator(c);
				c.close();
				return operator;
			}
		}
		return operator;
	}

//	public Operator getOperatorByCursor(Cursor c)
//	{
//		return new Operator(c.getInt(1),c.getString(2), c.getString(3), c.getString(4), c.getInt(5), c.getInt(6), c.getInt(7), c.getInt(8));
//	}

	public Cursor getOperatorCursorByLogin(String userLogin)
	{
		return rawQuery(dbRead, "SELECT * FROM `operators` WHERE `operator_login` LIKE '"+ userLogin +"'");
	}
	public Cursor getOperatorsCursorNoCurrent(String userLogin, int domain)
	{
		return rawQuery(dbRead, "SELECT * FROM `operators` WHERE `operator_login` " +
				"NOT LIKE '"+ userLogin +"'" +
				" AND `operator_domain`=" + Integer.toString(domain) +
				" ORDER BY CASE WHEN `operator_active`=1 THEN -1 ELSE 0 END, `operator_id`");
	}

	public int getOperatorsCount()
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `operators`");
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				int count = c.getCount();
				c.close();
				return count;
			}
		}
		return 0;
	}

	public String getOperatorNameByEventData(String data)
	{
		int op_id = Integer.valueOf(data);
		if(op_id != -1){
			Cursor cursor = getOperatorNameCursor(op_id);
			if(null!=cursor){
				if(0!=cursor.getCount()){
					cursor.moveToFirst();
					String name= cursor.getString(0);
					cursor.close();
					return " <b>" + name + "</b>";
				}
				cursor.close();
			}
		}
		return context.getString(R.string.UNKNOWN_USER);
	}

	public String getOperatorNameById(int op_id)
	{
		Cursor cursor = getOperatorNameCursor(op_id);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				String name= cursor.getString(0);
				cursor.close();
				return name;
			}
			cursor.close();
		}
		return context.getString(R.string.UNKNOWN_USER);
	}

	public Operator getOperatorById(int op_id)
	{
		Cursor cursor = getOperatorCursorById(op_id);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				Operator operator = new Operator(cursor);
				cursor.close();
				return operator;
			}
			cursor.close();
		}
		return null;
	}


	public Cursor getOperatorNameCursor(int id)
	{
		return rawQuery(dbRead, "SELECT `operator_name` FROM `operators` WHERE `operator_id`=" + Integer.toString(id));
	}

	/*
	* RELE
	* */

	public Cursor getRelesCursorForSite(int siteId)
	{
		return rawQuery(dbRead, "SELECT * FROM `zones` WHERE (`zone_site_id`=" + Integer.toString(siteId) + ")&(`zone_section_id`=" + Integer.toString(0) + ")&" +
				"(`zone_id`>" + Integer.toString(0) + ")");
	}

	public boolean getReleState(int site_id, int section_id, int id)
	{
		Cursor c = getReleStateCursor(site_id, section_id, id);
		if(null!=c){
			if(c.getCount() > 0){
				c.close();
				return true;
			}
			c.close();
		}
		return false;
	}

	private Cursor getReleStateCursor(int site_id, int section_id, int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `affects` WHERE (`site_id`=" + Integer.toString(site_id) +
				")&(`section_id`=" + Integer.toString(section_id) +
				")&(`detector_id`=" + Integer.toString(id) +
				")&(`event_class_id`=" + Integer.toString(5) +
				")&(`event_detector_id`=" + Integer.toString(RELAY_TYPE) +
				")&(`event_reason_id`=" + Integer.toString(6) + ")");
	}

	/*CLUSTERS*/

	public void addCluster(Cluster cluster)
	{
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `clusters` WHERE `cluster_id`=" + Integer.toString(cluster.id));
		if(null!=cursor){
			if(0!= cursor.getCount()){
				cursor.close();
				execSQL(dbWrite, "UPDATE `clusters` SET `cluster_system`=" + Integer.toString(cluster.system) +
				", `cluster_cluster`=" + Integer.toString(cluster.cluster) + 
				", `cluster_name`='" + String.valueOf(cluster.name) + "' WHERE `cluster_id`=" + Integer.toString(cluster.id));
				return;
			}
			cursor.close();
		}
		execSQL(dbWrite, "INSERT INTO `clusters` (`cluster_id`, `cluster_system`, `cluster_system`, `cluster_name`) VALUES (?, ?, ?, ?)",
					new String[]{Integer.toString(cluster.id),
							Integer.toString(cluster.system),
							Integer.toString(cluster.cluster),
							cluster.name});
	}


	public Cluster[] getClusters()
	{
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `clusters`");
		if(null!=cursor){
			int count = cursor.getCount();
			if(count != 0){
				Cluster[] clusters = new Cluster[count];
				cursor.moveToFirst();
				int i = 0;
				clusters[i] = new Cluster(cursor);
				while (cursor.moveToNext()){
					i++;
					clusters[i] = new Cluster(cursor);
				}
				cursor.close();
				return clusters;
			}
			cursor.close();
		}
		return null;
	}

	//TODO костыль маскировка всех кластеров кроме СХ для локализаций
	public Cluster[] getFilteredClusters()
	{
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `clusters` WHERE `cluster_id`=" + Integer.toString(1) + " LIMIT 1");
		if(null!=cursor){
			int count = cursor.getCount();
			if(count != 0){
				Cluster[] clusters = new Cluster[count];
				cursor.moveToFirst();
				int i = 0;
				clusters[i] = new Cluster(cursor);
				//TODO костыль на переименование кластера без русского языка
				clusters[i].name = "Security Hub";
				while (cursor.moveToNext()){
					i++;
					clusters[i] = new Cluster(cursor);
					clusters[i].name = "Security Hub";
				}
				cursor.close();
				return clusters;
			}
			cursor.close();
		}
		return null;
	}

	public int getClustersCount()
	{
		Cursor c = rawQuery(dbRead, "SELECT COUNT (`cluster_id`) FROM `clusters`");
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				int count = c.getInt(0);
				c.close();
				return count;
			}
			c.close();
		}
		return 0;
	}

	public void deleteAllClusters()
	{
		execSQL(dbWrite, "DELETE FROM `clusters`");
	}

	/*CAMERAS*/

	public Cursor getIVCamerasCursor(){
		return  rawQuery(dbRead, "SELECT * FROM `cameras_iv`");
	}

	public Camera[] getAllCameras(){
		Cursor c = getIVCamerasCursor();
		LinkedList<Camera> camerasList = new LinkedList<>();
		Camera[] cameras;
		int i = 0;
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				camerasList.add(new Camera(c));
				while (c.moveToNext()){
					camerasList.add(new Camera(c));
				}
				c.close();
			}
		}
		Cursor c1 = getRTSPCamerasCursor();
		if(null!=c1){
			if(0!=c1.getCount()){
				c1.moveToFirst();
				camerasList.add(new Camera(c1.getInt(0), c1.getString(1), c1.getString(2)));
				while (c1.moveToNext()){
					camerasList.add(new Camera(c1.getInt(0), c1.getString(1), c1.getString(2)));
				}
				c1.close();
			}
		}

		Cursor c2 = getDahuaCamerasCursor();
		if(null!=c2){
			if(0!=c2.getCount()){
				c2.moveToFirst();
				camerasList.add(new Camera(c2, Camera.CType.DAHUA));
				while (c2.moveToNext()){
					camerasList.add(new Camera(c2, Camera.CType.DAHUA));
				}
				c2.close();
			}
		}

		if(null!=camerasList && camerasList.size() > 0)
			return camerasList.toArray(new Camera[camerasList.size()]);
		return null;
	}

	public Camera[] getAllIVCamerasForSite(int site_id){
		Cursor c = getIVCamerasCursor();
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				int count = c.getCount();
				int i = 0;
				Camera[] cameras = new Camera[count];
				cameras[i] = new Camera(c);
				while (c.moveToNext()){
					i++;
					cameras[i] = new Camera(c);
				}
				c.close();
				return cameras;
			}
		}
		return null;
	}

	public void updateIVCamerasActual(int actual){
		execSQL(dbWrite, "UPDATE `cameras_iv` SET `actual`=" + Integer.toString(actual));
	}

	public void deleteNotActualIVCameras(){
		execSQL(dbWrite, "DELETE FROM `cameras_iv` WHERE `actual`=0");
	}

	public void addNewIVCamera(Camera camera){
		execSQL(dbWrite, "INSERT INTO `cameras_iv` (`iv_id`, `iv_name`,`camera_status`, `camera_quality`, `camera_audio`, `actual`) VALUES (? ,? ,?, ?, ?, ?)",
		new String[]{
				camera.id,
				camera.name,
				Integer.toString(camera.status),
				Integer.toString(camera.quality),
				camera.audio,
				Integer.toString(1)
		});

	}

	private void updateIVCamera(Camera camera)
	{
		execSQL(dbWrite, "UPDATE `cameras_iv` SET `iv_name`='" + String.valueOf(camera.name) + "'," +
		"`camera_status`=" + Integer.toString(camera.status) +
		", `actual`=1" +
		" WHERE `iv_id`='" + String.valueOf(camera.id) + "'");
	}

	public int getCameraQuality(String camera_id){
		Camera camera = getIVCameraById(camera_id);
		return camera.quality;
	}

	public void setIVCameraQuality(String camera_id, String value)
	{
		execSQL(dbWrite, "UPDATE `cameras_iv` SET `camera_quality`="
		+ value
		+ " WHERE `iv_id`='" + String.valueOf(camera_id) + "'");
	}

	public void setIVCameraStreamSound(String camera_id, boolean b)
	{
		execSQL(dbWrite, "UPDATE `cameras_iv` SET `camera_audio`='"
				+ String.valueOf(b? "both" : "image")
				+ "' WHERE `iv_id`='" + String.valueOf(camera_id) + "'");
	}

	public void addIVCamera(Camera camera){
		Cursor c  = getIVCameraCursorById(camera.id);
		if (null != c){
			c.close();
			updateIVCamera(camera);
		}else{
			addNewIVCamera(camera);
		}
	}

	private Cursor getIVCameraCursorById(String id)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `cameras_iv` WHERE `iv_id`='" + String.valueOf(id) + "'");
		if((null!=c) && (c.getCount()!=0))
		{
			return  c;
		}
		return null;
	}

	public Camera getIVCameraByCursor(Cursor cursor)
	{
		return new Camera(cursor);
	}

	public Camera getIVCameraById(String id){
		Cursor c = getIVCameraCursorById(id);
		if(null!=c &&c.getCount() != 0){
			c.moveToFirst();
			Camera camera =  getIVCameraByCursor(c);
			c.close();
			return camera;
		}
		return null;
	}

	public Zone getZoneByIVCameraId(String cameraId)
	{
		Cursor c = getZonesCursorByIVCameraId(cameraId);
		if(c!=null){
//			c.moveToFirst();
			Zone zone = new Zone(c);
			c.close();
			return zone;
		}
		return null;
	}

	/*changed*/
	public Zone[] getZonesByIVCameraId(String cameraId){
		Cursor c = getZonesCursorByIVCameraId(cameraId);
		if(c!=null){
			if(c.getCount() > 0)
			{
				Zone[] zones = new Zone[c.getCount()];
				c.moveToFirst();
				for (int i = 0; i < c.getCount(); i++)
				{
					zones[i] = getZone(c);
					c.moveToNext();
				}
				c.close();
				return zones;
			}else
			{
				c.close();
			}
		}
		return null;
	}

	/*changed*/
	public Cursor getZonesCursorByIVCameraId(String cameraId)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `zones` WHERE `zone_camera`='" +  cameraId + "'" );
		if(null!=c){
			if(0!=c.getCount()){
				return  c;
			}
			c.close();
		}
		return null;
	}

	/*changed*/
	public void resetCameraBindingForAllZones()
	{
		execSQL(dbWrite, "UPDATE `zones` SET `zone_camera` = '" + String.valueOf("") + "'");
	}

	/*changed*/
	public void updateCameraBindingForZone(int device, int section, int zone, String camera){
		execSQL(dbWrite, "UPDATE `zones` SET `zone_camera` = '" + String.valueOf(camera) +
				"' WHERE (`zone_id`=" + Integer.toString(zone) +
				")&(`zone_section_id`=" + Integer.toString(section) +
				")&(`zone_device_id`=" + Integer.toString(device) + ")");
	}

	/*changed*/
	public void deleteCameraBindingForZone(int device, int section, int zone)
	{
		execSQL(dbWrite, "UPDATE `zones` SET `zone_camera` = '" + String.valueOf("")
				+ "' WHERE (`zone_device_id`=" + Integer.toString(device)
				+")&(`zone_section_id`=" + Integer.toString(section)
				+")&(`zone_id`=" + Integer.toString(zone) + ")");
	}

	public void deleteAllCameraBindings(){
		execSQL(dbWrite, "UPDATE `zones` SET `zone_camera`='" + String.valueOf("") + "'");
	}

	public void deleteAllCameras(){
		execSQL(dbWrite, "DELETE FROM `cameras_iv`");
	}

	/*changed*/
	public Cursor getRecordsCursorForCamera(String id)
	{
		Zone[] zones = getZonesByIVCameraId(id);
		if(null!= zones)
		{
			String cSite = "SELECT * FROM `events` WHERE (";
			for(Zone zone : zones){
				cSite += "((`event_device` = " + Integer.toString(zone.device_id) +
				")&(`event_section`=" + Integer.toString(zone.section_id) +
				")&(`event_zone`=" + Integer.toString(zone.id) + "))OR";
			};
			cSite = cSite.substring(0, cSite.length()-2);
			cSite+= ")&";
			String cClass = "((`event_class_id`=" + Integer.toString(0) +
					") OR (`event_class_id`=" + Integer.toString(1) +
					") OR (`event_class_id`=" + Integer.toString(2) +
					"))" +
					"&(`event_jdata` NOT LIKE ''"  +
					") ORDER BY `event_id` DESC";
			Cursor c = rawQuery(dbRead, cSite + cClass);
			if(null!=c && c.getCount()!=0){
				return c;
			}
		}
		return null;
	}

	/*changed*/
	public void updateEventJdata(String recordData)
	{
		try
		{
			JSONObject jsonObject = new JSONObject(recordData);
			int id = jsonObject.getInt("event_id");
			if(id > 0){
				execSQL(dbWrite, "UPDATE `events` SET `event_jdata`='" + String.valueOf(recordData) + "' WHERE `event_id`=" + Integer.toString(id));
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	/*changed*/
	public void setCameraName(String name, String camera_id)
	{
		execSQL(dbWrite, "UPDATE `cameras_iv` SET `iv_name`='"
				+ String.valueOf(name)
				+ "' WHERE `iv_id`='" + String.valueOf(camera_id) + "'");
	}



	/*DEVICE_SITE */

//	public void addOrUpdateDelegationStatusForSite(int deviceId, int site, int delegated){
//		Cursor c = getDelegationStatusCursorForSite(site);
//		if(c!=null && c.getCount() !=0){
//			updateDelegationStatusForSite(site, delegated);
//		}else{
//			addNewDelegationStatusForSite(deviceId, site, delegated);
//		}
//	}
//
//	public void addNewDelegationStatusForSite(int device, int site, int delegated)
//	{
//		execSQL(dbWrite, "INSERT INTO `device_site` (`device_id`, `site_id`, `delegated`) VALUES (?, ?, ?)",
//				new String[]{
//						Integer.toString(device),
//						Integer.toString(site),
//						Integer.toString(delegated)
//				});
//	}

	public void addSiteDeviceSection(int site_id, int device_id, int section_id, int device_parted){
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `device_site` " +
		"WHERE (`device_device_id`=" + Integer.toString(device_id) +
		")&(`device_site_id`=" + Integer.toString(site_id) +
		")&(`device_section_id`=" + Integer.toString(section_id) + ")");
		if(null!=cursor){
			if(0!=cursor.getCount()){
				updateSiteDeviceSectionActual(site_id, device_id, section_id, 1, device_parted);
			}else{
				addNewSiteDeviceSection(site_id, device_id, section_id, device_parted);
			}
			cursor.close();
		}
	}

	public void addNewSiteDeviceSection(int site_id, int device_id, int section_id, int device_parted){
		execSQL(dbWrite, "INSERT INTO `device_site` " +
				"(`device_site_id`, `device_device_id`, `device_section_id`, `device_site_actual`, `device_site_parted`) VALUES (?, ? , ?, ?, ?)",
				new String[]{
					Integer.toString(site_id),
						Integer.toString(device_id),
						Integer.toString(section_id),
						Integer.toString(1),
						Integer.toString(device_parted)
				});
	}

	public void updateDelegationStatusForSite(int siteId, int delegated){
		execSQL(dbWrite, "UPDATE `sites` SET `site_delegated`=" + Integer.toString(delegated)  + " WHERE `site_id`=" + Integer.toString(siteId));
	}

	public int getDelegationStatusForSite(int siteId){
		Cursor c = getDelegationStatusCursorForSite(siteId);
		if(c!=null){
			if(c.getCount()!= 0){
				c.moveToFirst();
				int result = c.getInt(0);
				c.close();
				return result;
			}
			c.close();
		}
		return 0;
	}

	private Cursor getDelegationStatusCursorForSite(int siteId)
	{
		return rawQuery(dbRead,  "SELECT `site_delegated` FROM `sites` WHERE `site_id`=" + Integer.toString(siteId));
	}


//	public void resetAllDeviceSiteRelations()
//	{
//		execSQL(dbWrite, "DELETE FROM `device_site`");
//	}


	/*SITE DELEGATES*/

	public void resetAllDelegateToForSite(int site_id)
	{
		execSQL(dbWrite, "DELETE FROM `site_delegates` WHERE `site_id`=" + Integer.toString(site_id));

	}


	public long addDelegateToForSite(int site, int domain, String domain_name, int actual, int permissions)
	{
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `site_delegates` WHERE `site_id`=" +Integer.toString(site)
				+ " AND `domain`=" + Integer.toString(domain));
		if(null!=cursor){
			cursor.moveToFirst();
			if(0!=cursor.getCount()){
				execSQL(dbWrite, "UPDATE `site_delegates` " +
						"SET `actual`=" + Integer.toString(actual)
						+ ", `permissions`=" + Integer.toString(permissions)
						+ ", `domain_name`='" + String.valueOf(domain_name) + "'"
						+ " WHERE `site_id`=" + Integer.toString(site) +
				 " AND `domain`=" + Integer.toString(domain));
				cursor.close();
				return lastInsertedRowId(dbWrite);
			}
			cursor.close();
		}
		execSQL(dbWrite, "INSERT INTO `site_delegates` (`site_id`, `domain`, `actual`, `permissions`) VALUES (?, ?, ?, ?)", new String[]{
				Integer.toString(site),
				Integer.toString(domain),
				Integer.toString(actual),
				Integer.toString(permissions)
		});
		return lastInsertedRowId(dbWrite);
	}

	public void updateAllSiteDelegatesActual(int actual){
		execSQL(dbWrite, "UPDATE `site_delegates` SET  `actual`=" + Integer.toString(actual));
	}

	public void updateSitesDelegatesActual(int site_id, int actual){
		execSQL(dbWrite, "UPDATE `site_delegates` SET  `actual`=" + Integer.toString(actual) + " WHERE `site_id`=" + Integer.toString(site_id));
	}

	public void deleteAllSiteDelegatesWithActual(int actual){
		execSQL(dbWrite, "DELETE FROM `site_delegates` WHERE `actual`=" + Integer.toString(actual));
	}

	public Cursor getDelegateToDomainCursorForSite(int site_id){
		return  rawQuery(dbRead,"SELECT * FROM `site_delegates` WHERE `site_id`=" + Integer.toString(site_id));
	}

	public void resetAllDelegateTo()
	{
		execSQL(dbWrite, "DELETE FROM `site_delegates`");
	}

	/*LOCALES*/

	public String getDomainLocale()
	{
		return "ru";
	}

	public void deleteLocales()
	{
		execSQL(dbWrite, "DELETE FROM `locales`");
	}

	public void addNewLocale(LocaleD3 localeD3)
	{
		execSQL(dbWrite, "INSERT INTO `locales` (`locale_id`, `iso`, `description`) VALUES (?, ?, ?)",
				new String[]{
				Integer.toString(localeD3.id),
				localeD3.iso,
				localeD3.description});
	}

	public LocaleD3 getLocale(String lang)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `locales` WHERE `iso` LIKE '" + lang + "'" );
		if(c!=null && 0!= c.getCount()){
			c.moveToFirst();
			LocaleD3 locale = new LocaleD3(c.getInt(1), c.getString(2), c.getString(3));
			c.close();
			return 	locale;
		};
		return null;
	}

	public LocaleD3[] getLocales()
	{
		Cursor c = getLocalesCursor();
		if(null!=c){
			c.moveToFirst();
			if(0 != c.getCount()){
				int count  = c.getCount();
				LocaleD3[] localeD3s = new LocaleD3[count];
				for(int i = 0; i < count; i++){
					localeD3s[i] = new LocaleD3(c.getInt(1), c.getString(2), c.getString(3));
					c.moveToNext();
				}
				c.close();
				return localeD3s;
			}
			c.close();
		}
		return null;
	}

	public Cursor getLocalesCursor()
	{
		return rawQuery(dbRead, "SELECT * FROM `locales`");
	}

	/*29.08.2018*/

	/*TODO !WARNING! при получении нового раздела в данный момент он привязывается ко всем объектам к которым привязано устройство
	* надо что-то придумать для разделенных устройств*/
	public void addDeviceSite(Zone zone)
	{
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `device_site`" +
				" WHERE `device_device_id`=" + Integer.toString(zone.device_id) +
		" AND `device_section_id`=" + Integer.toString(zone.section_id));
		if(null!=cursor){
			if(cursor.getCount()==0)
			{
				Cursor c = rawQuery(dbRead, "SELECT DISTINCT " +
						"`device_site_id` FROM `device_site` " +
						"WHERE `device_device_id`=" + Integer.toString(zone.device_id));
//						+
//				" AND `device_site_parted`=" + Integer.toString(0));
				if(c!=null){
					if(c.getCount()!=0){
						c.moveToFirst();
						int site_id = c.getInt(0);
						execSQL(dbWrite, "INSERT INTO `device_site` (`device_device_id`, `device_site_id`, `device_section_id`, `device_site_actual`, `device_site_parted`) VALUES (?, ?, ?, ?, ?)",
								new String[]{
								Integer.toString(zone.device_id),
										Integer.toString(site_id),
										Integer.toString(zone.section_id),
										Integer.toString(1),
										Integer.toString(0)
								});
					}
					c.close();
				}
			}
			cursor.close();
		}
	}

//	public int getPartedStatusForDevice(int site_id, int device_id)
//	{
//		Cursor cursor = rawQuery(dbRead, "SELECT `device_site_parted` FROM `device_site` WHERE `device_device_id` =" + Integer.toString(device_id) +
//		" AND `device_site_id`=" + Integer.toString(site_id) + " AND `device_section_id`=" + Integer.toString(0));
//		if(null!=cursor){
//			if(0 != cursor.getCount()){
//				cursor.moveToFirst();
//				int status = cursor.getInt(0);
//				cursor.close();
//				return status;
//			}
//		}
//		return 0;
//	}

	public int getZonesCountForSite(int site_id)
	{
		int count = 0;
		Cursor c =  rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` INNER JOIN(SELECT DISTINCT `device_device_id`, `device_section_id`" +
				" FROM `device_site` WHERE `device_site_id`=" + Integer.toString(site_id) +
				" ) ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id` " +
				"WHERE `zone_id`>" + Integer.toString(0) + " AND `zone_section_id`>" + Integer.toString(0));
		if(null!=c){
			c.moveToFirst();
			count = c.getInt(0);
			c.close();
		}
		return count;
	}

	public int getZonesCountForDevice(int device_id)
	{
		int count = 0;
		Cursor c =  rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` " +
				"WHERE `zone_id`>" + Integer.toString(0) + " AND `zone_section_id`>" + Integer.toString(0) +
				" AND `zone_device_id`=" + Integer.toString(device_id)
		);
		if(null!=c){
			c.moveToFirst();
			count = c.getInt(0);
			c.close();
		}
		return count;
	}

	public int getZonesCountSection(int device_id, int section_id)
	{
		int count = 0;
		Cursor c =  rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` " +
				"WHERE `zone_id`>" + Integer.toString(0) + " AND `zone_section_id`=" + Integer.toString(section_id) +
				" AND `zone_device_id`=" + Integer.toString(device_id)
		);
		if(null!=c){
			c.moveToFirst();
			count = c.getInt(0);
			c.close();
		}
		return count;
	}

	public boolean getDevicePartedStatusForSite(int site_id, int device_id){
		Cursor c = rawQuery(dbRead, "SELECT DISTINCT * FROM `device_site` WHERE `device_device_id` =" + Integer.toString(device_id) +
		" AND `device_site_id`=" + Integer.toString(site_id) + " AND `device_site_parted`=" + Integer.toString(1));
		if(null!=c){
			if(c.getCount() > 0){
				c.close();
				return true;
			}
		}
		return false;
	}

	/*ARM&DISARM*/

	public void updateSectionArmStatus(int device_id, int section_id, int armed, long time)
	{
		execSQL(dbWrite,"UPDATE `zones` SET `zone_armed`=" + (armed == 1 ? time :0) +
				" WHERE `zone_device_id`=" + Integer.toString(device_id) +
				" AND `zone_section_id`=" + Integer.toString(section_id)
		);
	}

	public int getDeviceArmStatus(int device_id){
		Cursor c = rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` WHERE `zone_device_id`=" + Integer.toString( device_id) +
				" AND `zone_id`=0 AND `zone_section_id`> 0 AND `zone_detector`=1 AND `zone_armed`>0");
		Cursor cAll = rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` WHERE `zone_device_id`=" + Integer.toString( device_id) +
				" AND `zone_id`=0 AND `zone_section_id`> 0 AND `zone_detector`=1");
		if(null!=c&&null!=cAll){
			if(cAll.getCount() != 0 && c.getCount() !=0){
				cAll.moveToFirst();
				c.moveToFirst();

				int armedCount = c.getInt(0);
				int allCount = cAll.getInt(0);
				c.close();
				cAll.close();
				if(armedCount > 0){
					if(allCount == armedCount){
						return Const.STATUS_ARMED;
					}
					return Const.STATUS_PARTLY_ARMED;
				}
			}
			c.close();
			cAll.close();
		}
		return Const.STATUS_DISARMED;
	}

	public int getSiteDeviceArmStatus(int site_id, int device_id){
		Cursor cursor = rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` INNER JOIN (SELECT `device_device_id`, `device_section_id` " +
				"FROM `device_site`" +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + " " +
				"AND `device_device_id`=" + Integer.toString(device_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id` " +
				"WHERE `zone_detector`=" + Integer.toString(1) +
				" AND `zone_armed`>" + Integer.toString(0) +
				" AND `zone_id`=" + Integer.toString(0)
		);
		Cursor cursorAll = rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` " +
				"INNER JOIN " +
				"(SELECT `device_device_id`, `device_section_id` " +
				"FROM `device_site`" +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + " " +
				"AND `device_device_id`=" + Integer.toString(device_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id` " +
				"WHERE `zone_detector`=" + Integer.toString(1) +
				" AND `zone_id`=" + Integer.toString(0)
		);
		if(null!=cursor && null!=cursorAll){
			if(cursor.getCount() != 0 && cursorAll.getCount()!=0){
				cursor.moveToFirst();
				int armedCount = cursor.getInt(0);
				cursorAll.moveToFirst();
				int allCount = cursorAll.getInt(0);
				cursor.close();
				cursorAll.close();
				if(armedCount > 0){
					if(armedCount == allCount){
						return 1;
					}else{
						return 2;
					}
				}
			}
			cursor.close();
			cursorAll.close();
			return 0;
		}else{
			return 0;
		}
	}

	public int getSiteArmStatus(int site_id){
		Cursor cursor = rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` INNER JOIN (SELECT `device_device_id`, `device_section_id` " +
				"FROM `device_site`" +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id` " +
				"WHERE `zone_detector`=" + Integer.toString(1) +
				" AND `zone_armed`>" + Integer.toString(0) +
				" AND `zone_id`=" + Integer.toString(0)
		);
		Cursor cursorAll = rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` INNER JOIN (SELECT `device_device_id`, `device_section_id` " +
				"FROM `device_site`" +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + " ) " +
				"ON `device_device_id`=`zones`.`zone_device_id` AND `device_section_id`=`zones`.`zone_section_id` " +
				"WHERE `zone_detector`=" + Integer.toString(1) +
				" AND `zone_id`=" + Integer.toString(0)
		);
		if(null!=cursor && null!=cursorAll){
			if(cursor.getCount() != 0 && cursorAll.getCount()!=0){
				cursor.moveToFirst();
				int armedCount = cursor.getInt(0);
				cursorAll.moveToFirst();
				int allCount = cursorAll.getInt(0);
				cursor.close();
				cursorAll.close();
				if(armedCount > 0){
					if(armedCount == allCount){
						return Const.STATUS_ARMED;
					}else{
						return Const.STATUS_PARTLY_ARMED;
					}
				}
			}
			cursor.close();
			cursorAll.close();
		}
		return Const.STATUS_DISARMED;
	}

	/*checkStates and use only if device inside site is only one*/
	public boolean getSiteConnectionStatus(int site_id)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` " +
				"INNER JOIN  (SELECT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section` " +
				"WHERE" +
				"(`section_id`="  + Integer.toString(0) + ")&" +
				"(`detector_id`=" + Integer.toString(0) + ")&" +
				"(`event_class_id`=" + Integer.toString(2) + ")&" +
				"(`event_reason_id`=" + Integer.toString(3) + ")");
		if(null!=c){
			if(0!=c.getCount()){
				c.close();
				return false;
			}
			c.close();
			return true;
		}
		return false;
	}

	public boolean getZoneConnectionStatus(int device_id, int section_id, int zone_id)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` " +
				"WHERE" +
				"(`event_device`=" + Integer.toString(device_id) +
				")&(" +
				"((`event_section`="  + Integer.toString(0) + ")&" +
				"(`event_zone`="  + Integer.toString(0) + "))" +
				"OR((`event_section`=" + Integer.toString(section_id) +
				")&(`event_zone`=" + Integer.toString(zone_id) + "))" +
				")&" +
				"((`event_class_id`=" + Integer.toString(2) + ")&" +
				"(`event_reason_id`=" + Integer.toString(3) + ")&" +
				"(`event_affect`=" + Integer.toString(1) + "))");
		if(null!=c){
			if(0!=c.getCount()){
				c.close();
				return false;
			}
			c.close();
			return true;
		}
		return true;
	}

	public boolean isDeviceOnline(int device_id)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` " +
				"WHERE" +
				"(`event_device`=" + Integer.toString(device_id) + ")&" +
				"(`event_section`="  + Integer.toString(0) + ")&" +
				"(`event_zone`="  + Integer.toString(0) + ")&" +
				"(`event_class_id`=" + Integer.toString(2) + ")&" +
				"(`event_reason_id`=" + Integer.toString(3) + ")&" +
				"(`event_affect`=" + Integer.toString(1) + ")");
		if(null!=c){
			if(0!=c.getCount()){
				c.close();
				return false;
			}
			c.close();
			return true;
		}
		return true;
	}

	/*03.07.2018*/
	public boolean getSiteDeviceConnectionStatusOld(int site_id, int device_id)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` " +
				"INNER JOIN  (SELECT `device_device_id`, `device_section_id` FROM `device_site` " +
				"WHERE `device_site_id`=" + Integer.toString(site_id) + ") " +
				"ON `device_device_id`=`events`.`event_device` AND `device_section_id`=`events`.`event_section` " +
				"WHERE" +
				"(`event_device`=" + Integer.toString(device_id) + ")&" +
				"(`section_id`="  + Integer.toString(0) + ")&" +
				"(`detector_id`=" + Integer.toString(0) + ")&" +
				"(`event_class_id`=" + Integer.toString(2) + ")&" +
				"(`event_reason_id`=" + Integer.toString(3) + ")&" +
				"(`event_affect`=" + Integer.toString(1) + ")");
		if(null!=c){
			if(0!=c.getCount()){
				c.close();
				return false;
			}
			c.close();
			return true;
		}
		return false;
	}


	public boolean getDeviceInteractiveStatus(int setDeviceId)
	{
		Cursor cursor = rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_device` =" + Integer.toString(setDeviceId) +
		" AND `event_class_id`=5" +
		" AND `event_reason_id`=23" +
		" AND `event_active`=1" +
		" AND `event_affect`=1");
		if(null!=cursor){
			if(0 != cursor.getCount()){
				cursor.close();
				return true;
			}
		}

		return false;
	}


	public Event[] nGetCritEvents(long minTime, int site){
		Cursor c = nGetAttentionEventsCursor(minTime, site);
		if(null!=c){
			int count = c.getCount();
			if(0!=count){
				c.moveToFirst();
				Event[] events = new Event[count];
				for(int i =0; i< count; i++){
					events[i] = new Event(c);
					c.moveToNext();
				}
				c.close();
				return events;
			}
			c.close();
		}
		return null;
	}

	public Cursor nGetAlarmEventsCursor(long minTime, int currentSite)
	{
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `events` " +
				"LEFT JOIN (SELECT `device_device_id`, `device_site_id` FROM `device_site`) " +
				"ON `device_device_id`=`event_device` " +
				"WHERE `event_time`>" + Long.toString(minTime)
				+ " AND `event_affect`=1"
				+ " AND `event_active`=1"
				+ " AND (`event_class_id`=0)"
				+ " ORDER BY CASE WHEN `event_class_id`=0 THEN -1 ELSE 0 END, " +
				"CASE WHEN `device_site_id`=" + Integer.toString(currentSite) + " THEN -1 ELSE `device_site_id` END," +
				"`event_class_id`," +
				"`event_id` DESC"
		);
	}

	public Cursor nGetAttentionEventsCursor(long minTime, int currentSite)
	{
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `events` " +
				"LEFT JOIN (SELECT `device_device_id`, `device_site_id` FROM `device_site`) " +
				"ON `device_device_id`=`event_device` " +
				"WHERE `event_time`>" + Long.toString(minTime)
				+ " AND `event_affect`=1"
				+ " AND `event_active`=1"
				+ " AND (`event_class_id`=1 OR `event_class_id`=2 OR `event_class_id`=3)"
				+ " ORDER BY CASE WHEN `event_class_id`=0 THEN -1 ELSE 0 END," +
				"CASE WHEN `device_site_id`=" + Integer.toString(currentSite) + " THEN -1 ELSE `device_site_id` END," +
				"`event_class_id`," +
				"`event_id` DESC"
		);
	}

	public Cursor nGetCritEventsCursor(long minTime, int currentSite)
	{
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `events` " +
				"LEFT JOIN (SELECT `device_device_id`, `device_site_id` FROM `device_site`) " +
				"ON `device_device_id`=`event_device` " +
				"WHERE `event_time`>" + Long.toString(minTime)
				+ " AND `event_affect`=1"
				+ " AND `event_active`=1"
				+ " AND (`event_class_id`=0 OR `event_class_id`=1 OR `event_class_id`=2 OR `event_class_id`=3)"
				+ " ORDER BY CASE WHEN `event_class_id`=0 THEN -1 ELSE 0 END, " +
				"CASE WHEN `device_site_id`=" + Integer.toString(currentSite) + " THEN -1 ELSE `device_site_id` END," +
				"`event_class_id`," +
				"`event_id` DESC"
		);
	}

	public Cursor nGetCritEventsCursorAMMode(long minTime, int currentSite)
	{
		return rawQuery(dbRead, "SELECT *" +
//				"DISTINCT (`_id`, `event_id`, `event_device`," +
//				"`event_section`,`event_zone`, `event_active`, `event_class_id`, " +
//				"`event_detector_id`, `event_reason_id`, `event_time`, `event_local_time`, `event_channel`, `event_review_status`, `event_comment`, " +
//				"`event_visiblity`, `event_affect`, `event_jdata`, `affect_visiblity`)" +
				" FROM `events` " +
				"WHERE `event_time`>" + Long.toString(minTime)
				+ " AND `event_affect`=1"
				+ " AND `event_active`=1"
				+ " AND `event_class_id`=3"
				+ " ORDER BY CASE WHEN `event_class_id`=0 THEN -1 ELSE 0 END, " +
				"`event_class_id`," +
				"`event_id` DESC"
		);
	}

	public Cursor getNewActiveAlarmEvents(long minTime)
	{
		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_time`>" + Long.toString(minTime)
				+ " AND `event_affect`=1"
				+ " AND `event_active`=1"
				+ " AND `event_class_id`=0"
				+ " ORDER BY `event_id` DESC"
		);
	}

	public Cursor getNewActiveAttentionEvents(long minTime)
	{
		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_time`>" + Long.toString(minTime)
				+ " AND `event_affect`=1"
				+ " AND `event_active`=1"
				+ " AND `event_class_id`<4"
				+ " AND NOT(`event_class_id`=3 AND  `event_reason_id`=5)"
				+ " ORDER BY `event_id` DESC"
		);
	}

	public Cursor getNewRepairedAttentionEvents(long minTime)
	{
		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_time`>" + Long.toString(minTime)
				+ " AND `event_affect`=0"
				+ " AND `event_active`=1"
				+ " AND `event_class_id`<4"
				+ " AND NOT(`event_class_id`=3 AND `event_reason_id`=5)"
				+ " ORDER BY `event_id` DESC"
		);
	}


	public Cursor getNewActiveLostEvents()
	{
		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_affect`=" + Integer.toString(1)
				+ " AND `event_class_id`=2"
				+ " AND `event_reason_id`=51"
				+ " ORDER BY `event_id` DESC"
		);
	}


	public Event getRepairEventForAlarm(Event event)
	{
		Cursor c = rawQuery(dbRead, "SELECT MIN(`event_id`) FROM `events` WHERE `event_id`>"
				+ event.id
				+ " AND `event_device`=" + event.device
				+ " AND `event_section`=" + event.section
				+ " AND `event_zone`=" + event.zone
				+ " AND `event_class_id`=" + event.classId
				+ " AND `event_detector_id`=" + event.detectorId
				+ " AND `event_reason_id`=" + event.reasonId
		);
		if(null!=c){
			if(0 != c.getCount())
			{
				c.moveToFirst();
				int event_id = c.getInt(0);
				Event repairEvent = getEventById(event_id);
				if (null != repairEvent && repairEvent.active == 0)
				{
					return repairEvent;
				}
				c.close();
			}
		}
		return null;
	}

	//SCRIPTS

	//DEVICE_SCRIPTS

	public long addDeviceScripts(int device_id, Script[] scripts){
		Cursor cursor = getAllDevicesScriptsCursor();
		if(null!=cursor){
			if(0!=cursor.getCount()){
				for (Script script:scripts){
					updateDeviceScriptInfoAndActual(script, 0);
				}
				deleteDeviceScriptsWithActual(device_id, 1);
				updateAllDeviceScriptsActual(1);
				for(Script script:scripts){
					Cursor c = getDeviceScriptCursorByScriptId(script.id);
					if(null!=c){
						if(0 == c.getCount()){
							addNewDeviceScript(script);
						}
						c.close();
					}
				}
				cursor.close();
				return lastInsertedRowId(dbWrite);
			}else{
				for(Script script:scripts){
					addNewDeviceScript(script);
				}
			}
			cursor.close();
		}
		return lastInsertedRowId(dbWrite);
	}

	private void deleteDeviceScriptsWithActual(int device_id, int actual)
	{
		execSQL(dbWrite, "DELETE FROM `device_scripts` WHERE `actual`=" + actual + " AND `device`=" + device_id);
	}

	private void updateAllDeviceScriptsActual(int actual)
	{
		execSQL(dbWrite, "UPDATE `device_scripts` SET `actual`=" + Integer.toString(actual));
	}

	private void updateDeviceScriptInfoAndActual(Script script, int actual)
	{
		execSQL(dbWrite, "UPDATE `device_scripts` SET " +
				"`script_uid`=" + dbString(script.uid) +
				", `device`=" + Integer.toString(script.device) +
				", `bind`=" + Integer.toString(script.bind) +
				", `name`=" + dbString(script.name) +
				", `program`=" + dbString(script.program) +
				", `compile_program`=" + dbString(script.compileProgram) +
				", `params`=" + dbString(script.params) +
				", `extra`=" + dbString(script.extra) +
				", `enabled`=" + Integer.toString(script.enabled)+
				", `actual`=" + Integer.toString(actual) +
				" WHERE `script_id`=" + script.id);
	}

	private Cursor getAllDevicesScriptsCursor()
	{
		return rawQuery(dbRead, "SELECT * FROM `device_scripts` ORDER BY `script_id` DESC"
		);
	}

	private Cursor getDeviceScriptCursorByScriptId(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `device_scripts` WHERE `script_id`=" + Integer.toString(id));
	}

	public Script getDeviceScriptByScriptId(int scriptId)
	{
		Cursor cursor = getDeviceScriptCursorByScriptId(scriptId);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				Script script = new Script(cursor);
				cursor.close();
				return script;
			}
		}
		return null;
	}

	public String getDeviceScriptExtraByBind(int device, int zone){
		Cursor c = getDeviceScripExtraCursorByBind(device, zone);
		if(null!=c){
			if(0 < c.getCount()){
				c.moveToFirst();
				String result = c.getString(0);
				c.close();
				return result;
			}
		}
		return null;
	}

	private Cursor getDeviceScripExtraCursorByBind(int device, int zone)
	{
		return rawQuery(dbRead, "SELECT `extra` FROM `device_scripts` WHERE `bind`=" + Integer.toString(zone) +
				" AND `device`=" + Integer.toString(device));
	}

	public Script getDeviceScriptsByBind(int device_id, int bind)
	{
		Cursor cursor = getFullDeviceScriptCursorByBind(device_id, bind);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				Script script = new Script(cursor.getInt(0),
						cursor.getString(1),
						cursor.getInt(2),
						cursor.getInt(3),
						cursor.getInt(4),
						cursor.getString(5),
						cursor.getString(6),
						cursor.getString(7),
						cursor.getString(8),
						cursor.getString(9),
						cursor.getString(10),
						cursor.getString(11),
						cursor.getString(12),
						cursor.getInt(13),
						cursor.getString(14),
						cursor.getString(15),
						cursor.getString(16));
				cursor.close();
				return script;
			}
		}
		return null;
	}

	private Cursor getFullDeviceScriptCursorByBind(int device, int bind)
	{
		return rawQuery(dbRead, "SELECT `device_scripts`.`script_id` as `device_script_id`, " +
				"`device_scripts`.`script_uid` as `device_script_uid`, " +
				"`s`.`script_id` as `device_script_lib_id`, " +
				"`device_scripts`.`device`, " +
				"`device_scripts`.`bind`, " +
				"`device_scripts`.`name`, " +
				"`s`.`name` as `name_lib`, " +
				"`s`.`category`, " +
				"`s`.`description`, " +
				"`device_scripts`.`program`, " +
				"`device_scripts`.`compile_program`, " +
				"`device_scripts`.`extra`, " +
				"`s`.`extra` as `extra_lib`, " +
				"`device_scripts`.`enabled`, " +
				"`s`.`firmware`," +
				"`device_scripts`.`params`," +
				"`s`.`params` as `params_lib` " +
				"FROM `device_scripts` " +
				" LEFT JOIN `scripts` `s` ON `device_scripts`.`script_uid`=`s`.`script_uid`" +
				" WHERE `device_scripts`.`device`=" + Integer.toString(device) +
				" AND `device_scripts`.`bind`=" + Integer.toString(bind));
	}

	private Cursor getFullDeviceScriptCursorByScriptId(int id)
	{
		return rawQuery(dbRead, "SELECT `device_scripts`.`script_id` as `device_script_id`, " +
				"`device_scripts`.`script_uid` as `device_script_uid`, " +
				"`s`.`script_id` as `device_script_lib_id`, " +
				"`device_scripts`.`device`, " +
				"`device_scripts`.`bind`, " +
				"`device_scripts`.`name`, " +
				"`s`.`name` as `name_lib`, " +
				"`s`.`category`, " +
				"`s`.`description`, " +
				"`device_scripts`.`program`, " +
				"`device_scripts`.`compile_program`, " +
				"`device_scripts`.`extra`, " +
				"`s`.`extra` as `extra_lib`, " +
				"`device_scripts`.`enabled`, " +
				"`s`.`firmware`," +
				"`device_scripts`.`params`," +
				"`s`.`params` as `params_lib` " +
				"FROM `device_scripts` " +
				" LEFT JOIN `scripts` AS `s` ON `device_scripts`.`script_uid`=`s`.`script_uid`" +
				" WHERE `device_scripts`.`script_id`=" + Integer.toString(id));
	}

	public Script getFullDeviceScriptByScriptId(int scriptId){
		Cursor cursor = getFullDeviceScriptCursorByScriptId(scriptId);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				Script script = new Script(cursor.getInt(0),
						cursor.getString(1),
						cursor.getInt(2),
						cursor.getInt(3),
						cursor.getInt(4),
						cursor.getString(5),
						cursor.getString(6),
						cursor.getString(7),
						cursor.getString(8),
						cursor.getString(9),
						cursor.getString(10),
						cursor.getString(11),
						cursor.getString(12),
						cursor.getInt(13),
						cursor.getString(14),
						cursor.getString(15),
						cursor.getString(16));
				cursor.close();
				return script;
			}
		}
		return null;
	}

	private void addNewDeviceScript(Script script)
	{
		execSQL(dbWrite, "INSERT INTO `device_scripts` (`script_id`, `script_uid`, `device`, `bind`," +
				" `name`, `program`, `compile_program`, `params`," +
				" `extra`, `enabled`)" +
				" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", new String[]{
				Integer.toString(script.id),
				String.valueOf(script.uid),
				Integer.toString(script.device),
				Integer.toString(script.bind),
				String.valueOf(script.name),
				String.valueOf(script.program),
				String.valueOf(script.compileProgram),
				String.valueOf(script.params),
				String.valueOf(script.extra),
				Integer.toString(script.enabled)
		});
	}

	public void deleteDeviceScriptByScriptId(int scriptId)
	{
		execSQL(dbWrite, "DELETE FROM `device_scripts` WHERE `script_id`=" + Integer.toString(scriptId));
	}

	public void deleteDeviceScript(int scriptId, int deviceId, int bind)
	{
		execSQL(dbWrite, "DELETE FROM `device_scripts` WHERE `script_id`=" + Integer.toString(scriptId) + " AND `device`=" + Integer.toString(deviceId)
		+ " AND `bind`=" + Integer.toString(bind));
	}

	public Cursor getDeviceScriptsCursorByDeviceId(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `device_scripts` " +
				"INNER JOIN `zones` ON `device_scripts`.`device`=`zones`.`zone_device_id` AND `device_scripts`.`bind`=`zones`.`zone_id` "
				+  (-1!=id?"WHERE `device`=" + Integer.toString(id) :""));
	}

	public Cursor getDeviceScriptsCursorBySiteId(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `device_scripts` " +
				"LEFT JOIN `device_site` AS `ds` ON `ds`.`device_device_id`=`device_scripts`.`device`" +
				"INNER JOIN `zones` ON `device_scripts`.`device`=`zones`.`zone_device_id` AND `zones`.`zone_section_id`=`ds`.`device_section_id` AND `device_scripts`.`bind`=`zones`.`zone_id`" +
				" WHERE `ds`.`device_site_id`=" + Integer.toString(id) +
				" ORDER BY `enabled` DESC");
	}




	//LIBRARY SCRIPTS

	private String dbString(String s){
		return  s!=null ? String.valueOf(DatabaseUtils.sqlEscapeString(s)):null;
	}

	public long addLibraryScripts(Script[] scripts)
	{
		Cursor cursor = getAllLibraryScriptsCursor();
		if(null!=cursor){
			if(0!=cursor.getCount()){
				for (Script script:scripts){
					updateLibraryScriptsAndActual(script, 0);
				}
				deleteLibraryScriptsWithActual(1);
				updateAllLibraryScriptsActual(1);
				for(Script script:scripts){
					Cursor c = getLibraryScriptCursorById(script.libId);
					if(null!=c){
						if(0 == c.getCount()){
							addNewLibraryScript(script);
						}
						c.close();
					}
				}
				cursor.close();
			}else{
				for(Script script:scripts){
					addNewLibraryScript(script);
				}
			}
			cursor.close();
		}
		return lastInsertedRowId(dbWrite);
	}

	private void updateAllLibraryScriptsActual(int actual)
	{
		execSQL(dbWrite, "UPDATE `scripts` SET `actual`=" + Integer.toString(actual));
	}

	private void deleteLibraryScriptsWithActual(int actual)
	{
		execSQL(dbWrite, "DELETE FROM `scripts` WHERE `actual`=" + Integer.toString(actual));
	}

	private void updateLibraryScriptsAndActual(Script script, int actual)
	{
		execSQL(dbWrite, "UPDATE `scripts` " +
				"SET `script_uid`=" + dbString(script.uid) +
				", `category`=" + dbString(script.category) +
				", `name`=" + dbString(script.nameLib) +
				", `description`=" + dbString(script.description) +
				", `source`=" + dbString(script.program) +
				", `params`=" + dbString(script.paramsLib) +
				", `extra`=" + dbString(script.extraLib) +
				", `firmware`=" + dbString(script.firmwares) +
				", `actual`=" + Integer.toString(actual) +
				" WHERE `script_id`=" + Integer.toString(script.libId));
	}

	public Cursor getLibraryScriptCursorById(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `scripts` WHERE `script_id`=" + Integer.toString(id));
	}

	public Script getLibraryScriptByLibId(int id){
		Cursor cursor = getLibraryScriptCursorById(id);
		if(null!=cursor){
			if(0!=cursor.getCount())
			{
				cursor.moveToFirst();
				Script script= new Script(cursor.getInt(1),
						cursor.getString(2),
						cursor.getString(3),
						cursor.getString(4),
						cursor.getString(5),
						cursor.getString(6),
						cursor.getString(7),
						cursor.getString(8),
						cursor.getString(9));
				cursor.close();
				return script;
			}
			cursor.close();
		}
		return null;
	}

	public Cursor getAllLibraryScriptsCursor()
	{
		return rawQuery(dbRead, "SELECT * FROM `scripts`");
	}

	public Cursor getLibraryScriptsCursorByCategory(String category, int configVersion){
		return rawQuery(dbRead, "SELECT * FROM `scripts` " +
				" INNER JOIN `firmware_scripts` ON `firmware_scripts`.`script_id`=`scripts`.`script_id` " +
				"WHERE `scripts`.`category` LIKE '%" + category + "%'" +
				" AND `firmware_scripts`.`firmware`=" + Integer.toString(configVersion) + " ORDER BY `name`");
	}

	public LinkedList<Script> getLibraryScriptsByCategory(String category, int configVersion){
		Cursor cursor = getLibraryScriptsCursorByCategory(category, configVersion);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				LinkedList<Script> scripts = new LinkedList<>();
				cursor.moveToFirst();
				scripts.add(new Script(cursor.getInt(1),
						cursor.getString(2),
						cursor.getString(3),
						cursor.getString(4),
						cursor.getString(5),
						cursor.getString(6),
						cursor.getString(7),
						cursor.getString(8),
						cursor.getString(9)));
				while (cursor.moveToNext()){
					scripts.add(new Script(cursor.getInt(1),
							cursor.getString(2),
							cursor.getString(3),
							cursor.getString(4),
							cursor.getString(5),
							cursor.getString(6),
							cursor.getString(7),
							cursor.getString(8),
							cursor.getString(9)));
				}
				cursor.close();
				return scripts;
			}
			cursor.close();
		}
		return null;
	}

	public Cursor getAllScriptsCategoriesCursorByCV(int configVersion){
		return rawQuery(dbRead, "SELECT DISTINCT `category` " +
				"FROM `scripts` INNER JOIN `firmware_scripts` ON `firmware_scripts`.`script_id`=`scripts`.`script_id` " +
				"WHERE `firmware_scripts`.`firmware`=" + Integer.toString(configVersion) + " ORDER BY `category`");
	}

	private Cursor getAllScriptNamesCursorByCategory(String category)
	{
		return rawQuery(dbRead, "SELECT `name` FROM `scripts` WHERE `category` LIKE '%" + category + "%'");
	}

	public String[] getAllScriptsCategoriesByConfigVersion(int configVersion){
		Cursor cursor = getAllScriptsCategoriesCursorByCV(configVersion);
		if(null!=cursor){
			if(cursor.getCount()!=0){
				String[] categories = new String[cursor.getCount()];
				cursor.moveToFirst();
				for (int i =0; i < cursor.getCount(); i++){
					categories[i] = cursor.getString(0);
					cursor.moveToNext();
				}
				cursor.close();
				return categories;
			}
			cursor.close();
		}
		return null;
	}

	public String[] getScriptNamesByCategory(String category){
		Cursor cursor = getAllScriptNamesCursorByCategory(category);
		if(null!=cursor){
			if(cursor.getCount()!=0){
				String[] scripts = new String[cursor.getCount()];
				cursor.moveToFirst();
				for (int i =0; i < cursor.getCount(); i++){
					scripts[i] = cursor.getString(0);
					cursor.moveToNext();
				}
				cursor.close();
				return scripts;
			}
			cursor.close();
		}
		return null;
	}

	private void addNewLibraryScript(Script script)
	{
		execSQL(dbWrite, "INSERT INTO `scripts` (`script_id`, `script_uid`, `category`, `name`, `description`, " +
				"`source`, `params`, `extra`, `firmware`) " +
				"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", new String[]{
				Integer.toString(script.libId),
				String.valueOf(script.uid),
				String.valueOf(script.category),
				String.valueOf(script.nameLib),
				String.valueOf(script.description),
				String.valueOf(script.program),
				String.valueOf(script.paramsLib),
				String.valueOf(script.extraLib),
				String.valueOf(script.firmwares)
		});
	}

	public long addDeviceScript(Script script)
	{
		Cursor cursor = getDeviceScriptCursorByScriptId(script.id);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				updateDeviceScriptInfoAndActual(script, 1);
				cursor.close();
				return lastInsertedRowId(dbWrite);
			}
			cursor.close();
		}
		addNewDeviceScript(script);
		return lastInsertedRowId(dbWrite);
	}

	public Zone[] getAllAutoRelaysByDeviceId(int device_id)
	{
		Cursor cursor = getAllAutoRelaysCursorByDeviceId(device_id);
		if(null!=cursor){
			if(cursor.getCount()!=0){
				cursor.moveToFirst();
				Zone[] zones = new Zone[cursor.getCount()];
				for(int i=0; i < cursor.getCount(); i++){
					zones[i] = new Zone(cursor);
					cursor.moveToNext();
				}
				cursor.close();
				return zones;
			}
			cursor.close();
		}
		return null;
	}

	private Cursor getAllAutoRelaysCursorByDeviceId(int device_id)
	{
		return  rawQuery(dbRead, "SELECT * FROM `zones` WHERE " +
				"`zone_device_id`=" + Integer.toString(device_id) +
				" AND (`zone_detector`=" + Integer.toString(Const.DETECTOR_AUTO_RELAY) +
//				" OR `zone_detector`=" + Integer.toString(Const.DETECTOR_SIREN) + "" +
//				" OR `zone_detector`=" + Integer.toString(Const.DETECTOR_SZO) +
				")");
	}

	public int getBindedScriptIdForRelay(Zone zone)
	{
		Cursor cursor = getDeviceScriptCursorByBind(zone);
		if(null!=cursor && 0<cursor.getCount()){
			cursor.moveToFirst();
			int result = cursor.getInt(1);
			cursor.close();
			return result;
		}
		return -1;
	}

	public boolean getScriptBindedStatusForRelay(Zone zone)
	{
		Cursor cursor = getDeviceScriptCursorByBind(zone);
		if(null!=cursor && 0<cursor.getCount()){
			cursor.close();
			return true;
		}
		return false;
	}

	private Cursor getDeviceScriptCursorByBind(Zone zone)
	{
		return rawQuery(dbRead, "SELECT * FROM `device_scripts` WHERE `bind`=" + Integer.toString(zone.id)
				+ " AND `device`=" + Integer.toString(zone.device_id)
				);
	}

	//FIRMWARE_SCRIPTS

	public void addFirmwareScript(int firmware, int script){
		execSQL(dbWrite, "INSERT INTO `firmware_scripts` (`firmware`, `script_id`) VALUES (? , ?)", new String[]{
				Integer.toString(firmware),
				Integer.toString(script)
		} );
	}

	public void deleteFirmwareScriptByFId(int firmware){
		execSQL(dbWrite, "DELETE FROM `firmware_scripts` WHERE `firmware`=" + Integer.toString(firmware));
	}

	public boolean checkCritEventsExistence(long last_activity_closed_time)
	{
		Cursor cursor  = rawQuery(dbRead, "SELECT * FROM `events` WHERE (`event_affect`=" + Integer.toString(1)
				+ " AND `event_class_id`=2"
				+ " AND `event_reason_id`=51)" +
				" OR" +
				"(`event_time`>" +
				Long.toString(last_activity_closed_time) +
				" AND `event_class_id`<4" +
				" AND NOT(`event_class_id`=3 AND `event_reason_id`=5))"
				+ " ORDER BY `event_id` DESC"
		);
		if(null!=cursor && 0<cursor.getCount()){
			cursor.close();
			return true;
		}
		return  false;
	}

	public int getFirstSite()
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `sites` ORDER BY `site_id` LIMIT 1");
		if(null!=c && 0!=c.getCount()){
			c.moveToFirst();
			return c.getInt(1);
		}
		return 0;
	}

	/*MAIN BARS*/

	public Cursor getMainBars(int site_id, boolean shared) {
		if (shared)
			return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type`!='EMPTY' ORDER BY `_id`");
		else
			return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `site_id`=" + site_id + " ORDER BY `_id`");
	}

	public void fillBars(int site_id, boolean shared) {
		if (!shared)
			for (int i = 0; i < 6; i++) {
				MainBar mainBar = new MainBar(Func.getX(i), Func.getY(i));
				execSQL(dbWrite, "INSERT INTO `main_bars` (`type`, `site_id`, `x`, `y`) VALUES (? ,?, ?, ?)",
						new String[]{
								mainBar.type.name(),
								Integer.toString(site_id),
								Integer.toString(mainBar.x),
								Integer.toString(mainBar.y)
						});
			}
	}

	private MainBar getLastSharedBar() {
		Cursor c = rawQuery(dbRead, "SELECT * FROM `main_bars`");
		if (c.getCount() == 0)
			return null;
		c.moveToLast();
		return new MainBar(c);

	}

	private MainBar getSharedBar(int site_id) {
		MainBar prev = getLastSharedBar();
		Pair coord;
		if (prev != null) {
			if (prev.x == 0)
				coord = new Pair(1, prev.y);
			else
				coord = new Pair(0, prev.y + 1);
			return new MainBar(prev.id + 1, site_id, coord);
		} else {
			coord = new Pair(0, 0);
			return new MainBar(0, site_id, coord);
		}

	}

	public MainBar getEmptyBar(int site_id, boolean shared) {
		if (shared) {
			return getSharedBar(site_id);
		} else {
			for (int i = 0; i < 6; i++) {
				Pair coord = new Pair(Func.getX(i), Func.getY(i));
				Cursor c = getMainBarCursor(site_id, coord);
				if (null != c) {
					if (c.getCount() > 0) {
						c.moveToFirst();
						if (MainBar.Type.valueOf(c.getString(1)) == MainBar.Type.EMPTY) {
							return new MainBar(c.getInt(0), site_id, coord);
						}
					}
				}
			}
		}
		return null;
	}

	private Cursor getMainBarCursor(int site_id, Pair coord) {
		return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE " +
				"`site_id`=" + Integer.toString(site_id) +
				" AND `x`=" + Integer.toString(coord.x) +
				" AND `y`=" + Integer.toString(coord.y));
	}


	public boolean addNewBar(MainBar mainBar, boolean shared) {
		if (shared) {
			return addNewSharedBar(mainBar);
		} else
			return execSQL(dbWrite, "UPDATE `main_bars` SET " +
					"`type`=" + dbString(mainBar.type.name()) + ", " +
					(null != mainBar.subtype ? "`subtype`=" + dbString(mainBar.subtype.name()) + ", " : "`subtype`=" + dbString("") + ", ") +
					"`site_id`=" + Integer.toString(mainBar.site_id) + ", " +
					"`element_id`='" + mainBar.element_id + "', " +
					"`x`=" + Integer.toString(mainBar.x) + ", " +
					"`y`=" + Integer.toString(mainBar.y) +
					" WHERE `_id`=" + Integer.toString(mainBar.id));
	}

	private boolean addNewSharedBar(MainBar mainBar) {
		return execSQL(dbWrite, "INSERT INTO `main_bars` (`type`, `subtype`, `site_id`,`element_id`, `x`, `y`) " +
						"VALUES (?,?,?,?,?,?)",
				new String[]{
						mainBar.type.name(),
						null != mainBar.subtype ? mainBar.subtype.name() : "",
						Integer.toString(mainBar.site_id),
						mainBar.element_id,
						Integer.toString(mainBar.x),
						Integer.toString(mainBar.y)
				});
	}

	public boolean deleteMainBar(MainBar mainBar) {
		if (mainBar.subtype == MainBar.SubType.GROUP)
			deleteSectionGroup(Integer.valueOf(mainBar.element_id));
		return execSQL(dbWrite, "UPDATE `main_bars` SET `type`=" + dbString(MainBar.Type.EMPTY.name()) +
				", `subtype`=NULL" +
				", `site_id`=" + mainBar.site_id +
				" WHERE `_id`=" + mainBar.id);
	}

	public void deleteMainBars() {

		execSQL(dbWrite, "DELETE FROM `main_bars`");
	}

	public boolean deleteBarForElement(MainBar mainBar) {
		return execSQL(dbWrite, "UPDATE `main_bars` SET `type`=" + dbString(MainBar.Type.EMPTY.name()) +
				", `site_id`=" + mainBar.site_id +
				" WHERE `_id`=" + mainBar.id);
	}

	public boolean getFavoritedStatus() {
		return false;
	}

	public MainBar getMainBarForDevice(int site, Device device){
		Cursor c = getMainbarCursorForDevice(site, device.id);
		if(null!=c && c.getCount()>0){
			if(0< c.getCount()){
				c.moveToFirst();
				MainBar mainBar = new MainBar(c);
				return mainBar;
			}
			c.close();
		}
		return null;
	}

	public MainBar getMainBarForDevice(Device device){
		Cursor c = getMainbarCursorForDevice(device.id);
		if(null!=c && c.getCount()>0){
			if(0< c.getCount()){
				c.moveToFirst();
				MainBar mainBar = new MainBar(c);
				return mainBar;
			}
			c.close();
		}
		return null;
	}

	public boolean getFavoritedStatusForDevice(int site, int device)
	{
		Cursor c = getMainbarCursorForDevice(site, device);
		if(null!=c && c.getCount() > 0){
			c.close();
			return true;
		}
		return false;
	}

	private Cursor getMainbarCursorForDevice(int site, int device)
	{
		return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.ARM.name())
				+" AND `site_id`=" + Integer.toString(site)
				+" AND `element_id`=" + Integer.toString(device)
				+ " AND `subtype` LIKE " + dbString(MainBar.SubType.DEVICE.name()) + "");
	}

	private Cursor getMainbarCursorForDevice(int device)
	{
		Site site = getSiteByDeviceSection(device, 0);
		if(null!=site){
			return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.ARM.name())
					+" AND `site_id`=" + Integer.toString(site.id)
					+" AND `element_id`=" + Integer.toString(device)
					+ " AND `subtype` LIKE " + dbString(MainBar.SubType.DEVICE.name()) + "");
		}
		return null;
	}

	public MainBar getMainBarForSection(int site, Section section){
		Cursor c = getMainBarCursorForSection(site, section);
		if(null!=c && c.getCount()>0){
			if(0< c.getCount()){
				c.moveToFirst();
				MainBar mainBar = new MainBar(c);
				return mainBar;
			}
			c.close();
		}
		return null;
	}

	public MainBar getMainBarForSection(Section section){
		Cursor c = getMainBarCursorForSection(section);
		if(null!=c && c.getCount()>0){
			if(0< c.getCount()){
				c.moveToFirst();
				MainBar mainBar = new MainBar(c);
				return mainBar;
			}
			c.close();
		}
		return null;
	}

	public boolean getFavoriteStatusForSection(int site, Section section){
		Cursor c = getMainBarCursorForSection(site, section);
		if(null!=c && c.getCount()>0){
			c.close();
			return true;
		}
		return false;
	}

	private Cursor getMainBarCursorForSection(int site, Section section)
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put("device", section.device_id);
			jsonObject.put("section", section.id);
			return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.ARM.name())
					+" AND `site_id`=" + Integer.toString(site)
					+" AND `element_id`=" + dbString(jsonObject.toString())
					+ " AND `subtype` LIKE " + dbString(MainBar.SubType.SECTION.name()) + "");
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private Cursor getMainBarCursorForSection(Section section)
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put("device", section.device_id);
			jsonObject.put("section", section.id);
			Site site = getSiteByDeviceSection(section.device_id, section.id);
			if(null!=site){
				return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.ARM.name())
						+" AND `site_id`=" + Integer.toString(site.id)
						+" AND `element_id`=" + dbString(jsonObject.toString())
						+ " AND `subtype` LIKE " + dbString(MainBar.SubType.SECTION.name()) + "");
			}else
				return null;
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public MainBar getMainBarForZone(int site, Zone zone){
		Cursor c = getMainBarCursorForZone(site, zone);
		if(null!=c && c.getCount()>0){
			if(0< c.getCount()){
				c.moveToFirst();
				MainBar mainBar = new MainBar(c);
				return mainBar;
			}
			c.close();
		}
		return null;
	}

	public MainBar getMainBarForZone(Zone zone){
		Cursor c = getMainBarCursorForZone(zone);
		if(null!=c && c.getCount()>0){
			if(0< c.getCount()){
				c.moveToFirst();
				MainBar mainBar = new MainBar(c);
				return mainBar;
			}
			c.close();
		}
		return null;
	}

	public boolean getFavoriteStatusForZone(int site, Zone zone){
		Cursor c = getMainBarCursorForZone(site, zone);
		if(null!=c && c.getCount()>0){
			c.close();
			return true;
		}
		return false;
	}

	private Cursor getMainBarCursorForZone(int site, Zone zone)
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put("device", zone.device_id);
			jsonObject.put("section", zone.section_id);
			jsonObject.put("zone", zone.id);
			return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.DEVICE.name())
					+" AND `site_id`=" + Integer.toString(site)
					+" AND `element_id`=" + dbString(jsonObject.toString())
			);
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private Cursor getMainBarCursorForZone(Zone zone)
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put("device", zone.device_id);
			jsonObject.put("section", zone.section_id);
			jsonObject.put("zone", zone.id);
			return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.DEVICE.name())
					+" AND `element_id`=" + dbString(jsonObject.toString())
			);
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public MainBar getMainBarForRelay(int site, Zone relay){
		Cursor c = getMainBarCursorForRelay(site, relay);
		if(null!=c && c.getCount()>0){
			if(0< c.getCount()){
				c.moveToFirst();
				MainBar mainBar = new MainBar(c);
				return mainBar;
			}
			c.close();
		}
		return null;
	}

	public MainBar getMainBarForRelay( Zone relay){
		Cursor c = getMainBarCursorForRelay(relay);
		if(null!=c && c.getCount()>0){
			if(0< c.getCount()){
				c.moveToFirst();
				MainBar mainBar = new MainBar(c);
				return mainBar;
			}
			c.close();
		}
		return null;
	}


	public boolean getFavoriteStatusForRelay(int site, Zone relay){
		Cursor c = getMainBarCursorForRelay(site, relay);
		if(null!=c && c.getCount()>0){
			c.close();
			return true;
		}
		return false;
	}

	private Cursor getMainBarCursorForRelay(int site, Zone relay)
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put("device", relay.device_id);
			jsonObject.put("section", relay.section_id);
			jsonObject.put("zone", relay.id);
			return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.CONTROL.name())
					+" AND `site_id`=" + Integer.toString(site)
					+" AND `element_id`=" + dbString(jsonObject.toString())
			);
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private Cursor getMainBarCursorForRelay(Zone relay)
	{
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put("device", relay.device_id);
			jsonObject.put("section", relay.section_id);
			jsonObject.put("zone", relay.id);
			return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.CONTROL.name())
					+" AND `element_id`=" + dbString(jsonObject.toString())
			);
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}


	public boolean getFavoriteStatusForCamera(int site, Camera camera){
		Cursor c = getMainBarCursorForCamera(site,camera);
		if(null!=c && c.getCount()>0){
			c.close();
			return true;
		}
		return false;
	}

	private Cursor getMainBarCursorForCamera(int site, Camera camera)
	{
		return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `type` LIKE " + dbString(MainBar.Type.CAMERA.name())
				+" AND `site_id`=" + Integer.toString(site)
				+" AND `element_id`=" + camera.id
		);	}

	public boolean getZoneArmStatus(Zone zone)
	{
		Cursor cursor = getSectionCursorById(zone.device_id, zone.section_id);
		if(null!=cursor && cursor.getCount()>0){
			cursor.moveToFirst();
			long arm = cursor.getLong(9);
			cursor.close();
			if(arm > 0){
				return true;
			}
		}
		return false;
	}

	/*SECTION_GROUPS*/

	public long addSectionGroup(SectionGroup sectionGroup)
	{
		if(null!=sectionGroup){
			return addNewSectionGroup(sectionGroup);
		}
		return 0;
	}

	private long addNewSectionGroup(SectionGroup sectionGroup)
	{
		if(execSQL(dbWrite, "INSERT INTO `section_groups` (`name`) VALUES (?)", new String[]{sectionGroup.name})){
			long sg_id = lastInsertedRowId(dbWrite);
			if(0 != sg_id){
				if(null!=sectionGroup.sections && sectionGroup.sections.length > 0)
				{
					for (Section section : sectionGroup.sections)
					{
						addSectionGroupSection(sg_id, section);
					}
				}
				return sg_id;
			}
		};
		return 0;
	}

	private void addSectionGroupSection(long sg_id, Section section)
	{
		execSQL(dbWrite, "INSERT INTO `section_group_sections` (`group_id`, `device_id`, `section_id`) VALUES (?, ?, ?)",
				new String[]{
						Long.toString(sg_id),
						Integer.toString(section.device_id),
						Integer.toString(section.id)
				});
	}

	private void deleteSectionGroupSection(Section section)
	{
		execSQL(dbWrite, "DELETE FROM `section_group_sections` WHERE `device_id`=" + Integer.toString(section.device_id) + " " +
				"AND `section_id`=" + Integer.toString(section.id));
	}


	public SectionGroup getSectionGroup(Integer sg_id)
	{
		Cursor c = getSectionGroupCursor(sg_id);
		if(null!=c){
			if(c.getCount() > 0){
				c.moveToFirst();
				SectionGroup sectionGroup = new SectionGroup(c);
				if(null!=sectionGroup){
					Cursor c1 = getSectonGroupSectionsCursor(sectionGroup.id);
					if(null!=c1){
						if(c1.getCount() > 0){
							c1.moveToFirst();
							Section[] sections = new Section[c1.getCount()];
							int i =0;
							sections[i] = getDeviceSectionById(c1.getInt(2), c1.getInt(3));
							while (c1.moveToNext()){
								i++;
								sections[i] = getDeviceSectionById(c1.getInt(2), c1.getInt(3));
							}
							sectionGroup.sections = sections;
							c1.close();
						}
						c1.close();
					}
				}
				c.close();
				return sectionGroup;
			}
			c.close();
		}
		return  null;
	}

	private Cursor getSectonGroupSectionsCursor(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `section_group_sections` WHERE `group_id`=" + Integer.toString(id));
	}

	private Cursor getSectionGroupCursor(Integer sg_id)
	{
		return rawQuery(dbRead, "SELECT * FROM `section_groups` WHERE `_id`=" + Integer.toString(sg_id));
	}

	public  void deleteSectionGroup(int id){
		execSQL(dbWrite, "DELETE FROM `section_groups` WHERE `_id`=" + Integer.toString(id));
	}

	public int getSectionGroupArmStatus(SectionGroup sectionGroup)
	{
		if(null!=sectionGroup){
			if(null!=sectionGroup.sections && sectionGroup.sections.length > 0){
				Cursor c = rawQuery(dbRead, "SELECT COUNT(*) FROM `section_group_sections` " +
						" INNER JOIN `zones` ON `section_group_sections`.`section_id`=`zones`.`zone_section_id`" +
						" AND `section_group_sections`.`device_id` = `zones`.`zone_device_id` " +
						" WHERE `zones`.`zone_id`=0 AND `zones`.`zone_armed`>0");
				if(null!=c){
					int count = c.getCount();
					if(0 < count){
						if(count == sectionGroup.sections.length){
							c.close();
							return 1;
						}
						c.close();
						return 2;
					}
					c.close();
				}
			}
		}
		return 0;
	}

	public int getDeviceArmSta1tus(int device_id){
		Cursor c = rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` WHERE `zone_device_id`=" + Integer.toString( device_id) +
				" AND `zone_id`=0 AND `zone_section_id`> 0 AND `zone_armed`>0");
		Cursor cAll = rawQuery(dbRead, "SELECT COUNT(*) FROM `zones` WHERE `zone_device_id`=" + Integer.toString( device_id) +
				" AND `zone_id`=0 AND `zone_section_id`> 0");
		if(null!=c&&null!=cAll){
			if(cAll.getCount() != 0 && c.getCount() !=0){
				cAll.moveToFirst();
				c.moveToFirst();

				int armedCount = c.getInt(0);
				int allCount = cAll.getInt(0);
				c.close();
				cAll.close();
				if(armedCount > 0){
					if(allCount == armedCount){
						return 1;
					}
					return 2;
				}
			}
			c.close();
			cAll.close();
		}
		return 0;
	}

	public int getRelayStatusById(int device_id, int section_id, int id)
	{
		Cursor c = rawQuery(dbRead, "SELECT COUNT(*) FROM `events` WHERE " +
				" `event_device`=" + Integer.toString(device_id) +
				" AND `event_section`=" + Integer.toString(section_id) +
				" AND `event_zone`=" + Integer.toString(id) +
				" AND `event_class_id`=" + Integer.toString(5) +
				" AND `event_reason_id`=" + Integer.toString(6) +
				" AND `event_affect` =" + Integer.toString(1));
		if(null!=c){
			if(c.getCount() > 0){
				c.moveToFirst();
				if(c.getInt(0) > 0)
				{
					c.close();
					return Const.STATUS_ON;
				}
			}
			c.close();
		}
		return Const.STATUS_OFF;
	}

	public boolean nGetAlarmEventsByZone(Zone zone)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` WHERE "
				+ " `event_device`=" + Integer.toString(zone.device_id)
				+ " AND `event_section`=" + Integer.toString(zone.section_id)
				+ " AND `event_zone`=" + Integer.toString(zone.id)
				+ " AND `event_affect`=1"
				+ " AND `event_active`=1"
				+ " AND `event_class_id`=0"
				+ " ORDER BY `event_id` DESC"
		);
		if(null!=c){
			if(0 < c.getCount()){
				c.close();
				return true;
			}
			c.close();
		}
		return false;
	}

	public boolean nGetAttentionEventsByZone(Zone zone)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` WHERE "
				+ " `event_device`=" + Integer.toString(zone.device_id)
				+ " AND `event_section`=" + Integer.toString(zone.section_id)
				+ " AND `event_zone`=" + Integer.toString(zone.id)
				+ " AND `event_affect`=1"
				+ " AND `event_active`=1"
				+ " AND (`event_class_id`=1"
				+ " OR `event_class_id`=2"
				+ " OR `event_class_id`=3)"
				+ " ORDER BY `event_id` DESC"
		);
		if(null!=c){
			if(0 < c.getCount()){
				c.close();
				return true;
			}
			c.close();
		}
		return false;	}

	public Cursor getSitesWithScriptsCursor(int currentSite)
	{
		return rawQuery(dbRead, "SELECT DISTINCT * FROM `sites` " +
				"INNER JOIN (SELECT `device_device_id`, `device_site_id` FROM `device_site`) ON `device_site_id`=`site_id` " +
				"INNER JOIN (SELECT `device` FROM `device_scripts`) ON `device`=`device_device_id` "+
				"ORDER BY CASE WHEN `site_id`=" + Integer.toString(currentSite) + " THEN -1 ELSE `site_id` END"
		);
	}

	public MainBar getMainBarById(int id)
	{
		Cursor cursor = getMainBarCursor(id);
		if(null!=cursor){
			if(0!=cursor.getCount()){
				cursor.moveToFirst();
				MainBar mainBar = new MainBar(cursor);
				return mainBar;
			}
			cursor.close();
		}
		return null;
	}

	private Cursor getMainBarCursor(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `main_bars` WHERE `_id`=" + Integer.toString(id));
	}

	public Cursor getNAllNRElementsCursorSiteOrder(LinkedList<Event> eventsNRList)
	{
		String query = "SELECT DISTINCT *, `device_site_id` FROM `zones` " +
				"INNER JOIN  " +
				"(SELECT DISTINCT `device_device_id`,`device_site_id` FROM `device_site`) " +
				"ON `zones`.`zone_device_id`=`device_device_id` " +
				"WHERE ((`zone_section_id`> 0 AND `zone_id`!=0) OR (`zone_section_id`=0 AND `zone_id`!=100)) "
//				+ "AND `device_site_id`=" + Integer.toString(site_id)
				;
		if(null!=eventsNRList && eventsNRList.size() > 0){
			query += " AND(";
			int i = 0;
			for(Event event:eventsNRList){
				if(0!=i){query += "OR";}
				query += "(`zone_id`=" + Integer.toString(event.zone==100 ? 0:event.zone) +" AND `zone_section_id`=" + Integer.toString(event.section) + " AND `zone_device_id`=" + Integer.toString(event.device) + ")";
				i++;
			}
			query += ")";
		}
		query +=" ORDER BY  " +
//				" CASE WHEN `device_site_id`=" + Integer.toString(site_id) + " THEN -1 ELSE `device_site_id` END," +
				"`zone_section_id`," +
				"CASE WHEN `zone_id`=" + Integer.toString(100) + " THEN -1 ELSE `device_site_id` END";
		return rawQuery(dbRead, query);
	}


	//RTSP CAMERAS


	public Camera getRTSPCameraById(int id){
		Cursor c = getRTSPCameraCursorById(id);
		if(null!=c){
			if(0!=c.getCount()){
				c.moveToFirst();
				Camera camera = new Camera(c.getInt(0), c.getString(1), c.getString(2));
				c.close();
				return camera;
			}
			c.close();
		}
		return null;
	}

	private Cursor getRTSPCameraCursorById(int id)
	{
		return rawQuery(dbRead, "SELECT * FROM `cameras_rtsp` WHERE `_id`=" + Integer.toString(id));
	}

	public  Cursor getRTSPCamerasCursor(){
		return  rawQuery(dbRead, "SELECT * FROM `cameras_rtsp`");
	}

	public boolean addRTSPCamera(Camera camera)
	{
		return execSQL(dbWrite, "INSERT INTO `cameras_rtsp` (`name`, `link`) VALUES (?,?)", new String[]{
				camera.name,
				camera.link
		});
	}

	public  boolean deleteRTSPCamera(int id){
		return execSQL(dbWrite, "DELETE FROM `cameras_rtsp` WHERE `_id`=" + Integer.toString(id));
	}

	public void udpateRTSPCamera(Camera camera){
		execSQL(dbWrite, "UPDATE `cameras_rtsp` SET `name`='"+camera.name +
				"', `link`='" + camera.link +
				"' WHERE `_id`=" + Integer.toString(camera.local_id));
	}

	public boolean renameRTSPCamera(int id, String name){
		return  execSQL(dbWrite, "UPDATE `cameras_rtsp` SET `name`='"+ name +
				"' WHERE `_id`=" + Integer.toString(id));
	}

	public boolean setLinkRTSPCamera(int id, String link){
		return  execSQL(dbWrite, "UPDATE `cameras_rtsp` SET `link`='"+ link +
				"' WHERE `_id`=" + Integer.toString(id));
	}

	/*ALARMS*/

	public boolean addNewAlarm(Alarm alarm){
		return execSQL(dbWrite, "INSERT INTO `alarms` (`alarm_id`, `device_id`, `state`, `type`, `create_time`, `managed_user`, `extra`) VALUES (?,?,?,?,?,?,?)", new String[]{
				Integer.toString(alarm.id),
				Integer.toString(alarm.device),
				Integer.toString(alarm.state),
				Integer.toString(alarm.type),
				Long.toString(alarm.create_time),
				Integer.toString(alarm.managed_user),
				alarm.extra
		});
	}

	public boolean updateAlarm(Alarm alarm){
		return execSQL(dbWrite, "UPDATE `alarms` SET " +
				"`device_id`=" + Integer.toString(alarm.device) + ", " +
				"`state`=" + Integer.toString(alarm.state) + ", " +
				"`type`=" + Integer.toString(alarm.type) + ", " +
				"`create_time`=" + Long.toString(alarm.create_time) + ", " +
				"`managed_user`=" + Integer.toString(alarm.managed_user) + ", " +
				"`extra`='" + String.valueOf(alarm.extra) + "', " +
				"`actual`=1 " +
				"WHERE `alarm_id`=" + Integer.toString(alarm.id));
	}


	public long addAlarms(Alarm[] alarms) {
		Cursor cursor = getAllAlarmsCursor();
		if(null!=cursor){
			if(cursor.getCount() > 0){
				cursor.close();
				updateAllAlarmsActual(0);
				for(Alarm alarm:alarms){
					Cursor c = getAlarmCursorById(alarm.id);
					if(null!=c){
						if(c.getCount() > 0){
							updateAlarm(alarm);
						}else{
							addNewAlarm(alarm);
						}
						c.close();
					}
				}
				deleteInactiveAlarms();
				return lastInsertedRowId(dbWrite);
			}
			cursor.close();
		}
		for(Alarm alarm:alarms){
			addNewAlarm(alarm);
		}
		return lastInsertedRowId(dbWrite);
	}

	private void deleteInactiveAlarms() {
		execSQL(dbWrite, "DELETE FROM `alarms` WHERE `actual`=0");
	}

	private void updateAllAlarmsActual(int i) {
		execSQL(dbWrite, "UPDATE `alarms` SET `actual`=" + Integer.toString(i));
	}

	private Cursor getAlarmCursorById(int id) {
		return rawQuery(dbRead, "SELECT * FROM `alarms` WHERE `alarm_id`=" + Integer.toString(id));
	}

	private Cursor getAllAlarmsCursor() {
		return rawQuery(dbRead, "SELECT * FROM `alarms`");
	}

	public long addAlarm(Alarm alarm) {
		Cursor c = getAlarmCursorById(alarm.id);
		if(null!=c){
			if(c.getCount() > 0){
				updateAlarm(alarm);
			}else{
				addNewAlarm(alarm);
			}
			c.close();
			return lastInsertedRowId(dbWrite);
		}
		return 0;
	}

	public void addAlarmEvent(int result, int id) {
		execSQL(dbWrite, "INSERT INTO `alarm_events` (`alarm_id`, `event_id`) VALUES(?,?)", new String[]{
			Integer.toString(result),
			Integer.toString(id)
		});
	}

	public LinkedList<Alarm> getAlarmsList()
	{
		Cursor c = getAlarmsCursor();
		if(null!=c){
			if (0 < c.getCount()){
				c.moveToFirst();
				LinkedList<Alarm> alarms = new LinkedList<>();
				alarms.add(new Alarm(c));
				while (c.moveToNext()){
					alarms.add(new Alarm(c));
				}
				c.close();
				return alarms;
			}
			c.close();
		}
		return null;
	}

	public Cursor getAlarmsCursor()
	{
		return rawQuery(dbRead, "SELECT * FROM `alarms` WHERE `state`<21");
	}

	public Alarm getAlarmById(int alarm_id)
	{
		Cursor c = getAlarmCursorById(alarm_id);
		if(null!=c){
			if(0<c.getCount()){
				c.moveToFirst();
				Alarm alarm = new Alarm(c);
				c.close();
				return alarm;
			}
			c.close();
		}
		return null;
	}

	public Cursor getEventsCursorByAlarm(int id)
	{
		return rawQuery(dbRead,"SELECT * FROM `events` " +
				"INNER JOIN `alarm_events` ON `events`.`event_id` = `alarm_events`.`event_id` WHERE `alarm_id`=" + Integer.toString(id)
				+ " ORDER BY `event_id` DESC");
	}

	public Alarm getAlarmByDeviceId(int device)
	{
		Cursor c = getAlarmCursorByDeviceId(device);
		if(null!=c){
			if(0<c.getCount()){
				c.moveToFirst();
				Alarm alarm = new Alarm(c);
				c.close();
				return alarm;
			}
			c.close();
		}
		return null;	}

	private Cursor getAlarmCursorByDeviceId(int device)
	{
		return rawQuery(dbRead, "SELECT * FROM `alarms` WHERE `device_id`=" + Integer.toString(device) + " AND `state`<" + Integer.toString(Const.ALARM_STATE_CLOSED_WITH_GBR));
	}

	public Cursor getEventByDeviceSectionZoneWithType(int device_id, int section_id, int zone_id, int i)
	{
		/*TODO*/
		return rawQuery(dbRead,"SELECT * FROM `events`" +
				" WHERE `event_visiblity`=" + Integer.toString(1) +
				" AND `event_device`=" + Integer.toString(device_id)+
				" AND `event_section`=" + Integer.toString(section_id) +
				" AND `event_zone`=" + Integer.toString(zone_id) +
				" ORDER BY `event_id` DESC");
	}

	public void updateDeviceVersion(int id, int configVersion)
	{
		execSQL(dbWrite, "UPDATE `devices` SET `device_config_version`=" + Integer.toString(configVersion) +
				" WHERE `device_id` = " + Integer.toString(id));
	}

	public Cursor getSupplyEvents(int device_id, int section_id, int zone_id)
	{
		if(zone_id == 0 && section_id == 0){
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE " +
					"(`event_class_id`=6 OR " +
					"(`event_class_id`=2 AND `event_reason_id`=50))"
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND (`event_zone`=100 OR `event_zone`=0)" +
					" AND `event_visiblity`=1 ORDER BY `event_id` DESC"
			);
		}else{
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=6 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND `event_zone`=" + Integer.toString(zone_id) +
					" AND `event_visiblity`=1 ORDER BY `event_id` DESC");
		}
	}

	public Cursor getLevelEvents(int device_id, int section_id, int zone_id)
	{
		if(zone_id == 0 && section_id == 0){
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=7 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=0" +
					" AND (`event_zone`=100 OR `event_zone`=0)" +
					" ORDER BY `event_id` DESC"
			);
		}else{
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=7 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND `event_zone`=" + Integer.toString(zone_id) +
					" AND `event_visiblity`=1" +
					" ORDER BY `event_id` DESC");
		}

	}

	public Cursor getExtLevelEvents(int device_id, int section_id, int zone_id)
	{
		if(zone_id == 0 && section_id == 0){
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=7 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND (`event_section` =0)" +
					" AND (" +
					"(((`event_zone` =100) OR (`event_zone`=0)) &" +
					"(`event_affect`=1)) " +
					" OR (`event_class_id`=7 AND `event_detector_id`=21)" +
					" OR (`event_class_id`=7 AND `event_detector_id`=22)" +
					")" +
					" AND `event_visiblity`=1 ORDER BY `event_id` DESC"
			);
		}else{
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=7 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND `event_zone`=" + Integer.toString(zone_id) +
					" AND `event_visiblity`=1" +
					" ORDER BY `event_id` DESC");
		}

	}

	public Cursor getTempEvents(int device_id, int section_id, int zone_id)
	{
		return rawQuery(dbRead, "SELECT DISTINCT `events`.*, `devices`.`device_config_version`" +
				" FROM `events`" +
				" LEFT JOIN `devices` ON `devices`.`device_id`= `events`.`event_device`" +
				" WHERE `event_class_id`=10 AND `event_reason_id`=4 " +
				" AND `event_device`=" + Integer.toString(device_id) +
				" AND `event_section`=" + Integer.toString(section_id) +
				" AND (`event_zone`=" + Integer.toString(zone_id) +
				" OR `event_zone`=" + Integer.toString(zone_id+100) + " AND (`device_config_version`>>8)&0xff==1)" +
				" AND `event_visiblity`=1" +
				" ORDER BY `event_id` DESC");
	}

	public LinkedList<Event> getSupplyAffects(int deviceId, int sectionId, int zoneId){
		Cursor cursor = getSuplyAffectsCursor(deviceId, sectionId, zoneId);
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	public Cursor getSuplyAffectsCursor(int device_id, int section_id, int zone_id){
		if(zone_id == 0 && section_id == 0){
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE " +
					"(`event_class_id`=6 OR " +
					"(`event_class_id`=2 AND `event_reason_id`=50))"
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND (`event_zone`=100 OR `event_zone`=0)" +
					" AND `event_affect`=1 ORDER BY `event_id` DESC"
			);
		}else{
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE " +
					"(`event_class_id`=6 OR " +
					"(`event_class_id`=2 AND `event_reason_id`=50))"
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND `event_zone`=" + Integer.toString(zone_id) +
					" AND `event_affect`= 1 ORDER BY `event_id` DESC"
			);
		}
	}

	public LinkedList<Event> getLevelAffects(int deviceId, int sectionId, int zoneId){
		Cursor cursor = getLevelAffectsCursor(deviceId, sectionId, zoneId);
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}
	public LinkedList<Event> getExtLevelAffects(int deviceId, int sectionId, int zoneId){
			Cursor cursor = getExtLevelAffectsCursor(deviceId, sectionId, zoneId);
			LinkedList<Event> affects = new LinkedList<Event>();
			if(null!=cursor)
			{
				if (0 != cursor.getCount())
				{
					cursor.moveToFirst();
					affects.add(new Event(cursor));
					while (cursor.moveToNext())
					{
						affects.add(new Event(cursor));
					}
					cursor.close();
					return affects;
				}
				cursor.close();
			}
			return null;
		}

	public Cursor getLevelAffectsCursor(int device_id, int section_id, int zone_id){
		if(zone_id == 0 && section_id == 0){
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=7 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND (`event_zone`=100 OR `event_zone`=0)" +
					" AND `event_affect`=1 ORDER BY `event_id` DESC"
			);
		}else{
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=7 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND `event_zone`=" + Integer.toString(zone_id) +
					" AND `event_affect`=1 ORDER BY `event_id` DESC"
			);
		}
	}

	public Cursor getExtLevelAffectsCursor(int device_id, int section_id, int zone_id){
		if(zone_id == 0 && section_id == 0){
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=7 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND (`event_section` =0)" +
					" AND (" +
					"(((`event_zone` =100) OR (`event_zone`=0)) &" +
					"(`event_affect`=1)) " +
					" OR (`event_class_id`=7 AND `event_detector_id`=21)" +
					" OR (`event_class_id`=7 AND `event_detector_id`=22)" +
					")" +
					" AND `event_affect`=1 ORDER BY `event_id` DESC"
			);
		}else{
			return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=7 "
					+ " AND `event_device`=" + Integer.toString(device_id) +
					" AND `event_section`=" + Integer.toString(section_id) +
					" AND `event_zone`=" + Integer.toString(zone_id) +
					" AND `event_affect`=1 ORDER BY `event_id` DESC"
			);
		}
	}

	public LinkedList<Event> getTempAffects(int deviceId, int sectionId, int zoneId){
		Cursor cursor = getTempAffectsCursor(deviceId, sectionId, zoneId);
		LinkedList<Event> affects = new LinkedList<Event>();
		if(null!=cursor)
		{
			if (0 != cursor.getCount())
			{
				cursor.moveToFirst();
				affects.add(new Event(cursor));
				while (cursor.moveToNext())
				{
					affects.add(new Event(cursor));
				}
				cursor.close();
				return affects;
			}
			cursor.close();
		}
		return null;
	}

	public Cursor getTempAffectsCursor(int device_id, int section_id, int zone_id){
		return rawQuery(dbRead, "SELECT * FROM `events` WHERE `event_class_id`=10 AND `event_reason_id`=4 "
				+ " AND `event_device`=" + Integer.toString(device_id) +
				" AND (`event_section`=" + Integer.toString(section_id) +
				" AND `event_zone`=" + Integer.toString(zone_id) +
						" OR `event_zone`=" + Integer.toString(zone_id+100) + " AND `event_class_id`=10 AND `event_reason_id`=4)" +
				" AND `event_affect`=1 ORDER BY `event_id` DESC"
		);
	}

	public long getAlarmEventMaxTimeForZone(Zone zone)
	{
		Cursor c =  rawQuery(dbRead, "SELECT MAX(`event_time`) FROM `events` WHERE `event_device`=" + Integer.toString(zone.device_id) +
				" AND `event_section`=" + Integer.toString(zone.section_id) +
				" AND  `event_zone`=" + Integer.toString(zone.id) +
				" AND `event_class_id`=0 " +
				" AND (`event_active`=0 OR `event_active`=2)" +
				" AND `dropped`=0"); // только для завершающего события, активные тревоги и так обрабатываются в toplayout
		if(null!=c){
			c.moveToFirst();
			long l = c.getLong(0);
			c.close();
			return l;
		}
		return 0;
	}

	public long getAlarmEventMaxTimeForSection(Section section)
	{
		Cursor c =  rawQuery(dbRead, "SELECT MAX(`event_time`) FROM `events` WHERE `event_device`=" + Integer.toString(section.device_id) +
				" AND `event_section`=" + Integer.toString(section.id) +
				" AND `event_class_id`=0 " +
				" AND (`event_active`=0 OR `event_active`=2)" +
				" AND `dropped`=0"); // только для завершающего события, активные тревоги и так обрабатываются в toplayout
		if(null!=c){
			c.moveToFirst();
			long l = c.getLong(0);
			c.close();
			return l;
		}
		return 0;	}


	public void closeEventDroppedStatus(Zone zone)
	{
		execSQL(dbWrite, "UPDATE `events` SET `dropped`=1 WHERE `event_device`=" + Integer.toString(zone.device_id) +
				" AND `event_section`=" + Integer.toString(zone.section_id) +
				" AND `event_zone`=" + Integer.toString(zone.id) +
				" AND `event_class_id`=0 AND `dropped`=0");
	}

	/*18.02.2020*/
	public void deleteDevicesWithInfo()
	{
		execSQL(dbWrite, "DELETE FROM `devices`");
		execSQL(dbWrite, "DELETE FROM `zones`");
		execSQL(dbWrite, "DELETE FROM `users`");
		execSQL(dbWrite, "DELETE FROM `events`");
		execSQL(dbWrite, "DELETE FROM `device_scripts`");
		execSQL(dbWrite, "DELETE FROM `devices_extra`");
	}

	/*18.02.2020*/
	public void deleteAlarms()
	{
		execSQL(dbWrite, "DELETE FROM `alarms`");
		execSQL(dbWrite, "DELETE FROM `alarm_events`");
	}

	public boolean getDeviceUpdate(int device_id)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `events` " +
				"WHERE `event_device`=" + Integer.toString(device_id) +
				" AND  `event_section`=0" +
				" AND  `event_zone`=100" +
				" AND  `event_class_id`=8" +
				" AND `event_reason_id`=7 " +
				" AND (`event_active`=1 OR `event_active`=2)" +
				" AND `event_affect`=1");
		if(null!=c && c.getCount()>0 ){
			c.close();
			return true;
		}
		return false;
	}

	public Cursor getAllZones() {
		return rawQuery(dbRead, "SELECT * FROM `zones`");
	}

	/*EXTRAS*/

	public void addExtra(D3Element d3Element){
		if(null!=d3Element)
		{
			if (d3Element instanceof Device){

			}else if (d3Element instanceof Section) {
				Section section = (Section) d3Element;
				if(null!=section.extra){
					execSQL(dbWrite, "INSERT OR REPLACE INTO `devices_extra` (`zone`, `name`, `value`) " +
							"VALUES ((SELECT `_id` FROM `zones` WHERE `zone_device_id`= " + Integer.toString(section.device_id) + " " +
									"AND `zone_section_id`= " + Integer.toString(section.id) + " " +
									"AND `zone_id`= " + Integer.toString(0) + "), '"+ section.extra.name +"', '" + section.extra.value +"')");
				}else{
					execSQL(dbWrite, "DELETE FROM `devices_extra` WHERE " +
							"`zone`= (SELECT `_id` FROM `zones` WHERE " +
							"`zone_device_id` = " + Integer.toString(section.device_id) +
							" AND `zone_section_id` = " + Integer.toString(section.id) +
							" AND `zone_id`=" + Integer.toString(0) + ")");
				}
			}
			else if(d3Element instanceof Zone){
			}
		}
	}

	public void addExtra(Extra extra){
		execSQL(dbWrite, "INSERT OR REPLACE INTO `devices_extra` (`zone`, `name`, `value`) " +
						"VALUES ((SELECT `_id` FROM `zones` WHERE `zone_device_id`= " + Integer.toString(extra.device_id) + " " +
						"AND `zone_section_id`= " + Integer.toString(extra.section_id) + " " +
						"AND `zone_id`= " + Integer.toString(0) + "), '"+ extra.name +"', '" + extra.value + "')");
	}


	public void deleteExtra(int device_id, int section, int zone)
	{
		execSQL(dbWrite, "DELETE FROM `devices_extra` WHERE " +
				"`zone`= (SELECT `_id` FROM `zones` WHERE " +
				"`zone_device_id` = " + Integer.toString(device_id) +
				" AND `zone_section_id` = " + Integer.toString(section) +
				" AND `zone_id`=" + Integer.toString(zone) + ")");
	}
	//DAHUA CAMERAS
	public void addDahuaCamera(Camera camera)
	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `cameras_dahua` WHERE `sn` LIKE '" + camera.sn + "'");
		if(null!=c){
			if(c.getCount() > 0){
				execSQL(dbWrite, "UPDATE `cameras_dahua` SET " +
						"`name`='" + String.valueOf(camera.name) +
						"`login`='" + String.valueOf(camera.login) +
						"`pass`=" + String.valueOf(camera.pass) +
						"WHERE `sn` LIKE '" + camera.sn + "'");
			}else{
				execSQL(dbWrite, "INSERT INTO `cameras_dahua`(`sn`, `name`, `login`, `pass`) VALUES(?, ?, ? , ?)", new String[]{
						camera.sn,
						camera.name,
						camera.login,
						camera.pass
				});
			}
			c.close();
		}
	}

	public Cursor getDahuaCamerasCursor()
	{
		return rawQuery(dbRead, "SELECT * FROM `cameras_dahua`");
	}


	public Camera[] getAllDahuaCameras()
	{
		Cursor c = getDahuaCamerasCursor();
		if(null!=c) {
			if(c.getCount()> 0){
				c.moveToFirst();
				Camera[] cameras = new Camera[c.getCount()];
				int i =0;
				cameras[i] = new Camera(c, Camera.CType.DAHUA);
				while (c.moveToNext()){
					i++;
					cameras[i] = new Camera(c, Camera.CType.DAHUA);
				}
				c.close();
				return cameras;
			}
			c.close();
		}
		return null;
	}


	public void setDahuaCameraPreview(Camera camera, byte[] data){
		ContentValues contentValues = new ContentValues();
		contentValues.put("preview", data);

		dbWrite.update("cameras_dahua", contentValues, "`sn`=? ", new String[]{camera.sn});
		//		execSQL(dbWrite, "UPDATE `cameras_dahua` SET `preview`=X'"
		//			+ data.toString() + "' WHERE `sn` LIKE '" + camera.sn + "'" );
	}

	public Bitmap getDahuaCameraPreview(Camera camera){
		Cursor c = rawQuery(dbRead, "SELECT `preview` FROM `cameras_dahua` WHERE `sn` LIKE '" + camera.sn + "'");
		if(null!=c){
			if(c.getCount() >0){
				c.moveToFirst();
				Bitmap preview = Func.convertFromDB(c.getString(0));
				c.close();
				return preview;
			}
			c.close();
		}
		return null;
	}

	public Camera getDahuaCameraBySn(String sn)	{
		Cursor c = rawQuery(dbRead, "SELECT * FROM `cameras_dahua` WHERE `sn` LIKE '" + sn + "'");
		if(null!=c){
			if(c.getCount() > 0){
				c.moveToFirst();
				Camera camera = new Camera(c, Camera.CType.DAHUA);
				c.close();
				return camera;
			}
			c.close();
		}
		return null;
	}

	public boolean deleteDahuaCamera(Camera camera_dahua)
	{
		return execSQL(dbWrite, "DELETE FROM `cameras_dahua` WHERE `sn` LIKE '" + camera_dahua.sn + "'");
	}

	public boolean renameDahuaCamera(String camera_dahua_id, String trim)
	{
		return execSQL(dbWrite, "UPDATE `cameras_dahua` SET `name`='" + String.valueOf(trim) +
				"' WHERE `sn` LIKE '" + camera_dahua_id + "'");
	}
}

