package biz.teko.td.SHUB_APP.Activity.SwipeBack;

/**
 * @author Yrom
 */
public interface SwipeBackActivityBaseX {
	/**
	 * @return the SwipeBackLayout associated with this activity.
	 */
	public abstract SwipeBackLayoutX getSwipeBackLayout();

	public abstract void setSwipeBackEnable(boolean enable);

	/**
	 * Scroll out contentView and finish the activity
	 */
	public abstract void scrollToFinishActivity();

}
