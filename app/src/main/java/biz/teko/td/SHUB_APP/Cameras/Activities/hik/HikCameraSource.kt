package biz.teko.td.SHUB_APP.Cameras.Activities.hik

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET


interface HikCameraSource {
    @GET("/api/hpcgw/v1/token/get")
    fun getToken(): Observable<ResponseBody>
}