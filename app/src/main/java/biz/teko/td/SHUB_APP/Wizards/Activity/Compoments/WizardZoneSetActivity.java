package biz.teko.td.SHUB_APP.Wizards.Activity.Compoments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.ZoneSetViewPagerAdapter;

/**
 * Created by td13017 on 07.06.2017.
 */

public class WizardZoneSetActivity extends BaseActivity
{
	private Context context;
	private ZoneSetViewPagerAdapter adapter;
	private ViewPager pager;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;

	private Zone zone;

	private int sending = 0 ;

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						sending = 0;
						break;
					case -1:
						Func.pushToast(context, context.getString(R.string.COMMAND_NOT_BEEN_SEND), (Activity) context);
						sending = 0;
						break;
					default:
						sending = 0;
						break;
				}
			}
		}
	};

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));

		}
	};

	private BroadcastReceiver newZoneRegisteredReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(pager.getCurrentItem() == ZoneSetViewPagerAdapter.DEVICE_SET_PAGES_COUNT - 1)
			{
				if(0!=intent.getIntExtra("zone", 0)) setNextPage();
			}
		}
	};

	private BroadcastReceiver interactiveReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int page =  pager.getCurrentItem();
			if (page == ZoneSetViewPagerAdapter.DEVICE_SET_PAGES_COUNT - 3)
			{
				if(zone.device == intent.getIntExtra("device", -1))
				{
					int status = intent.getIntExtra("status", -1);
					if (status == 0)
					{
						String jdata = intent.getStringExtra("jdata");
						if (!jdata.equals(""))
						{
							try
							{
								JSONObject jsonObject = new JSONObject(jdata);
								int uid_type = jsonObject.getInt("uid_type");
								if (uid_type == 1)
								{
									String uid = jsonObject.getString("uid");
									int uid_format = Integer.parseInt(uid.substring(0, 2), 16);
									setUID(uid);
									setUIDFormat(uid_format);
									switch (uid_format){
										case 1:
											char[] chars = uid.toCharArray();
											setZoneModel(Character.digit(chars[chars.length - 2 ],16));
											setNextPage();
											break;
										case 2:
											String ss =uid.substring(uid.length()-4);
											String le = "";
											for(int i = ss.length()-2; i >=0; i-=2){
												le += ss.substring(i, i+2);
											}
											int model = Integer.parseInt(le, 16);
											if(model == 8231|| model == 8731){
												Func.nShowMessage(context, context.getString(R.string.WIZ_ZONE_RELAY_INSTEAD_OF_SENSOR));
											}else{
												setZoneModel(model);
												setNextPage();
											}
											break;
									}
								} else
								{
									if (uid_type == 2)
									{
										Func.nShowMessage(context, getString(R.string.WIZ_ERROR_USER_INSTEAD_ZONE));
									} else
									{
										finish();
									}
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}

						} else
						{
							Func.nShowMessage(context, getString(R.string.WIZ_ERROR_INTERACTIVE_TIMEOUT));
						}
					}
					refreshViews();
				}
			}
		}
	};

	@Override
	protected void onStart()
	{
		super.onStart();
	}

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_zone_setup);
		context = this;

		serviceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.WIZ_DEVICE_ADD_TITLE);

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		ImageView closeButton = (ImageView) findViewById(R.id.wizCloseButton);
		closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});

//		selSiteId = getIntent().getIntExtra("site_id", -1);
//		selDeviceId = getIntent().getIntExtra("device_id", -1);
//		selSectionId = getIntent().getIntExtra("section_id", -1);

		zone = new Zone(getIntent().getIntExtra("site_id", -1), getIntent().getIntExtra("device_id", -1), getIntent().getIntExtra("section_id", -1));

		zone.con_type = getIntent().getIntExtra("con_type", Const.WIRED);

		adapter = new ZoneSetViewPagerAdapter(getSupportFragmentManager());
		pager = (ViewPager) findViewById(R.id.deviceSetViewPager);
		pager.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true;
			}
		});
		pager.setAdapter(adapter);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
		registerReceiver(interactiveReceiver, new IntentFilter(D3Service.BROADCAST_INTERACTIVE_STATUS));
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		registerReceiver(newZoneRegisteredReceiver, new IntentFilter(D3Service.BROADCAST_ZONE_ADD));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(interactiveReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
		unregisterReceiver(newZoneRegisteredReceiver);
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	@Override
	public void onBackPressed() {
		Func.hideKeyboard((WizardZoneSetActivity)context);
		if (pager.getCurrentItem() == 0) {
			super.onBackPressed();
		} else {
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position - 1);
		}
	}

	public Zone getZone(){
		return this.zone;
	}

	public void setZone(Zone zone){
		this.zone = zone;
	}

//	public int getSiteId(){
//		return zone!=null?zone.site:-1;
//	}

	public void setSiteID(int siteId){
		this.zone.site = siteId;
	}

//	public int  getDeviceId(){
//		return zone!=null?zone.device:-1;
//	}

	public void  setDeviceid(int device_id){
		this.zone.device = device_id;
	}

//	public int getSectionId(){
//		return zone!=null?zone.section:-1;
//	}

	public void setSectionId(int sectionId){
		this.zone.section = sectionId;
	}

//	public int getSectionType()
//	{
//		return zone!=null? this.zone.sec_type:-1;
//	}

	public void setSectionType(int setSectionType)
	{
		this.zone.sec_type = setSectionType;
	}

	/*wireless OR wired*/
//	public int getZoneConType()
//	{
//		return zone!=null? zone.con_type:-1;
//	}

//	public void setZonConType(int setZoneConType)
//	{
//		this.zone.con_type = setZoneConType;
//	}


	public void setUID(String UID)
	{
		this.zone.uid = UID;
	}

//	public String getUID(){
//		return this.UID;
//	}

	public void setUIDFormat(int UIDFormat)
	{
		this.zone.uid_format = UIDFormat;
	}

	public void setWiredInput(int input)
	{
		this.zone.input = input;
	}

	public void setWiredType(int type){
		//reset in model change OR wired_type change

		if (zone.wired_type != type)
		{
			//забиваем дефолтное значение detecotor&alarm
			DWIType dwiType = D3Service.d3XProtoConstEvent.getDWIType(type);
			DType dType = dwiType.getDefDetectorType();
			if (null != dType)
			{
				this.zone.detector = dType.id;
				AType alarmType = dType.getDefaultAType();
				if (null != alarmType)
				{
					this.zone.alarm = alarmType.id;
				}else{
					this.zone.alarm = 0;
				}
			}else{
				this.zone.detector = 0;
				this.zone.alarm = 0;
			}
			this.zone.section = -1;
		}
		this.zone.wired_type = type;

	}


//	public void setZoneDelay(int zoneDelay)
//	{
//		this.zone.delay = zoneDelay;
//	}

	public void setZoneModel(int zoneModel){
		this.zone.model = zoneModel;
		this.zone.detector = 0;
		this.zone.alarm = 0;
		ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(zoneModel);
		if(null!=zType)
		{
			DType dType = zType.getDefDetectorType();
			if (null != dType)
			{
				this.zone.detector = dType.id;
				AType alarmType = dType.getDefaultAType();
				if (null != alarmType)
				{
					this.zone.alarm = alarmType.id;
				}
			}
		}
	}


	public void  setZoneAlarm(int alarm){
		this.zone.alarm = alarm;
	}

	public void setZoneDetector(int detector){
		this.zone.detector = detector;
	}

	public void refreshViews(){
		adapter.notifyDataSetChanged();
	}

	public void setNextPage()
	{
		if(pager.getCurrentItem() != ZoneSetViewPagerAdapter.DEVICE_SET_PAGES_COUNT - 1)
		{
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position + 1);
			refreshViews();
		}else{
			Intent activityIntent = new Intent(context, WizardZoneSetFinishActivity.class);
			activityIntent.putExtra("site", this.zone.site );
			activityIntent.putExtra("device", this.zone.device );
			activityIntent.putExtra("section", this.zone.section);
			activityIntent.putExtra("con_type", this.zone.con_type);
			activityIntent.putExtra("wizard", 2);
			startActivityForResult(activityIntent, D3Service.ADD_ZONE_IN_SECTION);
			finish();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.ADD_ZONE_IN_SECTION){
			if(resultCode == RESULT_OK){
				int sectionId = data.getIntExtra("section", 0);
				Intent intent = new Intent();
				intent.putExtra("section", sectionId);
				setResult(RESULT_OK, intent);
				finish();
			}
			else{
				onBackPressed();
			}
		}else{
			onBackPressed();
		}
	}

	public  class  Zone {
		public int site;
		public int device;
		public int section;

		public int con_type;
		public int sec_type;

		public String name;
		public String uid;
		public int uid_format;
		public int delay;
		public int model;

		//wired
		public int input;
		public int wired_type;

		//complex detector
		public int detector;
		public int alarm;

		private Zone(){

		}

		private Zone(int site, int device, int section){
			this.site = site;
			this.device = device;
			this.section = section;
		}
	}


}


