package biz.teko.td.SHUB_APP.Utils.NViews;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class NFilterView extends FrameLayout
{
	private Context context;

	private int layout;

	private boolean expand = false;

	private boolean animationEnd;

	private FrameLayout rootFrame;
	private FrameLayout inputFrame;

	public NFilterView(@NonNull Context context)
	{
		super(context);
		this.context  = context;
	}


	public NFilterView(@NonNull Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setup();
		bind();
	}

	private void setup()
	{
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(layout, this, true);

		rootFrame = (FrameLayout) findViewById(R.id.nFilterFrame);
		inputFrame = (FrameLayout) findViewById(R.id.nInputFrame);
	}

	private void bind()
	{
		if(null!=rootFrame)rootFrame.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				resize();
			}
		});
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NFilterView, 0, 0);
		try
		{
			layout = a.getResourceId(R.styleable.NFilterView_NFilterViewLayout, 0);
		} finally
		{
			a.recycle();
		}
	}

	private void resize()
	{
		if(!expand){
			resizeWithAnimation(rootFrame,500, 1);
		}else{
			resizeWithAnimation(rootFrame,500, 0);
		}
		expand=!expand;
	}

	private void resizeWithAnimation(final View view, int duration, int type) {
		int initWidth =0;
		int initHeight =0;
		int targetWidth = 0;
		int targetHeight = 0;

//		int availableWidth = ((Activity)context).getWindowManager().getDefaultDisplay().getWidth();

		//		int availableWidth = getParentWidth(filterFrame);

		int widthSpec = View.MeasureSpec.makeMeasureSpec(((Activity)context).getWindowManager().getDefaultDisplay().getWidth(), View.MeasureSpec.AT_MOST);
		int heightSpec = View.MeasureSpec.makeMeasureSpec(((Activity)context).getWindowManager().getDefaultDisplay().getHeight(), View.MeasureSpec.UNSPECIFIED);

		inputFrame.measure(widthSpec, heightSpec);

		int measuredHeight = inputFrame.getMeasuredHeight();
		int measuredWidht  = inputFrame.getMeasuredWidth();

		switch (type){
			case 0:
				inputFrame.setVisibility(View.GONE);
				initWidth = measuredWidht;
				targetWidth = inputFrame.getWidth();
				initHeight = measuredHeight;
				targetHeight = inputFrame.getHeight();
				break;
			case 1:
				inputFrame.setVisibility(View.VISIBLE);

				initWidth = inputFrame.getWidth();
				targetWidth =measuredWidht;
				initHeight = inputFrame.getHeight();
				targetHeight = measuredHeight;
				break;
		}


		final int distance = targetWidth - initWidth;

		view.setVisibility(View.VISIBLE);


		int finalTargetWidth = targetWidth;
		int finalInitWidth = initWidth;
		int finalTargetHeight = targetHeight;
		int finalInitHeight = initHeight;


		Animation animation = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if(!animationEnd)
				{
					if (interpolatedTime == 1 && (finalTargetWidth == 0 || finalTargetHeight == 0))
					{
						view.setVisibility(View.GONE);
					}
					view.getLayoutParams().width = (int) (finalInitWidth + distance * interpolatedTime);
					view.getLayoutParams().height = (int) (finalInitHeight + distance * interpolatedTime);
					view.requestLayout();
					if(interpolatedTime == 1){
						animationEnd  = true;
						FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) view.getLayoutParams();
						lp.width = LayoutParams.WRAP_CONTENT;
						lp.height = LayoutParams.WRAP_CONTENT;
						view.setLayoutParams(lp);
					}
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		animation.setAnimationListener(new Animation.AnimationListener()
		{
			@Override
			public void onAnimationStart(Animation animation)
			{
				animationEnd = false;
			}

			@Override
			public void onAnimationEnd(Animation animation) {}

			@Override
			public void onAnimationRepeat(Animation animation) {}
		});

		animation.setDuration(duration);
		view.startAnimation(animation);
	}
}
