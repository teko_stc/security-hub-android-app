package biz.teko.td.SHUB_APP.Cameras;

import android.content.Context;
import android.net.Uri;
import android.view.SurfaceView;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource;
import com.google.android.exoplayer2.util.Util;

import java.util.Map;

public class ExoPlayerVideoHandler
{
	public interface OnVideoListener{
		void OnLoaded();
	}

	private static ExoPlayerVideoHandler instance;
	private boolean playWhenReady = true;
	private int currentWindow = 0;
	private long playbackPosition = 0;

	public static ExoPlayerVideoHandler getInstance(){
		if(instance == null){
			instance = new ExoPlayerVideoHandler();
		}
		return instance;
	}
	private ExoPlayer exoPlayer;
	private Uri playerUri;

	private ExoPlayerVideoHandler(){}

	public void prepareExoPlayerForUri(Context context, Uri uri,
									   PlayerView exoPlayerView, OnVideoListener onVideoListener, Map<String, String> headers){
		if(context != null && uri != null && exoPlayerView != null){
			if(!uri.equals(playerUri) || exoPlayer == null){
				exoPlayer = new ExoPlayer.Builder(context).build();

				playerUri = uri;
				MediaSource mediaSource = buildMediaSource(context, playerUri, headers);
				exoPlayer.setMediaSource(mediaSource);
				exoPlayer.prepare();
				exoPlayer.setPlayWhenReady(playWhenReady);
				exoPlayer.seekTo(currentWindow, playbackPosition);
			}
			exoPlayer.addListener(new Player.EventListener()
			{
				@Override
				public void onPlayerStateChanged(boolean playWhenReady, int playbackState)
				{
						if(null!=onVideoListener)onVideoListener.OnLoaded();
				}
			});
			exoPlayer.setVideoSurfaceView((SurfaceView)exoPlayerView.getVideoSurfaceView());
			exoPlayerView.requestFocus();
			//			player.clearVideoSurface();
//			player.seekTo(player.getCurrentPosition() + 1);
			exoPlayerView.setPlayer(exoPlayer);
		}
	}

	private MediaSource buildMediaSource(Context context, Uri uri, Map<String, String> headers) {
		String userAgent = Util.getUserAgent(context, context.getPackageName());

		DefaultHttpDataSource.Factory httpDataSourceFactory = new DefaultHttpDataSource.Factory()
				.setAllowCrossProtocolRedirects(true)
				.setUserAgent(userAgent);

		if(headers != null && !headers.isEmpty()){
			httpDataSourceFactory.setDefaultRequestProperties(headers);
		}

		return new ProgressiveMediaSource.Factory(httpDataSourceFactory)
				.createMediaSource(MediaItem.fromUri(uri));
	}

	public void releaseVideoPlayer(){
		if(exoPlayer != null)
		{
//			playbackPosition = player.getCurrentPosition();
//			currentWindow = player.getCurrentWindowIndex();
			exoPlayer.release();
		}
		exoPlayer = null;
	}
}