package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class NImageView extends androidx.appcompat.widget.AppCompatImageView
{
	private static final int[] STATE_BLUE = {R.attr.state_blue};

	private boolean blue;

	public NImageView(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		parseArrtibutes(attrs);
	}

	private void parseArrtibutes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NImageView, 0, 0);
		try{
			blue = a.getBoolean(R.styleable.NImageView_NImageViewBlue, false);
		}finally
		{
			a.recycle();
		}
	}

	@Override
	public int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableStates = super.onCreateDrawableState(extraSpace+1);
		if(blue) mergeDrawableStates(drawableStates, STATE_BLUE);
		return drawableStates;
	}
}
