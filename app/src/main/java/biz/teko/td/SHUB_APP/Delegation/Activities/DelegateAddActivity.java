package biz.teko.td.SHUB_APP.Delegation.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

/**
 * Created by td13017 on 22.06.2017.
 */

public class DelegateAddActivity extends BaseActivity {
    private Context context;
    private int sending;
    private ServiceConnection serviceConnection;
    private D3Service myService;
    private boolean bound;

    private BroadcastReceiver siteDelegationResultReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int result = intent.getIntExtra("result", 0);
            if (result > 0) {
                Func.pushToast(context, getString(R.string.SUCCESS_OBJECT_DELEGATED), (DelegateAddActivity) context);
                onBackPressed();
                overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
            } else {
                Func.pushToast(context, Func.handleResult(context, result), (DelegateAddActivity) context);
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delegate_add);

        context = this;
        final DBHelper dbHelper = DBHelper.getInstance(context);

        setToolbar();

        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = (D3Service) myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };

        final NEditText editCode = (NEditText) findViewById(R.id.deleg_add_edit_code);
        editCode.showKeyboard();
        NActionButton textPaste = (NActionButton) findViewById(R.id.deleg_add_text_paste);
        textPaste.setOnButtonClickListener(action -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clipData = clipboard.getPrimaryClip();
            if (null != clipData && clipData.getItemCount() != 0) {
                String buffer = clipData.getItemAt(0).getText().toString();
                editCode.setText(buffer);
                editCode.setFocus();
            } else {
                Func.pushToast(context, getString(R.string.BUFFER_IS_EMPTY), (DelegateAddActivity) context);
            }
        });

        NActionButton delegGoButton = (NActionButton) findViewById(R.id.deleg_add_button_go);
        delegGoButton.setOnButtonClickListener(action -> {
            String name = editCode.getText().toString();
            if (!name.equals("")) {
                UserInfo userInfo = dbHelper.getUserInfo();
                JSONObject message = new JSONObject();
                try {
                    message.put("user_code", name);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                D3Request d3Request = D3Request.createMessage(userInfo.roles, "DELEGATE_REQ", message);
                if (d3Request.error == null) {
                    sendMessageToServer(d3Request);
                } else {
                    Func.pushToast(context, d3Request.error, (Activity) context);
                }
            } else {
                Func.pushToast(context, getString(R.string.DEL_NO_CODE), (DelegateAddActivity) context);
            }
        });
    }

    private void setToolbar() {
        NTopToolbarView toolbar = (NTopToolbarView) findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = new Intent(this, D3Service.class);
        bindService(intent, serviceConnection, 0);
        registerReceiver(siteDelegationResultReceiver, new IntentFilter(D3Service.BROADCAST_DELEGATE_REQ));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(serviceConnection);
        unregisterReceiver(siteDelegationResultReceiver);
    }

    private boolean sendMessageToServer(D3Request d3Request) {
        if (sending == 0) {
            if (null != myService) {
                if (myService.send(d3Request.toString())) {
                    return true;
                }
            } else {
                Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
            }
        } else {
            Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
        }
        return false;
    }

    ;
}
