package biz.teko.td.SHUB_APP.Cameras.Activities.IV;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.exoplayer2.ui.PlayerView;

import java.util.HashMap;
import java.util.Map;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.ExoPlayerVideoHandler;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 13.03.2018.
 */

public class CameraFullScreenVideoActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;

	private boolean destroyVideo = true;
	private String url = "";
	private PlayerView playerView;
	private ImageView fullScreenIcon;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		context = this;
		dbHelper = DBHelper.getInstance(context);

		setContentView(R.layout.activity_video_full_screen);
		playerView = (PlayerView) findViewById(R.id.video_view_full_screen);
		fullScreenIcon = (ImageView) findViewById(R.id.exo_fullscreen_icon);
		fullScreenIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_fullscreen_exit_48_brand));

		url = getIntent().getStringExtra("url");
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		hideSystemUi();
		if(url != null && playerView != null)
		{
			String accessToken = dbHelper.getUserInfoIVToken();
			Map<String, String> headersMap = new HashMap<>();
			if(accessToken != null && !accessToken.isEmpty()){
				headersMap.put("Authorization", "Bearer " + accessToken);
			}

			ExoPlayerVideoHandler.getInstance()
					.prepareExoPlayerForUri(this, Uri.parse(url), playerView, new ExoPlayerVideoHandler.OnVideoListener()
					{
						@Override
						public void OnLoaded()
						{

						}
					}, headersMap);



			findViewById(R.id.exo_fullscreen_icon).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					onBackPressed();
				}
			});
		}
	}

	@Override
	public void onBackPressed()
	{
		destroyVideo = false;
		super.onBackPressed();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		ExoPlayerVideoHandler.getInstance().releaseVideoPlayer();
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		if(destroyVideo)
		{
			ExoPlayerVideoHandler.getInstance().releaseVideoPlayer();
		}
	}

	@SuppressLint("InlinedApi")
		private void hideSystemUi() {
		playerView.setSystemUiVisibility(
				View.SYSTEM_UI_FLAG_LOW_PROFILE
				| View.SYSTEM_UI_FLAG_FULLSCREEN
				| View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				 );
	}
}

