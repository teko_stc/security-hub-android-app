package biz.teko.td.SHUB_APP.Activity;

import static biz.teko.td.SHUB_APP.D3.D3Service.BROADCAST_DEVICE_ADD;
import static biz.teko.td.SHUB_APP.D3.D3Service.OPEN_FROM_LOGIN;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Activities.HistoryActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardPinActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardPinEnterActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardWelcomeActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

/**
 * Created by td13017 on 09.06.2016.
 */
public class StartUpActivity extends AppCompatActivity {
	private static final int TIMEOUT_TO_SERVICE_STARTED_BROADCAST = 5;
	private static final int TIMEOUT_TO_FETCH_DATA_FROM_SERVER = 10;

	private static final String D3SERVICE_DEAD = "D3Service dead";
	private static final int SERVICE_BEEN_STOPED = 16;
	private final DBHelper dbHelper = DBHelper.getInstance(this);
	private boolean manualLogin = false;
	private StartServiceTask startServiceTask;
	private boolean serviceAlive = false;
	private Intent in;
	private boolean beenStopped;

	private boolean protocolCompare = true;
	private Context context;
	private PrefUtils prefUtils;
	private Disposable devicesReadyDisposable;
	Subject<Boolean> devicesReadySubject = BehaviorSubject.create();

//	private final BroadcastReceiver devicesAddingReceiver = new BroadcastReceiver() {
//		@Override
//		public void onReceive(Context context, Intent intent) {
//			devicesReadySubject.onNext(false);
//		}
//	};


	private final BroadcastReceiver devicesAddedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			devicesReadySubject.onNext(true);
		}
	};

	private final BroadcastReceiver newProtocolVersionReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			protocolCompare = false;
		}
	};


	private final BroadcastReceiver serviceDeadReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Func.nShowMessage(context, context.getString(R.string.DIALOG_START_SERVICE_PLS), (value, b) -> finish());
		}
	};

	@Override
	protected void onNewIntent(Intent intent) {
		Func.log_v("D3ServiceLog", "LIFECIRCLE: STARTUP ONNEWINTENT");
		if (((App) this.getApplication()).getBlockedStatus()) {
			ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
			if (null != am) {
				ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
				String currentActivity = cn.getClassName();
				String wizardPinAddActivity = WizardPinActivity.class.getName();
				String wizardPinEnterActivity = WizardPinEnterActivity.class.getName();
				if ((null != dbHelper.getUserInfo())
						&& (D3Service.OPEN_HISTORY_FROM_STATUS_BAR == intent.getIntExtra("FROM", -1)))
				{
					if(prefUtils.getPinEnabled())
					{
						if (currentActivity.equals(wizardPinAddActivity))
						{
							Intent activityIntent;
							activityIntent = new Intent(getBaseContext(), WizardPinActivity.class);
							activityIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
							startActivity(activityIntent);
							overridePendingTransition(R.animator.enter, R.animator.exit);
						} else
						{
							if (currentActivity.equals(wizardPinEnterActivity))
							{
								Intent activityIntent;
								activityIntent = new Intent(getBaseContext(), WizardPinEnterActivity.class);
								activityIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
								activityIntent.putExtra("FROM", D3Service.OPEN_FROM_LOGIN);
								startActivity(activityIntent);
								overridePendingTransition(R.animator.enter, R.animator.exit);
							} else
							{
								Intent activityIntent;
								activityIntent = new Intent(getBaseContext(), HistoryActivity.class);
								activityIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
								startActivity(activityIntent);
								overridePendingTransition(R.animator.enter, R.animator.exit);
							}
						}
					}else{
						Intent activityIntent;
						activityIntent = new Intent(getBaseContext(), MainNavigationActivity.class);
						//activityIntent = new Intent(getBaseContext(), MainActivity.class);
						activityIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
						startActivity(activityIntent);
						overridePendingTransition(R.animator.enter, R.animator.exit);
					}

				} else
				{
					super.onNewIntent(intent);
				}
			}
		}
	}
	private final BroadcastReceiver serviceAliveReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(final Context context, Intent intent)
		{
			serviceAlive = true;

			if (manualLogin)
				devicesReadySubject.onNext(false);
//				setSubjectWithTimeout(TIMEOUT_TO_GET_DEVICES_STARTED_FETCHING_BROADCAST);
//				setSubjectWithTimeout(TIMEOUT_TO_FETCH_DATA_FROM_SERVER);
			else
				devicesReadySubject.onNext(true);
		}
	};

	private Runnable doNextActivity = () -> {
		// Intent to jump to the next activity
		if (serviceAlive) {
			UserInfo userInfo = dbHelper.getUserInfo();
			Intent activityIntent = new Intent();
			if (null != userInfo) {
				in = getIntent();
				if (prefUtils.getPinEnabled()) {
					if (null != userInfo.pinCode && 0 != userInfo.pinCode.length()) {
						activityIntent = new Intent(getBaseContext(), WizardPinEnterActivity.class);
						activityIntent.putExtra("FROM", in.getIntExtra("FROM", -1));
						if (beenStopped) {
							activityIntent.putExtra("SERVICE", SERVICE_BEEN_STOPED);
						}
					} else {
						activityIntent = new Intent(getBaseContext(), WizardPinActivity.class);
						activityIntent.putExtra("FROM", in.getIntExtra("FROM", -1));
						if (beenStopped) {
							activityIntent.putExtra("SERVICE", SERVICE_BEEN_STOPED);
						}
					}
				} else {
					if ((0 == dbHelper.getSitesCount()) && ((((userInfo.roles & Const.Roles.DOMAIN_LAWYER) != 0) || ((userInfo.roles & Const.Roles.ORG_LAWYER) != 0)))) {
						activityIntent = new Intent(getBaseContext(), WizardControllerSetActivity.class);
						activityIntent.putExtra("from", 0);
					} else {
						activityIntent = new Intent(getBaseContext(), MainNavigationActivity.class);
						//activityIntent = new Intent(getBaseContext(), MainActivity.class);
						activityIntent.putExtra("FROM", D3Service.OPEN_FROM_STARTUP_ACTIVITY_AFTER_LOGIN);
					}
				}
			} else {
				if (protocolCompare) {
					activityIntent = new Intent(getBaseContext(), WizardWelcomeActivity.class);
					activityIntent.putExtra("FROM", -1);
					if (beenStopped) {
						activityIntent.putExtra("SERVICE", SERVICE_BEEN_STOPED);
					}
				}
			}
			finish();
			startActivity(activityIntent);
			overridePendingTransition(R.animator.enter, R.animator.exit);
		}
		// so the splash activity goes away
	};

	public class StartServiceTask extends AsyncTask<Object, Object, Void> {

		@Override
		protected Void doInBackground(Object... params) {
			if ((!Func.isServiceRunning(context, D3Service.class))
					|| (null == D3Service.d3XProtoConstEvent)
			) {
				Func.startD3Service(context, "StartUp Activity");
				beenStopped = true;
			} else {
				getApplicationContext().sendBroadcast(new Intent(D3Service.BROADCAST_D3SERVICE_ALIVE));
				beenStopped = false;
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
//			Long startTime = System.currentTimeMillis();
//			while ((!serviceAlive)&((System.currentTimeMillis() - startTime)< 15000)){}
			new Thread() {
				@Override
				public void run() {
					SystemClock.sleep(10000);
					if (!serviceAlive) {
						getApplicationContext().sendBroadcast(new Intent(D3SERVICE_DEAD));
					}
				}
			}.start();
		}

	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Func.log_v("D3ServiceLog", "LIFECIRCLE: STARTUP ONCREATE");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_startup);
		manualLogin = getIntent().getIntExtra("FROM", -1) == OPEN_FROM_LOGIN;
		context = this;
		this.prefUtils = PrefUtils.getInstance(context);
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}


	@Override
	protected void onStart() {
		super.onStart();
		/*DETECT APP RE-OPENING TO SHOW NEW CRITICAL EVENTS ON MAIN ACTIVITY*/
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		if (Func.getBooleanSPDefFalse(sp, "all_events_were_seen")) {
			sp.edit().putBoolean("all_events_were_seen", false).apply();
			Func.log_d("D3Service", "All events were seen in startup");

		}
	}

	private void setSubjectWithTimeout(int timeout) {
		if (devicesReadyDisposable != null)
			devicesReadyDisposable.dispose();
//		showProgress(true);
		devicesReadyDisposable = (devicesReadySubject.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.timeout(timeout, TimeUnit.SECONDS)
				.onErrorReturnItem(true)
				.subscribe(dataReceived -> {
					showProgress(!dataReceived);
					if (dataReceived) {
						doNextActivity.run();
						devicesReadySubject.onComplete();
					} else {
						setSubjectWithTimeout(TIMEOUT_TO_FETCH_DATA_FROM_SERVER);
					}
				}, throwable -> {
					if (throwable instanceof TimeoutException) {
						doNextActivity.run();
					}
				},
				doNextActivity::run
			));
	}

	private void showProgress(Boolean b) {
		findViewById(R.id.constraintLayout_device_data_loading).setVisibility(b? View.VISIBLE : View.GONE);
	}


	@Override
	protected void onPause() {
		super.onPause();
		if (startServiceTask != null)
			startServiceTask.cancel(true);
		if (devicesReadyDisposable != null)
			devicesReadyDisposable.dispose();
//		unregisterReceiver(devicesAddingReceiver);
		unregisterReceiver(devicesAddedReceiver);
		unregisterReceiver(serviceAliveReceiver);
		unregisterReceiver(serviceDeadReceiver);
		unregisterReceiver(newProtocolVersionReceiver);
	}

	@Override
	protected void onResume() {
		Func.log_v("D3ServiceLog", "LIFECIRCLE: STARTUP ONRESUME");
		super.onResume();
		findViewById(R.id.constraintLayout_device_data_loading).setVisibility(View.GONE);
//		registerReceiver(devicesAddingReceiver, new IntentFilter(BROADCAST_DATA_FROM_SERVER_FETCH));
		registerReceiver(devicesAddedReceiver, new IntentFilter(BROADCAST_DEVICE_ADD));
		registerReceiver(serviceDeadReceiver, new IntentFilter(D3SERVICE_DEAD));
		registerReceiver(serviceAliveReceiver, new IntentFilter(D3Service.BROADCAST_D3SERVICE_ALIVE));
		registerReceiver(newProtocolVersionReceiver, new IntentFilter(D3Service.BROADCAST_NEW_PROTOCOL_VERSION));
		setSubjectWithTimeout(TIMEOUT_TO_SERVICE_STARTED_BROADCAST);
		startServiceTask = new StartServiceTask();
		startServiceTask.execute();
	}
	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleHelper.onAttach(base));
	}
}
