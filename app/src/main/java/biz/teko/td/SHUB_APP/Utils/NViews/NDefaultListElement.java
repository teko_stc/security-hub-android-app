package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class NDefaultListElement extends LinearLayout {
    private int layout;
    private String title, summary;
    private TextView textTitleView, textSummary;
    private Drawable image;
    private ImageView imageView;
    private OnRootClickListener onRootClickListener;
    private LinearLayout rootView;
    private Drawable background;

    private static final int DEFAULT_DRAWABLE = R.drawable.n_background_devices_list_selector;

    public void setOnRootClickListener(OnRootClickListener onRootClickListener) {
        this.onRootClickListener = onRootClickListener;
    }

    public interface OnRootClickListener {
        void onRootClick();
    }

    public NDefaultListElement(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(attrs);
        setupView();
        bindView();
    }


    private void setupView() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutInflater.inflate(layout, this, true);

        textTitleView = findViewById(R.id.nText);
        textSummary = findViewById(R.id.nTextSummary);
        imageView = findViewById(R.id.nImage);
        rootView = this;
    }

    private void bindView() {
        if (null != textTitleView && null != title) textTitleView.setText(title);
        if (null != imageView && null != image) imageView.setImageDrawable(image);
        if (summary != null && textSummary != null) textSummary.setText(summary);
        if (null != rootView) rootView.setOnClickListener(view -> {
            if (null != onRootClickListener) onRootClickListener.onRootClick();
        });
        setBackground(background == null ? getResources().getDrawable(DEFAULT_DRAWABLE) : background);
    }

    private void parseAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NDefaultListElement, 0, 0);
        try {
            layout = a.getResourceId(R.styleable.NDefaultListElement_NDefaultListElementLayout, 0);
            title = a.getString(R.styleable.NDefaultListElement_NDefaultListElementTitle);
            summary = a.getString(R.styleable.NDefaultListElement_NDefaultListElementSummary);
            image = a.getDrawable(R.styleable.NDefaultListElement_NDefaultListElementImage);
            background = a.getDrawable(R.styleable.NDefaultListElement_NDefaultListElementBackground);
        } finally {
            a.recycle();
        }
    }

    public void setSummaryText(String summary) {
        this.summary = summary;
        if (summary != null && textSummary != null)
            textSummary.setText(summary);
    }

    public void setTitle(String title, int size) {
        this.title = title;
        if (textTitleView != null)
            textTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
        setTitleView();
    }

    private void setTitleView() {
        if (null != textTitleView && null != title) textTitleView.setText(title);
    }

    public void setTitle(String title) {
        this.title = title;
        setTitleView();
    }

    public void setImageLevel(int level) {
        if (imageView != null)
            imageView.setImageLevel(level);
    }
}
