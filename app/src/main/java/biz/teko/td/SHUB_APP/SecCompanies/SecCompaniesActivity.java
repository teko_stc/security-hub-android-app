package biz.teko.td.SHUB_APP.SecCompanies;

import android.content.Context;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SecCompanies.Adapters.SecCompAdapter;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;

/**
 * Created by td13017 on 26.02.2018.
 */

public class SecCompaniesActivity extends BaseActivity {
    private final static int DEF_POSITION = 8;
    private Context context;
    private NTopToolbarView toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_seccompanies);
        getSwipeBackLayout().setEnableGesture(false);

        initView();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.companiesRecycler);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        SecCompAdapter secCompAdapter = new SecCompAdapter(D3Service.sec.companies, this);
        recyclerView.setAdapter(secCompAdapter);
    }

    private void initView() {
        toolbar = (NTopToolbarView) findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }
}
