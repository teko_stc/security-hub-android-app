package biz.teko.td.SHUB_APP.SectionsTabs.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cursoradapter.widget.ResourceCursorAdapter;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraRecordViewActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 27.09.2016.
 * DEPRECATED 11.18.2020
 */
public class SectionsEventListAdapter extends ResourceCursorAdapter
{
	Cursor cursor;
	DBHelper dbHelper ;
	int id;
	Context context;

	public SectionsEventListAdapter(Context context, int id, Cursor cursor, int flags)
	{
		super(context, id, cursor, flags);
		this.id = id;
		this.cursor = cursor;
		this.context = context;
	}

	public void update(Cursor cursor)
	{
		this.changeCursor(cursor);
	}

	private void setTextViewText(View view, Context context, int id, String string){
		TextView textView = (TextView) view.findViewById(id);
		if(textView!=null){
			textView.setText(string==null? "" : string);
		}
	}



	@Override
	public void bindView(View view, final Context context, Cursor cursor)
	{
		dbHelper= DBHelper.getInstance(context);
		final Event event = dbHelper.getEvent(cursor);

		final String siteNameString = dbHelper.getSiteNameByDeviceSection(event.device, event.section);

		view.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
		setTextViewText(view, context, R.id.textTime, Func.getTimeText(context, event.localtime));
		setTextViewText(view, context, R.id.textDate, Func.getDateText(context, event.localtime));


		final String deviceLocalString;
		if(event.section == 0){
			deviceLocalString = context.getString(R.string.CONTROLLER);
		}else{
			if(event.zone ==0){
				deviceLocalString = dbHelper.getSectionNameByIdWithOptions(event.device, event.section) + "(" + event.section + ")";
			}else{
				deviceLocalString = dbHelper.getSectionNameByIdWithOptions(event.device, event.section) + "(" + event.section + "), " + dbHelper.getZoneNameById(event.device, event.section, event.zone) + "(" + event.zone + ")";
			}
		}
		setTextViewText(view, context, R.id.textDevice, deviceLocalString);

		final String eventExplanation = event.explainEventRegDescription(context);
		setTextViewText(view, context, R.id.textDescription, eventExplanation );
		final ImageView imageView  =(ImageView) view.findViewById(R.id.imageEvent);
		int imageResource = context.getResources().getIdentifier(event.explainIcon(), null, context.getPackageName());
		imageView.setImageResource(imageResource);

		ImageView optionImage = (ImageView) view.findViewById(R.id.imageOption);
		if((event.classId == 11)&&(event.reasonId == 1)){
			optionImage.setImageResource(R.drawable.ic_location);
			optionImage.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					float lat = Func.byteArrayToFloat(Func.reverseByteArray(Func.stringToByteArrayGlueTwo(event.data.substring(0, 8))));
					float lon = Func.byteArrayToFloat(Func.reverseByteArray(Func.stringToByteArrayGlueTwo(event.data.substring(8, 16))));
					//						String uri = String.format(LocaleD3.ENGLISH, "geo:%f,%f", lat, lon);
					String uri = String.format("geo:0,0?q="+ lat +"," + lon + " (Тревога тут)");
					Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					(App.getContext()).startActivity(intent);
				}
			});
		}else{
			optionImage.setImageResource(android.R.color.transparent);
			optionImage.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{

				}
			});
		}

		if(((event.classId == 2)||(event.classId == 1)||(event.classId == 0))&&((event.jdata !=null)&&(!event.jdata.equals(""))&&(event.jdata.contains("url")))){
			optionImage.setImageResource(R.drawable.ic_camera_brand_color);
			optionImage.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if ((event.jdata != null) && (!event.jdata.equals("")))
					{
						Intent intent = new Intent(context, NCameraRecordViewActivity.class);
						intent.putExtra("event", event.id);
						((Activity)context).startActivity(intent);
						((Activity)context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}
				}
			});
		}else{
			optionImage.setImageResource(android.R.color.transparent);
			optionImage.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{

				}
			});
		}

	}



	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint)
	{
		return super.runQueryOnBackgroundThread(constraint);
	}
}
