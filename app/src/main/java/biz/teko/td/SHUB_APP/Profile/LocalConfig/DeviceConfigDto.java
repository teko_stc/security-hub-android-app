package biz.teko.td.SHUB_APP.Profile.LocalConfig;

import biz.teko.td.SHUB_APP.Profile.Models.LocalConfigModel;

public class DeviceConfigDto {
    private final LocalConfigModel configModel;
    private final int hubVersion;

    public DeviceConfigDto(LocalConfigModel configModel, int hubVersion) {
        this.configModel = configModel;
        this.hubVersion = hubVersion;
    }


    public LocalConfigModel getConfigModel() {
        return configModel;
    }

    public int getHubVersion() {
        return hubVersion;
    }
}
