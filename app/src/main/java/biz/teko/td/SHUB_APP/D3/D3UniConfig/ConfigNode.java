package biz.teko.td.SHUB_APP.D3.D3UniConfig;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

/**
 * Created by td13017 on 29.06.2016.
 */
public class ConfigNode extends DescribedNode{

	@Attribute(name="hardware")
	public int Hardware;

	@Attribute(name="software")
	public int Software;

	@Attribute(name="name")
	public String Name;

	@ElementList(entry="file", inline=true)
	public LinkedList<FileNode> Files;

	@ElementList(entry="group", inline=true)
	public LinkedList<InterfaceNodeGroup> Groups;
}
