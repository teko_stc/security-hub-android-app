package biz.teko.td.SHUB_APP.UDP.Entities.Reply;

import android.os.Bundle;

public interface CustomizableConfig {
    void setNewAnotherConfig(Bundle bundle, String configType);

    byte[] getData();
}
