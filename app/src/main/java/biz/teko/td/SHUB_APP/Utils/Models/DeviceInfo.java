package biz.teko.td.SHUB_APP.Utils.Models;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.Zone;

public class DeviceInfo {
    private final Zone zone;
    private final LinkedList<Event> affects;
    private final Device device;
    private final Site site;
    private final Section section;

    public DeviceInfo(Zone zone, LinkedList<Event> affects, Device device, Site site, Section section) {
        this.zone = zone;
        this.affects = affects;
        this.device = device;
        this.site = site;
        this.section = section;
    }


    public Zone getZone() {
        return zone;
    }

    public Device getDevice() {
        return device;
    }

    public LinkedList<Event> getAffects() {
        return affects;
    }

    public Section getSection() {
        return section;
    }

    public Site getSite() {
        return site;
    }

    public static class DeviceInfoBuilder {
        private Zone zone;
        private LinkedList<Event> affects;
        private Device device;
        private Site site;
        private Section section;

        public DeviceInfoBuilder setZone(Zone zone) {
            this.zone = zone;
            return this;
        }

        public DeviceInfoBuilder setAffects(LinkedList<Event> affects) {
            this.affects = affects;
            return this;
        }

        public DeviceInfoBuilder setDevice(Device device) {
            this.device = device;
            return this;
        }

        public DeviceInfoBuilder setSite(Site site) {
            this.site = site;
            return this;
        }

        public DeviceInfoBuilder setSection(Section section) {
            this.section = section;
            return this;
        }

        public DeviceInfo createDeviceInfo() {
            return new DeviceInfo(zone, affects, device, site, section);
        }
    }
}