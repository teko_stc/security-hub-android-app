package biz.teko.td.SHUB_APP.Cameras.Activities.RTSP;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.view.WindowManager;

import androidx.annotation.Nullable;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopInfoLayout;

public class NCameraRTSPActivity extends BaseActivity {
    private NTopInfoLayout topInfoFrame;
    private int camera_id;
    private Context context;
    private DBHelper dbHelper;
    private Camera camera;
    private boolean favorited;
    private MainBar mainBar;
    private boolean showError = true;
    private boolean full;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_camera_rtsp);
        this.context = this;
        this.dbHelper = DBHelper.getInstance(context);

        setup();
    }

    private void setup() {
        topInfoFrame = (NTopInfoLayout) findViewById(R.id.nTopInfoFrame);
    }

    private void bind() {
        String camera_id_s = getIntent().getStringExtra("cameraId");
        if (null != camera_id_s) {
            this.camera_id = Integer.parseInt(camera_id_s);
            if (-1 != camera_id) {
                camera = dbHelper.getRTSPCameraById(camera_id);
            }
        }
        setTopFrame();
    }


    private void setTopFrame() {
        if (null != topInfoFrame) {
            if (null != camera) {
                topInfoFrame.setTitle(camera.name);
                topInfoFrame.setRTSPLink(camera.link);
                topInfoFrame.setOnVideoLoadListener(new NTopInfoLayout.OnVideoLoadListener() {
                    @Override
                    public void fullScreen() {
                        if (!full) {
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                            topInfoFrame.showNavigation(View.GONE);
                            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        } else {
                            topInfoFrame.showNavigation(View.VISIBLE);
                            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                        }
                        full = !full;
                        topInfoFrame.resizeVideoViewWithChangeRotation(full);
                    }

                    @Override
                    public void onError() {
                        if (showError)
                            Func.nShowMessageSmall(context, getString(R.string.N_RTSP_LINK_VIDEO_ERROR));
                    }
                });

            }
            topInfoFrame.setOnTopNavigationListener(new NTopInfoLayout.OnTopNavigationListener() {
                @Override
                public void favoriteChanged() {

                }

                @Override
                public void backPressed() {
                    onBackPressed();
                }

                @Override
                public void setOptions() {
                    Intent intent = new Intent(context, NCameraRTSPSettings.class);
                    intent.putExtra("camera", camera.local_id);
                    startActivityForResult(intent, Const.REQUEST_DELETE);
                    overridePendingTransition(R.animator.enter, R.animator.exit);
                }
            });

        }
    }

    private ActivityOptions getTransitionOptions(View view) {
        return ActivityOptions.makeSceneTransitionAnimation((NCameraRTSPActivity) context,
                new Pair<>(((NTopInfoLayout) view).getTitleView(), Const.TITLE_TRANSITION_NAME));
    }

    @Override
    protected void onResume() {
        super.onResume();
        bind();
        setRotationAnimation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (null != topInfoFrame) {
            topInfoFrame.stopVideo();
            topInfoFrame.setRTSPLink(null);
		}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.REQUEST_DELETE) {
            if (resultCode == RESULT_OK) {
                showError = false;
                finish();
            }
        }
    }

}
