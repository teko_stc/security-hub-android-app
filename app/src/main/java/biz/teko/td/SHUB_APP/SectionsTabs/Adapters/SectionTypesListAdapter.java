package biz.teko.td.SHUB_APP.SectionsTabs.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.R;

public class SectionTypesListAdapter extends ArrayAdapter
{
	private final Context context;
	private final SType[] sTypesArray;
	private final int currentType;
	private final LayoutInflater layoutInflater;

	public SectionTypesListAdapter(@NonNull Context context, int resource, SType[] sTypesArray, int currentType)
	{
		super(context, resource);
		this.context = context;
		this.sTypesArray = sTypesArray;
		this.currentType = currentType;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		SType sType = sTypesArray[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(sType.caption);
		if(currentType == sType.id){
			view.setBackgroundColor(context.getResources().getColor(R.color.brandColorLightPattern));
			textView.setTextColor(context.getResources().getColor(R.color.brandColorAccentColor));
		}else{
			view.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
		}
		return view;
	}

	public int getCount() {
		return sTypesArray.length;
	}

	public SType getItem(int position){
		return sTypesArray[position];
	}

	public long getItemId(int position){
		return position;
	}
}
