package biz.teko.td.SHUB_APP.UDP.Entities.Reply;

import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_1;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_2;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_3;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_4;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_5;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_6;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_7;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_8;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_1;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_2;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_3;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_4;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_5;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_6;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_7;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_8;

import android.os.Bundle;

import java.util.HashMap;

public class
ReplyConfigNg extends ReplyConfig {

    public ReplyConfigNg(byte[] data) {
        super(data);
    }

    @Override
    public HashMap<String, String> getMap() {
        HashMap<String, String> map = super.getMap();
        return getTempMap(map);
    }

    @Override
    public void setNewAnotherConfig(Bundle bundle, String configType) {
        super.setNewAnotherConfig(bundle, configType);
        setTempConfig(bundle, configType);
    }

    private void setTempConfig(Bundle bundle, String configType) {
        switch (configType) {
            case LOW_TEMP_1:
                setNewTemp(0x307, LOW_TEMP_1, bundle); break;
            case HIGH_TEMP_1:
                setNewTemp(0x308, HIGH_TEMP_1, bundle); break;
            case LOW_TEMP_2:
                setNewTemp(0x309, LOW_TEMP_2, bundle); break;
            case HIGH_TEMP_2:
                setNewTemp(0x30A, HIGH_TEMP_2, bundle); break;
            case LOW_TEMP_3:
                setNewTemp(0x30B, LOW_TEMP_3, bundle); break;
            case HIGH_TEMP_3:
                setNewTemp(0x30C, HIGH_TEMP_3, bundle); break;
            case LOW_TEMP_4:
                setNewTemp(0x30D, LOW_TEMP_4, bundle); break;
            case HIGH_TEMP_4:
                setNewTemp(0x30E, HIGH_TEMP_4, bundle); break;
            case LOW_TEMP_5:
                setNewTemp(0x30F, LOW_TEMP_5, bundle); break;
            case HIGH_TEMP_5:
                setNewTemp(0x310, HIGH_TEMP_5, bundle); break;
            case LOW_TEMP_6:
                setNewTemp(0x311, LOW_TEMP_6, bundle); break;
            case HIGH_TEMP_6:
                setNewTemp(0x312, HIGH_TEMP_6, bundle); break;
            case LOW_TEMP_7:
                setNewTemp(0x313, LOW_TEMP_7, bundle); break;
            case HIGH_TEMP_7:
                setNewTemp(0x314, HIGH_TEMP_7, bundle); break;
            case LOW_TEMP_8:
                setNewTemp(0x315, LOW_TEMP_8, bundle); break;
            case HIGH_TEMP_8:
                setNewTemp(0x316, HIGH_TEMP_8, bundle); break;
        }
    }

    private void setNewTemp(int offset, String type, Bundle bundle) {
        this.data[offset] = Byte.parseByte(bundle.getString(type));
    }

    private HashMap<String, String> getTempMap(HashMap<String, String> map) {
        map.put(LOW_TEMP_1, String.valueOf(getTemp(0x307)[0]));
        map.put(HIGH_TEMP_1, String.valueOf(getTemp(0x308)[0]));
        map.put(LOW_TEMP_2, String.valueOf(getTemp(0x309)[0]));
        map.put(HIGH_TEMP_2, String.valueOf(getTemp(0x30A)[0]));
        map.put(LOW_TEMP_3, String.valueOf(getTemp(0x30B)[0]));
        map.put(HIGH_TEMP_3, String.valueOf(getTemp(0x30C)[0]));
        map.put(LOW_TEMP_4, String.valueOf(getTemp(0x30D)[0]));
        map.put(HIGH_TEMP_4, String.valueOf(getTemp(0x30E)[0]));
        map.put(LOW_TEMP_5, String.valueOf(getTemp(0x30F)[0]));
        map.put(HIGH_TEMP_5, String.valueOf(getTemp(0x310)[0]));
        map.put(LOW_TEMP_6, String.valueOf(getTemp(0x311)[0]));
        map.put(HIGH_TEMP_6, String.valueOf(getTemp(0x312)[0]));
        map.put(LOW_TEMP_7, String.valueOf(getTemp(0x313)[0]));
        map.put(HIGH_TEMP_7, String.valueOf(getTemp(0x314)[0]));
        map.put(LOW_TEMP_8, String.valueOf(getTemp(0x315)[0]));
        map.put(HIGH_TEMP_8, String.valueOf(getTemp(0x316)[0]));
        return map;
    }

    private byte[] getTemp(int offset) {
        return getBytes(1, offset);
    }

    @Override
    public byte[] getData() {
        return data;
    }
}
