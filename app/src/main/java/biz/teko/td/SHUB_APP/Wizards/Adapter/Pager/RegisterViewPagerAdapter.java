package biz.teko.td.SHUB_APP.Wizards.Adapter.Pager;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Fragments.RegisterFragment;

/**
 * Created by td13017 on 09.06.2017.
 */

public class RegisterViewPagerAdapter extends FragmentPagerAdapter
{
	private int REGISTER_PAGES_COUNT = 5;


	public RegisterViewPagerAdapter(FragmentManager fm)
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int position)
	{
		return RegisterFragment.newInstance(position);
	}

	@Override
	public int getCount()
	{
		return REGISTER_PAGES_COUNT;
	}
}
