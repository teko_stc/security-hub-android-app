package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class NKeyPadView extends LinearLayout
{
	public static final int KEY_BS = -1;
	public static final int KEY_FP = -2;

	private OnKeyClickListener onKeyClickListener;

	public void setOnKeyClickListener(OnKeyClickListener onKeyClickListener){
		this.onKeyClickListener = onKeyClickListener;
	}

	public interface OnKeyClickListener{
		void onKeyClick(int value);
	}

	private final Context context;
	private int layout;
	private NActionButton button0;
	private NActionButton button1;
	private NActionButton button2;
	private NActionButton button3;
	private NActionButton button4;
	private NActionButton button5;
	private NActionButton button6;
	private NActionButton button7;
	private NActionButton button8;
	private NActionButton button9;
	private NActionButton buttonBS;
	private NActionButton buttonFP;

	public NKeyPadView(Context context, int layout)
	{
		super(context);
		this.context = context;
		this.layout = layout;
	}

	public NKeyPadView(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setup();
		bind();
	}

	private void setup()
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(layout, this, true);

		button0 = (NActionButton) findViewById(R.id.nButton0);
		button1 = (NActionButton) findViewById(R.id.nButton1);
		button2 = (NActionButton) findViewById(R.id.nButton2);
		button3 = (NActionButton) findViewById(R.id.nButton3);
		button4 = (NActionButton) findViewById(R.id.nButton4);
		button5 = (NActionButton) findViewById(R.id.nButton5);
		button6 = (NActionButton) findViewById(R.id.nButton6);
		button7 = (NActionButton) findViewById(R.id.nButton7);
		button8 = (NActionButton) findViewById(R.id.nButton8);
		button9 = (NActionButton) findViewById(R.id.nButton9);
		buttonBS = (NActionButton) findViewById(R.id.nButtonBS);
		buttonFP = (NActionButton) findViewById(R.id.nButtonFP);
	}

	private void bind()
	{
		if(null!=button0) button0.setOnButtonClickListener(getClickListener());
		if(null!=button1) button1.setOnButtonClickListener(getClickListener());
		if(null!=button2) button2.setOnButtonClickListener(getClickListener());
		if(null!=button3) button3.setOnButtonClickListener(getClickListener());
		if(null!=button4) button4.setOnButtonClickListener(getClickListener());
		if(null!=button5) button5.setOnButtonClickListener(getClickListener());
		if(null!=button6) button6.setOnButtonClickListener(getClickListener());
		if(null!=button7) button7.setOnButtonClickListener(getClickListener());
		if(null!=button8) button8.setOnButtonClickListener(getClickListener());
		if(null!=button9) button9.setOnButtonClickListener(getClickListener());
		if(null!=buttonBS) buttonBS.setOnButtonClickListener(getClickListener());
		if(null!=buttonFP) buttonFP.setOnButtonClickListener(getClickListener());
	}

	private NActionButton.OnButtonClickListener getClickListener()
	{
		return new NActionButton.OnButtonClickListener()
		{
			@Override
			public void onButtonClick(int action)
			{
				if(null!=onKeyClickListener) onKeyClickListener.onKeyClick(action);
			}
		};
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NKeyPadView, 0, 0);
		try
		{
			layout = a.getResourceId(R.styleable.NKeyPadView_NKeyPadLayout, 0);
		}finally
		{
			a.recycle();
		}
	}

	public void setFPVisibility(int visibility)
	{
		this.buttonFP.setVisibility(visibility);
	}
}
