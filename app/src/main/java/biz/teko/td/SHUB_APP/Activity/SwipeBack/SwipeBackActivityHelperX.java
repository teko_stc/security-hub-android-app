package biz.teko.td.SHUB_APP.Activity.SwipeBack;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Utils;


/**
 * @author Yrom
 */
public class SwipeBackActivityHelperX {
	private Activity mActivity;

	private SwipeBackLayoutX mSwipeBackLayoutX;

	public SwipeBackActivityHelperX(Activity activity) {
		mActivity = activity;
	}

	@SuppressWarnings("deprecation")
	public void onActivityCreate() {
		mActivity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		mActivity.getWindow().getDecorView().setBackgroundDrawable(null);
		mSwipeBackLayoutX = (SwipeBackLayoutX) LayoutInflater.from(mActivity).inflate(
				R.layout.swipeback_layout, null);
		mSwipeBackLayoutX.addSwipeListener(new SwipeBackLayoutX.SwipeListener() {
			@Override
			public void onScrollStateChange(int state, float scrollPercent) {
			}

			@Override
			public void onEdgeTouch(int edgeFlag) {
				Utils.convertActivityToTranslucent(mActivity);
			}

			@Override
			public void onScrollOverThreshold() {

			}
		});
	}

	public void onPostCreate() {
		mSwipeBackLayoutX.attachToActivity(mActivity);
	}

	public View findViewById(int id) {
		if (mSwipeBackLayoutX != null) {
			return mSwipeBackLayoutX.findViewById(id);
		}
		return null;
	}

	public SwipeBackLayoutX getSwipeBackLayoutX() {
		return mSwipeBackLayoutX;
	}
}

