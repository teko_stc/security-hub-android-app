package biz.teko.td.SHUB_APP.Devices_N.Activities;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Codes.Activities.NLocalUsersActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Activities.EventsDevActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomInfoLayout;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class DeviceSettingsActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private NMenuListElement nLocalUsers;
	private NMenuListElement nUSSDView;
	private NMenuListElement nRebootView;
	private NMenuListElement nUpdateView;
	private NMenuListElement nDeleteView;
	private NMenuListElement nMenuHistory;

	private LinearLayout buttonBack;
	private int site_id;
	private int device_id;
	private Device device;
	private boolean bound;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private int sending = 0;
	private UserInfo userInfo;
	private NBottomInfoLayout bottomFrame;

	private BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			bindView();
		}
	};

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_device_settings);
		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();


		site_id  = getIntent().getIntExtra("site", -1);
		device_id  = getIntent().getIntExtra("device", -1);

		setupView();
		bindView();
	}

	private void bindView()
	{
		if(-1!=device_id){
			device = dbHelper.getDeviceById(device_id);
		}

		if(null!=buttonBack)buttonBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		if(null!=device){
			if(null!=nMenuHistory){
				if(userInfo.isDEngineer()){
					nMenuHistory.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(getBaseContext(), EventsDevActivity.class);
							intent.putExtra("site", site_id);
							intent.putExtra("device", device.id);
							startActivity(intent, getTransitionOptions(nMenuHistory).toBundle());
						}
					});
				}else{
					nMenuHistory.setVisibility(View.GONE);
				}
			}
			if(null!=nLocalUsers) nLocalUsers.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					Intent intent = new Intent(getBaseContext(), NLocalUsersActivity.class);
					intent.putExtra("device", device.id);
					startActivity(intent);
					overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
			if(null!=nUSSDView){
				if(maySystemSet() && device.type != Const.DEVICETYPE_SH_4G){
					nUSSDView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(getBaseContext(), DeviceUssdActivity.class);
							intent.putExtra("device", device.id);
							startActivity(intent, getTransitionOptions(nUSSDView).toBundle());
						}
					});
				}else{
					nUSSDView.setVisibility(View.GONE);
				}
			}
			if(null!=nRebootView){
				if(mayDeviceSet()){
					nRebootView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							NDialog nDialog = new NDialog(context, R.layout.n_dialog_question);
							nDialog.setTitle(getString(R.string.N_DEVICE_SETTINGS_RENAME_Q));
							nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
							{
								@Override
								public void onActionClick(int value, boolean b)
								{
									switch (value){
										case NActionButton.VALUE_OK:
											if(rebootDevice())
												nDialog.dismiss();
											break;
										case NActionButton.VALUE_CANCEL:
											nDialog.dismiss();
											break;
									}
								}
							});
							nDialog.show();
						}
					});
				}else{
					nRebootView.setVisibility(View.GONE);
				}
			}
			if(null!=nUpdateView){
				boolean u_loaded = dbHelper.getDeviceUpdate(device.id);
				if(u_loaded){
					nUpdateView.setTitle(getString(R.string.N_DEVICE_SETTINGS_UPDATE));
				}else{
					nUpdateView.setTitle(getString(R.string.N_DEVICE_SETTINGS_UPDATE_LOAD));
				}
				if(mayDeviceSet()){
					nUpdateView.setOnRootClickListener(() -> {
						NDialog nDialog = new NDialog(context, R.layout.n_dialog_question);
						nDialog.setTitle( u_loaded ? getString(R.string.N_DEVICE_SETTINGS_UPDATE_Q) :  getString(R.string.N_DEVICE_SETTINGS_UPDATE_LOAD_Q));
						nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
						{
							@Override
							public void onActionClick(int value, boolean b)
							{
								switch (value){
									case NActionButton.VALUE_OK:
										if(u_loaded){
											updateDevice();
										}else{
											loadUpdate();
										}
										nDialog.dismiss();
										break;
									case NActionButton.VALUE_CANCEL:
										nDialog.dismiss();
										break;
								}
							}
						});
						nDialog.show();
					});
				}else{
					nUpdateView.setVisibility(View.GONE);
				}
			}
			if(null!=nDeleteView){
				if(maySystemSet()){
					nDeleteView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NDeleteActivity.class);
							intent.putExtra("type", Const.DEVICE);
							intent.putExtra("device", device.id);
							intent.putExtra("transition",true);
							startActivityForResult(intent, Const.REQUEST_DELETE, getTransitionOptions(nDeleteView).toBundle());
						}
					});
				}else{
					nDeleteView.setVisibility(View.GONE);
				}
			}

			setBottomFrame();
		}

	}

	private boolean mayDeviceSet()
	{
		return Func.nGetRemoteDeviceSetRights(userInfo, device);
	}

	private boolean maySystemSet()
	{
		return Func.nGetRemoteSystemSetRights(userInfo);
	}

	private void setBottomFrame()
	{
		if(null!=bottomFrame){
			DeviceType dType  = device.getType();
			if(null!=dType){
				bottomFrame.setModel(dType.caption);
				bottomFrame.setModelImage(dType.getListIcon(context));
				bottomFrame.setSn(Integer.toString(device.account));
			}
			bottomFrame.setDetector(getString(R.string.N_DEVICE_SETTINGS_TITLE_VERSION));
			bottomFrame.setAlarm(device.getVersion());
		}
	}

	private void deleteDevice()
	{
		JSONObject message = new JSONObject();
		try
		{
			message.put("device_id", device.id);
			message.put("domain_id", userInfo.domain);
			nSendMessageToServer(null, D3Request.DEVICE_REVOKE_DOMAIN, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void updateDevice(){
		nSendMessageToServer(device, Command.UPDATE_APPLY, new JSONObject(), true);

	}

	private void loadUpdate()
	{
		JSONObject commandAddr = new JSONObject();
		try
		{
			commandAddr.put("file", 0);
			nSendMessageToServer(device, Command.UPDATE, commandAddr, true);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private boolean rebootDevice()
	{
		return nSendMessageToServer(device, Command.REBOOT, new JSONObject(), true);
	}

	private void setupView()
	{
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		nLocalUsers = (NMenuListElement) findViewById(R.id.nMenuLocalUsers);
		nUSSDView = (NMenuListElement) findViewById(R.id.nMenuUSSD);
		nRebootView = (NMenuListElement) findViewById(R.id.nMenuReboot);
		nUpdateView = (NMenuListElement) findViewById(R.id.nMenuUpdate);
		nDeleteView = (NMenuListElement) findViewById(R.id.nMenuDelete);
		nMenuHistory = (NMenuListElement) findViewById(R.id.nMenuHistory);

		bottomFrame = (NBottomInfoLayout) findViewById(R.id.nBottomFrame);
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((DeviceSettingsActivity)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	public void  onResume(){
		super.onResume();
		bindView();
		bindD3();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_ADD);
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_VERSION_UPDATE);
		registerReceiver(updateReceiver, intentFilter);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
	}

	public void onPause(){
		super.onPause();
		if(bound)unbindService(serviceConnection);
		unregisterReceiver(updateReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}

}
