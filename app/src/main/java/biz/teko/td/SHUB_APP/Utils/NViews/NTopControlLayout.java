package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;


public class NTopControlLayout extends LinearLayout
{
	private OnActionClickListener onActionClickListener;
	private LinearLayout armFrame;
	private LinearLayout onOffFrame;
	private LinearLayout controlFrame;
	private LinearLayout scriptFrame;
	private LinearLayout resetFrame;
	private NTopLayout.ControlType type;
	private NActionButton buttonSwitchOn;
	private NActionButton buttonSwitchOff;
	private NActionButton buttonReset;
	private Script script;


	public static final int VALUE_NO_SCRIPTED = -123;

	public void setOnActionClickListener(OnActionClickListener onActionClickListener){
		this.onActionClickListener = onActionClickListener;
	}

	public interface OnActionClickListener{
		void onActionClick(int value);
		void onLinkClick(int id);
	}

//	public static  final int[] STATE_INVERT = {R.attr.state_invert};
//	public static  final int[] STATE_OFFLINE = {R.attr.state_offline};

	private final Context context;
	private int layout;
	private NActionButton buttonOn;
	private NActionButton buttonOff;
	private boolean alarm;
	private boolean sabotage;
	private boolean malf;
	private boolean attention;
	private boolean inverted;
	private boolean offline;
	private LinearLayout buttonFrame;


	public NTopControlLayout(Context context)
	{
		super(context);
		this.context = context;
	}

	public NTopControlLayout(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setView();
		bindView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NTopControlLayout, 0, 0);
		try{
			layout = a.getResourceId(R.styleable.NTopControlLayout_NControlFrameLayout, 0);
		}finally
		{
			a.recycle();
		}
	}

	private void setView()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		layoutInflater.inflate(layout, this, true);

		armFrame = (LinearLayout) findViewById(R.id.nFrameArm);
		onOffFrame = (LinearLayout) findViewById(R.id.nFrameOnOff);
		controlFrame = (LinearLayout) findViewById(R.id.nFrameControl);
		scriptFrame = (LinearLayout) findViewById(R.id.nFrameScript);
		resetFrame = (LinearLayout) findViewById(R.id.nFrameReset);

		buttonFrame = (LinearLayout) findViewById(R.id.nButtonFrame);

		buttonOn = (NActionButton) findViewById(R.id.nFrameButtonOn);
		buttonOff = (NActionButton) findViewById(R.id.nFrameButtonOff);

		buttonSwitchOn = (NActionButton) findViewById(R.id.nDialogButtonOn);
		buttonSwitchOff = (NActionButton) findViewById(R.id.nDialogButtonOff);

		buttonReset = (NActionButton) findViewById(R.id.nDialogButtonReset);
	}

	private void bindView()
	{
		setTypeSet();
		if(null!=buttonOn) buttonOn.setOnButtonClickListener(getOnClickListener());
		if(null!=buttonOff) buttonOff.setOnButtonClickListener(getOnClickListener());
		if(null!=buttonSwitchOff) {
			buttonSwitchOff.setOnButtonClickListener(getOnClickListener());
		}
		if(null!=buttonSwitchOn) {
			buttonSwitchOn.setOnButtonClickListener(getOnClickListener());
		}
		if(null!=buttonReset) {
			buttonReset.setOnButtonClickListener(getOnClickListener());
		}
	}

	private NActionButton.OnButtonClickListener getOnClickListener()
	{
		return new NActionButton.OnButtonClickListener()
		{
			@Override
			public void onButtonClick(int action)
			{
				if(null!=onActionClickListener)onActionClickListener.onActionClick(action);
			}
		};
	}

	public void setType(NTopLayout.ControlType type)
	{
		this.type = type;
		setTypeSet();
	}

	private void setTypeSet()
	{
		boolean arm = false;
		boolean control = false;
		boolean script = false;
		boolean reset = false;
		boolean tech = false;

		if(null!=type){
			switch (type){
				case arm:
					arm = true;
					break;
				case control:
					control = true;
					break;
				case script:
					script = true;
					break;
				case reset:
				case reset_fire:
					reset = true;
					break;
				case tech:
					tech = true;
					break;
				case none:
				case monitor:
					break;
			}
		}
		armFrame.setVisibility(arm ? VISIBLE: GONE);
		onOffFrame.setVisibility(control ? VISIBLE : GONE);
		scriptFrame.setVisibility(script ? VISIBLE : GONE);
		resetFrame.setVisibility(reset ? VISIBLE : GONE);
		controlFrame.setVisibility(tech ? VISIBLE : GONE);
	}

	public void setInverted(boolean inverted)
	{
		this.inverted = inverted;
		refreshDrawableState();
	}


	public void setScript(Script script){
		this.script = script;
		flushScriptFrame();
		setScriptButtons();
		setScriptLink();
	}

	private void flushScriptFrame()
	{
		if(null!=scriptFrame && scriptFrame.getVisibility() == VISIBLE) {
			LinearLayout scriptFrameButtons = scriptFrame.findViewById(R.id.nFrameScriptButtons);
			if(null!=scriptFrameButtons) scriptFrameButtons.removeAllViews();
			LinearLayout scriptFrameLink = scriptFrame.findViewById(R.id.nFrameScriptLink);
			if(null!=scriptFrameLink) scriptFrameLink.removeAllViews();
		}
	}

	private void setScriptButtons(){
		if(null!=script){
			JSONArray buttonsArray = script.getButtons();
			LinkedList<NActionButton> buttons = makeScriptButtons(script, buttonsArray);
			if(null!=buttons && 0 !=buttons.size()){
				for(NActionButton button:buttons){
					addScriptButton(button);
				}
			}
		}
	}

	private void addScriptButton(View view){
		NActionButton button = (NActionButton) view;
		if(null!= scriptFrame){
			LinearLayout scriptFrameButtons = scriptFrame.findViewById(R.id.nFrameScriptButtons);
			if(null!=scriptFrameButtons)
			{
				scriptFrameButtons.addView(button);
				if (null != button)
				{
					button.setOnButtonClickListener(getOnClickListener());
				}
			}
		}
	}


	private LinkedList<NActionButton> makeScriptButtons(Script script, JSONArray buttonsArray)
	{
		if (null != buttonsArray && buttonsArray.length() > 0)
		{
			try
			{
				LinkedList<NActionButton> buttons = new LinkedList<>();
				for (int i1 = 0; i1 < buttonsArray.length(); i1++)
				{
					JSONObject item = buttonsArray.getJSONObject(i1);
					String title = Func.getScriptDataForCurrLang(item.getString("content"), PrefUtils.getInstance(context).getCurrentLang());
					final int value = item.getInt("value");

					NActionButton button = new NActionButton(context, R.layout.n_button_script_action);
//					button.setBackground(getResources().getDrawable(R.drawable.n_top_info_frame_script_background_selector));
					button.setTitle(title);
					button.setAction(value);
					button.setImage(R.drawable.n_image_button_on);
					button.setTextColor(R.color.n_action_button_text_selector);

					if(script.uid.equals("smart-impuls-gate1-fhjdf9-g4t-im")){
						if(value ==1 ){
							button.setImage(R.drawable.ic_gate_open);
						}else{
							button.setImage(R.drawable.ic_gate_close);
						}
					}
					buttons.add(button);
				}
				return buttons;
			}catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return null;
	}

	private void setScriptLink()
	{
		if(null!=scriptFrame){
			LinearLayout scriptFrameLink = scriptFrame.findViewById(R.id.nFrameScriptLink);
			if(null!=scriptFrameLink){
				NWithAButtonElement nWithAButtonElement = new NWithAButtonElement(context, null!=script ? R.layout.n_script_link_view: R.layout.n_script_set_link_view);
				nWithAButtonElement.setTitle(null!=script ? "" : getContext().getString(R.string.N_TOP_INFO_SCRIPT_2));
				nWithAButtonElement.setActionValue(null!=script? script.id:VALUE_NO_SCRIPTED);
				nWithAButtonElement.setActionTitle(null!=script? getContext().getString(R.string.N_TOP_INFO_SCRIPT_1) + script.name + "»": getContext().getString(R.string.N_TOP_INFO_SCRIPT_SET));
				nWithAButtonElement.setActionTextColor(R.color.brandColorWhite);
				nWithAButtonElement.setActionTextSize(16);
				nWithAButtonElement.setActionIncrease(false);
				if(null!=script){
					nWithAButtonElement.setBackground(android.R.color.transparent);
					nWithAButtonElement.setActionTextColor(R.color.n_text_dark_grey_8a);
				}
				nWithAButtonElement.setOnChildClickListener((action, option) -> {
					if(null!=onActionClickListener)onActionClickListener.onLinkClick(action);
				});
				scriptFrameLink.addView(nWithAButtonElement);
			}
		}
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		return super.onCreateDrawableState(extraSpace + 2);
	}
}
