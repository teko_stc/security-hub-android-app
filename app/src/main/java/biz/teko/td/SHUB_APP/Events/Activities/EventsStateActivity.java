package biz.teko.td.SHUB_APP.Events.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopStatesLayout;

public  class EventsStateActivity extends BaseActivity
{
	private View close;
	private NTopStatesLayout topStateFrame;
	private int device_id;
	private int section_id;
	private int zone_id;
	private Context context;
	private DBHelper dbHelper;
	private ListView eventsList;
	private TextView message;
	private NEventsListAdapter eventsAdapter;
	private int type;

	private BroadcastReceiver eventReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateEventsList();
		}
	};
	private Device device;
	private DeviceType deviceType;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		type = getIntent().getIntExtra("type", 0);
		switch (type){
			case Const.STATE_TYPE_SUPPLY:
				setContentView(R.layout.n_activity_events_supply);
				break;
			case Const.STATE_TYPE_LEVEL:
				setContentView(R.layout.n_activity_events_level);
				break;
			case Const.STATE_TYPE_TEMP:
				setContentView(R.layout.n_activity_events_temp);
				break;
		}

		context = this;
		dbHelper = DBHelper.getInstance(context);

		device_id = getIntent().getIntExtra("device", 0);
		device = dbHelper.getDeviceById(device_id);
		deviceType = device.getType();

		section_id = getIntent().getIntExtra("section", -1);
		zone_id = getIntent().getIntExtra("zone", -1);

		setup();

	}

	@Override
	protected void onStart()
	{
		super.onStart();
		bind();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(eventReceiver);
	}

	private void bind()
	{
		if(null!=close) close.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});
		if(0!= device_id && -1!= section_id && -1!= zone_id){
			if(null!=topStateFrame){
				/*set top frame*/
				topStateFrame.setEType(getEType());
				topStateFrame.setStates(getAffects(), zone_id);
				topStateFrame.bind();
			}
			if(null!=eventsList) updateEventsList();
		}else{
			if(null!=topStateFrame)topStateFrame.setVisibility(View.GONE);
			if(null!=eventsList) eventsList.setVisibility(View.GONE);
			if(null!=message) message.setText(R.string.N_DATA_ERROR);
		}
	}

	private LinkedList<Event> getAffects()
	{
		switch (type){
			case Const.STATE_TYPE_SUPPLY:
				return dbHelper.getSupplyAffects(device_id, section_id, zone_id);
			case Const.STATE_TYPE_LEVEL:
				if(null!=deviceType)
				{
					switch (deviceType.id)
					{
						case Const.DEVICETYPE_SH:
						case Const.DEVICETYPE_SH_1:
						case Const.DEVICETYPE_SH_1_1:
						case Const.DEVICETYPE_SH_2:
						case Const.DEVICETYPE_SH_4G:
							return dbHelper.getLevelAffects(device_id, section_id, zone_id);
						default:
							return dbHelper.getExtLevelAffects(device_id, section_id, zone_id);
					}
				}
				return dbHelper.getLevelAffects(device_id, section_id, zone_id);
			case Const.STATE_TYPE_TEMP:
				return dbHelper.getTempAffects(device_id, section_id, zone_id);
		}
		return null;
	}

	private void updateEventsList()
	{
		Cursor cursor = null;
		switch (type){
			case Const.STATE_TYPE_SUPPLY:
				cursor = dbHelper.getSupplyEvents(device_id, section_id, zone_id);
				break;
			case Const.STATE_TYPE_LEVEL:
				if(null!=deviceType)
				{
					switch (deviceType.id)
					{
						case Const.DEVICETYPE_SH:
						case Const.DEVICETYPE_SH_1:
						case Const.DEVICETYPE_SH_1_1:
						case Const.DEVICETYPE_SH_2:
						case Const.DEVICETYPE_SH_4G:
							cursor = dbHelper.getLevelEvents(device_id, section_id, zone_id);
							break;
						default:
							cursor = dbHelper.getExtLevelEvents(device_id, section_id, zone_id);
							break;
					}
				}else{
					cursor = dbHelper.getLevelEvents(device_id, section_id, zone_id);
				}
				break;
			case Const.STATE_TYPE_TEMP:
				cursor = dbHelper.getTempEvents(device_id, section_id, zone_id);
				break;
		}
		if(null!=cursor){
			if(null==eventsAdapter){
				eventsAdapter  = new NEventsListAdapter(context,R.layout.n_events_list_element, cursor, 0, false);
				eventsList.setAdapter(eventsAdapter);
			}else{
				eventsAdapter.update(cursor);
			}
			if(0<cursor.getCount()){
				eventsList.setVisibility(View.VISIBLE);
				return;
			}
		}
		eventsList.setVisibility(View.GONE);
		if(null!=message) message.setText(getResources().getString(R.string.N_EVENT_HISTORY_NO_DATA));
	}

	private NTopStatesLayout.EType getEType()
	{
		if(0 == section_id){
			if(0 == zone_id || 100 == zone_id){
				Device device = dbHelper.getDeviceById(device_id);
				if(null!=device){
					if(device.type == Const.DEVICETYPE_SH_2){
						return NTopStatesLayout.EType.device_sh_2;
					}
					return NTopStatesLayout.EType.device;
				}
			}
		} else {
			Zone zone = dbHelper.getZoneById(device_id, section_id, zone_id);
			if(Const.ZONETYPE_4511 == zone.getModelId()
				|| Const.ZONETYPE_421_RK2 == zone.getModelId()
				|| Const.ZONETYPE_431_RK2 == zone.getModelId()){
				return NTopStatesLayout.EType.zone_with_bu;
			}
			if(Const.ZONETYPE_3731 == zone.getModelId()){
				return NTopStatesLayout.EType.zone_temp;
			}
		}
		return NTopStatesLayout.EType.zone;
	}

	private void setup()
	{
		close = (LinearLayout) findViewById(R.id.nButtonClose);
		topStateFrame  = (NTopStatesLayout) findViewById(R.id.nTopStateFrame);
		eventsList = (ListView) findViewById(R.id.nEventsList);
		message = (TextView) findViewById(R.id.nTextEmpty);
	}
}
