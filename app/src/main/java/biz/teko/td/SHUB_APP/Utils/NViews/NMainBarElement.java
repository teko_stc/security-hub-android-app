package biz.teko.td.SHUB_APP.Utils.NViews;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;

public class NMainBarElement extends LinearLayout implements Textable
{
	public static  final int[] STATE_ALARM = {R.attr.state_alarm}; // for all errors
	public static  final int[] STATE_ATTENTION = {R.attr.state_attention}; // for all errors

	public static  final int[] STATE_OFFLINE = {R.attr.state_offline};
	public static  final int[] STATE_RK_OFFLINE = {R.attr.state_rk_offline};

	public static  final int[] STATE_DELAY_ARM = {R.attr.state_delay_arm};

	public static  final int[] STATE_NO_ARM = {R.attr.state_no_arm};
	public static  final int[] STATE_ARM = {R.attr.state_arm};
	public static  final int[] STATE_DISARM = {R.attr.state_disarm};
	public static  final int[] STATE_PARTLY_ARM = {R.attr.state_partly_arm};
	public static  final int[] STATE_ON = {R.attr.state_on};
	public static  final int[] STATE_OFF = {R.attr.state_off};
	public static  final int[] STATE_SECTION_CONTROL = {R.attr.state_section_control};
	public static  final int[] STATE_NO_SECTION_CONTROL = {R.attr.state_no_section_control};

	public static  final int[] STATE_AUTO = {R.attr.state_auto};
	public static  final int[] STATE_SCRIPTED = {R.attr.state_scripted};
	private static final long MIN_CLICK_DURATION = 1000;

	private Context context;
	private boolean increase = true;
	private boolean clickable = true;
	private boolean vibro = false;

	private MainBar.Type type;

	private boolean no_arm;
	private boolean armed;
	private boolean disarmed;
	private boolean partly_armed;
	private boolean turned_on;
	private boolean turned_off;
	private boolean control;
	private boolean no_control;
	private boolean alarm;
	private boolean attention;
	private boolean offline;
	private boolean rk_offline;
	private boolean auto;
	private boolean scripted;
	private boolean delay_arm;

	private int layout;private ImageView imageBigView;
	private TextView titleText;
	private TextView subTitleText;
	private TextView siteTitleText;
	private TextView textBig;


	private int image;
	private String title;
	private String subtitle;
	private String sitetitle;
	private String valueBig;
	private OnElementClickListener onElementClickListener;

	private boolean pressed;
	private final Handler handler = new Handler();
	private final Runnable runnable = new Runnable()
	{
		public void run()
		{
			checkPressedState();
		}
	};


	public void setOnElementClickListener(OnElementClickListener onElementClickListener){
		this.onElementClickListener = onElementClickListener;
	}

	public void setType(MainBar.Type type)
	{
		this.type = type;
	}

	public MainBar.Type getType()
	{
		return type;
	}

	public interface OnElementClickListener{
		void onClick();
		void onLongClick();
	}


	public NMainBarElement(Context context, int layout)
	{
		super(context);
		this.context = context;
		this.layout = layout;
		setupView();
	}

	public NMainBarElement(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setupView();
		bindView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NMainBarElement, 0, 0);
		try
		{
			layout = a.getResourceId(R.styleable.NMainBarElement_NMainBarElementLayout, 0);
		}finally
		{
			a.recycle();
		}
	}

	private void setupView()
	{
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(layout, this, true);

		imageBigView = (ImageView) findViewById(R.id.nImageBarBig);
		textBig = (TextView) findViewById(R.id.nTextBarBig);

		titleText = (TextView) findViewById(R.id.nTextTitle);
		subTitleText = (TextView) findViewById(R.id.nTextBarSubTitle);
		siteTitleText = (TextView) findViewById(R.id.nTextBarSiteTitle);

	}

	private void bindView()
	{
		setBigImageView();
		setTitleView();
		setSubTitleView();
	}


	public void setStates(LinkedList<Event> affects)
	{
		int alarmImage = 0;
		int temp = Event.TEMP_NO_VALUE;

		boolean alarm = false;
		boolean attention = false;
		boolean offline  = false;
		boolean rk_offline  = false;
		boolean delay = false;

		if(null!=affects){
			for(Event event:affects){
				switch (event.classId){
					case Const.CLASS_ALARM:
						if(type == MainBar.Type.DEVICE){
							alarmImage = event.getMainIcon(context);
						}else{
							alarm = true;
						}
						break;
					case Const.CLASS_SABOTAGE:
						attention = true;
						break;
					case Const.CLASS_MALFUNCTION:
						switch (event.reasonId){
							case Const.REASON_MALF_LOST_CONNECTION:
								if(event.detectorId == Const.DETECTOR_CONTROLLER){
									offline = true;
								}else{
									rk_offline = true;
								}
								break;
							default:
								attention = true;
								break;
						}
						break;
					case Const.CLASS_ATTENTION:
						attention = true;
						break;
					case Const.CLASS_ARM:
						break;
					case Const.CLASS_CONTROL:
						break;
					case Const.CLASS_SUPPLY:
						switch (event.reasonId){
							case Const.REASON_BATTERY_LEVEL:
								break;
							case Const.REASON_SOCKET_FAULT:
							case Const.REASON_BATTERY_ERROR:
								attention = true;
								break;
							default:
								break;
						}
						break;
					case Const.CLASS_CONNECTIVITY:
						switch (event.reasonId){
							case Const.REASON_CONNECTIVITY_UNAVAILABLE:
								break;
							case Const.REASON_CONNECTIVITY_LEVEL:
								break;
							case Const.REASON_CONNECTIVITY_IMSI:
								break;
							default:
								break;
						}
						break;
					case Const.CLASS_INFORMATION:
						switch (event.reasonId){
							case Const.REASON_INFORMATION_ARM_DELAY:
								delay = true;
								break;
							default:
								break;
						}
						break;
					case Const.CLASS_DEBUG:
						break;
					case Const.CLASS_TELEMETRY:
						switch (event.reasonId){
							case Const.REASON_TEMPERATURE:
								temp = event.getTemperatureValue();
								break;
							default:
								break;
						}
						break;
					case Const.CLASS_GEOLOCATION:
						break;
					case Const.CLASS_INNER:
						break;
					default:
						break;
				}
			}
		}

		if(type == MainBar.Type.DEVICE && temp!=Event.TEMP_NO_VALUE){
			setTemp(Integer.toString(temp));
		}else{
			setTemp(null);
		}

		if(0!=alarmImage){
			setImageBig(alarmImage);
		}

		setAlarm(alarm);
		setAttention(attention);
		setOffline(offline);
		setRKOffline(rk_offline);
		setDelayArm(delay);

	}

	private void setSiteTitleView()
	{
		if(null!= siteTitleText){
			if(null!=sitetitle){
				siteTitleText.setText(sitetitle);
				siteTitleText.setVisibility(VISIBLE);
			}
		}
	}

	private void setSubTitleView()
	{
		if(null!= subTitleText){
			if(null!=subtitle){
				subTitleText.setText(subtitle);
				subTitleText.setVisibility(VISIBLE);
			}
		}
	}

	private void setTitleView()
	{
		if(null!=titleText) {
			if(null!=title){
				titleText.setText(title);
			}
		}
	}

	public TextView getTitleView(){
		return titleText;
	}


	private void setBigImageView()
	{
		if(null!= imageBigView){
			if(0!=image){
				imageBigView.setImageDrawable(getResources().getDrawable(image));
				if(null!=textBig)textBig.setVisibility(GONE);
			}
		}
	}

	private  void setTempText(){
		if(null!= textBig){
			if(null!=valueBig){
				textBig.setText(valueBig + "º");
				textBig.setVisibility(VISIBLE);
			}else{
				textBig.setVisibility(GONE);
			}
		}
	}

	public void resetStates()
	{
		this.alarm = false;
		this.attention = false;
		this.offline = false;
		this.rk_offline = false;
		this.no_arm = false;
		this.armed = false;
		this.disarmed = false;
		this.partly_armed = false;
		this.turned_off = false;
		this.turned_on = false;
		this.no_control = false;
		this.control = false;
		this.auto = false;
		this.scripted = false;
		this.delay_arm = false;
//		refreshDrawableState();
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 15);

		if(alarm) mergeDrawableStates(drawableState, STATE_ALARM);
		if(attention) mergeDrawableStates(drawableState, STATE_ATTENTION);
		if(offline) mergeDrawableStates(drawableState,STATE_OFFLINE);
		if(rk_offline) mergeDrawableStates(drawableState,STATE_RK_OFFLINE);

		if(delay_arm) mergeDrawableStates(drawableState,STATE_DELAY_ARM);

		if(no_arm)mergeDrawableStates(drawableState, STATE_NO_ARM);
		if(armed)mergeDrawableStates(drawableState, STATE_ARM);
		if(disarmed) mergeDrawableStates(drawableState, STATE_DISARM);
		if(partly_armed)mergeDrawableStates(drawableState, STATE_PARTLY_ARM);
		if(turned_on)mergeDrawableStates(drawableState, STATE_ON);
		if(turned_off) mergeDrawableStates(drawableState, STATE_OFF);
		if(control) mergeDrawableStates(drawableState, STATE_SECTION_CONTROL);
		if(no_control) mergeDrawableStates(drawableState, STATE_NO_SECTION_CONTROL);

		if(auto) mergeDrawableStates(drawableState, STATE_AUTO);
		if(scripted) mergeDrawableStates(drawableState, STATE_SCRIPTED);

		return drawableState;
	}

	public void setTitle(String title){
		this.title = title;
		setTitleView();
	}

	public void setSubtitle(String subtitle){
		this.subtitle = subtitle;
		setSubTitleView();
	}

	public void setSitetitle(String sitetitle){
		this.sitetitle = sitetitle;
		setSiteTitleView();
	}

	public void setImageBig(int image){
		if(0!=image)
		{
			this.image = image;
			setBigImageView();
		}
	}

	public void setTemp(String temp){
		this.valueBig = temp;
		setTempText();
	}


	/*STATES*/

	public void setAlarm(boolean no_arm){
		//		flushControlStates();
		this.alarm = no_arm;
//		refreshDrawableState();
	}

	public void setAttention(boolean attention)
	{
		//		flushControlStates();
		this.attention = attention;
//		refreshDrawableState();
	}

	public void setOffline(boolean offline)
	{
		this.offline = offline;
//		refreshDrawableState();
	}

	public void setRKOffline(boolean offline)
	{
		this.rk_offline = offline;
		//		refreshDrawableState();
	}

	public void setDelayArm(boolean delay_arm){
		this.delay_arm = delay_arm;
//		refreshDrawableState();
	}

	public void setNoArm(boolean no_arm){
//		flushControlStates();
		this.no_arm = no_arm;
//		refreshDrawableState();
	}

	public void setArmed(boolean armed){
//		flushControlStates();
		this.armed = armed;
//		refreshDrawableState();
	}

	public void setDisarmed(boolean disarmed){
//		flushControlStates();
		this.disarmed = disarmed;
//		refreshDrawableState();
	}

	public void setOn(boolean on){
//		flushControlStates();
		this.turned_on = on;
//		refreshDrawableState();
	}

	public void setOff(boolean off){
//		flushControlStates();
		this.turned_off = off;
//		refreshDrawableState();
	}
	public void setControl(boolean control){
//		flushControlStates();
		this.control = control;
//		refreshDrawableState();
	}

	public void setNoControl(boolean no_control){
//		flushControlStates();
		this.no_control = no_control;
//		refreshDrawableState();
	}

	public void setPartlyArmed(boolean partly){
//		flushControlStates();
		this.partly_armed = partly;
//		refreshDrawableState();
	}



	//only for relay
	public void setAuto(boolean auto)
	{
		this.auto = auto;
//		refreshDrawableState();
	}

	//only for relay
	public void setScripted(boolean scripted)
	{
		this.scripted = scripted;
//		refreshDrawableState();
	}

	public void acceptStates(){
		refreshDrawableState();
	}

	public void setIncrease(boolean increase){
		this.increase = increase;
	}

	public  void setClickable(boolean clickable){
		this.clickable = clickable;
	}

	public void setVibro(boolean vibro)
	{
		this.vibro = vibro;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (clickable){
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					if(!pressed) {
						handler.postDelayed(runnable, 1000);
						pressed = true;
					}
					animateDown();
					return true;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
					if(pressed){
						pressed = false;
						handler.removeCallbacks(runnable);
						if(event.getAction() == MotionEvent.ACTION_UP
								&& null!=onElementClickListener) {
							onElementClickListener.onClick();
						}
					}
					animateUp();

					return true;
				case MotionEvent.ACTION_MOVE:
					return true;
				default:
					if(pressed)
					{
						pressed = false;
						handler.removeCallbacks(runnable);
					}
					animateUp();
			}
		}
		return false;
	}


	private void checkPressedState()
	{
		if(pressed){
			pressed = false;
			if(null!=onElementClickListener) {
				onElementClickListener.onLongClick();
				if(vibro)((Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(10);
			}
		}
	}

	private void animateUp()
	{
		AnimatorSet regainer = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.regain_size);
		regainer.setTarget(this);
		regainer.start();
	}

	private void animateDown()
	{
		AnimatorSet reducer = (AnimatorSet) AnimatorInflater.loadAnimator(context, increase ? R.animator.increase_size_1_2 : R.animator.reduce_size);
		reducer.setTarget(this);
		reducer.start();
	}
}


