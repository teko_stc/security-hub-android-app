package biz.teko.td.SHUB_APP.SectionsTabs.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.Events.Adapters.UniEventListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 10.02.2017.
 */

public class SectionsHistoryTabFragment extends Fragment
{
	private Context context;
	private FrameLayout frameLayout;
	private ListView eventList;
	private TextView t;
	private DBHelper dbHepler;
	private View view;
	private int siteId;
	private Cursor cursor;
	private UniEventListAdapter eventListAdapter;
	private int deviceId;
	private boolean showRelays;
	private final int zoneOrRelay = 0;

	public static SectionsHistoryTabFragment newInstance(int siteId, int deviceId){
		SectionsHistoryTabFragment sectionsHistoryTabFragment = new SectionsHistoryTabFragment();
		Bundle b = new Bundle();
		b.putInt("site", siteId);
		b.putInt("device", deviceId);
		sectionsHistoryTabFragment.setArguments(b);
		return sectionsHistoryTabFragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		context = container.getContext();
		siteId = getArguments().getInt("site");
		deviceId = getArguments().getInt("device");
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		showRelays = Func.getBooleanSPDefTrue(sp, "pref_show_relays");
		//MAIN LAYOUT
		frameLayout =  (FrameLayout) new FrameLayout(container.getContext());
		frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		eventList = new ListView(container.getContext());
		t = new TextView(container.getContext());
		t.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		t.setGravity(Gravity.CENTER);
		dbHepler = DBHelper.getInstance(container.getContext());

		Cursor cursor = refreshCursor();
		eventListAdapter = new UniEventListAdapter(context , R.layout.card_event_uni, cursor , 0, 2, siteId);
		eventList.setAdapter(eventListAdapter);
		frameLayout.addView(eventList);

		view = frameLayout;
		return view;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		if(null!=eventListAdapter){
			Cursor cursor = refreshCursor();
			eventListAdapter.update(cursor);
		}
		getActivity().registerReceiver(eventsReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(eventsReceiver);
	}

	private final BroadcastReceiver eventsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			DBHelper dbHelper = DBHelper.getInstance(context);
			if(null!=eventListAdapter)
			{
				Cursor cursor = refreshCursor();
				eventListAdapter.update(cursor);
			}
		}
	};

	private Cursor refreshCursor(){
		Cursor cursor;
		ArrayList<Integer> classes = Func.getCurrentHistoryClasses(context);
		if(deviceId != -1)
		{
			cursor = dbHepler.getEventsForSiteDeviceWithFilter(siteId, deviceId, classes, showRelays, zoneOrRelay);
		}else{
			cursor = dbHepler.getEventsForSiteWithFilter(siteId, classes, showRelays, zoneOrRelay);
		}
		return cursor;
	}
}
