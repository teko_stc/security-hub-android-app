package biz.teko.td.SHUB_APP.Cameras.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ResourceCursorAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraBindListActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;

/**
 * Created by td13017 on 18.03.2018.
 */

public class NCameraBindedAdapter extends ResourceCursorAdapter
{
	private final DBHelper dbHelper;
	private final Context context;
	private int sending = 0;
	private D3Service myService;

	public void  update(Cursor c ){
		this.changeCursor(c);
	}

	public NCameraBindedAdapter(Context context, int layout, Cursor c, int flags)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
	}

	@Override
	public void bindView(View view, final Context context, Cursor cursor)
	{
		final Zone zone = new Zone(cursor);

		if(null!=zone)
		{
			NWithAButtonElement elementView = (NWithAButtonElement) view.findViewById(R.id.nDevicesListElement);

			elementView.setTitle(zone.name);
			elementView.setSubtitle(dbHelper.getSiteNameByDeviceSection(zone.device_id, zone.section_id));

			elementView.setDescImage(getDescImage(zone));

			elementView.setVibro(true);
			elementView.setOnRootLongClickListener(()->{
				NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
				nDialog.setTitle(context.getResources().getString(R.string.CAMERA_DEL_RELATION_DIALOG_MESS));
				nDialog.setOnActionClickListener((value, b) -> {
					switch (value){
						case NActionButton.VALUE_OK:
							UserInfo userInfo = dbHelper.getUserInfo();
							JSONObject message = new JSONObject();
							try
							{
								message.put("device", zone.device_id);
								message.put("section", zone.section_id);
								message.put("zone", zone.id);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							D3Request d3Request = D3Request.createMessage(userInfo.roles, "IV_DEL_RELATION", message);
							sendMessageToServer(d3Request);
							break;
						case NActionButton.VALUE_CANCEL:
							break;
					}
					nDialog.dismiss();
				});
				nDialog.show();
			});
		}
	}

	private Drawable getDescImage(Zone zone)
	{
		int descImage = R.drawable.n_image_device_common_list_selector;
		int model = zone.getModelId();
		switch (model){
			case Const.SENSORTYPE_WIRED_OUTPUT:
				DWOType dwoType = zone.getDWOType();
				if (null != dwoType && null != dwoType.icons)
				{
					descImage = dwoType.getListIcon(context);
				}else if(zone.detector != Const.DETECTOR_AUTO_RELAY && zone.detector != Const.DETECTOR_MANUAL_RELAY){
					descImage = R.drawable.n_image_wired_output_common_list_selector;
				}
				break;
			case Const.SENSORTYPE_WIRED_INPUT:
				DWIType dwiType = zone.getDWIType();
				if (null != dwiType && null != dwiType.icons)
				{
					descImage = dwiType.getListIcon(context);
					DType dType = dwiType.getDetectorType(zone.getDetector());
					if(null!=dType){
						AType aType = dType.getAlarmType(zone.getAlarm());
						if(null!=aType && null!=aType.icons){
							descImage = aType.getListIcon(context);
						}
					}
				}else{
					descImage = R.drawable.n_image_wired_input_common_list_selector;
				}
			case Const.SENSORTYPE_UNKNOWN:
				break;
			default:
				ZType zType = zone.getModel();
				if (null != zType && null != zType.icons)
				{
					descImage = zType.getListIcon(context);
					DType dType = zType.getDetectorType(zone.getDetector());
					if(null!=dType){
						AType aType = dType.getAlarmType(zone.getAlarm());
						if(null!=aType && null!=aType.icons){
							descImage = aType.getListIcon(context);
						}
					}
				}else{
					descImage = R.drawable.n_image_device_common_list_selector;
				}
				break;
		}
		return 0!=descImage ? context.getResources().getDrawable(descImage) : null;
	}

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			myService = getService();
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};

	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	public D3Service getService()
	{
		Activity activity = (NCameraBindListActivity)context;
		if(activity != null){
			NCameraBindListActivity NCameraBindListActivity = (NCameraBindListActivity) activity;
			return NCameraBindListActivity.getLocalService();
		}
		return null;
	}
}
