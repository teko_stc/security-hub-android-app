package biz.teko.td.SHUB_APP.Utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;

import java.util.Locale;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * This class is used to change your application locale and persist this change for the next time
 * that your app is going to be used.
 * <p/>
 * You can also change the locale of your application on the fly by using the setLocale method.
 * <p/>
 */

public class LocaleHelper {

	public static Context onAttach(Context context) {
//		Func.log_d(D3Service.LOG_TAG + "_Context", context.toString());
		String lang = getPersistedData(context);
//		Func.log_d(Const.LOG_TAG , "_onAttach " + context.toString());
//		Func.log_d(Const.LOG_TAG , "_onAttach lang " + lang);
		return setLocale(context, lang);
	}

	public static String getLanguage(Context context) {

		return getPersistedData(context);
	}

	public static Context setLocale(Context context, String language) {
		persist(context, language);
		Func.log_v(Const.LOG_TAG, "Change language to " + language);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			return updateResources(context, language);
		}

		return updateResourcesLegacy(context, language);
	}

	private static String getPersistedData(Context context) {
		if(Func.needLangForBuild(context)){
			String lang = Locale.getDefault().getLanguage();
			if (null==lang)
			{
				lang = Locale.getDefault().getDisplayLanguage();
			}
//			Func.log_d("D3Service", "App System Lang: " + lang);

			if(null == PrefUtils.getInstance(context).getCurrentLangNull()){// тут null , чтобы понять был уже язык записан или нет
				if(null!=lang && inLangList(lang, context)){
					PrefUtils.getInstance(context).setCurrentLang(lang);
				}else{
					PrefUtils.getInstance(context).setCurrentLang(context.getResources().getString(R.string.default_iso));
				}
			}
			return PrefUtils.getInstance(context).getCurrentLang();
		}
		return PrefUtils.getInstance(context).getCurrentLang();
	}

	private static boolean inLangList(String lang, Context context)
	{
		String[] isoArray = context.getResources().getStringArray(R.array.pref_language_values);
		for(String iso:isoArray){
			if(iso.equals(lang))
				return true;
		}
		return false;
	}

	/*save new locale in preferences*/
	private static void persist(Context context, String language) {
		PrefUtils.getInstance(context).setCurrentLang(language);
	}

	@TargetApi(Build.VERSION_CODES.N)
	private static Context updateResources(Context context, String language) {

		Locale locale = new Locale(language);
//		Func.log_d(D3Service.LOG_TAG , "New locale " + locale.getDisplayName());
		Locale.setDefault(locale);

//		Configuration configuration = context.getResources().getConfiguration();
		Configuration configuration = new Configuration(context.getResources().getConfiguration());
		configuration.setLocale(locale);
//		configuration.setLayoutDirection(locale);


		context = context.createConfigurationContext(configuration);
		context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
		context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());


		return context;
	}

	@SuppressWarnings("deprecation")
	private static Context updateResourcesLegacy(Context context, String language)
	{
		Locale locale = new Locale(language);
		Locale.setDefault(locale);

		Resources resources = context.getResources();

		Configuration configuration = resources.getConfiguration();
		configuration.locale = locale;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			configuration.setLayoutDirection(locale);
		}

		resources.updateConfiguration(configuration,
				resources.getDisplayMetrics());

		return context;
	}
}