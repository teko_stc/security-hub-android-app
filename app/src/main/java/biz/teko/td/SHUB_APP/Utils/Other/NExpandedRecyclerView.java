package biz.teko.td.SHUB_APP.Utils.Other;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class NExpandedRecyclerView extends RecyclerView {

    public NExpandedRecyclerView(@NonNull Context context) {
        super(context);
        setDynamicHeight();
    }

    public NExpandedRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setDynamicHeight();
    }

    public NExpandedRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setDynamicHeight();
    }

    private void setDynamicHeight() {
        ListAdapter adapter = (ListAdapter) getAdapter();
        if (adapter == null)
            return;
        int height = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(getWidth(), View.MeasureSpec.UNSPECIFIED);
        for (int i = 0; i < adapter.getCount(); i++) {
            View listItem = adapter.getView(i, null, this);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            height += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = height + ((adapter.getCount() - 1));
        setLayoutParams(layoutParams);
        requestLayout();
    }
}
