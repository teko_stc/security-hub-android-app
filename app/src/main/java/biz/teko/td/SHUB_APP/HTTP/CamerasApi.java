package biz.teko.td.SHUB_APP.HTTP;

import biz.teko.td.SHUB_APP.HTTP.Models.IVResponse;
import biz.teko.td.SHUB_APP.HTTP.Models.IVSetBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by td13017 on 22.02.2018.
 */

public interface CamerasApi
{
//	String base_iv_url = "openapi-alpha-eu01.ivideon.com/";
//	String base_web_url = "cloud.security-hub.ru/log/?";

	@POST("cameras?")
	Call<IVResponse> getCamerasList(@Query("op") String op, @Query("access_token") String access_token);

	@GET("cameras/{id}/live_preview?")
	Call<ResponseBody> getCameraPreview(@Path("id") String id, @Query("access_token") String access_token);

	@POST( "cameras/{id}/name?")
	Call<IVResponse> renameCamera(@Path("id") String id, @Query("op") String op, @Query("access_token") String access_token, @Body IVSetBody body);
}
