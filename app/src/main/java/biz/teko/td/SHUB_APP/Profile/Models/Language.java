package biz.teko.td.SHUB_APP.Profile.Models;

/**
 * Created by td13017 on 20.04.2018.
 */

public class Language
{
	public  String name, value;

	public Language(String name, String value){
		this.name = name;
		this.value = value;
	}
}
