package biz.teko.td.SHUB_APP.Sections_N.Activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

public class SectionTempLimitsActivity extends BaseActivity
{
	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private int sending = 0;

	private NMenuListElement switchLimits;
	private LinearLayout frameLimits;
	private TextInputLayout bottomLimitInputView;
	private TextInputLayout topLimitInputView;
	private NEditText bottomLimitView;
	private NEditText topLimitView;
	private Context context;
	private DBHelper dbHelper;
	private int section_id;
	private int device_id;
	private Section section;
	private Device device;
	private LinearLayout buttonBack;
	private LinearLayout buttonSet;
	private boolean transition;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_section_limits);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		section_id = getIntent().getIntExtra("section", -1);
		device_id = getIntent().getIntExtra("device", -1);
		transition = getIntent().getBooleanExtra("transition", false);

		setup();
		bind();

		addTransitionListener(transition);

	}

	private void setup()
	{
		buttonBack = (LinearLayout) findViewById(R.id.nButtonClose);
		buttonSet = (LinearLayout) findViewById(R.id.nButtonChange);

		switchLimits = (NMenuListElement) findViewById(R.id.nMenuTempLimitsSwitch);
		frameLimits = (LinearLayout) findViewById(R.id.nMenuTempLimitsFrame);
		bottomLimitInputView = (TextInputLayout) findViewById(R.id.nInputLayoutBottomLimit);
		topLimitInputView = (TextInputLayout) findViewById(R.id.nInputLayoutTopLimit);

		bottomLimitView = (NEditText) findViewById(R.id.nEditBottomLimit);
		topLimitView = (NEditText) findViewById(R.id.nEditTopLimit);
	}

	private void bind()
	{
		section = dbHelper.getDeviceSectionByIdWithExtra(device_id, section_id);
		device = dbHelper.getDeviceById(device_id);

		if(null!=buttonBack) buttonBack.setOnClickListener(v -> onBackPressed());

		if(null!=section)
		{
			if (null != switchLimits)
			{
				switchLimits.setChecked(null != section.low_limit || null != section.high_limit);
				changeVisibility();

				switchLimits.setOnCheckedChangeListener(new NMenuListElement.OnCheckedChangeListener()
				{
					@Override
					public void onCheckedChanged()
					{
						changeVisibility();
						if(null!=buttonSet) buttonSet.setEnabled(getEnableState());
					}
				});
			}

			if(null!=bottomLimitView){
				if(null!=section.low_limit) bottomLimitView.setText(section.low_limit);
				bottomLimitView.setOnEditorActionListener(new TextView.OnEditorActionListener()
				{
					@Override
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
					{
						if(actionId == EditorInfo.IME_ACTION_DONE){
							if(getEnableState()) setSection();
						}
						return false;
					}
				});
				bottomLimitView.addTextChangedListener(getTextWatcher());
			}

			if(null!=topLimitView) {
				if(null!=section.high_limit ) topLimitView.setText(section.high_limit);
				topLimitView.addTextChangedListener(getTextWatcher());
			}

			if (null!=buttonSet){
				buttonSet.setEnabled(getEnableState());
				buttonSet.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						setSection();
					}
				});
			}
		}
	}

	private boolean getEnableState()
	{
		String top_c = section.high_limit;
		String bottom_c = section.low_limit;
		String top_n = getTopLimit();
		String bottom_n = getBottomLimit();

		if((null!=top_c || null!=bottom_c) && !switchLimits.isChecked())
			return true;

		if(top_n.equals(bottom_n)) return false;

		if((!top_n.equals("") ? (null== top_c || !top_c.equals(top_n)) : null!= top_c)
			|| (!bottom_n.equals("") ? (null == bottom_c || !bottom_c.equals(bottom_n)) : null!=bottom_c))
		{
			return true;
		}
		return false;
	}

	public String getTopLimit(){
		if(null!=topLimitView && null!=topLimitView.getText()) return topLimitView.getText().toString();
		return "";
	}

	public String getBottomLimit(){
		if(null!=bottomLimitView && null!=bottomLimitView.getText()) return bottomLimitView.getText().toString();
		return "";
	}

	private TextWatcher getTextWatcher()
	{
		return new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
				if (null!=buttonSet)
					buttonSet.setEnabled(getEnableState());
			}

			@Override
			public void afterTextChanged(Editable s)
			{

			}
		};
	}



	private void setSection()
	{
		if(!switchLimits.isChecked()){
			sendSetSectionCommand();
		}else if(getTopLimit().equals("") || getBottomLimit().equals("")){
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
			nDialog.setTitle(getString(R.string.N_SECTION_TEMO_LIMIT_ALERT_ONLY_ONE_LIMIT));
			nDialog.setPositiveButton(getResources().getString(R.string.N_BUTTON_YES), () -> {
				sendSetSectionCommand();
				nDialog.dismiss();
			});
			nDialog.setNegativeButton(()->{
				nDialog.dismiss();
			});
			nDialog.show();
		}else if(Integer.valueOf(getTopLimit()) < Integer.valueOf(getBottomLimit())){
			Func.nShowMessageSmall(context, getString(R.string.N_SECTION_TEMP_LIMIT_ALERT_TOP_SMALLER_BOTTOM));
		}else{
			sendSetSectionCommand();
		}
	}

	private void sendSetSectionCommand()
	{
		JSONObject commandAddr = new JSONObject();
		try
		{
			commandAddr.put("index", section.id);
			commandAddr.put("name", section.name);
			commandAddr.put("type", section.detector);
			if(switchLimits.isChecked())
			{
				String topS = getTopLimit();
				String bottomS = getBottomLimit();
				if (null != bottomS && !bottomS.equals("")) commandAddr.put("low_temp", Integer.parseInt(bottomS));
				if (null != topS && !topS.equals("")) commandAddr.put("high_temp", Integer.parseInt(topS));
			}
			if(nSendMessageToServer(section, Command.SECTION_SET, commandAddr, true)){
				onBackPressed();
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void changeVisibility()
	{
		if(null!=frameLimits) frameLimits.setVisibility(switchLimits.isChecked() ? View.VISIBLE : View.GONE);
		switchLimits.setSubtitle(switchLimits.isChecked() ? getString(R.string.N_WIZ_SECTION_LIMITS_ALREADY_SET) : getResources().getString(R.string.N_WIZ_SECTION_LIMITS_SUBTITLE));
	}

	public void  onResume(){
		super.onResume();

		bindD3();
	}

	public void onPause(){
		super.onPause();
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};
}
