package biz.teko.td.SHUB_APP.Wizards.Activity.Login;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.StartUpActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

/**
 * Created by td13017 on 07.06.2017.
 */

public class WizardLoginActivity extends BaseActivity {
    private static final String TAG = "WizardLoginActivity";
    private static final int SERVICE_BEEN_STOPED = 16;
    private static final int CONNECTION_DROP = 17;
    private static final long TIMEOUT = 10000;
    private static final int NEW_PROTOCOL_VERSION = 18;

	private NEditText loginText;
	private NEditText passwordText;
	private NActionButton loginButton;
	private NActionButton signupLink;
    public Intent intent;
    private ServiceConnection sConn;
    private D3Service myService;
    private NTopToolbarView toolbar;

    public UserInfo userInfo;
    private Context context;
    private WizardLoginActivity wizardLoginActivity;
    private TextView passRecLink;
    private boolean bound;
    private String spLogin = "";
    private String spPass = "";
    private int logged = 0;
    private CheckBox checkBox;

    public WizardLoginActivity() {
    }

    @Override
    protected void onStop() {
        Func.hideEditTextKeyboard(loginText);
        Func.hideEditTextKeyboard(passwordText);
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        context = this;
        wizardLoginActivity = this;
        intent = new Intent(this, D3Service.class);
        bindService(intent, sConn, 0);
        registerReceiver(connectionDropReceiver, new IntentFilter(D3Service.BROADCAST_CONNECTION_DROP));
        registerReceiver(loginReceiver, new IntentFilter(D3Service.BROADCAST_HANDSHAKE_OK));
        registerReceiver(connectionErrorReceiver, new IntentFilter(D3Service.BROADCAST_CONNECTION_ERROR_REPEATED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(sConn);
        unregisterReceiver(connectionDropReceiver);
        unregisterReceiver(loginReceiver);
        unregisterReceiver(connectionErrorReceiver);
    }

    private final BroadcastReceiver loginReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Intent intent1 = new Intent(getBaseContext(), StartUpActivity.class);
            intent1.putExtra("FROM", D3Service.OPEN_FROM_LOGIN);
            startActivity(intent1);
            overridePendingTransition(biz.teko.td.SHUB_APP.R.animator.enter, biz.teko.td.SHUB_APP.R.animator.exit);
            finish();
        }
    };

    private final BroadcastReceiver connectionDropReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != myService) {
                Func.log_d(D3Service.LOG_TAG, "Stop connection from login activity ");
                myService.stopConnection();
            }
            Func.pushToast(context, Func.handleResult(context, intent.getIntExtra("result", 0)), (WizardLoginActivity) context);
            returnLoginButton();
            resetLastUserPreferences();
        }
    };

    private final BroadcastReceiver connectionErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != myService)
                myService.dropCredentials(false); //! connection problems when log in (except infinity log in, server\ll stop after drop credentials)
            returnLoginButton();
            resetLastUserPreferences();
            Func.pushToast(context, getString(R.string.ERROR_CANT_CONNECT_TO_SERVER_CHECK_CONNECTION), (WizardLoginActivity) context);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_wizard_login);
        DBHelper dbHelper = DBHelper.getInstance(this);
        context = this;

        toolbar = (NTopToolbarView) findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);

        sConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("D3LOGIN", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = (D3Service) myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d("D3LOGIN", "onServiceDisconnected");
                bound = false;
            }
        };

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);

        checkBox = (CheckBox) findViewById(R.id.la_remember_check);

        logged = sp.getInt("logged", 0);
        if (1 == logged) {
            spLogin = sp.getString("login", "");
            if (!spLogin.equals("")) {
                spPass = sp.getString(spLogin, "");
            }
            checkBox.setChecked(true);
        }

        loginText = (NEditText) findViewById(R.id.wizLoginEditLogin);
        loginText.setText(spLogin);
        loginText.showKeyboard();
        loginText.setOnClickListener(v -> {
            if (logged == 1) {
                spPass = "";
                passwordText.setText(spPass);
                logged = 0;
            }
        });
        loginText.setOnEditorActionListener((textView, actionId, keyEvent) -> {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                passwordText.setFocus();
                return true;
            }
            return false;
        });

        passwordText = (NEditText) findViewById(R.id.wizLoginEditPass);
        if (!spPass.equals("")) {
            passwordText.setText("password");
        }
        passwordText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login();
                return true;
            }
            return false;
        });
        passwordText.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD);
        passwordText.setTransformationMethod(new Func.AsteriskPasswordTransformationMethod());
        passwordText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (logged == 1) {
                        spPass = "";
                        passwordText.setText(spPass);
                        logged = 0;
                    }
                }
            }
        });

        loginButton = (NActionButton) findViewById(R.id.wizLoginButtonLogin);
        loginButton.requestFocus();
        loginButton.setEnabled(true);

        signupLink = (NActionButton) findViewById(R.id.wizLoginButtonReg);
        signupLink.setOnButtonClickListener(v -> {
            NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_vertical_small_link);
            nDialog.setTitle(getResources().getString(R.string.LA_SINGUP_DIALOG_MESS_1) + " " + getResources().getString(R.string.application_name) + " " + getString(R.string.LA_SINGUP_DIALOG_MESS_2));
            nDialog.setOnActionClickListener((value, b) -> {
                switch (value){
                    case NActionButton.VALUE_OK:
                        nDialog.dismiss();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Func.getRegLink(context));
                        startActivity(browserIntent);
                        break;
                    case NActionButton.VALUE_CANCEL:
                        nDialog.dismiss();
                        break;
                }
            });
            nDialog.show();
        });

        passRecLink = (TextView) findViewById(R.id.wizLoginLostPass);
        passRecLink.setOnClickListener(v -> {
            NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_vertical_small_link);
            nDialog.setTitle(getResources().getString(R.string.LA_PASS_REC_DIALOG_MESS_1) + " " + getResources().getString(R.string.application_name)
                    + " " + getString(R.string.LA_PASS_REC_DIALOG_MESS_2));
            nDialog.setOnActionClickListener((value, b) -> {
                switch (value){
                    case NActionButton.VALUE_OK:
                        nDialog.dismiss();
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Func.getRecLink(context));
                        startActivity(browserIntent);break;
                    case NActionButton.VALUE_CANCEL:
                        nDialog.dismiss();
                        break;
                }
            });
            nDialog.show();
        });

        loginButton.setOnButtonClickListener(v -> login());

    }

    private void openLink() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Func.getRegLink(this));
        startActivity(browserIntent);
    }

    public void login() {
        Log.d(TAG, "Login");
        if (loginText.getText().toString().isEmpty()) {
            loginText.setFocus();
        }
        else if (passwordText.getText().toString().isEmpty()) {
            passwordText.setFocus();
        }
        else {
            Func.hideEditTextKeyboard(loginText);
            Func.hideEditTextKeyboard(passwordText);
            userInfo = new UserInfo();
            if (loginButton.isEnabled()) {
                wizardLoginActivity.runOnUiThread(() -> {
                    loginButton.setText(R.string.WAITING_ENTER);
                    loginButton.setEnabled(false);
                });
                //проверем запущен ли сервис
                if (Func.isServiceRunning(context, D3Service.class)) {
                    //проверяем подцепился ли к сервису, на случай умышленного отклю чения из манифеста
                    if (null != myService) {
                        spLogin = loginText.getText().toString().trim();
                        if (logged == 0) {
                            spPass = passwordText.getText().toString();
                            if (!spPass.equals("")) {
                                spPass = Func.md5(spPass);
                            }
                        }
                        if (validate()) {
//						String request = "GET /?user=" + spLogin + "&pass=" + spPass + " HTTP/1.1\r\n\r\n\r\n";
                            myService.setCredentials(spLogin, spPass, true);
                            myService.shakeConnection(false);
                            if (checkBox.isChecked()) {
                                rememberLastUserPreferences();
                            } else {
                                resetLastUserPreferences();
                            }
                        } else {
                            returnLoginButton();
                        }
                    } else {
                        pushToast(getString(R.string.application_name) + ": " + getString(R.string.SERVICE_DEAD_TOAST), wizardLoginActivity);
                        Log.d("D3LOGIN", "SERVICE DEAD");
                        returnLoginButton();
                    }
                } else {
                    Func.startD3Service(context, "Wizard Login Activity");
                    returnLoginButton();
                }
            }
        }
    }

    private void returnLoginButton() {
        loginText.showKeyboard();
        wizardLoginActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loginButton.setText(R.string.WIZ_LOGIN_GO_ENTER);
                loginButton.setEnabled(true);
            }
        });
    }

    public void pushToast(final String s, final Activity loginActivity) {
        loginActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast = Toast.makeText(getBaseContext(), s, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });
    }

    public boolean validate() {
        boolean valid = true;

//		String login = loginText.getText().toString();
//		String unsecurePass = passwordText.getText().toString();

        if (spLogin.isEmpty()) {
            //			loginLayout.setError("Неверный логин/пароль");
            Toast toast = Toast.makeText(getBaseContext(), R.string.ERROR_ENTER_LOGIN, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.CENTER, 0, 0);
            toast.show();
            valid = false;
        } else {
            if (spPass.isEmpty()) {
                Toast toast = Toast.makeText(getBaseContext(), R.string.ERROR_ENTER_PASS, Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
                valid = false;
            }
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        if (-1 == getIntent().getIntExtra("FROM", 0)) {
            finishAffinity();
        } else {
            super.onBackPressed();
            overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
        }
    }

    private void rememberLastUserPreferences() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putInt("logged", 1).apply();
        sp.edit().putString("login", spLogin).apply();
        sp.edit().putString(spLogin, spPass).apply();
    }

    private void resetLastUserPreferences() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putInt("logged", 0).apply();
        sp.edit().putString("login", "").apply();
        sp.edit().putString(spLogin, "").apply();
        sp.edit().putInt(spLogin + "_pass_length", 0).apply();
    }

}
