package biz.teko.td.SHUB_APP.D3.D3UniConfigUI;

import android.util.Log;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3UniConfig.ConfigNode;
import biz.teko.td.SHUB_APP.D3.D3UniConfig.InterfaceNodeGroup;
import biz.teko.td.SHUB_APP.D3.D3UniConfig.UniConfig;

/**
 * Created by td13017 on 25.07.2016.
 */

@Root(name="preference-headers")
public class HeadersUI
{
	@Attribute(name="xmlns:android")
	public String attr;

	@ElementList
	public LinkedList<HeaderUI> headerUIs;

	public HeadersUI(){};

	public void getXML(){
		UniConfig uniConfig = D3Service.uniConfig;

		//get all CONFIG NODES
		LinkedList<ConfigNode> configNodeList = D3Service.uniConfig.configNodes;

		//WORK WITH CONCRETE CONFIG NODE
		//GET CONFIG NODE
		ConfigNode configNode = configNodeList.get(0);

		//GET GROUPS FROM CONFIG NODE
		LinkedList<InterfaceNodeGroup> interfaceNodeGroupsList = configNode.Groups;

		Serializer  s = new Persister();
		OutputStream stream = new ByteArrayOutputStream();
		this.attr = "http://schemas.android.com/apk/res/android";
		this.headerUIs = new LinkedList<>();
		for(InterfaceNodeGroup interfaceNodeGroup:interfaceNodeGroupsList){
			HeaderUI headerUI = new HeaderUI();
			headerUI.andr = "biz.teko.SHUP_APP.Activity.HardwarePreferenceFragment";
			headerUI.title = interfaceNodeGroup.Caption;
			this.headerUIs.add(headerUI);
		}
		try
		{
//			File file = new File(String.valueOf(R.xml.preference_headers));
			s.write(this, stream);
			Log.d("D3UNICONFIG", stream.toString());
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
