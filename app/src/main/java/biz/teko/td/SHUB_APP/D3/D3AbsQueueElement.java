package biz.teko.td.SHUB_APP.D3;
/**
 * Created by td13017 on 07.06.2016.
 */
public abstract class D3AbsQueueElement extends D3QueueElement
{
	public REQ_TYPE req_type;
	public String data;
	public abstract void receiver(String data);

	public D3AbsQueueElement(REQ_TYPE req_type, String data) {
		super(req_type, data);
		this.req_type = req_type;
		this.data = data;
	}
}
