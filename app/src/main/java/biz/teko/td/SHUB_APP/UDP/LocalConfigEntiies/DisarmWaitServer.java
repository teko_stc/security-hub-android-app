package biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies;

import java.util.Map;

public class DisarmWaitServer extends BaseBooleanConfigEntity {

    public DisarmWaitServer() {
        map.put(false, "Allow");
        map.put(true, "Disallow");
        setValues();
    }

    public boolean get(byte flag) {
        now = ((1 << 1 & flag) > 0);
        return now;
    }

    public void set(byte[] flags, String choice) {
        for (Map.Entry<Boolean, String> entry : map.entrySet()) {
            if (entry.getValue().equals(choice))
                now = entry.getKey();
        }
        byte mask;
        mask = (byte) (1 << 1);
        if (now)
            flags[1] = (byte) (flags[1] | mask);
        else
            flags[1] = (byte) (flags[1] & ~mask);

    }
}
