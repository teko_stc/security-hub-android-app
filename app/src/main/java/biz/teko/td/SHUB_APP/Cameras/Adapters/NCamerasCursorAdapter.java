package biz.teko.td.SHUB_APP.Cameras.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;

import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NCameraGridElement;

public class NCamerasCursorAdapter extends ResourceCursorAdapter
{
	private final Context context;
	private final DBHelper dbHelper;
	private final Camera.CType type;
	private OnCamerasElementClickListener onCamerasElementClickListener;

//	public void setOnCamerasElementClickListener(OnCamerasElementClickListener onCamerasElementClickListener){
//		this.onCamerasElementClickListener = onCamerasElementClickListener;
//	}

	public interface OnCamerasElementClickListener{
		void onClick(Camera camera);
		void onLongClick(Camera camera);
		void onLoaded();
	}

	public NCamerasCursorAdapter(Context context, int layout, Cursor c, Camera.CType type, int flags, OnCamerasElementClickListener onCamerasElementClickListener)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.onCamerasElementClickListener = onCamerasElementClickListener;
		this.type = type;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor)
	{
		NCameraGridElement v = (NCameraGridElement) view;
		Camera camera = null;
		switch (type){
			case RTSP:
				camera =new Camera(cursor.getInt(0), cursor.getString(1), cursor.getString(2));
				if(null!=camera){
					v.setTitle(camera.name);
				}
				break;
			case IV:
				camera =new Camera(cursor);
				if(null!=camera){
					v.setTitle(camera.name);
					Zone[] zones =  dbHelper.getZonesByIVCameraId(camera.id);
					v.setSubTitle(null!=zones ? context.getString(R.string.N_IV_BINDED) : context.getString(R.string.N_IV_NOT_BINDED));
					v.setPreview(camera, dbHelper.getUserInfoIVToken());
				}
				break;
			case DAHUA:
				camera = new Camera(cursor, type);
				if(null!=camera){
					v.setTitle(camera.name);
					v.setPreview(camera, null);

//					if(nDahuaHelper.startP2pService(camera.sn, camera.login, camera.pass)){
//						nDahuaHelper.setSnapRevCallBack((l, bytes, i, i1, i2) -> {
//
//						});
//						nDahuaHelper.remoteCapturePicture(0);
//					}
				}
				break;
		}
		Camera finalCamera = camera;
		v.setOnElementClickListener(new NCameraGridElement.OnElementListener()
		{
			@Override
			public void onClick()
			{
				if(null!=onCamerasElementClickListener) onCamerasElementClickListener.onClick(finalCamera);
			}

			@Override
			public void onLongClick()
			{
				if(null!=onCamerasElementClickListener) onCamerasElementClickListener.onLongClick(finalCamera);
			}

			@Override
			public void onLoaded()
			{
				if(null!=onCamerasElementClickListener) onCamerasElementClickListener.onLoaded();
			}
		});
	}

	public void update(Cursor cursor)
	{
		this.changeCursor(cursor);
	}
}
