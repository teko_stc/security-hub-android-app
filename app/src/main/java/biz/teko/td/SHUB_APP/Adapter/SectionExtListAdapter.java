//package biz.teko.td.SHUB_APP.Adapter;
//
//import android.app.Activity;
//import android.app.AlertDialog;
//import android.content.Context;
//import android.database.Cursor;
//import android.graphics.Typeface;
//import android.os.AsyncTask;
//import android.support.v4.widget.ResourceCursorAdapter;
//import android.text.InputType;
//import android.util.Log;
//import android.util.TypedValue;
//import android.view.Gravity;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.LinearLayout;
//import android.widget.ListView;
//import android.widget.RadioButton;
//import android.widget.TextView;
//
//import java.lang.reflect.Field;
//import java.util.LinkedList;
//
//import biz.teko.td.SHUB_APP.Utils.Dir.Func;
//import biz.teko.td.SHUB_APP.OldServerLib.ElementActivity;
//import biz.teko.td.SHUB_APP.D3.D3AbsQueueElement;
//import biz.teko.td.SHUB_APP.D3.D3Service;
//import biz.teko.td.SHUB_APP.D3DB.Affect;
//import biz.teko.td.SHUB_APP.D3DB.DBHelper;
//import biz.teko.td.SHUB_APP.D3DB.Device;
//import biz.teko.td.SHUB_APP.D3DB.Section;
//import biz.teko.td.SHUB_APP.D3DB.UserInfo;
//import biz.teko.td.SHUB_APP.R;
//
///**
// * Created by td13017 on 21.09.2016.
// */
//public class SectionExtListAdapter extends ResourceCursorAdapter
//{
//	private final int layout;
//	private final ElementActivity activity;
//	Context context;
//	int site;
//	Cursor cursor;
//	private DBHelper dbHelper;
//	private ListView devicesList;
//	private Cursor deviceCursor;
//	private DevicesExtListAdapter devicesExtListAdapter;
//	private D3Service service;
//	private D3Service myService;
//	private Button addZoneButton;
//	private TextView t;
//	private Section section;
//
//	public SectionExtListAdapter(Context context, int layout, int site, Cursor c, int flags)
//	{
//		super(context, layout, c, flags);
//		this.cursor = c;
//		this.site = site;
//		this.context = context;
//		this.layout = layout;
//		this.activity = (ElementActivity) context;
//	}
//
//	public void update(Cursor cursor){
//		this.changeCursor(cursor);
////		Section section = dbHelper.getSection(cursor);
////		if(null!=devicesExtListAdapter)
////		{
////			deviceCursor = dbHelper.getAllDevicesCursorForSection(site, section.id);
////			devicesExtListAdapter.update(deviceCursor);
////			if(0!=devicesExtListAdapter.getCount())
////			{
////				t.setVisibility(View.GONE);
////				devicesList.setVisibility(View.VISIBLE);
////			}else
////			{
////				t.setVisibility(View.VISIBLE);
////				t.setText(R.string.EZONES_NOZONES);
////				devicesList.setVisibility(View.GONE);
////			}
////		}
////		else{
////			t.setVisibility(View.VISIBLE);
////			t.setText(R.string.EZONES_NOZONES);
////			devicesList.setVisibility(View.GONE);
////		}
//	}
//
//	private void setTextViewText(View view, Context context, int id, int color,  String string){
//		TextView textView = (TextView) view.findViewById(id);
//		if(null!=textView){
//			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
//			textView.setTextColor(color);
//			textView.setText(string);
//		}
//	}
//
//	@Override
//	public void bindView(final View view, Context context, Cursor cursor)
//	{
//		dbHelper = DBHelper.newInstance(context);
//		section = dbHelper.getSection(cursor);
//		Device[] devices = dbHelper.getAllDevicesForSection(section);
//		int devCount;
//		if(null!=devices)
//		{
//			devCount = devices.length;
//		}else{
//			devCount = 1;
//		}
//
//		t = (TextView) view.findViewById(R.id.sle_text);
//
//		addZoneButton = (Button) view.findViewById(R.id.sle_addzone_button);
//		addZoneButton.setOnClickListener(new AddZoneOnClickListener());
//		addZoneButton.setOnTouchListener(new Func.ButtonOnTouch(context, R.color.brandColorWhite, R.color.brandColorDarkGrey, R.color.brandColorDark, R.color.brandColorWhite));
//
//		devicesList = (ListView) view.findViewById(R.id.devicesList);
//		devicesList.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, devCount * Func.dpToPx(133, context)));
//		devicesList.setDivider(null);
//
////		OpenZonesTask openZonesTask = new OpenZonesTask();
////		openZonesTask.execute();
//
////		deviceCursor = dbHelper.getAllDevicesCursorForSection(site, section.id);
////		devicesExtListAdapter = new DevicesExtListAdapter(context, R.layout.device_list_element_view, site, section.id,deviceCursor, 0 );
////		devicesList.setAdapter(devicesExtListAdapter);
//
//			//GET CURSOR
//		deviceCursor = dbHelper.getAllDevicesCursorForSection(site, section.id);
//		devicesExtListAdapter = new DevicesExtListAdapter(context, R.layout.device_list_element_view, site, section.id,deviceCursor, 0 );
//		devicesList.setAdapter(devicesExtListAdapter);
//		if(0!=devicesExtListAdapter.getCount())
//		{
//			t.setVisibility(View.GONE);
//			devicesList.setVisibility(View.VISIBLE);
//		}else
//		{
//			t.setText(R.string.EZONES_NOZONES);
//			t.setVisibility(View.VISIBLE);
//			devicesList.setVisibility(View.GONE);
//		}
//
//	}
//
//
//	private class OpenZonesTask extends AsyncTask<Void, Void, Boolean>
//	{
//		@Override
//		protected void onPreExecute()
//		{
//			t.setText(R.string.EZONES_GETDATA);
////			t.setVisibility(View.VISIBLE);
//		}
//
//		@Override
//		protected Boolean doInBackground(Void... params)
//		{
//			try{
//				//GET CURSOR
//
//				deviceCursor = dbHelper.getAllDevicesCursorForSection(site, section.id);
//				devicesExtListAdapter = new DevicesExtListAdapter(context, R.layout.device_list_element_view, site, section.id,deviceCursor, 0 );
//			}
//			catch (Exception e) {
//
//				e.printStackTrace();
//				return false;
//			}
//			if(null!=devicesExtListAdapter)
//			{
//				if(0!=devicesExtListAdapter.getCount())
//				{
//					return true;
//				}else
//				{
//					return false;
//				}
//			}else{
//				return false;
//			}
//		}
//
//		@Override
//		protected void onPostExecute(Boolean result)
//		{
//			if(result)
//			{
//				t.setVisibility(View.GONE);
//				devicesList.setAdapter(devicesExtListAdapter);
//			}else{
//				t.setText(R.string.EZONES_NOZONES);
//			}
//
//		}
//	}
//
////	private class MoreOnClickListener implements View.OnClickListener
////	{
////		Section section;
////		public MoreOnClickListener(Section section)
////		{
////			this.section = section;
////		}
////
////		@Override
////		public void onClick(View v)
////		{
////			AlertDialog.Builder moreDialogBuilder = Func.adbForCurrentSDK(context);
////			final AlertDialog moreDialog = moreDialogBuilder.create();
////			LinearLayout patternLayout = new LinearLayout(context);
////			patternLayout.setOrientation(LinearLayout.VERTICAL);
////			patternLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
////			patternLayout.setGravity(Gravity.CENTER);
////
////			TextView nameText = new TextView(context);
////			nameText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
////			nameText.setGravity(Gravity.CENTER);
////			nameText.setText(section.name);
////			nameText.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
////			nameText.setTypeface(null, Typeface.BOLD);
////			nameText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
////			patternLayout.addView(nameText);
////
////			LinearLayout buttonsLayout = new LinearLayout(context);
////			buttonsLayout.setOrientation(LinearLayout.VERTICAL);
////			buttonsLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
////			buttonsLayout.setPadding(0, Func.dpToPx(12, context), Func.dpToPx(12, context), 0);
////			buttonsLayout.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
////			buttonsLayout.setGravity(Gravity.CENTER);
////
////			final Button buttonAddDev = new Button(context);
////			buttonAddDev.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
////			buttonAddDev.setText("ДОБАВИТЬ ДАТЧИК");
////			buttonAddDev.setTypeface(null, Typeface.BOLD);
////			buttonAddDev.setTextColor(context.getResources().getColor(R.color.md_black_1000));
////			buttonAddDev.setGravity(Gravity.LEFT);
////			buttonAddDev.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
////			buttonAddDev.setOnTouchListener(new View.OnTouchListener()
////			{
////				@Override
////				public boolean onTouch(View v, MotionEvent event)
////				{
////					switch (event.getAction())
////					{
////						case MotionEvent.ACTION_DOWN:
////							buttonAddDev.setTextColor(context.getResources().getColor(R.color.md_grey_600));
////							break;
////						case MotionEvent.ACTION_UP:
////							buttonAddDev.setTextColor(context.getResources().getColor(R.color.md_black_1000));
////							break;
////					}
////					return false;
////				}
////			});
////			buttonAddDev.setOnClickListener(new View.OnClickListener()
////			{
////				@Override
////				public void onClick(View v)
////				{
////					UserInfo userInfo = dbHelper.getUserInfo();
////					myService = getService();
////					myService.addInNormalQueue(new D3AbsQueueElement("{\"S\":\"" + userInfo.getSession() + "\",\"Q\":\"COMMAND\",\"D\":{\"command\":{\"REGISTER\":" + section.id + "},\"site\":" + site + "}}")
////					{
////						@Override
////						public void receiver(String data)
////						{
////							Log.d("D3MAIN", data);
////						}
////					});
////				}
////			});
////
////			final Button buttonAddOnMain = new Button(context);
////			buttonAddOnMain.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
////			buttonAddOnMain.setGravity(Gravity.LEFT);
////			buttonAddOnMain.setTypeface(null, Typeface.BOLD);
////			buttonAddOnMain.setTextColor(context.getResources().getColor(R.color.md_black_1000));
////			buttonAddOnMain.setText("ДОБАВИТЬ НА ГЛАВНУЮ");
////			buttonAddOnMain.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
////			buttonAddOnMain.setOnTouchListener(new View.OnTouchListener()
////			{
////				@Override
////				public boolean onTouch(View v, MotionEvent event)
////				{
////					switch (event.getAction())
////					{
////						case MotionEvent.ACTION_DOWN:
////							buttonAddOnMain.setTextColor(context.getResources().getColor(R.color.md_grey_600));
////							break;
////						case MotionEvent.ACTION_UP:
////							buttonAddOnMain.setTextColor(context.getResources().getColor(R.color.md_black_1000));
////							break;
////					}
////					return false;
////				}
////			});
////
////			buttonsLayout.addView(buttonAddOnMain);
////
//////			View dV1 = new View(context);
//////			dV1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Func.dpToPx(1, context)));
//////			dV1.setBackgroundColor(context.getResources().getColor(R.color.md_grey_500));
//////			dV1.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), 0);
//////			patternLayout.addView(dV1);
////
////			LinearLayout footerButtons = new LinearLayout(context);
////			footerButtons.setOrientation(LinearLayout.VERTICAL);
////			footerButtons.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
////			footerButtons.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
////			footerButtons.setGravity(Gravity.RIGHT);
////			buttonsLayout.addView(footerButtons);
////
////			final Button buttonCancel = new Button(context);
////			buttonCancel.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
////			buttonCancel.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
////			buttonCancel.setGravity(Gravity.RIGHT);
////			buttonCancel.setTypeface(null, Typeface.BOLD);
////			buttonCancel.setTextColor(context.getResources().getColor(R.color.md_black_1000));
////			buttonCancel.setText("ЗАКРЫТЬ");
////			buttonCancel.setOnTouchListener(new View.OnTouchListener()
////			{
////				@Override
////				public boolean onTouch(View v, MotionEvent event)
////				{
////					switch (event.getAction())
////					{
////						case MotionEvent.ACTION_DOWN:
////							buttonCancel.setTextColor(context.getResources().getColor(R.color.md_grey_600));
////							break;
////						case MotionEvent.ACTION_UP:
////							buttonCancel.setTextColor(context.getResources().getColor(R.color.md_black_1000));
////							break;
////					}
////					return false;
////				}
////			});
////			buttonCancel.setOnClickListener(new View.OnClickListener()
////			{
////				@Override
////				public void onClick(View v)
////				{
////					moreDialog.dismiss();
////				}
////			});
//////			footerButtons.addView(buttonCancel);
////
////
////			patternLayout.addView(buttonsLayout);
////			moreDialog.setView(patternLayout);
////			moreDialog.show();
////		}
////	};
//
//	public D3Service getService()
//	{
//		Activity activity = (ElementActivity)context;
//		if(activity != null){
////			ElementActivity elementActivity = (ElementActivity) activity;
////			return elementActivity.getLocalService();
//			return null;
//		}
//		return service;
//	}
//
//	private class AddZoneOnClickListener implements View.OnClickListener
//	{
//		@Override
//		public void onClick(View v)
//		{
//			if (!dbHelper.getSiteArmStatus(site))
//			{
//				AlertDialog.Builder addDevDialogBuilder = Func.adbForCurrentSDK(context);
//				final AlertDialog addDevDialog = addDevDialogBuilder.create();
//				//				addDevDialog.setCancelable(false);
//
//				LinearLayout parentLayout = new LinearLayout(context);
//				parentLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//				parentLayout.setOrientation(LinearLayout.VERTICAL);
//				parentLayout.setPadding(Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context));
//				parentLayout.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//
//				TextView titleText = new TextView(context);
//				titleText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//				titleText.setPadding(0, 0, 0, Func.dpToPx(12, context));
//				titleText.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//				titleText.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//				titleText.setTypeface(null, Typeface.BOLD);
//				titleText.setText(R.string.ADD_DETECTOR_DIALOG_TITLE);
//				parentLayout.addView(titleText);
//
//				final RadioButton radioAuto = new RadioButton(context);
//				radioAuto.setText(R.string.DETECTOR_ADD_AUTO);
//				radioAuto.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//				radioAuto.setChecked(true);
//				radioAuto.setHighlightColor(context.getResources().getColor(R.color.brandColorBlack));
//
//				parentLayout.addView(radioAuto);
//
//				final RadioButton radioManual = new RadioButton(context);
//				radioManual.setText(R.string.DETECTOR_ADD_MANUAL);
//				radioManual.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//				radioManual.setHighlightColor(context.getResources().getColor(R.color.brandColorBlack));
//				parentLayout.addView(radioManual);
//
//
//				final TextView messText = new TextView(context);
//				messText.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//				messText.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//				messText.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//				messText.setText(R.string.ADD_DETECTOR_DIALOG_MESS);
//				parentLayout.addView(messText);
//
//				LinearLayout editTextLayout = new LinearLayout(context);
//				editTextLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//				editTextLayout.setOrientation(LinearLayout.VERTICAL);
//				editTextLayout.setPadding(Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context));
//				editTextLayout.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//
//				parentLayout.addView(editTextLayout);
//
//				final EditText editDetectorId = new EditText(context);
//				editDetectorId.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//				editDetectorId.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//				editDetectorId.setTextColor(context.getResources().getColor(R.color.md_grey_700));
//				editDetectorId.setInputType(InputType.TYPE_CLASS_NUMBER);
//				editDetectorId.setGravity(Gravity.CENTER);
//				try
//				{
//					Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
//					f.setAccessible(true);
//					f.set(editDetectorId, R.drawable.caa_cursor);
//				} catch (Exception ignored)
//				{
//				}
//
//				editTextLayout.addView(editDetectorId);
//
//				//HIDE TEXT AND EDIT IN START
//				messText.setVisibility(View.GONE);
//				editDetectorId.setVisibility(View.GONE);
//
//				radioAuto.setOnClickListener(new View.OnClickListener()
//				{
//					@Override
//					public void onClick(View v)
//					{
//						radioAuto.setChecked(true);
//						radioManual.setChecked(false);
//						messText.setVisibility(View.GONE);
//						//						InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
//						//						View view = activity.getCurrentFocus();
//						//						if (null == view)
//						//						{
//						//							view = new View(activity);
//						//						}
//						//						imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//						//						imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.RESULT_HIDDEN);
//						Func.hideKeyboard(activity);
//						editDetectorId.setVisibility(View.GONE);
//					}
//				});
//				radioManual.setOnClickListener(new View.OnClickListener()
//				{
//					@Override
//					public void onClick(View v)
//					{
//						radioAuto.setChecked(false);
//						radioManual.setChecked(true);
//						messText.setVisibility(View.VISIBLE);
//						editDetectorId.setVisibility(View.VISIBLE);
//						editDetectorId.requestFocus();
//					}
//				});
//
//				LinearLayout addDetecotrBLayout = new LinearLayout(context);
//				addDetecotrBLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//				addDetecotrBLayout.setOrientation(LinearLayout.HORIZONTAL);
//				addDetecotrBLayout.setBackgroundColor(context.getResources().getColor(R.color.md_white_1000));
//
//				final Button buttonOk = new Button(context);
//				buttonOk.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
//				buttonOk.setBackgroundResource(R.drawable.rectangle_white);
//				buttonOk.setText(context.getString(R.string.ADD_DETECTOR_DIALOG_ADD));
//				buttonOk.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//				buttonOk.setTypeface(null, Typeface.BOLD);
//				buttonOk.setOnTouchListener(new View.OnTouchListener()
//				{
//					@Override
//					public boolean onTouch(View v, MotionEvent event)
//					{
//						switch (event.getAction())
//						{
//							case MotionEvent.ACTION_DOWN:
//								buttonOk.setTextColor(context.getResources().getColor(R.color.brandColorDark));
//								break;
//							case MotionEvent.ACTION_UP:
//								buttonOk.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//								break;
//						}
//						return false;
//					}
//				});
//				buttonOk.setOnClickListener(new View.OnClickListener()
//				{
//					@Override
//					public void onClick(View v)
//					{
//						LinkedList<Affect> affects = dbHelper.getAffectsBySiteId(site);
////						if (!dbHelper.getSiteArmStatus(site))
////						{
//							if (radioAuto.isChecked())
//							{
//
//								UserInfo userInfo = dbHelper.getUserInfo();
//								myService = getService();
//								myService.addInNormalQueue(new D3AbsQueueElement(D3AbsQueueElement.REQ_TYPE.JSON, "{\"S\":\"" + userInfo.getSession() + "\",\"Q\":\"COMMAND\",\"D\":{\"command\":{\"REGISTER\":" + 0 + "},\"site\":" + site + "}}")
//								{
//									@Override
//									public void receiver(String data)
//									{
//										if (null != data)
//										{
//											Log.d("D3ELEMENT", data);
//										} else
//										{
//											Log.d("D3ELEMENT", "RECEIVED NULL");
//										}
//									}
//								});
//								Func.hideKeyboard(activity);
//								editDetectorId.clearFocus();
//								addDevDialog.dismiss();
//								Func.nShowMessage(context, context.getString(R.string.EA_NEW_DEVICE_ADD_MESS_DEVICE_WAITING_MODE));
//
//							} else
//							{
//								if (radioManual.isChecked())
//								{
//									if (!editDetectorId.getText().toString().matches(""))
//									{
//										int num = Integer.parseInt(editDetectorId.getText().toString());
//										if ((num > 32) || (num < 1))
//										{
//											Func.pushToast(context, context.getString(R.string.EA_INCORRECT_DETECTOR_NUMBER), activity);
//											editDetectorId.setText("");
//										} else
//										{
//											UserInfo userInfo = dbHelper.getUserInfo();
//											myService = getService();
//											myService.addInNormalQueue(new D3AbsQueueElement(D3AbsQueueElement.REQ_TYPE.JSON, "{\"S\":\"" + userInfo.getSession() + "\",\"Q\":\"COMMAND\",\"D\":{\"command\":{\"REGISTER\":" + (num) + "},\"site\":" + site + "}}")
//											{
//												@Override
//												public void receiver(String data)
//												{
//													if (null != data)
//													{
//														Log.d("D3ELEMENT", data);
//													} else
//													{
//														Log.d("D3ELEMENT", "RECEIVED NULL");
//													}
//												}
//											});
//											Func.hideKeyboard(activity);
//											editDetectorId.clearFocus();
//											addDevDialog.dismiss();
//											Func.nShowMessage(context, context.getString(R.string.EA_NEW_DEVICE_ADD_MESS_DEVICE_WAITING_MODE));
//										}
//									} else
//									{
//										Func.pushToast(context, context.getString(R.string.EA_DETECTOR_ADD_DIALOG_ENTER_NUMBER), activity);
//									}
//								}
//							}
////						} else
////						{
////							Func.hideKeyboard(activity);
////							editDetectorId.clearFocus();
////							addDevDialog.dismiss();
////							Func.nShowMessage(context, context.getString(R.string.EA_OBJECT_ARMED_MESS_WHEN_USER_ADD_NEW_DETECTOR));
////						}
//					}
//				});
//
//				addDetecotrBLayout.addView(buttonOk);
//
//				final Button buttonCancel = new Button(context);
//				buttonCancel.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f));
//				buttonCancel.setBackgroundResource(R.drawable.rectangle_white);
//				buttonCancel.setText(context.getString(R.string.ADD_DETECTOR_DIALOG_CANCEL));
//				buttonCancel.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//				buttonCancel.setTypeface(null, Typeface.BOLD);
//				buttonCancel.setOnTouchListener(new View.OnTouchListener()
//				{
//					@Override
//					public boolean onTouch(View v, MotionEvent event)
//					{
//						switch (event.getAction())
//						{
//							case MotionEvent.ACTION_DOWN:
//								buttonCancel.setTextColor(context.getResources().getColor(R.color.brandColorDark));
//								break;
//							case MotionEvent.ACTION_UP:
//								buttonCancel.setTextColor(context.getResources().getColor(R.color.md_black_1000));
//								break;
//						}
//						return false;
//					}
//				});
//				buttonCancel.setOnClickListener(new View.OnClickListener()
//				{
//					@Override
//					public void onClick(View v)
//					{
//						Func.hideKeyboard(activity);
//						editDetectorId.clearFocus();
//						addDevDialog.dismiss();
//					}
//				});
//
//				addDetecotrBLayout.addView(buttonCancel);
//				parentLayout.addView(addDetecotrBLayout);
//
//				addDevDialog.setView(parentLayout);
//
//				addDevDialog.show();
//			}else{
//				Func.nShowMessage(context, context.getString(R.string.EA_OBJECT_ARMED_MESS_WHEN_USER_ADD_NEW_DETECTOR));
//			}
//		}
//	}
//
//}
