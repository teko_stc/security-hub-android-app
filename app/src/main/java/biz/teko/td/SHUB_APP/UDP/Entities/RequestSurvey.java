package biz.teko.td.SHUB_APP.UDP.Entities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class RequestSurvey extends RequestBasePincoded {
    public final static byte _command = 0;

    public RequestSurvey() {
        this.magic = new byte[]{0x73, 0x11, 0x7a, 0x6e};
        this.command = _command;
    }

    public byte[] getBytes() {
        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            outStream.write(magic);
            outStream.write(command);
            return outStream.toByteArray();
        } catch (IOException e) { e.printStackTrace(); }
        return null;
    }
}
