package biz.teko.td.SHUB_APP.Codes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.TypedValue;
import android.view.View;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Codes.Activities.NLocalUserSettingsActivity;
import biz.teko.td.SHUB_APP.Codes.Activities.NLocalUsersActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.User;
import biz.teko.td.SHUB_APP.R;

public class NLocalUsersListAdapter extends ResourceCursorAdapter {
    private final DBHelper dbHelper;
    private final int device_id;
    private Context context;
    @Nullable
    private String deviceSerial;
    private String deviceModel;

    public NLocalUsersListAdapter(Context context, int layout, Cursor c, int flags, int device_id) {
        super(context, layout, c, flags);
        this.context = context;
        this.dbHelper = DBHelper.getInstance(context);
        this.device_id = device_id; // device id переданный из того листа, из которого был открыт список пользователей
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        User user = new User(cursor);

        if (-1 == device_id) {
            Device device = dbHelper.getDeviceById(user.device);
            if (null != device) {
                deviceSerial = Integer.toString(device.account);
                deviceModel = device.getName();
            }
            setTextViewText(view, context, R.id.userDestination, 8, deviceModel + "(" + deviceSerial + ")");
        } else {
            view.findViewById(R.id.userDestination).setVisibility(View.GONE);
        }

        setTextViewText(view, context, R.id.userName, 18, user.name);
        setTextViewText(view, context, R.id.userId, 14, context.getString(R.string.USERS_USER_NUM) + " " + user.id);

        if (!user.sections.equals(""))
        {
            String partS = "";
            String[] secIds = user.sections.substring(1, user.sections.length() - 1).split(", ");
            for(String secId : secIds){
                Section section = dbHelper.getDeviceSectionById(user.device, Integer.valueOf(secId));
                if(null!=section) partS += section.name + ", ";
            }
            if(!partS.equals(""))setTextViewText(view, context, R.id.userSections, 14, context.getString(R.string.USERS_SECTIONS_TITLE) + " " + partS.substring(0, partS.length()-2));

        }


        view.findViewById(R.id.userItemLayout).setOnClickListener(v -> startSettingActivity(device_id, user.id));
    }

    private void startSettingActivity(int deviceId, int userId) {
        Intent intent = new Intent(context, NLocalUserSettingsActivity.class);
        intent.putExtra("userId", userId);
        intent.putExtra("deviceId", deviceId);
        context.startActivity(intent);
        ((NLocalUsersActivity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
    }

    private void setTextViewText(View view, Context context, int id, int size, String text) {
        TextView textView = (TextView) view.findViewById(id);
        textView.setText(text);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
    }

    public void update(Cursor cursor) {
        this.changeCursor(cursor);
    }

//	private Section[] getSections(){
//		return sections;
//	}

//	private void setSections(Section[] sections){
//		this.sections  = sections;
//	}
}
