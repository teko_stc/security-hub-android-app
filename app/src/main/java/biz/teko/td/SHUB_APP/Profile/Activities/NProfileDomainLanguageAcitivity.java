package biz.teko.td.SHUB_APP.Profile.Activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.LocaleD3;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Profile.Adapters.LocaleListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NProfileDomainLanguageAcitivity extends BaseActivity
{
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private Context context;
	private DBHelper dbHelper;
	private int sending = 0;
	private LocaleListAdapter languageListAdapter;
	private ListView languageList;

	private BroadcastReceiver rolesUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};
	private LinearLayout close;
	private TextView title;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_domain_language);
		context = this;
		dbHelper = DBHelper.getInstance(context);

		setup();
		bind();

	}

	private void bind()
	{
		if(null!=close) close.setOnClickListener(v -> {
			onBackPressed();
		});
		if(null!=title) title.setText(getResources().getString(R.string.DOMAIN_LANG_TITLE));
		updateList();

	}

	private void setup()
	{
		close  = (LinearLayout) findViewById(R.id.nButtonBack);
		title = (TextView) findViewById(R.id.nTextTitle);
		languageList = (ListView) findViewById(R.id.nLanguageList);
	}


	private void updateList()
	{
		if(null!=languageListAdapter){
			languageListAdapter.clear();
			LocaleD3[] localeD3s = dbHelper.getLocales();
			UserInfo userInfo = dbHelper.getUserInfo();
			languageListAdapter.addAll(localeD3s);
			languageListAdapter.setCurLang(userInfo.locale);
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					languageListAdapter.notifyDataSetChanged();
				}
			});
		}else{
			LocaleD3[] localeD3s = dbHelper.getLocales();
			languageListAdapter = new LocaleListAdapter(context, R.layout.n_lang_list_element, localeD3s, dbHelper.getUserInfo().locale);
			languageListAdapter.setOnElementClickListener(language -> {
				UserInfo userInfo = dbHelper.getUserInfo();
				if(!userInfo.locale.equals(language.iso)){
					JSONObject jsonObject = new JSONObject();
					try
					{
						jsonObject.put("locale_id", language.id);
						nSendMessageToServer(null, D3Request.SET_LOCALE, jsonObject, false);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}

				}
			});
			languageList.setAdapter(languageListAdapter);
		}
	}


	@Override
	protected void onResume()
	{
		super.onResume();
		bindD3();
		registerReceiver(rolesUpdateReceiver, new IntentFilter(D3Service.BROADCAST_ROLES_GET));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(rolesUpdateReceiver);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

}
