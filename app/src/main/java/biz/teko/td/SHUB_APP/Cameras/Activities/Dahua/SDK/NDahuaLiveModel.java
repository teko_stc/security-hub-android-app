package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.view.SurfaceView;
import android.view.View;

import com.company.NetSDK.CB_fRealDataCallBackEx;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.SDK_RealPlayType;
import com.company.PlaySDK.IPlaySDK;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class NDahuaLiveModel
{
	private static final String TAG = NDahuaLiveModel.class.getSimpleName();
	private final int STREAM_BUF_SIZE = 1024*1024*2;
	private final int RAW_AUDIO_VIDEO_MIX_DATA = 0; ///原始音视频混合数据;  ///Raw audio and video mixing data.
	private final long loginHandle;
	private Camera camera;
	long mRealHandle = 0;
	private Context context;
	Resources res;
	int mPlayPort = 0;
	int mCurVolume = -1;
	boolean isRecording = false;
	boolean isOpenSound = true;
	boolean isDelayPlay = false;
	Map<Integer,Integer> streamTypeMap = new HashMap<Integer,Integer>();

	/// for preview date callback
	private CB_fRealDataCallBackEx mRealDataCallBackEx;

	public NDahuaLiveModel(Context context, Camera camera, long loginHandle){
		this.context = context;
		this.camera = camera;
		this.loginHandle = loginHandle;

		mPlayPort = IPlaySDK.PLAYGetFreePort();

		isOpenSound = true;
		isDelayPlay = false;
		initMap();
	}

	private void initMap(){
		streamTypeMap.put(0, SDK_RealPlayType.SDK_RType_Realplay_0);
		streamTypeMap.put(1,SDK_RealPlayType.SDK_RType_Realplay_1);
	}

	public boolean prePlay(SurfaceView sv){
		boolean isOpened = IPlaySDK.PLAYOpenStream(mPlayPort, null, 0, STREAM_BUF_SIZE) != 0;
		if(!isOpened) {
			Func.log_d(Const.LOG_TAG_DAHUA, "OpenStream Failed");
			return false;
		}
		boolean isPlayin = IPlaySDK.PLAYPlay(mPlayPort, sv) != 0;
		if (!isPlayin) {
			Func.log_d(Const.LOG_TAG_DAHUA, "PLAYPlay Failed");
			IPlaySDK.PLAYCloseStream(mPlayPort);
			return false;
		}

		if (isOpenSound) {
			boolean isSuccess = IPlaySDK.PLAYPlaySoundShare(mPlayPort) != 0;
			if (!isSuccess) {
				Func.log_d(Const.LOG_TAG_DAHUA, "SoundShare Failed");
				IPlaySDK.PLAYStop(mPlayPort);
				IPlaySDK.PLAYCloseStream(mPlayPort);
				return false;
			}
			if (-1 == mCurVolume) {
				mCurVolume = IPlaySDK.PLAYGetVolume(mPlayPort);
			} else {
				IPlaySDK.PLAYSetVolume(mPlayPort, mCurVolume);
			}
		}

		if (isDelayPlay) {
			if (IPlaySDK.PLAYSetDelayTime(mPlayPort, 500/*ms*/, 1000/*ms*/) == 0) {
				Func.log_d(Const.LOG_TAG_DAHUA, "SetDelayTime Failed");
			}
		}

		return true;
	}

	public long getHandle(){
		return this.mRealHandle;
	}

	public boolean isRealPlaying() {
		return mRealHandle != 0;
	}

	public int getPlayPort(){
		return this.mPlayPort;
	}

	public void setOpenSound(boolean isOpenSound) {
		this.isOpenSound = isOpenSound;
	}

	public void setDelayPlay(boolean isDelayPlay) {
		this.isDelayPlay = isDelayPlay;
	}

	public void startPlay(int channel,int streamType,final SurfaceView view){
		Func.log_d(Const.LOG_TAG_DAHUA, "StreamType: "+ streamTypeMap.get(streamType));
		mRealHandle = INetSDK.RealPlayEx(loginHandle, channel, streamTypeMap.get(streamType));
		if (mRealHandle == 0){
			Func.log_d(Const.LOG_TAG_DAHUA, "RealPlayEx failed!");
			return;
		}

		if (!prePlay( view)) {
			Func.log_d(Const.LOG_TAG_DAHUA, "prePlay returned false..");
			INetSDK.StopRealPlayEx(mRealHandle);
			return;
		}
		if (mRealHandle!=0){
			mRealDataCallBackEx = (rHandle, dataType, buffer, bufSize, param) -> {
//				Func.log_d(Const.LOG_TAG_DAHUA, "dataType:"+dataType+"; bufSize:"+bufSize+"; param:"+param);
				new Handler(Looper.getMainLooper()).post(() -> {
					view.setVisibility(View.VISIBLE);
//					Func.log_d(Const.LOG_TAG_DAHUA, "start play live video in module " + " " + this.camera.sn);
				});
				if (RAW_AUDIO_VIDEO_MIX_DATA == dataType){
//					Func.log_d(Const.LOG_TAG_DAHUA, "dataType == 0");
					IPlaySDK.PLAYInputData(mPlayPort,buffer,buffer.length);
				}
			};
			INetSDK.SetRealDataCallBackEx(mRealHandle, mRealDataCallBackEx, 1);
		}
	}

	public void stopRealPlay(){
		if(mRealHandle == 0) {
			return;
		}

		try{
			IPlaySDK.PLAYStop(mPlayPort);
			if (isOpenSound) {
				IPlaySDK.PLAYStopSoundShare(mPlayPort);
			}
			IPlaySDK.PLAYCloseStream(mPlayPort);
			INetSDK.StopRealPlayEx(mRealHandle);
			IPlaySDK.PLAYRefreshPlay(mPlayPort);
			if (isRecording) {
				INetSDK.StopSaveRealData(mRealHandle);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		mRealHandle = 0;
		isRecording = false;
		Func.log_d(Const.LOG_TAG_DAHUA, "Real play stopped");
	}
	public void initSurfaceView(final SurfaceView sv){
		if (sv == null)
			return;
		IPlaySDK.InitSurface(mPlayPort,sv);
	}

	public boolean record(boolean recordFlag){
		if (mRealHandle == 0) {
			Func.log_d(Const.LOG_TAG_DAHUA, "Not open");
			Func.pushToast(context, context.getResources().getString(R.string.N_DAHUA_RECORD_ERROR));
			return false;
		}

//		Func.log_d(Const.LOG_TAG_DAHUA, "ExternalFilesDir:" + Func.getMediaFilesDirectory(context));
		isRecording = recordFlag;
		if(isRecording){
			/*try new files directory*/
			String recordFile = createInnerAppFile("dav", true);
			String dir = Func.getMediaFilesDirectory(context);
			Func.log_d(Const.LOG_TAG_DAHUA, "record file:"+recordFile);
			if (!INetSDK.SaveRealData(mRealHandle, recordFile)){
				/*try old files directory*/
				recordFile = createInnerAppFile("dav", false);
				dir = Func.getMediaFilesOldDirectory(context);
				Func.log_d(Const.LOG_TAG_DAHUA, "record file:"+recordFile);
				if (!INetSDK.SaveRealData(mRealHandle, recordFile))
				{
					Func.log_d(Const.LOG_TAG_DAHUA, "record failed");
					Func.pushToast(context, context.getResources().getString(R.string.N_DAHUA_RECORD_CANT_START));
					//				INetSDK.StopSaveRealData(mRealHandle);
					return false;
				}else{
					PrefUtils.getInstance(context).setMediaFilesDirectory(dir);
				}
			}else{
				PrefUtils.getInstance(context).setMediaFilesDirectory(dir);
			}
		}else {
			INetSDK.StopSaveRealData(mRealHandle);
			Func.pushToast(context, context.getResources().getString(R.string.N_DAHUA_SAVE_REC));
		}
		return true;
	}

	public synchronized String createInnerAppFile(String suffix, boolean new_type){
		File imagesDir = new File((new_type ? Func.getMediaFilesDirectory(context) : Func.getMediaFilesOldDirectory(context))+"/"+ camera.sn);
		if(!imagesDir.exists())
			imagesDir.mkdir();

		File f = new File(imagesDir, getFileName(suffix));


		return f.getAbsolutePath();
	}

	private String getFileName(String suffix)
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = format.format(new Date());
		return  time.replace(":","-").replace(" ", "_") +
				"." + suffix;
	}
}