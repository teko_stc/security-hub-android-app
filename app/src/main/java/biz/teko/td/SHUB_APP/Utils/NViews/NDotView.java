package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import biz.teko.td.SHUB_APP.R;

public class NDotView extends LinearLayout
{
	public static  final int[] STATE_FILLED = {R.attr.state_filled};

	private boolean filled;

	private final Context context;
	private final int layout;

	public NDotView(Context context, int layout)
	{
		super(context);
		this.context = context;
		this.layout = layout;

		setup();
	}

	private void setup()
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(layout, this, true);
	}


	public boolean isFilled()
	{
		return filled;
	}

	public void setFilled(boolean filled)
	{
		this.filled = filled;
		refreshDrawableState();
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		if(filled) mergeDrawableStates(drawableState, STATE_FILLED);
		return drawableState;
	}
}
