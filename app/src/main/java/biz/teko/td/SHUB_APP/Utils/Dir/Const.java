package biz.teko.td.SHUB_APP.Utils.Dir;

import java.util.Collection;

public class Const
{

	public static  final  String LOCALCONFIG_LOG_TAG = "D3LocalConfig";
	public static  final  String LOG_TAG = "D3";
	public static  final  String LOG_TAG_DAHUA = "D3DAHUA";

	public static final Collection<String> BARCODE_CODES =
			Func.list("ITF", "QR", "QR_CODE");

	public static final long SECTION_ZONE_TO_DROP_ALARM_TIME = 600;

	public static final int AUTH_METHOD_FINGER = 0;
	public static final int AUTH_METHOD_PIN = 1;


	public static final class EXTRAS {
		public static final String IMSI1 = "IMSI1";
		public static final String TEMP_THRESHOLDS = "temp_thresholds";
	}

	public static class Roles{
		public static final int SERVER_ADMIN  = 1;
		public static final int ORG_ADMIN  = 16;
		public static final int ORG_BKS_OPER  = 32;
		public static final int ORG_LAWYER = 64;
		public static final int ORG_BUH  = 128;
		public static final int ORG_ENGIN = 256;
		public static final int UNKNOWN_2  = 512;
		public static final int UNKNOWN_3  = 1024;
		public static final int DOMAIN_ADMIN = 2048;
		public static final int DOMEN_DO  = 4096;
		public static final int DOMAIN_OPER = 8192;
		public static final int DOMAIN_ENGIN = 16384;
		public static final int DOMAIN_HOZ_ORG = 32768;
		public static final int DOMAIN_LAWYER = 65536;
		public static final int ROLE_RZ  = 131072;
		public static final int POLICE_SUPERVISOR  = 262144;
		public static final int TRINKET  = 524288;
	}

	public  static final int SITE = 0;
	public  static final int DEVICE = 1;
	public  static final int SECTION = 2;
	public  static final int ZONE = 3;
	public  static final int USER = 4;
	public  static final int CAMERA_RTSP = 5;
	public  static final int LOGIN = 6;
	public  static final int CAMERA_IV = 7;
	public  static final int LOCAL_USER = 8;
	public  static final int LOCAL_CONFIG = 9;
	public  static final int CAMERA_DAHUA = 10;

	public static final String TITLE_TRANSITION_NAME = "temp:transition:title";
	public static final String IMAGE_TRANSITION_NAME = "temp:transition:image";

	public static final int REQUEST_DELETE = 5;
	public static final int REQUEST_IV_LOGIN = 6;
	public static final int REQUEST_TURN_AM = 7;
	public static final int REQUEST_SHOW_LIST_ELEMENT = 8;
	public static final int REQUEST_CONTROL_AM = 9;
	public static final int REQUEST_SET_CONFIG = 10;
	public static final int REQUEST_RESET_CONFIG = 11;

	public static final int ALL = -1;

	public static final int STATUS_NO_ARM_CONTROL = -1;
	public static final int STATUS_DISARMED = -2;
	public static final int STATUS_ARMED = -3;
	public static final int STATUS_PARTLY_ARMED = -4;
	public static final int STATUS_NO_CONTROL = -5;
	public static final int STATUS_ON = -6;
	public static final int STATUS_OFF = -7;
	public static final int STATUS_NO_SECTION_CONTROL = -8;
	public static final int STATUS_ONLINE = -9;
	public static final int STATUS_OFFLINE = -10;

	public static final int CLASS_ALARM = 0;
	public static final int CLASS_SABOTAGE = 1;
	public static final int CLASS_MALFUNCTION = 2;
	public static final int CLASS_ATTENTION = 3;
	public static final int CLASS_ARM = 4;
	public static final int CLASS_CONTROL = 5;
	public static final int CLASS_SUPPLY = 6;
	public static final int CLASS_CONNECTIVITY = 7;
	public static final int CLASS_INFORMATION = 8;
	public static final int CLASS_DEBUG = 9;
	public static final int CLASS_TELEMETRY = 10;
	public static final int CLASS_GEOLOCATION = 11;
	public static final int CLASS_INNER = 15;


	public static final int REASON_TURN_ON = 6;

	public static final int REASON_MALF_LOST_CONNECTION = 3;
	public static final int REASON_MAFL_BATT_NO = 12;
	public static final int REASON_MAFL_BATT_LOW = 50;

	public static final int REASON_BATTERY_NORESERVE = 1;
	public static final int REASON_BATTERY_SUPPLY = 2;
	public static final int REASON_SOCKET_FAULT = 3;
	public static final int REASON_BATTERY_MAINSUNDER = 4;
	public static final int REASON_BATTERY_MAINOVER = 5;
	public static final int REASON_BATTERY_RESERVE = 6;
	public static final int REASON_BATTERY_BATTLOW = 7;
	public static final int REASON_BATTERY_RESBATTLOW = 8;
	public static final int REASON_BATTERY_LEVEL = 9;
	public static final int REASON_BATTERY_RESBATTLEVEL = 10;
	public static final int REASON_BATTERY_ERROR = 11;
	public static final int REASON_BATTERY_RESBATTERROR = 12;

	public static final int REASON_CONNECTIVITY_UNAVAILABLE = 1;
	public static final int REASON_CONNECTIVITY_NOCARRIER = 2;
	public static final int REASON_CONNECTIVITY_BUSY = 3;
	public static final int REASON_CONNECTIVITY_NOANSWER = 4;
	public static final int REASON_CONNECTIVITY_LEVEL = 5;
	public static final int REASON_CONNECTIVITY_IMSI = 6;
	public static final int REASON_CONNECTIVITY_SMS = 7;
	public static final int REASON_CONNECTIVITY_USSD = 8;
	public static final int REASON_CONNECTIVITY_BALANCE = 9;
	public static final int REASON_CONNECTIVITY_JAM = 10;

	public static final int REASON_TEMPERATURE = 4;


	public static final int REASON_INFORMATION_ARM_DELAY = 6;
	public static final int REASON_INFORMATION_UPDATE_READY = 7;

	public static final int SECTIONTYPE_GUARD = 1;
	public static final int SECTIONTYPE_ALARM = 2;
	public static final int SECTIONTYPE_FIRE = 3;
	public static final int SECTIONTYPE_FIRE_DOUBLE = 4;
	public static final int SECTIONTYPE_TECH = 5;
	public static final int SECTIONTYPE_TECH_CONTROL = 6;

	public static final int DETECTOR_FIRE = 1;
	public static final int DETECTOR_CONTROLLER = 8;
	public static final int DETECTOR_KEYPAD = 11;
	public static final int DETECTOR_AUTO_RELAY = 13;
	public static final int DETECTOR_SIREN = 14;
	public static final int DETECTOR_LAMP = 15;
	public static final int DETECTOR_KEYPAD_WITH_DISPLAY = 16;
	public static final int DETECTOR_MODEM = 21;
	public static final int DETECTOR_ETHERNET = 22;
	public static final int DETECTOR_MANUAL_RELAY = 23;
	public static final int DETECTOR_SZO = 25;
	public static final int DETECTOR_BIK = 26;
	public static final int DETECTOR_THERMOSTAT = 27;
	public static final int DETECTOR_GPRS_1 = 28;
	public static final int DETECTOR_GRPS_2 = 29;


	public static final int SENSORTYPE_UNKNOWN = 0;
	public static final int SENSORTYPE_WIRED_OUTPUT = 55; // сменил, чтобы не путать с РК
	public static final int SENSORTYPE_WIRED_INPUT = 44;	// --

	public static final int ZONETYPE_3731 = 5;
	public static final int ZONETYPE_8131 = 8131;
	public static final int ZONETYPE_8121 = 8121;
	public static final int ZONETYPE_8731 = 8731;
	public static final int ZONETYPE_4511 = 4511;
	public static final int ZONETYPE_421_RK2 = 421;
	public static final int ZONETYPE_431_RK2 = 431;

	public static final int DEVICETYPE_COMMON = 0;
	public static final int DEVICETYPE_SH = 1;
	public static final int DEVICETYPE_SH_1 = 2;
	public static final int DEVICETYPE_SH_1_1 = 3;
	public static final int DEVICETYPE_SH_2 = 4;
	public static final int DEVICETYPE_SH_4G = 5;
	public static final int DEVICETYPE_MB = 9;

	public static final int WIREDTYPE_GUARD = 1;
	public static final int WIREDTYPE_FIRE = 2;
	public static final int WIREDTYPE_FIRE_DOUBLE = 3;
	public static final int WIREDTYPE_BIK = 4;


	public static final int IV_ERROR_NOT_AUTHORIZED = 401;
	public static final int IV_CAMERA_GET_SUCCESS = 200;

	public static final int ALARM_STATE_CLOSED_WITH_GBR = 21;
	public static final int ALARM_OPEN_INTENT = 1;
	public static final int ALARM_CLOSE_INTENT = 0;

	public static final int WIRELESS = 0;
	public static final int WIRED = 1;

	public static  final int STATE_TYPE_SUPPLY = 0;
	public static  final int STATE_TYPE_LEVEL = 1;
	public static  final int STATE_TYPE_TEMP = 2;
	public static  final int STATE_TYPE_CONNECTION = 3;
	public static  final int STATE_TYPE_DELAY = 4;
	public static  final int STATE_TYPE_DELAY_ARM = 5;
	public static  final int STATE_TYPE_UPDATE = 6;

	public static final int NONE = 0;
	public static final int FULL = 1;
	public static final int PART = 2;
	public static final int BOTH = 3;
	public static final int CONTROLLER = 4;

	public static final int ACTION_RMV = 1;

	public static final int NOTIF_ALARM_TYPE_NONE = 0;
	public static final int NOTIF_ALARM_TYPE_INSTANT = 1;
	public static final int NOTIF_ALARM_TYPE_REPEAT = 2;

	public static final String OPEN_IV_KEY_FROM = "from";
	public static final int OPEN_IV_FROM_SETTINGS = 1;
	public static final int OPEN_IV_FROM_CAMERA = 0;

	public static final int ADD_DOMAIN_USER = 110;
	public static final int ADD_LOCAL_USER = 111;

	public static final String IS_FIRST_TUTORIAL = "isFirstTutorial";

	public static final String SCREEN_NAME = "screen name";
}
