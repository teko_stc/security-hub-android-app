package biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces;

public interface IValueOperation {
    String getTextDialogValue(String value);
}
