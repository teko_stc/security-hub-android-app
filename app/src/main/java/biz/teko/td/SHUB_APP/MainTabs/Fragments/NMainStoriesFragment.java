package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentPagerAdapter;
import biz.teko.td.SHUB_APP.MainTabs.Adapters.NStoryPagerAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NStoriesView;
import biz.teko.td.SHUB_APP.Utils.Other.NStoryViewPager;

@SuppressLint("ClickableViewAccessibility")
public class NMainStoriesFragment extends DialogFragment implements NStoriesView.StoriesListener {
    private final long PRESSED_LIMIT = 200;
    private final long SWIPE_LIMIT = 150;
    private List<Integer> whiteCounts = new ArrayList<>();
    private long pressTime = 0;
    private int currentFragment = 0;
    private int downX, upX;
    private NStoryViewPager viewPager;
    private NStoryPagerAdapter adapter;
    private NStoriesView storiesView;
    private ImageView closeStories;
    private Listener listener;

    private final OnBackPressedCallback backCallback = new OnBackPressedCallback(true) {
        @Override
        public void handleOnBackPressed() {
            closeAllStories();
        }
    };

    public interface Listener {
        void onComplete();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (storiesView != null) {
            storiesView.resume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (storiesView != null) {
            storiesView.pause();
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private View.OnTouchListener touchListener = (view, event) -> {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            pressTime = System.currentTimeMillis();
            downX = (int) event.getX();
            storiesView.pause();
            return false;
        }
        else if (event.getAction() == MotionEvent.ACTION_UP) {
            long now = System.currentTimeMillis();
            upX = (int) event.getX();
            float deltaX = downX - upX;
            if (Math.abs(deltaX) > SWIPE_LIMIT) {
                return swipeNavigation(deltaX);
            } else {
                storiesView.resume();
                return PRESSED_LIMIT < now - pressTime;
            }
        }
        return false;
    };

    private boolean swipeNavigation(float deltaX) {
        if (deltaX < SWIPE_LIMIT) {
            storiesView.reverse();
        } else if (deltaX > SWIPE_LIMIT) {
            storiesView.skip();
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.n_fragment_main_stories, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View view) {
        getActivity().getOnBackPressedDispatcher().addCallback(backCallback);
        setupCloseStories(view);
        setupSkipReverse(view);
        setupViewPager(view);
        setupStoriesView(view);
        setupWhiteCounts();
    }

    private void setupCloseStories(View view) {
        closeStories = view.findViewById(R.id.closeStoriesImage);
        closeStories.setOnClickListener(v -> {
            closeAllStories();
        });
    }

    private void setupSkipReverse(View view) {
        View reverse = view.findViewById(R.id.reverse);
        View skip = view.findViewById(R.id.skip);
        reverse.setOnClickListener(v -> pressedLeft());
        reverse.setOnTouchListener(touchListener);
        skip.setOnClickListener(v -> pressedRight());
        skip.setOnTouchListener(touchListener);
    }

    private void setupStoriesView(View view) {
        storiesView = view.findViewById(R.id.stories);
        storiesView.setStoriesCount(adapter.getCount());
        storiesView.setStoryDuration(5000);
        storiesView.setStoriesListener(this);
        storiesView.playStories();
    }

    private void pressedRight() {
        storiesView.skip();
    }

    private void pressedLeft() {
        storiesView.reverse();
    }

    private void setPagerItem(int position) {
        viewPager.setCurrentItem(position, true);
        changeStoryColor(position);
        changeCloseColor(position);
    }

    private void changeCloseColor(int position) {
        if (isWhiteStory(position))
            closeStories.setImageResource(R.drawable.ic_close_story_black);
        else
            closeStories.setImageResource(R.drawable.ic_close_story_white);
    }

    private void changeStoryColor(int position) {
        if (isWhiteStory(position))
            storiesView.setWhiteStory();
        else
            storiesView.setBlueStory();
    }

    private boolean isWhiteStory(int position) {
        return whiteCounts.contains(position);
    }

    private void setupWhiteCounts() {
        whiteCounts.addAll(Arrays.asList(0, 2, 4 , 8));
    }

    private void setupViewPager(View view) {
        viewPager = view.findViewById(R.id.storiesPager);
        adapter = new NStoryPagerAdapter(getChildFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        viewPager.setScrollDurationFactor(3);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onNext() {
        currentFragment++;
        if (currentFragment >= adapter.getCount() - 1)
            currentFragment = adapter.getCount() - 1;
        setPagerItem(currentFragment);
    }

    @Override
    public void OnPrevious() {
        currentFragment--;
        if (currentFragment <= 0)
            currentFragment = 0;
        setPagerItem(currentFragment);
    }


    @Override
    public void onComplete() {
        listener.onComplete();
    }


    private void closeAllStories() {
        listener.onComplete();
    }

    @Override
    public void onDestroy() {
        storiesView.destroy();
        super.onDestroy();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, R.style.DialogFragmentTheme);

    }

}
