package biz.teko.td.SHUB_APP.FaqTab;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;

import java.util.LinkedList;

import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.FaqTab.Adapters.FaqRecyclerViewAdapter;
import biz.teko.td.SHUB_APP.FaqTab.Faq.FChapter;
import biz.teko.td.SHUB_APP.FaqTab.Faq.FQuestion;
import biz.teko.td.SHUB_APP.FaqTab.RecycleGroup.FaqGroup;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 25.05.2016.
 */
public class FaqActivity extends BaseActivity
{
	private DrawerBuilder drawerBuilder;
	private DBHelper dbHelper;
	private final  static int DEF_POSITION = 7;

	private D3Service myService;
	private Context context;
	private Drawer drawerResult;


	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity_faq);
		getSwipeBackLayout().setEnableGesture(false);


		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.drawer_item_help);
		setSupportActionBar(toolbar);
//		drawerResult = navigationDrawer(context, toolbar, getIntent().getIntExtra("position", DEF_POSITION)).build();


		RecyclerView recyclerView = (RecyclerView) findViewById(R.id.faqRecycler);
		recyclerView.setLayoutManager(new LinearLayoutManager(this));
//		if(D3Service.faq != null){
//			LinkedList<FChapter> fChapters = D3Service.faq.fChapters;
//			LinkedList<FaqGroup>  faqGroups = new LinkedList<>();
//			for(FChapter fChapter:fChapters){
//				LinkedList<FPQuestion> fpQuestions = new LinkedList<>();
//				for(FQuestion fQuestion:fChapter.fQuestions)
//				{
//					FPQuestion fpQuestion = new FPQuestion(fQuestion);
//					fpQuestions.add(fpQuestion);
//				}
//				faqGroups.add(new FaqGroup(fChapter, fpQuestions));
//			}
//			FaqRecyclerViewAdapter faqRecyclerViewAdapter = new FaqRecyclerViewAdapter(faqGroups);
//			recyclerView.setAdapter(faqRecyclerViewAdapter);
//		}


	}

	@Override
	public void onBackPressed()
	{
		if ((drawerResult != null) && (drawerResult.isDrawerOpen()))
		{
			drawerResult.closeDrawer();
		}else{
			AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
			adb.setMessage(R.string.EXIT_FROM_APP_MESSAGE);
			adb.setPositiveButton(getResources().getString(R.string.YES), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialogInterface, int i)
				{
					finishAffinity();
				}
			});
			adb.setNegativeButton(getResources().getString(R.string.NO), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialogInterface, int i)
				{
					dialogInterface.dismiss();
				}
			});
			adb.show();
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
	}
}
