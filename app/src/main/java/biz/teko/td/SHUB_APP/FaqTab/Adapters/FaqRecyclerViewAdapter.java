package biz.teko.td.SHUB_APP.FaqTab.Adapters;

import android.content.Intent;
import android.net.Uri;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.LinkedList;
import java.util.List;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.FaqTab.Faq.FAnswer;
import biz.teko.td.SHUB_APP.FaqTab.Faq.FImage;
import biz.teko.td.SHUB_APP.FaqTab.FPQuestion;
import biz.teko.td.SHUB_APP.FaqTab.RecycleGroup.FaqGroup;
import biz.teko.td.SHUB_APP.FaqTab.RecycleHolders.ChapterGroupViewHolder;
import biz.teko.td.SHUB_APP.FaqTab.RecycleHolders.QuestionViewHolder;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 08.02.2017.
 */

public class FaqRecyclerViewAdapter extends ExpandableRecyclerViewAdapter<ChapterGroupViewHolder, QuestionViewHolder>
{

	public FaqRecyclerViewAdapter(List<? extends ExpandableGroup> groups)
	{
		super(groups);
	}

	@Override
	public ChapterGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType)
	{
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_chapter, parent, false);
		return new ChapterGroupViewHolder(view);
	}

	@Override
	public QuestionViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType)
	{
		View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_question, parent, false);
		return new QuestionViewHolder(view);
	}

	@Override
	public void onBindChildViewHolder(QuestionViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex)
	{
		final FPQuestion fQuestion = ((FaqGroup) group).getItems().get(childIndex);

		holder.setQuestionName(fQuestion.caption);
		String answer = "";
		SpannableString spanAnswer = null;
		String link;
		if(fQuestion.fAnswers != null){
			String caption = "";
			for(FAnswer fAnswer:fQuestion.fAnswers){
				caption = fAnswer.caption;
				link = fAnswer.link;
				if(!answer.equals("")){
					if(null==link)
					{
						answer += "\n";
					}
				}
				answer+= caption;
				spanAnswer = new SpannableString(answer);
				if(null!=link){
					final String finalLink = link;
					ClickableSpan span = new ClickableSpan()
					{
						@Override
						public void onClick(View widget)
						{
							Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(finalLink));
							browserIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							(App.getContext()).startActivity(browserIntent);
						}
					};
					int start = answer.length() - caption.length();
					int end = answer.length();
					spanAnswer.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				}

			}
		}
		holder.setQuestionAnswer(spanAnswer);
		if(null!=fQuestion.fImages)
		{
			LinkedList<FImage> fImages = fQuestion.fImages;
			LinkedList<String> srcs = new LinkedList<String>();
			for(FImage fImage:fImages)
			{
				srcs.add(fImage.src);
			}
			holder.setQuestionImages(srcs);
		}
	}

	@Override
	public void onBindGroupViewHolder(ChapterGroupViewHolder holder, int flatPosition, ExpandableGroup group)
	{
		holder.setChapterName(group);
	}
}

