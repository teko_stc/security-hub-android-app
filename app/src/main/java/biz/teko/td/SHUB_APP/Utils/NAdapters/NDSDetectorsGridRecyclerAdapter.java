package biz.teko.td.SHUB_APP.Utils.NAdapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;

import java.util.LinkedList;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Relays_N.Activities.RelayActivity;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Models.DeviceInfo;
import biz.teko.td.SHUB_APP.Utils.NViews.NMainBarElement;
import biz.teko.td.SHUB_APP.Zones_N.Activities.ZoneActivity;
import io.reactivex.disposables.CompositeDisposable;

public class NDSDetectorsGridRecyclerAdapter extends RecyclerView.Adapter<NDSDetectorsGridRecyclerAdapter.DetectorVH>
{
	public static final int STATUS_NO_ARM_CONTROL = -1;
	private final Context context;
	private Cursor cursor;
	private final DBHelper dbHelper;

	private final CompositeDisposable disposables = new CompositeDisposable();

	@Override
	public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
		super.onDetachedFromRecyclerView(recyclerView);
		disposables.dispose();
	}



	@Override
	public int getItemCount() {
		return cursor.getCount();
	}

	protected static class DetectorVH extends RecyclerView.ViewHolder{

		public DetectorVH(@NonNull View itemView) {
			super(itemView);
		}
	}


	@NonNull
	@Override
	public DetectorVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		NMainBarElement mainBarView = new NMainBarElement(context, R.layout.n_zones_grid_element_view);
		return new DetectorVH(mainBarView);
	}

	@Override
	public void onBindViewHolder(@NonNull DetectorVH holder, int position) {
			applyData(fetchData(position), holder.itemView);
	}

	public void update(Cursor cursor)
	{
		this.cursor = cursor;
		notifyDataSetChanged();
	}

	public NDSDetectorsGridRecyclerAdapter(Context context, Cursor c)
	{
		this.context = context;
		this.cursor = c;
		this.dbHelper = DBHelper.getInstance(context);
	}


	private DeviceInfo fetchData(int position){
		cursor.moveToPosition(position);
		Zone zone = dbHelper.getZone(cursor);
		LinkedList<Event> affects = dbHelper.getAffectsListOnlyByZoneId(zone.device_id, zone.section_id, zone.id);
		return new DeviceInfo.DeviceInfoBuilder().setZone(zone).setAffects(affects).createDeviceInfo();
	}

	@MainThread
	private void applyData(DeviceInfo info, View view){
		if(null!=info.getZone()){
			NMainBarElement mainBarView = (NMainBarElement) view;
			view.refreshDrawableState();
			mainBarView.setTitle(info.getZone().name);

			int bigImage = R.drawable.ic_relay_main;
			// set by default, if not relay it will change to zone icon
			int armStatus = Const.STATUS_NO_ARM_CONTROL;

			switch (info.getZone().getDetector()){
				case Const.DETECTOR_SIREN:
					bigImage = R.drawable.n_image_siren_main_selector;
					break;
				case Const.DETECTOR_LAMP:
					bigImage = R.drawable.n_image_lamp_main_selector;
					break;
				case Const.DETECTOR_KEYPAD:
				case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
					bigImage = R.drawable.n_image_keypad_main_selector;
					break;
				case Const.DETECTOR_AUTO_RELAY:
					mainBarView.setAuto(true);
					Script script = dbHelper.getDeviceScriptsByBind(info.getZone().device_id, info.getZone().id);
					if(null!=script){
						mainBarView.setScripted(true);
						int scriptIcon = script.getIcon();
						if(0!=scriptIcon) bigImage = scriptIcon;
//						break; // to avoid status set
					}
				case Const.DETECTOR_MANUAL_RELAY:
					armStatus = dbHelper.getRelayStatusById(info.getZone().device_id, info.getZone().section_id, info.getZone().id);
					break;
				default:
					mainBarView.setType(MainBar.Type.DEVICE);
					bigImage = R.drawable.n_image_device_common_main_selector;
					break;
			}

			int model = info.getZone().getModelId();
			switch (model){
				case Const.SENSORTYPE_WIRED_OUTPUT:
					DWOType dwoType = info.getZone().getDWOType();
					if (null != dwoType && null != dwoType.icons) {
						bigImage = dwoType.getMainIcon(context);
					}else if(info.getZone().detector != Const.DETECTOR_AUTO_RELAY && info.getZone().detector != Const.DETECTOR_MANUAL_RELAY){
						bigImage = R.drawable.n_image_wired_output_common_main_selector;
					}
					if(info.getZone().detector == Const.DETECTOR_AUTO_RELAY)
						armStatus = Const.STATUS_NO_ARM_CONTROL;
					break;
				case Const.SENSORTYPE_WIRED_INPUT:
					DWIType dwiType = info.getZone().getDWIType();
					if (null != dwiType && null != dwiType.icons)
					{
						bigImage = dwiType.getMainIcon(context);
						DType dType = dwiType.getDetectorType(info.getZone().getDetector());
						if(null!=dType){
							AType aType = dType.getAlarmType(info.getZone().getAlarm());
							if(null!=aType && null!=aType.icons){
								bigImage = aType.getMainIcon(context);
							}
						}
					}else{
						bigImage = R.drawable.n_image_wired_input_common_main_selector;
					}
				case Const.SENSORTYPE_UNKNOWN:
					break;
				default:
					ZType zType = info.getZone().getModel();
					if (null != zType && null != zType.icons)
					{
						bigImage = zType.getMainIcon(context);
						DType dType = zType.getDetectorType(info.getZone().getDetector());
						if(null!=dType){
							AType aType = dType.getAlarmType(info.getZone().getAlarm());
							if(null!=aType && null!=aType.icons){
								bigImage = aType.getMainIcon(context);
							}
						}
					}
					break;
			}

			switch (armStatus){
				case Const.STATUS_OFF:
					mainBarView.setOff(true);
					break;
				case Const.STATUS_ON:
					mainBarView.setOn(true);
					break;
				default:
					mainBarView.setNoArm(true);
					break;
			}

			mainBarView.setImageBig(bigImage);

			mainBarView.setStates(info.getAffects());

			mainBarView.setOnElementClickListener(new NMainBarElement.OnElementClickListener()
			{
				@Override
				public void onClick()
				{
					openZone(dbHelper.getSiteByDeviceSection(info.getZone().device_id, info.getZone().section_id), info.getZone(), mainBarView);
				}

				@Override
				public void onLongClick()
				{

				}
			});
		}
	}


	private void openZone(Site site, Zone zone, NMainBarElement elementView)
	{
		switch (zone.detector){
			case 13:
			case 23:
				openRelayActivity(site, zone, elementView);
				break;
			default:
				openZoneActivity(site, zone, elementView);
				break;
		}
	}

	private void openZoneActivity(Site site, Zone zone, NMainBarElement elementView)
	{
		Intent intent = new Intent(context, ZoneActivity.class);
		intent.putExtra("bar_title", zone.name);
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		intent.putExtra("transition", true);
		intent.putExtra("transition_start", getTransitionStartSize(elementView));
		context.startActivity(intent, getTransitionOptions(elementView).toBundle());
	}



	private void openRelayActivity(Site site, Zone zone, NMainBarElement elementView)
	{
		Intent intent = new Intent(context, RelayActivity.class);
		intent.putExtra("bar_title", zone.name);
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		intent.putExtra("transition", true);
		intent.putExtra("transition_start", getTransitionStartSize(elementView));
		context.startActivity(intent, getTransitionOptions(elementView).toBundle());
	}

	private int getTransitionStartSize(View view){
		return  Func.getTextSize(context, ((NMainBarElement)view).getTitleView());
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((Activity) context,
				new Pair<>(((NMainBarElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}
}
