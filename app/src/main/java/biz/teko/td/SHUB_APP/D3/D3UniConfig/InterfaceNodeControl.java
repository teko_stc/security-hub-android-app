package biz.teko.td.SHUB_APP.D3.D3UniConfig;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

/**
 * Created by td13017 on 29.06.2016.
 */
public class InterfaceNodeControl extends InterfaceNode{
	@Attribute(name="type", required = false)
	public String Type;

	@Attribute(name="field", required = false)
	public String Field;

	@Attribute(name="password", required = false)
	public String Password;

	@Attribute(name="inverse", required = false)
	public String Inverse;

	@ElementList(entry="verify", inline = true, required = false)
	public LinkedList<VerificationNode> Verify;

	@ElementList(entry="action", inline = true, required = false)
	public LinkedList<ActionNode> Action;

	@ElementList(entry="option", inline = true, required = false)
	public LinkedList<OptionNode> Options;
}
