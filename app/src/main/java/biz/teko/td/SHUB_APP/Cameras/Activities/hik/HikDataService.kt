package biz.teko.td.SHUB_APP.Cameras.Activities.hik

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers
import java.util.concurrent.TimeUnit

const val HIK_TIMEOUT = 30L
const val HIK_API_BASE_URL = "https://api.hik-proconnectru.com"

class HikDataService {


        fun getClient(
            interceptors: Collection<Interceptor>
        ): OkHttpClient {

            return OkHttpClient.Builder()
                .connectTimeout(HIK_TIMEOUT, TimeUnit.SECONDS).apply {
                    for (interceptor in interceptors) {
                        addInterceptor(interceptor)
                    }
                }
                .build()
        }
        private val rxAdapter: RxJavaCallAdapterFactory =
            RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io())

        fun getBaseRetrofit(client: OkHttpClient): Retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(HIK_API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(rxAdapter)
            .build()
        fun getService(retrofit: Retrofit): HikCameraSource =
            retrofit.create(
                HikCameraSource::class.java
            )

    }