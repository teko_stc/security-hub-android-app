package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by td13017 on 17.05.2017.
 */
public class Cluster
{
	public int id;
	public int system;
	public int cluster;
	public String name;

	public Cluster(){};

	public Cluster(JSONObject cluster){
		try
		{
			this.id = cluster.getInt("id");
			this.system = cluster.getInt("system");
			this.cluster = cluster.getInt("cluster");
			this.name = cluster.getString("name");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	};

	public Cluster(Cursor cursor)
	{
		this.id = cursor.getInt(1);
		this.system = cursor.getInt(2);
		this.cluster = cursor.getInt(3);
		this.name = cursor.getString(4);
	}
}
