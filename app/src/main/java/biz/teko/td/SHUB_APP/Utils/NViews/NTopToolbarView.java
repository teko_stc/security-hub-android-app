package biz.teko.td.SHUB_APP.Utils.NViews;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NTopToolbarView extends LinearLayout {
    public static final int[] STATE_INFO = {R.attr.state_info};
    private boolean isInfo = false;
    private boolean visible = false;

    private Context context;
    private int layout;
    private String title;
    private int titleColor;
    private int titleSize;
    private String subTitle;
    private int subTitleColor;
    private float subTitleSize;
    private int background;
    private int info;

    private ImageView backImage;
    private TextView titleView, subTitleView;
    private ConstraintLayout mainLayout;
    private FrameLayout infoLayout;
    private LinearLayout nButtonBack;

    public void setOnBackClickListener(OnBackClickListener onBackClickListener) {
        if (nButtonBack != null) {
            nButtonBack.setOnClickListener(view -> onBackClickListener.backClick());
        }
    }

    public interface OnBackClickListener {
        void backClick();
    }

    public View getViewById(int id) {
        return mainLayout.findViewById(id);
    }

    public void setTitle(String title) {
        this.title = title;
        setTitleView();
    }

    public void setSubtitle(String subTitle) {
        this.subTitle = subTitle;
        setSubTitleView();
    }



    public NTopToolbarView(Context context) {
        super(context);
        this.context = context;
    }

    public NTopToolbarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        parseAttributes(attrs);
        setup();
        bind();
    }

    private void bind() {
        if (null != backImage) setImageView();
        if (null != titleView) setTitleView();
        if (null != subTitleView) setSubTitleView();
        if (null != mainLayout) setMainLayout();
        if (null != infoLayout) setInfoLayout();
    }

    private void setInfoLayout() {
        if (info != 0) {
            View infoRow = LayoutInflater.from(context).inflate(info, null);
            infoLayout.addView(infoRow);
            infoLayout.setVisibility(isInfo ? VISIBLE : GONE);
        }
    }

    private void setMainLayout() {
        if (background != 0)
            mainLayout.setBackgroundResource(background);
    }

    private void setImageView() {
        if (backImage != null) {
            backImage.setImageResource(R.drawable.n_top_toolbar_back_button_selector);
            backImage.setImageLevel(isInfo ? 1 : 0);
        }
    }

    private void setTitleView() {
        if (null!=titleView) {
            if(null!=title)
            {
                if (0 != titleColor) titleView.setTextColor(context.getResources().getColorStateList(titleColor));
                if (0 != titleSize) titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, titleSize);
                titleView.setText(title);
                titleView.setVisibility(VISIBLE);
            }else{
                titleView.setVisibility(GONE);
            }
        }
    }

    private void setSubTitleView() {
        if (null != subTitleView) {
            if(null!=subTitle)
            {
                if (0 != subTitleColor) subTitleView.setTextColor(context.getResources().getColorStateList(subTitleColor));
                if (0 != subTitleSize) subTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, subTitleSize);
                subTitleView.setText(subTitle);
                subTitleView.setVisibility(VISIBLE);
                setTitlePadding(5, VISIBLE);
            }else{
                subTitleView.setVisibility(GONE);
                setTitlePadding(15, GONE);
            }
        }
    }

    private void setTitlePadding(int paddingBottom, int visibility) {
        if(null!=titleView) titleView.setPadding(0, 0,0, paddingBottom);
    }

    private void setup() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutInflater.inflate(layout, this, true);

        titleView = findViewById(R.id.nTextTitle);
        subTitleView = findViewById(R.id.nSubTextTitle);
        mainLayout = findViewById(R.id.mainToolbarLayout);
        infoLayout = findViewById(R.id.toolbarInfoLayout);
        nButtonBack = findViewById(R.id.includeBackButton);
        backImage = findViewById(R.id.image_back);
    }

    private void parseAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NTopToolbarView, 0, 0);
        try {
            layout = a.getResourceId(R.styleable.NTopToolbarView_NTopToolbarViewLayout, R.layout.n_top_toolbar_view);
            title = a.getString(R.styleable.NTopToolbarView_NTopToolbarViewTitle);
            titleColor = a.getResourceId(R.styleable.NTopToolbarView_NTopToolbarViewTextColor, R.color.n_text_dark_grey);
            titleSize = a.getInt(R.styleable.NTopToolbarView_NTopToolbarViewTextSize, 25);
            subTitle = a.getString(R.styleable.NTopToolbarView_NTopToolbarViewSubTitle);
            subTitleColor = a.getResourceId(R.styleable.NTopToolbarView_NTopToolbarViewSubTextColor, R.color.n_text_dark_grey);
            subTitleSize = a.getFloat(R.styleable.NTopToolbarView_NTopToolbarViewSubTextSize, (float) 15.5);
            background = a.getResourceId(R.styleable.NTopToolbarView_NTopToolbarViewBackground, android.R.color.transparent);
            info = a.getResourceId(R.styleable.NTopToolbarView_NTopToolbarViewInfoLayout, 0);
        } finally {
            a.recycle();
        }
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        if (isInfo) {
            final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
            mergeDrawableStates(drawableState, STATE_INFO);
            return drawableState;
        } else {
            return super.onCreateDrawableState(extraSpace);
        }
    }

    public void setStateInfo(boolean info) {
        this.isInfo = info;
        refreshDrawableState();
        if (infoLayout != null) {
            if (info) {
                showView(infoLayout);
            } else {
                hideView(infoLayout);
            }
        }
    }

    private void showView(View view) {
        if (visible) return;
        view.setVisibility(View.VISIBLE);
        view.setAlpha(0.0f);
        ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) view.getLayoutParams();
        backImage.setImageLevel(1);
        lp.topMargin = Func.dpToPx(16, context);
        view.setLayoutParams(lp);
        view.animate()
                .alpha(1.0f)
                .setDuration(500)
                .setListener(null);
        visible = true;
    }


    private void hideView(View view) {
        if (!visible) return;
        view.setPadding(0, 0, 0, 0);
        backImage.setImageLevel(0);
        view.animate()
                .translationY(0)
                .alpha(0.0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        view.setVisibility(View.GONE);
                    }
                });
        visible = false;
    }
}
