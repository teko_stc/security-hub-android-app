package biz.teko.td.SHUB_APP.Profile.Security;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 31.08.2017.
 */

public class SecurityForceCodeConfirmFragment extends Fragment
{

	private Context context;
	private DBHelper dbHelper;
	private View indicator1, indicator2, indicator3, indicator4;
	private int enteredCount = 0, code = 0;
	private String codeToConfirm;
	private int siteId;

	public static SecurityForceCodeConfirmFragment newInstance(int id, String code)
	{
		SecurityForceCodeConfirmFragment securityForceCodeConfirmFragment  = new SecurityForceCodeConfirmFragment();
		Bundle b = new Bundle();
		b.putInt("site", id);
		b.putString("code", code);
		securityForceCodeConfirmFragment.setArguments(b);
		return securityForceCodeConfirmFragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
	{
		context = container.getContext();
		dbHelper  = DBHelper.getInstance(context);

		siteId = getArguments().getInt("site");
		codeToConfirm = getArguments().getString("code");

		View v = inflater.inflate(R.layout.activity_wizard_pin_enter, container, false);
		LinearLayout dotsLayout = (LinearLayout) v.findViewById(R.id.CAA_Type);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		layoutParams.setMargins(Func.dpToPx(10, context), Func.dpToPx(10,context), Func.dpToPx(10, context), 0);
		dotsLayout.setLayoutParams(layoutParams);

		TextView textEnterPin = (TextView) v.findViewById(R.id.wizEnterPinTitle);
		textEnterPin.setText(R.string.CONFIRM_CODE);
		TextView textForgetPin = (TextView) v.findViewById(R.id.wizPinEnterForgetPin);
		textForgetPin.setVisibility(View.GONE);
		LinearLayout buttonFingerPrint = (LinearLayout) v.findViewById(R.id.buttonFingerPrint);
		buttonFingerPrint.setVisibility(View.GONE);
		Button buttonChangeUser = (Button) v.findViewById(R.id.wizPinChangeUser);
		buttonChangeUser.setVisibility(View.GONE);

		indicator1 = (View) v.findViewById(R.id.indicator1);
		indicator2 = (View) v.findViewById(R.id.indicator2);
		indicator3 = (View) v.findViewById(R.id.indicator3);
		indicator4 = (View) v.findViewById(R.id.indicator4);

		LinearLayout[] linearLayouts = new LinearLayout[11];

		LinearLayout button0 = (LinearLayout) v.findViewById(R.id.buttonCode0);
		linearLayouts[0] = button0;
		LinearLayout button1 = (LinearLayout) v.findViewById(R.id.buttonCode1);
		linearLayouts[1] = button1;
		LinearLayout button2 = (LinearLayout) v.findViewById(R.id.buttonCode2);
		linearLayouts[2] = button2;
		LinearLayout button3 = (LinearLayout) v.findViewById(R.id.buttonCode3);
		linearLayouts[3] = button3;
		LinearLayout button4 = (LinearLayout) v.findViewById(R.id.buttonCode4);
		linearLayouts[4] = button4;
		LinearLayout button5 = (LinearLayout) v.findViewById(R.id.buttonCode5);
		linearLayouts[5] = button5;
		LinearLayout button6 = (LinearLayout) v.findViewById(R.id.buttonCode6);
		linearLayouts[6] = button6;
		LinearLayout button7 = (LinearLayout) v.findViewById(R.id.buttonCode7);
		linearLayouts[7] = button7;
		LinearLayout button8 = (LinearLayout) v.findViewById(R.id.buttonCode8);
		linearLayouts[8] = button8;
		LinearLayout button9 = (LinearLayout) v.findViewById(R.id.buttonCode9);
		linearLayouts[9] = button9;
		LinearLayout buttonBackSpace = (LinearLayout) v.findViewById(R.id.buttonCodeBackSpace);
		linearLayouts[10] = buttonBackSpace;

		for(int i = 0; i < linearLayouts.length; i++){
			final int finalI = i;
			linearLayouts[i].setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(finalI != 10)
					{
						if (enteredCount < 4)
						{
							enteredCount++;
							code = code *10 + finalI;
						}
					}else{
						if(enteredCount > 0){
							enteredCount--;
							code = code /10;
						}
					}
					updatePinIndicators(enteredCount);
					if(enteredCount == 4){
						checkCodeSet();
					}
				}
			});
		}

		return v;
	}

	private void checkCodeSet()
	{
		String codeString = Func.md5(String.valueOf(code));
		if (codeString.equals(codeToConfirm))
		{
//			SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//			sharedPreferences.edit().putString("pref_force_site_" + String.valueOf(site), codeString).apply();
			dbHelper.updateSiteForceCode(siteId, codeString);
			Func.pushToast(context, getString(R.string.SUCCESS_SET), (Activity) context);
			((SecurityForceActivity) context).finish();
		} else
		{
			Func.pushToast(context, getString(R.string.PN_ADD_INCORRECT_PIN), (Activity) context);
			code = 0;
			enteredCount = 0;
			updatePinIndicators(0);
		}
	}

	private void updatePinIndicators(int enteredCount)
	{
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int resizeValue = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 20, metrics);
		int defaultValue = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 10, metrics);
		switch (enteredCount){
			case 0:
				indicator1.getLayoutParams().height = defaultValue;
				indicator1.getLayoutParams().width = defaultValue;
				indicator1.requestLayout();
				indicator2.getLayoutParams().height = defaultValue;
				indicator2.getLayoutParams().width = defaultValue;
				indicator2.requestLayout();
				indicator3.getLayoutParams().height = defaultValue;
				indicator3.getLayoutParams().width = defaultValue;
				indicator3.requestLayout();
				indicator4.getLayoutParams().height = defaultValue;
				indicator4.getLayoutParams().width = defaultValue;
				indicator4.requestLayout();
				break;
			case 1:
				indicator1.getLayoutParams().height = resizeValue;
				indicator1.getLayoutParams().width = resizeValue;
				indicator1.requestLayout();
				indicator2.getLayoutParams().height = defaultValue;
				indicator2.getLayoutParams().width = defaultValue;
				indicator2.requestLayout();
				indicator3.getLayoutParams().height = defaultValue;
				indicator3.getLayoutParams().width = defaultValue;
				indicator3.requestLayout();
				indicator4.getLayoutParams().height = defaultValue;
				indicator4.getLayoutParams().width = defaultValue;
				indicator4.requestLayout();
				break;
			case 2:
				indicator1.getLayoutParams().height = resizeValue;
				indicator1.getLayoutParams().width = resizeValue;
				indicator1.requestLayout();
				indicator2.getLayoutParams().height = resizeValue;
				indicator2.getLayoutParams().width = resizeValue;
				indicator2.requestLayout();
				indicator3.getLayoutParams().height = defaultValue;
				indicator3.getLayoutParams().width = defaultValue;
				indicator3.requestLayout();
				indicator4.getLayoutParams().height = defaultValue;
				indicator4.getLayoutParams().width = defaultValue;
				indicator4.requestLayout();
				break;
			case 3:
				indicator1.getLayoutParams().height = resizeValue;
				indicator1.getLayoutParams().width = resizeValue;
				indicator1.requestLayout();
				indicator2.getLayoutParams().height = resizeValue;
				indicator2.getLayoutParams().width = resizeValue;
				indicator2.requestLayout();
				indicator3.getLayoutParams().height = resizeValue;
				indicator3.getLayoutParams().width = resizeValue;
				indicator3.requestLayout();
				indicator4.getLayoutParams().height = defaultValue;
				indicator4.getLayoutParams().width = defaultValue;
				indicator4.requestLayout();
				break;
			case 4:
				indicator1.getLayoutParams().height = resizeValue;
				indicator1.getLayoutParams().width = resizeValue;
				indicator1.requestLayout();
				indicator2.getLayoutParams().height = resizeValue;
				indicator2.getLayoutParams().width = resizeValue;
				indicator2.requestLayout();
				indicator3.getLayoutParams().height = resizeValue;
				indicator3.getLayoutParams().width = resizeValue;
				indicator3.requestLayout();
				indicator4.getLayoutParams().height = resizeValue;
				indicator4.getLayoutParams().width = resizeValue;
				indicator4.requestLayout();
				break;
			default:
				break;
		}
	}
}
