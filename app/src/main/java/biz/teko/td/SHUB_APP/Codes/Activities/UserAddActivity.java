package biz.teko.td.SHUB_APP.Codes.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.LinkedList;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Codes.Adapters.UserAddSectionsListAdapter;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class UserAddActivity extends BaseActivity
{
	private int site_id, device_id;
	private String deviceSerial, deviceModel;
	private Context context;
	private DBHelper dbHelper;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private LinkedList<Section> sectionsList;
	private LinkedList<Section> cSectionsList;
	private Button createButton;
	private Device device;
	private UserAddSectionsListAdapter userAddSectionsListAdapter;
	private Section[] sections;
	private TextView editSections;
	private UserInfo userInfo;	private CountDownTimer timer;
	private RadioGroup userAddType;

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver userAddReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			finish();
		}
	};

	private BroadcastReceiver interactiveReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int status = intent.getIntExtra("status", -1);
			if(status == 1)
			{
				if(null!=userAddType){
					userAddType.setEnabled(false);
				}
				LinearLayout timerFrame = (LinearLayout) findViewById(R.id.userTimerLayout);
				if (null != timerFrame)
				{
					timerFrame.setVisibility(View.VISIBLE);
					final LinearLayout userManualLayout = (LinearLayout) findViewById(R.id.userManualLayout);
					final LinearLayout userAutoLayout = (LinearLayout) findViewById(R.id.userAutoLayout);
					if (null != userManualLayout)
					{
						userManualLayout.setVisibility(View.GONE);
					}
					;
					if (null != userAutoLayout)
					{
						userAutoLayout.setVisibility(View.GONE);
					}
					if (null != createButton)
					{
						createButton.setVisibility(View.GONE);
					}
					final TextView textTimer = (TextView) findViewById(R.id.userTimerView);
					if (null == timer)
					{
						timer = new CountDownTimer(60000, 1000)
						{

							public void onTick(long millisUntilFinished)
							{
								textTimer.setText("0:" + checkDigit((int) millisUntilFinished / 1000));
							}

							public void onFinish()
							{
								if (null != createButton)
								{
									createButton.setVisibility(View.VISIBLE);
								}
								textTimer.setVisibility(View.INVISIBLE);
								if(null!=userAddType){
									userAddType.setEnabled(true);
								}
								timer = null;
							}

						};
						timer.start();
					}
				}
			}

		}
	};


	public String checkDigit(int number) {
		return number <= 9 ? "0" + number : String.valueOf(number);
	}

	private int sending = 0;

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(final Context context, Intent intent)
		{
			sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
						} else
						{
							Func.pushToast(context, context.getString(R.string.COMMAND_BEEN_SEND), (Activity) context);
						}
						break;
					case -1:
						Func.pushToast(context, context.getString(R.string.COMMAND_NOT_BEEN_SEND), (Activity) context);
						sending = 0;
						break;
					default:
						sending = 0;
						break;
				}
			}
			if(sending == 0){
				createButton.setEnabled(true);
			}else{
				createButton.setEnabled(false);
			}
		}
	};


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_add);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		site_id = getIntent().getIntExtra("site", -1);
		device_id = getIntent().getIntExtra("device", -1);

		if(-1 != site_id){
			toolbar.setTitle(dbHelper.getSiteNameById(site_id));
		}
		if(-1 != device_id){
			device = dbHelper.getDeviceById(device_id);
			if(null!=device){
				deviceSerial = Integer.toString(device.account);
				deviceModel =device.getName();
			}
			toolbar.setSubtitle(deviceModel + "(" + deviceSerial + ")  •  " + getString(R.string.USER_CODE_ADD_TITLE));
			toolbar.setSubtitleTextColor(getResources().getColor(R.color.brandColorWhite));

		}
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		serviceConnection = new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}


			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		final LinearLayout userManualLayout = (LinearLayout) findViewById(R.id.userManualLayout);
		final LinearLayout userAutoLayout = (LinearLayout) findViewById(R.id.userAutoLayout);
		final LinearLayout timerFrame = (LinearLayout) findViewById(R.id.userTimerLayout);
		userAddType = (RadioGroup) findViewById(R.id.userAddRadioGroup);
		createButton = (Button) findViewById(R.id.aua_button_add);

		userAddType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId)
			{
				switch (checkedId){
					case R.id.checkManualUser:
						if(null!= userManualLayout){
							userManualLayout.setVisibility(View.VISIBLE);
						};
						if(null!= userAutoLayout){
							userAutoLayout.setVisibility(View.GONE);
						}
						if(null!=timerFrame){
							timerFrame.setVisibility(View.GONE);
						}
						if(null!=createButton){
							createButton.setText(R.string.USER_MANUAL_CREATE);
						}
						break;
					case R.id.checkAutoUser:
						if(null!= userManualLayout){
							userManualLayout.setVisibility(View.GONE);
						};
						if(null!= userAutoLayout){
							userAutoLayout.setVisibility(View.VISIBLE);
						}
						if(null!=timerFrame){
							timerFrame.setVisibility(View.GONE);
						}
						if(null!=createButton){
							createButton.setText(R.string.USER_AUTO_CREATE);
						}
						break;

				}
			}
		});
		userAddType.check(R.id.checkManualUser);

		TextView deviceText = (TextView) findViewById(R.id.aua_text_device);
		final EditText userName = (EditText) findViewById(R.id.aua_edit_name);
		final EditText userAutoName = (EditText) findViewById(R.id.aua_auto_edit_name);
		final EditText userPass = (EditText) findViewById(R.id.aua_edit_pass);
		editSections = (TextView) findViewById(R.id.aua_edit_sections);

		if(null!=userName)
		{
			userName.setOnEditorActionListener(new TextView.OnEditorActionListener()
			{
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
					{
						if(null!=userPass)
						{
							userPass.requestFocus();
						}
						return true;
					}
					return false;
				}
			});
		}

		deviceText.setText(deviceSerial);

		sectionsList = new LinkedList<>();
		sections = dbHelper.getGuardSectionsForSiteDeviceWithoutZones(site_id, device_id);
		if(null!=sections)
		{
			for (Section section : sections)
			{
				sectionsList.add(section);
			}
		}

		editSections.setText(R.string.UAA_PRESS_TO_CHOOSE);
		editSections.setOnClickListener(new SetionListShowOnClick());

		createButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(null!=userAddType){
					switch (userAddType.getCheckedRadioButtonId()){
						case R.id.checkManualUser:if(!userName.getText().toString().matches(""))
						{
							if(!userPass.getText().toString().matches("")){
								String name = userName.getText().toString();
								String pass = userPass.getText().toString();
								JSONObject commandAddress = new JSONObject();
								try
								{
									commandAddress.put("name", name);
									commandAddress.put("password", pass);
									if(cSectionsList !=null && cSectionsList.size() != 0){
										JSONArray cSections = new JSONArray();
										for(int i = 0; i < cSectionsList.size(); i++){
											cSections.put(cSectionsList.get(i).id);
										}
										commandAddress.put("sections", cSections);
										int command_count = Func.getCommandCount(context);
										D3Request d3Request = D3Request.createCommand(userInfo.roles, device_id, "USER_SET", commandAddress, command_count);
										if(d3Request.error == null){
											if(sendCommandToServer(d3Request)){
												sendIntentCommandBeenSend(1);
											}
										}else{
											Func.pushToast(context, d3Request.error, (Activity) context);
										}
									}else{
										Func.pushToast(context, getString(R.string.USER_ADD_ERROR_ENTER_SECTIONS), (Activity) context);
									}

								} catch (JSONException e)
								{
									e.printStackTrace();
								}
							}else{
								Func.pushToast(context, getString(R.string.USER_ADD_ERROR_ENTER_PASS), (Activity) context);
							}
						}else{
							Func.pushToast(context, getString(R.string.USER_ADD_ERROR_ENTER_NAME), (Activity) context);
						}

							break;
						case R.id.checkAutoUser:
							if(!userAutoName.getText().toString().matches("")){
								JSONObject commandAddress = new JSONObject();
								try
								{
									commandAddress.put("index", 1);
									commandAddress.put("name", userAutoName.getText().toString());
									commandAddress.put("hashcode", -1);
									JSONArray cSections = new JSONArray();
									cSections.put(0);
									commandAddress.put("sections", cSections);
									int command_count = Func.getCommandCount(context);
									D3Request d3Request = D3Request.createCommand(userInfo.roles, device_id, "USER_SET", commandAddress, command_count);
									if(d3Request.error == null){
										if(sendCommandToServer(d3Request)){
											sendIntentCommandBeenSend(1);
										}
									}else{
										Func.pushToast(context, d3Request.error, (Activity) context);
									}
								} catch (JSONException e)
								{
									e.printStackTrace();
								}

							}else{
								Func.pushToast(context, getString(R.string.USER_ADD_ERROR_ENTER_NAME), (Activity) context);
							}
							break;
					}
				}

			}
		})
		;
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(commandSendReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(userAddReceiver);
		unregisterReceiver(interactiveReceiver);
		unbindService(serviceConnection);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		registerReceiver(userAddReceiver, new IntentFilter(D3Service.BROADCAST_USER_ADD));
		registerReceiver(interactiveReceiver, new IntentFilter(D3Service.BROADCAST_INTERACTIVE_STATUS));
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	private boolean sendCommandToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				} else
				{
					sendIntentCommandBeenSend(-1);
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};

	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	public void setSections(Section[] sections){
		this.sections = sections;
		if(null!=cSectionsList)
		{
			cSectionsList.clear();
		}else{
			cSectionsList = new LinkedList<>();
		}
		for(Section section: sections){
			if(section.checked){
				cSectionsList.add(section);
			}
		}

		if(cSectionsList.size() > 0){
			String[] sectionIds = new String[cSectionsList.size()];
			for(int i =0 ; i < cSectionsList.size(); i++){
				sectionIds[i] = Integer.toString(cSectionsList.get(i).id);
			}
			String sectionString = Arrays.toString(sectionIds);
			editSections.setText(sectionString.substring(1, sectionString.length()-1));
		}else{
			editSections.setText(R.string.UAA_PRESS_TO_CHOOSE);
		}
	}

	public Section[] getSections(){
		return sections;
	}

	private class SetionListShowOnClick implements View.OnClickListener
	{
		private Section[] sections;

		public SetionListShowOnClick(){
		}

		@Override
		public void onClick(View v)
		{
			AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
			final AlertDialog checkSectionsDialog = adb.create();
			checkSectionsDialog.setTitle(getString(R.string.USER_ADD_MESSAGE_ENTER_SECTIONS));

			FrameLayout frameLayout = (FrameLayout) new FrameLayout(context);
			frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

			this.sections = getSections();

			ListView sectionsCheckList = new ListView(context);
			if(sections.length != 0)
			{
				userAddSectionsListAdapter = new UserAddSectionsListAdapter(context, R.layout.card_for_section_zone_check, sections);
				sectionsCheckList.setAdapter(userAddSectionsListAdapter);
				frameLayout.addView(sectionsCheckList);
			}else{
				TextView noSectionsText = new TextView(context);
				noSectionsText.setPadding(Func.dpToPx(20, context), 0, Func.dpToPx(20, context), 0);
				noSectionsText.setText(R.string.USER_ADD_ERROR_NO_COMPATIBLE_SECTIONS);
				frameLayout.addView(noSectionsText);
			}

			checkSectionsDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.OK), new DialogInterface.OnClickListener()
			{

				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					if(null!=userAddSectionsListAdapter){
						sections = userAddSectionsListAdapter.getSections();
						setSections(sections);

					}
					dialog.dismiss();
				}
			});

			checkSectionsDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.ADB_CANCEL), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
				}
			});
			checkSectionsDialog.setView(frameLayout);

			checkSectionsDialog.show();
		}
	}
}
