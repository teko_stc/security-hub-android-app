package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class ServiceInfoDialogFragment extends DialogFragment {
    private NActionButton skip;
    private NActionButton enable;
    private NActionButton more;
    private IInputListeners inputListeners;
    private Context context;

    public interface IInputListeners {
        void onEnable();
        void onMore();
    }

    public void setInputListeners(IInputListeners inputListeners) {
        this.inputListeners = inputListeners;
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);
        this.context = getActivity();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.DialogFragmentTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.n_dialog_fragment_service_info, null);
        setup(view);
        setClickListeners();
        return view;
    }

    private void setClickListeners() {
        skip.setOnButtonClickListener(v -> {
            dismissPrefs();
            dismiss();
        });
        enable.setOnButtonClickListener(view -> {
            dismissPrefs();
            inputListeners.onEnable();
            dismiss();
        });
        more.setOnButtonClickListener(view -> {
            inputListeners.onMore();
        });
    }

    private void dismissPrefs()
    {
        PrefUtils.getInstance(context).setNotificationsProblem(false);
        Func.log_d(D3Service.LOG_TAG + " Fragment", "Dismiss notification problems pref");
    }

    private void setup(View view) {
        skip = view.findViewById(R.id.nButtonSkip);
        enable = view.findViewById(R.id.nButtonEnable);
        more = view.findViewById(R.id.nButtonInfo);
    }


}
