package biz.teko.td.SHUB_APP.Alarms.ViewHolders;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NEAListElement;

public class AlarmFullScreenViewHolder extends RecyclerView.ViewHolder {

    NEAListElement view;

    public AlarmFullScreenViewHolder(@NonNull View itemView) {
        super(itemView);
        initView(itemView);
    }

    public void bind(Event event, String location, Context context) {
        view.setTitle(event.explainEventRegDescriptionLite(context));
        view.setLocation(location);
        view.setTime(Func.getServerTimeTextShort(context, event.localtime));
        view.setDate(Func.getServerDateTextShort(context, event.localtime));
        view.setImage(event.getMainIcon(context));
    }

    private void initView(View itemView) {
        view = itemView.findViewById(R.id.nEventListElement);
    }
}
