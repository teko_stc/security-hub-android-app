package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

/**
 * Created by td13017 on 21.03.2017.
 */

public class Command
{
	public static final String ARM = "ARM";
	public static final String DISARM = "DISARM";
	public static final String RELAY = "RELAY";
	public static final String REBOOT = "REBOOT";
	public static final String REGISTER = "REGISTER";
	public static final String DEREGISTER = "DEREGISTER";
	public static final String SWITCH = "SWITCH";
	public static final String USSD = "USSD";
	public static final String UPDATE = "UPDATE";
	public static final String UPDATE_APPLY = "UPDATE_APPLY";
	public static final String USER_SET = "USER_SET";
	public static final String USER_DEL = "USER_DEL";
	public static final String SECTION_SET = "SECTION_SET";
	public static final String SECTION_DEL = "SECTION_DEL";
	public static final String ZONE_SET = "ZONE_SET";
	public static final String ZONE_DEL = "ZONE_DEL";
	public static final String INTERACTIVE = "INTERACTIVE";
	public static final String RESET = "RESET";
	public static final String PING = "PING";
	public static final String BLOCK = "BLOCK";
	public static final String SCRIPT_SET = "SCRIPT_SET";
	public static final String SCRIPT_DEL = "SCRIPT_DEL";



	public int result = -1;
	public int id, inner_id;
	public String type;

	public Command(int id){
		this.id = id;
	}

	public Command(int id, int inner_id){
		this.id = id;
		this.inner_id = inner_id;
	}

	public Command(int id, String type){
		this.id = id;
		this.type = type;
	}

	public Command(int id, String type, int result){
		this.id = id;
		this.type = type;
		this.result = result;
	}

	public Command(int id, int inner_id, String type){
		this.id = id;
		this.inner_id = inner_id;
		this.type = type;
	}

	public Command(Cursor cursor)
	{
		cursor.moveToFirst();
		this.id = cursor.getInt(1);
		this.inner_id= cursor.getInt(2);
		this.type = cursor.getString(3);
	}

	public int explainCommand()
	{
		int desc_plus = 0;
		if(null!=this.type)
		{
			switch (this.type){
				case ARM:
					desc_plus = 1;
					break;
				case DISARM:
					desc_plus = 2;
					break;
				case REGISTER:
					desc_plus = 3;
					break;
				case DEREGISTER:
					desc_plus = 4;
					break;
				case SWITCH:
					desc_plus = 5;
					break;
				case USSD:
					desc_plus = 6;
					break;
				case UPDATE:
					desc_plus = 7;
					break;
				case USER_SET:
					desc_plus = 8;
					break;
				case USER_DEL:
					desc_plus = 9;
					break;
				case SECTION_SET:
					desc_plus = 10;
					break;
				case SECTION_DEL:
					desc_plus = 11;
					break;
				case ZONE_SET:
					desc_plus = 12;
					break;
				case ZONE_DEL:
					desc_plus = 13;
					break;
				case INTERACTIVE:
					desc_plus = 14;
					break;
				case RESET:
					desc_plus = 15;
					break;
				case PING:
					desc_plus = 16;
					break;
				case REBOOT:
					desc_plus = 17;
					break;
				case BLOCK:
					desc_plus = 18;
					break;
				case SCRIPT_SET:
					desc_plus = 19;
					break;
				case SCRIPT_DEL:
					desc_plus = 20;
					break;
				default:
					break;
			}
		}
		return desc_plus;
	}
}
