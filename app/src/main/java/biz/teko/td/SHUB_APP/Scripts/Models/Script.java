package biz.teko.td.SHUB_APP.Scripts.Models;

import android.database.Cursor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Script
{
	public int id;
	public int libId;
	public String uid;
	public int device;
	public int bind;
	public String name;
	public String nameLib;
	public String description;
	public String category;
	public String program; //source
	public String compileProgram;
	public String params;
	public String paramsLib;
	public String extra;
	public String extraLib;
	public String firmwares;
	public int enabled;

	//for lib
	public Script(int libId,
				  String uid,
				  String category,
				  String nameLib,
				  String description,
				  String program,
				  String paramsLib,
				  String extraLib,
				  String firmwares){
		this.libId = libId;
		this.uid = uid;
		this.category = category;
		this.nameLib = nameLib;
		this.description = description;
		this.program = program;
		this.paramsLib = paramsLib;
		this.extraLib = extraLib;
		this.firmwares = firmwares;
	}

	public Script(){};

	public Script(int id, String uid){
		this.id =id;
		this.uid = uid;
	};

	//full
	public Script(int id, String uid, int libId, int device, int bind, String name, String nameLib, String category, String description,
				  String program, String compileProgram, String extra, String extraLib, int enabled, String firmwares, String params, String paramsLib){
		this.id = id;
		this.uid = uid;
		this.libId = libId;
		this.device = device;
		this.bind = bind;
		this.name = name;
		this.nameLib = nameLib;
		this.category = category;
		this.description =description;
		this.program = program;
		this.compileProgram = compileProgram;
		this.extra = extra;
		this.extraLib = extraLib;
		this.enabled = enabled;
		this.firmwares = firmwares;
		this.params = params;
		this.paramsLib = paramsLib;
	}

	//for lib
//	public Script(int id, String uid, String name, String description, String category, String program, String params, String extra, String firmwares){
//		this.id = id;
//		this.uid = uid;
//		this.name =  name;
//		this.description = description;
//		this. category = category;
//		this.program = program;
//		this.params = params;
//		this.extra = extra;
//		this.firmwares = firmwares;
//	};

	//for device
	public Script(int id, String uid, int device, int bind, String name, String program, String compileProgram, String params, String extra, int enabled){
		this.id = id;
		this.uid = uid;
		this.device = device;
		this.bind = bind;
		this.name = name;
		this.program = program;
		this.compileProgram = compileProgram;
		this.params = params;
		this.extra = extra;
		this.enabled = enabled;
	};

	public Script(Cursor cursor)
	{
		this.id = cursor.getInt(1);
		this.uid = cursor.getString(2);
		this.device = cursor.getInt(3);
		this.bind = cursor.getInt(4);
		this.name = cursor.getString(5);
		this.program = cursor.getColumnName(6);
		this.compileProgram = cursor.getString(7);
		this.params = cursor.getString(8);
		this.extra = cursor.getString(9);
		this.enabled = cursor.getInt(10);
	}

	public int getIcon()
	{
		return 0;
	}

	public JSONArray getButtons()
	{
		if(null!=this)
		{
			String string = this.extra;
			if (null != string && this.enabled == 1)
			{
				try
				{
					JSONObject extraObject = new JSONObject(string);
					JSONObject switchObject = extraObject.getJSONObject("switch");

					if (null != switchObject)
					{
						JSONArray rowsArray = switchObject.getJSONArray("rows");
						for (int i = 0; i < rowsArray.length(); i++)
						{
							JSONObject rowObject = rowsArray.getJSONObject(i);
							if (null != rowObject)
							{
								JSONArray items = rowObject.getJSONArray("items");
								if (null != items && items.length() > 0)
								{
									return items;
								}
							}
						}
					}
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
			}
		}
		return null;
	}

}
