package biz.teko.td.SHUB_APP.ZonesTabs.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ResourceCursorTreeAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DPhysic;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptActivity;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptsLibraryActivity;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardSectionsSpinnerAdapter;
import biz.teko.td.SHUB_APP.ZonesTabs.ZonesActivity;

/**
 * Created by td13017 on 10.02.2017.
 */

public class ZonesTreeAdapter extends ResourceCursorTreeAdapter
{
	private static final int FIRE_TYPE = 1;
	private final UserInfo userInfo;
	private final SharedPreferences sp;
	private final String iso;
	private Context context;
	private DBHelper dbHelper;
	private int collapsedGroupLayoutViewId;
	private int expandedGroupLayoutViewId;
	private D3Service myService;
	private int sending = 0;
	private int site_id;
	private int device_id;
	private int section_id;
	private boolean visibleButtons;
	private int setSectionId = -1;

	private boolean wait = false;
	private Timer timer;
	private MaterialDialog materialDialog;
	private boolean act = true; // false - редактирование, true - установка нового и переход на списки из библиотеки
	private Zone zone = null;

	public ZonesTreeAdapter(Context context, Cursor cursor, int collapsedGroupLayout, int expandedGroupLayout, int childLayout, int lastChildLayout, int site_id, int device_id, int section_id)
	{
		super(context, cursor, collapsedGroupLayout, expandedGroupLayout, childLayout, lastChildLayout);
		this.context = context;
		this.site_id = site_id;
		this.device_id = device_id;
		this.section_id=section_id;
		this.dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));
		View view = newGroupView(context, null, false, null);
		collapsedGroupLayoutViewId = view.getId();
		view = newGroupView(context, null, true, null);
		expandedGroupLayoutViewId = view.getId();
	}

	public void update(Cursor cursor, int sending){
		this.changeCursor(cursor);
		this.sending = sending;
	}

	public void goToScriptSet(){
		if (materialDialog != null && materialDialog.isShowing())
		{
			materialDialog.dismiss();
			if (wait)
			{
				if (null != timer) timer.cancel();
				wait = false;
				if(act)
				{
					openScriptsLibrary(zone);
				}else{
					act = true;
					openDeviceScript(zone);
				}
			}else{
				Func.nShowMessage(context, context.getString(R.string.SCRIPT_ERROR_CANT_LOAD_LIB));
			}
		}
	}

	@Override
	protected Cursor getChildrenCursor(Cursor groupCursor)
	{
		int device_id = groupCursor.getInt(4);
		int section_id = groupCursor.getInt(3);
		int zone_id = groupCursor.getInt(1);
		return dbHelper.getAffectCursorBySiteDeviceSectionZoneIdWithExclude(site_id, device_id, section_id, zone_id);
	}

	@Override
	protected void bindGroupView(View view, final Context context, Cursor cursor, boolean isExpanded)
	{
		final Zone zone = dbHelper.getZone(cursor);
		visibleButtons = false;

		TextView name = (TextView) view.findViewById(R.id.siteName);
		TextView  physic  = (TextView) view.findViewById(R.id.siteSt);
		TextView  detector  = (TextView) view.findViewById(R.id.zoneDetector);
		TextView zoneSt = (TextView) view.findViewById(R.id.siteId);
		TextView destination = (TextView) view.findViewById(R.id.zoneDestination) ;
		TextView delay = (TextView) view.findViewById(R.id.zoneDelay) ;
		ImageView typeImage = (ImageView) view.findViewById(R.id.zoneSubTypeImage);

		/*arm status*/
		ImageView armImage = (ImageView) view.findViewById(R.id.zoneArmImage);
		if(null!= armImage){
			Section section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
			if(null!=section && section.detector == Func.SECTION_GUARD_TYPE && zone.detector != Func.RELAY_TYPE && zone.detector!= Func.AUTO_RELAY_TYPE){
				if(section.armed > 0){
					armImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_security_checked_green_500_big_48));
				}else{
					armImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_restriction_shield_grey_500_big));
				}
			}else{
				armImage.setVisibility(View.INVISIBLE);
			}
		}

		if(zone.detector == Func.AUTO_RELAY_TYPE){
			if(null!=typeImage) typeImage.setVisibility(View.VISIBLE);
			if(dbHelper.getScriptBindedStatusForRelay(zone)){
				armImage.setVisibility(View.VISIBLE);
				armImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_scripts_brand_48));
			}
		}else{
			if(null!=typeImage) typeImage.setVisibility(View.GONE);
		}

		if(null!=delay && zone.delay !=0){
			delay.setText(context.getString(R.string.TEXT_DELAY_BEFORE_ARM) + " " + (zone.delay == 0? zone.delay : "60") + " " + context.getString(R.string.SECONDS_COUNT));
		}else{
			delay.setVisibility(View.GONE);
		}

		name.setText(zone.name);

		/*physic&detector*/
		if(D3Service.d3XProtoConstEvent != null){
			DPhysic dPhysic = D3Service.d3XProtoConstEvent.getPhysicById(zone.physic);
			DType dType = D3Service.d3XProtoConstEvent.getDetectorType(zone.detector);
			if(null!=dType && null!=dPhysic)
			{
				detector.setText(dType.caption);
				physic.setText(dPhysic.caption);
			}else{
				physic.setVisibility(View.GONE);
				detector.setVisibility(View.GONE);
			}
		}else{
			physic.setVisibility(View.GONE);
			detector.setVisibility(View.GONE);
		}
		zoneSt.setText(R.string.EA_DISARMED_LONG);

		/*device&section*/
		if (destination != null)
		{
			String destinationText = "";

			if (device_id != -1)
			{
				if (section_id != -1)
				{
					destination.setVisibility(View.INVISIBLE);
				} else
				{
					destination.setVisibility(View.VISIBLE);
					destinationText += dbHelper.getSectionNameByIdWithOptions(zone.device_id, zone.section_id) + "(" + Integer.toString(zone.section_id)+ ")";
					destination.setText(destinationText);
				}
			} else
			{
				Device device = dbHelper.getDeviceById(zone.device_id);
				if(null!=device){
					destinationText += device.getName() + "(" + Integer.toString(device.account) + ")";
				}
				destination.setVisibility(View.VISIBLE);
				destinationText += "  • " +  dbHelper.getSectionNameByIdWithOptions(zone.device_id, zone.section_id) + "(" + Integer.toString(zone.section_id)+ ")";
				destination.setText(destinationText);
			}

		}


		/*show affects*/
		LinkedList<Event> affects = dbHelper.getAffectsByZoneId(site_id, zone.device_id, zone.section_id, zone.id);

		ImageView imageStatement = (ImageView) view.findViewById(R.id.siteImage);
		LinearLayout imageSmallStLayout = (LinearLayout) view.findViewById(R.id.smallIconLayout);
		if(imageSmallStLayout != null)
		{
			if (isExpanded)
			{
				imageSmallStLayout.setVisibility(View.INVISIBLE);
			} else
			{
				imageSmallStLayout.setVisibility(View.VISIBLE);
			}
		}
		ImageView[] imagesSmall = new ImageView[7];
		for (int i = 0; i < 7; i++)
		{
			imagesSmall[i] = new ImageView(context);
			imagesSmall[i].setLayoutParams(new LinearLayout.LayoutParams(Func.dpToPx(24, context), Func.dpToPx(24, context)));
		}

		if(zone.detector != Func.RELAY_TYPE && zone.detector != Func.AUTO_RELAY_TYPE)
		{
			//set model icon
			int ir = R.drawable.ic_sensor_brand_color;
			Device device = dbHelper.getDeviceById(zone.device_id);
			if(null!=device){
				if(device.canBeSetFromApp()){
					if(zone.id == 100 && zone.detector == 8)
					{
//						DeviceType deviceType = D3Service.d3XProtoConstEvent.getDeviceTypeById(device.type);
//						ir = deviceType.getMainIcon(context);
					}
					else{
						if(zone.uid_type !=0 && zone.section_id!=0){
							int model = 0;
							if(zone.uid_type != 0){
								int type = zone.uid_type >> 24;
								switch (type){
									case 1:
										model = zone.uid_type&0xff;
										ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(model);
										if(null!=zType){
											ir  = zType.getMainIcon(context);
										}
										break;
									case 2:
										break;
									case 4:
										break;
									case 5:
										break;
								}
							}

						}
					}
				}
			}

			imageStatement.setImageResource(ir);
			if (null != affects)
			{
				boolean excluded = false;

				LinkedList<Event> sectionAffects = dbHelper.getAffectsBySiteDeviceSectionId(site_id, zone.device_id, zone.section_id);
				if (null != sectionAffects)
				{
					for (Event sectionAffect : sectionAffects)
					{
						if ((sectionAffect.classId == 5) & (sectionAffect.reasonId == 27))
						{
							excluded = true;
						}
					}
				}

				int imgSmallNum = 0;
				int alarmI = 0;
				int sabI = 0;
				int malfI = 0;
				int warnI = 0;
				int armI = 0;
				int powI = 0;
				int gprsI = 0;
				int ethI = 0;
				int radioI = 0;

				for (Event affect : affects)
				{
					switch (affect.classId)
					{
						case 0:
							imageStatement.setImageResource(context.getResources().getIdentifier(affect.explainAffectIconBig(), null, context.getPackageName()));
							if (alarmI == 0)
							{
								alarmI++;
							}
							break;
						case 1:
							if (0 == sabI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								sabI++;
							}
							break;
						case 2:
							if (0 == malfI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								malfI++;
							}
							break;
						case 3:
							if (0 == warnI)
							{
								imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
								imgSmallNum++;
								warnI++;
							}
							break;
						case 4:
							if (!excluded)
							{
								if (0 == armI)
								{
									imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
									imgSmallNum++;
									armI++;
								}
								zoneSt.setText(R.string.EA_ARMED_LONG);
							}
							break;
						case 6:
							if (9 != affect.reasonId)
							{
								if (0 == powI)
								{
									imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
									imgSmallNum++;
									powI++;
								}
							}
							break;
						case 7:
							if (5 != affect.reasonId)
							{
								switch (affect.detectorId)
								{
									case 21:
										if (1 == affect.reasonId)
										{
											if (0 == gprsI)
											{
												imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
												imgSmallNum++;
												gprsI++;
											}
										}
										break;
									case 22:
										if (1 == affect.reasonId)
										{
											if (0 == ethI)
											{
												imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
												imgSmallNum++;
												ethI++;
											}
										}
										break;
									default:
										if (0 == radioI)
										{
											imagesSmall[imgSmallNum].setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
											imgSmallNum++;
											radioI++;
										}
										break;
								}
							}
							break;
					}
				}
				if (null != imageSmallStLayout)
				{
					for (int n = 0; n < imgSmallNum; n++)
					{
						imageSmallStLayout.addView(imagesSmall[n]);
					}
				}
			}
		}else{
			zoneSt.setText(R.string.STATEMENT_OFF);
			imageStatement.setImageResource(R.drawable.ic_lightbulb_off_outline_big);
			if(null!=affects){
				for(Event affect:affects){
					if(affect.classId == 5 && affect.reasonId ==6){
						imageStatement.setImageResource(R.drawable.ic_lightbulb_on_big);
						zoneSt.setText(R.string.STATEMENT_ON);
					}
				}
			}
		}

		/*part & sh & roles checkStates*/
		ImageView imageMore = (ImageView) view.findViewById(R.id.imageMore);
//		if(Func.getRemoteSetRights(userInfo, dbHelper, site_id, zone.device_id)){
			imageMore.setOnClickListener(new zoneCardMoreOnClick(zone, dbHelper));
//		}else
//		{
//			imageMore.setVisibility(View.GONE);
//		}

		/*set click listeners*/
		final TextView buttonMore = (TextView) view.findViewById(R.id.buttonMore);
		if(null!=buttonMore)
			buttonMore.setVisibility(View.GONE);
		final TextView buttonOn = (TextView) view.findViewById(R.id.buttonArm);
		final TextView buttonOff = (TextView) view.findViewById(R.id.buttonDisarm);
		final TextView buttonFireReset = (TextView) view.findViewById(R.id.buttonFireReset);
		final LinearLayout armDisarmLayout = (LinearLayout) view.findViewById(R.id.armDisarmLayout);
		final LinearLayout customButtonLayout = (LinearLayout) view.findViewById(R.id.customButtonsFrame);

		if(null!=armDisarmLayout && null!=customButtonLayout)
		{
			if (zone.detector != Func.RELAY_TYPE && zone.detector != Func.THERMOSTAT_TYPE)
			{
				armDisarmLayout.setVisibility(View.GONE);
				customButtonLayout.setVisibility(View.GONE);
				if(zone.detector == Func.AUTO_RELAY_TYPE){
					Script script = dbHelper.getDeviceScriptsByBind(zone.device_id, zone.id);
					if(null!=script)
					{
						String string = script.extra;
						if (null != string && script.enabled == 1)
						{
							try
							{
								JSONObject extraObject = new JSONObject(string);
								JSONObject switchObject = extraObject.getJSONObject("switch");
								if (null != switchObject)
								{
									int counter = switchObject.getInt("size");
									JSONArray rowsArray = switchObject.getJSONArray("rows");
									for (int i = 0; i < rowsArray.length(); i++)
									{
										JSONObject rowObject = rowsArray.getJSONObject(i);
										if (null != rowObject)
										{
											JSONArray items = rowObject.getJSONArray("items");
											if (null != items && items.length() > 0)
											{
												customButtonLayout.setVisibility(View.VISIBLE);
												visibleButtons = true;
												for (int i1 = 0; i1 < items.length(); i1++)
												{
													JSONObject item = items.getJSONObject(i1);
													String title = Func.getScriptDataForCurrLang(item.getString("content"), iso);
													final int value = item.getInt("value");
													LinearLayout linearLayout = (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.temp_linear_weight_wrap, null);
													LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
													linearLayout.setLayoutParams(params);
													TextView button = (TextView) ((Activity) context).getLayoutInflater().inflate(R.layout.template_switch_button, null);

//													button.setLayoutParams(params);
													button.setText(title);
													;
													button.setOnClickListener(new View.OnClickListener()
													{
														@Override
														public void onClick(View view)
														{
															if (dbHelper.isDeviceOnline(zone.device_id))
															{
																JSONObject commandAddress = new JSONObject();
																try
																{
																	commandAddress.put("section", zone.section_id);
																	commandAddress.put("zone", zone.id);
																	commandAddress.put("state", value);
																	int command_count = Func.getCommandCount(context);
																	D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "SWITCH", commandAddress, command_count);
																	if (d3Request.error == null)
																	{
																		sendMessageToServer(zone, d3Request, true);
																	} else
																	{
																		Func.pushToast(context, d3Request.error, (Activity) context);
																	}
																} catch (JSONException e)
																{
																	e.printStackTrace();
																}
															} else
															{
																Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
															}
														}
													});
													linearLayout.addView(button);
													customButtonLayout.addView(linearLayout);
												}
											}
										}
									}
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
						}
					}
				}
			}else{
				visibleButtons = true;
			}
		}
		if(null!=buttonFireReset){
			Section section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
			if(null!=section){
				if(section.detector == Func.SECTION_FIRE_TYPE || section.detector == Func.SECTION_FIRE_DOUBLE_TYPE){
					if(dbHelper.getAlarmStatusForZone(zone.device_id, zone.section_id, zone.id)){
						buttonFireReset.setVisibility(View.VISIBLE);
						visibleButtons = true;
					}
				}
			}
		}

		LinearLayout cardLinear = (LinearLayout) view.findViewById(R.id.cardLinearExp);
		View dividerLayout = (View) view.findViewById(R.id.dividerExp);

		/*change size of cards*/
		if(isExpanded){
			if(null!=cardLinear)
			{
				cardLinear.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
				if (!visibleButtons)
				{
					cardLinear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Func.dpToPx(153, context)));
				} else
				{
					cardLinear.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, Func.dpToPx(201, context)));
				}
			}
			if(null!=affects && 0!=affects.size())
				{
					if (null != dividerLayout)
					{
						dividerLayout.setVisibility(View.GONE);
					}
				}
		}

		if(null!=buttonOn)
		{
			if(sending ==0 )
			{
				buttonOn.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (dbHelper.isDeviceOnline(zone.device_id))
						{
							JSONObject commandAddress = new JSONObject();
							try
							{
								commandAddress.put("section", zone.section_id);
								commandAddress.put("zone", zone.id);
								commandAddress.put("state", 1);
								int command_count = Func.getCommandCount(context);
								D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "SWITCH", commandAddress, command_count);
								if (d3Request.error == null)
								{
									sendMessageToServer(zone, d3Request, true);
								} else
								{
									Func.pushToast(context, d3Request.error, (Activity) context);
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
						}
					}
				});
			}else{
				buttonOn.setEnabled(false);
			}
		}

		if(null!=buttonOff)
		{
			if(sending == 0)
			{
				buttonOff.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (dbHelper.isDeviceOnline(zone.device_id))
						{
							JSONObject commandAddress = new JSONObject();
							try
							{
								commandAddress.put("section", zone.section_id);
								commandAddress.put("zone", zone.id);
								commandAddress.put("state", 0);
								int command_count = Func.getCommandCount(context);
								D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "SWITCH", commandAddress, command_count);
								if (d3Request.error == null)
								{
									sendMessageToServer(zone, d3Request, true);
								} else
								{
									Func.pushToast(context, d3Request.error, (Activity) context);
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
						}
					}
				});
			}else{
				buttonOff.setEnabled(false);
			}
		}
		if(null!=buttonFireReset){
			if(sending == 0)
			{
				buttonFireReset.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if (dbHelper.isDeviceOnline(zone.device_id))
						{
							JSONObject commandAddress = new JSONObject();
							try
							{
								commandAddress.put("section", zone.section_id);
								commandAddress.put("zone", zone.id);
								int command_count = Func.getCommandCount(context);
								D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "RESET", commandAddress, command_count);
								if (d3Request.error == null)
								{
									sendMessageToServer(zone, d3Request, true);
								} else
								{
									Func.pushToast(context, d3Request.error, (Activity) context);
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
						}
					}
				});
			}else{
				buttonFireReset.setEnabled(false);
			}
		}
	}

	@Override
	protected void bindChildView(View view, Context context, Cursor cursor, boolean isLastChild)
	{
		TextView affectName = (TextView) view.findViewById(R.id.affectName);
		ImageView affectImage = (ImageView) view.findViewById(R.id.affectImage);
		Event affect = dbHelper.getAffect(cursor);
		affectName.setText(affect.explainAffectRegDescription(context));
		affectImage.setImageResource(context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
		View divider = (View) view.findViewById(R.id.childDivider);
		View padding = (View) view.findViewById(R.id.childPadding);
		if(isLastChild){
			divider.setVisibility(View.VISIBLE);
			padding.setVisibility(View.VISIBLE);
		}else{
			divider.setVisibility(View.GONE);
			padding.setVisibility(View.GONE);

		}
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		Cursor cursor = getGroup(groupPosition);
		View v = convertView;
		if (cursor != null)
		{
			if (convertView == null)
			{
				v = newGroupView(context, cursor, isExpanded, parent);
			} else
			{
				if ((isExpanded && convertView.getId() == collapsedGroupLayoutViewId) || (!isExpanded && convertView.getId() == expandedGroupLayoutViewId))
				{
					v = newGroupView(context, cursor, isExpanded, parent);
				}
			}
			bindGroupView(v, context, cursor, isExpanded);
		}
		return v;
	}

	@Override
	public void notifyDataSetChanged() {
		notifyDataSetChanged(false);
	}

	private class zoneCardMoreOnClick implements View.OnClickListener
	{
		private final ZonesActivity activity;
		private Zone zone;
		private DBHelper dbHelper;

		public zoneCardMoreOnClick(Zone zone, DBHelper dbHelper)
		{
			this.zone = zone;
			this.dbHelper = dbHelper;
			activity  = (ZonesActivity) context;
		}

		@Override
		public void onClick(View v)
		{
			final UserInfo userInfo = dbHelper.getUserInfo();
			final AlertDialog.Builder deviceMenuDialogBuilder = Func.adbForCurrentSDK(context);
			deviceMenuDialogBuilder.setTitle(zone.name);

			final String[] menuItemsArray = context.getResources().getStringArray(R.array.zone_menu_values);
			List<String> menuItemsList = new ArrayList<>();
			menuItemsList.add(menuItemsArray[0]);

			if(zone.section_id == 0 && zone.id == 100){
				if(Func.getRemoteSetRights(userInfo, dbHelper, site_id, zone.device_id)){
					menuItemsList.add(menuItemsArray[1]);
				}
				if(0==(userInfo.roles&Const.Roles.TRINKET))
				{
					menuItemsList.add(menuItemsArray[3]);
				}
			}else
			{
				if((zone.detector == Func.AUTO_RELAY_TYPE)
						&& (0==(userInfo.roles& Const.Roles.TRINKET))
//						&& ((!context.getPackageName().equals("public.shub"))||(sp.getString("prof_language_value", "ru").equals("ru")))
				){
					if(Func.needScriptsForBuild())
					{
						if (dbHelper.getScriptBindedStatusForRelay(zone))
						{
							menuItemsList.add(menuItemsArray[9]);
						} else
						{
							if (Func.getRemoteSetRights(userInfo, dbHelper, site_id, zone.device_id))
							{
								menuItemsList.add(menuItemsArray[8]);
							}
						}
					}
				}
				if(Func.getRemoteSetRights(userInfo, dbHelper, site_id, zone.device_id)){
					menuItemsList.add(menuItemsArray[1]);
					Section section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
					if(section.detector == Func.SECTION_GUARD_TYPE
							&& zone.detector != Func.RELAY_TYPE
							&& zone.detector != Func.AUTO_RELAY_TYPE
							&& zone.detector != Func.LAMP_TYPE
							&& zone.detector != Func.SIREN_TYPE
							&& zone.detector != Func.THERMOSTAT_TYPE
							&& zone.detector !=Func.SZO_TYPE)
						if(zone.delay == 0){
							menuItemsList.add(menuItemsArray[4]);
						}else{
							menuItemsList.add(menuItemsArray[5]);
						}
					menuItemsList.add(menuItemsArray[2]);
//					menuItemsList.add(menuItemsArray[6]);
				}
				if(zone.detector == Func.THERMOSTAT_TYPE){
					menuItemsList.add(menuItemsArray[7]);
				}


			}

			final CharSequence[] items = menuItemsList.toArray(new CharSequence[menuItemsList.size()]);
			deviceMenuDialogBuilder.setItems(items, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(final DialogInterface dialog, int which)
				{
					String curValue = items[which].toString();
					if(curValue.equals(menuItemsArray[0])){showInfoDialog(zone);}
					else if(curValue.equals(menuItemsArray[1])){showRenameDialog(zone);}
					else if(curValue.equals(menuItemsArray[2])){showDeleteDialog(zone);}
					else if(curValue.equals(menuItemsArray[3])){showUSSDDialog(zone);}
					else if(curValue.equals(menuItemsArray[4])){showDelayDialog(zone, 1);}
					else if(curValue.equals(menuItemsArray[5])){showDelayDialog(zone, 0);}
					else if(curValue.equals(menuItemsArray[6])){showChangeSectionDialog(zone);}
					else if(curValue.equals(menuItemsArray[7])){showTempSetDialog(zone);}
					else if(curValue.equals(menuItemsArray[8])){
						if (dbHelper.isDeviceOnline(zone.device_id))
						{
							if (0 == dbHelper.getSiteDeviceArmStatus(site_id, zone.device_id))
							{
								getLibraryScripts(true, zone);
							} else
							{
								Func.nShowMessage(context, context.getString(R.string.SCRIPTS_ERROR_ARMED));
							}
						} else
						{
							Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
						}
					}
					else if(curValue.equals(menuItemsArray[9])){
						getLibraryScripts(false, zone);}
					dialog.dismiss();
				}
			});

			deviceMenuDialogBuilder.show();
		}
	}

	public void getLibraryScripts(boolean act, Zone zone)
	{
		this.act = act;
		this.zone = zone;
		((Activity)context).runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				materialDialog = new MaterialDialog.Builder(context)
						.title(R.string.DIALOG_WAIT_TITLE)
						.content(R.string.SCRIPTS_LIB_DOWNLOAD)
						.progress(true, 0)
						.show();
			}
		});
		wait = true;
		sendScriptsRequest(zone.device_id);
		timer = new Timer();
		timer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				wait = false;
				context.sendBroadcast(new Intent(D3Service.BROADCAST_LIBRARY_SCRIPTS));
			}
		}, 15000);
	}

	private void sendScriptsRequest(int device_id)
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		Device device  = dbHelper.getDeviceById(device_id);
		JSONObject message = new JSONObject();
		try
		{
			message.put("config_version", device.config_version);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		D3Request d3Request = D3Request.createMessage(userInfo.roles, "LIBRARY_SCRIPTS", message);
		if(d3Request.error == null){
			sendMessageToServer(device, d3Request, false);
		}else{
			wait = false;
			context.sendBroadcast(new Intent(D3Service.BROADCAST_LIBRARY_SCRIPTS));
			Func.pushToast(context, d3Request.error, (Activity) context);
		}
	}

	private void openDeviceScript(Zone zone)
	{
		int script_id = dbHelper.getBindedScriptIdForRelay(zone);
		if(-1 != script_id)
		{
			Intent deviceScriptsIntent = new Intent(context, ScriptActivity.class);
			deviceScriptsIntent.putExtra("script_id", script_id);
			deviceScriptsIntent.putExtra("site_id", site_id);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
			{
				// Apply activity transition
				Activity activity = (Activity) context;
				context.startActivity(deviceScriptsIntent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
			} else
			{
				// Swap without transition
				context.startActivity(deviceScriptsIntent);
				((Activity) context).overridePendingTransition(R.animator.from_down_enter, R.animator.from_down_exit);
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.SCRIPT_SET_ERROR));
		}
	}

	private void openScriptsLibrary(Zone zone){
		Intent intent = new Intent(context, ScriptsLibraryActivity.class);
		intent.putExtra("device_id", zone.device_id);
		intent.putExtra("site_id", site_id);
		intent.putExtra("bind", zone.id);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			// Apply activity transition
			context.startActivity(intent, ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle());
		} else
		{
			// Swap without transition
			context.startActivity(intent);
			((Activity)context).overridePendingTransition(R.animator.from_down_enter, R.animator.from_down_exit);
		}
	}

	private void showInfoDialog(Zone zone)
	{
		AlertDialog.Builder infoDialogBuilder = Func.adbForCurrentSDK(context);
		infoDialogBuilder.setTitle(R.string.INFORMATION);
		String message = "\n";
		message += ((zone.section_id == 0 && zone.id == 100) ? context.getString(R.string.ZONE_INFO_DEVICE_NAME) : context.getString(R.string.ZONE_INFO_ZONE_NAME)) + " " + zone.name + "(" + zone.id + ")\n";
		message += "\n";
		message += "\n";
		int model = 0;
		if(zone.uid_type != 0){
			int type = zone.uid_type >> 24;
			model = zone.uid_type&0xfffff;
			switch (type){
				case 1:
					break;
				case 2:
					break;
				case 4:
					break;
				case 5:
					break;
			}
		}
		ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(model);
		message += context.getString(R.string.DEVICE_INFO_MODEL) + (zone.uid_type !=0 ?
				zType!= null? zType.caption: context.getString(R.string.DEVICE_INFO_UNKNOWN) :
				context.getString(R.string.DEVICE_INFO_UNKNOWN));
		message += "\n";
		message += "\n";
		message += context.getString(R.string.ZONE_INFO_TYPE) + "\n";
		if(D3Service.d3XProtoConstEvent != null){
			DPhysic dPhysic = D3Service.d3XProtoConstEvent.getPhysicById(zone.physic);
			DType dType = D3Service.d3XProtoConstEvent.getDetectorType(zone.detector > 100 ? Character.digit(Integer.toString(zone.detector, 16).toCharArray()[Integer.toString(zone.detector, 16).toCharArray().length - 1], 16) : zone.detector);

			if(null!=dType && null!=dPhysic)
			{
				message += dType.caption  + "\n";
			}
			if(zone.detector >100){
//				char[] characters = Integer.toString(zone.detector).toCharArray();
//				String s = Integer.toString(zone.detector);
				String s = Integer.toString(zone.detector, 16);
				int detector = Integer.valueOf(s.substring(2), 16);
				int alarm = Integer.valueOf(s.substring(0,s.length() - 2), 16);
				AType aType = D3Service.d3XProtoConstEvent.getAlarmType(alarm);
				dType = D3Service.d3XProtoConstEvent.getDetectorType(detector);
				if(null!=aType){
					message +=aType.caption + "\n";
				}
			}

			if(null!=dPhysic){
				message += "\n" + dPhysic.caption + "\n";
			}
		}
		message += "\n";

		Device device = dbHelper.getDeviceById(zone.device_id);
		if(null!=device){
			message += context.getString(R.string.ZONE_INFO_DEVICE) +  device.getName() + "(" + Integer.toString(device.account) + ")" + "\n";
		}

		message += context.getString(R.string.ZONE_INFO_SECTION) + dbHelper.getSectionNameByIdWithOptions(zone.device_id, zone.section_id) + "(" + zone.section_id + ")" + "\n";

		infoDialogBuilder.setMessage(message);
		infoDialogBuilder.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		infoDialogBuilder.show();
	}

	private void showRenameDialog(final Zone zone)
	{
		if (dbHelper.isDeviceOnline(zone.device_id))
		{
			if (0 == dbHelper.getSiteDeviceArmStatus(site_id, zone.device_id))
			{
				AlertDialog.Builder renameDialogBuilder = Func.adbForCurrentSDK(context);
				renameDialogBuilder.setTitle(R.string.RENAME);

				LinearLayout parentLayout = new LinearLayout(context);
				parentLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
				parentLayout.setOrientation(LinearLayout.VERTICAL);
				parentLayout.setPadding(Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context), Func.dpToPx(12, context));

				final EditText editName = new EditText(context);
				editName.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				editName.setCursorVisible(true);
				editName.isCursorVisible();
				try
				{
					Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
					f.setAccessible(true);
					f.set(editName, R.drawable.caa_cursor);
				} catch (Exception ignored)
				{
				}
				editName.setGravity(Gravity.CENTER);
				editName.setTextColor(context.getResources().getColor(R.color.md_black_1000));
				editName.setText(zone.name);
				editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
				int editNameMaxLength = 32;
				editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
				editName.requestFocus();

				parentLayout.addView(editName);
				renameDialogBuilder.setView(parentLayout);

				renameDialogBuilder.setMessage(R.string.TA_ENTER_NEW_NAME);
				renameDialogBuilder.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{

					}
				});
				renameDialogBuilder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});

				final AlertDialog renameDialog = renameDialogBuilder.create();
				renameDialog.show();
				renameDialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						if (!editName.getText().toString().matches(""))
						{
							String newName = editName.getText().toString();
							//DO CHANGE NAME

							JSONObject commandAddr = new JSONObject();
							try
							{
								commandAddr.put("section", zone.section_id);
								commandAddr.put("index", zone.id);
								commandAddr.put("name", newName);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							int command_count = Func.getCommandCount(context);
							D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "ZONE_SET", commandAddr, command_count);
							if (d3Request.error == null)
							{
								if (sendMessageToServer(zone, d3Request, true))
								{
									Func.hideKeyboard((Activity) context);
									editName.clearFocus();
									renameDialog.dismiss();
								}
							} else
							{
								Func.pushToast(context, d3Request.error, (Activity) context);
							}
						} else
						{
							Func.pushToast(context, context.getString(R.string.DELA_INCORRECT_NAME_PUSHTOAST), (Activity) context);
						}
					}
				});
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.MESSAGE_DISARM_TO_CHANGE_ZONE_NAME));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showDeleteDialog(final Zone zone)
	{
		if (dbHelper.isDeviceOnline(zone.device_id))
		{
			if (0 == dbHelper.getSiteDeviceArmStatus(site_id, zone.device_id))
			{
				AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
				adb.setTitle(R.string.DELETING);
				adb.setMessage(context.getResources().getString(R.string.ZTA_DELETE_MESS_1) + " " + zone.name + " " + context.getResources().getString(R.string.ZTA_DELETE_MESS_2));
				adb.setPositiveButton(R.string.DELETE, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						if (dbHelper.getZoneRegisterStatus(zone))
						{
							JSONObject commandAddr = new JSONObject();
							try
							{
								commandAddr.put("section", zone.section_id);
								commandAddr.put("index", zone.id);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							int command_count = Func.getCommandCount(context);
							D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "ZONE_DEL", commandAddr, command_count);
							if (d3Request.error == null)
							{
								if (sendMessageToServer(zone, d3Request,true))
								{
									dialog.dismiss();
								}
							} else
							{
								Func.pushToast(context, d3Request.error, (Activity) context);
							}
						} else
						{
							JSONObject message = new JSONObject();
							try
							{
								message.put("device_id", zone.device_id);
								message.put("device_section", zone.section_id);
								message.put("zone", zone.id);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							D3Request d3Request = D3Request.createMessage(userInfo.roles, "ZONE_DEL", message);
							if (d3Request.error == null)
							{
								sendMessageToServer(zone, d3Request, false);
								dialog.dismiss();
								Func.hideKeyboard((Activity) context);
							} else
							{
								Func.pushToast(context, d3Request.error, (Activity) context);
							}
						}

					}
				});
				adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
				adb.show();
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.OBJECT_DISARM_MESSAGE_ZONE_DEL));
			}
		}
		else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showUSSDDialog(final Zone zone)
	{
		AlertDialog.Builder ussdRequestDialog = Func.adbForCurrentSDK(context);
		AlertDialog addDevDialog = ussdRequestDialog.create();
		addDevDialog.setTitle(R.string.USSD_DIALOG_TITLE);
		addDevDialog.setMessage(context.getString(R.string.USSD_DIALOG_MESSAGE));
		FrameLayout frameLayout = (FrameLayout) new FrameLayout(context);
		frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		frameLayout.setPadding(Func.dpToPx(20, context), 0, Func.dpToPx(20, context), 0);
		final EditText editText  = new EditText(context);
		String ussd = "*100#";
		String imsi = dbHelper.getCurrectDeviceImsi(zone.device_id);
		if((null!=imsi)&&(!imsi.equals(""))){
			ussd = Func.getUSSD(Integer.valueOf(imsi.substring(0,8)));
		}
		editText.setText(ussd);
		try
		{
			Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
			f.setAccessible(true);
			f.set(editText, R.drawable.caa_cursor);
		} catch (Exception ignored)
		{
		}
		editText.setCursorVisible(true);
		editText.isCursorVisible();
		editText.setTextColor(context.getResources().getColor(R.color.md_black_1000));
		editText.setGravity(Gravity.CENTER);
		editText.requestFocus();

		frameLayout.addView(editText);
		addDevDialog.setButton(Dialog.BUTTON_POSITIVE, context.getString(R.string.ADB_USSD), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				String request = editText.getText().toString();
				if((request!=null)&&(request.length()!=0))
				{
					JSONObject commandAddr = new JSONObject();
					try
					{
						commandAddr.put("text", request);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					int command_count = Func.getCommandCount(context);
					D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "USSD", commandAddr, command_count);
					if (d3Request.error == null)
					{
						if (sendMessageToServer(zone, d3Request, true))
						{
							dialog.dismiss();
						}
					} else
					{
						Func.pushToast(context, d3Request.error, (Activity) context);
					}
				}else{
					Func.pushToast(context, context.getString(R.string.ENTER_USSD), (Activity) context);
				}
			}
		});
		addDevDialog.setButton(Dialog.BUTTON_NEGATIVE, context.getString(R.string.ADB_CANCEL), new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		addDevDialog.setView(frameLayout);
		addDevDialog.show();
	}


	private void showDelayDialog(final Zone zone, final int position)
	{
		if (dbHelper.isDeviceOnline(zone.device_id))
		{
			if (0 == dbHelper.getSiteDeviceArmStatus(site_id, zone.device_id))
			{
				AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
				adb.setTitle(R.string.ZONE_DELAY_SET_DIALOG_TITLE);
				adb.setMessage(position == 1 ? context.getString(R.string.ZONE_DELAY_SET_DIALOG_MESS_1) + " " + zone.name + "?" : context.getString(R.string.ZONE_DELAY_SET_DIALOG_MESS_2) + " " + zone.name + "?");
				adb.setPositiveButton(position == 1 ? R.string.TURN_ON : R.string.TURN_OFF, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						JSONObject commandAddr = new JSONObject();
						try
						{
							commandAddr.put("section", zone.section_id);
							commandAddr.put("index", zone.id);
							commandAddr.put("delay", position);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						int command_count = Func.getCommandCount(context);
						D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "ZONE_SET", commandAddr, command_count);
						if (d3Request.error == null)
						{
							if (sendMessageToServer(zone, d3Request, false))
							{
								dialog.dismiss();
							}
						} else
						{
							Func.pushToast(context, d3Request.error, (Activity) context);
						}
					}
				});
				adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
				adb.show();
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.OBJECT_DISARM_MESSAGE_ZONE_DEL));
			}
		}
		else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showChangeSectionDialog(final Zone zone){
		if (dbHelper.isDeviceOnline(zone.device_id))
		{
			if (0 == dbHelper.getSiteDeviceArmStatus(site_id, zone.device_id))
			{
				AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
				adb.setTitle(R.string.ZONE_MOVE_SECTION_TITLE);
				adb.setMessage(R.string.ZONE_MOVESECTION_DIALOG_MESS);

				View view = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_with_section_spinner, null);
				Spinner sectionsSpinner = view.findViewById(R.id.wizSectionSpinner);
				Section[] sections = (zone.detector == Func.SIREN_TYPE || zone.detector == Func.LAMP_TYPE || zone.detector == Func.SZO_TYPE) ?
						dbHelper.getAllSectionsForSiteDeviceWithControllerNoCurrent(site_id, device_id, zone.section_id) :
						dbHelper.getSectionsForSiteDeviceWithoutControllerAndCurrentKnownType(site_id, zone.device_id, zone.section_id);
				if((sections!=null)&&(sections.length > 0))
				{
					final WizardSectionsSpinnerAdapter sectionsSpinnerAdapter = new WizardSectionsSpinnerAdapter(context, R.layout.spinner_item_dropdown, sections);
					sectionsSpinner.requestLayout();
					sectionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
					{
						@Override
						public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
						{
							Section section = sectionsSpinnerAdapter.getItem(position);
							setSectionId = section.id;
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent)
						{
							Section section = sectionsSpinnerAdapter.getItem(0);
							setSectionId = section.id;
						}
					});
					sectionsSpinner.setAdapter(sectionsSpinnerAdapter);
				}
				adb.setView(view);

				adb.setPositiveButton(R.string.ADB_MOVE, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						JSONObject commandAddr = new JSONObject();
						try
						{
							commandAddr.put("section", setSectionId);
							commandAddr.put("index", zone.id);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						int command_count = Func.getCommandCount(context);
						D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "ZONE_SET", commandAddr, command_count);
						if (d3Request.error == null)
						{
							if (sendMessageToServer(zone, d3Request, true))
							{
								dialog.dismiss();
							}
						} else
						{
							Func.pushToast(context, d3Request.error, (Activity) context);
						}
					}
				});
				adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
				adb.show();
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.MESSAGE_DISARM_TO_MOVE_ZONE));
			}
		}
		else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	private void showTempSetDialog(final Zone zone)
	{
		if (dbHelper.isDeviceOnline(zone.device_id))
		{
			if (0 == dbHelper.getSiteDeviceArmStatus(site_id, zone.device_id))
			{
				AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
				adb.setTitle(R.string.TEMP_SET_TITLE);
				adb.setMessage(R.string.TEMP_SET_MESSAGE);

				View view = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_with_temperature_set, null);
				final EditText editTemp = (EditText) view.findViewById(R.id.temperatureValueEdit);
				adb.setView(view);

				adb.setPositiveButton(R.string.ADB_MOVE, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						if(null!=editTemp)
						{
							if(!editTemp.getText().toString().equals(""))
							{
								int temp = Integer.valueOf(editTemp.getText().toString());
								JSONObject commandAddress = new JSONObject();
								try
								{
									commandAddress.put("section", zone.section_id);
									commandAddress.put("zone", zone.id);
									commandAddress.put("value", temp);
									int command_count = Func.getCommandCount(context);
									D3Request d3Request = D3Request.createCommand(userInfo.roles, zone.device_id, "SWITCH", commandAddress, command_count);
									if (d3Request.error == null)
									{
										sendMessageToServer(zone, d3Request, true);
									} else
									{
										Func.pushToast(context, d3Request.error, (Activity) context);
									}
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
							}else{
								Func.pushToast(context, context.getString(R.string.TEMP_SET_ERROR_NOO_ENTER), (Activity) context);
							}
						}
					}
				});
				adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
				adb.show();
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.MESSAGE_DISARM_TO_MOVE_ZONE));
			}
		}
		else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
		}
	}

	public boolean sendMessageToServer(D3Element d3Element, D3Request d3Request, boolean command)
	{
//		return  Func.sendMessageToServer(d3Element, d3Request, getService(), context, command, sending);
		return  false;
	};

	public D3Service getService()
	{
		Activity activity = (ZonesActivity)context;
		if(activity != null){
			ZonesActivity zonesActivity = (ZonesActivity) activity;
			return zonesActivity.getLocalService();
		}
		return null;
	}
}
