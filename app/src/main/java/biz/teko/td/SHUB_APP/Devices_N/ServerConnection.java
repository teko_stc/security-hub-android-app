package biz.teko.td.SHUB_APP.Devices_N;

import org.json.JSONObject;

import biz.teko.td.SHUB_APP.D3DB.D3Element;

public interface ServerConnection {
    boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command);
}
