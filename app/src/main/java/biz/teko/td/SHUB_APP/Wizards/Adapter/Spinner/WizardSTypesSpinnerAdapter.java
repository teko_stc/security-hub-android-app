package biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class WizardSTypesSpinnerAdapter extends ArrayAdapter<SType>
{
	private final Context context;
	private final LinkedList<SType> sTypes;

	public WizardSTypesSpinnerAdapter(@NonNull Context context, int resource, LinkedList<SType> sTypes)
	{
		super(context, resource, sTypes);
		this.context = context;
		this.sTypes = sTypes;
	}

	public int getCount(){
		return sTypes.size();
	}

	public SType getItem(int position){
		return sTypes.get(position);
	}

	public long getItemId(int position){
		return position;
	}

	@Override
	public int getPosition(@Nullable SType item)
	{
		return super.getPosition(item);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		label.setText(sTypes.get(position).caption);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		label.setText(sTypes.get(position).caption);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}
}
