package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.widget.ResourceCursorAdapter;

import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NMainBarElement;

public class NSiteScriptsCursorAdapter extends ResourceCursorAdapter
{
	private OnElementClickListener onElementClickListener;

	public void setOnElementClickListener(OnElementClickListener onElementClickListener){
		this.onElementClickListener = onElementClickListener;
	}

	public interface OnElementClickListener{
		void onClick(int device_id, int script_id);
	}

	private final Context context;
	private final DBHelper dbHelper;

	public NSiteScriptsCursorAdapter(Context context, int layout, Cursor c, int flags)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
	}


	@Override
	public void bindView(View view, Context context, Cursor cursor)
	{

		Script script = new Script(cursor);
		if(null!=script){
			NMainBarElement element  = view.findViewById(R.id.nScriptElement);
			element.setTitle(script.name);
			Zone relay = dbHelper.getZoneById(script.device, script.bind);
			if(null!=relay){
				element.setSubtitle(relay.name);
				if(relay.getModelId() != Const.SENSORTYPE_WIRED_OUTPUT){
					if(dbHelper.getRelayStatusById(relay.device_id, relay.section_id, relay.id) == Const.STATUS_ON){
						element.setOn(true);
					}else {
						element.setOff(true);
					}
				}
			}
			int icon = R.drawable.n_image_script_common_main_selector;
			/*select icon for script, need backend*/
			element.setImageBig(icon);
			element.setIncrease(false);
			element.setOnElementClickListener(new NMainBarElement.OnElementClickListener()
			{
				@Override
				public void onClick()
				{
					if(null!=onElementClickListener) onElementClickListener.onClick(script.device, script.id);
				}

				@Override
				public void onLongClick()
				{

				}
			});
			element.setEnabled(script.enabled == 1);
		}
	}

//	@Override
//	public int getItemCount() {
//		return cursor.getCount();
//	}
//
//	public interface OnElementClickListener{
//		void onClick(int device_id, int script_id);
//	}
//	private final Cursor cursor;
//	private final DBHelper dbHelper;
//
//	public NSiteScriptsCursorAdapter(Context context, Cursor cursor)
//	{
//		super();
//		this.dbHelper = DBHelper.getInstance(context);
//		this.cursor = cursor;
//	}

}
