package biz.teko.td.SHUB_APP.Profile.Models;

public class APN {
    private String server;
    private String login;
    private String password;

    public APN(String server, String login, String password) {
        this.server = server;
        this.login = login;
        this.password = password;
    }

    public String getServer() {
        return server;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }
}
