package biz.teko.td.SHUB_APP.D3;

import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.Spanned;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import org.apache.commons.lang.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidParameterSpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

import biz.teko.td.SHUB_APP.Activity.PopupActivity;
import biz.teko.td.SHUB_APP.Activity.StartUpActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.BuildConfig;
import biz.teko.td.SHUB_APP.D3.D3UniConfig.UniConfig;
import biz.teko.td.SHUB_APP.D3.D3XProto.D3XProtoConst;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.D3XProtoConstEvent;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.D3DB.Alarm;
import biz.teko.td.SHUB_APP.D3DB.Cluster;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.DBNewEventReceiver;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.LocaleD3;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.User;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Events.Activities.NFullScreenNotifActivity;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Receivers.D3ConnectionDropReceiver;
import biz.teko.td.SHUB_APP.Receivers.D3NewProtocolVersionReceiver;
import biz.teko.td.SHUB_APP.Receivers.WidgetUpdateReceiver;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.SecCompanies.Models.Sec;
import biz.teko.td.SHUB_APP.Utils.ActivityLifecycleCall;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.NotificationUtils;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Widgets.NormalWidget;
import io.reactivex.subjects.ReplaySubject;
import kotlin.jvm.Synchronized;

/**
 * Created by td13017 on 24.01.2017.
 */
public class D3Service extends Service
{
	public static final int OPEN_FROM_RESET_PIN = 1;
	public static final int OPEN_HISTORY_FROM_STATUS_BAR = 11;
	public static final int OPEN_FROM_STARTUP_ACTIVITY_AFTER_LOGIN = 13;
	public static final int OPEN_FROM_ADD_PIN = 15;
	public static final int SERVICE_BEEN_STOPED = 16;
	public static final int CONNECTION_DROP = 17;
	public static final int NEW_PROTOCOL_VERSION = 18;
	public static final int OPEN_FROM_LOGIN = 19;
	public static final int OPEN_FROM_ADD_PIN_AND_HISTORY_STATUS_BAR = 20;
	public static final int ADD_ZONE_IN_SECTION = 101;
	public static final int ADD_SECTION_IN_DEVICE = 301;
	public static final int ADD_NEW_DEVICE = 201;
	public static final int IV_LOGIN = 701;
	public static final int CHOOSE_LANGUAGE = 801;
	public static final int CHOOSE_MASTER_MODE = 802;
	public static final int BIND_CAMERAS = 803;

	public static final int EVENTS_REQUEST_ID = 1;
	public static final int AFFECTS_REQUEST_ID = 2;
	public static final int SHOW_NEW_EVENTS = 120;
	public static final int ADD_NEW_SCRIPT = 130;
	public static final int EDIT_SCRIPT = 131;

	private static final String SECURITY_HUB_CHANNEL = "All messages";
	private static final int SYSTEM_NOTIFICATION_ID = 1000;
	private static final int EVENT_NOTIFICATION_ID = 1001;
	private static final int SYSTEM_NOTIFICATIONS_GROUP_ID = 1002;
	private static final int EVENTS_NOTIFICATION_GROUP_ID = 1003;
	private static final int SERVICE_NOTIFICATION_ID = 1004;
	private static final int SERVICE_NOTIFICATION_GROUP_ID = 1005;
	private static final int CONNECTION_DROP_NOTIFICATIONS_ID = 1010;
	private static final String NOTIFICATION_GROUP = "Events Group";
	private static final String SYSTEM_NOTIFICATION_GROUP = "System events Group";
	private static final String SERVICE_NOTIFICATION_GROUP = "Service events Group";


	public static D3XProtoConst d3XProtoConst;
	public static D3XProtoConstEvent d3XProtoConstEvent;
	public static UniConfig uniConfig;
//	public static Faq faq;
	public static Sec sec;

	public static final String BROADCAST_LIBRARY_SCRIPTS = "Broadcast get scripts from libraray";
	public static final String BROADCAST_SCRIPT_SET = "Broadcast script set result";
	public static final String BROADCAST_SCRIPT_DEL = "Broadcast script delete result";
	public static final String BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV = "Broadcast get scripts list udpate";
	public static final String BROADCAST_GET_DEVICE_SCRIPTS = "Broadcast get scripts for device";
	public static final String BROADCAST_DELETE_DEVICE_SCRIPT = "Broadcast ipc delete script for device";
	public static final String BROADCAST_EVENT = "Registered new event";
	public static final String BROADCAST_TO_DELETE_OLD_EVENTS = "Delete old events";
	public static final String BROADCAST_SITES = "Get sites";
	public static final String BROADCAST_SITE_SECTION_ZONE_UPD_RMV = "Sites list been changed";
	public static final String BROADCAST_AFFECT = "Registered new affect";
	public static final String BROADCAST_GET_NEW_ELEMENTS = "Get data about site";
	public static final String BROADCAST_NEW_STATEMENT = "Registered statement changes";
	public static final String BROADCAST_D3SERVICE_ALIVE = "D3Service is alive";
	public static final String D3SERVICE_KEEP_ALIVE_SEND = "KEEP_ALIVE_SEND";
	public static final String BROADCAST_EVENT_REQUESTS_RESPONCE_GET = "Get response for events request";
	public static final String BROADCAST_SITE_STATEMENTS = "Get statemets info";
	public static final String BROADCAST_SITE_NOT_READY = "Site not ready to arm" ;
	public static final String BROADCAST_GET_SITE_BATTERY_OR_GPRS = "Get new value of battery level or gprs conn level" ;
	public static final String BROADCAST_GET_DEVICE_BATTERY = "Get new value of battery for zone";
	public static final String BROADCAST_OPERATORS = "Get new opertors list";
	public static final String BROADCAST_WHEN_CANT_RELOGIN = "User can't login";
	public static final String BROADCAST_HANDSHAKE_OK = "Handshake successful";
	public static final String BROADCAST_CONNECTION_DROP = "Connection been dropped, logout and close all" ;
	public static final String BROADCAST_DEVICE_ASSIGN_DOMAIN = "New site been added";
	public static final String BROADCAST_OPERATOR_UPD = "Operator's data been changed IPC";
	public static final String BROADCAST_OPERATOR_SET = "Operator's data been changed";
	public static final String BROADCAST_OPERATOR_RMV = "Operator been deleted ICP";
	public static final String BROADCAST_OPERATOR_DEL = "Operator been deleted";
	public static final String BROADCAST_ZONE_DEL = "Zone been deleted";
	public static final String BROADCAST_CANT_ZONE_DELETE = "Zone cant been delete" ;
	public static final String BROADCAST_CONNECTION_STATUS_CHANGE = "Connection been repaired";
	public static final String BROADCAST_NEW_PROTOCOL_VERSION = "New protocol version";
	public static final String BROADCAST_COMMAND_RESULT_UPD = "Get command result";
	public static final String BROADCAST_COMMAND_BEEN_SEND = "Command been sendEx";
	public static final String BROADCAST_UPDATE_WIDGETS = "biz.teko.td.SHUP_APP.UPDATE_WIDGETS";
	public static final String BROADCAST_LOGIN_ERROR = "Authorization error";
	public static final String BROADCAST_CONNECTION_ERROR_REPEATED = "Repeated connection error";
	public static final String BROADCAST_DELEGATE_NEW = "Delegation code for site";
	public static final String BROADCAST_DELEGATE_REQ = "Delegation complete";
	public static final String BROADCAST_SCAN_RESULT = "Scan result value";
	public static final String BROADCAST_ZONE_SET = "Zone been set";
	public static final String BROADCAST_ZONE_ADD = "Zone been added";
	public static final String BROADCAST_SECTION_ADD = "Broadcast when get new section";
	public static final String BROADCAST_SITE_SET = "Broadcast get site set result";
	public static final String BROADCAST_POPUP = "Show popup";
	public static final String BROADCAST_OPERATOR_SAFE_SESSION = "Broadcast result of safe session getting";
	public static final String BROADCAST_IV_BIND_CAM = "Broadcast result of camera zone binding";
	public static final String BROADCAST_IV_GET_RELATIONS = "Broadcast get cameras relations";
	public static final String BROADCAST_IV_GET_TOKEN = "Broadcast get iv token";
	public static final String BROADCAST_NEW_RECORD = "Got new record";
	public static final String BROADCAST_IV_DEL_USER = "Broadcast IV token delete request";
	public static final String BROADCAST_IV_DEL_RELATION = "Get result to del relations";
	public static final String BROADCAST_SITE_DEL = "Broadcast site delete result";
	public static final String BROADCAST_DELEGATE_REVOKE_RESULT = "Broadcast get result of delegation revoke";
	public static final String BROADCAST_SITE_DELEGATES = "Broadcast updated site delegated list";
	public static final String BROADCAST_ROLES_GET = "Broadcast get new roles";
	public static final String BROADCAST_DEVICE_ASSIGN_SITE = "Broadcast when new devices been assigned to site";
	public static final String BROADCAST_DEVICE_USER_UPD = "Broadcast update device users";
	public static final String BROADCAST_DEVICE_USER_SET = "Broadcast device users set result";
	public static final String BROADCAST_DEVICE_ADD = "Broadcast device added";
	public static final String BROADCAST_DATA_FROM_SERVER_FETCH = "Broadcast device info fetching";
	public static final String BROADCAST_DEVICE_RMV = "Broadcast device removed";
	public static final String BROADCAST_DEVICE_VERSION_UPDATE = "Broadcast device version update";
	public static final String BROADCAST_INTERACTIVE_STATUS = "Broadcast  send interactive status";
	public static final String BROADCAST_USER_ADD = "Broadcast when new user add";
	public static final String BROADCAST_DEVICE_USER_RMV = "Broadcast when user delete";
	public static final String BROADCAST_DEVICE_FORCE_ASSIGN = "Broadcast when adiign new device to site";
	public static final String BROADCAST_DEVICE_REVOKE_DOMAIN = "Broadcast when device revoke from domain";
	public static final String BROADCAST_GET_UDP_SURVEY = "Get request to survey";
//	public static final String BROADCAST_CHANGE_MASTER_MODE = "Master mode been-act/deact";
	public static final String BROADCAST_APPLICATION_LOCALE_CHANGE = "App locale been changed";
	public static final String BROADCAST_ALARM_OPEN_CLOSE = "Broadcast alarm closed";
	public static final String BROADCAST_ALARM_ADD_UPD_RMV = "Broadcast add, rmv or del alarm";
	public static final String BROADCAST_ALARM_EVENTS = "Broadcast alarm events";

	private static final String BROADCAST_ALARM_TO_PING_SEND = "Time to sendEx ping";
	private static final String BROADCAST_ALARM_TO_SHORT_RESTART = "Time to fast reconnect";
	private static final String BROADCAST_CHECK_PING = "checkStates ping send/receive";
	private static final String BROADCAST_NOTIFICATION_DISMISS = "Notificationdismissed from notifications bar";
	private static final String BROADCAST_START_ALARM_NOTIFICATION = "Broadcast to start alarm notification activity";
	public static final String BROADCAST_STOP_ALARM_NOTIFICATION = "Broadcast to stop alarm notification process";


	private final IBinder myBinder = new myBinderClass();

	private static final String HOST = BuildConfig.HOST_IP; /*TTK*/
	private static final String SHOST = BuildConfig.HOST; /*TTK*/
	private static final int PORT = BuildConfig.PORT;
	private static final int SPORT = BuildConfig.SPORT;
	private static final boolean  SSL = BuildConfig.SSL; // use ssl flag
	private static final boolean  STUNNEL= BuildConfig.STUNNEL; // use tunnel flag
//	private static final int NPORT = BuildConfig.NPORT; // only for tests


	//	private static final String HOST = "89.232.115.81"; /*TTK*/
	//	private static final String HOST = "78.138.164.58"; /*DEV*/
	//	private static final String HOST = "178.205.168.11"; /*TEST_NEW*///
	//	private static final String HOST = "178.205.168.10"; /*DEV_NEW*/
	//	private static final String HOST = "192.168.20.10"; /*DEV_L
	//	private static final String HOST = "192.168.20.20"; /*TEST_L
	// OC*/

	public static final String LOG_TAG = "D3ServiceLogTag";
	public static final String LOG_TAG_EVENTS = "D3ServiceLogTagEvents";

	private static final long CONNECTION_LONG_TIMEOUT_REPEATING = 300000L;
	private static final long CONNECTION_SHORT_TIMEOUT_NOT_REPEATING = 5000L;
	private static final long ALARM_WINDOW = 1000L;
	private static final int CONNECTION_TIMEOUT_FOR_SOCKET = 15000;
	public static final int EVENTS_TIME_INTERVAL = 3600*24; //events request time
	private static final int PING_CHECK_TIMEOUT = 60000;
	private static final long EVENT_DELAY_INTERVAL = 600;

	private static final int REQUEST_CODE_ALARM_LONG = -10;
	private static final int REQUEST_CODE_ALARM_SHORT = -11;
	private static final int REQUEST_CODE_PING = -12;
	private static final int REQUEST_CODE_ALARM_NOTIFICATION = -13;


	private static final Charset UTF8_CHARSET = StandardCharsets.UTF_8;
	private static final Charset UCS_CHARSET = Charset.forName("UNICODE");
	private static final Charset CP1251_CHARSET = Charset.forName("Windows-1251");
	private static final Charset LATIN_CHARSET = StandardCharsets.ISO_8859_1;
	private boolean eventsNotificationExisted = false;
	private boolean systemNotificationExist = false;
	private boolean serviceNotificationExist = false;
	public static int roles = 0 ;
	private int domain;
	public static int user_id = 0;
	private String locale;

	private D3Tts d3Tss;
	private PrefUtils prefUtils;
	private ReplaySubject<Event> eventSubject = ReplaySubject.create();
	private boolean notifAlarmAM;
	private NotificationUtils notificationUtils;

	//	private static final Charset ASCII = Charset.forName("US-ASCII");

	private enum NOTIF_TYPE{
		CONNECTION,
		EVENTS_FROM_D3,
		EVENT_FROM_D3,
	}

	private InetSocketAddress inetSocketAddress;
	public ConnectionThread connectionThread;
	private boolean errorsRepeated = false;

	private String login;
	private String pass;
	private boolean saveLoginPass = false;

	private int repeat_auth = 0;

//	public UserInfo userInfo ;
	private DBHelper dbHelper;
	private SharedPreferences sp;

	private final LinkedList<Event> eventsToStatusBarList = new LinkedList<>();
	private final LinkedList<Event> eventsWithClipList = new LinkedList<>();
	private final LinkedList<Integer> commandReferencesList = new LinkedList<>();
	private final LinkedList<Integer> operatorReferencesList = new LinkedList<>();
//	private EventFilter eventFilter;

	private DBNewEventReceiver dbNewEventReceiver;
	private D3ConnectionDropReceiver connectionDropReceiver;
	private D3NewProtocolVersionReceiver protocolVersionReceiver;
	private WidgetUpdateReceiver widgetUpdateReceiver;
	private Handler handler;

	private final BroadcastReceiver notificationDismissReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			/*stop full srceen alarm notification process
			**/
			stopFullScreenProcess(context);

			eventsToStatusBarList.clear();
			eventsNotificationExisted = false;
			stopTts();
		}
	};

	private final BroadcastReceiver stopFullProcessReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			/*stop full srceen alarm notification process
			 **/
			stopFullScreenProcess(context);
		}
	};

	private void stopFullScreenProcess(Context context)
	{
		restartEventSubject();
		stopAlarmNotifAlarmManager(context);
	}

	private final BroadcastReceiver popupReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
//			Func.log_d(LOG_TAG, "Show popup");
//			intent.setClass(context, PopupActivity.class);
//			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//			context.startActivity(intent);
		}
	};

	private final BroadcastReceiver localeChangeReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(null!=d3Tss){
//				d3Tss.setLanguage(context);
				d3Tss = D3Tts.updateInstance(getApplicationContext());
			}else{
				d3Tss = D3Tts.getInstance(getApplicationContext());
			}
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			{
				setDefaultNotificationChannels();
			}
		}
	};

//	private BroadcastReceiver setDefaultNotificationChannelsReceiver = new BroadcastReceiver()
//	{
//		@Override
//		public void onReceive(Context context, Intent intent)
//		{
//			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//			{
//				setDefaultNotificationChannels();
//			}
//		}
//	};


	private final BroadcastReceiver widgetControlReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int site_id = intent.getIntExtra("site_id", -1);
			int arm = intent.getIntExtra("arm", -1);
			if((site_id!=-1)&&(arm!=-1)){
				try
				{
					JSONObject commandAddress = new JSONObject();
					commandAddress.put("section", 0);
					commandAddress.put("zone", 0);
					JSONObject commandType = new JSONObject();
					switch (arm){
						case 0:
							commandType.put("DISARM", commandAddress);
							break;
						case 1:
							commandType.put("ARM", commandAddress);
							break;
						default:
							break;
					}

					JSONObject command = new JSONObject();
					command.put("site", site_id);
					command.put("command", commandType.toString());

					send((new D3Request("COMMAND_SET", command, Func.getCommandCount(context))).toString());

				} catch (JSONException e)
				{
					e.printStackTrace();
				}
			}
		}
	};

	private final BroadcastReceiver camerasGetReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getIntExtra("result", -1) == Const.IV_CAMERA_GET_SUCCESS)
			{
				getRelations();
			}
		}
	};


	private final BroadcastReceiver alarmShakeConnectionReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			shakeConnection(true);
			startShakeConnectionAlarmManager(context, true);
		}
	};

	private final BroadcastReceiver alarmShortShakeConnectionReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			shakeConnection(false);
			shakeConnection(false);
		}
	};

	private final BroadcastReceiver alarmCheckPingReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(connectionThread!=null){
				Func.log_i(LOG_TAG, "No Pong");
				stopConnection();
			}

		}
	};

	private final BroadcastReceiver alarmNotificationAlarmStartReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			notifAlarmAM = false;
			startAlarmNotificationActivity(null, Build.VERSION.SDK_INT >= Build.VERSION_CODES.O);
		}
	};

	public D3Service() {}


	public void onCreate() {
		super.onCreate();
		Func.log_i(LOG_TAG, "MyService onCreate");

// 		XML PROTO AND CONFIG PARSING
//		reparseRaw(this);

		this.inetSocketAddress = new InetSocketAddress(HOST, PORT);
	}

	public static void reparseRaw(Context context){
//		d3XProtoConst = D3XProtoConst.load(context);
		d3XProtoConstEvent = D3XProtoConstEvent.load(context);
//		faq = Faq.load(context);
		sec = Sec.load(context);

//		DESERIALIZATION INTO EVENTS.JSON & TYPES.JSON 4 IOS
//		try
//		{
//			Func.deserializeXproto();
//		} catch (JSONException e)
//		{
//			e.printStackTrace();
//		}
	}

	public static void parseRaw(Context context)
	{

//		if(null==d3XProtoConst)
//			d3XProtoConst = D3XProtoConst.load(context);
		if(null==d3XProtoConstEvent)
			d3XProtoConstEvent = D3XProtoConstEvent.load(context);
//		if(null==faq)
//			faq = Faq.load(context);
		if(null ==sec)
			sec = Sec.load(context);
	}

	public IBinder onBind(Intent intent) {
		Func.log_d(LOG_TAG, "MyService onBind");
		return myBinder;
	}

	public void onRebind(Intent intent) {
		super.onRebind(intent);
		Func.log_d(LOG_TAG, "MyService onRebind");
	}

	public boolean onUnbind(Intent intent) {
		Func.log_d(LOG_TAG, "MyService onUnbind");
		return super.onUnbind(intent);
	}

	public void onDestroy() {
		unregisterReceiver(dbNewEventReceiver, true);
		unregisterReceiver(popupReceiver, true);
		unregisterReceiver(connectionDropReceiver, true);
		unregisterReceiver(notificationDismissReceiver, true);
		unregisterReceiver(protocolVersionReceiver, true);
		unregisterReceiver(widgetControlReceiver, true);
		unregisterReceiver(widgetUpdateReceiver, true);
		unregisterReceiver(alarmCheckPingReceiver, true);
		unregisterReceiver(alarmShakeConnectionReceiver, true);
		unregisterReceiver(alarmShortShakeConnectionReceiver, true);
		unregisterReceiver(popupReceiver, true);
		unregisterReceiver(notificationDismissReceiver, true);
		unregisterReceiver(stopFullProcessReceiver, true);
//		unregisterReceiver(setDefaultNotificationChannelsReceiver);
		unregisterReceiver(localeChangeReceiver, true);
		unregisterReceiver(stopFullProcessReceiver, true);
		unregisterReceiver(camerasGetReceiver, true);
		unregisterReceiver(alarmNotificationAlarmStartReceiver, true);

//		closeConnection();
		stopConnection();
//		stopTts();
//		stopShakeConnectionAlarmManager(this);
		Func.log_i(LOG_TAG,   "D3Service release CPU");
//		sendBroadcast(new Intent("biz.teko.shub_app.D3ServiceRestartBroadcast"));
		Func.log_d(LOG_TAG, "MyService onDestroy");
		super.onDestroy();
	}

	private void unregisterReceiver(BroadcastReceiver broadcastReceiver, boolean flag){
		try
		{
			unregisterReceiver(broadcastReceiver);
		}catch (Exception e){
			if(flag) e.printStackTrace();
		}
	}


	private void stopTts()
	{
		if(null!=d3Tss && d3Tss.isSpeaking()) d3Tss.stop();
	}

	public boolean send(String data) {
		if (connectionThread == null) {
			return false;
		}
		Func.log_d(LOG_TAG, data);
		connectionThread.sendExAsync(D3QueueElement.REQ_TYPE.JSON, data.getBytes());
		return true;
	}

	public static int getRole() {
		return roles;
	}

	private void startCheckPingAlarmManager(Context context) {
		Func.log_i(LOG_TAG, "Create alarm manager waiting for pong, period: " + PING_CHECK_TIMEOUT);
		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			alarm.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + PING_CHECK_TIMEOUT, getAMPendingIntent(context, REQUEST_CODE_PING));
		} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			alarm.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + PING_CHECK_TIMEOUT, getAMPendingIntent(context, REQUEST_CODE_PING));
		} else {
			alarm.setWindow(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + PING_CHECK_TIMEOUT, ALARM_WINDOW, getAMPendingIntent(context, REQUEST_CODE_PING));
		}
	}

	private void stopCheckPingAlarmManager(Context context){
		Func.log_i(LOG_TAG,   "Stop checkping alarm manager");
		AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		alarm.cancel(getAMPendingIntent(context, REQUEST_CODE_PING));
	}

	private PendingIntent getAMPendingIntent(Context context, int requestCode){
		switch (requestCode){
			case REQUEST_CODE_ALARM_LONG:
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
				{
					return PendingIntent.getBroadcast(context, requestCode, new Intent(BROADCAST_ALARM_TO_PING_SEND), PendingIntent.FLAG_ONE_SHOT | (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M  ? PendingIntent.FLAG_IMMUTABLE: 0));
				}
			case REQUEST_CODE_ALARM_SHORT:
				return PendingIntent.getBroadcast(context, requestCode, new Intent(BROADCAST_ALARM_TO_SHORT_RESTART), PendingIntent.FLAG_ONE_SHOT | (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0));
			case REQUEST_CODE_PING:
				return PendingIntent.getBroadcast(context, requestCode, new Intent(BROADCAST_CHECK_PING), PendingIntent.FLAG_ONE_SHOT | (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0));
			case REQUEST_CODE_ALARM_NOTIFICATION:
				return PendingIntent.getBroadcast(context, requestCode, new Intent(BROADCAST_START_ALARM_NOTIFICATION), PendingIntent.FLAG_ONE_SHOT | (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0));
		}
		return null;
	}

	public static int getUserId() {
		return user_id;
	}

	private void stopForeground() {
		if (Func.isServiceRunningInForeground(App.getContext(), D3Service.class)) {
			stopForeground(true);
		}
		deleteServiceNotification();
	}

	public void setCredentials(String login, String pass, boolean saveOnSuccess) {
		this.login = login;
		this.pass = pass;
		this.saveLoginPass = saveOnSuccess;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && null == login)
			deleteNotificationChannels(false);
	}

	public void setOptCredentials(int user_id, int roles, int domain, String locale) {
		D3Service.roles = roles;
		this.domain = domain;
		D3Service.user_id = user_id;
		prefUtils.setCurrentUserId(user_id);
		this.locale = locale;
	}

	public void dropCredentials(boolean dropDB) {
		Func.log_d(LOG_TAG, "Credentials will be dropped");
		this.login = null;
		this.pass = null;
		roles = 0;
		this.domain = 0;
		user_id = 0;
		this.locale = null;

		this.saveLoginPass = false;

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && null == login)
			deleteNotificationChannels(dropDB);

		if (dropDB) {
			dbHelper.deleteAllDataFromDB();
		} else {
			dbHelper.saveAllDataInDB();
		}

		Func.dropPreferences(prefUtils, dropDB); // добавлено для сброса истории событий при входе с разными ролями
		Func.log_d(LOG_TAG, "Credentials was dropped");
	}

	private void startShakeConnectionAlarmManager(Context context, boolean repeating) {
		Func.log_i(LOG_TAG, "Start shake connection alarm manager, ping or fast re-start:" + (repeating ? " ping" : " re-start") + ", period:" + (repeating ? CONNECTION_LONG_TIMEOUT_REPEATING : CONNECTION_SHORT_TIMEOUT_NOT_REPEATING) + "  || NOW IT ALWAYS NOT REPEATING");
		if (repeating) {
			AlarmManager alarmRepeat = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				alarmRepeat.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + CONNECTION_LONG_TIMEOUT_REPEATING, getAMPendingIntent(context, REQUEST_CODE_ALARM_LONG));
			} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				alarmRepeat.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + CONNECTION_LONG_TIMEOUT_REPEATING, getAMPendingIntent(context, REQUEST_CODE_ALARM_LONG));
			} else {
				alarmRepeat.setWindow(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + CONNECTION_LONG_TIMEOUT_REPEATING, ALARM_WINDOW, getAMPendingIntent(context, REQUEST_CODE_ALARM_LONG));
//				afnotlarmRepeat.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + CONNECTION_LONG_TIMEOUT_REPEATING, pintent);
			}
		} else {
			AlarmManager alarmShot = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				alarmShot.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + CONNECTION_SHORT_TIMEOUT_NOT_REPEATING, getAMPendingIntent(context, REQUEST_CODE_ALARM_SHORT));
			} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
				alarmShot.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + CONNECTION_SHORT_TIMEOUT_NOT_REPEATING, getAMPendingIntent(context, REQUEST_CODE_ALARM_SHORT));
			} else {
				alarmShot.setWindow(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + CONNECTION_SHORT_TIMEOUT_NOT_REPEATING, ALARM_WINDOW, getAMPendingIntent(context, REQUEST_CODE_ALARM_SHORT));
//				alarmShot.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + CONNECTION_SHORT_TIMEOUT_NOT_REPEATING,  pintent);
			}
		}
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Func.log_i(LOG_TAG, "Call onStartCommand in d3Service");
		super.onStartCommand(intent, flags, startId);

//		if(D3Connectivity.isConnectedWifi(getApplicationContext()))
//		{
//			WifiManager.WifiLock lock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE)).createWifiLock("MyWifiLock");
//			lock.acquire();
//		}

		parseRaw(getApplicationContext());

		this.dbHelper = DBHelper.getInstance(this);
		this.d3Tss = D3Tts.getInstance(getApplicationContext());
		this.sp = PreferenceManager.getDefaultSharedPreferences(this);
		prefUtils = PrefUtils.getInstance(this);
		notificationUtils = NotificationUtils.getInstance(this);

		UserInfo userInfo = dbHelper.getUserInfo();
		if(userInfo!=null){
			setCredentials(userInfo.userLogin, userInfo.userPassword, false);
			setOptCredentials(userInfo.id,userInfo.roles, userInfo.domain, userInfo.locale);
		}

		this.dbNewEventReceiver = new DBNewEventReceiver();
		registerReceiver(dbNewEventReceiver, new IntentFilter(BROADCAST_TO_DELETE_OLD_EVENTS));
		this.connectionDropReceiver = new D3ConnectionDropReceiver(this);
		registerReceiver(connectionDropReceiver, new IntentFilter(BROADCAST_CONNECTION_DROP));
		this.protocolVersionReceiver = new D3NewProtocolVersionReceiver();
		registerReceiver(protocolVersionReceiver, new IntentFilter(BROADCAST_NEW_PROTOCOL_VERSION));
		this.widgetUpdateReceiver = new WidgetUpdateReceiver();
		registerReceiver(widgetUpdateReceiver, new IntentFilter(BROADCAST_UPDATE_WIDGETS));
		registerReceiver(widgetControlReceiver, new IntentFilter(NormalWidget.BROADCAST_WIDGET_SITE_CONTROL));

		registerReceiver(alarmCheckPingReceiver, new IntentFilter(BROADCAST_CHECK_PING));
		registerReceiver(alarmShortShakeConnectionReceiver, new IntentFilter(BROADCAST_ALARM_TO_SHORT_RESTART));
		registerReceiver(alarmShakeConnectionReceiver, new IntentFilter(BROADCAST_ALARM_TO_PING_SEND));

		registerReceiver(popupReceiver, new IntentFilter(BROADCAST_POPUP));
		registerReceiver(notificationDismissReceiver, new IntentFilter(BROADCAST_NOTIFICATION_DISMISS));
		registerReceiver(stopFullProcessReceiver, new IntentFilter(BROADCAST_STOP_ALARM_NOTIFICATION));
		registerReceiver(localeChangeReceiver, new IntentFilter(BROADCAST_APPLICATION_LOCALE_CHANGE));
//		registerReceiver(setDefaultNotificationChannelsReceiver, new IntentFilter(BROADCAST_CHANGE_MASTER_MODE));
		registerReceiver(camerasGetReceiver, new IntentFilter(WebHelper.BROADCAST_CAMERA_GET));
		registerReceiver(alarmNotificationAlarmStartReceiver, new IntentFilter(BROADCAST_START_ALARM_NOTIFICATION));


		shakeConnection(true);
		startShakeConnectionAlarmManager(this, true);
		getApplicationContext().sendBroadcast(new Intent(BROADCAST_D3SERVICE_ALIVE));
		Func.log_d(Const.LOG_TAG, "Service alive d3serv");

		if(Func.foregroundMode(this))
		{
			Func.log_v(D3Service.LOG_TAG, "Start D3Service FOREGROUND");
			runAsForeground();
			return START_STICKY;
		}else{
			stopForeground();
			Func.log_v(D3Service.LOG_TAG, "Start D3Service BACKGROUND");
			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				return START_NOT_STICKY;}
			else{
				return START_STICKY;
			}
		}
	}

	public void showConnectionDropNotification() {

//		нет необходимости, так как и так уже при старте сервиса устанавливаются настройки каналов уведомлений по умолчанию
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//			setDefaultConnectionDropNotificationChannel();
//		}

		Intent notificationIntent = new Intent(this, StartUpActivity.class);
		PendingIntent notifContentIntent = PendingIntent.getActivity(getApplicationContext(),
				0,
				notificationIntent,
				android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0);

		NotificationCompat.Builder notifBuilder = new NotificationCompat.Builder(getApplicationContext(),
				notificationUtils.getConnectionDropNotifChannelId())
				.setContentIntent(notifContentIntent)
				.setSmallIcon(R.drawable.ic_connection_off)
				.setTicker(getString(R.string.application_name))
				.setContentTitle(getString(R.string.CONNECTION_DROP_NOTIFICATION_MESSAGE))
				.setStyle(new NotificationCompat.BigTextStyle()
						.bigText(getString(R.string.CONNECTION_DROP_MESSAGE)))
				.setAutoCancel(true);
		NotificationManagerCompat.from(this).notify(
				CONNECTION_DROP_NOTIFICATIONS_ID,
				notifBuilder.build()
		);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public void setDefaultNotificationChannels() {
		if (null != d3XProtoConstEvent) {
			LinkedList<EClass> eClasses = d3XProtoConstEvent.classes;
			for (EClass eClass : eClasses) {

				notificationUtils.createClassDefaultChannel(eClass.id, eClass.caption);
//				if(prefUtils.getImportNotifChannels()){
//					notificationUtils.deleteClassChannel(eClass.id);
//					notificationUtils.updateClassChannelID(eClass.id);
//					notificationUtils.createClassDefaultChannel(eClass.id, eClass.caption);
//				}
			}
			if(prefUtils.getImportNotifChannels()) prefUtils.setImportNotifChannels(false);
		}
		setDefaultConnectionDropNotificationChannel();
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void setDefaultConnectionDropNotificationChannel() {
		notificationUtils.createChannel(notificationUtils.getConnectionDropNotifChannelId(),
				getString(R.string.CONNECTION_DROP_NOTIFICATION),
				NotificationManager.IMPORTANCE_HIGH
		);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void deleteNotificationChannels(boolean dontSave)
	{
		if(null!=d3XProtoConstEvent)
		{
			for (EClass eClass : d3XProtoConstEvent.classes){
				notificationUtils.deleteClassChannel(eClass.id);
				if(dontSave) {
					notificationUtils.removeChannelParamsFromPrefs(eClass.id);
//					notificationUtils.updateClassChannelID(eClass.id); //удаляем и увеличиваем id для канала уведомлений, чтобы не сохранялись настройки предыдущие
				}
			}
		}
		((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).deleteNotificationChannel(getString(R.string.SYSTEM_NOTIFICATIONS));
	}

	private void enableNotifications(){
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			setDefaultNotificationChannels();
		}else
		{
			prefUtils.setNotificationStatus(true);
		}

	}
	private void disableNotifications(){
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
			deleteNotificationChannels(false);
		}else{
			prefUtils.setNotificationStatus(false);
		}
	}


	@Synchronized
	public void shakeConnection(boolean full){
		Func.log_i(LOG_TAG, "Starting(shake) connection thread");

		if(connectionThread != null){
			Func.log_i(LOG_TAG, "Connection NOT NULL");
			if(!connectionThread.running){
				connectionThread.stopRunning(false);
				connectionThread.interrupt();
				Func.log_i(LOG_TAG, "Connection NOT running - restart connection");
				connectionThread = new ConnectionThread();
				connectionThread.start();
			}else{
				if(full)
				{
					Func.log_i(LOG_TAG, "Connection running + FULL shake - not restart connection, only checkStates by ping");
					if (!connectionThread.ping())
					{
						Func.log_i(LOG_TAG, "Cant sent ping");
						connectionThread.stopRunning(true);
					} else
					{
						startCheckPingAlarmManager(getApplicationContext());
					}
				}else{
					Func.log_i(LOG_TAG, "Connection running + NOT FULL shake - not restart connection");
				}
			}
		}else{
			Func.log_i(LOG_TAG, "Connection NULL - restart connection");
			connectionThread = new ConnectionThread();
			connectionThread.start();
		}

	}

	public boolean getStatus(){
		if(null==connectionThread){
			return false;
		}
		return connectionThread.getStatus();
	}

	public void stopConnection(){
		if(null!=connectionThread){
			connectionThread.stopRunning(false);
		}
	}

	/*CONNECTING*/

	private class ConnectionThread extends Thread
	{
		private Socket tunnel;
		private Socket socket;
		private SSLSocket sslSocket;

		private boolean running;
		private SSLSocketFactory factory;
		//		private boolean pinging = false;

		public ConnectionThread(){
		}

		private Socket getSocket(){
			return SSL ? sslSocket : socket;
		}

		public void stopRunning(boolean restart){
			closeSocket();
			/*refresh activity layouts if read exception*/
			sendIntentCommandBeenSend(-2, 0);
			running = false;
			if(restart)
			{
				startShakeConnectionAlarmManager(getApplicationContext(), false);
				if(errorsRepeated){
					if(Func.getBooleanSPDefFalse(sp, "notif_connection"))
					{
						showNoConnectionNotification();
					}
					sendBroadcast(new Intent(BROADCAST_CONNECTION_ERROR_REPEATED));
				}else
				{
					errorsRepeated = true;
				}
			}
//			else{
//				cancelNoConnectionNotification();
//			}
		}

		public void run(){
			running = true;
			Func.log_i(LOG_TAG,   "Starting running thread, connecting to "  + inetSocketAddress.getHostName());
			try
			{
				/*except situation, then user disconnected and logout after discon (but not reconn)*/
				if (null == login || null == pass) {
					throw new InvalidParameterSpecException("No login");
				}

				updateSocket();
				if(!SSL) this.socket.connect(inetSocketAddress, CONNECTION_TIMEOUT_FOR_SOCKET);

				/* Set errors count to know how many times connection problems true error occured*/
				errorsRepeated = false;
				cancelNoConnectionNotification();

				if(autorize()){
					while(running){
						Func.log_i(LOG_TAG, "Waiting for data in read");
						String reply = read(D3QueueElement.REQ_TYPE.JSON);
						Func.log_d(LOG_TAG, reply);
						handle(reply);
					}
				}else{
					if(repeat_auth == 1){
						stopRunning(true);
					}else{
						repeat_auth = 0;
					}
				}
			} catch (Exception e)
			{
				Func.log_e(LOG_TAG, "Connection exception: " + e);
				boolean connectionProblems = (e.getClass()==EOFException.class)
						||(e.getClass()==IOException.class)
						||(e.getClass() == ConnectException.class)
						||(e.getClass() == SocketException.class)
						||(e.getClass() == SocketTimeoutException.class)
						||(e.getClass() == Exception.class)
						||(e.getClass() == ArrayIndexOutOfBoundsException.class)
						||(e.getClass() == ClassCastException.class)
						||(e.getClass() == NullPointerException.class)
						||(e.getClass() == SSLException.class)
						||(e.getClass() == SSLHandshakeException.class);
				stopRunning(connectionProblems);
			}
		}

		private void closeSocket()
		{
			new Thread(() -> {
				try
				{
					Func.log_i(LOG_TAG,   "Socket close");
					Socket socket = getSocket();
					if(null!=socket)socket.close();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}).start();
		}

		private void updateSocket()
		{

			if(SSL)
			{
				if (STUNNEL)
				{
					factory = (SSLSocketFactory) SSLSocketFactory.getDefault();
					this.tunnel = new Socket();
				} else
				{
					factory = createAdditionalCertsSSLSocketFactory();
				}

				try
				{
					if (STUNNEL)
					{
						this.tunnel = new Socket(SHOST, SPORT);
						this.sslSocket = (SSLSocket) factory.createSocket(tunnel, SHOST, PORT, true);

					} else
					{
						this.sslSocket = (SSLSocket) factory.createSocket(SHOST, SPORT);
					}

					this.sslSocket.setSoTimeout(0);
					this.sslSocket.setKeepAlive(false);
					/*
					 * register a callback for handshaking completion event
					 */
					this.sslSocket.addHandshakeCompletedListener(event -> {
						Func.log_d(LOG_TAG, "Handshake finished!");
						Func.log_d(LOG_TAG, "\tCipherSuite:" + event.getCipherSuite());
						Func.log_d(LOG_TAG, "\tSessionId " + event.getSession());
						Func.log_d(LOG_TAG, "\tPeerHost " + event.getSession().getPeerHost());
					});

					if (STUNNEL) this.sslSocket.startHandshake();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}else{
				this.socket = new Socket();
				try
				{
					this.socket.setSoTimeout(0);
					this.socket.setKeepAlive(false);
				} catch (SocketException e)
				{
					e.printStackTrace();
				}

			}
		}

		public boolean getStatus(){
			return running;
		}

		public boolean ping() {
			if(connectionThread==null){
				return  false;
			}
//			if(pinging){
//				return false;
//			}
			byte[] ping = {(byte) 0x89, (byte) 0x00};
			sendExAsync(D3QueueElement.REQ_TYPE.WEB, ping);
			Func.log_i(LOG_TAG, "Ping sent");
//			pinging = true;
			return true;
		}

		public void sendExAsync(final D3QueueElement.REQ_TYPE req_type, final byte[] buf){
			new AsyncTask<Void, Void, Void>()
			{
				@Override
				protected void onPreExecute()
				{
					super.onPreExecute();
				}

				@Override
				protected Void doInBackground(Void... params)
				{
					try
					{
						sendEx(req_type, buf);
						/* TO DO send broadcast command send 0*/
//						sendIntentCommandBeenSend(0, 0);
					} catch (Exception e)
					{
						Func.log_e(LOG_TAG, "AsyncSend exception - " + e.toString());
						/* TO DO send broadcast command send -1
						* временно убрал, так как выводит тосты с неудачной отправкой команды не только на команды, но и запросы
						* sendEx и на команды, и на запросы*/
//						sendIntentCommandBeenSend(-1, 0);
//						shakeConnection(true);
						stopRunning(true);
					}
					return null;
				}
			}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
		}

		@Synchronized
		public void sendEx(D3QueueElement.REQ_TYPE req_type, byte[] buf) throws Exception
		{
			OutputStream out = getSocket().getOutputStream();
			switch (req_type)
			{
				case WEB:
					out.write(buf, 0, buf.length);
					out.flush();
					break;
				case JSON:
					int count = buf.length;
					int bytes_for_size = 1;

					byte[] start = {(byte) 0x81};

					if(count > 125){
						if(count > 65535){
							bytes_for_size = 9;
						}else{
							bytes_for_size = 3;
						}
					}

					byte[] b_size = new byte[bytes_for_size];

					if(1 == bytes_for_size){
						b_size = invertByte(intToBytes(count));
					}else{
						byte[] size_preamble = new byte[1];
						byte[] size;
						if(3==bytes_for_size){
							size_preamble[0] = 0x7E;
							size = shortToBytes((short)count);
						}else{
							size_preamble[0] = 0x7F;
							size = longToBytes((long) count);
						}
						System.arraycopy(size_preamble, 0, b_size, 0, size_preamble.length);
						System.arraycopy(size, 0, b_size, size_preamble.length, bytes_for_size - 1);
					}

					byte[] request_byte = new byte[1 + bytes_for_size + count];
					System.arraycopy(start, 0, request_byte, 0, 1);
					System.arraycopy(b_size, 0, request_byte, 1, bytes_for_size);
					System.arraycopy(buf, 0, request_byte, bytes_for_size + 1, count);
					out.write(request_byte, 0, request_byte.length);
					break;
			}
		}

		public String read(D3QueueElement.REQ_TYPE req_type)throws Exception
		{
			int r;
			byte[] buf;
			InputStream in = getSocket().getInputStream();
            Func.log_v(LOG_TAG,  "Got it! data in socket ");
            switch (req_type){
				case WEB:
					buf = new byte[1024];
					r = in.read(buf, 0, 1024);
					if (r <= 0) {
						throw new EOFException("Read result " + r);
					}
					return decodeUTF8(ArrayUtils.subarray(buf, 0, r));
				case JSON:
					boolean reread;
					do
					{
						reread = false;
						byte[] start = new byte[1];
						Func.log_v(LOG_TAG,  "First byte in input buf is " + start[0]);
						r = in.read(start, 0, 1);
						if (r <= 0)
						{
							throw new EOFException("Read result " + r);
						}
						if (start[0] == (byte) 0x81)
						{
							byte[] size_mod = new byte[1];
							in.read(size_mod, 0, 1);
							if (size_mod[0] == (byte) 0x7E)
							{
								byte[] size = new byte[2];
								in.read(size, 0, size.length);
								buf = new byte[byteArrayToInt(size)];
								int totalL = buf.length;
								int readedL = 0;
								while ((totalL - readedL) != 0)
								{
									readedL += in.read(buf, readedL, buf.length - readedL);
								}
								return decodeUTF8(buf);
							} else
							{
								if (size_mod[0] == (byte) 0x7F)
								{
									byte[] size = new byte[8];
									in.read(size, 0, size.length);
									buf = new byte[byteArrayToInt(size)];
									int totalL = buf.length;
									int readedL = 0;
									while ((totalL - readedL) != 0)
									{
										readedL += in.read(buf, readedL, buf.length - readedL);
									}
									return decodeUTF8(buf);
								} else
								{
									buf = new byte[size_mod[0]];
									in.read(buf, 0, buf.length);
									return decodeUTF8(buf);
								}
							}
						} else
						{
							if (start[0] == (byte) 0x8A)
							{
							/* PONG */
								byte[] pong_size = new byte[1];
								int result = in.read(pong_size, 0, 1);
								if(byteArrayToInt(pong_size) !=0){
									throw new Exception("Wrong ping answer");
								}
//								byte[] pong = new byte[pong_size[0]];
//								result = in.read(pong, 0, pong.length);
//								buf = new byte[start.length + pong_size.length + pong.length];
//								System.arraycopy(start, 0, buf, 0, start.length);
//								System.arraycopy(pong_size, 0, buf, start.length, pong_size.length);
//								System.arraycopy(pong, 0, buf, start.length + pong_size.length, pong.length);
								Func.log_i(LOG_TAG, "Pong");
								boolean alarmUp = (PendingIntent.getBroadcast(getApplicationContext(), -12,
										new Intent(BROADCAST_CHECK_PING),
										PendingIntent.FLAG_ONE_SHOT | (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0 )) != null);
								if(alarmUp){
									stopCheckPingAlarmManager(getApplicationContext());
								}
//								pinging = false;
								reread = true;
							}
						}
					} while (reread);
					throw new Exception("Unexpected ws data");
				default:
					throw new Exception("Unexpected type");
			}
		}


		public boolean autorize() throws Exception{
			if (null == login || null == pass) {
				throw new InvalidParameterSpecException("No login");
			}
			Func.log_i(LOG_TAG,   "Handshake");
			signIn(login, pass);
			String reply = read(D3QueueElement.REQ_TYPE.WEB);
			// checkStates protocol version
			D3WebResponse response = new D3WebResponse(reply);
			int protocol_version = response.getProtocolVersion();
			if((BuildConfig.PROTOCOL == protocol_version)||(0 == protocol_version))
			{
				int loginResult = response.getLoginResult();
				//checkStates result and delete data if result not 1 & sendEx broadcast about logout
				if (1 <= loginResult)
				{
					Func.log_i(LOG_TAG,   "Handshake ok");
					getActualRoles();

					if (saveLoginPass) {
						saveLoginPass = false;
						/*yes am*/
						dbHelper = DBHelper.updateDBInstance(App.getContext()); // при новом логине сохраняет юзера в общую базу, не ломая базу другого пользователя
						dbHelper.addUserInfo(new UserInfo(1,login, pass, System.currentTimeMillis() / 1000));
					}
					repeat_auth = 0;
					return true;
				} else
				{
					//analize handshake response
					Func.log_e(LOG_TAG,   "Handshake failed, login result: " + Integer.toString(loginResult));
					dropCredentials(false);//! login failed
					Intent loginFailedIntent  = new Intent(BROADCAST_CONNECTION_DROP);
					loginFailedIntent.putExtra("result", loginResult);
					loginFailedIntent.putExtra("AppName", getPackageName());
					loginFailedIntent.putExtra("from", "d3 auth");
					sendBroadcast(loginFailedIntent);
					repeat_auth++;
					return false;
				}
			}else{
				Func.log_e(LOG_TAG,   "Protocol version doesn't compare");
				dropCredentials(false); //! protocol problem
				sendBroadcast(new Intent(BROADCAST_NEW_PROTOCOL_VERSION));
			}
			repeat_auth = 0;
			return false;
		}

//      @Synchronized
		public void signIn(String login, String pass) throws Exception{
			sendEx(D3QueueElement.REQ_TYPE.WEB, ("GET /?user=" + login +"&pass=" + pass +" HTTP/1.1\r\n\r\n\r\n").getBytes());
		}
	}

	private void getActualRoles()
	{
		send((new D3Request("ROLES_GET", new JSONObject())).toString());
	}

	private void setFCMToken()
	{
		//put it in a separate class not to break the logic of D3Service and use coroutines
		D3FCM d3FCM = new D3FCM(this);
		d3FCM.setFCMToken();
	}

	public void resetFCMToken()
	{
		send((new D3Request("FIREBASE_ID_DEL", new JSONObject())).toString());
	}

	private void getIVToken()
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject message = new JSONObject();
		try
		{
			message.put("domain", userInfo.domain);
			send((new D3Request(D3Request.IV_GET_TOKEN, message)).toString());
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void showNoConnectionNotification()
	{
		showNotification(NOTIF_TYPE.CONNECTION, null);
	}

	public void cancelNoConnectionNotification()
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
		{
			((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).deleteNotificationChannel(getString(R.string.SYSTEM_NOTIFICATIONS));
		}
		Intent intent = new Intent(BROADCAST_CONNECTION_STATUS_CHANGE);
		intent.putExtra("status", 1);
		sendBroadcast(intent);
		NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.cancel(SYSTEM_NOTIFICATION_ID);
		notifManager.cancel(SYSTEM_NOTIFICATIONS_GROUP_ID);
	}

	public void deleteServiceNotification()
	{
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			recreateServiceNotificationChannel(false);
		}
		NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notifManager.cancel(SERVICE_NOTIFICATION_ID);
		notifManager.cancel(SERVICE_NOTIFICATION_GROUP_ID);
		serviceNotificationExist = false;
	}


	public boolean getActualRoleFromServer(){
		return send((new D3Request("ROLES_GET", new JSONObject())).toString());
	}

	public void getActualDataFromServer()
	{
		boolean result =
		send((new D3Request("DOMAINS", new JSONObject())).toString())
				&&send((new D3Request("LOCALES", new JSONObject())).toString())
				&&send((new D3Request("OPERATORS", new JSONObject())).toString())
				&&send((new D3Request("DEVICES", getDevicesObject())).toString())
				&&send((new D3Request("SITES", new JSONObject())).toString())
				&&send((new D3Request("DEVICE_SITES", new JSONObject())).toString())
				;

		if(result)
		{
			result = send((new D3Request("CLUSTERS", new JSONObject())).toString());
		}
 		if(result)
		{
			result = send((new D3Request("ALARMS", getEventsObject(), EVENTS_REQUEST_ID)).toString())
				&&send((new D3Request("EVENTS", getEventsObject(), EVENTS_REQUEST_ID)).toString())
				&&send((new D3Request("EVENTS", getAffectsObject(), AFFECTS_REQUEST_ID)).toString())
			;
		}
		Func.log_i(LOG_TAG, "Send actual data request");
		sendBroadcast(new Intent(BROADCAST_DATA_FROM_SERVER_FETCH));
	}

	private JSONObject getAffectsObject(){
		JSONObject affectsRequest = new JSONObject();
		try
		{
			affectsRequest.put("affect", 1);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return affectsRequest;
	}

	private JSONObject getDeviceObject(int device_id){
		JSONObject deviceObject = new JSONObject();
		try
		{
			deviceObject.put("device_id", device_id);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return deviceObject;
	}

	private JSONObject getDevicesObject(){
		JSONObject js = new JSONObject();
		try
		{
			js.put("temp_thresholds", 1);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		return js;
	}

	private JSONObject getEventsObject(){
		JSONObject eventsRequest = new JSONObject();
		long tmin = dbHelper.getEventMaxTime();
		Func.log_d(LOG_TAG, "tmin = " + tmin);
		if (0 == tmin)
		{
			try
			{
				eventsRequest.put("tmin", Func.getStartOfDay(System.currentTimeMillis() - EVENTS_TIME_INTERVAL*1000)/1000);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		} else
		{
			try
			{
				eventsRequest.put("tmin", tmin);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		return eventsRequest;
	}

	private void getCameras(String iv_token)
	{
		WebHelper.getInstance().getCamerasList(iv_token);
	}

	private void getRelations(){
		JSONObject message = new JSONObject();
		try
		{
			message.put("device", 0);
			message.put("section", 0);
			message.put("zone", 0);
			send(new D3Request(D3Request.IV_GET_RELATIONS, message).toString());
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void handle(String response)
	{
		try
		{
			Func.log_i(LOG_TAG,   "Handle response");

			D3Response d3Response = new D3Response(response);
//			Func.log(LOG_TAG,  + d3Response.getQ(), d3Response.toString());

			switch (d3Response.getQ()){

				/* HANDLE CLUSTERS*/

				case("CLUSTERS"):
					switch (d3Response.getResult()){
						case 1:
							int count = d3Response.getCount();
							JSONArray jsonArray = d3Response.getDataField().getJSONArray("items");
							if(count!=jsonArray.length())
								count = jsonArray.length();
							if(count < dbHelper.getClustersCount())
							{
								dbHelper.deleteAllClusters();
							}
							for(int i =0; i < count; i++){
								JSONObject jsonObject = jsonArray.getJSONObject(i);
								Cluster cluster = new Cluster(jsonObject);
								dbHelper.addCluster(cluster);
							}
							break;
						default:
							break;
					}
					break;

				/* HANDLE ROLES */

				case("ROLES_GET"):
					if(d3Response.getResult() > 0){
						setOptCredentials(d3Response.getId(), d3Response.getRole(), d3Response.getDomainID(), d3Response.getLocale().iso);
						dbHelper = 	DBHelper.updateDBInstance(App.getContext());
						/*TODO убрать после обновления, когда импорт уже не будет нужен*/
						if(0 != prefUtils.getCurrentUserId() && prefUtils.getImportSettings()){
							dbHelper.importFromDefault();
							prefUtils.importSettings(d3XProtoConstEvent.classes);
						}
						dbHelper.setUserParams(this.login, this.pass, user_id, roles, this.domain, this.locale);
						if((roles& Const.Roles.TRINKET) !=0) // add for trinket role
						{
							/*удаляем все каналы оповещений и отключаем оповещения для брелка*/
							resetFCMToken();
							disableNotifications();
							Func.loadDefaultPreferences(sp, this);
						}else{
							/*set fcm notifications*/
							setFCMToken();
							enableNotifications();
							Func.loadDefaultPreferences(sp, this);
							/*check iv binding and get cameras if iv acc binded*/
							getIVToken();
						}
						getActualDataFromServer();
						sendBroadcast(new Intent(BROADCAST_HANDSHAKE_OK));

						NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
						notifManager.cancel(CONNECTION_DROP_NOTIFICATIONS_ID);
						sendBroadcast(new Intent(BROADCAST_ROLES_GET));
					}
					break;

				case("ROLE"):
				case("ROLES_UPD"):
					setOptCredentials(d3Response.getId(), d3Response.getRole(), d3Response.getDomainID(), d3Response.getLocale().iso);
					dbHelper.setUserParams(user_id, roles, this.domain, this.locale);
					break;

				/* HANDLE LOCALES*/
				case ("LOCALES"):
					switch (d3Response.getResult()){
						case 1:
							dbHelper.deleteLocales();
							int count = d3Response.getCount();
							JSONArray localesArray = d3Response.getItems();
							if(count != localesArray.length()){
								count = localesArray.length();
							}
							for(int i = 0; i < count; i++){
								LocaleD3 localeD3 = new LocaleD3(localesArray.getJSONObject(i));
								dbHelper.addNewLocale(localeD3);
							}

							break;
						default:
							break;
					}
					break;

				case ("SET_LOCALE"):
					switch (d3Response.getResult()){
						case 1:
							send((new D3Request("ROLES_GET", new JSONObject())).toString());
							break;
						default:
							break;
					}
					break;

				/* HANDLE SITES */

				case("SITES"):
 					switch(d3Response.getResult()){
						case 0:
							break;
						case 1:
							Site[] sites = d3Response.getSites();
							if(null!=sites)
							{
								if (0 < sites.length)
								{
									dbHelper.addSites(sites);
									for(Site site:sites){
										JSONObject delegatesJson = new JSONObject();
										delegatesJson.put("site_id", site.id);
										send((new D3Request("SITE_DELEGATES", delegatesJson)).toString());
									}
									break;
								}
							}
							dbHelper.deleteAllSitesAndStructure();
							break;
						default:
							break;
					}
					sendBroadcast(new Intent(BROADCAST_SITES));
					sendBroadcast(new Intent(BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
					sendBroadcast(new Intent(BROADCAST_UPDATE_WIDGETS));
					break;

				case("SITE_SET"):
					Intent siteSetIntent = new Intent(BROADCAST_SITE_SET);
					siteSetIntent.putExtra("result", d3Response.getResult());
					sendBroadcast(siteSetIntent);
					break;


				case("SITE_DEL"):
					Intent intent = new Intent(BROADCAST_SITE_DEL);
					intent.putExtra("result", d3Response.getResult());
					sendBroadcast(intent);
					break;

//				case("SITE_UPD"):
//					Site updatedSite = d3Response.getSite();
//					if(updatedSite != null){
//						dbHelper.addSite(updatedSite);
//						JSONObject delegatesJson = new JSONObject();
//						delegatesJson.put("site_id", updatedSite.id);
//						send((new D3Request("SITE_DELEGATES", delegatesJson)).toString());
//					}
//					sendBroadcast(new Intent(BROADCAST_UPDATE_SITE_SECTION_OR_ZONE_RELOAD));
//					break;
				case("SITE"):
					Site updatedSite = d3Response.getSite();
					if(updatedSite != null){
						dbHelper.addSite(updatedSite);
						JSONObject delegatesJson = new JSONObject();
						delegatesJson.put("site_id", updatedSite.id);
						send((new D3Request("SITE_DELEGATES", delegatesJson)).toString());
					}
					sendBroadcast(new Intent(BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
					break;
				case("SITE_UPD"):
				case("SITE_UPD_M"):
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("site_id", d3Response.getDataField().getInt("id"));
					send((new D3Request("SITE", jsonObject)).toString());
					sendBroadcast(new Intent(BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
					break;

				case("SITE_RMV"):
					JSONObject siteObject = d3Response.getDataField();
					int site_id = siteObject.getInt("site_id");
					dbHelper.deleteSiteWithStructure(site_id);
					intent = new Intent(BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
					intent.putExtra("site", site_id);
					sendBroadcast(intent);
					/*update device_site relations*/
//					send((new D3Request("DEVICE_SITES", new JSONObject())).toString());
					break;

				case("SITE_ZONE_ADDED"):
					/*
					*DEPRECATED OR NOT ?!
					*
					switch (d3Response.getResult()){
						case 2:
							send((new D3Request("SITES", new JSONObject())).toString());
							break;
						default:
							break;
					}
					*/
					break;

				/* HANDLE ZONES */

				case("ZONE_ADD"):
					/*
					*DEPRECATED OR NOT ?!
					*/
					switch (d3Response.getResult()){
						case 1:
							break;
						default:
							break;
					}
					break;

				case("ZONE_SET"):
					Intent zoneSetResultIntent = new Intent(BROADCAST_ZONE_SET);
					zoneSetResultIntent.putExtra("result", d3Response.getResult());
					sendBroadcast(zoneSetResultIntent);
					break;

				case("ZONE_DEL"):
					Intent zoneDelResultIntent = new Intent(BROADCAST_ZONE_DEL);
					zoneDelResultIntent.putExtra("result", d3Response.getResult());
					sendBroadcast(zoneDelResultIntent);
					break;

				case("SECTION_ZONE_UPD"):
					Zone zone = d3Response.getFullZone();
					dbHelper.addZone(zone);
					dbHelper.updateAllZonesActual(1);
					if(zone.id == 0 ){
						dbHelper.addDeviceSite(zone);
//						JSONObject json = new JSONObject();
//						json.put("device_id", zone.device_id);
//						json.put("section", zone.section_id);
//						send(new D3Request(D3Request.TEMP_TRESHOLDS_GET, json).toString());
					}
					intent = new Intent(BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
					intent.putExtra("device", zone.device_id);
					intent.putExtra("detector", zone.detector);
					intent.putExtra("zone", zone.id);
					sendBroadcast(intent);
					break;

				case("SECTION_ZONE_RMV"):
					zone = d3Response.getZone();
					dbHelper.deleteDataForZone(zone);
					Intent intent1  = new Intent(BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
					intent1.putExtra("device",zone.device_id);
					intent1.putExtra("section", zone.section_id);
					intent1.putExtra("zone", zone.id);
					intent1.putExtra("action", Const.ACTION_RMV);
					sendBroadcast(intent1);
					if(zone.section_id == 0 && (zone.id == 100 || zone.id == 0)){
						send((new D3Request("SITES", new JSONObject())).toString());
					}
					break;

				case ("DEVICE_EXTRA_ADD"):
				case ("DEVICE_EXTRA_UPD"):
					dbHelper.addExtra(d3Response.getExtra());
					sendBroadcast(new Intent(BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
					break;

				case ("DEVICE_EXTRA_RMV"):
					JSONObject data = d3Response.getDataField();
					dbHelper.deleteExtra(data.getInt("device_id"), data.getInt("section"), data.getInt("zone"));
					sendBroadcast(new Intent(BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
					break;

				/* \ */
				case ("ALARMS"):
					if(d3Response.getResult() > 0){
						JSONArray items = d3Response.getItems();
						if(null!= items && items.length() > 0){
							int count = d3Response.getCount();
							if(count!= items.length())
								count = items.length();
							Alarm[] alarms = d3Response.getAlarms();
							if(null!=alarms){
								dbHelper.addAlarms(alarms);
								for(Alarm alarm:alarms){
									JSONObject alarm_json = new JSONObject();
									alarm_json.put("alarm_id", alarm.id);
//									send((new D3Request("ALARM", alarm)).toString());
									send((new D3Request("ALARM_EVENTS", alarm_json)).toString());
								}
							}
						}else{
							/*empty alarms list*/
							dbHelper.deleteAlarms();
						}
						sendBroadcast(new Intent(BROADCAST_ALARM_ADD_UPD_RMV));
					}
					break;
				case ("ALARM"):
					if(d3Response.getResult() > 0){
						Alarm alarm =d3Response.getAlarm();
						if(null!=alarm){
							dbHelper.addAlarm(alarm);
						}
						sendBroadcast(new Intent(BROADCAST_ALARM_ADD_UPD_RMV));
					}
					break;
				case ("ALARM_ACTIVITIES"):
					if(d3Response.getResult() > 0){

					}
					break;

				case "ALARM_EVENTS":
					int result = d3Response.getResult();
					if(result > 0){
						JSONArray items = d3Response.getItems();
						int count = d3Response.getCount();
						if(count != items.length()){
							count = items.length();
						}
						for(int i=0; i< count; i++){
							dbHelper.addAlarmEvent(result, items.getJSONObject(i).getInt("id"));
						}
						intent = new Intent(BROADCAST_ALARM_EVENTS);
						intent.putExtra("alarm_id", result);
						sendBroadcast(intent);
					}
					break;

				case "ALARM_UPD":
				case "ALARM_RMV":
					Alarm alarm = d3Response.getAlarm();
					if(null!=alarm) {
						dbHelper.addAlarm(alarm);
						intent = new Intent(BROADCAST_ALARM_ADD_UPD_RMV);
						intent.putExtra("alarm_id", alarm.id);
						sendBroadcast(intent);
					}
					break;

				case "ALARM_ADD":
					alarm = d3Response.getAlarm();
					if(null!=alarm){
						dbHelper.addNewAlarm(alarm);

						JSONObject alarm_json = new JSONObject();
						alarm_json.put("alarm_id", alarm.id);
//						send((new D3Request("ALARM", alarm)).toString());
						send((new D3Request("ALARM_EVENTS", alarm_json)).toString());

						intent = new Intent(BROADCAST_ALARM_ADD_UPD_RMV);
						intent.putExtra("alarm_id", alarm.id);
						sendBroadcast(intent);
					}
					break;

				case "ALARM_OPEN":
					result = d3Response.getResult();
					if(result > 0){
						Alarm alarm1 = d3Response.getAlarm();
						intent = new Intent(BROADCAST_ALARM_OPEN_CLOSE);
						intent.putExtra("result", alarm1.id);
						intent.putExtra("action", Const.ALARM_OPEN_INTENT); // 1 - open
						sendBroadcast(intent);

						JSONObject alarmJSON = new JSONObject();
						alarmJSON.put("alarm_id", alarm1.id);
						alarmJSON.put("success" , 11); //TODO get saved result value
						alarmJSON.put("comment", "");
						send(new D3Request("ALARM_CLOSE", alarmJSON).toString());
					}
					break;

				case "ALARM_CLOSE":
					result = d3Response.getResult();
					if(result > 0){
						intent = new Intent(BROADCAST_ALARM_OPEN_CLOSE);
						intent.putExtra("result", result);
						intent.putExtra("action", Const.ALARM_CLOSE_INTENT); // 0 - close
						sendBroadcast(intent);
					}
					break;

				/* HANDLE EVENTS */

				case("EVENTS"):
					int max_id = dbHelper.getEventMaxId();
					long received_time = System.currentTimeMillis()/1000;

					if(d3Response.getResult() > 0 ){
						int count = d3Response.getCount();
						JSONArray jsonArray = d3Response.getDataField().getJSONArray("items");
						if(count!=jsonArray.length())
							count = jsonArray.length();

						if(d3Response.getID() == AFFECTS_REQUEST_ID)
						{
							dbHelper.updateAllEventsAffectStatus(0);
						}

						List<Event> events = new ArrayList<>();
						boolean existense = dbHelper.getEventsExistence();
						for (int i = 0; i < count; i++) {
							events = handleEvent(events, jsonArray.getJSONObject(i), max_id, received_time, existense, true);
						}

						/*26.01.2021 detect problems with noitifcations*/
						checkPushDelay(events);


						Func.log_d(LOG_TAG_EVENTS, "Received EVENTS total count - " + count);
						Func.log_d(LOG_TAG_EVENTS, "Go to handling queue after EVENTS");
						handleQueues();
					}
					break;

				case("EVENT"):
					max_id = dbHelper.getEventMaxId();
					received_time = System.currentTimeMillis()/1000;

					List<Event> events = new ArrayList<>();
					events = handleEvent(events, d3Response.getDataField(), max_id, received_time, true, false);

					/*26.01.2021 detect problems with noitifcations*/
					checkPushDelay(events);

					Func.log_d(LOG_TAG_EVENTS, "Received IPC EVENT");
					Func.log_d(LOG_TAG_EVENTS, "Go to handling queue after EVENT");
					handleQueues();
					break;

				/* HANDLE AFFECTS */

				case("AFFECT_RMV"):
					JSONObject affectData = d3Response.getDataField();
					Func.log_d(LOG_TAG, d3Response.getQ() + affectData.toString());
					int canceledAffect = affectData.getInt("id");
					dbHelper.updateEventAffectStatus(canceledAffect, 0);
					sendBroadcast(new Intent(BROADCAST_AFFECT));
					sendBroadcast(new Intent(BROADCAST_UPDATE_WIDGETS));
					break;

				/* HANDLE DEVICES */

				case("DEVICES"):
					switch (d3Response.getResult()) {
						case 1:
//							sendBroadcast(new Intent(BROADCAST_DEVICE_INFO_FETCH));
							Device[] devices = d3Response.getDevices();

							if (null != devices && devices.length > 0) {
								dbHelper.addDevices(devices);
								UserInfo userInfo = dbHelper.getUserInfo();
								for (Device device : devices) {
									D3Request d3Request = D3Request.createMessage(userInfo.roles, "SCRIPTS", getDeviceObject(device.id));
									if (null == d3Request.error) send(d3Request.toString());
								}
							} else {
								dbHelper.deleteDevicesWithInfo();
							}
							break;
						default:
							break;
					}
					sendBroadcast(new Intent(BROADCAST_DEVICE_ADD));
					break;

				case("DEVICE_SITE_RMV"):
					dbHelper.deleteDeviceSite(new Zone(0, d3Response.getSectionId(), d3Response.getDeviceId()));
//					send((new D3Request("ALARMS", getEventsObject(), EVENTS_REQUEST_ID)).toString());
					break;
				case("DEVICE_ADD"):
					Device device = d3Response.getDevice();
					if(null!=device){
						dbHelper.addDevice(device);
						send((new D3Request("EVENTS", getAffectsObject())).toString());
						send((new D3Request("ALARMS", getEventsObject(), EVENTS_REQUEST_ID)).toString());
					}
					sendBroadcast(new Intent(BROADCAST_DEVICE_ADD));
					break;
				case("DEVICE_RMV"):
					Intent intent2 = new Intent(BROADCAST_DEVICE_RMV);
					intent2.putExtra("device", d3Response.getId());
					sendBroadcast(intent2);
					getActualDataFromServer();
					break;

				case("DEVICE_ASSIGN_DOMAIN"):
					int siteAddResult = d3Response.getResult();
					if(siteAddResult > 0){
						getActualDataFromServer();
					}
					Intent siteAddResultIntent = new Intent(BROADCAST_DEVICE_ASSIGN_DOMAIN);
					siteAddResultIntent.putExtra("result", siteAddResult);
					sendBroadcast(siteAddResultIntent);
					break;

				case ("DEVICE_FORCE_ASSIGN"):
					result = d3Response.getResult();
					Intent deviceAssignResultIntent = new Intent(BROADCAST_DEVICE_FORCE_ASSIGN);
					deviceAssignResultIntent.putExtra("result", result);
					sendBroadcast(deviceAssignResultIntent);
					break;

				case ("DEVICE_REVOKE_DOMAIN"):
					int revokeResult = d3Response.getResult();
					Intent intentx = new Intent(BROADCAST_DEVICE_REVOKE_DOMAIN);
					intentx.putExtra("device", revokeResult);
					sendBroadcast(intentx);
					break;

				case("DEVICE_ASSIGN_SITE"):
					int assignResult = d3Response.getResult();
					Intent intentAssingDevice = new Intent(BROADCAST_DEVICE_ASSIGN_SITE);
					intentAssingDevice.putExtra("result", assignResult);
					sendBroadcast(intentAssingDevice);
					if(assignResult == 1)
					{
						getActualDataFromServer();
					}
					break;
				case("DEVICE_REVOKE_SITE"):
					if(d3Response.getResult() == 1)
					{
						send((new D3Request("SITES", new JSONObject())).toString());
					}
					break;

//				case("DEVICE_USER_SET"):
//					intent = new Intent(BROADCAST_DEVICE_USER_SET);
//					intent.putExtra("result", d3Response.getResult());
//					sendBroadcast(intent);
//					/*TODO*/
//					break;
				case("DEVICE_USER_RMV"):
					JSONObject deletedUserObject = d3Response.getDataField();
					if(null!=deletedUserObject){
						int index = deletedUserObject.getInt("user_index");
						int device_id = deletedUserObject.getInt("device_id");
						dbHelper.deleteDeviceUser(device_id, index);
						intent = new Intent(BROADCAST_DEVICE_USER_RMV);
						intent.putExtra("local_user_id", index);
						intent.putExtra("device_id", device_id);
						sendBroadcast(intent);
					}
					break;

				case("DEVICE_USER_UPD"):
					User user = d3Response.getUser();
					if(null!=user){
						dbHelper.addUser(user);
					}
					sendBroadcast(new Intent(BROADCAST_DEVICE_USER_UPD));
					break;

				case ("DEVICE_FW_UPD"):
					dbHelper.updateDeviceVersion(d3Response.getId(), d3Response.getConfigVersion());
					sendBroadcast(new Intent(BROADCAST_DEVICE_VERSION_UPDATE));
					break;
				/* HANDLE OPERATORS */

				case("OPERATORS"):
					switch(d3Response.getResult()){
						case 0:
							break;
						case 1:
							data = d3Response.getDataField();
							int count = d3Response.getCount();
							JSONArray operatorsArray = data.getJSONArray("items");
							if(count!=operatorsArray.length())
								count = operatorsArray.length();
							LinkedList<Operator> operators = new LinkedList<>();
							for(int i  = 0; i < count; i++){
								JSONObject operatorObject = operatorsArray.getJSONObject(i);
								operators.add(new Operator(operatorObject));
							}
							dbHelper.addOperators(operators);
							sendBroadcast(new Intent(BROADCAST_OPERATORS));
							break;
						default:
							break;
					}
					break;
				case ("OPERATOR_UPD"):
					Intent i = new Intent(BROADCAST_OPERATOR_UPD);
					i.putExtra("user", d3Response.getOpID());
					i.putExtra("result", d3Response.getResult());
					sendBroadcast(i);
					send((new D3Request("OPERATORS", new JSONObject())).toString());
					break;
				case ("OPERATOR_RMV"):
					i = new Intent(BROADCAST_OPERATOR_RMV);
					i.putExtra("user", d3Response.getOpID());
					i.putExtra("result", d3Response.getResult());
					sendBroadcast(i);
					send((new D3Request("OPERATORS", new JSONObject())).toString());
					break;
				case("OPERATOR_SET"):
					i = new Intent(BROADCAST_OPERATOR_SET);
					i.putExtra("result", d3Response.getResult());
					sendBroadcast(i);
					break;
				case("OPERATOR_DEL"):
					i = new Intent(BROADCAST_OPERATOR_DEL);
					i.putExtra("result", d3Response.getResult());
					sendBroadcast(i);
					break;

				/* HANDLE CONNECTION CHANGES */

				case("CONNECTION_DROP"):
					dropCredentials(false); //! api connection drop
					Intent apiConnectionDropIntent  = new Intent(BROADCAST_CONNECTION_DROP);
					apiConnectionDropIntent.putExtra("AppName", getPackageName());
					apiConnectionDropIntent.putExtra("from", "server");
					apiConnectionDropIntent.putExtra("saveData", true);
					sendBroadcast(apiConnectionDropIntent);
					break;

				/* HANDLE COMMANDS*/

				case("COMMAND_SET"):
					int command_id = d3Response.getResult();
					int command_inner_id = d3Response.getCommandInnerId();
					sendIntentCommandBeenSend(0, command_id);
					dbHelper.addCommandById(new Command(command_id, command_inner_id));
					break;

				case("SITE_COMMAND"):
					switch(d3Response.getResult()){
						case -11:
							break;
						default:
							break;
					}
					break;

				case("COMMANDS"):
					break;

				case("COMMAND_RESULT_UPD"):
					Command commandUPD = d3Response.getCommandFromIPC();
					if(null!=commandUPD){
						dbHelper.addCommandById(commandUPD);
						if (user_id == d3Response.getOpID())
						{
							Intent comandResultIntent = new Intent(BROADCAST_COMMAND_RESULT_UPD);
							comandResultIntent.putExtra("result", commandUPD.result);
							sendBroadcast(comandResultIntent);
						}
					}
					break;

				case("REFERENCE"):
					int idd = d3Response.getResult();
					if(idd > 0)
					{
						String type = d3Response.getUpdatedCommandType();
						Command command = new Command(idd, type);
						dbHelper.addCommandById(command);
					}
					break;

				case("REFERENCES"):
					result = d3Response.getResult();
					if(result > 0)
					{
						String refType = d3Response.getRefType();
						switch (refType){
							case "command":
								Command[] commands = d3Response.getCommandsFromReference();
								if(commands != null){
									for (Command command:commands)
										{
											dbHelper.addCommandById(command);
										}
									}
								break;
							case "name":
								Operator[] operators = d3Response.getOperatorsFromReference();
								if(null!=operators){
									for(Operator operator:operators){
										dbHelper.addOperatorByReference(operator);
									}
								}
								break;
							default:
								break;
						}
					}
					break;

				/*SITE DELEGATION*/

				case ("DELEGATE_NEW"):
					String codeString = null;
					if(d3Response.getResult() > 0)
					{
						codeString = d3Response.getDelegationCode();
						Intent intentDelegationCode = new Intent(BROADCAST_DELEGATE_NEW);
						intentDelegationCode.putExtra("user_code", codeString);
						sendBroadcast(intentDelegationCode);
					}
					break;

				case ("DELEGATE_REQ"):
					int delegarionResult = d3Response.getResult();
					if(delegarionResult > 0){
						getActualDataFromServer();
					}
					Intent delegationResultIntent = new Intent(BROADCAST_DELEGATE_REQ);
					delegationResultIntent.putExtra("result", d3Response.getResult());
					sendBroadcast(delegationResultIntent);
					break;
				case ("OPERATOR_SAFE_SESSION"):
					Intent safeSessionIntent = new Intent(BROADCAST_OPERATOR_SAFE_SESSION);
					if(d3Response.getResult()>0){
						String session = d3Response.getSafeSession();
//						dbHelper.setUserInfoSafeSession(session);
						safeSessionIntent.putExtra("session", session);
					}else{
						safeSessionIntent.putExtra("error", d3Response.getResult());
					}
					sendBroadcast(safeSessionIntent);
					break;

				/*CAMERAS*/
				case ("IV_GET_TOKEN"):
					/*
					* TODO
					* update user_info
					* and send broadcast
					* */
//					String iv_token = "100-U20015172-e8d1-4390-99f6-78bee6387c02";
//					dbHelper.setUserInfoIVToken(iv_token);
					int iv_result = d3Response.getResult();
					if(iv_result > 0){
						JSONObject iv_data = d3Response.getDataField();
						String iv_token = iv_data.getString("access_token");
						dbHelper.setUserInfoIVToken(iv_token);
						getCameras(iv_token);
					}else{
						dbHelper.resetIVAllData();
					}
					Intent iv_intent = new Intent(BROADCAST_IV_GET_TOKEN);
					iv_intent.putExtra("result", iv_result);
					sendBroadcast(iv_intent);
					break;
				case ("IV_DEL_USER"):
					/*
					* TODO
					* delele token and cam info
					* */
					int iv_u_del_result = d3Response.getResult();
					if(iv_u_del_result > 0){
						dbHelper.resetIVAllData();
					}
					Intent iv_u_del_intent= new Intent(BROADCAST_IV_DEL_USER);
					iv_u_del_intent.putExtra("result", iv_u_del_result);
					sendBroadcast(iv_u_del_intent);
					break;
				case ("IV_GET_RELATIONS"):
					/*
					* TODO
					* update cameras
					* and send broadcast
					* */
					if(d3Response.getResult() > 0){
						dbHelper.resetCameraBindingForAllZones();
						JSONArray items = d3Response.getItems();
						if(null!=items){
							for(int i_a = 0; i_a < items.length(); i_a++){
								JSONObject json = items.getJSONObject(i_a);
								String cam = json.getString("cam_id");
								int c_device = json.getInt("device");
								int c_section = json.getInt("section");
								int c_zone = json.getInt("zone");
								dbHelper.updateCameraBindingForZone(c_device, c_section, c_zone, cam);
							}
						}
					}
					Intent intentGetRelations = new Intent(BROADCAST_IV_GET_RELATIONS);
					sendBroadcast(intentGetRelations);
					break;
				case ("IV_BIND_CAM"):

					/*TODO
					 update device_camera and
					  send broadcast
					* */

					if(d3Response.getResult() > 0){
						//						dbHelper.updateCameraBindingForZone();
						UserInfo userInfo = dbHelper.getUserInfo();
						JSONObject message = new JSONObject();
						try
						{
							message.put("device", 0);
							message.put("section", 0);
							message.put("zone", 0);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						D3Request d3Request = D3Request.createMessage(userInfo.roles, "IV_GET_RELATIONS", message);
						if(null==d3Request.error)send(d3Request.toString());
					}
					Intent intentCamBind = new Intent(BROADCAST_IV_BIND_CAM);
					intentCamBind.putExtra("result", d3Response.getResult());
					sendBroadcast(intentCamBind);
					break;
				case "IV_DEL_RELATION":
					int iv_del_rel_result = d3Response.getResult();
					if(iv_del_rel_result > 0){
						JSONObject json = d3Response.getDataField();
						int c_device = json.getInt("device");
						int c_section = json.getInt("section");
						int c_zone = json.getInt("zone");
						dbHelper.deleteCameraBindingForZone(c_device, c_section, c_zone);
					}
					Intent iv_del_rel_intent = new Intent(BROADCAST_IV_DEL_RELATION);
					iv_del_rel_intent.putExtra("result", iv_del_rel_result);
					sendBroadcast(iv_del_rel_intent);
					break;

				/*Handle device_site relations*/
				case "DEVICE_SITES":
					break;
				case("DEVICE_SITE_UPD"):
					/*TODO ADD DELEGATION LOGIC*/
					break;
				case "DELEGATE_REVOKE":
				/*TODO Handle delegate revoke and del site_delegates
				* непонятно для какого сайта приходит delegate_revoke,
				* поэтому как временное решение запрашиваем заново device_site
				* и полностью обновляем все site_delegates*/
				 	int delegateRevokeResult = d3Response.getResult();
					if(delegateRevokeResult> 0){
						/*delete from site_delegates*/
					}
					Intent delegateRevokeResultIntent = new Intent(BROADCAST_DELEGATE_REVOKE_RESULT);
					delegateRevokeResultIntent.putExtra("result", delegateRevokeResult);
					sendBroadcast(delegateRevokeResultIntent);
					break;
				/*Handle delegation info*/
				case "SITE_DELEGATES":
					int sdResult = d3Response.getResult();
					if(sdResult > 0 ){
						dbHelper.updateSitesDelegatesActual(sdResult, 0);
						int count = d3Response.getCount();
						JSONArray items = d3Response.getItems();
						if(null!=items)
						{
							if (count != items.length())
							{
								count = items.length();
							}
							for (int i_a = 0; i_a < count; i_a++)
							{
								JSONObject json = items.getJSONObject(i_a);
								int owner_domain_id = json.getInt("domain_id");
								int delegated_domain_id = json.getInt("delegated_domain");
								String owner_domain_name = json.getString("domain");
								String delegated_domain_name = json.getString("delegated_domain_name");
								int permissions = json.getInt("permissions");
								dbHelper.addDelegateToForSite(sdResult, delegated_domain_id, delegated_domain_name,  1, permissions);
							}
						}
						dbHelper.deleteAllSiteDelegatesWithActual(0);
					}
					sendBroadcast(new Intent(BROADCAST_SITE_DELEGATES));
					break;
				//SCRIPTS
				case "SCRIPTS":
					if(d3Response.getResult() > 0){
						Script[] scripts = d3Response.getDeviceScripts();
						if(null != scripts){
							dbHelper.addDeviceScripts(d3Response.getResult(), scripts);
						}
					}
					sendBroadcast(new Intent(BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV));
					break;
				case "SCRIPT_SET":
					if(d3Response.getResult() > 0){

					}
					sendBroadcast(new Intent(BROADCAST_SCRIPT_SET));
					break;
				case "SCRIPT_DEL":
					if(d3Response.getResult() > 0){
						dbHelper.deleteDeviceScriptByScriptId(d3Response.getResult());
					}
					sendBroadcast(new Intent(BROADCAST_SCRIPT_DEL));
					break;
				case "LIBRARY_SCRIPTS":
					result = d3Response.getResult();
					if(result > 0){
						Script[] scripts = d3Response.getLibraryScripts();
						if(null != scripts){
							dbHelper.deleteFirmwareScriptByFId(result);
							for(Script script: scripts){
								dbHelper.addFirmwareScript(result, script.libId);
							}
							dbHelper.addLibraryScripts(scripts);
						}

					}
					sendBroadcast(new Intent(BROADCAST_LIBRARY_SCRIPTS));
					break;
				case "LIBRARY_SCRIPT_IMPORT":
					//NO USE IN APP
					break;
				case "DEVICE_SCRIPT_ADD":
				case "DEVICE_SCRIPT_UPD":
					Script script = d3Response.getDeviceScript();
					if(null!=script)
					{
						dbHelper.addDeviceScript(script);
						Intent intentScriptUPD = new Intent(BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
						intentScriptUPD.putExtra("device_id", script.device);
						sendBroadcast(intentScriptUPD);
					}
					break;
				case "DEVICE_SCRIPT_RMV":
					JSONObject scriptData = d3Response.getDataField();
					if(null!=scriptData)
					{
						dbHelper.deleteDeviceScript(scriptData.getInt("id"), scriptData.getInt("device_id"), scriptData.getInt("bind"));
						Intent intent3 = new Intent(BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
						intent3.putExtra("script_id", scriptData.getInt("id"));
						sendBroadcast(intent3);
					}
					break;
				default:
					break;
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void checkPushDelay(List<Event> events)
	{
		if(null!=events)
		{
			Func.log_d(LOG_TAG_EVENTS, "Events count to detect notification problems - " + events.size());
			if(events.size() > 1)
			{
				Collections.sort(events, (e1, e2) -> (int) (e1.time - e2.time));
			}
			if (events.size() > 0)
			{
				Func.log_d(LOG_TAG_EVENTS, "Problems with notifications detected ");
				Func.log_d(LOG_TAG_EVENTS, "Min time: " + events.get(0).time + "; current time: " + System.currentTimeMillis()/1000);
				prefUtils.setNotificationsProblem(true);
				prefUtils.setNotificationsProblemMinTime(events.get(0).time);
				prefUtils.setNotificationsProblemDifTime(System.currentTimeMillis()/1000 - events.get(0).time);

				try
				{
					JSONObject jo = new JSONObject();
					jo.put("reason", "Notification problems");
					jo.put("delayed events count", events.size());
					UserInfo userInfo = DBHelper.getInstance(this).getUserInfo();
					if (null != userInfo) jo.put("user", userInfo.id);
					String phoneI = Build.MANUFACTURER + ", " + Build.VERSION.CODENAME + ", " + Build.VERSION.RELEASE + ", " + Build.VERSION.SDK_INT;
					jo.put("device", phoneI);
					String timeI = "Min time: " + prefUtils.getNotificationProblemMinTime() + ", Dif time: " + prefUtils.getNotificationProblemDifTime();
					jo.put("time", timeI);
					jo.put("foreground service", Func.isServiceRunningInForeground(this, D3Service.class)? "enabled": "disabled");

					Func.log_v(LOG_TAG + "WebHelper", jo.toString());
					WebHelper.getInstance().setLogs(jo.toString());

				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	private List<Event> handleEvent(List<Event> events, JSONObject jsonObject, int max_id, long received_time, boolean exist, boolean multi){
		Event event = new Event(jsonObject);

		event.exist = exist; //to know, should we speak about all events in EVENTS(to skip first EVENTS request)
		boolean eventDisplay = event.isShouldBeDisplayedAtAll(dbHelper, roles, sp);

		if(exist)
		{
			if (event.affect == 1) //add for trinket role
			{
				dbHelper.addAffect(event, event.displayAffect(roles) ? 1 : 0, eventDisplay ? 1 : 0);
				sendBroadcast(new Intent(BROADCAST_AFFECT));
				sendBroadcast(new Intent(BROADCAST_UPDATE_WIDGETS));
			} else
			{
//                if ((event.id > max_id) || (event.id < min_id)) 1611677678
//                {
				dbHelper.addNewEvent(event, eventDisplay ? 1 : 0);
//                }
			}
			if (event.id > max_id)
			{
				if (4 == event.classId)
				{
					if (!event.jdata.equals(""))
					{
						try
						{
							JSONObject jdataObject = new JSONObject(event.jdata);
							JSONArray sectionsArray = jdataObject.getJSONArray("sections");
							if (null != sectionsArray && sectionsArray.length() != 0)
							{
								for (int i = 0; i < sectionsArray.length(); i++)
								{
									int id = sectionsArray.getInt(i);
									Section section = dbHelper.getDeviceSectionById(event.device, id);
									if ((null != section) && (section.armed < event.time))
									{
										dbHelper.updateSectionArmStatus(event.device, id, event.active, event.time);
									}
								}
							}

						} catch (JSONException e)
						{
							e.printStackTrace();
						}
					}
					sendBroadcast(new Intent(BROADCAST_AFFECT));
					sendBroadcast(new Intent(BROADCAST_UPDATE_WIDGETS));
				}

				if (5 == event.classId && 23 == event.reasonId)
				{
					Intent intent = new Intent(BROADCAST_INTERACTIVE_STATUS);
					intent.putExtra("device", event.device);
					intent.putExtra("status", event.active);
					intent.putExtra("jdata", event.jdata);
					sendBroadcast(intent);
				}
				if (!multi &&(((3 == event.classId) && (26 == event.reasonId) && (2 == event.active)) || ((5 == event.classId) && (26 == event.reasonId))))
				{
					Intent intent = new Intent(BROADCAST_SITE_NOT_READY);
					intent.putExtra("eventId", event.id);
					sendBroadcast(intent);
				}

				/*fixed 29.06.2018*/
				if ((5 == event.classId) && (24 == event.reasonId))
				{
					if (0 == event.active)
					{
						String intentFilter = BROADCAST_ZONE_ADD;
						if (0 == event.zone)
						{
							intentFilter = BROADCAST_SECTION_ADD;
						}
						Intent intent = new Intent(intentFilter);
						intent.putExtra("device", event.device);
						intent.putExtra("section", event.section);
						intent.putExtra("zone", event.zone);
						sendBroadcast(intent);
					} else
					{
						JSONObject deviceObject = new JSONObject();
						try
						{
							deviceObject.put("device_id", event.device);
							deviceObject.put("device_section", event.section);
							deviceObject.put("zone", event.zone);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						send((new D3Request("ZONE_DEL", deviceObject)).toString());
					}
				}

				if ((5 == event.classId) && (25 == event.reasonId))
				{
					if (0 == event.active)
					{
						Intent intent = new Intent(BROADCAST_USER_ADD);
						intent.putExtra("deviceId", event.device);
						sendBroadcast(intent);
					}
				}

				/*add notification status*/
				event = getNotificationStatus(getApplicationContext(), event);
				if(event.notify){
					if(ActivityLifecycleCall.getOpenCount() == 0){
						eventSubject.onNext(event);
					}

					eventsToStatusBarList.add(event);

					/*to list to detect problems with notifications*/
					if (null != events && received_time - event.time > EVENT_DELAY_INTERVAL) {
						events.add(event);
						Func.log_d(D3Service.LOG_TAG, "Delayed event - id:" + event.id
								+ " ; event time:" + event.time
								+ " ; received time:" + received_time
								+ "; affect:" + event.affect
								+ "; max_id: " + max_id);
					}
				}else{
					Func.log_d(D3Service.LOG_TAG, "Not to status bar list event (notification disabled) - id:" + event.id
							+ " ; event time:" + event.time
							+ "; affect:" + event.affect
							+ "; max_id: " + max_id);
				}
			}else {
				Func.log_d(D3Service.LOG_TAG, "Not to status bar list event (recently in DB) - id:" + event.id
						+ " ; event time:" + event.time
						+ "; affect:" + event.affect
						+ "; max_id: " + max_id);
			}
			if(event.classId < Const.CLASS_ATTENTION  && event.active == 1){
				Alarm alarm = dbHelper.getAlarmByDeviceId(event.device);
				if(null!=alarm)
				{
					dbHelper.addAlarmEvent(alarm.id, event.id);
					Intent intent = new Intent(BROADCAST_ALARM_EVENTS);
					intent.putExtra("alarm_id", alarm.id);
					sendBroadcast(intent);
				}
			}
		}else{
			dbHelper.addNewEvent(event, eventDisplay ? 1 : 0);
		}

		if ((15 == event.classId) && (10 == event.reasonId))
		{
			try
			{
				commandReferencesList.add(new JSONObject(event.jdata).getInt("command_id"));
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
			try
			{
				operatorReferencesList.add(new JSONObject(event.jdata).getInt("operator_id"));
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

		if ((15 == event.classId) && (1 == event.reasonId || 2 == event.reasonId))
		{
			eventsWithClipList.add(event);
		}
		return events;
	}

	private Event getNotificationStatus(Context context, Event event)
	{
		if(Func.getNotificationStatus(context))
		{
			//check notif status for classes for android < Oreo
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
				if(event.isShowable(sp)){
					event.notify = true;
				}
			}else{
				event.notify = notificationUtils.getClassChannel(event.classId).getImportance()
						> NotificationManager.IMPORTANCE_NONE;
			}
		}
		return event;
	}

	private void handleQueues()
	{
		if((roles&Const.Roles.TRINKET) == 0) // add for trinket role
			showEventsAsNotification();
		handleCollectedJData();
		sendBroadcast(new Intent(BROADCAST_EVENT));
	}

	private void handleCollectedJData()
	{
		//add clip info in events
		if(eventsWithClipList.size() > 0){
			handleEventsWithClip();
			eventsWithClipList.clear();
		}

		//command & operator references
		if(commandReferencesList.size() > 0 || operatorReferencesList.size() > 0){
			handleReferences();
			commandReferencesList.clear();
			operatorReferencesList.clear();
		}

	}

	private void handleReferences(){
		//09.10.2019 Добавляем очередь обработки референсов
		JSONObject jsonObj;
		if(commandReferencesList.size() > 0)
		{
			jsonObj = new JSONObject();
			try
			{
				jsonObj.put("name", "command");
				JSONArray jsonArray = new JSONArray();
				for (int i = 0; i < commandReferencesList.size(); i++)
				{
					jsonArray.put(commandReferencesList.get(i));
				}
				jsonObj.put("id", jsonArray);
				send((new D3Request("REFERENCES", jsonObj)).toString());
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}
		if(operatorReferencesList.size() > 0){
			jsonObj = new JSONObject();
			try
			{
				jsonObj.put("name", "name");
				JSONArray jsonArray = new JSONArray();
				for (int i = 0; i < operatorReferencesList.size(); i++)
				{
					jsonArray.put(operatorReferencesList.get(i));
				}
				jsonObj.put("id", jsonArray);
				send((new D3Request("REFERENCES", jsonObj)).toString());
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}

	}

	private void handleEventsWithClip(){
		for(Event event: eventsWithClipList){
			String recordData = event.jdata;
			dbHelper.updateEventJdata(recordData);
		}
		Intent intent = new Intent(BROADCAST_NEW_RECORD);
		sendBroadcast(intent);
	}

	public void sendIntentCommandBeenSend(int value, int result){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		intent.putExtra("result", result);
		sendBroadcast(intent);
	}


/*--------------------------------------------------------------------------------------------------*/

	// should be called upon list changed
	private void showEventsAsNotification()
	{
		if(null != eventsToStatusBarList && eventsToStatusBarList.size() > 0){
			Func.log_v(LOG_TAG, "Events count to status bar list - " + eventsToStatusBarList.size());
			showNotification(NOTIF_TYPE.EVENT_FROM_D3, eventsToStatusBarList);
			eventsToStatusBarList.clear();
		}else{
			Func.log_v(LOG_TAG, "Events count to status bar list - 0 (list is null or empty)" );
		}
	}



/*--------------------------------------------------------------------------------------------------*/
	private void showNotification(NOTIF_TYPE notif_type, LinkedList<Event> events)
	{
//		Context context = App.getContext();
		Context context = getBaseContext();
		switch (notif_type){
			case CONNECTION:

				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
					notificationUtils.createChannel(getString(R.string.SYSTEM_NOTIFICATIONS), getString(R.string.SYSTEM_NOTIFICATIONS), NotificationManager.IMPORTANCE_LOW);
				}
				NotificationManagerCompat systemNotificationManagerCompat = NotificationManagerCompat.from(context);
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
				{
					if(!systemNotificationExist)
					{
						Notif notif = new Notif(context);
						notif.channelId = getString(R.string.SYSTEM_NOTIFICATIONS);
						systemNotificationManagerCompat.notify(SYSTEM_NOTIFICATIONS_GROUP_ID, createGroupNotificationBuilder(context, SYSTEM_NOTIFICATION_GROUP, notif).build());
						systemNotificationExist = true;
					}
				}
				systemNotificationManagerCompat.notify(SYSTEM_NOTIFICATION_ID, createSystemNotificationBuilder(context).build());

				break;

			case EVENT_FROM_D3:
				NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context);
				if(events.size() > 0)
				{
					if (events.size() > 1)
					{
						for(Event e : events){
							handleEventNotification(e, context, notificationManagerCompat);
						}

					} else
					{
						handleEventNotification(events.get(0), context, notificationManagerCompat);
					}
				}
				Func.log_d(D3Service.LOG_TAG, "Showable events count - " + events.size());
				break;
		}
	}

	private void handleEventNotification(Event event, Context context, NotificationManagerCompat notificationManagerCompat)
	{
		Notif notif = new Notif(context);
		if(null!=event){
			notif.event_id = event.id;
//			notif.large_icon = BitmapFactory.decodeResource(context.getResources(), event.getAffectIcon(context));
			notif.large_icon = Func.drawableToBitmap(getRes(context).getDrawable(event.getNotificationIcon(context)));
			notif.icon = event.getNotificationIcon(context);
			Site[] sites = dbHelper.getSitesByDeviceSection(event.device, event.section);
			notif.sites = sites;
			if(null!=sites)
			{
				String[] siteNames = new String[sites.length];
				for (int i = 0; i < siteNames.length; i++)
				{
					siteNames[i] = sites[i].name;
				}
				notif.sitename = Arrays.toString(siteNames);
				notif.sitename = notif.sitename.substring(1, notif.sitename.length() - 1);

				//set event location
				notif.zone = event.getEventLocationForTts(context);
				//to show event location inside event description
//				if(null!=notif.zone)notif.text = "<b>" + notif.zone + "</b>\n\n" + " ";
			}
			notif.text = event.explainEventRegDescriptionLite(context);
			if(Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
			{
				doPopUpActionsWithCurrentEvent(sp, context, event, notif.text);
			}

			notif.classId = event.classId;

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				if(null!=d3XProtoConstEvent)
				{
					EClass eClass = d3XProtoConstEvent.getClassById(notif.classId);
					notif.channelName = eClass.caption;
					notif.channelId = notificationUtils.getClassChannelId(eClass.id);
				}
//				if(notificationService.getNotificationChannel(notif.channelId) == null)
//				{
//					NotificationChannel mChannel = new NotificationChannel(notif.channelId, notif.channelName, NotificationManager.IMPORTANCE_HIGH);
//					notificationService.createNotificationChannel(mChannel);
//				}
			}

			// we should create notification group if notifications are not existed yet and new notification should be shown

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
			{
				if (!eventsNotificationExisted)
				{
					notificationManagerCompat.notify(EVENTS_NOTIFICATION_GROUP_ID, createGroupNotificationBuilder(context, NOTIFICATION_GROUP, notif).build());
					eventsNotificationExisted = true;
				}
			}

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			{
                showAlarmStyleNotification(event, true);
                notificationManagerCompat.notify(Func.getNotificationId(context), createEventNotificationBuider(context, sp, notif).build());
			} else {
				if(prefUtils.getClassNotificationStatus(notif.classId))
				{
					showAlarmStyleNotification(event, false);
					notificationManagerCompat.notify(EVENT_NOTIFICATION_ID, createEventNotificationBuider(context, sp, notif).build());
				}
			}
		}
	}

	private void showAlarmStyleNotification(Event event, boolean isOreo) {
		if(null!=event)
		{
			int flag = Integer.parseInt(prefUtils.getClassNotificatonAlarmMode(event.classId));
			if (!ActivityLifecycleCall.getIsFullNotifActivityLaunch() && !Func.isAppLaunch(getApplicationContext()))
			{
				switch (flag)
				{
					case Const.NOTIF_ALARM_TYPE_NONE:
						break;
					case Const.NOTIF_ALARM_TYPE_INSTANT:
						startAlarmNotificationActivity(event, isOreo);
						break;
					case Const.NOTIF_ALARM_TYPE_REPEAT:
						startAlarmNotifAlarmManager(this, event);
						break;
				}
			}
		}
	}

	private void startAlarmNotificationActivity(Event event, boolean isOreo)
	{
		Func.log_i(LOG_TAG, "Start Alarm Notification Activity");
		Intent fullIntent = new Intent(this, NFullScreenNotifActivity.class);
		fullIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		fullIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		fullIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		fullIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		fullIntent.putExtra("isOreo", isOreo);
		if(null!=event)fullIntent.putExtra("eventClass", event.classId);
		startActivity(fullIntent);
	}

	private void startAlarmNotifAlarmManager(Context context, Event event)
	{
		if(!notifAlarmAM)
		{
			long interval = prefUtils.getClassNotifAlarmInterval(event.classId) * 60000L;
			Func.log_i(LOG_TAG, "Create alarm notification alarm manager, period: " + interval + ", event: " + event.id);
			AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
			{
				alarm.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + interval, getAMPendingIntent(context, REQUEST_CODE_ALARM_NOTIFICATION));
			} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
			{
				alarm.setAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + interval, getAMPendingIntent(context, REQUEST_CODE_ALARM_NOTIFICATION));
			} else
			{
				alarm.setWindow(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + interval, ALARM_WINDOW, getAMPendingIntent(context, REQUEST_CODE_ALARM_NOTIFICATION));
			}
			notifAlarmAM = true;
		}
	}

	private void stopAlarmNotifAlarmManager(Context context){
		Func.log_i(LOG_TAG,   "Stop alarm notification alarm manager");
		AlarmManager alarm = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
		alarm.cancel(getAMPendingIntent(context, REQUEST_CODE_ALARM_NOTIFICATION));
		notifAlarmAM = false;
	}

	private  NotificationCompat.Builder createGroupNotificationBuilder(Context context, String groupKey, Notif notif){
		Intent eventNotificationIntent = new Intent(context, StartUpActivity.class);
		eventNotificationIntent.putExtra("FROM", OPEN_HISTORY_FROM_STATUS_BAR);
		PendingIntent eventNotifContentIntent = PendingIntent.getActivity(context, EVENTS_NOTIFICATION_GROUP_ID, eventNotificationIntent, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE: 0);

		NotificationCompat.Builder groupNotificationBuilder = new NotificationCompat.Builder(this, notif.channelId);
		return groupNotificationBuilder
						.setSmallIcon(R.drawable.ic_home_status_bar_filled)
						.setColor(ContextCompat.getColor(context, R.color.brandColorDark))
						.setStyle(new NotificationCompat.BigTextStyle().bigText(""))
						.setContentIntent(eventNotifContentIntent)
						.setDeleteIntent(createOnDismissedIntent(context, 1001))
						.setGroup(groupKey)
						.setGroupSummary(true);
	}

	private NotificationCompat.Builder createEventNotificationBuider(Context context, SharedPreferences sp, Notif notif){

		Spanned eventData = Html.fromHtml(notif.text);
		NotificationCompat.Builder eventNotifBuilder = new NotificationCompat.Builder(this, notif.channelId);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
		{
			boolean notif_vibrate = prefUtils.getClassNotificationVibro(notif.classId);
			boolean notif_sound = prefUtils.getClassNotificationSound(notif.classId);
			String notif_led = prefUtils.getClassNotificationLed(notif.classId);
			String notif_sound_ring = prefUtils.getClassNotificationRingtone(notif.classId);
			boolean notif_priority = prefUtils.getClassNotificationPriority(notif.classId);

			/*set_vibro*/
			eventNotifBuilder.setVibrate(new long[]{0});
			if ((!(((App) context.getApplicationContext()).getStatus() && (!prefUtils.getClassNotificationInAppVibro(notif.classId)))) && (notif_vibrate))
			{
				eventNotifBuilder.setVibrate(new long[]{100, 300, 100, 300});
			}
			/*set priority*/
			if (notif_priority)
			{
				eventNotifBuilder.setPriority(Notification.PRIORITY_HIGH);
			} else
			{
				eventNotifBuilder.setPriority(Notification.PRIORITY_DEFAULT);
			}
			/*set_lights*/
			if (!notif_led.equals("0"))
			{
				eventNotifBuilder.setLights((getResources().getIntArray(R.array.hub_led_values_rgb))[Integer.valueOf(notif_led)], 500, 500);
			}
			/*set_sound*/
			if ((!(((App) context.getApplicationContext()).getStatus() && (!prefUtils.getClassNotificationInAppSound(notif.classId)))) && (notif_sound))
			{
				if (!notif_sound_ring.equals(getResources().getString(R.string.SUMMARY_DEFAULT)))
				{
					eventNotifBuilder.setSound(Uri.parse(notif_sound_ring));
				} else
				{
					Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
					eventNotifBuilder.setSound(alarmSound);
				}
			}

			if(prefUtils.getClassNotificationTts(notif.classId))
			{
				int site_count = dbHelper.getSitesCount();
				d3Tss.speak(android.text.Html.fromHtml(notif.text).toString()
						+ ( site_count> 1 ? (notif.sites.length < 2 ? ". " + getRes(context).getString(R.string.N_TTS_OBJECT) + " -- " + notif.sitename : ". " + getRes(context).getString(R.string.N_TTS_OBJECTS) + " -- " + notif.sitename): "")
						+ (notif.zone != null  ? ". " + notif.zone : ""));
			}
		}else{
			if(prefUtils.getClassNotificationTts(notif.classId))
			{
				int site_count = dbHelper.getSitesCount();
				String t = android.text.Html.fromHtml(notif.text).toString()
						+ ( site_count> 1 ? (null!=notif.sites ? (notif.sites.length < 2 ? ". " + getRes(context).getString(R.string.N_TTS_OBJECT) + " -- " + notif.sitename : ". "  + getRes(context).getString(R.string.N_TTS_OBJECTS) + " -- " + notif.sitename): "") : "")
						+ (notif.zone != null  ? ". " + notif.zone : "");
				d3Tss.speak(t);
			}
		}

		Intent eventNotificationIntent = new Intent(context, StartUpActivity.class);
		eventNotificationIntent.putExtra("FROM", OPEN_HISTORY_FROM_STATUS_BAR);
		PendingIntent eventNotifContentIntent
				= PendingIntent.getActivity(context,
				EVENTS_NOTIFICATION_GROUP_ID,
				eventNotificationIntent,
				android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE: 0);

		eventNotifBuilder
				.setContentIntent(eventNotifContentIntent)
				.setSmallIcon(R.drawable.ic_home_status_bar_filled)
				.setColor(ContextCompat.getColor(context, R.color.n_brand_blue))
				.setContentTitle(!notif.sitename.equals(getString(R.string.SITE_NULL)) ? notif.sitename + (notif.zone != null  ? " • " + notif.zone : ""): notif.title)
				.setContentText(eventData)
				.setStyle(new NotificationCompat.DecoratedCustomViewStyle())
				.setCustomContentView(getContentView(R.layout.n_notification_layout_small, notif))
				.setCustomBigContentView(getContentView(R.layout.n_notification_layout, notif))
//				.setContentTitle(!notif.sitename.equals(getString(R.string.SITE_NULL)) ? notif.sitename + (notif.zone != null  ? " • " + notif.zone : ""): notif.title)
//				.setContentText(eventData)

				.setWhen(System.currentTimeMillis())
				.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
				.setDeleteIntent(createOnDismissedIntent(context, 1001))
				.setAutoCancel(true);

		if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT){
			eventNotifBuilder.setGroup(NOTIFICATION_GROUP);
		}

		return eventNotifBuilder;
	}

	private RemoteViews getContentView(int layout, Notif notif)
	{
		RemoteViews contentView = new RemoteViews(getPackageName(), layout);
		contentView.setImageViewResource(R.id.image, notif.icon);
		contentView.setTextViewText(R.id.title, !notif.sitename.equals(getString(R.string.SITE_NULL)) ? notif.sitename + (notif.zone != null ? " • " + notif.zone : "") : notif.title);
		contentView.setTextViewText(R.id.text, Html.fromHtml(notif.text));
		return contentView;
	}

	private RemoteViews getContentView(int layout) {
		return new RemoteViews(getPackageName(), layout);
	}

	private NotificationCompat.Builder createSystemNotificationBuilder(Context context) {
		Intent statusNotificationIntent = new Intent(context, StartUpActivity.class);
		PendingIntent statusNotifContentIntent = PendingIntent.getActivity(context, 0, statusNotificationIntent, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE : 0);

		return new NotificationCompat.Builder(this, getString(R.string.SYSTEM_NOTIFICATIONS))
				.setContentIntent(statusNotifContentIntent)
				.setSmallIcon(R.drawable.ic_home_status_bar_filled)
				.setTicker(getString(R.string.application_name))
				.setContentTitle(getString(R.string.D3SERVICE_CONN_TRY))
				.setContentText(getString(R.string.N_NOTIFICATION_NO_CONNECTION))
				.setWhen(System.currentTimeMillis())
				.setGroup(SYSTEM_NOTIFICATION_GROUP)
				.setChannelId(getString(R.string.SYSTEM_NOTIFICATIONS))
				.setOngoing(true);
	}

	private NotificationCompat.Builder createServiceNotificationBuilder(Context context){

		NotificationCompat.Builder builder = new NotificationCompat.Builder(this, Func.getServiceChannelId(this));
//		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){
//			builder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
//					.setStyle(new NotificationCompat.BigTextStyle().bigText(getString(R.string.NOTIF_CONNECTION_SERVICE_ENABLED)));
//		}

		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://cloud.security-hub.ru/wiki/doku.php?id=%D1%81%D0%B5%D1%80%D0%B2%D0%B8%D1%81_%D1%83%D0%B2%D0%B5%D0%B4%D0%BE%D0%BC%D0%BB%D0%B5%D0%BD%D0%B8%D0%B9_security_hub"));
		PendingIntent pendingintent = PendingIntent.getActivity(context, SERVICE_NOTIFICATION_GROUP_ID, intent, android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M ? PendingIntent.FLAG_IMMUTABLE :0);

		return builder
				.setContentIntent(pendingintent)
				.setSmallIcon(R.drawable.ic_home_status_bar_filled)
				.setTicker(getString(R.string.application_name))
				.setContentTitle(getRes(context).getString(R.string.NOTIF_CONNECTION_SERVICE_ENABLED))
				.setContentText(getRes(context).getString(R.string.N_PRESS_TO_VIEW_MORE))
				.setWhen(System.currentTimeMillis())
				.setGroup(SERVICE_NOTIFICATION_GROUP)
				.setChannelId(Func.getServiceChannelId(this))
				.setOngoing(true)
				.setShowWhen(false);
	}

	private PendingIntent createOnDismissedIntent(Context context, int id)
	{
		Intent intent = new Intent(BROADCAST_NOTIFICATION_DISMISS);
		intent.putExtra("notificationId", id);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
		{
			return PendingIntent.getBroadcast(context.getApplicationContext(),
					id, intent, PendingIntent.FLAG_IMMUTABLE);
		}
		return PendingIntent.getBroadcast(context.getApplicationContext(),
				id, intent, 0);
	}

	private Event getMaxPriorityEvent(LinkedList<Event> events){
		int count = events.size();
		int minClass = 16; // EVENTCLASS_UNUSED
		Event event = null;
		if(0!=count)
			for (Event e : events)
			{
				if (e.classId < minClass) {
					minClass = e.classId;
					event = e;
				}
			}
		return event;
	}

//	private Event getMaxPriorityPopUpEvent(LinkedList<Event> events, SharedPreferences sp){
//		int count = events.size();
//		int minClass = 16;
//		Event event = null;
//		if(0!=count)
//			for (Event e : events)
//			{
//				if ((!sp.getString("notif_class_popup_" + e.classId, "0").equals("0"))&(e.classId < minClass)) {
//					minClass = e.classId;
//					event = e;
//				}
//			}
//		return event;	}

	private void doPopUpActionsWithCurrentEvent(SharedPreferences sp, Context context, Event event, String eventData)
	{
		String notif_popup = sp.getString("notif_class_popup_" + event.classId, "0");
		boolean popup = false;
		switch (notif_popup)
		{
			case "0":
				break;
			case "1":
				KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
				boolean locked = myKM.inKeyguardRestrictedInputMode();
				if(!locked){
					popup = true;
				}
				break;
			case "2":
				myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
				locked = myKM.inKeyguardRestrictedInputMode();
				if(locked){
					popup = true;
				}
				break;
			case "3":
				popup = true;
				break;
			default:
				break;
		}

		if (popup)
		{

			if(!((App)this.getApplicationContext()).getStatus())
			{
				Intent popupIntent = new Intent(BROADCAST_POPUP);
				popupIntent.putExtra("eventClass", event.classId);
				popupIntent.putExtra("eventDetector", event.detectorId);
				popupIntent.putExtra("eventReason", event.reasonId);
				popupIntent.putExtra("eventData", eventData);
//				popupIntent.putExtra("addCount", additionalCount);
				popupIntent.setClass(context, PopupActivity.class);
				popupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				context.startActivity(popupIntent);
			}
//			else{
//				sendBroadcast(popupIntent);
//			}
		}
	}



  	/*BIG ENDIAN*/
	public static int byteArrayToInt(byte[] b) {
		if(b.length == 8){
			return b[0] << 56 | (b[1] & 0xff) << 48 | (b[2] & 0xff) << 40 | (b[3] & 0xff) << 32 | b[4] << 24 | (b[5] & 0xff) << 16 | (b[6] & 0xff) << 8 | (b[7] & 0xff);
		}else
		{
			if (b.length == 4) return b[0] << 24 | (b[1] & 0xff) << 16 | (b[2] & 0xff) << 8 | (b[3] & 0xff);
			else if (b.length == 2) return 0x00 << 24 | 0x00 << 16 | (b[0] & 0xff) << 8 | (b[1] & 0xff);
		}
		return 0;
	}

	public static byte[] leIntToByteArray(int i) {
		final ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putInt(i);
		return bb.array();
	}

	public static byte[] beIntToByteArray(int i) {
		final ByteBuffer bb = ByteBuffer.allocate(Integer.SIZE / Byte.SIZE);
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.putInt(i);
		return bb.array();
	}

	public static byte[] intToBytes( final int i ) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.putInt(i);
		return bb.array();
	}

	public static  byte[] shortToBytes(final short i){
		ByteBuffer bb = ByteBuffer.allocate(2);
		bb.putShort(i);
		return bb.array();
	}

	public static  byte[] longToBytes(final long i){
		ByteBuffer bb = ByteBuffer.allocate(8);
		bb.putLong(i);
		return bb.array();
	}

	public static byte[] invertByte( byte[] bytes){
		for(int i = 0; i < bytes.length / 2; i++)
		{
			byte temp = bytes[i];
			bytes[i] = bytes[bytes.length - i - 1];
			bytes[bytes.length - i - 1] = temp;
		}
		return bytes;
	}

	public static  String decodeUTF8(byte[] bytes) {
		return new String(bytes, UTF8_CHARSET);
	}

	public static  String decodeUNICODE(byte[] bytes) {
		return new String(bytes, UCS_CHARSET);
	}

	public static  String decodeCP1251(byte[] bytes) {
		return new String(bytes, CP1251_CHARSET);
	}

	public static  String decodeLatin(byte[] bytes) {
		return new String(bytes, LATIN_CHARSET);
	}

	public void writeInFile(byte[] b){
		File file = new File(getCacheDir(), "incache.txt");
		try
		{
			OutputStream out = new FileOutputStream(file);
			try
			{
				out.write(b);
			} catch (IOException e)
			{
				e.printStackTrace();
			}
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}

	}

	public class myBinderClass extends Binder
	{
		public D3Service getService()
		{
			return D3Service.this;
		}
	}

	public static String md5(String in) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.reset();
			digest.update(in.getBytes());
			byte[] a = digest.digest();
			int len = a.length;
			StringBuilder sb = new StringBuilder(len << 1);
			for (int i = 0; i < len; i++) {
				sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
				sb.append(Character.forDigit(a[i] & 0x0f, 16));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
		return null;
	}

	/* Used to build and start foreground service. */
	private void runAsForeground(){
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			if (!serviceNotificationExist) {
				Func.log_d(Const.LOG_TAG, "run as foreground - service notification not exist");
				if(null == Func.getServiceChannel(getBaseContext())){
					recreateServiceNotificationChannel(true);
				}
				Notif notif = new Notif(getBaseContext());
				notif.channelId = Func.getServiceChannelId(this);
				NotificationManagerCompat.from(this).notify(SERVICE_NOTIFICATION_GROUP_ID, createGroupNotificationBuilder(this, SERVICE_NOTIFICATION_GROUP, notif).build());
				serviceNotificationExist = true;
			}
		}
		Notification notification = createServiceNotificationBuilder(getBaseContext()).build();
		startForeground(111, notification);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void recreateServiceNotificationChannel(boolean show) {
		Func.log_d(Const.LOG_TAG, "recreate service notification cahnnel");
		Func.deleteServiceChannel(this);
		Func.updateServiceChannelId(this);
		Func.createServiceChannel(this, show ? NotificationManager.IMPORTANCE_HIGH: NotificationManager.IMPORTANCE_NONE);
	}

	public ReplaySubject<Event> getEventSubject() {
		return eventSubject;
	}

	public void restartEventSubject() {
		eventSubject = ReplaySubject.create();
	}

	class Notif{
		public Site[] sites;
		String title;
		String sitename;
		String zone;
		String text;
		String popupText;
		String channelId ;
		String channelName ;
//		int additionalCount;
		Bitmap large_icon ;
		int icon;
		int classId = -1;
		boolean show;
		int event_id;

		public Notif(Context context){
			this.title = getRes(context).getString(R.string.application_name);
			this.sitename = getRes(context).getString(R.string.SITE_NULL);
			this.text = "";
			this.popupText = "";
			this.show = true;
//            this.additionalCount = 0;
			this.channelId = SECURITY_HUB_CHANNEL;
			this.channelName = getRes(context).getString(R.string.NOTIF_CHANNEL_ALL_MESS);
//			this.large_icon = BitmapFactory.decodeResource(getRes(context), R.drawable.ic_action_info_grey);
			this.large_icon = Func.drawableToBitmap(getRes(context).getDrawable(R.drawable.ic_action_info_grey));
			this.icon = R.drawable.ic_action_info_grey;
		}
	}

	private static Resources getRes(Context context)
	{
		return LocaleHelper.onAttach(context).getResources();
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		Func.log_d(D3Service.LOG_TAG, "onTaskRemoved called");
		stopTts();
	}

	protected D3KsSSLSocketFactory createAdditionalCertsSSLSocketFactory() {
		try {
			final KeyStore keyStore = KeyStore.getInstance("BKS");

			final InputStream in = this.getResources().openRawResource(R.raw.sh06082021);
			try {
				keyStore.load(in, BuildConfig.ps.toCharArray());
			} finally {
				in.close();
			}

			return new D3KsSSLSocketFactory(keyStore);

		} catch( Exception e ) {
			throw new RuntimeException(e);
		}
	}

}
