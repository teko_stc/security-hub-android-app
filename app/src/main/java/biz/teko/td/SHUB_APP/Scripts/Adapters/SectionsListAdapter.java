package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.R;

public class SectionsListAdapter extends ArrayAdapter
{
	private final Context context;
	private final Section[] sections;
	private final int curSection;
	private final LayoutInflater layoutInflater;

	public SectionsListAdapter(Context context, int resource, Section[] sections, int curSection)
	{
		super(context, resource);
		this.context = context;
		this.sections = sections;
		this.curSection = curSection;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		Section section = sections[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(section.name);
		if(curSection ==  section.id){
			view.setBackgroundColor(context.getResources().getColor(R.color.brandColorLightPattern));
			textView.setTextColor(context.getResources().getColor(R.color.brandColorAccentColor));
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
		}else{
			view.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
			textView.setTextColor(context.getResources().getColor(R.color.brandColorTextTitle));
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
		}
		return view;
	}

	public int getCount() {
		return sections.length;
	}

	public Section getItem(int position){
		return sections[position];
	}

	public long getItemId(int position){
		return position;
	}
}
