package biz.teko.td.SHUB_APP.Profile.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.text.InputFilter;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Profile.Activities.NProfileUsersActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NProfileUsersSettingsActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NDefaultListElement;

/**
 * Created by td13017 on 17.02.2017.
 */

public class NProfileUsersListAdapter extends ResourceCursorAdapter
{

	private final Context context;
	private final UserInfo userInfo;
	private D3Service myService;
	private DBHelper dbHelper;
	private int sending = 0;
	private NDefaultListElement cardItem;

	public NProfileUsersListAdapter(Context context, int layout, Cursor c, int flags)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();
	}

	public void update(Cursor cursor){
		this.changeCursor(cursor);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor)
	{
		Operator operator = new Operator(cursor);
		cardItem = view.findViewById(R.id.card_operator_item);
		cardItem.setTitle(operator.name);
		cardItem.setSummaryText(context.getResources().getStringArray(R.array.user_roles)[operator.getRole()]);
		cardItem.setImageLevel(operator.active);

		cardItem.setOnRootClickListener(new NDefaultListElement.OnRootClickListener()
		{
			@Override
			public void onRootClick()
			{
				Intent intent = new Intent(context, NProfileUsersSettingsActivity.class);
				intent.putExtra("user", operator.id);
				context.startActivity(intent);

//				final AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
//				adb.setTitle(operator.name);
//				String activeString = "";
//				if(1!=operator.active){
//					activeString = context.getResources().getString(R.string.PLA_ENABLE_USER);
//				}else{
//					activeString = context.getResources().getString(R.string.PLA_DISABLE_USER);
//				}
//				CharSequence[] items =  {context.getString(R.string.PLA_CHANGE_NAME), context.getString(R.string.PLA_CHANGE_LOGIN), context.getString(R.string.PLA_CHANGE_PASS), activeString, context.getString(R.string.CHANGE_USER_ROLES), context.getString(R.string.PLA_DELETE_USER)};
//				adb.setItems(items, new DialogInterface.OnClickListener()
//				{
//					@Override
//					public void onClick(DialogInterface dialog, int which)
//					{
//						switch (which){
//							case 0:
//								openNameChangeDialog(context, operator);
//								break;
//							case 1:
//								openLoginChangeDialog(context, operator);
//								break;
//							case 2:
//								openPassChangeDialog(context, operator);
//								break;
//							case 3:
//								AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
//								if(0==operator.active){
//									adb.setTitle(R.string.PLA_ENABLING_USER);
//									adb.setMessage(R.string.PLA_USER_ON_MESS);
//								}else{
//									adb.setTitle(R.string.PLA_DISABLING_USER);
//									adb.setMessage(R.string.PLA_USER_OFF_MESS);
//								}
//
//								adb.setPositiveButton("ок", new DialogInterface.OnClickListener()
//								{
//									@Override
//									public void onClick(DialogInterface dialog, int which)
//									{
//										JSONObject message = new JSONObject();
//										try
//										{
//											message.put("id", operator.id);
//											message.put("active", operator.active);
//										} catch (JSONException e)
//										{
//											e.printStackTrace();
//										}
//										D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_SET", message);
//										if(d3Request.error == null){
//											//										sendMessageToServer(d3Request);
//											dialog.dismiss();
//										}else{
//											Func.pushToast(context, d3Request.error, (Activity) context);
//										}
//									}
//								});
//								adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
//								{
//									@Override
//									public void onClick(DialogInterface dialog, int which)
//									{
//										dialog.dismiss();
//									}
//								});
//								adb.show();
//								break;
//							case 4:
//								openRolesChangeDialog(context, operator);
//								break;
//							case 5:
//								deleteOperatorDialog(context, operator);
//								break;
//							default:
//								break;
//						}
//						dialog.dismiss();
//					}
//				});
//				adb.show();
			}
		});
	}

	private void deleteOperatorDialog(final Context context, final Operator operator)
	{
		final UserInfo userInfo = dbHelper.getUserInfo();
		AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		adb.setTitle(R.string.PLA_DELETE_USER_TITLE);
		adb.setMessage(R.string.PLA_DELETE_USER_MESS);

		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
//				myService = getService();
				JSONObject message = new JSONObject();
				try
				{
					message.put("id", operator.id);
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
				D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_DEL", message);
				if(d3Request.error == null){
//					sendMessageToServer(d3Request);
					dialog.dismiss();
				}else{
					Func.pushToast(context, d3Request.error, (Activity) context);
				}
			}
		});
		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		adb.show();
	}

	private void openRolesChangeDialog(final Context context, final Operator operator)
	{
		final UserInfo userInfo = dbHelper.getUserInfo();
		AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		adb.setTitle(R.string.PLA_CHANGE_ROLES_TITLE);
		adb.setMessage(R.string.PLA_CHANGE_ROLES_MESS);

		View view =((NProfileUsersActivity) context).getLayoutInflater().inflate(R.layout.dialog_userroles_change, null);

		final RadioButton radioRead = (RadioButton) view.findViewById(R.id.radioRead);
		radioRead.setText(R.string.PUA_ONLY_READ);
		final RadioButton radioWrite = (RadioButton) view.findViewById(R.id.radioWrite);
		radioWrite.setText(R.string.PUS_READ_AND_WRITE);
		radioRead.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				radioRead.setChecked(true);
				radioWrite.setChecked(false);
			}
		});
		radioWrite.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				radioRead.setChecked(false);
				radioWrite.setChecked(true);
			}
		});
		if((operator.roles& Const.Roles.DOMAIN_OPER)>0){
			if((operator.roles&Const.Roles.DOMAIN_HOZ_ORG)>0){
				radioWrite.setChecked(true);
			}else{
				radioRead.setChecked(true);
			}
		}else{
			radioRead.setChecked(true);
		}
		adb.setView(view);

		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				int roles = 0;
				if(radioRead.isChecked()){
					roles = Const.Roles.DOMAIN_OPER;
				}else{
					if(radioWrite.isChecked()){
						roles = Const.Roles.DOMAIN_HOZ_ORG + Const.Roles.DOMAIN_OPER;
					}
				}
				JSONObject message = new JSONObject();
				try
				{
					message.put("id", operator.id);
					message.put("roles", roles);
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
				D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_SET", message);
				if(d3Request.error == null){
//					sendMessageToServer(d3Request);
					dialog.dismiss();
				}else{
					Func.pushToast(context, d3Request.error, (Activity) context);
				}
			}
		});

		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		adb.show();
	}

	private void openPassChangeDialog(final Context context, final Operator operator)
	{
		final UserInfo userInfo = dbHelper.getUserInfo();
		final AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		adb.setTitle(R.string.TITLE_USER_PASS_CHANGE);

		View view =((NProfileUsersActivity) context).getLayoutInflater().inflate(R.layout.dialog_userpassword_change, null);

		final EditText editNewPass  = (EditText) view.findViewById(R.id.editNewPass) ;
		final EditText editConfirmPass  = (EditText) view.findViewById(R.id.editConfirmPass) ;

		editNewPass.isCursorVisible();
		try
		{
			Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
			f.setAccessible(true);
			f.set(editNewPass, R.drawable.caa_cursor);
			f.set(editConfirmPass, R.drawable.caa_cursor);
		} catch (Exception ignored)
		{
		}
		editNewPass.setGravity(Gravity.CENTER);
		editConfirmPass.setGravity(Gravity.CENTER);

		int editNameMaxLength = 30;
		editNewPass.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
		editConfirmPass.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
		editNewPass.setTransformationMethod(new Func.AsteriskPasswordTransformationMethod());
		editConfirmPass.setTransformationMethod(new Func.AsteriskPasswordTransformationMethod());
		editNewPass.requestFocus();
		editNewPass.setOnKeyListener(new View.OnKeyListener()
		{

			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
				{
					// Perform action on Enter key press
					editNewPass.clearFocus();
					editConfirmPass.requestFocus();
					return true;
				}
				return false;
			}
		});

		adb.setView(view);

		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

			}
		});
		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		final AlertDialog ad = adb.create();
		ad.show();

		ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String newP = editNewPass.getText().toString();
				String newPC = editConfirmPass.getText().toString();
				if(!newP.isEmpty()){
					if(!newPC.isEmpty()){
						String newPH = Func.md5(newP);
						String newPCH = Func.md5(newPC);
						if(newPH.equals(newPCH)){
							JSONObject message = new JSONObject();
							try
							{
								message.put("id", operator.id);
								message.put("password", newPH);
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
							D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_SET", message);
							if(d3Request.error == null){
//								sendMessageToServer(d3Request);
								ad.dismiss();
							}else{
								Func.pushToast(context, d3Request.error, (Activity) context);
							}
						}else{
							Func.pushToast(context, context.getString(R.string.PLA_PASS_DONT_EQUALS), (NProfileUsersActivity) context);
						}
					}else{
						Func.pushToast(context, context.getString(R.string.PLA_CONFIRM_NEW_PASS), (NProfileUsersActivity) context);
					}
				}else{
					Func.pushToast(context, context.getString(R.string.PLA_ENTER_NEW_PASS), (NProfileUsersActivity) context);
				}
			}
		});

	}


	private void openLoginChangeDialog(final Context context, final Operator operator)
	{
		final UserInfo userInfo = dbHelper.getUserInfo();
		AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		adb.setTitle(R.string.PLA_LOGIN_CHANGE_MESS);
		adb.setMessage(R.string.PLA_ENTER_NEW_LOGIN);

		View view =((NProfileUsersActivity) context).getLayoutInflater().inflate(R.layout.dialog_userlogin_change, null);
		final EditText editLogin = (EditText) view.findViewById(R.id.editLogin);

		editLogin.setText(operator.login);
		editLogin.isCursorVisible();
		try
		{
			Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
			f.setAccessible(true);
			f.set(editLogin, R.drawable.caa_cursor);
		} catch (Exception ignored)
		{
		}
		editLogin.setGravity(Gravity.CENTER);

		int editNameMaxLength = 30;
		editLogin.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
		editLogin.requestFocus();

		adb.setView(view);

		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
			}
		});

		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Func.hideKeyboard((NProfileUsersActivity) context);
				dialog.dismiss();
			}
		});

		final AlertDialog ad = adb.create();
		ad.show();

		ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String login = editLogin.getText().toString();
				if((!login.isEmpty())&(!login.equals(operator.login)))
				{
					JSONObject message = new JSONObject();
					try
					{
						message.put("id", operator.id);
						message.put("login", login);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_SET", message);
					if(d3Request.error == null){
//						sendMessageToServer(d3Request);
						ad.dismiss();
					}else{
						Func.pushToast(context, d3Request.error, (Activity) context);
					}
				}else{
					Func.pushToast(context, context.getResources().getString(R.string.PLA_ENTER_NEW_LOGIN), (NProfileUsersActivity) context);
				}
			}
		});

	}

	private void openNameChangeDialog(final Context context, final Operator operator)
	{
		final UserInfo userInfo = dbHelper.getUserInfo();
		final AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		adb.setTitle(R.string.PLA_USERNAME_CHANGE);
		adb.setMessage(R.string.PLA_ENTER_NEW_NAME);
		View view =((NProfileUsersActivity) context).getLayoutInflater().inflate(R.layout.dialog_username_change, null);
		final EditText editName = (EditText) view.findViewById(R.id.editName);

		editName.setText(operator.name);
		editName.isCursorVisible();
		try
		{
			Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
			f.setAccessible(true);
			f.set(editName, R.drawable.caa_cursor);
		} catch (Exception ignored)
		{
		}
		editName.setGravity(Gravity.CENTER);

		int editNameMaxLength = 30;
		editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
		editName.requestFocus();

		adb.setView(view);


		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

			}
		});

		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Func.hideKeyboard((NProfileUsersActivity) context);
				dialog.dismiss();
			};
		});

		final AlertDialog ad = adb.create();
		ad.show();

		ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String name = editName.getText().toString();
				if((!name.isEmpty())&(!name.equals(operator.name)))
				{
					JSONObject message = new JSONObject();
					try
					{
						message.put("id", operator.id);
						message.put("name", name);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_SET", message);
					if(d3Request.error == null){
//						sendMessageToServer(d3Request);
						ad.dismiss();
					}else{
						Func.pushToast(context, d3Request.error, (Activity) context);
					}
				}else{
					Func.pushToast(context, context.getResources().getString(R.string.PLA_ENTER_NEW_NAME), (NProfileUsersActivity) context);
				}
			}
		});
	}

}
