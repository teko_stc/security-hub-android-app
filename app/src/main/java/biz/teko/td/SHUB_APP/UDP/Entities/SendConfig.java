package biz.teko.td.SHUB_APP.UDP.Entities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import biz.teko.td.SHUB_APP.UDP.LocalConnection;

public class SendConfig extends RequestBasePincoded {
    public final static byte _command = 5;
    private byte[] data;

    public SendConfig(byte[] data, int pin) {
        this.magic = LocalConnection.leIntToByteArray(pin);
        this.command = _command;
        this.data = data;
    }

    public byte[] getBytes() {
        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            outStream.write(magic);
            outStream.write(command);
            outStream.write(data);
            return outStream.toByteArray();
        } catch (IOException e) {e.printStackTrace();}
        return null;
    }
}
