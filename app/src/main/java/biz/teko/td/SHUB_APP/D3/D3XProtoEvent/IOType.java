package biz.teko.td.SHUB_APP.D3.D3XProtoEvent;

import org.simpleframework.xml.Attribute;

public class IOType
{
	@Attribute(name = "id")
	public int id;

	@Attribute(name = "caption")
	public String caption;
}
