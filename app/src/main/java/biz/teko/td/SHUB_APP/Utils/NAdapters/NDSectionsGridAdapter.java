package biz.teko.td.SHUB_APP.Utils.NAdapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.TreeMap;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Devices_N.Activities.DeviceActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Relays_N.Activities.RelayActivity;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Sections_N.Activities.SectionActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Models.DeviceInfo;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMainBarElement;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Utils.NViews.Textable;
import biz.teko.td.SHUB_APP.Zones_N.Activities.ZoneActivity;
import io.reactivex.disposables.CompositeDisposable;

public class NDSectionsGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	private final CompositeDisposable disposables = new CompositeDisposable();
	private final DBHelper dbHelper;
	private Cursor cursor;
	private final Context context;
	private final int sending = 0;
	private boolean sync = false;
	private int items;

	private TreeMap<Integer, Integer> sectionPositions = new TreeMap<>(); // position - id
//	private HashMap<Integer, Integer> sectionsDetectorCount = new HashMap<>(); // id - detectorCount

	public enum ItemType{
		SECTION, DETECTOR
	}

	public boolean isSync() {
		return sync;
	}

	public void setSync(boolean sync) {
		this.sync = sync;
	}

	private int init(){
		int sectionCount = cursor.getCount();
		int detectors = 0;
		for (int i = 0; i < sectionCount; i++) {
			if(i!=0) sectionPositions.put(i + detectors -1 ,i);
			//RED
			Section section = new Section(getItem(i));
			Cursor c = dbHelper.getAllZonesCursorForDeviceSectionNoController(section.device_id, section.id, Const.ALL);
			int detectorsCount = c.getCount();
			detectors+= detectorsCount;
//			sectionsDetectorCount.put(i,detectorsCount);
			//RED
		}
		return sectionCount + detectors -1;
	}

	public void update(Cursor cursor)
	{
		this.cursor = (cursor);
		sectionPositions.clear();
//		sectionsDetectorCount.clear();
		notifyDataSetChanged();
	}

	public NDSectionsGridAdapter(Context context, Cursor cursor)
	{
		super();
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.cursor = cursor;
	}

	public Cursor getItem(int position){
		cursor.moveToPosition(position);
		return cursor;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		if (viewType == ItemType.SECTION.ordinal())
			return SectionGridHolder.newInstance(parent);
		else if (viewType == ItemType.DETECTOR.ordinal()) {
			return DetectorGridHolder.newInstance(parent);
		}
		return null;
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public int getItemCount() {
		if (sectionPositions.size() == 0){
			return items = init();
		}else
			return items;
	}

	@Override
	public int getItemViewType(int position) {
		return sectionPositions.containsKey(position)?
				ItemType.SECTION.ordinal() : ItemType.DETECTOR.ordinal();
	}

	@Override
	public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
		super.onDetachedFromRecyclerView(recyclerView);
		disposables.dispose();
	}
	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		holder.setIsRecyclable(false);
		if (sectionPositions.containsKey(position)){
			applyData(fetchSectionData(sectionPositions.get(position)), ItemType.SECTION, holder);
		} else {
			int sectionPosition = 0;
			int sectionNum = 0;
			if(null!=sectionPositions.floorKey(position)){
				sectionPosition = sectionPositions.floorKey(position);
				sectionNum = sectionPositions.get(sectionPosition);
			}
			applyData(fetchDetectorData(sectionNum, position - sectionPosition - (sectionNum == 0 ? 0:1)), ItemType.DETECTOR, holder);
		}
	}

	private DeviceInfo fetchSectionData(int position) {
		Section section = new Section(getItem(position));
		Site site = null;
		if (null != section) {
			section.zone_count = dbHelper.getZonesCountSection(section.device_id, section.id);
			site = dbHelper.getSiteByDeviceSection(section.device_id, section.id);
		}
		return new DeviceInfo(null, null, null, site, section);
	}
	private DeviceInfo fetchDetectorData(int sectionNum, int position) {
		Section section = new Section(getItem(sectionNum));
		Cursor c = dbHelper.getAllZonesCursorForDeviceSectionNoController(section.device_id, section.id, Const.ALL);
		if (null != c && c.getCount() > 0) {
			//itemView.findViewById(R.id.nTextEmpty).setVisibility(View.GONE);
			c.moveToPosition(position);
			Zone zone = dbHelper.getZone(c);
			LinkedList<Event> affects = dbHelper.getAffectsListOnlyByZoneId(zone.device_id, zone.section_id, zone.id);
			return new DeviceInfo.DeviceInfoBuilder().setZone(zone).setAffects(affects).createDeviceInfo();
		} else {
			//чтобы не висело раздел пуст на 0м разделе, так как это не раздел вовсе
			return null;
		}
	}
	private void applyData(DeviceInfo info, ItemType itemType, RecyclerView.ViewHolder holder){
		if (itemType == ItemType.SECTION) {
			applySectionData(info, (SectionGridHolder) holder);
		} else if (itemType == ItemType.DETECTOR) {
			applyDetectorData(info, (DetectorGridHolder) holder );
		}
	}

	@MainThread
	private void applySectionData(DeviceInfo info, SectionGridHolder holder){
		Section section = info.getSection();
		Site site = info.getSite();
		if (section != null) {
			NWithAButtonElement sectionElement = holder.itemView.findViewById(R.id.nSectionElement);
			if (null != sectionElement) {
				if (section.id == 0) {
					(holder).hide();
				} else {
					(holder).reset();
					if(section.zone_count == 0){
						holder.setEmpty();
					}
					SType sType = section.getType();
					sectionElement.setNoArm(true);
					sectionElement.setDescImage(null != sType ? context.getResources().getDrawable(sType.getListIcon(context))
							: context.getResources().getDrawable(R.drawable.n_image_section_common_list_selector));
					if (null != sType) {
						switch (sType.id) {
							case Const.SECTIONTYPE_GUARD:
								if (section.armed > 0) {
									sectionElement.setArmed(true);
								} else {
									sectionElement.setDisarmed(true);
								}
								break;
							case Const.SECTIONTYPE_TECH_CONTROL:
								if (section.armed > 0) {
									sectionElement.setControl(true);
								} else {
									sectionElement.setNoControl(true);
								}
								break;
							default:
								break;
						}
					}
					sectionElement.setTitle(section.name);
					sectionElement.setBlack(true);

					sectionElement.setOnChildClickListener((action, option) -> {
						if (null != sType) {
							switch (sType.id) {
								case Const.SECTIONTYPE_GUARD:
									showControlDialog(sectionElement, site, section, R.layout.n_dialog_arm);
									break;
								case Const.SECTIONTYPE_TECH_CONTROL:
									showControlDialog(sectionElement, site, section, R.layout.n_dialog_control);
									break;
								default:
									openSection(site, section, sectionElement);
									break;
							}
						} else {
							openSection(site, section, sectionElement);
						}
					});
					sectionElement.setOnRootClickListener(() -> openSection(site, section, sectionElement));
				}
			}
		} else {
			(holder).hide();
		}
	}


	@MainThread
	private void applyDetectorData(DeviceInfo info, DetectorGridHolder vh){
		if (info == null){
			vh.hide();
		} else if(null!=info.getZone()){
			vh.itemView.setVisibility(View.VISIBLE);
			vh.reset();
			Zone zone  = info.getZone();
			NMainBarElement mainBarView = (NMainBarElement) vh.itemView;

			mainBarView.setTitle(zone.name);

			int bigImage = R.drawable.ic_relay_main;
			// set by default, if not relay it will change to zone icon
			int armStatus = Const.STATUS_NO_ARM_CONTROL;

			switch (zone.getDetector()){
				case Const.DETECTOR_SIREN:
					bigImage = R.drawable.n_image_siren_main_selector;
					break;
				case Const.DETECTOR_LAMP:
					bigImage = R.drawable.n_image_lamp_main_selector;
					break;
				case Const.DETECTOR_KEYPAD:
					bigImage = R.drawable.n_image_keypad_main_selector;
					break;
				case Const.DETECTOR_AUTO_RELAY:
					mainBarView.setAuto(true);
					Script script = dbHelper.getDeviceScriptsByBind(zone.device_id, zone.id);
					if(null!=script){
						mainBarView.setScripted(true);
						int scriptIcon = script.getIcon();
						if(0!=scriptIcon) bigImage = scriptIcon;
//						break; // to avoid status set
					}
				case Const.DETECTOR_MANUAL_RELAY:
					armStatus = dbHelper.getRelayStatusById(zone.device_id, zone.section_id, zone.id);
					break;
				default:
					mainBarView.setType(MainBar.Type.DEVICE);
					bigImage = R.drawable.n_image_device_common_main_selector;
					break;
			}

			int model = zone.getModelId();
			switch (model){
				case Const.SENSORTYPE_WIRED_OUTPUT:
					DWOType dwoType = zone.getDWOType();
					if (null != dwoType && null != dwoType.icons) {
						bigImage = dwoType.getMainIcon(context);
					}else if(zone.detector != Const.DETECTOR_AUTO_RELAY && zone.detector != Const.DETECTOR_MANUAL_RELAY){
						bigImage = R.drawable.n_image_wired_output_common_main_selector;
					}
					if(zone.detector == Const.DETECTOR_AUTO_RELAY)
						armStatus = Const.STATUS_NO_ARM_CONTROL;
					break;
				case Const.SENSORTYPE_WIRED_INPUT:
					DWIType dwiType = zone.getDWIType();
					if (null != dwiType && null != dwiType.icons)
					{
						bigImage = dwiType.getMainIcon(context);
						DType dType = dwiType.getDetectorType(zone.getDetector());
						if(null!=dType){
							AType aType = dType.getAlarmType(zone.getAlarm());
							if(null!=aType && null!=aType.icons){
								bigImage = aType.getMainIcon(context);
							}
						}
					}else{
						bigImage = R.drawable.n_image_wired_input_common_main_selector;
					}
				case Const.SENSORTYPE_UNKNOWN:
					break;
				default:
					ZType zType = zone.getModel();
					if (null != zType && null != zType.icons)
					{
						bigImage = zType.getMainIcon(context);
						DType dType = zType.getDetectorType(zone.getDetector());
						if(null!=dType){
							AType aType = dType.getAlarmType(zone.getAlarm());
							if(null!=aType && null!=aType.icons){
								bigImage = aType.getMainIcon(context);
							}
						}
					}
					break;
			}

			switch (armStatus){
				case Const.STATUS_OFF:
					mainBarView.setOff(true);
					break;
				case Const.STATUS_ON:
					mainBarView.setOn(true);
					break;
				default:
					mainBarView.setNoArm(true);
					break;
			}

			mainBarView.setImageBig(bigImage);

			mainBarView.setStates(info.getAffects());

			mainBarView.setOnElementClickListener(new NMainBarElement.OnElementClickListener()
			{
				@Override
				public void onClick()
				{
					openZone(dbHelper.getSiteByDeviceSection(zone.device_id, zone.section_id), zone, mainBarView);
				}

				@Override
				public void onLongClick()
				{

				}
			});

			mainBarView.acceptStates();
		}
	}

	private void openZone(Site site, Zone zone, NMainBarElement elementView)
	{
		switch (zone.detector){
			case 13:
			case 23:
				openRelayActivity(site, zone, elementView);
				break;
			default:
				openZoneActivity(site, zone, elementView);
				break;
		}
	}

	private void openZoneActivity(Site site, Zone zone, NMainBarElement elementView)
	{
		Intent intent = new Intent(context, ZoneActivity.class);
		intent.putExtra("bar_title", zone.name);
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		intent.putExtra("transition", true);
		intent.putExtra("transition_start", getTransitionStartSize(elementView));
		context.startActivity(intent, getTransitionOptions(elementView).toBundle());
	}



	private void openRelayActivity(Site site, Zone zone, NMainBarElement elementView)
	{
		Intent intent = new Intent(context, RelayActivity.class);
		intent.putExtra("bar_title", zone.name);
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		intent.putExtra("transition", true);
		intent.putExtra("transition_start", getTransitionStartSize(elementView));
		context.startActivity(intent, getTransitionOptions(elementView).toBundle());
	}


	private  void  showControlDialog(NWithAButtonElement sectionElement, Site site, Section section, int layout){
		NDialog finalNDialog = new NDialog(context, layout);
		finalNDialog.setTitle(section.name);
		finalNDialog.setSubTitle(site.name);
		finalNDialog.setOnActionClickListener((value, b) -> {
			switch (value)
			{
				case NActionButton.VALUE_ARM:
					try
					{
						JSONObject commandAddress = new JSONObject();
						commandAddress.put("section",section.id);
						commandAddress.put("zone", 0);
						nSendMessageToServer(section, Command.ARM, commandAddress, true);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					finalNDialog.dismiss();
					break;
				case NActionButton.VALUE_DISARM:
					try
					{
						JSONObject commandAddress = new JSONObject();
						commandAddress.put("section",section.id);
						commandAddress.put("zone", 0);
						nSendMessageToServer(section, Command.DISARM, commandAddress, true);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					finalNDialog.dismiss();
					break;
				case NActionButton.VALUE_INFO:
					openSection(site, section, sectionElement);
					finalNDialog.dismiss();
					break;
			}
		});
		finalNDialog.show();
	}

	private void openSection(Site site, Section section, NWithAButtonElement elementView)
	{
		Intent intent = new Intent(context, SectionActivity.class);
		intent.putExtra("bar_title", section.name);
		intent.putExtra("site", site.id);
		intent.putExtra("device", section.device_id);
		intent.putExtra("section", section.id);
		intent.putExtra("transition", true);
		intent.putExtra("transition_start", getTransitionStartSize(elementView));
		context.startActivity(intent, getTransitionOptions(elementView).toBundle());
	}


	private int getTransitionStartSize(View view){
		return  Func.getTextSize(context, ((Textable)view).getTitleView());
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((DeviceActivity)context,
				new Pair<>(((Textable)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	}

	public D3Service getLocalService()
	{
		Activity activity = (DeviceActivity)context;
		if(activity != null){
			DeviceActivity deviceActivity = (DeviceActivity) activity;
			return deviceActivity.getLocalService();
		}
		return null;
	}

}
