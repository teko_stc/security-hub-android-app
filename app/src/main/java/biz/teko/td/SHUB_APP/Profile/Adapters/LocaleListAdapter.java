package biz.teko.td.SHUB_APP.Profile.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.LocaleD3;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class LocaleListAdapter extends ArrayAdapter<LocaleD3>
{
	private OnElementClickListener onElementClickListener;

	public interface OnElementClickListener{
		void onClick(LocaleD3 language);
	}

	public void setOnElementClickListener(OnElementClickListener onElementClickListener){
		this.onElementClickListener = onElementClickListener;
	}

	private String curLang;
	private final int layout;
	private Context context;
	private LayoutInflater layoutInflater;
	private LocaleD3[] languages;

	public LocaleListAdapter(Context context, int resource, LocaleD3[] languages, String curLang)
	{
		super(context, resource);
		this.context = context;
		this.languages = languages;
		this.curLang = curLang;
		this.layout = resource;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setCurLang (String curLang){
		this.curLang = curLang;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if (null==view) {
			view = layoutInflater.inflate(layout, parent, false);
		}
		LocaleD3 language = languages[position];

		NMenuListElement element = (NMenuListElement) view.findViewById(R.id.nLangElement);
		if(null!=element){
			element.setTitle(language.description);
			if(curLang.equals(language.iso)){
				element.setChecked(true);
			}else{
				element.setChecked(false);
				element.setOnRootClickListener(() -> {
					if(null!=onElementClickListener) onElementClickListener.onClick(language);
				});
			}
		}
		return view;
	}

	public int getCount() {
		return languages.length;
	}

	public LocaleD3 getItem(int position){
		return languages[position];
	}

	public long getItemId(int position){
		return position;
	}
}
