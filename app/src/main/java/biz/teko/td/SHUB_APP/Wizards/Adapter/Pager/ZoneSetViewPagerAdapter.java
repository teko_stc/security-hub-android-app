package biz.teko.td.SHUB_APP.Wizards.Adapter.Pager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Fragments.ZoneSetFragment;

/**
 * Created by td13017 on 23.06.2017.
 */

public class ZoneSetViewPagerAdapter extends FragmentPagerAdapter
{
	public static int DEVICE_SET_PAGES_COUNT = 3;

	public ZoneSetViewPagerAdapter(FragmentManager fm)
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int position)
	{
		return ZoneSetFragment.newInstance(position);
	}

	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}

	@Override
	public int getItemPosition(@NonNull Object object)
	{
		return POSITION_NONE;
	}

	@Override
	public int getCount()
	{
		return DEVICE_SET_PAGES_COUNT;
	}
}
