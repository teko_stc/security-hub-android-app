package biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.Fragments.ProgressDialogFragment;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Activities.LocalConfigNavigation;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Presenters.DeviceFactoryResetViewModel;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;

public class DeviceFactoryResetFragment extends Fragment
{

	private final LocalConfigNavigation activity;
	private ProgressDialogFragment progressDialog;

	private final DeviceFactoryResetViewModel presenter;

	private LinearLayout buttonClose;

	private NWithAButtonElement buttonDelete;
	private String name;
	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private final int sending = 0;
	private String serial;


	private final BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, activity.findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private final BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, activity.findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, activity.findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, activity.findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};

	@Override
	public void onStart() {
		super.onStart();
		activity.setOnBackPressedCallback(this::onBackPressed);
	}
	public void onBackPressed(){
		activity.openDeviceConfig(presenter.getDevice());
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		presenter.finish();
	}

	public DeviceFactoryResetFragment(LocalConfigNavigation activity, DeviceDTO device, SocketConnection connection) {
		this.activity = activity;
		presenter = new DeviceFactoryResetViewModel(connection, device );
	}
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.n_activity_delete, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		setup();
		bind();
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		serial= presenter.getDevice().getSerial();
		name = presenter.getDevice().getName();

	}
	private void bind()
	{
		if(null!=buttonClose) buttonClose.setOnClickListener(view ->
				onBackPressed());
		if(null!=buttonDelete)
		{
			buttonDelete.setOnChildClickListener((action, option) -> resetToDefaults());
			String title;
			String subTitle;
			String actionTitle;
			title = getString(R.string.RESET) + " " + name + " " + serial + " " +
					getString(R.string.RESET_ON_START);
			subTitle = getString(R.string.CONFIG_RESET_QUESTION);
			actionTitle = getString(R.string.RESET);
			buttonDelete.setTitle(title);
			buttonDelete.setSubtitle(subTitle);
			if(null!=actionTitle)
				buttonDelete.setActionTitle(actionTitle);
		}
	}


	private void resetToDefaults() {

		presenter.resetToDefaults(() -> {
			progressDialog.dismiss();
			Toast.makeText(activity, activity.getString(R.string.SUCCESS_SET), Toast.LENGTH_SHORT).show();
			activity.openDeviceList();
		}, () -> {
			progressDialog.dismiss();
			Toast.makeText(activity, activity.getString(R.string.ERROR_RESET_TIMEOUT), Toast.LENGTH_SHORT).show();
			activity.openDeviceList();
		},  () -> {
			progressDialog.dismiss();
			Toast.makeText(activity, activity.getString(R.string.ERROR_RESET_NETWORK_ERROR), Toast.LENGTH_SHORT).show();
		});
		showProgressBar();
	}
	private void showProgressBar() {
		progressDialog = new ProgressDialogFragment();
		progressDialog.setCancelable(false);
		progressDialog.show(getChildFragmentManager(), "progressBar");
	}

	public boolean sendCommandToServer(D3Request d3Request) {
		if (null != myService) {
			if (myService.send(d3Request.toString())) {
				return true;
			} else {
				sendIntentCommandBeenSend(-1);
			}
		} else {
			Func.nShowMessage(activity, activity.getString(R.string.EA_D3_OFF_1) + " " + activity.getString(R.string.application_name) + " " + activity.getString(R.string.EA_D3_OFF_2));
		}

		return false;
	}

	public void sendIntentCommandBeenSend(int value) {
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		activity.sendBroadcast(intent);
	}

	private void setup()
	{
		buttonDelete = (NWithAButtonElement) activity.findViewById(R.id.nButtonDelete);
		buttonClose = (LinearLayout) activity.findViewById(R.id.nButtonClose);
	}

	public void  onResume(){
		super.onResume();
		if (!presenter.getDevice().isTestMode()) {
			presenter.sendTestModeOn();
		}
		bindD3();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_REVOKE_DOMAIN);
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_OPERATOR_RMV);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_USER_RMV);
		activity.registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		activity.registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
	}

	public void onPause(){
		super.onPause();
		if(bound)
			activity.unbindService(serviceConnection);
		activity.unregisterReceiver(commandResultReceiver);
		activity.unregisterReceiver(commandSendReceiver);
	}

	private void bindD3(){
		Intent intent = new Intent(activity, D3Service.class);
		serviceConnection = getServiceConnection();
		activity.bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(activity, activity.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), activity, command, sending);
	}
}
