package biz.teko.td.SHUB_APP.SecCompanies.Models;

import org.simpleframework.xml.Attribute;

public class CPhone
{
	@Attribute(name="caption", required = false)
	public String caption;

	@Attribute(name="phone", required = false)
	public String phone;
}