package biz.teko.td.SHUB_APP.Utils.Dir;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.SmsMessage;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer2.util.NotificationUtil;
import com.google.android.exoplayer2.util.Util;
import com.google.android.material.snackbar.Snackbar;
import com.skydoves.balloon.ArrowOrientation;
import com.skydoves.balloon.Balloon;
import com.skydoves.balloon.BalloonAnimation;

import org.apache.commons.lang.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicReference;

import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraRecordViewActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.D3XProtoConstEvent;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EDetector;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EReason;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EStatement;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.Icon;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.UDP.LocalConfig;
import biz.teko.td.SHUB_APP.UDP.Reply_SURVEY;
import biz.teko.td.SHUB_APP.Utils.CI_snackbar_callback;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDotsView;
import biz.teko.td.SHUB_APP.Utils.NotificationUtils;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

import static biz.teko.td.SHUB_APP.D3.D3Service.decodeCP1251;
import static biz.teko.td.SHUB_APP.D3.D3Service.decodeLatin;
import static biz.teko.td.SHUB_APP.D3.D3Service.decodeUNICODE;

/**
 * Created by td13017 on 05.08.2016.
 */
public class Func
{
//	public static boolean isStartedNotifActivity = false;
	public static final int SECTION_GUARD_TYPE = 1;
	public static final int SECTION_ALARM_TYPE = 2;
	public static final int SECTION_FIRE_TYPE = 3;
	public static final int SECTION_FIRE_DOUBLE_TYPE = 4;
	public static final int SECTION_TECH_TYPE = 5;
	public static final int SECTION_TECH_CONTROL_TYPE = 6;

	public static final int SIREN_TYPE = 14;
	public static final int LAMP_TYPE = 15;
	public static final int AUTO_RELAY_TYPE = 13;
	public static final int RELAY_TYPE = 23;
	public static final int SZO_TYPE = 25;
	public static final int THERMOSTAT_TYPE = 27;

	public static final long DAY_IN_SEC = 86400000;

	public static String defaultRingtone = "content://settings/system/notification_sound";

	public static final String DATE = "dd.MM.yyyy";
	public static final String DATE_SHORT = "dd.MM";
	public static final String TIME = "HH:mm:ss";
	public static final String TIME_SHORT = "HH:mm";

	public static boolean isAppLaunch(Context context) {
		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
		if (appProcesses == null) {
			return false;
		}
		final String packageName = context.getPackageName();
		for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
				return true;
			}
		}
		return false;
	}

	public static int getPendingFlags(int flag) {
		int pendingFlags;
		if (Util.SDK_INT >= 23) {
			pendingFlags = flag | PendingIntent.FLAG_IMMUTABLE;
		} else {
			pendingFlags = flag;
		}
		return pendingFlags;
	}

	public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public static int getNavBarHeight(Context context) {
		int result = 0;
		boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
		boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);

		if (!hasMenuKey && !hasBackKey) {
			Resources resources = context.getResources();
			int orientation = resources.getConfiguration().orientation;
			int resourceId;
			if (isTablet(context)){
				resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_height_landscape", "dimen", "android");
			}  else {
				resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_width", "dimen", "android");
			}
			if (resourceId > 0) {
				return resources.getDimensionPixelSize(resourceId);
			}
		}
		return result;
	}

	private static boolean isTablet(Context c) {
		return (c.getResources().getConfiguration().screenLayout
				& Configuration.SCREENLAYOUT_SIZE_MASK)
				>= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	public static boolean isServiceRunningInForeground(Context context, Class<?> serviceClass) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceClass.getName().equals(service.service.getClassName())) {
				if (service.foreground) {
					return true;
				}
			}
		}
		return false;
	}

	public static void setDotsPinEditText(NEditText pinEditText, AtomicReference<String> shared, NDotsView dotsView) {
		pinEditText.showKeyboard();
		dotsView.setOnClickListener(view -> pinEditText.showKeyboard());

		pinEditText.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
				String newPin = charSequence.toString();
				if(null!=shared.get())
				{
					if (newPin.length() > shared.get().length() && shared.get().length() < 4)
						dotsView.increase();
					else if (newPin.length() < shared.get().length()) dotsView.decrease();
					shared.set(newPin);
				}
			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});

	}

	public static boolean isPayed(){
		switch (App.getContext().getPackageName()){
			case "ru.sh.intermatrix":
				return true;
		}
		return false;
	}

//	public static String getChannelID(Context context, int classId)
//	{
//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//		int id = sp.getInt("pref_channel_id_" + Integer.toString(classId), classId);
//		return Integer.toString(id);
//	}

//	public static void updateChannelID(Context context, int classId)
//	{
//		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//		int counter = sp.getInt("pref_channel_counter", 15) + 1;
//		sp.edit().putInt("pref_channel_id_" + Integer.toString(classId), counter).apply();
//		sp.edit().putInt("pref_channel_counter", counter).apply();
//
//	}

	public static boolean isCurrentVersionSdk(int version) {
		return Build.VERSION.SDK_INT >= version;
	}

	public static boolean needScriptsForBuild()
	{
		switch (App.getContext().getPackageName()){
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
			case "uvosrtv.sh":
			case "uvomsk.sh":
			case "uvokrsnd.sh":
			case "ru.sh.teleplus":
			case "ru.axiostv.smarthouse":
			case "ru.sh.intermatrix":
			case "ru.sh.uvokuzbass":
			case "ksb.sh":

				return true;
		}
		return false;
	}

	public static boolean needCamerasForBuild()
	{
		switch (App.getContext().getPackageName()){
			case "ttk.sh":
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
			case "uvokrsnd.sh":
			case "uvomsk.sh":
			case "uvosmr.sh":
			case "uvosrtv.sh":
			case "uvornd.sh":
			case "gepard.sh":
			case "kuzet.sh":
			case "omicron.sh":
			case "ru.sh.legion":
			case "ru.axiostv.smarthouse":
			case "ru.sh.teleplus":
			case "ru.sh.intermatrix":
			case "ksb.sh":

				return true;
		}
		return false;
	}


	public static boolean needLicenseAForBuild()
	{
		switch (App.getContext().getPackageName()){
			case "ttk.sh":
			case "ru.sh.teleplus":
			case "ru.sh.intermatrix":
				return false;
		}
		return true;
	}

	public static boolean needSecCompForBuild()
	{
		switch (App.getContext().getPackageName()){
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
			case "uvosrtv.sh":
			case "ru.axiostv.smarthouse":
				return true;
		}
		return false;
	}

	public static boolean needLangForBuild(Context context){
		switch (context.getPackageName()){
			case "public.shub":
			case "ru.sh.intermatrix":
			case "dev.shub":
			case "teko.shub":
			case "ru.sh.teleplus":
				return true;
		}
		return false;
	}

	public static boolean debug()
	{
		return App.getContext().getPackageName().equals("dev.shub");
	}

	public static boolean tekoBuild(){
		switch (App.getContext().getPackageName())
		{
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
				return true;
		}
		return false;
	}

	public static  boolean needRegForBuild(){
		switch (App.getContext().getPackageName()){
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
			case "ru.axiostv.smarthouse":
			case "ru.sh.teleplus":
			case "ru.sh.intermatrix":
			case "com.sh.akmsistem":
				return true;
		}
		return false;
	}

	public static Uri getWikiLink(){
		String url = "https://cloud.security-hub.ru/wiki/doku.php?id=start";
		return Uri.parse(url);
	}

	public static Uri getRegLink(Context context)
	{
		String url = context.getString(R.string.web_site);

		switch (App.getContext().getPackageName())
		{
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
				url = "cloud." + url;
				url+= "/?p=100";
				String curLang = PrefUtils.getInstance(context).getCurrentLang();
				if(null!=curLang)url = url + "&l=" + curLang;
				break;
			case "ru.sh.intermatrix":
				url+= "/reg";
			default:
				break;
		}
		url = "https://" + url;
		return Uri.parse(url);
	}

	public static Uri getRecLink(Context context)
	{
		String url = context.getString(R.string.web_site);

		switch (App.getContext().getPackageName())
		{
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
                url = "cloud." + url;
                url+= "/?p=101";
				String curLang = PrefUtils.getInstance(context).getCurrentLang();
				if(null!=curLang)url = url + "&l=" + curLang;
				break;
			case "ru.sh.intermatrix":
				url+= "/rec";
			default:
				break;
		}
        url = "https://" + url;
        return Uri.parse(url);
	}

	public static boolean needSupportPhone(Context context)
	{
		switch (context.getPackageName()){
			case("avangardsb.sh"):
			case("uvomsk.sh"):
				break;
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
			case "uvokrsnd.sh":
				if((PrefUtils.getInstance(context).getCurrentLang()).equals("ru")){
					return true;
				}
				break;
			default:
				if((PrefUtils.getInstance(context).getCurrentLang().equals(context.getString(R.string.default_iso)))){
					return true;
				}
				break;
		}
		return false;
	}

	public static boolean needShopForBuild()
	{
		switch (App.getContext().getPackageName()){
			case "public.shub":
			case "dev.shub":
			case "teko.shub":
			case "ru.axiostv.smarthouse":
				return true;
		}
		return false;
	}

	public static int getX(int i)
	{
		return i%2;
	}

	public static int getY(int i)
	{
		return i/2;
	}

	public static boolean isAxiosConfig() {
		return App.getContext().getPackageName().equals("ru.axiostv.smarthouse");
	}

	public static boolean getNotificationStatus(Context context)
	{
		return 	Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ?
				NotificationManagerCompat.from(context).areNotificationsEnabled() :
				PrefUtils.getInstance(context).getNotificationStatus();
	}

	public static boolean isRuLang(Context context) {
		return PrefUtils.getInstance(context).getCurrentLang().equals("ru");
	}

	public static boolean checkMOD(String re)
	{
		boolean ch = false;
		int ch_sum = 0;
		for( int i = re.length()-1; i > 0; i--){
			int val = Integer.valueOf(re.substring(i,i+1));
			if(ch){
				val *= 2;
				if(val>9)val=val%10+1;
			}
			ch_sum+=val;
			ch=!ch;
		}
		return (ch_sum%10==0);
	}

	public static boolean checkLC2o5(String s){
		int le = s.length();
		s=s.substring(1,le);
		le = --le;
		int sum = 0;
		while (--le >= 0){
			int digit = Integer.valueOf(s.substring(le, le+1));
			if(1 == (le&1))digit*=3;
			sum+=digit;
		}
		return sum%10==0;
	}

	public static String getMediaFilesDirectory(Context context)
	{
		String dir =  context.getExternalFilesDir("").toString();
		Func.log_d("D3", "ext. files. dir: " + dir);
//		return android.os.Build.VERSION.SDK_INT >= 29 ? context.getExternalFilesDir("").toString() : Environment.getExternalStorageDirectory().getAbsolutePath().toString() + '/' + context.getString(R.string.application_name);
		return dir;
	}

	public static String getMediaFilesOldDirectory(Context context)
	{
		String dir = Environment.getExternalStorageDirectory().getAbsolutePath();
		Func.log_d("D3", "ext. files. dir: " + dir);
		return dir;
	}

	public static class Prefs{
		public static final String PREF_APP_UPDATE_TIME = "app_update_time";
		public static final String PREF_APP_UPDATED = "app_updated";
	}


//	public static String getAppLanguage(Context context)
//	{
//		final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
//		String curLangInPref = sharedPreferences.getString("prof_language_value", null);
//		Locale locale = Locale.getDefault();
//		if(null!=curLangInPref){
//			return curLangInPref;
//		}else{
//			String lang = Locale.getDefault().getLanguage();
//			if(null == lang){
//				lang = Locale.getDefault().getDisplayLanguage();
//			};
//			return lang;
//		}
//	}

	public static boolean isMasterMode(SharedPreferences sharedPreferences)
	{
		return  Func.getBooleanSPDefTrue(sharedPreferences, "prof_master_set");
	}

//	public static String getDeviceModel(int config_version)
//	{
//		String[] types = App.getContext().getResources().getStringArray(R.array.device_types);
//		byte[] b = intToByteArray(config_version);
//		switch (b[1])
//		{
//			case 1:
//				switch (b[0])
//				{
//					case 1://sh1
//					case 2://sh1nic
//					case 3://sh2
//					case 4://sh4g
//						return types[b[0] + 1];
//					default:
//						return types[b[1]];
//				}
//			case 2:
//				return types[6];
//			case 10:
//				return types[7];
//			case 11:
//				return types[8];
//			default:
//				return types[0];
//		}
//	}

	public static String getDeviceVersion(int config_version){
		byte[] b = intToByteArray(config_version);
		return b[3] + "." + b[2];
	}

	public static int getDeviceType(int config_version, int cluster)
	{
		switch (cluster)
		{
			case 1:
				byte[] b = intToByteArray(config_version);
				switch (b[1])
				{
					case 1:
						switch (b[0])
						{
							case 1://sh1
							case 2://sh1nic
							case 3://sh2
							case 4://sh4g
								return (b[0] + 1);
							default:
								return b[1];
						}
					case 2:
						return 6;
					case 10:
						return 7;
					case 11:
						return 8;
					default:
						return 0;
				}
			case 2:
				return 6;
			case 3:
				return 9;
		}
		return 0;
	}

	public static String getBatteryStatus(Context context, String dataFromJData)
	{
		switch (Integer.valueOf(dataFromJData)){
			case -1:
				return context.getString(R.string.BATTERY_DISCHARGED);
			case 0:
				return context.getString(R.string.BATTERY_NOT_DIASCHARGED);
			case 1:
				return context.getString(R.string.BATTERY_CHARGED);
			default:
				return context.getString(R.string.BATTERY_UNKNOWN);
		}
	}

	public static boolean getRemoteSetRights(UserInfo userInfo, DBHelper dbHelper, int site_id, int device_id)
	{
		if(!(((userInfo.roles&Const.Roles.DOMAIN_ENGIN)==0)&&((userInfo.roles&Const.Roles.ORG_ENGIN)==0))){
//			if(!dbHelper.getDevicePartedStatusForSite(site_id, device_id)){ /*03.08.2022 исключаем, так как больше нет функционала деления устройства на несколько объектов*/
				return (0 < dbHelper.getDeviceTypeById(device_id) && (6 > dbHelper.getDeviceTypeById(device_id)));
//			}
		}
		return false;
	}

	public static boolean nGetRemoteDeviceSetRights(UserInfo userInfo, Device device)
	{
		if(nGetRemoteSystemSetRights(userInfo)){
			if(null!=device){
				return (device.canBeSetFromApp());
			}
		}
		return false;
	}

	public static boolean nGetRemoteSystemSetRights(UserInfo userInfo)
	{
		return !(((userInfo.roles & Const.Roles.DOMAIN_ENGIN) == 0) && ((userInfo.roles & Const.Roles.ORG_ENGIN) == 0));
	}

	public static boolean nGetRemoteSystemAdminRights(UserInfo userInfo)
	{
		return !((userInfo.roles & Const.Roles.DOMAIN_ADMIN) == 0);
	}

	public static boolean nGetRemoteSystemLawyerRights(UserInfo userInfo)
	{
		return !((userInfo.roles & Const.Roles.DOMAIN_ADMIN) == 0);
	}


	public static String getServerTimeText(Context context, long servertime){
		return  Func.getDate(context, servertime*1000, Func.TIME);
	}

	public  static String getServerDateText(Context context, long servertime){
		return  Func.getDate(context, servertime*1000, Func.DATE);
	}

	public static String getServerTimeTextShort(Context context, long servertime){
		return  Func.getDate(context, servertime*1000, Func.TIME_SHORT);
	}

	public  static String getServerDateTextShort(Context context, long servertime){
		return  Func.getDate(context, servertime*1000, Func.DATE_SHORT);
	}

	public static  String getServerTimeDateTextShort(Context context, long servertime){
		return Func.getDate(context, servertime*1000, Func.TIME_SHORT) + " " + Func.getDate(context, servertime*1000, Func.DATE_SHORT);
	}

	public static String getTimeText(Context context, long localtime){
		return  ((localtime < 400000000) ? (Func.getDate(context, localtime * 1000 - ((1000) * 3 * 3600), Func.TIME)) : Func.getDate(context, ((localtime + 978307200) * 1000), Func.TIME) + " ");
	}

	public static String getDateText(Context context, long localtime){
		return ((localtime < 400000000) ? (" " + context.getString(R.string.FROM_START_TIME) ): Func.getDate(context, ((localtime + 978307200)*1000),  Func.DATE));
	}

	public static String getTimeDateText(Context context, long localtime)
	{
		return  ((localtime < 400000000) ? (Func.getDate(context, localtime * 1000 - ((1000) * 3 * 3600), Func.TIME)) : Func.getDate(context, ((localtime + 978307200) * 1000), Func.TIME) + " ")
				+" "
				+ ((localtime < 400000000) ? (" " + context.getString(R.string.FROM_START_TIME) ): Func.getDate(context, ((localtime + 978307200)*1000),  Func.DATE));
	}

//	private static final String dateFormat = "dd/MM/yyyy hh:mm:ss.SSS";
	public static String getDate(Context context, long milliSeconds, String dateFormat)
	{
		if(dateFormat.equals(DATE)){
			if( getStartOfDay(milliSeconds) == getStartOfDay(System.currentTimeMillis())){
				return context.getString(R.string.FUNC_TODAY);
			}else if (getStartOfDay(milliSeconds) == getStartOfDay(System.currentTimeMillis()) - DAY_IN_SEC){
				return context.getString(R.string.FUNC_YESTERDAY);
			}
		}
		// Create a DateFormatter object for displaying date in specified format.
		SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

		// Create a calendar object that will convert the date and time value in milliseconds to date.
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return formatter.format(calendar.getTime());
	}

	public static String getWeekDayText(Context context, long milliSeconds)
	{
		if( getStartOfDay(milliSeconds) == getStartOfDay(System.currentTimeMillis())){
			return context.getString(R.string.FUNC_TODAY);
		}else if (getStartOfDay(milliSeconds) == getStartOfDay(System.currentTimeMillis()) - DAY_IN_SEC){
			return context.getString(R.string.FUNC_YESTERDAY);
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		return new SimpleDateFormat("EEEE").format(calendar.getTime());
	}

	public static long getStartOfDay (long milliSeconds){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milliSeconds);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	public static long getEndOfDay (long milliSeconds){
		Calendar calendar = Calendar.getInstance();
//		calendar.setTimeInMillis(milliSeconds + 86400000);
		calendar.setTimeInMillis(milliSeconds);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	public static int dpToPx(float i, Context context){
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, context.getResources().getDisplayMetrics());
	}

	public static int dpToPx(int i, Context context){
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, i, context.getResources().getDisplayMetrics());
	}

	public static void hideKeyboard(Activity activity) {
		InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		//Find the currently focused view, so we can grab the correct window token from it.
		View view = activity.getCurrentFocus();
		//If no view currently has focus, create a new one, just so we can grab a window token from it
		if (null==view) {
			view = new View(activity);
		}
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	public static void hideEditTextKeyboard(EditText editText) {
		InputMethodManager imm2 = (InputMethodManager)editText.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm2.hideSoftInputFromWindow(editText.getWindowToken(), 0);

	}

	public static void showKeyboard(Activity activity, View view){
		InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
	}

	public static String handleResult(Context context, int result)
	{
		String s;
		switch (result)
		{
			case 0:
				s = context.getResources().getString(R.string.D3_SUCCEED);
				break;
			case -1:
				s = context.getString(R.string.D3_ERR_GENERIC);
				break;
			case -2:
				s = context.getString(R.string.D3_ERR_USER_IS_INACTIVE);
				break;
			case -3:
				s = context.getString(R.string.D3_ERR_USER_DOES_NOT_EXIST);
				break;
			case -4:
				s = context.getString(R.string.D3_ERR_USER_ALREADY_LOGGED_IN);
				break;
			case -5:
				s = context.getString(R.string.D3_ERR_USER_NO_RIGHTS);
				break;
			case -6:
				s = context.getString(R.string.D3_ERR_USER_INVALID_PASSWORD);
				break;
			case -7:
				s = context.getString(R.string.D3_ERR_API_DEVICE_NOT_FOUND);
				break;
			case -8:
				s = context.getString(R.string.PARAM_ERROR);
				break;
			case -9:
				s = context.getString(R.string.GET_PARSING_ERROR);
				break;
			case -10:
				s = context.getString(R.string.API_PARAMETERS_FORMAT_ERROR);
				break;
			case -11:
				s = context.getString(R.string.JSON_COMMAND_NOT_EXIST);
				break;
			case -12:
				s = context.getString(R.string.D3_ERR_INVALID_REFERENCE_NAME);
				break;
			case -13:
				s = context.getString(R.string.D3_ERR_REFERENCE_DATA_NOT_AVAILABLE);
				break;
			case -14:
				s = context.getString(R.string.D3_ERR_PARENT_CONTAIN_INNER_CHILDS);
				break;
			case -15:
				s = context.getString(R.string.USER_ALREADY_CREATED_ERROR);
				break;
			/*too much events
			don't show to user now
			*
			case -16:
				break;
			*/
			case -18:
				s = context.getString(R.string.D3_ERR_OLD_API);
				break;
			case -20:
				s = context.getString(R.string.D3_ERR_ALREADY_DONE);
				break;
			case -21:
				s = context.getString(R.string.D3_ERR_DEVICE_NOT_FOUND);
				break;
			case -22:
				s = context.getString(R.string.D3_ERR_DEVICE_SECTION_IN_USE);
				break;
			case -23:
				s = context.getString(R.string.D3_ERR_DEVICE_SPLIT);
				break;
			case -24:
				s = context.getString(R.string.D3_ERR_SITE_NOT_FOUND);
				break;
			case -25:
				s = context.getString(R.string.D3_ERR_SITE_SECTION_IN_USE);
				break;
			case -26:
				s = context.getString(R.string.D3_ERR_SITE_SPLIT);
				break;
			case -27:
				s = context.getString(R.string.D3_ERROR_NO_SUCH_DELEGATION_CODE);
				break;
			case -28:
				s = context.getString(R.string.D3_ERR_NO_DELEGATE_CODE_AVAILABLE);
				break;
			case -29:
				s = context.getString(R.string.D3_ERR_SITE_NAME_IN_USE);
				break;
			case -30:
				s = context.getString(R.string.D3_ERR_DEVICE_IS_DELEGATED);
				break;
			case -31:
				s = context.getString(R.string.D3_ERR_ORGANIZATION_INFO_NOT_FOUND);
				break;
			case -32 :
				s = context.getString(R.string.D3_ERR_PERSON_NOT_FOUND);
				break;
			case -33:
				s = context.getString(R.string.D3_ERR_DOMAIN_NOT_FOUND);
				break;
			case -34:
				s = context.getString(R.string.D3_ERROR_DELEGATE_NOT_FOUND);
				break;
			case -35:
				s = context.getString(R.string.D3_ERR_LOCALE_NOT_FOUND);
				break;
			case -36:
				s = context.getString(R.string.D3_ERR_SITE_ALREADY_IN_DOMAIN);
				break;
			case -37:
				s = context.getString(R.string.D3_ERROR_TWO_OR_MORE_BINDINGS);
				break;
			case -38:
				s = context.getString(R.string.D3_ERROR_TIMEOUT_UNSUPPORTED);
				break;
			case -39:
				s = context.getString(R.string.D3_ERR_NAME_IN_USE);
				break;
			case -40:
				s = context.getString(R.string.D3_ERR_NO_RESULTS);
				break;
			case -41:
				s = context.getString(R.string.D3_ERR_OPERATOR_NOT_FOUND);
				break;
			case -42:
				s = context.getString(R.string.D3_ERR_OPERATOR_OFFLINE);
				break;
			case -43:
				s = context.getString(R.string.D3_ERR_NOT_SUPPORTED);
				break;
			case -44:
				s = context.getString(R.string.D3_ERR_DEVICE_SCRIPT_NOT_FOUND);
				break;
			case -45:
				s = context.getString(R.string.D3_ERR_WRONG_INPUT);
				break;
			case -46:
				s = context.getString(R.string.D3_ERR_SCRIPT_NOT_COMPILED);
				break;
			case -47:
				s = context.getString(R.string.D3_ERROR_ALREADY_EXIST);
				break;
			case -50:
				s = context.getString(R.string.D3_ERR_SQL);
				break;
			case -61:
				s = context.getString(R.string.D3_ERR_NO_REVIEW);
				break;
			case -62:
				s = context.getString(R.string.D3_ERR_INVALID_REVIEW_STATE);
				break;
			case -63:
				s = context.getString(R.string.D3_ERR_NO_OPERATOR_TO_HANDLE_REVIEW);
				break;
			case -71:
				s = context.getString(R.string.D3_ERR_CONTRACTOR_NOT_FOUND);
				break;
			case -81:
				s = context.getString(R.string.D3_ERR_CONTRACT_NOT_FOUND);
				break;
			case -82:
				s = context.getString(R.string.D3_ERR_CONTRACT_NAME_IS_NOT_UNIQUE);
				break;
			case -100:
				s = context.getString(R.string.API_JSON_LOAD_ERROR);
				break;
			case -101:
				s = context.getString(R.string.API_JSON_UNPACK_ERROR);
				break;
			case -102:
				s = context.getString(R.string.API_JSON_LOAD_ERROR);
				break;
			case -200:
				s = context.getString(R.string.API_SURGARD_SITE_NOT_FOUND);
				break;
			case -201:
				s = context.getString(R.string.API_RETRY_LATER);
				break;
			case -202:
				s = context.getString(R.string.API_BAD_HTTP_REQUEST);
				break;
			case -203:
				s = context.getString(R.string.API_EMPTY_HTTP_RESPONSE);
				break;
			case -204:
				s = context.getString(R.string.API_IVIDEON_ERROR);
				break;
			case -205:
				s = context.getString(R.string.API_IVIDEON_NEED_AUTHORIZATION);
				break;
			case -206:
				s = context.getString(R.string.API_IVIDEON_NO_TOKEN);
				break;
			case -207:
				s = context.getString(R.string.API_NO_SUCH_CAM_RELATION_ERROR);
				break;
			default:
				s = context.getString(R.string.D3_ERRROR);
				break;
		}
		return s;
	}

	public static String getUSSD(int imsi){
		switch (imsi){
			case 2502834:
			case 2509934:
				/*Beeline*/
				return "*102#";
			case 2500234:
				/*Megafon*/
				return "*100#";
			case 2501034:
				/*MTS*/
				return "*100#";
			default:
				return "*100#";
		}
	}

	public static void pushToast(final Context context, final String s, Activity activity){
		activity.runOnUiThread(new Runnable()
		{
			@Override
			public void run()
			{
				Toast toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}
		});
	}
	public static void pushToast(final Context context, final String s){
		((Activity) context).runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					Toast toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			});
		}

	//	public static void showCommandResult(Activity activity, Context context, Intent intent)
	//	{
	//		int result = intent.getIntExtra("result", -1);
	//		String[] results;
	//		if(result >0)
	//		{
	//			results = context.getResources().getStringArray(R.array.command_results);
	//		}else{
	//			results = context.getResources().getStringArray(R.array.command_results_negative);
	//			result = Math.abs(result);
	//		}
	//		Func.pushToast(context, context.getString(R.string.ERROR_COMMAND) + " " + (result!=0? (results.length >=result ? results[result - 1] : "") : results[result]), activity);
	//
	//	}

	public static void showCommandResultSnackbar(Context context, View view, int result)
	{
		String[] results;
		if(result >0)
		{
			results = context.getResources().getStringArray(R.array.command_results);
		}else{
			results = context.getResources().getStringArray(R.array.command_results_negative);
			result = Math.abs(result);
		}
		showSnackBar(context, view, context.getString(R.string.ERROR_COMMAND) + " " + (result!=0 ? (results.length >=result ? results[result - 1] : "") : results[result]));
	}

	public static void showSnackBar(Context context, View view, String message){
//		Context context = App.getContext();
		Snackbar snackbar = getSimpleSnackBar(context, view, getSnackbar(context, view, message));
		App.getSnackBarQueue().add(snackbar);
		if(App.getSnackBarQueue().size() == 1){
			App.getSnackBarQueue().get(0).show();
		}
	}

	private static Snackbar getSnackbar(Context context, View view, String message){
		return Snackbar.make(view,
				message,
				Snackbar.LENGTH_LONG)
				.setActionTextColor(context.getResources().getColor(R.color.brandColorAccentColor))
				.setAction("Ок", new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{

					}
				});
	}

	public static void showSnackBar(Context context, View view, String message, String buttonText, CI_snackbar_callback ci_snackbar_callback){
		//		Context context = App.getContext();
		Snackbar snackbar = getSimpleSnackBar(context, view, getSnackbar(context, view, message, buttonText, ci_snackbar_callback));
		App.getSnackBarQueue().add(snackbar);
		if(App.getSnackBarQueue().size() == 1){
			App.getSnackBarQueue().get(0).show();
		}
	}



	private static Snackbar getSnackbar(Context context, View view, String message, String button, CI_snackbar_callback ci_snackbar_callback){
		return Snackbar.make(view,
				message,
				Snackbar.LENGTH_LONG)
				.setActionTextColor(context.getResources().getColor(R.color.brandColorAccentColor))
				.setAction(button, new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						ci_snackbar_callback.callback();
					}
				});
	}

	private static Snackbar getSimpleSnackBar(Context context, View view, Snackbar snackbar)
	{
		snackbar.addCallback(new Snackbar.Callback(){
			@Override
			public void onDismissed(Snackbar snackb, int event)
			{
				App.getSnackBarQueue().remove(snackb);
				if(App.getSnackBarQueue().size() > 0){
					App.getSnackBarQueue().get(0).show();
				}
			}
		});

		final View snackBarView = (View) snackbar.getView();

		ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams)snackBarView.getLayoutParams();

		FrameLayout workFrame = view.findViewById(R.id.nav_host);
		if (null!=workFrame){
			ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) view.getLayoutParams();
			params.setMargins(params.leftMargin + Func.dpToPx(12, context),
					params.topMargin,
					params.rightMargin + Func.dpToPx(12, context),
//					params.bottomMargin + Func.dpToPx(88, context) + Func.dpToPx(layoutParams.bottomMargin, context)  - Func.dpToPx(71, context)
					params.bottomMargin + Func.dpToPx(12, context) + layoutParams.bottomMargin + Func.dpToPx(71, context)
			);
		}else
		{
			params.setMargins(params.leftMargin + Func.dpToPx(12, context), params.topMargin, params.rightMargin + Func.dpToPx(12, context), params.bottomMargin + Func.dpToPx(12, context));
		}
		snackBarView.setLayoutParams(params);
		snackBarView.setBackground(context.getResources().getDrawable(R.drawable.background_snackbar));
		TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
		ViewGroup.LayoutParams params1 = textView.getLayoutParams();
		params1.height = FrameLayout.LayoutParams.WRAP_CONTENT;
		textView.setLayoutParams(params1);
		textView.setMaxLines(4);
		textView.setTextColor(context.getResources().getColor(R.color.brandColorWhite));
		return snackbar;
	}

	public static void log_v(String logTag,   String s)
	{
		Log.v(logTag, System.currentTimeMillis()/1000 + "[" + getThreadSignature() + "] " + s);
	}

	public static void log_i(String logTag,   String s)
	{
		Log.i(logTag, System.currentTimeMillis()/1000 +  "[" + getThreadSignature() + "] " + s);
	}

	public static void log_d(String s)
	{
		Log.d(Const.LOG_TAG, System.currentTimeMillis()/1000 +  "[" + getThreadSignature() + "] " + s);
	}

	public static void log_d(String logTag,   String s)
	{
		Log.d(logTag, System.currentTimeMillis()/1000 +  "[" + getThreadSignature() + "] " + s);
	}

	public static void log_w(String logTag,   String s)
	{
		Log.e(logTag, System.currentTimeMillis()/1000 +  "[" + getThreadSignature() + "] " + s);
	}

	public static void log_e(String logTag,   String s)
	{
		Log.e(logTag, System.currentTimeMillis()/1000 +  "[" + getThreadSignature() + "] " + s);
	}

	public static String getThreadSignature()
	{
		Thread t = Thread.currentThread();
		long l = t.getId();
		String name = t.getName();
		long p = t.getPriority();
		String gname = t.getThreadGroup().getName();
		return (name
				+ ":(id)" + l
				+ ":(priority)" + p
				+ ":(group)" + gname);
	}

	public static String getStringFromPdu(String data)
	{
		byte[] bytes = stringToByteArrayGlueTwo(data);
		SmsMessage smsMessage = SmsMessage.createFromPdu(bytes);
		return smsMessage.getDisplayMessageBody();
	}

	public static String describeUssd(String data)
	{
		byte[] bytes = stringToByteArrayGlueTwo(data);
		if(bytes != null)
		{
			byte coding = bytes[bytes.length-1];
			int dec_hex = (coding&0xF) + (((int) coding >> 4)&0xF)*10;
			switch ((dec_hex>>4)&0x0F){
				case 0:
					switch (dec_hex&0x0F){
						case 1:
						case 0x0F:
							return decodeLatin(ArrayUtils.subarray(bytes, 0, bytes.length-1));
						default:
							Func.log_d("D3ServiceLog", "Unsupported language code as bits 3..0 of the Coding Scheme");
							break;
					}
					break;
				case 1:
					switch (dec_hex&0x0F){
						case 0:
							String lang = decodeLatin(ArrayUtils.subarray(bytes, 0, 1));
							if(lang.equals("ru")){
								return decodeLatin(ArrayUtils.subarray(bytes, 2, bytes.length-1));
							}else{
								Func.log_d("D3ServiceLog", "Unsupported ISO 639 language code");
							}
							break;
						case 1:
							return decodeUNICODE(ArrayUtils.subarray(bytes, 0, bytes.length-1));
						default:
							Func.log_d("D3ServiceLog", "Reserved");
							break;
					}
					break;
				case 2:
					switch (dec_hex&0x0F){
						case 3:
							return decodeCP1251(ArrayUtils.subarray(bytes, 0, bytes.length-1));
						default:
							Func.log_d("D3ServiceLog", "Unsupported language code as bits 3..0 of the Coding Scheme");
							break;
					}
					break;
				case 3:
					Func.log_d("D3ServiceLog", "Unsupported language code as bits 3..0 of the Coding Scheme");
					break;
				case 8:
					Func.log_d("D3ServiceLog", "Reserved coding groups");
					break;
				case 9:
					Func.log_d("D3ServiceLog", "Unsupported \"Message with User Data Header (UDH) structure\"");
					break;
				case 0xA:
					Func.log_d("D3ServiceLog", "Reserved coding groups");
					break;
				case 0XB:
					Func.log_d("D3ServiceLog", "Reserved coding groups");
					break;
				case 0xC:
					Func.log_d("D3ServiceLog", "Reserved coding groups");
					break;
				case 0xD:
					Func.log_d("D3ServiceLog", "Reserved coding groups");
					break;
				case 0xE:
					Func.log_d("D3ServiceLog", "Unsupported \"Defined by the WAP Forum\"");
					break;
				case 0xF:
					Func.log_d("D3ServiceLog", "Unsupported \"Data coding / message handling\"");
				default:
					switch ((dec_hex>>4)&0x02){
						case 0:
							switch ((dec_hex>>2)&0x03){
								case 0:
									break;
								case 1:
									break;
								case 2:
									return decodeUNICODE(ArrayUtils.subarray(bytes, 0, bytes.length-1));
								default:
									Func.log_d("D3ServiceLog", "Reserved");
									break;
							}
							break;
						default:
							Func.log_d("D3ServiceLog", "Unsupported text compression");
							break;
					}
					break;
			}
			return decodeUNICODE(ArrayUtils.subarray(bytes, 0, bytes.length-1));
//			}
		}
		return "Неизвестная кодировка USSD.";
	}

	public static class AsteriskPasswordTransformationMethod extends PasswordTransformationMethod
	{
		@Override
		public CharSequence getTransformation(CharSequence source, View view) {
			return new PasswordCharSequence(source);
		}

		private class PasswordCharSequence implements CharSequence {
			private final CharSequence mSource;
			public PasswordCharSequence(CharSequence source) {
				mSource = source; // Store char sequence
			}
			public char charAt(int index) {
				return '*'; // This is the important part
			}
			public int length() {
				return mSource.length(); // Return default
			}
			public CharSequence subSequence(int start, int end) {
				return mSource.subSequence(start, end); // Return default
			}
		}
	}



	public static String md5(String in) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
			digest.reset();
			digest.update(in.getBytes());
			byte[] a = digest.digest();
			int len = a.length;
			StringBuilder sb = new StringBuilder(len << 1);
			for (int i = 0; i < len; i++) {
				sb.append(Character.forDigit((a[i] & 0xf0) >> 4, 16));
				sb.append(Character.forDigit(a[i] & 0x0f, 16));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
		return null;
	}

	public static void showRestartServiceMessage(final Context context, String message){
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		adb.setTitle(R.string.TITLE_ATTENTION);
		adb.setMessage(message);
		adb.setPositiveButton(R.string.N_MESSAGE_RESTART, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				restartD3Service(context, "activity");
				dialog.dismiss();
			}
		});

		final AlertDialog ad = adb.create();
		ad.setCancelable(false);
		ad.show();
	}

	public static void showMessage(final Context context, String message)
	{
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		adb.setTitle(R.string.TITLE_ATTENTION);
		adb.setMessage(message);
		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		final AlertDialog ad = adb.create();
		ad.setCancelable(false);
		ad.show();
	}

	public static void nShowMessage(final Context context, String message, NDialog.OnActionClickListener clickListener){
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
		nDialog.setTitle(message);
		nDialog.setOnActionClickListener(clickListener);
		nDialog.show();
	}



	public static void nShowMessage(final Context context, String message){
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
		nDialog.setTitle(message);
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}
		});
		nDialog.show();
	}

	public static void nShowMessage(Context context, String title, String subTitle, int layout){
		NDialog nDialog = new NDialog(context, layout);
		nDialog.setTitle(title);
		nDialog.setSubTitle(subTitle);
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}
		});
		nDialog.show();
	}

	public static void nShowMessageSmall(final Context context, String message){
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small_no);
		nDialog.setTitle(message);
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}
		});
		nDialog.show();
	}

	public static void nShowSuccessMessage(final Context context){
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
		nDialog.setTitle(context.getString(R.string.N_FUNC_SUCCESS));
		nDialog.setOnActionClickListener((value, b) -> nDialog.dismiss());
		nDialog.show();
	}

	public static void nShowSuccessMessage(final Context context, NDialog.OnActionClickListener clickListener){
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
		nDialog.setAutoDismiss(true);
		nDialog.setTitle(context.getString(R.string.N_FUNC_SUCCESS));
		nDialog.setOnActionClickListener(clickListener);
		nDialog.show();
	}

	public static void nShowOkMessage(String message, Context context){
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
		nDialog.setTitle(message);
		nDialog.setOnActionClickListener((value, b) -> nDialog.dismiss());
		nDialog.show();
	}

	public static void showAffect(final Context context, String message)
	{
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		View v = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_affect_info, null);
		TextView messageView = v.findViewById(R.id.textMessage);
		if(null!=messageView) messageView.setText(Html.fromHtml(message));

		adb.setView(v);
		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		adb.show();
	}

	public static AlertDialog getEventDialog(Context context, String message){
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		adb.setTitle(R.string.MESSAGE);
		adb.setMessage(Html.fromHtml(message));
		adb.setNegativeButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		final AlertDialog ad = adb.create();
		ad.setCancelable(false);
		return ad;
	}

	public static void showEvent(final Context context, String message)
	{
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		View v = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_affect_info, null);
		TextView messageView = v.findViewById(R.id.textMessage);
		if(null!=messageView) messageView.setText(Html.fromHtml(message));

		adb.setView(v);
		adb.setNegativeButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		final AlertDialog ad = adb.create();
		ad.show();
	}

	public static AlertDialog getShowEventDialog(final Context context, String message)
	{
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		View v = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_affect_info, null);
		TextView messageView = v.findViewById(R.id.textMessage);
		if(null!=messageView) messageView.setText(Html.fromHtml(message));

		adb.setView(v);
		adb.setNegativeButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		final AlertDialog ad = adb.create();
		return ad;
	}

	public static void showAffectInfo(Context context, int siteId, String zoneText, String sectionText, String affectText, Event affect)
	{
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		View v = ((Activity)context).getLayoutInflater().inflate(R.layout.card_event_info, null);

		DBHelper dbHelper = DBHelper.getInstance(context);
		Event event = affect;

		String desc = affectText;
		Func.setTextViewText(v, context, R.id.cardEventDesc, 18, desc, null);

		String site = (siteId == -1? dbHelper.getSiteNameByDeviceSection(event.device, event.section): dbHelper.getSiteNameById(siteId));
		Func.setTextViewText(v, context, R.id.cardEventSite, 12, site, null);
		if(zoneText.equals("") && sectionText.equals("")){
			Func.showView(v, context, R.id.cardEventLoc, false);
		}else
		{
			String local = !zoneText.equals("") ?
					zoneText + (!sectionText.equals("") ? " • " +sectionText: "")
					: (!sectionText.equals("") ? sectionText : "");
			Func.showView(v, context, R.id.cardEventLoc, true);
			Func.setTextViewText(v, context, R.id.cardEventLoc, 12, local, null);
		}


		Device device = dbHelper.getDeviceById(event.device);
		if(null!=device){
			String model  = context.getString(R.string.EVENTS_DEVICE)
					+ device.getName();
			String serial = context.getString(R.string.EVENTS_SERIAL)
					+ device.account;
			Func.showView(v, context, R.id.cardEventDeviceLayout, true);
			Func.setTextViewText(v, context, R.id. cardEventModel, 12, model, null);
			Func.setTextViewText(v, context, R.id. cardEventSN, 12, serial, null);
		}else{
			Func.showView(v, context, R.id.cardEventDeviceLayout, false);
		}

		String servertime = Func.getServerTimeText(context, event.time);
		String serverdate = Func.getServerDateText(context, event.time);
		String localtime = Func.getTimeText(context, event.localtime);
		String localdate = Func.getDateText(context, event.localtime);
		Func.setTextViewText(v, context, R.id.cardEventTime, 12, (servertime + " " +serverdate), (localtime + " " + localdate));

		if(Func.debug())
		{
			Func.showView(v, context, R.id.cardEventDebugLayout, true);
			String key = "Key:" + event.classId + " " + event.detectorId + " "  + event.active;
			Func.setTextViewText(v, context, R.id.cardEventKey, 12, key, null);
			String extra = "Extra: " + " " + (event.jdata != null ? !event.jdata.equals("") ? event.jdata : "EMPTY" : "NULL");
			Func.setTextViewText(v, context, R.id.cardEventExtra, 12, extra, null);
			String id = "ID:" + " " + event.id;
			Func.setTextViewText(v, context, R.id.cardEventId, 12, id, null);
		}else{
			Func.showView(v, context, R.id.cardEventDebugLayout, false);
		}


		adb.setView(v);
		adb.setNegativeButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		if(((event.classId == 2)||(event.classId == 1)||(event.classId == 0))&&((event.jdata !=null)&&(!event.jdata.equals("")))){
			adb.setPositiveButton(context.getString(R.string.CRIT_EVENT_VIDEO_BUTTON), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialogInterface, int i)
				{
					if(event.jdata.contains("url")){
						Intent intent = new Intent(context, NCameraRecordViewActivity.class);
						intent.putExtra("event", event.id);
						context.startActivity(intent);
						((Activity)context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}else if(event.jdata.contains("notification")){
						Func.nShowMessage(context, context.getString(R.string.IV_CLIP_FAILED));
					}
				}
			});
		}


		final AlertDialog ad = adb.create();
		ad.show();
	}

	public static void showAffectInfoOld(Context context, int siteId, String zoneText, String sectionText, String affectText, Event affect)
	{
		DBHelper dbHelper = DBHelper.getInstance(context);

		String message = "<font color='#0095B6'><big>"+ affectText + "</big></font>" + "<br><br>";

		message += "<small>";
		if(!zoneText.equals("") || !sectionText.equals("")){
			message+= (!zoneText.equals("") ?
					zoneText + (!sectionText.equals("") ? "<br>" + "<br>" + sectionText: "")
					: (!sectionText.equals("") ? sectionText : ""));
			message+= "<br><br>";
		}
		final Device device = dbHelper.getDeviceById(affect.device);
		String finalDeviceDesc = context.getResources().getString(R.string.UNKNOWN_DEVICE);
		if(null!=device){
			finalDeviceDesc = device.getName() + "(" + device.account + ")";
		}
		message += finalDeviceDesc
				+ "<br>"
				+ (siteId == -1? dbHelper.getSiteNameByDeviceSection(affect.device, affect.section): dbHelper.getSiteNameById(siteId));
		message+= "<br><br>" + Func.getTimeDateText(context, affect.localtime);
		message += "</small>";

		Func.showAffect(context, message);
	}

	/*DEPRECATED*/
	public static void showEventInfoDialog(final Context context, int event_id)
	{
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		View v = ((Activity)context).getLayoutInflater().inflate(R.layout.card_event_info, null);

		DBHelper dbHelper = DBHelper.getInstance(context);
		Event event = dbHelper.getEventById(event_id);
		String desc = event.explainEventRegDescription(context);

		Func.setTextViewText(v, context, R.id.cardEventDesc, 18, desc, null);
		String site = dbHelper.getSiteNameByDeviceSection(event.device, event.section);
		Func.setTextViewText(v, context, R.id.cardEventSite, 12, site, null);
		String local = event.getEventLocationWithNum(context);
		Func.setTextViewText(v, context, R.id.cardEventLoc, 12, local, null);

		Device device = dbHelper.getDeviceById(event.device);
		if(null!=device){
			String model  = context.getString(R.string.EVENTS_DEVICE) + device.getName();
			String serial = context.getString(R.string.EVENTS_SERIAL)
					+ device.account;
			Func.showView(v, context, R.id.cardEventDeviceLayout, true);
			Func.setTextViewText(v, context, R.id. cardEventModel, 12, model, null);
			Func.setTextViewText(v, context, R.id. cardEventSN, 12, serial, null);
		}else{
			Func.showView(v, context, R.id.cardEventDeviceLayout, false);
		}

		String time = Func.getServerTimeText(context, event.time);
		String date = Func.getServerDateText(context, event.time);
		String localtime = Func.getTimeText(context, event.localtime);
		String localdate = Func.getDateText(context, event.localtime);
		Func.setTextViewText(v, context, R.id.cardEventTime, 12, (time + " " +date), (localtime + " " + localdate));

		if(Func.debug())
		{
			Func.showView(v, context, R.id.cardEventDebugLayout, true);
			String key = "Key:" + event.classId + " " + event.detectorId + " "  + event.active;
			Func.setTextViewText(v, context, R.id.cardEventKey, 12, key, null);
			String extra = "Extra: " + " " + (event.jdata != null ? !event.jdata.equals("") ? event.jdata : "EMPTY" : "NULL");
			Func.setTextViewText(v, context, R.id.cardEventExtra, 12, extra, null);
			String id = "ID:" + " " + event.id;
			Func.setTextViewText(v, context, R.id.cardEventId, 12, id, null);
		}else{
			Func.showView(v, context, R.id.cardEventDebugLayout, false);
		}


		adb.setView(v);
		adb.setNegativeButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		if(((event.classId == 2)||(event.classId == 1)||(event.classId == 0))&&((event.jdata !=null)&&(!event.jdata.equals("")))){
			adb.setPositiveButton(context.getString(R.string.CRIT_EVENT_VIDEO_BUTTON), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialogInterface, int i)
				{
					if(event.jdata.contains("url")){
						Intent intent = new Intent(context, NCameraRecordViewActivity.class);
						intent.putExtra("event", event.id);
						context.startActivity(intent);
						((Activity)context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}else if(event.jdata.contains("notification")){
						Func.nShowMessage(context, context.getString(R.string.IV_CLIP_FAILED));
					}
				}
			});
		}


		final AlertDialog ad = adb.create();
		ad.show();
	}

	public static void setTextViewText(View view, Context context, int id, int size, String text, String tooltip)
	{
		TextView textView = view.findViewById(id);
		if(null!=textView)
		{
			textView.setText(Html.fromHtml(text));
			textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
			if(null!=tooltip)
			{
				textView.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View view)
					{
						Func.showTooltip(view, context, context.getString(R.string.CONTROLLER_TIME_TITLE) + "\n" + tooltip);
					}
				});

			}
		}
	}

	private static void showTooltip(View view, Context context, String s)
	{
		Balloon balloon = new Balloon.Builder(context)
				.setArrowSize(10)
				.setArrowOrientation(ArrowOrientation.TOP)
				.setArrowVisible(true)
				.setWidthRatio(0.55f)
				.setTextSize(12f)
				.setArrowPosition(0.32f)
				.setCornerRadius(4f)
				.setAlpha(1f)
				.setText(s)
				.setTextColor(ContextCompat.getColor(context, R.color.brandColorDarkGrey))
				.setBackgroundColor(ContextCompat.getColor(context, R.color.brandColorLightGrey))
				.setBalloonAnimation(BalloonAnimation.NONE)
				.build();
		balloon.showAlignBottom(view);
	}

	public static  void showView(View view, Context context, int id,boolean visiblity){
		View view1 = view.findViewById(id);
		if(null!= view1){
			view1.setVisibility(visiblity ? View.VISIBLE : View.GONE);
		}
	}


	public static void showInfo(final Context context, String message)
	{
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		adb.setTitle(R.string.INFORMATION);
		adb.setMessage(Html.fromHtml(message));
		adb.setNegativeButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});

		final AlertDialog ad = adb.create();
		ad.setCancelable(false);
		ad.show();
	}

	public static AlertDialog.Builder adbWithCheckBox(Context context){
		AlertDialog.Builder adb = adbForCurrentSDK(context);
		View view = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_with_checkbox_pattern, null);
		adb.setView(view);
		return adb;
	}

	public static AlertDialog.Builder adbForCurrentSDK(Context context){
		AlertDialog.Builder builder;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			builder = new AlertDialog.Builder(context, R.style.AlertStyle);
		} else {
			builder = new AlertDialog.Builder(context);
		}
		return builder;
	}

	public static class ButtonOnTouch implements View.OnTouchListener{

		private final Context context;
		private final int startBackgroudColor;
		private final int touchBackgroudColor;
		private final int startTextColor;
		private final int touchTextColor;

		public ButtonOnTouch(Context context, int startBackgroudColor, int startTextColor, int touchBackgroudColor, int touchTextColor){
			this.context = context;
			this.startBackgroudColor = startBackgroudColor;
			this.startTextColor = startTextColor;
			this.touchBackgroudColor = touchBackgroudColor;
			this.touchTextColor = touchTextColor;
		}

		@Override
		public boolean onTouch(View v, MotionEvent event)
		{
			Button button = (Button) v;
			switch (event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
					((Button) v).setTextColor(context.getResources().getColor(touchTextColor));
					v.setBackgroundColor(context.getResources().getColor(touchBackgroudColor));
					break;
				case MotionEvent.ACTION_MOVE:
					((Button) v).setTextColor(context.getResources().getColor(startTextColor));
					v.setBackgroundColor(context.getResources().getColor(startBackgroudColor));
					break;
				case MotionEvent.ACTION_UP:
					((Button) v).setTextColor(context.getResources().getColor(startTextColor));
					v.setBackgroundColor(context.getResources().getColor(startBackgroudColor));
					break;
				case MotionEvent.ACTION_CANCEL:
					((Button) v).setTextColor(context.getResources().getColor(startTextColor));
					v.setBackgroundColor(context.getResources().getColor(startBackgroudColor));
					break;
			}
			return false;
		}
	}
//
	public static  byte[] serialize(LocalConfig message) throws IOException
	{
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(bos);
		out.writeObject(message);
		return bos.toByteArray();
	}

	public static Reply_SURVEY deserialize(byte[] data) throws IOException, ClassNotFoundException
	{
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		ObjectInputStream in = new ObjectInputStream(bis);
		Reply_SURVEY r = (Reply_SURVEY) in.readObject();
		return r;
	}

	public static byte[] intToBytes( final int i ) {
		ByteBuffer bb = ByteBuffer.allocate(4);
		bb.putInt(i);
		return bb.array();
	}

	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len/2];

		for(int i = 0; i < len; i+=2){
			data[i/2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i+1), 16));
		}

		return data;
	}

	public static byte[] intToByteArray(int v){
		byte[] b = new byte[4];
		for(int i =0; i < 4; i++){
			b[i] = (byte) (v&0xff);
			v >>=8;
		}
		return  b;
	}

	public static byte[] stringToByteArrayGlueTwo(String data){
		byte[] bytes = new byte[data.length()/2];
		char[] chars = data.toCharArray();
		for(int i = 0; i < data.length(); i++){
			bytes[i/2] = (byte)((Character.digit(chars[i],16) << 4) | (Character.digit(chars[i+1],16)));
			i++;
		}
		return bytes;
	}

	public static byte[] stringToByteArray(String data){
		byte[] bytes = new byte[data.length()];
		char[] chars = data.toCharArray();
		for(int i=0; i < data.length(); i++){
			bytes[i] = (byte) Character.digit(chars[i], 16);
		}
		return bytes;
	}

	public static byte[] reverseByteArray(byte[] bytes){
		if (bytes == null) return null;
		int i = 0;
		int j = bytes.length - 1;
		byte tmp;
		while (j > i) {
			tmp = bytes[j];
			bytes[j] = bytes[i];
			bytes[i] = tmp;
			j--;
			i++;
		}
		return bytes;
	}


	public static float byteArrayToFloat(byte[] bytes)
	{
		if(null!=bytes){
			return ByteBuffer.wrap(bytes).getFloat();
		}
		return 0;
	}


	public static long hexLeToLong(String data, int start, int length){
		long result;
		if(data.length() >= (start + length))
		{
			char[] chars = (data.substring(start, start + length)).toCharArray();
			switch (length)
			{
				case 1:
					result = Character.digit(chars[0], 16);
					break;
				case 2:
					result = (byte) ((Character.digit(chars[0], 16) << 4) | (Character.digit(chars[1], 16)));
					break;
				case 4:
					result = (short) ((Character.digit(chars[0], 16) << 12) | (Character.digit(chars[1], 16) << 8) | (Character.digit(chars[2], 16) << 4) | (Character.digit(chars[3], 16)));
					break;
				default:
					result = 0;
					if ((length & 1) == 0)
					{
						for (int i = 0; i < length; )
						{
							result |= ((Character.digit(chars[i++], 16) << 4) | Character.digit(chars[i], 16)) << ((i++ >> 1) << 3);
						}
					}
					break;
			}
			return result;
		}
		return 0;
	}

	public static void dropPreferences(PrefUtils prefUtils, boolean dropDB){
		Func.log_d(D3Service.LOG_TAG, "Drop preferences");

		prefUtils.removeFingerPrintEnabled();
		prefUtils.removePinEnabled();
		prefUtils.removeNotificationProblem();
		prefUtils.removeNotificationProbemMinTime();
		prefUtils.removeNotificationProblemDifTime();

		if(dropDB) {
            prefUtils.removeCurrentSite();
            prefUtils.removeSharedScreen();

        }

		prefUtils.setCurrentUserId(0); // сбрасываем, чтобы создать новую базу при необходимости
	}

	public static void loadDefaultPreferences(SharedPreferences sp, Context context)
	{
		if(0 == PrefUtils.getInstance(context).getCommandCount())
			PrefUtils.getInstance(context).setCommandCount(1);
	}

	public static ArrayList<Integer> getCurrentHistoryClasses(Context context)
	{
		if(App.getMode())
		{
			final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
			Set<String> stringSet = sharedPreferences.getStringSet("prof_history_settings", null);
			if (stringSet != null)
			{
				ArrayList<Integer> classes = new ArrayList<>();
				Set<String> classesSet = new HashSet<>();

				try
				{
					for (String string : stringSet)
					{
						classes.add(Integer.valueOf(string));
					}
				} catch (Exception e)
				{
					LinkedList<EClass> eventClasses = D3Service.d3XProtoConstEvent.classes;
					for (String string : stringSet)
					{
						for (EClass eClass : eventClasses)
						{
							if (string.equals(eClass.caption))
							{
								classes.add(eClass.id);
								classesSet.add(Integer.toString(eClass.id));
							}
						}
					}
					SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
					sp.edit().putStringSet("prof_history_settings", classesSet).apply();
				}
				return classes;
			}
		}else{
			ArrayList<Integer> classes = new ArrayList<>();
			String[] default_types = context.getResources().getStringArray(R.array.class_types_default);
			for(String defalt:default_types){
				classes.add(Integer.valueOf(defalt));
			}
			return classes;
		}
		return null;
	}

	public static int getCommandCount(Context context){
		int command_count = PrefUtils.getInstance(context).getCommandCount();
		if((0 == command_count)|| (100000 == command_count)){
			command_count = 1;
		}
		PrefUtils.getInstance(context).setCommandCount(command_count + 1);
		return command_count;
	}

	public static int getNotificationId(Context context){
		int notification_count = PrefUtils.getInstance(context).getNotificationCount();
		if((0 == notification_count)|| (100000 == notification_count)){
			notification_count = 1001;
		}
		PrefUtils.getInstance(context).setNotificationCount(notification_count + 1);
		return notification_count;
	}


	public static boolean getBooleanSPDefTrue(SharedPreferences sp, String key){
		boolean result;
		try{
			result = sp.getBoolean(key, true);
		}catch (Exception e){
			try{
				result = sp.getInt(key, 1)!=0;
			}catch (Exception e1){
				result = false;
			}
		}
		return result;
	}

	public static boolean getBooleanSPDefFalse(SharedPreferences sp, String key){
		boolean result;
		try{
			result = sp.getBoolean(key, false);
		}catch (Exception e){
			try{
				result = sp.getInt(key, 0)!=0;
			}catch (Exception e1){
				result = false;
			}
		}
		return result;
	}


	public static int GetPixelFromDips(float pixels, Context context)
	{
		// Get the screen's density scale
		final float scale = context.getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

	public static List<String> list(String... values) {
		return Collections.unmodifiableList(Arrays.asList(values));
	}

	public static void showServiceStopedDialog(final Context context){
		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		if(getBooleanSPDefTrue(sp, "pref_service_stoped_show"))
		{
			AlertDialog.Builder adb = adbWithCheckBox(context);
			adb.setTitle(R.string.TITLE_ATTENTION);
			adb.setMessage(context.getResources().getString(R.string.SERVICE_BENN_STOPPED_ERROR_1) + " " + context.getResources().getString(R.string.application_name) + context.getResources().getString(R.string.SERVICE_BENN_STOPPED_ERROR_2));
			adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{

				}
			});

			final AlertDialog ad = adb.create();
			ad.show();

			ad.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					CheckBox checkBox = ad.findViewById(R.id.dialog_checkbox);
					if (checkBox.isChecked())
					{
						sp.edit().putBoolean("pref_service_stoped_show", false).apply();
					}
					ad.dismiss();
				}
			});
		}
	}

	public static void showDozeDialog(final Context context){
		final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		if(getBooleanSPDefTrue(sp, "pref_doze_show"))
		{
			AlertDialog.Builder adb = adbWithCheckBox(context);
			adb.setTitle(R.string.TITLE_DOZE_MODE);
			adb.setMessage(R.string.MESS_DOZE_MODE);
			adb.setPositiveButton(R.string.GO, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{

				}
			});
			adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					//				((Activity)context).finishAffinity();
				}
			});

			final AlertDialog ad = adb.create();
			ad.show();

			ad.getButton(DialogInterface.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					CheckBox checkBox = ad.findViewById(R.id.dialog_checkbox);
					if (checkBox.isChecked())
					{
						sp.edit().putBoolean("pref_doze_show", false).apply();
					}
					ad.dismiss();
				}
			});

			ad.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					CheckBox checkBox = ad.findViewById(R.id.dialog_checkbox);
					if (checkBox.isChecked())
					{
						sp.edit().putBoolean("pref_doze_show", false).apply();
					}
					Intent intent = new Intent();
					intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					context.startActivity(intent);
					((Activity) context).finishAffinity();
				}
			});
		}
	}

	private static void showLicenseCancelDialog(final Context context){
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small_decline);
		nDialog.setTitle(context.getResources().getString(R.string.MESS_LICENSE));
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_OK:
						((Activity)context).finishAffinity();
						break;
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}
		});
		nDialog.show();
	}

	public static void showLicenseAgreementDialog(final Context context)
	{
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_license);
		nDialog.setTitle(context.getResources().getString(R.string.TITLE_LICENSE));
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_OK:
						PrefUtils.getInstance(context).setLicenseAgreed();
						nDialog.dismiss();
						break;
					case NActionButton.VALUE_CANCEL:
						showLicenseCancelDialog(context);
						break;
				}
			}
		});
		nDialog.show();
	}


	public static void showUpdateDialog(final Context context){
		AlertDialog.Builder abd = adbForCurrentSDK(context);
		abd.setTitle(R.string.TITLE_NEED_UPDATE);
		abd.setMessage(R.string.D3_OLD_PROTOCOL_VRERSION_MESSAGE);
		abd.setPositiveButton(R.string.UPDATE, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				final String appPackageName = context.getPackageName(); // getPackageName() from Context or Activity object
				try {
					context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
					context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
				}
			}
		});
		abd.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				((Activity)context).finishAffinity();
			}
		});
		abd.setCancelable(false);
		abd.show();
	}

	public static void showLocalesDifferenceDialog(final Context context)
	{
		AlertDialog.Builder abd = adbForCurrentSDK(context);
		abd.setTitle("Смена языка");
		abd.setMessage("");
		abd.setPositiveButton(R.string.UPDATE, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

			}
		});
		abd.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		abd.setCancelable(false);
		abd.show();
	}




	/*FOR IOS*/
	public static void deserializeXproto() throws JSONException
	{
		D3XProtoConstEvent d3XProtoConstEvent = D3Service.d3XProtoConstEvent;
		LinkedList<EClass> eClasses = d3XProtoConstEvent.classes;
		JSONObject new_classes_object = new JSONObject();
		JSONArray new_classes_array = new JSONArray();
		for (EClass eClass: eClasses){
			JSONObject new_class_object = new JSONObject();
			new_class_object.put("id", eClass.id);
			new_class_object.put("name", eClass.caption);
			new_class_object.put("iconSmall", eClass.iconSmall);
			new_class_object.put("iconBig", eClass.iconBig);
			new_class_object.put("affect", eClass.affect);


			LinkedList<EDetector> detectors = eClass.detectors;
			LinkedList<EReason> reasons = eClass.reasons;

			JSONArray new_reasons_array = new JSONArray();
			for (EReason reason :reasons){
				JSONObject new_reason = new JSONObject();
				new_reason.put("id", reason.id);
				new_reason.put("name",reason.caption);
				new_reason.put("iconSmall", reason.iconSmall);
				new_reason.put("iconBig", reason.iconBig);
				new_reason.put("affect", reason.affect);

				JSONArray new_detectors_array = new JSONArray();
				for(EDetector detector:detectors){
					boolean compare = false;
					EReason reasonToSave = new EReason();
					LinkedList<EReason> reasons1 = detector.reasons;
					for(EReason reason1:reasons1){
						if(reason.id == reason1.id){
							compare = true;
							reasonToSave = reason1;
						}
					}
					if(compare){
						JSONObject new_detector = new JSONObject();
						new_detector.put("id", detector.id);
						new_detector.put("name", detector.caption);
						new_detector.put("iconSmall", reasonToSave.iconSmall);
						new_detector.put("iconBig", reasonToSave.iconBig);
						new_detector.put("affect", reasonToSave.affect);

						LinkedList<EStatement> reasonStatements4Detector = reasonToSave.statements;
						JSONArray reasonStatementsArray4Detector = new JSONArray();
						for (EStatement eStatement :reasonStatements4Detector){
							JSONObject jsonObject = new JSONObject();
							jsonObject.put("id", eStatement.id);
							jsonObject.put("name", eStatement.caption);
							jsonObject.put("desc", eStatement.description);
							jsonObject.put("icon", eStatement.icon);
							JSONArray icons = new JSONArray();
							if(null!=eStatement.icons){
								for(int i =0; i < eStatement.icons.size(); i++){
									icons.put(eStatement.icons.get(i).src);
								}
								jsonObject.put("icons", icons);
							}
							reasonStatementsArray4Detector.put(jsonObject);
						}
						new_detector.put("statements", reasonStatementsArray4Detector);
						new_detectors_array.put(new_detector);
					}
				}
				if(new_detectors_array.length()!=0){
					new_reason.put("detectors", new_detectors_array);
				}

				LinkedList<EStatement> reasonStatements = reason.statements;
				JSONArray reasonStatementsArray = new JSONArray();
				for (EStatement eStatement :reasonStatements){
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("id", eStatement.id);
					jsonObject.put("name", eStatement.caption);
					jsonObject.put("desc", eStatement.description);
					jsonObject.put("icon", eStatement.icon);
					JSONArray icons = new JSONArray();
					if(null!=eStatement.icons){
						for(int i =0; i < eStatement.icons.size(); i++){
							icons.put(eStatement.icons.get(i).src);
						}
						jsonObject.put("icons", icons);
					}
					reasonStatementsArray.put(jsonObject);
				}
				new_reason.put("statements", reasonStatementsArray);

				new_reasons_array.put(new_reason);
			}
			if(new_reasons_array.length()!=0)
			{
				new_class_object.put("reasons", new_reasons_array);
			}

			JSONArray new_class_detectors = new JSONArray();
			for(EDetector detector:detectors){
				JSONObject new_class_detector = new JSONObject();
				new_class_detector.put("id", detector.id);
				new_class_detector.put("name", detector.caption);
				new_class_detector.put("iconSmall", eClass.iconSmall);
				new_class_detector.put("iconBig", eClass.iconBig);
				new_class_detector.put("affect", eClass.affect);

				LinkedList<EStatement> detectorStatements = detector.statements;
				JSONArray detectorStatementsArray = new JSONArray();
				for (EStatement eStatement :detectorStatements){
					JSONObject jsonObject = new JSONObject();
					jsonObject.put("id", eStatement.id);
					jsonObject.put("name", eStatement.caption);
					jsonObject.put("desc", eStatement.description);
					jsonObject.put("icon", eStatement.icon);
					JSONArray icons = new JSONArray();
					if(null!=eStatement.icons){
						for(int i =0; i < eStatement.icons.size(); i++){
							icons.put(eStatement.icons.get(i).src);
						}
						jsonObject.put("icons", icons);
					}
					detectorStatementsArray.put(jsonObject);
				}
				new_class_detector.put("statements", detectorStatementsArray);
				new_class_detectors.put(new_class_detector);
			}

			if(new_class_detectors.length()!=0){
				new_class_object.put("detectors", new_class_detectors);
			}

			LinkedList<EStatement> eStatements = eClass.statements;
			JSONArray array = new JSONArray();
			for (EStatement eStatement :eStatements){
				JSONObject jsonObject = new JSONObject();
				jsonObject.put("id", eStatement. id);
				jsonObject.put("name", eStatement.caption);
				jsonObject.put("desc", eStatement.description);
				jsonObject.put("icon", eStatement.icon);
				JSONArray icons = new JSONArray();
				if(null!=eStatement.icons){
					for(int i =0; i < eStatement.icons.size(); i++){
						icons.put(eStatement.icons.get(i).src);
					}
					jsonObject.put("icons", icons);
				}
				array.put(jsonObject);
			}
			new_class_object.put("statements",array);



			new_classes_array.put(new_class_object);

		}
		new_classes_object.put("classes", new_classes_array);

		String s = new_classes_object.toString(4);
		byte[] b = s.getBytes();
		String ss = new String(b, StandardCharsets.UTF_8);

//		try {
//			FileWriter file = new FileWriter("/data/data/" + App.getContext().getPackageName() + "/" + "jso.json");
//			file.write(ss);
//			file.flush();
//			file.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

		/*11.06.2021 v*/

		LinkedList<ZType> zTypes = d3XProtoConstEvent.zonetypes;
		LinkedList<DWIType> iTypes = d3XProtoConstEvent.dwitypes;
		LinkedList<DWOType> oTypes = d3XProtoConstEvent.dwotypes;
		LinkedList<DeviceType> deviceTypes = d3XProtoConstEvent.devicetypes;
		LinkedList<SType> sectionTypes = d3XProtoConstEvent.sectiontypes;

		JSONObject types = new JSONObject();
		JSONArray zones = new JSONArray();
		JSONArray inputs = new JSONArray();
		JSONArray outputs = new JSONArray();
		JSONArray devices = new JSONArray();
		JSONArray sections = new JSONArray();

		try
		{
			for (ZType zType : zTypes)
			{
				JSONObject object = new JSONObject();
				object.put("id", zType.id);
				object.put("title", zType.caption);
				object.put("subTitle", zType.caption_e);
				object.put("img", zType.img);

				if(null!=zType.icons)
				{
					JSONArray zTypeIcons = new JSONArray();
					for (Icon icon : zType.icons)
					{
						zTypeIcons.put(icon.src);
					}

					object.put("icons", zTypeIcons);
				}

				LinkedList<DType> dTypes = zType.detectortypes;
				if(null!=dTypes)
				{
					JSONArray detectors = new JSONArray();

					for (DType dType : dTypes)
					{
						JSONObject dObject = new JSONObject();
						dObject.put("id", dType.id);
						dObject.put("title", dType.caption);
						dObject.put("sectoinMask", dType.sectionmask);
						dObject.put("sectionDefault", dType.sectiondefault);
						dObject.put("isDefaultDetector", dType.def);

						LinkedList<AType> aTypes = dType.alarmtypes;
						if(null!=aTypes)
						{
							JSONArray alarms = new JSONArray();

							for (AType aType : aTypes)
							{
								JSONObject aObject = new JSONObject();
								aObject.put("id", aType.id);
								aObject.put("title", aType.caption);
								aObject.put("isDefaultAlarm", aType.def);

								JSONArray aTypeIcons = new JSONArray();

								if(null!=aType.icons)
								{
									for (Icon icon : aType.icons)
									{
										aTypeIcons.put(icon.src);
									}

									aObject.put("icons", aTypeIcons);
								}

								alarms.put(aObject);
							}

							dObject.put("alarmTypes", alarms);
						}

						detectors.put(dObject);
					}

					object.put("detectorTypes", detectors);


				}

				zones.put(object);
			}

			for (DWIType iType : iTypes)
			{
				JSONObject object = new JSONObject();
				object.put("id", iType.id);
				object.put("title", iType.caption);

				if(null!=iType.icons)
				{
					JSONArray inputIcons = new JSONArray();
					for (Icon icon : iType.icons)
					{
						inputIcons.put(icon.src);
					}

					object.put("icons", inputIcons);
				}

				LinkedList<DType> dTypes = iType.detectortypes;

				if(null!=dTypes)
				{
					JSONArray detectors = new JSONArray();

					for (DType dType : dTypes)
					{
						JSONObject dObject = new JSONObject();
						dObject.put("id", dType.id);
						dObject.put("title", dType.caption);
						dObject.put("sectoinMask", dType.sectionmask);
						dObject.put("sectionDefault", dType.sectiondefault);
						dObject.put("isDefaultDetector", dType.def);

						LinkedList<AType> aTypes = dType.alarmtypes;

						if(null!=aTypes)
						{
							JSONArray alarms = new JSONArray();

							for (AType aType : aTypes)
							{
								JSONObject aObject = new JSONObject();
								aObject.put("id", aType.id);
								aObject.put("title", aType.caption);
								aObject.put("isDefaultAlarm", aType.def);

								if(null!=aType.icons)
								{
									JSONArray aTypeIcons = new JSONArray();
									for (Icon icon : aType.icons)
									{
										aTypeIcons.put(icon.src);
									}

									aObject.put("icons", aTypeIcons);
								}
								alarms.put(aObject);
							}

							dObject.put("alarmTypes", alarms);
						}
						detectors.put(dObject);
					}

					object.put("detectorTypes", detectors);

				}
				inputs.put(object);
			}

			for (DWOType oType : oTypes)
			{
				JSONObject object = new JSONObject();
				object.put("id", oType.id);
				object.put("title", oType.caption);

				if(null!=oType.icons)
				{
					JSONArray outputIcons = new JSONArray();
					for (Icon icon : oType.icons)
					{
						outputIcons.put(icon.src);
					}

					object.put("icons", outputIcons);
				}

				outputs.put(object);
			}

			for (DeviceType deviceType:deviceTypes){
				JSONObject device = new JSONObject();
				device.put("id", deviceType.id);
				device.put("title", deviceType.caption);
				if(null!=deviceType.icons && deviceType.icons.size() > 0){
					JSONArray icons = new JSONArray();
					for(Icon icon:deviceType.icons){
						icons.put(icon.src);
					}
					device.put("icons", icons);
				}
				devices.put(device);
			}

			for(SType sectionType : sectionTypes){
				JSONObject section = new JSONObject();
				section.put("id", sectionType.id);
				section.put("title", sectionType.caption);
				if(null!=sectionType.icons && sectionType.icons.size()>0){
					JSONArray icons = new JSONArray();
					for(Icon icon:sectionType.icons){
						icons.put(icon.src);
					}
					section.put("icons", icons);
				}
				sections.put(section);
			}


		}catch (Exception e){
			e.printStackTrace();
		}

		types.put("inputTypes", inputs);
		types.put("outputTypes", outputs);
		types.put("zoneTypes", zones);
		types.put("deviceTypes", devices);
		types.put("sectionTypes", sections);

		s = types.toString(4);
		b = s.getBytes();
		ss = new String(b, StandardCharsets.UTF_8);

//		try {
//			FileWriter file = new FileWriter("/data/data/" + App.getContext().getPackageName() + "/" + "jso.json");
//			file.write(ss);
//			file.flush();
//			file.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

	}

	private static final String PICASSO_CACHE = "picasso-cache";

	public static void clearCache(Context context) {
		final File cache = new File(
				context.getApplicationContext().getCacheDir(),
				PICASSO_CACHE);
		if (cache.exists()) {
			deleteFolder(cache);
		}
	}

	private static void deleteFolder(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory()) {
			for (File child : fileOrDirectory.listFiles())
				deleteFolder(child);
		}
		fileOrDirectory.delete();
	}


	/*update context after lang change changing language in app*/
	public static boolean updateContext(Context context){
		try{
			context = LocaleHelper.onAttach(context);

//			App.newContext();
//			((App) ((Activity) context).getApplication()).newContext();
			App.updateContext();

			if(!PrefUtils.getInstance(context).getImportSettings() && 0 != PrefUtils.getInstance(context).getCurrentUserId()) DBHelper.updateDBInstance(context);

			PrefUtils.updateInstance(context);

			D3Service.reparseRaw(context);

			context.sendBroadcast(new Intent(D3Service.BROADCAST_APPLICATION_LOCALE_CHANGE));

			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	public static Bitmap textAsBitmap(String text, float textSize, int textColor) {
		Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setTextSize(textSize);
		paint.setColor(textColor);
		paint.setTextAlign(Paint.Align.LEFT);
		float baseline = -paint.ascent(); // ascent() is negative
		int width = (int) (paint.measureText(text) + 0.0f); // round
		int height = (int) (baseline + paint.descent() + 0.0f);
		Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

		Canvas canvas = new Canvas(image);
		canvas.drawText(text, 0, baseline, paint);
		return image;
	}

	public static  String getScriptDataForCurrLang(String s, String iso)
	{
		try
		{
			JSONArray jsonArray = new JSONArray(s);
			String value = "";
			boolean match = false;
			int i =0;
			while (!match){
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				if(jsonObject.getString("iso").equals(iso)){
					value = jsonObject.getString("value");
					match = true;
				}
				i++;
			}
			return  value;
		} catch (Exception e)
		{
			e.printStackTrace();
			return  " " + App.getContext().getString(R.string.UNKNOWN);
		}
	}

	public static String getS(JSONObject jsonObject, String key)
	{
		try{
			return jsonObject.getString(key);
		}catch (JSONException e){
			return null;
		}
	}

	public static boolean getB(JSONObject jsonObject, String key){
		try
		{
			return jsonObject.getBoolean(key);
		} catch (JSONException e)
		{
			return true;
		}
	}

	/*get time zone offset in minutes*/
	public static int getTimeZoneOffset(){
		int tz = TimeZone.getDefault().getRawOffset()/60000;
		return tz;
	}


	public static void setDynamicHeight(ListView listView) {
		ListAdapter adapter = listView.getAdapter();
		//checkStates adapter if null
		if (adapter == null) {
			return;
		}
		int height = 0;
		int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
		for (int i = 0; i < adapter.getCount(); i++) {
			View listItem = adapter.getView(i, null, listView);
			listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			height += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
		layoutParams.height = height + (listView.getDividerHeight() * (adapter.getCount() - 1));
		listView.setLayoutParams(layoutParams);
		listView.requestLayout();
	}

	public static void nSetDynamicHeight(ListView listView) {
		ListAdapter adapter = listView.getAdapter();
		//checkStates adapter if null
		if (adapter == null) {
			return;
		}
		int height = 0;
		int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
		for (int i = 0; i < adapter.getCount(); i++) {
			View listItem = adapter.getView(i, null, listView);
			listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			height += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
		layoutParams.height = height + (listView.getDividerHeight() * (adapter.getCount() - 1));
		listView.setLayoutParams(layoutParams);
		listView.requestLayout();
	}

	public static void setDynamicHeight(GridView listView, int colums)
	{
		ListAdapter adapter = listView.getAdapter();
		//checkStates adapter if null
		if (adapter == null) {
			return;
		}
		int height = 0;
		int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
		for (int i = 0; i < adapter.getCount(); i++) {
			View listItem = adapter.getView(i, null, listView);
			listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			int h = listItem.getMeasuredHeight();
			if((i%colums)==0){
				height += h;
			}
		}
		ViewGroup.LayoutParams layoutParams = listView.getLayoutParams();
		layoutParams.height = height;
		listView.setLayoutParams(layoutParams);
		listView.requestLayout();
	}

	public static void restartD3ServiceForced(Context context, String from){
		context.stopService(new Intent(context.getApplicationContext(), D3Service.class));
		try {
			startD3Service(context.getApplicationContext(), from);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public static void restartD3Service(Context context, String from){
		if(!Func.isServiceRunning(context.getApplicationContext(), D3Service.class)){
//			startD3Service(context.getApplicationContext(), from);
			try {
				startD3Service(context, from);
			}catch (Exception e){
				e.printStackTrace();
			}
		}
	}

	public  static  void startD3Service(Context context, String from){
		LocaleHelper.onAttach(context);
		Func.updateContext(context);

		Func.log_d(D3Service.LOG_TAG , "Start D3Service MANUAL attempt to send API command OR onResume ");
		Func.log_d(D3Service.LOG_TAG , "From " + from);

		if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) && foregroundMode(context))
		{
			context.startForegroundService(new Intent(context, D3Service.class));
//			if(null!=getServiceChannel(context)){
//				if(getServiceChannel(context).getImportance() == NotificationManager.IMPORTANCE_NONE){
//					deleteServiceChannel(context);
//					updateServiceChannelId(context);
//					createServiceChannel(context, NotificationManager.IMPORTANCE_HIGH);
//				}
//				context.startForegroundService(new Intent(context, D3Service.class));
//			}
//			try {
//				deleteServiceChannel(context);
//				updateServiceChannelId(context);
//				createServiceChannel(context, NotificationManager.IMPORTANCE_HIGH);
//			}catch (SecurityException e) {
//				e.printStackTrace();
//			}
//			context.startForegroundService(new Intent(context, D3Service.class));
		}else
		{
			context.startService(new Intent(context, D3Service.class));
		}
	}

	public static boolean foregroundMode(Context context) {
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ){
			if(getServiceChannel(context) != null && getServiceChannel(context).getImportance() > NotificationManagerCompat.IMPORTANCE_NONE){
				return true;
			}else{
				return false;
			}
		}else{
			return PrefUtils.getInstance(context).getConnectionMode();
		}

//			if(PrefUtils.getInstance(context).getConnectionMode()) {
//			if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ){
//				return getServiceChannel(context) != null &&
//						getServiceChannel(context).getImportance() > NotificationManagerCompat.IMPORTANCE_NONE;
//			}
//			return true;
//		}
//		return false;
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public static void createServiceChannel(Context context, int importance){
		NotificationUtils notificationUtils = NotificationUtils.getInstance(context);
		notificationUtils.createChannelNoVibro(getServiceChannelId(context),
				context.getResources().getString(R.string.NOTIF_CONNECTION_SERVICE_ENABLED),
				importance
		);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public static NotificationChannel getServiceChannel(Context context){
		NotificationUtils notificationUtils = NotificationUtils.getInstance(context);
		return notificationUtils.getServiceNotifChannel();
	}

	public static String getServiceChannelId(Context context) {
		NotificationUtils notificationUtils = NotificationUtils.getInstance(context);
		return notificationUtils.getServiceNotifChannelId();
	}

	public static void updateServiceChannelId(Context context) {
		NotificationUtils notificationUtils = NotificationUtils.getInstance(context);
		notificationUtils.updateServiceNotifChannelId();
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	public static void deleteServiceChannel(Context context){
		NotificationUtils notificationUtils = NotificationUtils.getInstance(context);
		notificationUtils.deleteServiceNotifChannel();
	}

	public static boolean nSendMessageToServer(D3Element d3Element, String q, JSONObject data, D3Service localService, Context context, boolean command, int sending){
		if(sending == 0)
		{
			boolean element_online = true;
			int armed = Const.STATUS_DISARMED;
			D3Request d3Request;
			UserInfo userInfo = DBHelper.getInstance(context).getUserInfo();

			if(command && null!=d3Element){
				int device_id = 0;
				if(d3Element instanceof Zone){
					Zone zone = (Zone) d3Element;
					device_id = zone.device_id;
					element_online = DBHelper.getInstance(context).getZoneConnectionStatus(zone.device_id, zone.section_id, zone.id);
				}else if (d3Element instanceof Device){
					device_id = d3Element.id;
					element_online = DBHelper.getInstance(context).isDeviceOnline(d3Element.id);
				}else if(d3Element instanceof Section){
					Section section = (Section) d3Element;
					device_id = section.device_id;
					element_online = DBHelper.getInstance(context).isDeviceOnline(section.device_id);
				}
				switch (q){
					case Command.ARM:
					case Command.DISARM:
					case Command.SWITCH:
					case Command.RESET:
						break;
					default:
						armed = DBHelper.getInstance(context).getDeviceArmStatus(device_id);
						break;
				}
				d3Request = D3Request.createCommand(userInfo.roles, device_id, q, data, Func.getCommandCount(context));
			}else{
				d3Request = D3Request.createMessage(userInfo.roles, q, data);
			}
			if(d3Request.error == null){
				if(element_online)
				{
					if(armed == Const.STATUS_DISARMED){
						if (null != localService)
						{
							if (localService.send(d3Request.toString()))
							{
								if (command) sendIntentCommandBeenSend(context, 1);
								return true;
							} else if (command)
							{
								sendIntentCommandBeenSend(context, -1);
							}
						}
					}else{
						Func.showDeviceArmedMessage(context);
					}
				}else{
					Func.showNoConnectionMessage(context);
				}
			}else{
				Func.nShowMessage(context, d3Request.error);
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	}

	/*DEPRECATED 25.09.2020*/
//	public static boolean sendMessageToServer(D3Element d3Element, D3Request d3Request, D3Service myService, Context context, boolean command, int sending)
//	{
//		if(sending == 0)
//		{
//			boolean online = true;
//			int armed = Const.STATUS_DISARMED;
//			if(command && null!=d3Element){
//				int device_id = 0;
//				if(d3Element instanceof Zone){
//					Zone zone = (Zone) d3Element;
//					device_id = zone.device_id;
//					online = DBHelper.getInstance(context).getZoneConnectionStatus(zone.device_id, zone.section_id, zone.id);
//				}else if (d3Element instanceof Device){
//					device_id = d3Element.id;
//					online = DBHelper.getInstance(context).isDeviceOnline(d3Element.id);
//				}else if(d3Element instanceof Section){
//					Section section = (Section) d3Element;
//					device_id = section.device_id;
//					online = DBHelper.getInstance(context).isDeviceOnline(section.device_id);
//				}
//				armed = DBHelper.getInstance(context).getDeviceArmStatus(device_id);
//			}
//			if(online)
//			{
//				if(armed == Const.STATUS_DISARMED){
//					if (null != myService)
//					{
//						if (myService.send(d3Request.toString()))
//						{
//							if (command) sendIntentCommandBeenSend(context, 1);
//							return true;
//						} else if (command)
//						{
//							sendIntentCommandBeenSend(context, -1);
//						}
//					}
//				}else{
//					Func.showDeviceArmedMessage(context);
//				}
//			}else{
//				Func.showNoConnectionMessage(context);
//			}
//		}else{
//			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
//		}
//		return false;
//	};

	private static void sendIntentCommandBeenSend(Context context, int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	public static boolean isFirstInstall(Context context) {
		try {
			long firstInstallTime =   App.getContext().getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
			long lastUpdateTime = App.getContext().getPackageManager().getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
			return firstInstallTime == lastUpdateTime;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
			return true;
		}
	}



	public static long isInstallFromUpdate(Context context) {
		try {
			long firstInstallTime =   App.getContext().getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
			long lastUpdateTime = App.getContext().getPackageManager().getPackageInfo(context.getPackageName(), 0).lastUpdateTime;
			if( firstInstallTime != lastUpdateTime){
				return lastUpdateTime;
			}
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static void setTextViewText(View view, Context context, int id, int size, String text)
	{
		TextView textView = view.findViewById(id);
		textView.setText(text);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
	}

	public static void showNoConnectionMessage(Context context)
	{
		nShowMessage(context, context.getString(R.string.N_FUNC_NO_CONNECTION));
	}

	public static void showDeviceArmedMessage(Context context)
	{
		nShowMessage(context, context.getString(R.string.N_FUNC_NEED_DISARM));
	}

	public static Bitmap drawableToBitmap (Drawable drawable) {
		Bitmap bitmap = null;

		if (drawable instanceof BitmapDrawable) {
			BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
			if(bitmapDrawable.getBitmap() != null) {
				return bitmapDrawable.getBitmap();
			}
		}

		if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
			bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
		} else {
			bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
		}

		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);
		return bitmap;
	}

	public static int getTextSize(Context context, TextView textView){
//		return (int) (textView.getTextSize()/ context.getResources().getDisplayMetrics().scaledDensity);
		return (int)textView.getTextSize();
	}

	private static Resources getResources(Context context)
	{
		return LocaleHelper.onAttach(context).getResources();
	}

	public static byte[] resizeBitmap(byte[] source)
	{
		Bitmap bitmap = getImage(source);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		return stream.toByteArray();
	}

	public static Bitmap resizeBitmap(Bitmap source)
	{
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		source.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] data = stream.toByteArray();
		return BitmapFactory.decodeByteArray(data, 0, data.length);

	}

	// convert from bitmap to byte array
	public static byte[] getBytes(Bitmap bitmap) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
		return stream.toByteArray();
	}

	// convert from byte array to bitmap
	public static Bitmap getImage(byte[] image) {
		return BitmapFactory.decodeByteArray(image, 0, image.length);
	}

	public static Bitmap scaleToFitWidth(Bitmap b, int width)
	{
		float factor = width / (float) b.getWidth();
		return Bitmap.createScaledBitmap(b, width, (int) (b.getHeight() * factor), true);
	}

	public static Bitmap convertFromDB(String base64Str) throws IllegalArgumentException
	{
		byte[] decodedBytes = Base64.decode(
				base64Str.substring(base64Str.indexOf(",")  + 1),
				Base64.DEFAULT
		);
		return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
	}

	public static String convertToDB(Bitmap bitmap)
	{
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
		return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);
	}

	public static String convertToDB(byte[] bytes)
	{
		return Base64.encodeToString(bytes, Base64.DEFAULT);
	}
}

