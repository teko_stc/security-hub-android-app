package biz.teko.td.SHUB_APP.DevicesTabs.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputFilter;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Adapter.NRListAdapter;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.DevicesTabs.Activities.DevicesActivity;
import biz.teko.td.SHUB_APP.DevicesTabs.Adapters.DevicesTreeAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardExitSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardRelaySetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardZoneSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerBindActivity;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardDevicesSpinnerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardSTypesSpinnerAdapter;

public class DevicesTabFragment extends Fragment
{
	private Context context;
	private DBHelper dbHelper;
	private int siteId;
	private ExpandableListView expandableListView;
	private DevicesTreeAdapter devicesTreeAdapter;
	private final int previousItem = -1;
	private FrameLayout fabLayout;
	private FrameLayout fabLayout1;
	private FrameLayout fabLayout2;
	private FrameLayout fabLayout3;
	private FrameLayout fabLayout4;
	private FrameLayout fabLayout5;
	private FloatingActionButton fab;
	private FloatingActionButton fab1;
	private FloatingActionButton fab2;
	private FloatingActionButton fab3;
	private FloatingActionButton fab4;
	private FloatingActionButton fab5;
	private TextView fabText1;
	private TextView fabText2;
	private TextView fabText3;
	private TextView fabText4;
	private TextView fabText5;
	private boolean isFABOpen = false;
	private int sending  = 0;
	private D3Service myService;

	private AlertDialog.Builder nrDialogBuilder;
	private AlertDialog nrDialog;
	private NRListAdapter nrListAdapter;
	private ListView nrList;
	private final LinkedList<Event> eventsNRList = new LinkedList<>();
	private int deviceId;


	public static DevicesTabFragment newInstance(int siteId){
		DevicesTabFragment devicesTabFragment = new DevicesTabFragment();
		Bundle b = new Bundle();
		b.putInt("site", siteId);
		devicesTabFragment.setArguments(b);
		return devicesTabFragment;
	}

	private final BroadcastReceiver delegateRevokeResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result > 0){
				Func.pushToast(context, getString(R.string.DELEGATE_REVOKE_SUCCESS), (DevicesActivity) context);
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (DevicesActivity) context);
			}
		}
	};

	private final BroadcastReceiver sitesUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};

	private final BroadcastReceiver affectsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};

	private final BroadcastReceiver revokeReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if( result > 0)
			{
				Func.pushToast(context, getString(R.string.DTA_SUCCESFULLY_UNBINDED), (DevicesActivity) context);
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (DevicesActivity) context);
			}
		}
	};

	private final BroadcastReceiver notReadyReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int eventId = intent.getIntExtra("eventId", -1);
			Event event = dbHelper.getEventById(eventId);
			//checkStates site
			if(null!=event && siteId !=-1){
				Device[] devices = dbHelper.getAllDevicesForSite(siteId);
				if(null!=devices && devices.length != 0){
					int count = 0;
					for(Device device:devices){
						if(event.device == device.id){
							count++;
						}
					}
					if(count > 0){
						eventsNRList.add(event);
						showNotReadyMessage(context);
					}
				}
			}
		}
	};

	private final BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, getActivity().findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private final BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
						} else
						{
							Func.pushToast(context, context.getString(R.string.COMMAND_BEEN_SEND), (Activity) context);
						}
						break;
					case -1:
						Func.pushToast(context, context.getString(R.string.COMMAND_NOT_BEEN_SEND), (Activity) context);
						sending = 0;
						break;
					default:
						sending = 0;
						break;
				}
			}
			updateList();
			if(null!=fab){
				fab.setEnabled(sending == 0);
			}
		}
	};

	private void updateList()
	{
		if(devicesTreeAdapter!=null){
			Cursor sectionsCursor  = getDevicesRefreshedCursor();
			devicesTreeAdapter.update(sectionsCursor, sending);
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		getActivity().registerReceiver(delegateRevokeResultReceiver, new IntentFilter(D3Service.BROADCAST_DELEGATE_REVOKE_RESULT));
		getActivity().registerReceiver(sitesUpdateReceiver, new IntentFilter(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
		getActivity().registerReceiver(affectsReceiver, new IntentFilter(D3Service.BROADCAST_AFFECT));
		getActivity().registerReceiver(notReadyReceiver, new IntentFilter(D3Service.BROADCAST_SITE_NOT_READY));
		getActivity().registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		getActivity().registerReceiver(revokeReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_REVOKE_DOMAIN));
		if(expandableListView!=null){
			if(devicesTreeAdapter!=null){
				updateList();
			}
		}
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(delegateRevokeResultReceiver);
		getActivity().unregisterReceiver(sitesUpdateReceiver);
		getActivity().unregisterReceiver(affectsReceiver);
		getActivity().unregisterReceiver(notReadyReceiver);
		getActivity().unregisterReceiver(commandResultReceiver);
		getActivity().unregisterReceiver(commandSendReceiver);
		getActivity().unregisterReceiver(revokeReceiver);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.tab_site_devices, container, false);
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		siteId = getArguments().getInt("site");
		UserInfo userInfo = dbHelper.getUserInfo();

		expandableListView = (ExpandableListView) v.findViewById(R.id.devicesList);
		expandableListView.setGroupIndicator(null);
		Cursor devicesCursor  = getDevicesRefreshedCursor();

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		boolean showSections = Func.getBooleanSPDefFalse(sp, "pref_show_sections");
		boolean showRelays = Func.getBooleanSPDefTrue(sp, "pref_show_relays");

		if(null!=devicesCursor){
			int expandedLayout = R.layout.card_device_expanded;
			if(showRelays && ((userInfo.roles&Const.Roles.TRINKET)==0)){
				expandedLayout = R.layout.card_device_expanded_relay;
			}
			devicesTreeAdapter = new DevicesTreeAdapter(context, devicesCursor, R.layout.card_device, expandedLayout, R.layout.card_affect, R.layout.card_affect, siteId, showSections);
			expandableListView.setAdapter(devicesTreeAdapter);
		}

		expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

			@Override
			public void onGroupExpand(int groupPosition) {
//				Cursor c = devicesTreeAdapter.getCursor();
//				if(null!=c){
//					if(c.getCount() != 0){
//						c.moveToPosition(groupPosition);
//						final int device_id = c.getInt(1);
//						if(0 != device_id){
//							if(!dbHelper.getDevicePartedStatusForSite(siteId, device_id)){
//								if (0 == dbHelper.getZonesCountForDevice(device_id))
//								{
//									//SHOW ADD DEVICE DIALOG
////									AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
////									adb.setMessage(R.string.WIZ_NO_DEVICES);
////									adb.setPositiveButton(R.string.ADB_ADD, new DialogInterface.OnClickListener()
////									{
////										@Override
////										public void onClick(DialogInterface dialog, int which)
////										{
////											if(dbHelper.getDeviceConnectionStatus(device_id)){
////												if(0 == dbHelper.getSiteDeviceArmStatus(siteId, device_id)){
////													Intent intent = new Intent(getBaseContext(), WizardZoneSetActivity.class);
////													intent.putExtra("site_id", siteId);
////													((Activity) context).startActivity(intent);
////													((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
////												}
////												else
////												{
////													Func.nShowMessage(context, context.getString(R.string.EA_DEVICE_ARMED_MESS_WHEN_USER_ADD_NEW_DETECTOR));
////												}
////											}else{
////												Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
////											}
////										}
////									});
////									adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
////									{
////										@Override
////										public void onClick(DialogInterface dialog, int which)
////										{
////											dialog.dismiss();
////										}
////									});
////									adb.show();
//								}
//							}
//						}
//					}
//				}
//				if(previousItem == -1){
//					expandableListView.setBackgroundColor(getResources().getColor(R.color.brandColorLightPattern));
//				}
//				previousItem = groupPosition;
			}
		});

		fabLayout = (FrameLayout) v.findViewById(R.id.fabLayout);
		fabLayout1 = (FrameLayout) v.findViewById(R.id.fabLayout1);
		fabLayout2 = (FrameLayout) v.findViewById(R.id.fabLayout2);
		fabLayout3 = (FrameLayout) v.findViewById(R.id.fabLayout3);
		fabLayout4 = (FrameLayout) v.findViewById(R.id.fabLayout4);
		fabLayout5 = (FrameLayout) v.findViewById(R.id.fabLayout5);
		fab = (FloatingActionButton) v.findViewById(R.id.fab);
		fab1 = (FloatingActionButton) v.findViewById(R.id.fab1);
		fab2 = (FloatingActionButton) v.findViewById(R.id.fab2);
		fab3 = (FloatingActionButton) v.findViewById(R.id.fab3);
		//		fab3.setEnabled(false);
		fab4 = (FloatingActionButton) v.findViewById(R.id.fab4);
		fab5 = (FloatingActionButton) v.findViewById(R.id.fab5);
		fabText1 = (TextView) v.findViewById(R.id.fabText1);
		fabText2 = (TextView) v.findViewById(R.id.fabText2);
		fabText3 = (TextView) v.findViewById(R.id.fabText3);
		fabText4 = (TextView) v.findViewById(R.id.fabText4);
		fabText5 = (TextView) v.findViewById(R.id.fabText5);

		if(((userInfo.roles& Const.Roles.DOMAIN_ENGIN)==0)&&((userInfo.roles&Const.Roles.ORG_ENGIN)==0)){
			fabLayout.setVisibility(View.GONE);
//			fab.setVisibility(View.GONE);
		}else
		{
			fab.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(!isFABOpen){
						showFABMenu();
					}else{
						closeFABMenu();
					}
				}
			});
			expandableListView.setOnScrollListener(new AbsListView.OnScrollListener()
			{
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState)
				{

				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
				{
					if (0 != firstVisibleItem && fab.isShown())
					{
						fab.hide();
						fab1.hide();
						fab2.hide();
						fab3.hide();
						fab4.hide();
						fab5.hide();
						fabText1.setVisibility(View.INVISIBLE);
						fabText2.setVisibility(View.INVISIBLE);
						fabText3.setVisibility(View.INVISIBLE);
						fabText4.setVisibility(View.INVISIBLE);
						fabText5.setVisibility(View.INVISIBLE);
						fabLayout.setClickable(false);
					}
					if (0 == firstVisibleItem && !fab.isShown())
					{
						fab.show();
						fab1.show();
						fab2.show();
						fab3.show();
						fab4.show();
						fab5.show();
						if(isFABOpen)
							closeFABMenu();
					}
				}
			});
			fab5.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					Intent intent = new Intent(context, WizardControllerBindActivity.class);
					intent.putExtra("site_id", siteId);
					startActivityForResult(intent, D3Service.ADD_NEW_DEVICE);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
			fab4.setOnClickListener(new AddSectionOnClickListener(siteId));
			fab3.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					Intent intent = new Intent(context, WizardZoneSetActivity.class);
					intent.putExtra("site_id", siteId);
					startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
			fab2.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					Intent intent = new Intent(context, WizardRelaySetActivity.class);
					intent.putExtra("site_id", siteId);
					startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
			fab1.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					closeFABMenu();
					Intent intent = new Intent(context, WizardExitSetActivity.class);
					intent.putExtra("site_id", siteId);
					startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
		}

		expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
		{
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
			{
				return false;
			}
		});

		return v;
	}

	public Cursor getDevicesRefreshedCursor()
	{
		return dbHelper.getDevicesCursorBySiteId(siteId);
	}

	public class AddSectionOnClickListener implements View.OnClickListener
	{
		private final int site_id;
		private int type_id;
		private int device_id;

		public AddSectionOnClickListener(int siteId)
		{
			this.site_id = siteId;
		}

		@Override
		public void onClick(View v)
		{
			closeFABMenu();
			final UserInfo userInfo = dbHelper.getUserInfo();
			AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
			adb.setTitle(R.string.SETA_ADD_SECTION_TITLE);
			adb.setMessage(R.string.SETA_ADD_SECTION_MESS);
			adb.setPositiveButton(R.string.ADB_ADD, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{

				}
			});
			adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					Func.hideKeyboard((DevicesActivity) context);
					dialog.dismiss();
				}
			});
			final AlertDialog ad = adb.create();

			final View view = ((DevicesActivity) context).getLayoutInflater().inflate(R.layout.dialog_section_add_big, null);
			final EditText editName = (EditText) view.findViewById(R.id.editName);
			editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
			int editNameMaxLength = 32;
			editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
			editName.setOnKeyListener(new View.OnKeyListener()
			{

				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					// If the event is a key-down event on the "enter" button
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
					{
						// Perform action on Enter key press
						editName.clearFocus();
						Func.hideKeyboard((DevicesActivity) context);
						return true;
					}
					return false;
				}
			});


			Spinner devicesSpinner = (Spinner) view.findViewById(R.id.wizDevicesSpinner);
			Device[] devices  = dbHelper.getAllDevicesForSiteWithRemoteSet(site_id);
			if(null != devices)
			{
				final WizardDevicesSpinnerAdapter devicesSpinnerAdapter = new WizardDevicesSpinnerAdapter(context, R.layout.spinner_item_dropdown, devices);
				devicesSpinner.setAdapter(devicesSpinnerAdapter);
				devicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
				{
					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
					{
						Device device = devicesSpinnerAdapter.getItem(position);
						device_id = device.id;
						setSectionSpinner(device_id);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent)
					{
						Device device = devicesSpinnerAdapter.getItem(0);
						device_id = device.id;
						setSectionSpinner(device_id);
					}


					public void setSectionSpinner(int device_id)
					{

						if(null!=D3Service.d3XProtoConstEvent)
						{

							Spinner sTypeSpinner = (Spinner) view.findViewById(R.id.wizSectionTypeSpinner);
							if (null != sTypeSpinner)
							{
								final LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
								LinkedList<SType> sTypes = new LinkedList<SType>();

								Device device = dbHelper.getDeviceById(device_id);
								if(device.canBeSetFromApp())
								{
									for (SType sType: sTypesList)
									{
										if(null!=sType)
										{
											if (sType.id != 4 && sType.id !=6)
											{
												sTypes.add(sType);
											}
										}
									}
								}else{
									sTypes = sTypesList;
								}
								final WizardSTypesSpinnerAdapter wizardSTypesSpinnerAdapter = new WizardSTypesSpinnerAdapter(context, R.layout.spinner_item_dropdown, sTypes);
								sTypeSpinner.setAdapter(wizardSTypesSpinnerAdapter);
								sTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
								{
									@Override
									public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
									{
										SType sType = wizardSTypesSpinnerAdapter.getItem(position);
										type_id = sType.id;
									}

									@Override
									public void onNothingSelected(AdapterView<?> parent)
									{
										SType sType = wizardSTypesSpinnerAdapter.getItem(0);
										type_id = sType.id;
									}
								});
							}
						}
					}

				});


			}
			else {
				device_id = 0;
			}


			ad.setView(view);
			ad.show();
			ad.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

			ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					String name = editName.getText().toString();
					if (!name.equals(""))
					{
						if(dbHelper.isDeviceOnline(device_id))
						{
							if (0 == dbHelper.getSiteDeviceArmStatus(siteId, device_id))
							{
								if(device_id != 0)
								{
									JSONObject commandAddr = new JSONObject();
									try
									{
										commandAddr.put("type", type_id);
										commandAddr.put("name", name);
										if(sendMessageToServer(dbHelper.getDeviceById(device_id), Command.SECTION_SET, commandAddr,true)){
											ad.dismiss();
										}
									} catch (JSONException e)
									{
										e.printStackTrace();
									}
								}else{
									Func.pushToast(context, getString(R.string.DTA_CHOOSE_DEVICE), (DevicesActivity) context);
								}
							} else
							{
								Func.nShowMessage(context, context.getString(R.string.OBJECT_DISARM_MESSAGE_ZONE_DEL));
							}
						}else{
							Func.nShowMessage(context, context.getString(R.string.DTA_NO_CONNECTION_DEVICE));
						}
					} else
					{
						Func.pushToast(context, getString(R.string.SETA_ENTER_SEC_NUM), (DevicesActivity) context);
					}
				}
			});

		}
	}

	private void showFABMenu(){

		isFABOpen=true;
		setFabInMain();
		fab.animate().rotation(-45);
		fabLayout.setClickable(true);
		fabLayout.setBackgroundColor(getResources().getColor(R.color.brandTransparentWhite50));
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		fabLayout1.animate().translationY(-(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 65, metrics));
		fabLayout2.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 125, metrics));
		fabLayout3.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 185, metrics));
		fabLayout4.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 245, metrics));
		fabLayout5.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 305, metrics));
		fabText1.setVisibility(View.VISIBLE);
		fabText2.setVisibility(View.VISIBLE);
		fabText3.setVisibility(View.VISIBLE);
		fabText4.setVisibility(View.VISIBLE);
		fabText5.setVisibility(View.VISIBLE);
	}

	public void closeFABMenu(){
		isFABOpen=false;
		fab.animate().rotation(0);
		setFabInMain();
		fabText1.setVisibility(View.INVISIBLE);
		fabText2.setVisibility(View.INVISIBLE);
		fabText3.setVisibility(View.INVISIBLE);
		fabText4.setVisibility(View.INVISIBLE);
		fabText5.setVisibility(View.INVISIBLE);
		fabLayout1.animate().translationY(0);
		fabLayout2.animate().translationY(0);
		fabLayout3.animate().translationY(0);
		fabLayout4.animate().translationY(0);
		fabLayout5.animate().translationY(0);
		fabLayout.setClickable(false);
		fabLayout.setBackgroundColor(getResources().getColor(R.color.brandTransparentWhite00));
	}

	private void setFabInMain(){
		Activity activity = getActivity();
		if(activity != null){
			DevicesActivity devicesActivity = (DevicesActivity) activity;
			devicesActivity.setFabStatus(isFABOpen);
		}
	}

	private void showNotReadyMessage(final Context context)
	{
		final LinkedList<Event> eventsList = eventsNRList;
		if((null!=nrDialog)&&(nrDialog.isShowing())){
			//updateAdapter
			if(nrListAdapter != null){
				nrListAdapter.notifyDataSetChanged();
			}
		}else
		{
			nrDialogBuilder = Func.adbForCurrentSDK(context);
			nrDialog = nrDialogBuilder.create();
			nrDialog.setTitle(R.string.TITLE_FAILED);
			nrDialog.setMessage(getString(R.string.MESS_NOT_READY));

			nrList = new ListView(context);

			UserInfo userInfo = dbHelper.getUserInfo();
			if((userInfo.roles&Const.Roles.DOMAIN_HOZ_ORG)!=0)
			{
				nrDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ADB_RETRY), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						UserInfo userInfo = dbHelper.getUserInfo();
						if ((eventsList != null) && (eventsList.size() != 0))
						{
							int deviceId = eventsList.get(0).device;
							sendMessageToServer(dbHelper.getDeviceById(deviceId), Command.ARM, new JSONObject(),true);
							eventsNRList.clear();
						}
					}
				});
			}

			nrDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.ADB_CANCEL), new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
					dialog.dismiss();
					eventsNRList.clear();
				}
			});

			nrListAdapter = new NRListAdapter(context, eventsList);
			nrList.setAdapter(nrListAdapter);
			FrameLayout frameLayout = (FrameLayout) new FrameLayout(context);
			frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
			frameLayout.addView(nrList);

			nrDialog.setView(frameLayout);
			nrDialog.show();
		}
	}

	public boolean sendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		Activity activity = (DevicesActivity)context;
		if(activity != null){
			DevicesActivity devicesActivity = (DevicesActivity) activity;
			return  devicesActivity.nSendMessageToServer(d3Element, Q, data, command);
		}
		return false;
	}
}
