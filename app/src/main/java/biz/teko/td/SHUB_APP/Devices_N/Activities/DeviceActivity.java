package biz.teko.td.SHUB_APP.Devices_N.Activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import org.json.JSONObject;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Activity.BaseInterfaceActivityTemp;
import biz.teko.td.SHUB_APP.Activity.MainNavigationActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Activities.EventsStateActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NAdapters.NDSectionsGridAdapter;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NStartTutorialView;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopLayout;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Utils.Screen;

public class DeviceActivity extends BaseInterfaceActivityTemp
{
	private Context context;
	private DBHelper dbHelper;
	private Device device;
	private boolean transition;

	private final static int COLUMN_COUNT = 2;

	private boolean firstCreate = true;
	private int transition_start;
	private String title;
	private int site_id;
	private int device_id;
	private boolean favorited;
	private MainBar mainBar;
	private NTopLayout topFrame;
	private RecyclerView sectionsList;
	private Cursor cursor;
	//!!
	private NDSectionsGridAdapter gridAdapter;
	private NStartTutorialView tutorial;

	private final int sending = 0;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private UserInfo userInfo;

	private boolean binded;

	private final BroadcastReceiver deviceRmvReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getIntExtra("device", 0) == device_id){
				onBackPressed();
			}
		}
	};

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if (gridAdapter!= null) {
					gridAdapter.setSync(true);
			}
			bind();
		}
	};

	private final BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private final BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_device);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();

		site_id = getIntent().getIntExtra("site", -1);
		title  = getIntent().getStringExtra("bar_title");
		transition = getIntent().getBooleanExtra("transition",false);
		transition_start = getIntent().getIntExtra("transition_start", 0);

		device_id = getIntent().getIntExtra("device", -1);

		addTransitionListener(transition);

		topFrame = (NTopLayout) findViewById(R.id.nTopFrame);
		sectionsList = (RecyclerView) findViewById(R.id.nListZones);

		GridLayoutManager gridLayoutManager = new GridLayoutManager(this, COLUMN_COUNT);
		gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
			@Override
			public int getSpanSize(int position) {
				int type = gridAdapter.getItemViewType(position);
				if (type == NDSectionsGridAdapter.ItemType.SECTION.ordinal()){
					return 2;
				} else if (type == NDSectionsGridAdapter.ItemType.DETECTOR.ordinal()){
					return 1;
				}
				return 1;
			}
		});
		sectionsList.setLayoutManager(gridLayoutManager);


		//		topInfoFrame = (NTopInfoFrame) findViewById(R.id.nTopInfoFrame);
//		controlFrame = (NControlFrame) findViewById(R.id.nControlFrame);


		//bind();
	}
	private void bind()
	{
		device = dbHelper.getDeviceById(device_id);
		if(null!=device)
		{
			mainBar = getRefreshedMainBar();
			if (null != mainBar) favorited = true;
			else favorited = false;
			setTopFrame();
			setGridFrame();
		}else{
			onBackPressed();
		}
		binded = true;
	}

	private void setGridFrame()
	{
		if(null!= sectionsList){
			updateGrid();
		}
	}

	private void updateGrid()
	{
		updateCursor();
		if(null!=cursor && null==gridAdapter) {
			gridAdapter = new NDSectionsGridAdapter(context, cursor);
			sectionsList.setAdapter(gridAdapter);
		}else{
			gridAdapter.update(cursor);
		}
		//Func.setDynamicHeight(sectionsList);
	}

	private void updateCursor()
	{
		cursor = dbHelper.getSectionsCursorForDevice(device.id);
	}

	private void setTopFrame()
	{
		if(null!=topFrame){
			topFrame.setNormal(true);
			topFrame.setType(NTopLayout.Type.device_common);
			DeviceType deviceType = device.getType();
			if(null!=deviceType){
				topFrame.setBigImage(deviceType.getBigIcon(context)); /*TODO*/
				switch (deviceType.id){
					case Const.DEVICETYPE_SH:
					case Const.DEVICETYPE_SH_4G:
						//					case Const.DEVICETYPE_SH_4G:
						topFrame.setType(NTopLayout.Type.device_sh);
						break;
					case Const.DEVICETYPE_SH_2:
//					case Const.DEVICETYPE_SH_4G:
						topFrame.setType(NTopLayout.Type.device_sh_2);
						break;
					case  Const.DEVICETYPE_MB:
						topFrame.setType(NTopLayout.Type.device_mb);
						break;
					default:
						break;
				}
			}
			topFrame.setFavorited(favorited);
			topFrame.setTransition(this, transition, transition_start);
			topFrame.setTitle(null!=title? title:device.getName());
			topFrame.setArmed(dbHelper.getDeviceArmStatus(device.id));
			topFrame.setOnActionListener(new NTopLayout.OnActionListener()
			{
				@Override
				public void onFavoriteChanged()
				{
					if(!favorited){
						addOnMain();
					}else{
						if(null!=mainBar){
							if(dbHelper.deleteMainBar(mainBar)) {
								favorited = false;
								topFrame.setFavorited(favorited);
							}
						}
					}
				}

				@Override
				public void backPressed()
				{
					onBackPressed();
				}

				@Override
				public void onSettingsPressed()
				{
					Intent intent = new Intent(context, DeviceSettingsActivity.class);
					intent.putExtra("site", site_id);
					intent.putExtra("device", device.id);
					startActivityForResult(intent, Const.REQUEST_DELETE);
					overridePendingTransition(R.animator.enter, R.animator.exit);
				}

				@Override
				public void onControlPressed(int value)
				{
					switch (value){
						case NActionButton.VALUE_ARM:
							nSendMessageToServer(device, Command.ARM, new JSONObject(), true);
							break;
						case NActionButton.VALUE_DISARM:
							nSendMessageToServer(device, Command.DISARM, new JSONObject(), true);
							break;
					}
				}

				@Override
				public void onLinkPressed(int id)
				{
					/*for scripted*/
				}

				@Override
				public void onStateClick(int type)
				{
					switch (type)
					{
						case Const.STATE_TYPE_UPDATE:
							showUpdateDialog();
							break;
						case Const.STATE_TYPE_DELAY_ARM:
							Func.nShowMessage(context, getString(R.string.N_STATE_DELAY_ARM_MESSAGE_DEVICE));
							break;
						default:
							Intent intent = new Intent(context, EventsStateActivity.class);
							intent.putExtra("type", type);
							intent.putExtra("zone", 0);
							intent.putExtra("section", 0);
							intent.putExtra("device", device.id);
							startActivity(intent);
							break;
					}
				}
			});

			LinkedList<Event> affects;
			if(null!=deviceType){
				switch (deviceType.id){
					case Const.DEVICETYPE_SH:
					case Const.DEVICETYPE_SH_1:
					case Const.DEVICETYPE_SH_1_1:
					case Const.DEVICETYPE_SH_2:
					case Const.DEVICETYPE_SH_4G:
						affects = dbHelper.getAffectsByDeviceId(device.id);
						break;
					default:
						affects = dbHelper.getExtAffectsByDeviceId(device.id);
						break;
				}
			}else{
				affects = dbHelper.getAffectsByDeviceId(device.id);
			}
//			if(null!=affects)
				topFrame.setStates(affects, device, false);

			topFrame.bindView();
		}
	}

	private void showUpdateDialog()
	{
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
		nDialog.setTitle(getString(R.string.N_DEVICE_UPDATE_READY_QUESTION));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value){
				case NActionButton.VALUE_OK:
					nSendMessageToServer(device, Command.UPDATE_APPLY, new JSONObject(), true);
					nDialog.dismiss();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}
		});
		nDialog.show();
	}

	private void addOnMain() {
		boolean shared = PrefUtils.getInstance(context).getSharedMainScreenMode();

		int site_id = this.site_id;
		if (shared)
			site_id = dbHelper.getSiteByDeviceId(device_id).id;
		Cursor cursor = dbHelper.getMainBars(site_id, shared);


		if (null == cursor || 0 == cursor.getCount()) {
			dbHelper.fillBars(site_id, shared);
		}
		MainBar mainBar = dbHelper.getEmptyBar(site_id, shared);
		if (null != mainBar) {
			mainBar.type = MainBar.Type.ARM;
			mainBar.subtype = MainBar.SubType.DEVICE;
			mainBar.element_id = Integer.toString(device.id);

			if (dbHelper.addNewBar(mainBar, shared)) {
				favorited = true;
				topFrame.setFavorited(favorited);
				this.mainBar = getRefreshedMainBar();
			} else {
				Func.pushToast(context, "DB ERROR", (DeviceActivity) context);
			}
		} else if (!shared) {
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
			nDialog.setTitle(getString(R.string.N_ERROR_NO_SPACE_ON_MAIN));
			nDialog.setOnActionClickListener(new NDialog.OnActionClickListener() {
				@Override
				public void onActionClick(int value, boolean b) {
					switch (value) {
						case NActionButton.VALUE_CANCEL:
							nDialog.dismiss();
							break;
					}
				}
			});
			nDialog.show();
		}


	}

	private MainBar getRefreshedMainBar()
	{
		CollapsingToolbarLayout coll_toolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout);
		coll_toolbar.setTitle(device.getName());
		return dbHelper.getMainBarForDevice(site_id, device);
	}


	public void  onResume(){
		super.onResume();
		if (gridAdapter != null)
			gridAdapter.setSync(!firstCreate);
		firstCreate = false;
		if(!binded) {
			bind();
		}
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		registerReceiver(updateReceiver, intentFilter);
		registerReceiver(deviceRmvReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_RMV));
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		bindD3();
		setupTutorial();
	}

	private void setupTutorial() {
		if (tutorial == null && PrefUtils.getInstance(context).getStartTutorial()) {
			tutorial = new NStartTutorialView(context, this, 8);
			tutorial.setNextActivityListener(() -> {
				Intent intent = new Intent(this, MainNavigationActivity.class);
				intent.putExtra(Const.SCREEN_NAME, Screen.DEVICES);
				intent.putExtra(Const.IS_FIRST_TUTORIAL, false);
				startActivity(intent);
				finish();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			});
		}
	}

	public void onPause(){
		super.onPause();
		binded = false;
		unregisterReceiver(updateReceiver);
		unregisterReceiver(deviceRmvReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	public boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				finish();
			}
		}
	}

	@Override
	public void nrAction(int id)
	{
		nSendMessageToServer(device, Command.ARM, new JSONObject(), true);
	}

	@Override
	public void onBackPressed() {
		if (tutorial != null) {
			showFinishTutorialDialog();
		} else {
			super.onBackPressed();
		}
	}

	private void showFinishTutorialDialog() {
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.TUTORIAL_FINISH_QUESTION));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value){
				case NActionButton.VALUE_OK:
					finishTutorial();
					nDialog.dismiss();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}

		});
		nDialog.show();
	}

	private void finishTutorial() {
		tutorial.finishTutorial();
		tutorial = null;
	}
}
