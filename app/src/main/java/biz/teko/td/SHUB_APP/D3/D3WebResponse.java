package biz.teko.td.SHUB_APP.D3;

import android.util.Log;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 27.01.2017.
 */
public class D3WebResponse
{
	public String data;

	public D3WebResponse(String data){
		this.data = data;
	}

	public int getLoginResult(){
		String[] lines = this.data.split(System.getProperty("line.separator"));
		String xstring = "X-Result";
		int response = 0;
		for(String line:lines){
			if((line.length()>9)){
				String s = line.substring(0, 8);
				if(xstring.equals(s))
				{
					Func.log_d("D3ServiceLog", "WebResponseResult: " + line);
					String string_response = line.substring(line.indexOf(":")+2, line.indexOf("\r"));
					return Integer.parseInt(string_response);
				}
			}
		}
		return response;
	}

	public int getProtocolVersion(){
		String[] lines = this.data.split(System.getProperty("line.separator"));
		String xstring = "X-Version";
		int version = 0;
		for(String line:lines){
			if((line.length()>11)){
				String s = line.substring(0, 9);
				if(xstring.equals(s))
				{
					Log.e("D3WebResponse", line);
					String string_response = line.substring(line.indexOf(":")+2, line.indexOf("\r"));
					return Integer.parseInt(string_response);
				}
			}
		}
		return version;
	}
}
