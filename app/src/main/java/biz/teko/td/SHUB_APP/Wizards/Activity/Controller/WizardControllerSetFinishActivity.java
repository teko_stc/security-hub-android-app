package biz.teko.td.SHUB_APP.Wizards.Activity.Controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.MainNavigationActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardZoneSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.DomainUsers.WizardDomainUserSetActivity;

public class WizardControllerSetFinishActivity extends BaseActivity
{
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.wizard_activity_controller_set_finish);
		this.context = getApplicationContext();

		final int site_id = getIntent().getIntExtra("site_id", -1);

		final ImageButton userSetupButton = (ImageButton) findViewById(R.id.wizUserSetupButton);
		userSetupButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent activityIntent = new Intent(getBaseContext(), WizardDomainUserSetActivity.class);
				startActivityForResult(activityIntent, Const.ADD_DOMAIN_USER);
			}
		});

		final ImageButton deviceSetupButton = (ImageButton) findViewById(R.id.wizDeviceSetupButton);
		deviceSetupButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent activityIntent = new Intent(getBaseContext(), WizardZoneSetActivity.class);
				activityIntent.putExtra("site_id", site_id);
				finish();
				startActivity(activityIntent);

				overridePendingTransition(R.animator.enter, R.animator.exit);
			}
		});

		final ImageButton addMoreDeviceButton = (ImageButton) findViewById(R.id.wizContrBindDeviceButton);
		addMoreDeviceButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent activityIntent = new Intent(getBaseContext(), WizardControllerBindActivity.class);
				activityIntent.putExtra("site_id", site_id);
				finish();
				startActivity(activityIntent);
				overridePendingTransition(R.animator.enter, R.animator.exit);
			}
		});

		final ImageButton contrSetupEndButton = (ImageButton) findViewById(R.id.wizContrSetupEndButton);
		contrSetupEndButton.setOnClickListener(v -> {
			Intent activityIntent = new Intent(getBaseContext(), MainNavigationActivity.class);
			finish();
			startActivity(activityIntent);
//				overridePendingTransition(R.animator.enter, R.animator.exit);
		});
	}
}
