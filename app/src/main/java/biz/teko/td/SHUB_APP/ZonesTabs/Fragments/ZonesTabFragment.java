package biz.teko.td.SHUB_APP.ZonesTabs.Fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardExitSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardRelaySetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardZoneSetActivity;
import biz.teko.td.SHUB_APP.ZonesTabs.Adapters.ZonesTreeAdapter;
import biz.teko.td.SHUB_APP.ZonesTabs.ZonesActivity;

/**
 * Created by td13017 on 10.02.2017.
 */

public class ZonesTabFragment extends Fragment
{
	private DBHelper dbHelper;
	private Context context;
	private int siteId;
	private int deviceId;
	private int sectionId;
	private int previousItem = -1;
	private ZonesActivity activity;
	private ZonesTreeAdapter zonesTreeAdapter;
	private D3Service myService;
	private FloatingActionButton fab;
	private FloatingActionButton fab1;
	private FloatingActionButton fab2;
	private int sending = 0;
	private Cursor zonesCursor;
	private int zoneOrRelay = 0;
	private boolean isFABOpen = false;
	private FrameLayout fabLayout;
	private FrameLayout fabLayout1;
	private FrameLayout fabLayout2;
	private TextView fabText1;
	private TextView fabText2;
	private ExpandableListView expandableListView;

	public static Fragment newInstance(int siteId, int deviceId, int sectionId, int zoneRelay)
	{
		ZonesTabFragment zonesTabFragment = new ZonesTabFragment();
		Bundle b = new Bundle();
		b.putInt("site", siteId);
		b.putInt("device", deviceId);
		b.putInt("section", sectionId);
		b.putInt("version", zoneRelay);
		zonesTabFragment.setArguments(b);
		return zonesTabFragment;
	}

	private BroadcastReceiver libraryScriptsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(null!=zonesTreeAdapter){
				zonesTreeAdapter.goToScriptSet();
			}

		}
	};

	private BroadcastReceiver updateListReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};

	private BroadcastReceiver affectsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};

	private void updateList()
	{
		if(zonesTreeAdapter!=null){
			Cursor refreshedCursor = refreshCursor();
			zonesTreeAdapter.update(refreshedCursor, sending);
			if(null!=expandableListView){
				expandList();
			}
		}
	}

	private BroadcastReceiver zoneDeleteReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", -1);
			if(result == 1 )
			{
				Func.pushToast(context, getString(R.string.ZONE_DELETE_SUCCESS), (Activity) context);
			}else{
				Func.showCommandResultSnackbar(context, getActivity().findViewById(android.R.id.content), intent.getIntExtra("result", -1));
			}
		}
	};

	private BroadcastReceiver sitesUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
						} else
						{
							Func.pushToast(context, context.getString(R.string.COMMAND_BEEN_SEND), (Activity) context);
						}
						break;
					case -1:
						Func.pushToast(context, context.getString(R.string.COMMAND_NOT_BEEN_SEND), (Activity) context);
						sending = 0;
						break;
					default:
						sending = 0;
						break;
				}
			}
			if(null!=fab){
				if(sending == 0){
					fab.setEnabled(true);
				}else{
					fab.setEnabled(false);
				}
			}
			updateList();
		}
	};

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, getActivity().findViewById(android.R.id.content), intent.getIntExtra("result", -1));

		}
	};

	@Override
	public void onResume()
	{
		super.onResume();
		updateList();
		getActivity().registerReceiver(affectsReceiver, new IntentFilter(D3Service.BROADCAST_AFFECT));
		getActivity().registerReceiver(sitesUpdateReceiver, new IntentFilter(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
		getActivity().registerReceiver(zoneDeleteReceiver, new IntentFilter(D3Service.BROADCAST_ZONE_DEL));
		getActivity().registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		getActivity().registerReceiver(updateListReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV));
		getActivity().registerReceiver(libraryScriptsReceiver, new IntentFilter(D3Service.BROADCAST_LIBRARY_SCRIPTS));

	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(affectsReceiver);
		getActivity().unregisterReceiver(sitesUpdateReceiver);
		getActivity().unregisterReceiver(zoneDeleteReceiver);
		getActivity().unregisterReceiver(commandResultReceiver);
		getActivity().unregisterReceiver(commandSendReceiver);
		getActivity().unregisterReceiver(updateListReceiver);
		getActivity().unregisterReceiver(libraryScriptsReceiver);
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.tab_section_zones, container, false);
		context = container.getContext();
		activity = (ZonesActivity) context;
		dbHelper = DBHelper.getInstance(context);
		siteId = getArguments().getInt("site");
		deviceId = getArguments().getInt("device");
		sectionId = getArguments().getInt("section");
		zoneOrRelay = getArguments().getInt("version");
		UserInfo userInfo = dbHelper.getUserInfo();

		expandableListView = (ExpandableListView) v.findViewById(R.id.zonesList);
		expandableListView.setGroupIndicator(null);
		Cursor refreshedCursor = refreshCursor();
		zonesTreeAdapter = new ZonesTreeAdapter(context, refreshedCursor, R.layout.card_zone, R.layout.card_zone_expanded, R.layout.card_affect_zone, R.layout.card_affect_zone, siteId, deviceId, sectionId);
		expandableListView.setAdapter(zonesTreeAdapter);

		expandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

			@Override
			public void onGroupExpand(int groupPosition) {
				if(previousItem == -1){
					expandableListView.setBackgroundColor(getResources().getColor(R.color.brandColorLightPattern));
				}
				previousItem = groupPosition;
			}
		});

		expandList();

		expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener()
		{
			@Override
			public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l)
			{
				return true;
			}
		});

		fab = (FloatingActionButton) v.findViewById(R.id.fab);
		fab1 = (FloatingActionButton) v.findViewById(R.id.fab1);
		fab2 = (FloatingActionButton) v.findViewById(R.id.fab2);

		fabLayout = (FrameLayout) v.findViewById(R.id.fabLayout);
		fabLayout1 = (FrameLayout) v.findViewById(R.id.fabLayout1);
		fabLayout2 = (FrameLayout) v.findViewById(R.id.fabLayout2);

		fabText1 = (TextView) v.findViewById(R.id.fabText1);
		fabText2 = (TextView) v.findViewById(R.id.fabText2);

		fab.setOnClickListener(zoneOrRelay == 0? new AddZoneOnClickListener(): new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(!isFABOpen){
					showFABMenu();
				}else{
					closeFABMenu();
				}
			}
		});

		fab2.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				closeFABMenu();
				if (dbHelper.isDeviceOnline(deviceId))
				{
					if(0 == dbHelper.getSiteDeviceArmStatus(siteId, deviceId))
					{
						Intent intent = new Intent(context, WizardRelaySetActivity.class);
						intent.putExtra("site_id", siteId);
						intent.putExtra("device_id", deviceId);
						startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}else{
						Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_ZONE_ADD));
					}
				} else
				{
					Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
				}
			}
		});
		fab1.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				closeFABMenu();
				if (dbHelper.isDeviceOnline(deviceId))
				{
					if(0 == dbHelper.getSiteDeviceArmStatus(siteId, deviceId))
					{
						Intent intent = new Intent(context, WizardExitSetActivity.class);
						intent.putExtra("site_id", siteId);
						intent.putExtra("device_id", deviceId);
						startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
					}else{
						Func.nShowMessage(context, context.getString(R.string.DEVICE_DISARM_MESSAGE_ZONE_ADD));
					}
				} else
				{
					Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
				}
			}
		});

		if(deviceId != -1 ?  (!Func.getRemoteSetRights(userInfo, dbHelper, siteId, deviceId)) : (((userInfo.roles& Const.Roles.DOMAIN_ENGIN)==0)&&((userInfo.roles&Const.Roles.ORG_ENGIN)==0))){
//			fab.setVisibility(View.GONE);
//			fab1.setVisibility(View.GONE);
//			fab2.setVisibility(View.GONE);
		}else
		{
			expandableListView.setOnScrollListener(new AbsListView.OnScrollListener()
			{
				@Override
				public void onScrollStateChanged(AbsListView view, int scrollState)
				{
				}

				@Override
				public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
				{
					if (0 != firstVisibleItem && fab.isShown())
					{
						fab.hide();
						fab1.hide();
						fab2.hide();
					}
					if (0 == firstVisibleItem && !fab.isShown())
					{
						fab.show();
						fab1.show();
						fab2.show();
					}
				}
			});
		}


		return v;
	}

	private void expandList()
	{
		//set zones list alwas expanded
		for(int i=0; i < expandableListView.getAdapter().getCount(); i ++){
			expandableListView.expandGroup(i);
		}
	}

	private void setFabInMain(){
		Activity activity = getActivity();
		if(activity != null){
			ZonesActivity zonesActivity = (ZonesActivity) activity;
			zonesActivity.setFabStatus(isFABOpen);
		}
	}

	private void closeFABMenu()
	{
		isFABOpen=false;
		fab.animate().rotation(0);
		setFabInMain();
		fabText1.setVisibility(View.INVISIBLE);
		fabText2.setVisibility(View.INVISIBLE);
		fabLayout1.animate().translationY(0);
		fabLayout2.animate().translationY(0);
		fabLayout.setClickable(false);
		fabLayout.setBackgroundColor(getResources().getColor(R.color.brandTransparentWhite00));
	}

	private void showFABMenu()
	{
		isFABOpen=true;
		setFabInMain();
		fab.animate().rotation(-45);
		fabLayout.setClickable(true);
		fabLayout.setBackgroundColor(getResources().getColor(R.color.brandTransparentWhite50));
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		fabLayout1.animate().translationY(-(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 65, metrics));
		fabLayout2.animate().translationY(-(int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 125, metrics));
		fabText1.setVisibility(View.VISIBLE);
		fabText2.setVisibility(View.VISIBLE);
	}

	public Cursor refreshCursor(){
		Cursor refreshedCursor;
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		boolean showRelays = Func.getBooleanSPDefTrue(sp, "pref_show_relays");
		if(!showRelays){
			zoneOrRelay = 2;
		}
		if(deviceId != -1)
		{
			if (sectionId != -1)
			{
				refreshedCursor = dbHelper.getAllZonesCursorForSiteDeviceSection(siteId, deviceId, sectionId, zoneOrRelay);
			} else
			{
				refreshedCursor = dbHelper.getAllZonesCursorForSiteDevice(siteId, deviceId, zoneOrRelay);
			}
		}else{
			refreshedCursor = dbHelper.getAllZonesCursorForSite(siteId, zoneOrRelay);
		}
		return refreshedCursor;
	}

	private class AddZoneOnClickListener implements View.OnClickListener
	{
		@Override
		public void onClick(View v)
		{
			if(dbHelper.isDeviceOnline(deviceId))
			{
				if (0 == dbHelper.getSiteDeviceArmStatus(siteId,deviceId))
				{

					Intent intent = new Intent(context, zoneOrRelay == 0 ? WizardZoneSetActivity.class : WizardRelaySetActivity.class);
					intent.putExtra("site_id", siteId);
					intent.putExtra("device_id", deviceId);
					((Activity) context).startActivity(intent);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				} else
				{
					Func.nShowMessage(context, context.getString(R.string.OBJECT_DISARM_MESSAGE_ZONE_ADD));
				}
			}else{
				Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
			}
		}
	}

	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

}
