package biz.teko.td.SHUB_APP.Cameras.Activities.IV;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.Adapters.NCameraBindedAdapter;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 09.04.2018.
 */

public class NCameraBindListActivity extends BaseActivity
{
	private static final int FROM_BIND_LIST = 2;
	private static final int FROM_CAMERA = 0;
	private NCameraBindListActivity context;
	private DBHelper dbHelper;
	private Camera camera;
	private String id;
	private NCameraBindedAdapter NCameraBindedAdapter;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private UserInfo userInfo;
	private int from = 0;

	private BroadcastReceiver receiveDelRelations = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result > 0){
				Func.pushToast(context, getString(R.string.CAMERA_UNBIND_SUCCESS_MESS), (NCameraBindListActivity) context);
				updateBindingList();
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (NCameraBindListActivity) context);
			}
		}
	};

	private BroadcastReceiver camerasListRelationsReceiver= new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateBindingList();
		}
	};
	private LinearLayout back;
	private LinearLayout add;
	private TextView title;
	private TextView empty;
	private ListView list;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera_bind_list);

		this.context = this;
		dbHelper = DBHelper.getInstance(context);

		id = getIntent().getStringExtra("cameraId");
		camera = dbHelper.getIVCameraById(id);
		userInfo = dbHelper.getUserInfo();

		setup();
		bind();
	}

	private void setup()
	{
		back = (LinearLayout) findViewById(R.id.nButtonClose);
		add = (LinearLayout) findViewById(R.id.nButtonAdd);
		title = (TextView) findViewById(R.id.nTextTitle);
		list = (ListView) findViewById(R.id.cameraBindList);
		empty = (TextView) findViewById(R.id.notBindedText);
	}

	private void bind()
	{
		if(null!=back)back.setOnClickListener((v)->{
			onBackPressed();
		});
		if(null!=add){
			add.setOnClickListener((v -> {
				if((userInfo.roles&Const.Roles.DOMAIN_ENGIN)!=0) {
					Intent intent = new Intent(getBaseContext(), NCameraBindActivity.class);
					intent.putExtra("cameraId", camera.id);
					intent.putExtra(Const.OPEN_IV_KEY_FROM, Const.OPEN_IV_FROM_SETTINGS);
					startActivity(intent);
					((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
				}else{
					Func.nShowMessage(context, getResources().getString(R.string.ERROR_NO_RULES));
				}
			}));
		}
		if(null!=title)title.setText(getResources().getString(R.string.CAMEA_TITLE_BIND_LIST));
		updateBindingList();
	}


	private void updateBindingList()
	{

		Cursor c = getUpdatedCursor();
		if((c!=null)&&(c.getCount()!= 0))
		{
			empty.setVisibility(View.GONE);
			list.setVisibility(View.VISIBLE);
			if(null== NCameraBindedAdapter)
			{
				NCameraBindedAdapter = new NCameraBindedAdapter(context, R.layout.n_card_camera_bindings_list_element, c, 0);
				list.setAdapter(NCameraBindedAdapter);
			}else{
				NCameraBindedAdapter.update(c);
			}
		}else{
			empty.setVisibility(View.VISIBLE);
			list.setVisibility(View.GONE);
			/*NO CAMERAS*/
		}
	}


	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(camerasListRelationsReceiver, new IntentFilter(D3Service.BROADCAST_IV_GET_RELATIONS));
		registerReceiver(receiveDelRelations, new IntentFilter(D3Service.BROADCAST_IV_DEL_RELATION));

		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
		bindService(intent, serviceConnection, 0);
		updateBindingList();
	}

	public Cursor getUpdatedCursor(){
		return dbHelper.getZonesCursorByIVCameraId(camera.id);

	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(camerasListRelationsReceiver);
		unregisterReceiver(receiveDelRelations);
	}
}
