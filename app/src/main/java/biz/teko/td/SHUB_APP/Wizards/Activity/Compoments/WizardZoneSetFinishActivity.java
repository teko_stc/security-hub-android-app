package biz.teko.td.SHUB_APP.Wizards.Activity.Compoments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 23.06.2017.
 */

public class WizardZoneSetFinishActivity extends BaseActivity
{
	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_zone_setup_finish);

		final int siteId = getIntent().getIntExtra("site", 0);
		final int sectionId = getIntent().getIntExtra("section", 0);
		final int deviceId = getIntent().getIntExtra("device", 0);
		final int con_type = getIntent().getIntExtra("con_type", 0);
		final int wizard = getIntent().getIntExtra("wizard", 2);

		TextView textSuccess = (TextView) findViewById(R.id.wizDeviceSetupFinishText);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		switch (wizard){
			case 0:
				toolbar.setTitle(R.string.TITLE_EXIT_SET);
				textSuccess.setText(R.string.WIZ_EXIT_SET_SUCCESS);
				break;
			case 1:
				toolbar.setTitle(R.string.TITLE_RELAY_ADD);
				textSuccess.setText(R.string.WIZ_RELAY_SET_SUCCESS);
				break;
			case 2:
				toolbar.setTitle(R.string.WIZ_DEVICE_ADD_TITLE);
				break;
		}

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_x_white_32);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				goBack(sectionId);
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});


		Button finishButton = (Button) findViewById(R.id.wizDeviceSetupFinishButton);
		Button repeatButton = (Button) findViewById(R.id.wizDeviceReSetupButton);

		switch (wizard){
			case 0:
				if(sectionId != 0 )
				{
					textSuccess.setText(getString(R.string.MESS_SET_EXIT_SUCCESS) + " " + Integer.toString(sectionId));
				}
				repeatButton.setText(R.string.MESS_SET_ONE_MORE_EXIT);
				repeatButton.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if(0!=deviceId)
						{
							finish();
							Intent activityIntent = new Intent(getBaseContext(), WizardExitSetActivity.class);
							activityIntent.putExtra("device_id", deviceId);
							activityIntent.putExtra("con_type", con_type);
							startActivity(activityIntent);
							overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
						}
					}
				});
				break;
			case 1:
				if(sectionId != 0 )
				{
					textSuccess.setText(getString(R.string.MESS_SET_RELAY_SUCCESS) + " " + Integer.toString(sectionId));
				}
				repeatButton.setText(R.string.MESS_SET_ONE_MORE_RELAY);
				repeatButton.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if(0!=deviceId)
						{
							finish();
							Intent activityIntent = new Intent(getBaseContext(), WizardRelaySetActivity.class);
							activityIntent.putExtra("device_id", deviceId);
							activityIntent.putExtra("con_type", con_type);
							startActivity(activityIntent);
							overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
						}
					}
				});
				break;
			case 2:
				if(sectionId != 0 )
				{
					textSuccess.setText(textSuccess.getText() + "\n" + getString(R.string.WIZ_DEVICE_ADDED_IN_SECTION_MESS_1) + " " + Integer.toString(sectionId));
				}
				repeatButton.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						if(0!=deviceId)
						{
							finish();
							Intent activityIntent = new Intent(getBaseContext(), WizardZoneSetActivity.class);
							activityIntent.putExtra("device_id", deviceId);
							activityIntent.putExtra("con_type", con_type);
							startActivity(activityIntent);
							overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
						}
					}
				});
				break;
		}

		finishButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				goBack(sectionId);
			}
		});
	}

	private void goBack(int sectionId)
	{
		Intent intent = new Intent();
		intent.putExtra("section", sectionId);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void onBackPressed()  {}
}
