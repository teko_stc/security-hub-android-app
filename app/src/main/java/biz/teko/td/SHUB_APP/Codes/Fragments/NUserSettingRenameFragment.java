//package biz.teko.td.SHUB_APP.Codes.Fragments;
//
//import android.app.Activity;
//import android.os.Bundle;
//import android.text.InputType;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.LinearLayout;
//import android.widget.TextView;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import biz.teko.td.SHUB_APP.Codes.Activities.NUserSettingActivity;
//import biz.teko.td.SHUB_APP.Codes.Activities.UsersActivity;
//import biz.teko.td.SHUB_APP.D3.D3Request;
//import biz.teko.td.SHUB_APP.R;
//import biz.teko.td.SHUB_APP.Utils.Dir.Func;
//import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
//
//public class NUserSettingRenameFragment extends Fragment {
//    private LinearLayout buttonSetName, buttonBack;
//    private TextView textName;
//    private NEditText editName;
//    private NUserSettingActivity activity;
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.n_fragment_user_setting_rename, container, false);
//        setupView(view);
//        return view;
//    }
//
//    @Override
//    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        activity = (NUserSettingActivity) getActivity();
//        setClickListeners();
//        editName.setText(activity.getUser().name);
//    }
//
//    private void setClickListeners() {
//        buttonBack.setOnClickListener(view -> {
//            Func.hideKeyboard(activity);
//            onBackPressed();
//        });
//        buttonSetName.setOnClickListener(v -> {
//            rename();
//            Func.hideKeyboard(activity);
//        });
//    }
//
//    private void rename() {
//        if (!editName.getText().toString().matches("")) {
//            String newName = editName.getText().toString();
//            if (!newName.equals(activity.getUser().name)) {
//                JSONObject commandAddress = new JSONObject();
//                try {
//                    commandAddress.put("index", activity.getUser().id);
//                    commandAddress.put("name", newName);
//                    int command_count = Func.getCommandCount(activity);
//                    D3Request d3Request = D3Request.createCommand(activity.getUserInfo().roles, activity.getUser().device, "USER_SET", commandAddress, command_count);
//                    if (d3Request.error == null) {
//                        if (activity.sendCommandToServer(d3Request))
//                            activity.sendIntentCommandBeenSend(1);
//                    } else
//                        Func.pushToast(activity, d3Request.error, activity);
//                    Func.hideEditTextKeyboard(editName);
//                    activity.removeFragment(this);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        } else
//            Func.pushToast(activity, activity.getString(R.string.DELA_INCORRECT_NAME_PUSHTOAST), activity);
//    }
//
//    private void onBackPressed() {
//        activity.removeFragment(this);
//    }
//
//    private void setupView(View view) {
//        buttonSetName = view.findViewById(R.id.nButtonChange);
//        buttonBack = view.findViewById(R.id.nButtonBack);
//        editName = view.findViewById(R.id.nText);
//        textName = view.findViewById(R.id.nTextTitle);
//        editName.showKeyboard();
//        editName.setCursorVisible(true);
//        editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//    }
//}
