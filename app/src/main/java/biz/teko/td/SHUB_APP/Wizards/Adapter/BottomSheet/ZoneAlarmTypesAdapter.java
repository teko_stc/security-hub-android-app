package biz.teko.td.SHUB_APP.Wizards.Adapter.BottomSheet;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Models.ZoneAlarm;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;

public class ZoneAlarmTypesAdapter extends ArrayAdapter<ZoneAlarm>
{
	private final ZoneAlarm[] zoneAlarms;
	private final LayoutInflater layoutInflater;
	private final Context context;
	private OnItemClickListener onItemClickListener;

	public void setOnItemClickListener(OnItemClickListener onItemClickListener){
		this.onItemClickListener = onItemClickListener;
	}

	public interface OnItemClickListener{
		void onClick(ZoneAlarm zoneAlarm);
	}


	public ZoneAlarmTypesAdapter(@NonNull Context context, int resource, ZoneAlarm[] zoneAlarms)
	{
		super(context, resource);
		this.context = context;
		this.zoneAlarms = zoneAlarms;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_alarm_types_list_element, parent, false);
		}
		ZoneAlarm zoneAlarm = zoneAlarms[position];
		if(null!=zoneAlarm)
		{
			NWithAButtonElement nWithAButtonElement = view.findViewById(R.id.nDevicesListElement);
			if(null!=nWithAButtonElement)
			{
				nWithAButtonElement.setTitle(zoneAlarm.caption);
				nWithAButtonElement.setSubtitle(null);
				nWithAButtonElement.setDescImage(context.getResources().getDrawable(zoneAlarm.getListIcon(context)));
				nWithAButtonElement.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						if(null!=onItemClickListener) onItemClickListener.onClick(zoneAlarm);
					}
				});
			}
		}
		return view;
	}

	public int getCount() {
		return zoneAlarms.length;
	}

	public ZoneAlarm getItem(int position){
		return zoneAlarms[position];
	}

	public long getItemId(int position){
		return position;
	}
}
