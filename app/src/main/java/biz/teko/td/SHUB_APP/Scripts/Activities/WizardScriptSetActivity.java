package biz.teko.td.SHUB_APP.Scripts.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Adapters.ScriptSetPagerAdapter;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;


public class WizardScriptSetActivity extends BaseActivity
{
	private ScriptSetPagerAdapter adapter;
	private ViewPager pager;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private int libScriptId;
	private Context context;
	private DBHelper dbHelper;
//	private Script script;
	private int pagerCount;
	private int deviceId;
	private final int sending = 0;

	private int selBind = -1;

	private HashMap<String, JSONArray> newParams;

	private HashMap[] paramArrays;
	private int siteId;
	private int scriptId;
	private SharedPreferences sp;
	private String iso;
	private Target target;
	private Script fullScript;
	private boolean anim = true;

	public Target getTarget()
	{
		return target;
	}

	public enum Target
	{
		SET,
		EDIT
	}

	private final BroadcastReceiver scripDelReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getIntExtra("script_id", -1) == scriptId){
				target = Target.SET;
				goNext();
				Handler handler = new Handler();
				handler.removeCallbacksAndMessages(null);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_script_set);
		context = this;
		dbHelper = DBHelper.getInstance(context);

		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));

		scriptId = getIntent().getIntExtra("script_id", 0);
		libScriptId = getIntent().getIntExtra("lib_script_id", 0);
		deviceId = getIntent().getIntExtra("device_id", -1);
		siteId = getIntent().getIntExtra("site_id",-1);
		selBind = getIntent().getIntExtra("bind",-1);
		target = getIntent().getIntExtra("target", 0) == 0 ? Target.SET : Target.EDIT;

//		switch (target){
//			case SET:
//				break;
//			case EDIT:
//				break;
//		}

		if(0!=scriptId)
		{
			fullScript = dbHelper.getFullDeviceScriptByScriptId(scriptId);
		}else{
			fullScript = dbHelper.getLibraryScriptByLibId(libScriptId);
		}

		if(null!=fullScript && null!=fullScript.paramsLib){
			try
			{
				JSONArray paramsArray = new JSONArray(fullScript.paramsLib);
				if(null!=paramsArray) {
					pagerCount = paramsArray.length() + 1;
				}else{
					pagerCount = 1;
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
				pagerCount = 1;
			}
		}else{
			pagerCount = 1;
		}

		paramArrays = new HashMap[pagerCount-1];

		if(0!=scriptId){
			//form hashmap from db
			selBind = fullScript.bind;
			try
			{
				if(null!=fullScript.paramsLib && null!=fullScript.params)
				{
					JSONArray fullParamsArray = new JSONArray(fullScript.paramsLib);
					JSONArray valueParamsArray = new JSONArray(fullScript.params);
					for (int i = 0; i < fullParamsArray.length(); i++)
					{
						JSONObject paramObject = fullParamsArray.getJSONObject(i);
						HashMap map = new HashMap();
						map = constructMap(map, paramObject, valueParamsArray);
						paramArrays[i] = map;
					}
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}


		adapter = new ScriptSetPagerAdapter(getSupportFragmentManager(), pagerCount, fullScript.id, fullScript.libId, deviceId, siteId, selBind);
		pager = (ViewPager) findViewById(R.id.scriptSetViewPager);
		pager.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true;
			}
		});
		pager.setAdapter(adapter);

		serviceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(scripDelReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV));
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(scripDelReceiver);
		unbindService(serviceConnection);
	}

	private HashMap constructMap(HashMap map, JSONObject paramObject, JSONArray valueParamsArray)
	{
		String type = Func.getS(paramObject, "type");
		if(null!=type)
		{
			switch (type)
			{
				case "time":
				case "group":
				case "time_utc_group":
					try
					{
						JSONArray timeParamsArray = paramObject.getJSONArray("group");
						for (int i = 0; i < timeParamsArray.length(); i++)
						{
							map = constructMap(map, timeParamsArray.getJSONObject(i), valueParamsArray);
						}
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					return map;
				default:
					String name = Func.getS(paramObject, "name");
					map.put(name, findEqual(name, valueParamsArray));
					return map;
			}
		}else{
			String name = Func.getS(paramObject, "name");
			map.put(name, findEqual(name, valueParamsArray));
			return map;
		}
	}

	private JSONObject findEqual(String name, JSONArray valueParamsArray){
		for(int ii = 0; ii < valueParamsArray.length(); ii++){
			try
			{
				JSONObject valueParamsObject = valueParamsArray.getJSONObject(ii);
				String eqName = valueParamsObject.getString("name");
				if(eqName!=null && eqName.equals(name)){
					return  valueParamsObject;
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}

		}
		return null;
	}

	@Override
	public void onBackPressed()
	{
		goPrevious();
	}

	public void refreshViews(boolean anim){
		this.anim = anim;
		adapter.notifyDataSetChanged();
	}

	public int getPagerCount(){
		return pagerCount;
	}

	public int getCurrentItem(){
		return pager.getCurrentItem();
	}

	public void goPrevious(){
		int position = getCurrentItem();
		if(position != 0){
			if(target == Target.SET) paramArrays[position - 1] = new HashMap<String, JSONObject>();
			pager.setCurrentItem(position - 1);
			refreshViews(true);
		}else{
			super.onBackPressed();
		}
	}

	public void goNext()
	{
		int position = getCurrentItem();
		if(position != pagerCount - 1){
			pager.setCurrentItem(position + 1);
			refreshViews(true);
		}else{
			/*FINISH*/
			if(target == Target.EDIT){
				sendDelete();
			}else
			{
				sendScriptSet();
				goBack();
			}
		}
	}

	private void sendScriptSet()
	{
		JSONArray setArray = new JSONArray();
		if(null!=paramArrays && paramArrays.length > 0){
			for(HashMap<String, JSONObject> map:paramArrays){
				for(String key:map.keySet()){
					setArray.put(map.get(key));
				}

			}
		}
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject setObject = new JSONObject();
		try
		{
			setObject.put("device_id", deviceId);
			setObject.put("bind", selBind);
			setObject.put("program", fullScript.program);
			setObject.put("name", Func.getScriptDataForCurrLang(fullScript.nameLib, iso));
			setObject.put("params", (setArray!=null && setArray.length() > 0)? setArray : "");
			setObject.put("uid", fullScript.uid);
			setObject.put("extra", fullScript.extraLib !=null? fullScript.extraLib : "");
			D3Request d3Request = D3Request.createMessage(userInfo.roles, "SCRIPT_SET", setObject);
			if (d3Request.error == null)
			{
				sendMessageToServer(d3Request);
			} else
			{
				Func.pushToast(context, d3Request.error, (WizardScriptSetActivity) context);
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void sendDelete()
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put("device_id", fullScript.device);
			jsonObject.put("bind", fullScript.bind);
			D3Request d3Request = D3Request.createMessage(userInfo.roles, "SCRIPT_DEL", jsonObject);
			if(null==d3Request.error){
				sendMessageToServer(d3Request);
			}else{
				Func.pushToast(context, d3Request.error, (Activity) context);
			}

		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}


	public void addParamArray(HashMap map){
		this.paramArrays[getCurrentItem()-1] = map;
	}

	public HashMap<String, JSONObject> getParamArray(int position)
	{
		return paramArrays[position-1];
	}

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			if (null != myService)
			{
				return myService.send(d3Request.toString());
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(this, getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	}

	private void goBack()
	{
		Intent intent = new Intent();
		setResult(RESULT_OK, intent);
		finish();
	}

	public int getSelBind()
	{
		return selBind;
	}

	public void setSelBind(int selBind){
		this.selBind = selBind;
		refreshViews(false);
	}

	public boolean getAnim (){
		return this.anim;
	}

}
