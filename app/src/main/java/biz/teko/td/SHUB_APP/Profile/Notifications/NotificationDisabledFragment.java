package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import biz.teko.td.SHUB_APP.R;

public class NotificationDisabledFragment extends PreferenceFragment
{
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.notification_class_disabled);
	}
}
