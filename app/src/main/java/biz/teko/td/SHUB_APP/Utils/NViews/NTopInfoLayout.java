package biz.teko.td.SHUB_APP.Utils.NViews;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.text.Html;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.exoplayer2.ui.PlayerView;

import org.videolan.libvlc.MediaPlayer;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import biz.teko.td.SHUB_APP.Cameras.Activities.RTSP.NCameraRTSPFullScreenVideoActivity;
import biz.teko.td.SHUB_APP.Cameras.VLC.VLCListener;
import biz.teko.td.SHUB_APP.Cameras.VLC.VLCModule;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Other.Transition.SharedTextSizeHandler;

public class NTopInfoLayout extends FrameLayout {
	private static final int VIDEO_HEIGHT = 250;

	public static final int VIDEO_LOADING = 0;
	public static final int VIDEO_SUCCESS = 1;
	public static final int VIDEO_ERROR = 2;

	private final Context context;
	private int layout;
	private String title;
	private TextView textTitle;
	private String subtitle;
	private TextView textSubTitle;
	private CheckBox buttonFavorite;
	private boolean favorited;
	private NTopLayout.Type type;
	private LinearLayout closeButton;
	private LinearLayout settingsButton, mainLayout;
	private NActionButton actionButton;
	private FrameLayout navigationFrame;

	private boolean transition;

	private boolean alarm;
	private boolean offline;
	private boolean black;
	private boolean inverted;

	private ImageView bigImageView;
	private ImageView overlayImageView;
	private ImageView connectionImageView;
	private ImageView rkConnectionImageView;
	private ImageView socketImageView;
	private ImageView lanImageView;
	private ImageView relayTypeImageView;
	private ImageView relayScriptedImageView;
	private ImageView batteryImageView;
	private ImageView levelImageView;
	private ImageView tempImageView;
	private ImageView tempOutImageView;
	private ImageView simImageView;
	private ImageView sim1ImageView;
	private ImageView sim2ImageView;
	private ImageView delayImageView;
	private ImageView updateImageView;
	private TextView levelTextView;
	private TextView batteryTextView;
	private TextView simTextView;
	private TextView sim1TextView;
	private TextView sim2TextView;
	private TextView tempTextView;
	private TextView tempOutTextView;
	private FrameLayout tempOutFrame;
	private FrameLayout smallImageFrame;

	private int bigImage;
	private int socketImage;
	private int lanImage;
	private int relayTypeImage;
	private int relayScriptedImage;
	private int batteryImage;
	private int levelImage;
	private int tempImage;
	private int tempOutImage;
	private int sim1Image;
	private int sim2Image;
	private String levelText;
	private String batteryText;
	private String simText;
	private String sim1Text;
	private String sim2Text;
	private String tempText;
	private String tempOutText;

	private SurfaceView dahuaVideoView;
	private SurfaceView rtspVideoView;
	private PlayerView exoVideoView;
	private ProgressBar videoProgress;
	private FrameLayout rtspVideoViewParent;
	private ImageView fullScreenIcon;
	private MediaPlayer rtspMediaPlayer;
	private boolean prepared = false;
	private static final float DEFAULT_RTSP_RATIO = (float) 1024 / (float) 600;
	private ImageView videoErrorImageView;
	private VLCModule vlcModule;

	private String rtsp_link;
	private final int rtsp_ratio = 0;
	private String exo_link;
	private boolean video_full = false;

	private int actionButtonVisiblity = GONE;
	private int actionButtonImage = 0;

	private OnTopNavigationListener onTopNavigationListener;
	private OnActionClickListener onActionClickListener;
	private OnVideoLoadListener onVideoLoadListener;
	private SurfaceHolder.Callback surfaceCallback;

	private int transition_start;

	private OnStateClickListener onStateClickListener;
	private OnTouchListener onTouchListener;

	private LinearLayout batteryButton;
	private LinearLayout levelButton;
	private FrameLayout connectionButton;
	private LinearLayout socketButton;
	private LinearLayout lanButton;
	private LinearLayout tempButton;
	private LinearLayout tempOutButton;
	private LinearLayout simButton;
	private LinearLayout sim1Button;
	private LinearLayout sim2Button;
	private LinearLayout delayButton;
	private LinearLayout updateButton;

	public void setOnActionClickListener(OnActionClickListener onActionClickListener) {
		this.onActionClickListener = onActionClickListener;
	}

	public void stopVideo() {
		if (rtspMediaPlayer != null) {
			vlcModule.playerNull();
		}
	}

//	public void setSurfaceCallback(SurfaceHolder.Callback callback)
//	{
//		this.surfaceCallback = callback;
//		if(null!=dahuaVideoView)dahuaVideoView.getHolder().addCallback(callback);
//	}
//
	public SurfaceView getSurfaceView(){
		return dahuaVideoView;
	}
//
//	public void initSurfaceView(NDahuaLiveModel liveModel)
//	{
//		if(null!=dahuaVideoView){
//			showVideoFrame();
//			liveModel.initSurfaceView(dahuaVideoView);
//		}
//	}


	public interface OnActionClickListener {
		void onActionClick(int value);
	}

	public void setOnTopNavigationListener(OnTopNavigationListener onTopNavigationListener) {
		this.onTopNavigationListener = onTopNavigationListener;
	}

	public interface OnTopNavigationListener {
		void favoriteChanged();

		void backPressed();

		void setOptions();
	}

	public void setOnVideoLoadListener(OnVideoLoadListener onVideoLoadListener) {
		this.onVideoLoadListener = onVideoLoadListener;
	}

	public interface OnVideoLoadListener {
		void fullScreen();
		void onError();
	}

	public void setOnStateClickListener(OnStateClickListener onStateClickListener) {
		this.onStateClickListener = onStateClickListener;
	}

	public interface OnStateClickListener {
		void onStateClick(int type);
//		void onLeveClick();
//		void onTempClick();
	}


	public NTopInfoLayout(@NonNull Context context) {
		super(context);
		this.context = context;
	}

	public NTopInfoLayout(Context context, AttributeSet attr) {
		super(context, attr);
		this.context = context;
		parseAttributes(attr);
		setupView();
		bindView();
	}

	private void parseAttributes(AttributeSet attr) {
		TypedArray a = getContext().obtainStyledAttributes(attr, R.styleable.NTopInfoLayout, 0, 0);
		try {
			layout = a.getResourceId(R.styleable.NTopInfoLayout_NTopFrameLayout, 0);
			title = a.getString(R.styleable.NTopInfoLayout_NTopFrameTitle);
		} finally {
			a.recycle();
		}
	}

	private void setupView() {
		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
		layoutInflater.inflate(layout, this, true);

		rtspVideoViewParent =findViewById(R.id.nVideoViewParent);
		rtspVideoView =  findViewById(R.id.nVideoView);
		exoVideoView =  findViewById(R.id.nVideoViewExo);
		videoProgress =  findViewById(R.id.pb_search);
		fullScreenIcon = findViewById(R.id.exo_fullscreen_icon);
		videoErrorImageView =  findViewById(R.id.nImageVideoError);

		smallImageFrame = findViewById(R.id.nFrameSmallIcon);

		textTitle = findViewById(R.id.nTextTitle);
		textSubTitle = findViewById(R.id.nTextSubTitle);

		buttonFavorite = findViewById(R.id.nButtonFavorite);
		closeButton = findViewById(R.id.nButtonClose);
		settingsButton = findViewById(R.id.nButtonSettings);
		mainLayout = findViewById(R.id.mainLayout);
		actionButton = findViewById(R.id.nButtonAction);
		navigationFrame = findViewById(R.id.nTopNavigationFrame);

		bigImageView = findViewById(R.id.nImageBig);
		overlayImageView = findViewById(R.id.nImageOverlay);

		connectionImageView = findViewById(R.id.nImageConnection);
		rkConnectionImageView = findViewById(R.id.nImageRKConnection);
		connectionButton = findViewById(R.id.nButtonConnection);

		delayImageView = findViewById(R.id.nImageDelay);
		delayButton = findViewById(R.id.nButtonDelay);

		updateImageView = findViewById(R.id.nImageUpdate);
		updateButton = findViewById(R.id.nButtonUpdate);

		socketImageView = findViewById(R.id.nImageSocket);
		lanImageView = findViewById(R.id.nImageLan);
		relayTypeImageView = findViewById(R.id.nImageRelayType);
		relayScriptedImageView = findViewById(R.id.nImageScript);

		socketButton = findViewById(R.id.nButtonSocket);
		lanButton = findViewById(R.id.nButtonLan);

		batteryImageView = findViewById(R.id.nImageBattery);
		batteryTextView = findViewById(R.id.nTextBattery);
		batteryButton = findViewById(R.id.nButtonBattery);

		levelImageView = findViewById(R.id.nImageLevel);
		levelTextView = findViewById(R.id.nTextLevel);
		levelButton = findViewById(R.id.nButtonLevel);

		tempImageView = findViewById(R.id.nImageTemp);
		tempTextView = findViewById(R.id.nTextTemp);
		tempButton = findViewById(R.id.nButtonTemp);

		tempOutImageView = findViewById(R.id.nImageTempOut);
		tempOutTextView = findViewById(R.id.nTextTempOut);
		tempOutFrame = findViewById(R.id.nFrameTempOut);
		tempOutButton = findViewById(R.id.nButtonTempOut);

		simImageView = findViewById(R.id.nImageSim);
		simTextView = findViewById(R.id.nTextSim);
		simButton = findViewById(R.id.nButtonSim);

		sim1ImageView = findViewById(R.id.nImageSim1);
		sim1TextView = findViewById(R.id.nTextSim1);
		sim1Button = findViewById(R.id.nButtonSim1);

		sim2ImageView = findViewById(R.id.nImageSim2);
		sim2TextView = findViewById(R.id.nTextSim2);
		sim2Button = findViewById(R.id.nButtonSim2);

		dahuaVideoView = (SurfaceView) findViewById(R.id.real_view);

	}

	private void bindView() {
		setTitleText();
		setFavoriteButton();
		setCloseButton();
		setSettingsButton();
		setActionButton();

		setVideoVisibility(VIDEO_LOADING);

		setRTSPVideoView();

		setBigImageView();
		setSmallImages();
	}

	//RTSP Cameras
	private void setRTSPVideoView() {
		if (null != rtspVideoView) {
			if (null != rtsp_link) {
				setVideoVisibility(VIDEO_LOADING);
				createVLCModule();
				setSurfaсeHolderCallback();
				if(null!=rtspMediaPlayer) rtspMediaPlayer.play();
			} else {
				setVideoVisibility(VIDEO_ERROR);
				if (null != rtspMediaPlayer) {
					rtspMediaPlayer.release();
					rtspMediaPlayer = null;
					prepared = false;
				}
			}
		}
	}


	private void createVLCModule() {
		vlcModule = new VLCModule(rtspVideoView, rtspVideoViewParent, context, rtsp_link);
		rtspMediaPlayer = vlcModule.getMediaPlayer();
		vlcModule.setListener(new VLCModule.ISizeClickListener() {
			@Override
			public void fullscreen() {
				Intent intent = new Intent(context, NCameraRTSPFullScreenVideoActivity.class);
				intent.putExtra("link", rtsp_link);
				context.startActivity(intent);
			}

			@Override
			public void windowed() {

			}
		});
	}

	private void setSurfaсeHolderCallback() {
		SurfaceHolder videoHolder = rtspVideoView.getHolder();
		videoHolder.addCallback(new SurfaceHolder.Callback() {
			@Override
			public void surfaceCreated(@NonNull SurfaceHolder holder) {
				/*непонятно почему, то вызывается дважды при resume
				 * поэтому флаг prepared*/
				updateRTSPMediaPlayer();
				if (null != rtspMediaPlayer && !prepared) {
					prepared = true;
				}
			}

			@Override
			public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {
				if (null != rtspMediaPlayer) {
				}
			}

			@Override
			public void surfaceDestroyed(@NonNull SurfaceHolder holder) {

			}
		});
	}

	private void updateRTSPMediaPlayer() {
		try {
			if (null == rtspMediaPlayer)
				createVLCModule();
			vlcModule.setEventListener(new VLCListener() {
				@Override
				public void onComplete() {
					setVideoVisibility(VIDEO_SUCCESS);
					resizeVideoView();
					prepared = false;
				}

				@Override
				public void onError() {
					if (null != onVideoLoadListener) onVideoLoadListener.onError();
					setVideoVisibility(VIDEO_ERROR);
				}
			});
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		}
	}

	public void setVideoVisibility(int state){
		switch (state){
			case VIDEO_LOADING:
				if(null!=dahuaVideoView) dahuaVideoView.setVisibility(INVISIBLE);
				if (null != rtspVideoViewParent) {
					rtspVideoViewParent.setVisibility(INVISIBLE);
					rtspVideoViewParent.setAlpha(0);
				}
				if (null != videoProgress) videoProgress.setVisibility(VISIBLE);
				if (null != videoErrorImageView) videoErrorImageView.setVisibility(GONE);
				break;
			case VIDEO_SUCCESS:
				if(null!=dahuaVideoView) dahuaVideoView.setVisibility(VISIBLE);
				if (null != rtspVideoViewParent) {
					rtspVideoViewParent.setVisibility(VISIBLE);
					rtspVideoViewParent.setAlpha(1);
				}
				if (null != videoProgress) videoProgress.setVisibility(GONE);
				if (null != videoErrorImageView) videoErrorImageView.setVisibility(GONE);
				break;
			case VIDEO_ERROR:
				if (null != rtspVideoViewParent) {
					rtspVideoViewParent.setVisibility(INVISIBLE);
					rtspVideoViewParent.setAlpha(0);
				}
				if (null != videoProgress) videoProgress.setVisibility(GONE);
				if (null != videoErrorImageView) videoErrorImageView.setVisibility(VISIBLE);
				break;
		}
	}

//	public void showVideoLoading() {
//		if(null!=dahuaVideoView) dahuaVideoView.setVisibility(INVISIBLE);
//		if (null != rtspVideoViewParent) {
//			rtspVideoViewParent.setVisibility(INVISIBLE);
//			rtspVideoViewParent.setAlpha(0);
//		}
//		if (null != videoProgress) videoProgress.setVisibility(VISIBLE);
//		if (null != videoErrorImageView) videoErrorImageView.setVisibility(GONE);
//	}
//
//	private void showRTSPError() {
//		if (null != rtspVideoViewParent) {
//			rtspVideoView.setVisibility(INVISIBLE);
//			rtspVideoView.setAlpha(0);
//		}
//		if (null != videoProgress) videoProgress.setVisibility(GONE);
//		if (null != videoErrorImageView) videoErrorImageView.setVisibility(VISIBLE);
//	}
//
//	public void showVideoFrame() {
//		if(null!=dahuaVideoView) dahuaVideoView.setVisibility(VISIBLE);
//		if (null != rtspVideoViewParent) {
//			rtspVideoViewParent.setVisibility(VISIBLE);
//			rtspVideoViewParent.setAlpha(1);
//		}
//		if (null != videoProgress) videoProgress.setVisibility(GONE);
//		if (null != videoErrorImageView) videoErrorImageView.setVisibility(GONE);
//	}

	public void resizeVideoView() {
		if (video_full) {
			this.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		} else {
			this.setLayoutParams(new LinearLayout.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT, 0));
		}

		int screenWidth = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();
		int screenHeight = ((Activity) context).getWindowManager().getDefaultDisplay().getHeight();

		DisplayMetrics displaymetrics = new DisplayMetrics();
		((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		ViewGroup.LayoutParams videoParams = rtspVideoView.getLayoutParams();
		videoParams.width = displaymetrics.widthPixels;
		videoParams.height = displaymetrics.heightPixels;
		rtspVideoView.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		rtspVideoViewParent.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
		vlcModule.getVout().setWindowSize(screenWidth, (Func.dpToPx(VIDEO_HEIGHT, context)));
		ViewGroup.LayoutParams parentLayoutParams = rtspVideoViewParent.getLayoutParams();
		parentLayoutParams.height = (Func.dpToPx(VIDEO_HEIGHT, context));
	}

	public void resizeVideoViewWithChangeRotation(boolean full) {
		this.video_full = full;
		if (video_full) {
			this.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		} else {
			this.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Func.dpToPx(0, context), 1.0f));
		}
		int screenHeight = ((Activity) context).getWindowManager().getDefaultDisplay().getHeight();
		int screenWidth = ((Activity) context).getWindowManager().getDefaultDisplay().getWidth();

		FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) rtspVideoView.getLayoutParams();

		lp.width = screenWidth;
		lp.height = (int) ((float) screenHeight / (rtsp_ratio == 0 ? DEFAULT_RTSP_RATIO : (float) rtsp_ratio));

		if (lp.height > screenWidth) {
			lp.height = screenHeight;
			lp.width = (int) ((float) screenWidth * (rtsp_ratio == 0 ? DEFAULT_RTSP_RATIO : (float) rtsp_ratio));
			lp.leftMargin = (screenHeight - lp.width) / 2;

		} else {
			lp.leftMargin = 0;
		}

		rtspVideoView.setLayoutParams(lp);
	}

	//INNER VIEW SETTERS

	private void setBigImageView() {
		if (0 != bigImage && null != bigImageView)
			bigImageView.setImageDrawable(getResources().getDrawable(bigImage));
	}

	private void setSmallImages() {
		setConnectionView();
		setUpdateView();
		setDelayView();
		setSocketView();
		setLanView();
		setRelayTypeView();
		setRelayScriptedView();
		setBatteryView();
		setLevelView();
		setSimView();
		setSim1View();
		setSim2View();
		setTempView();
		setTempOutView();
	}

	private void setUpdateView()
	{
		if(null!=updateButton){
			updateButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_UPDATE));
		}
	}

	private void setDelayView() {
		if (null != delayButton) {
			delayButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_DELAY));
		}
	}

	private void setConnectionView() {
		if (null != connectionButton) {
			connectionButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_CONNECTION));
		}
	}


	private void setSocketView() {
		if (null != socketImageView) {
			if (0 != socketImage) {
				socketImageView.setImageDrawable(getResources().getDrawable(socketImage));
			}
		}
		if (null != socketButton)
			socketButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_SUPPLY));
	}

	private void setLanView() {
		if (null != lanImageView) {
			if (0 != lanImage) lanImageView.setImageDrawable(getResources().getDrawable(lanImage));
		}
		if (null != lanButton)
			lanButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_LEVEL));
	}

	private void setRelayTypeView() {
		if (null != relayTypeImageView) {
			if (0 != relayTypeImage)
				relayTypeImageView.setImageDrawable(getResources().getDrawable(relayTypeImage));
		}
	}

	private void setRelayScriptedView() {
		if (null != relayScriptedImageView) {
			if (0 != relayScriptedImage)
				relayTypeImageView.setImageDrawable(getResources().getDrawable(relayScriptedImage));
		}
	}


	private void setBatteryView() {
		if (null != batteryImageView) {
			if (0 != batteryImage) {
				batteryImageView.setImageDrawable(getResources().getDrawable(batteryImage));
			}
		}
		if (null != batteryTextView) {
			if (null != batteryText) {
				batteryTextView.setText(batteryText);
			}
		}
		if (null != batteryButton)
			batteryButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_SUPPLY));
	}

	private void setLevelView() {
		if (null != levelImageView) {
			if (0 != levelImage)
				levelImageView.setImageDrawable(getResources().getDrawable(levelImage));
		}
		if (null != levelTextView) {
			if (null != levelText) levelTextView.setText(levelText);
		}
		if (null != levelButton)
			levelButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_LEVEL));
	}

	private void setSimView() {
		if (null != simImageView) {
			if (0 != sim1Image)
				simImageView.setImageDrawable(getResources().getDrawable(sim1Image));
		}
		if (null != simTextView) {
			if (null != simText) simTextView.setText(simText);
		}
		if (null != simButton)
			simButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_LEVEL));
	}

	private void setSim1View() {
		if (null != sim1ImageView) {
			if (0 != sim1Image)
				sim1ImageView.setImageDrawable(getResources().getDrawable(sim1Image));
		}
		if (null != sim1TextView) {
			if (null != sim1Text) sim1TextView.setText(sim1Text);
		}
		if (null != sim1Button)
			sim1Button.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_LEVEL));
	}

	private void setSim2View() {
		if (null != sim2ImageView) {
			if (0 != sim2Image)
				sim2ImageView.setImageDrawable(getResources().getDrawable(sim2Image));
		}
		if (null != sim2TextView) {
			if (null != sim2Text) sim2TextView.setText(sim2Text);
		}
		if (null != sim2Button)
			sim2Button.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_LEVEL));
	}

	private void setTempView() {
		if (null != tempImageView) {
			if (0 != tempImage)
				tempImageView.setImageDrawable(getResources().getDrawable(tempImage));
		}
		if (null != tempTextView) {
			if (null != tempText) tempTextView.setText(tempText);
		}
		if (null != tempButton)
			tempButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_TEMP));
	}

	private void setTempOutView() {
		if (null != tempOutImageView) {
			if (0 != tempOutImage)
				tempOutImageView.setImageDrawable(getResources().getDrawable(tempOutImage));
		}
		if (null != tempOutTextView) {
			if (null != tempOutText) tempOutTextView.setText(tempOutText);
		}
		if (null != tempOutButton)
			tempOutButton.setOnTouchListener(getStateTouchListener(Const.STATE_TYPE_TEMP));
	}

	private void setSettingsButton() {
		if (null != settingsButton) {
			settingsButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					if (null != onTopNavigationListener) onTopNavigationListener.setOptions();
				}
			});
		}
	}

	private void setActionButton() {
		if (null != actionButton) {
			actionButton.setVisibility(actionButtonVisiblity);
			actionButton.setImage(actionButtonImage);
			actionButton.setIncrease(true);
			actionButton.setOnButtonClickListener(new NActionButton.OnButtonClickListener() {
				@Override
				public void onButtonClick(int action) {
					if (null != onActionClickListener) onActionClickListener.onActionClick(action);
				}
			});
		}
	}


	private void setCloseButton() {
		if (null != closeButton) closeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (null != onTopNavigationListener) onTopNavigationListener.backPressed();
			}
		});
	}

	private void setFavoriteButton() {
		if (null != buttonFavorite) {
			buttonFavorite.setChecked(favorited);
			buttonFavorite.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					if (event.getAction() == MotionEvent.ACTION_DOWN) {
						if (null != onTopNavigationListener)
							onTopNavigationListener.favoriteChanged();
						return true;
					}
					return false;
				}
			});
		}
	}

	private OnTouchListener getStateTouchListener(int type) {
		return (v, event) -> {
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					setPressed(true);
					return false;
				case MotionEvent.ACTION_CANCEL:
				case MotionEvent.ACTION_UP:
					setPressed(false);
					if (event.getAction() == MotionEvent.ACTION_UP
							&& null != onStateClickListener) {
						onStateClickListener.onStateClick(type);
						performClick();
					}
					return false;
				case MotionEvent.ACTION_MOVE:
					return false;
				default:
					setPressed(false);
					return false;
			}
		};
	}

	public void setTransition(Context context, boolean transition, int transition_start) {
		if (transition) {
			SharedTextSizeHandler handler = new SharedTextSizeHandler((Activity) context);
			if (transition_start != 0)
				handler.addTextView(textTitle, transition_start, Func.getTextSize(context, textTitle));
//			handler.addTextViewSizeResource(textTitle, R.dimen.mini_text_size, R.dimen.small_text_size);
			ViewCompat.setTransitionName(textTitle, Const.TITLE_TRANSITION_NAME);
		}
	}

	private void setTitleText() {
		if (null != textTitle && null != title) {
			textTitle.setText(Html.fromHtml(title));
		}
	}

	private void setSubTitleText() {
		if (null != textSubTitle && null != subtitle) {
			textSubTitle.setText(subtitle);
		}
	}

	private void setTypeSet() {
		if (null != type) {
			switch (type) {
				case zone_wired:
					levelImageView.setVisibility(GONE);
					levelTextView.setVisibility(GONE);
					tempImageView.setVisibility(GONE);
					tempTextView.setVisibility(GONE);
					batteryImageView.setVisibility(GONE);
					batteryTextView.setVisibility(GONE);
				case zone_wireless:
					tempOutFrame.setVisibility(GONE);
//					tempOutImageView.setVisibility(GONE);
					tempOutTextView.setVisibility(GONE);
				case zone_temp:
					rkConnectionImageView.setVisibility(VISIBLE);
					simImageView.setVisibility(GONE);
					simTextView.setVisibility(GONE);
					sim1ImageView.setVisibility(GONE);
					sim1TextView.setVisibility(GONE);
					sim2ImageView.setVisibility(GONE);
					sim2TextView.setVisibility(GONE);
					relayScriptedImageView.setVisibility(GONE);
					relayTypeImageView.setVisibility(GONE);
					lanImageView.setVisibility(GONE);
					socketImageView.setVisibility(GONE);
					break;
				case relay_wired_siren:
					relayTypeImageView.setVisibility(GONE);
				case relay_wired_auto_binded:
//					relayScriptedImageView.setVisibility(VISIBLE);
				case relay_wired_auto:
					overlayImageView.setVisibility(GONE);
				case relay_wired:
					rkConnectionImageView.setVisibility(VISIBLE);
					batteryImageView.setVisibility(GONE);
					batteryTextView.setVisibility(GONE);
					levelImageView.setVisibility(GONE);
					levelTextView.setVisibility(GONE);
					simImageView.setVisibility(GONE);
					simTextView.setVisibility(GONE);
					sim1ImageView.setVisibility(GONE);
					sim1TextView.setVisibility(GONE);
					sim2ImageView.setVisibility(GONE);
					sim2TextView.setVisibility(GONE);
					lanImageView.setVisibility(GONE);
					socketImageView.setVisibility(GONE);
					tempImageView.setVisibility(GONE);
					tempTextView.setVisibility(GONE);
					tempOutFrame.setVisibility(GONE);
//					tempOutImageView.setVisibility(GONE);
					tempOutTextView.setVisibility(GONE);
					break;
				case relay_socket:
					batteryImageView.setVisibility(GONE);
					batteryTextView.setVisibility(GONE);
				case relay_szo:
				case relay_wireless:
					rkConnectionImageView.setVisibility(VISIBLE);
					relayScriptedImageView.setVisibility(VISIBLE);
					simImageView.setVisibility(GONE);
					simTextView.setVisibility(GONE);
					sim1ImageView.setVisibility(GONE);
					sim1TextView.setVisibility(GONE);
					sim2ImageView.setVisibility(GONE);
					sim2TextView.setVisibility(GONE);
					lanImageView.setVisibility(GONE);
					socketImageView.setVisibility(GONE);
					tempImageView.setVisibility(GONE);
					tempTextView.setVisibility(GONE);
					tempOutFrame.setVisibility(GONE);
//					tempOutImageView.setVisibility(GONE);
					tempOutTextView.setVisibility(GONE);
					if (type == NTopLayout.Type.relay_szo) relayTypeImageView.setVisibility(GONE);
					break;
				case section:
					if (null != smallImageFrame) smallImageFrame.setVisibility(GONE);
					break;
				case device_sh:
					updateButton.setVisibility(VISIBLE);
				case device_common:
					connectionImageView.setVisibility(VISIBLE);
					sim1Button.setVisibility(GONE);
					sim1ImageView.setVisibility(GONE);
					sim1TextView.setVisibility(GONE);
					sim2Button.setVisibility(GONE);
					sim2ImageView.setVisibility(GONE);
					sim2TextView.setVisibility(GONE);
					relayScriptedImageView.setVisibility(GONE);
					relayTypeImageView.setVisibility(GONE);
					levelImageView.setVisibility(GONE);
					levelTextView.setVisibility(GONE);
					tempImageView.setVisibility(GONE);
					tempTextView.setVisibility(GONE);
					tempOutFrame.setVisibility(GONE);
//					tempOutImageView.setVisibility(GONE);
					tempOutTextView.setVisibility(GONE);
					break;
				case device_sh_2:
					connectionImageView.setVisibility(VISIBLE);
					simImageView.setVisibility(GONE);
					simTextView.setVisibility(GONE);
					relayScriptedImageView.setVisibility(GONE);
					relayTypeImageView.setVisibility(GONE);
					levelImageView.setVisibility(GONE);
					levelTextView.setVisibility(GONE);
					tempImageView.setVisibility(GONE);
					tempTextView.setVisibility(GONE);
					tempOutFrame.setVisibility(GONE);
//					tempOutImageView.setVisibility(GONE);
					tempOutTextView.setVisibility(GONE);
					updateButton.setVisibility(VISIBLE);
					break;
				case device_mb:
					smallImageFrame.setVisibility(GONE);
					break;
			}
		}
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace) {
		return super.onCreateDrawableState(extraSpace + 29);
	}


	public void setType(NTopLayout.Type type) {
		this.type = type;
		setTypeSet();
	}

	//	//Background states
	public void setAlarm(boolean alarm) {
		this.alarm = alarm;
		this.inverted = true;
	}

	//PUBLIC SET DATA & OPTIONS

	public void setRTSPLink(String link) {
		this.rtsp_link = link;
		setRTSPVideoView();
	}

	public void setTitle(String title) {
		this.title = title;
		setTitleText();
	}

	public void setSubTitle(String subTitle) {
		this.subtitle = subTitle;
		setSubTitleText();
	}

	public void setFavorited(boolean b) {
		this.favorited = b;
		setFavoriteButton();
	}

	public void setLevelText(String levelText) {
		if (null != levelText) {
			this.levelText = levelText;
		} else {
			this.levelText = "—";
		}
		setLevelView();
	}

	public void setBatteryText(String batteryText) {
		if (null != batteryText) {
			this.batteryText = batteryText;
		} else {
			this.batteryText = "—";
		}
		setBatteryView();
	}

	public void setSimText(String simText) {
		if (null != simText) {
			this.simText = simText;
		} else {
			this.simText = "—";
		}
		setSimView();
	}

	public void setSim1Text(String sim1Text) {
		if (null != sim1Text) {
			this.sim1Text = sim1Text;
		} else {
			this.sim1Text = "—";
		}
		setSim1View();
	}

	public void setSim2Text(String sim2Text) {
		if (null != sim2Text) {
			this.sim2Text = sim2Text;
		} else {
			this.sim2Text = "—";
		}
		setSim2View();
	}

	public void setTempText(String tempText) {
		if (null != tempText) {
			this.tempText = tempText + "°C";
		} else {
			this.tempText = "—";
		}
		setTempView();
	}

	public void setTempOutText(String tempOutText) {
		if (null != tempOutText) {
			this.tempOutText = tempOutText + "°C";
		} else {
			this.tempOutText = "—";
		}
		setTempOutView();
	}

	//Not used now

	public void setBigImage(int bigImage) {
		this.bigImage = bigImage;
		setBigImageView();
	}

	public void setSocketImage(int socketImage) {
		this.socketImage = socketImage;
		setSocketView();
	}

	public void setLanImage(int lanImage) {
		this.lanImage = lanImage;
		setLanView();
	}

	public void setRelayTypeImage(int relayTypeImage) {
		this.relayTypeImage = relayTypeImage;
		setRelayTypeView();
	}

	public void setRelayScriptedImage(int relayScriptedImage) {
		this.relayScriptedImage = relayScriptedImage;
		setRelayScriptedView();
	}

	public void setBatteryImage(int batteryImage) {
		this.batteryImage = batteryImage;
		setBatteryView();
	}

	public void setLevelImage(int levelImage) {
		this.levelImage = levelImage;
		setLevelView();
	}

	public void setTempImage(int tempImage) {
		this.tempImage = tempImage;
		setTempView();
	}

	public void setTempOutImage(int tempOutImage) {
		this.tempOutImage = tempOutImage;
		setTempOutView();
	}

	public void setSim1Image(int sim1Image) {
		this.sim1Image = sim1Image;
		setSim1View();
	}

	public void setSim2Image(int sim2Image) {
		this.sim2Image = sim2Image;
		setSim2View();
	}

	public void setActionButtonVisiblity(int visiblity) {
		this.actionButtonVisiblity = visiblity;
		setActionButton();
	}

	public void setActionButtonImage(int imageAction) {
		this.actionButtonImage = imageAction;
		setActionButton();
	}

	public TextView getTitleView() {
		return textTitle;
	}

	public void showNavigation(int visibility) {
		if (null != navigationFrame) navigationFrame.setVisibility(visibility);
	}
}
