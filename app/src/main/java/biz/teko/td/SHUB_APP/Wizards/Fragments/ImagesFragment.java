package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 26.06.2017.
 */

public class ImagesFragment extends Fragment
{
	private int position;
	private Context context;
	private View v;

	public static Fragment newInstance(int position)
	{
		ImagesFragment imagesFragment = new ImagesFragment();
		Bundle b = new Bundle();
		b.putInt("position", position);
		imagesFragment.setArguments(b);
		return imagesFragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
	{
		position = getArguments().getInt("position");
		int layout = R.layout.fragment_wizard_reg_1;
		context = container.getContext();
		switch (position)
		{
			case 0:
				layout = R.layout.fragment_image_1;
				v = inflater.inflate(layout, container, false);
				break;
			case 1:
				layout = R.layout.fragment_image_2;
				v = inflater.inflate(layout, container, false);
				break;
			case 2:
				layout = R.layout.fragment_image_3;
				v = inflater.inflate(layout, container, false);
				break;
			case 3:
				layout = R.layout.fragment_image_4;
				v = inflater.inflate(layout, container, false);
				break;
			default:
				break;
		}
		return v;
	}
}
