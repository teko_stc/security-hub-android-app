package biz.teko.td.SHUB_APP.Profile.Security;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Adapter.HistorySitesSpinnerAdapter;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 31.08.2017.
 */

public  class SecurityForceObjectsFragment extends Fragment
{
	private Context context;
	private DBHelper dbHelper;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
	{
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		View v = inflater.inflate(R.layout.fragment_security_force_select_object, container, false);

		Site[] sites  = dbHelper.getAllSitesArray();
		final Spinner spinnerObjects  = (Spinner) v.findViewById(R.id.securityForceSpinnerObjects);
		final HistorySitesSpinnerAdapter sitesSpinnerAdapter = new HistorySitesSpinnerAdapter(context,  R.layout.spinner_item_dropdown, sites, 0);
		spinnerObjects.setAdapter(sitesSpinnerAdapter);

		Button button = (Button) v.findViewById(R.id.securityForceSpinnerOSetNext);
		button.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				int id = sitesSpinnerAdapter.getItem(spinnerObjects.getSelectedItemPosition()).id;
				getFragmentManager()
						.beginTransaction()
						.replace(R.id.prefContainer, SecurityForceCodeSetFragment.newInstance(id))
						.addToBackStack("0")
						.commit();
			}
		});
		return v;
	}
}
