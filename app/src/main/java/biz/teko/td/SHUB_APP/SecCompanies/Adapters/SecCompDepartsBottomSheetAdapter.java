package biz.teko.td.SHUB_APP.SecCompanies.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SecCompanies.Models.SCDepart;

public  class SecCompDepartsBottomSheetAdapter extends ArrayAdapter
{
	private final Context context;
	private final LayoutInflater layoutInflater;
	SCDepart[] departs;

	public SecCompDepartsBottomSheetAdapter(@NonNull Context context, int resource, SCDepart[] departs)
	{
		super(context, resource);
		this.departs = departs;
		this.context = context;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		SCDepart scDepart = departs[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(scDepart.subject);
		return view;
	}

	public int getCount() {
		return departs.length;
	}

	public SCDepart getItem(int position){
		return departs[position];
	}

	public long getItemId(int position){
		return position;
	}
}
