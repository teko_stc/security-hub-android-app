package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Other.NSwitchSelect;

public class NMenuListElement extends FrameLayout implements Checkable
{
	private OnCheckedChangeListener onCheckedChangeListener;
	private OnRootClickListener onRootClickListener;


	private int layout;
	private TextView textTitle;
	private TextView textSubTitle;
	private TextView textSubTitleBottom;
	private String title;
	private String subtitle;
	private String subtitleBottom;
	private int titleColor;
	private int subtitleColor;
	private boolean checked;

	public static final int DEFAULT_TEXT_COLOR = Color.TRANSPARENT;
	private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked, android.R.attr.state_checkable};
	private ImageView checkButton;
	private NSwitchSelect switchView;
	private boolean checkable;
	private int checkImage;
	private FrameLayout rootView;
	private boolean transition;
	private boolean isSwitch;

	public void setOnRootClickListener(OnRootClickListener onRootClickListener){
		this.onRootClickListener = onRootClickListener;
	}

	public interface OnRootClickListener{
		void onRootClick();
	}

	public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener){
		this.onCheckedChangeListener = onCheckedChangeListener;
	}

	public interface OnCheckedChangeListener{
		void onCheckedChanged();
	}

	public NMenuListElement(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		parseAttributes(attrs);
		setupView();
		bindView();
	}

	private void setupView()
	{
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(layout, this, true);

		textTitle = (TextView) findViewById(R.id.nTextTitle);
		textSubTitle =  (TextView) findViewById(R.id.nTextSubTitle);
		checkButton = (ImageView) findViewById(R.id.nButtonCheck);
		switchView = (NSwitchSelect) findViewById(R.id.nButtonSwitch);
		rootView = (FrameLayout) findViewById(R.id.nRootView);

	}

	private void bindView()
	{
		setTitleText();
		setSubTitleText();
		setSubTitleBottomText();
		setCheckable(checkable);

		if(null!=rootView) rootView.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				if(checkable){
					if(null!=onCheckedChangeListener)
					{
						switchCheck();
						onCheckedChangeListener.onCheckedChanged();
					}
				}
				if(null!=onRootClickListener)onRootClickListener.onRootClick();
			}
		});
	}

	public NMenuListElement(Context context, int layout)
	{
		super(context);
		setupView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a  = getContext().obtainStyledAttributes(attrs, R.styleable.NMenuListElement, 0, 0);
		Resources resources = getContext().getResources();
		try
		{
			layout = a.getResourceId(R.styleable.NMenuListElement_NMenuListLayout, 0);
			title = a.getString(R.styleable.NMenuListElement_NMenuListTitle);
			subtitle = a.getString(R.styleable.NMenuListElement_NMenuListSubtitle);
			subtitleBottom = a.getString(R.styleable.NMenuListElement_NMenuListSubtitleBottom);
			titleColor = a.getResourceId(R.styleable.NMenuListElement_NMenuListTitleColor, R.color.n_menu_edit_text_selector);
			subtitleColor = a.getResourceId(R.styleable.NMenuListElement_NMenuListSubtitleColor, R.color.n_menu_edit_text_selector);
			checkable = a.getBoolean(R.styleable.NMenuListElement_NMenuListCheckable, false);
			checkImage = a.getResourceId(R.styleable.NMenuListElement_NMenuListCheckImage, 0);
			isSwitch = a.getBoolean(R.styleable.NMenuListElement_NMenuListSwitchable, false);;

			//			subtitleColor = a.getColor(R.styleable.NMenuListView_NMenuListSubtitleColor, resources.getColor(R.color.n_menu_edit_text_selector));
		} finally
		{
			a.recycle();
		}
	}

	private void setTitleText(){
		if(null!=textTitle)
		{
			if (null!=title && !title.equals(""))
			{
				textTitle.setVisibility(VISIBLE);
				textTitle.setText(title);
			} else
			{
				textTitle.setVisibility(GONE);
			}
			if (DEFAULT_TEXT_COLOR != titleColor) textTitle.setTextColor(getResources().getColorStateList(titleColor));
		}
	}

	public TextView getTitleView(){
		return textTitle;
	}

	private void setSubTitleText(){
		if(null!=textSubTitle)
		{
			if (null!= subtitle && !subtitle.equals(""))
			{
				textSubTitle.setVisibility(VISIBLE);
				textSubTitle.setText(subtitle);
			} else
			{
				textSubTitle.setVisibility(GONE);
			}
			if (DEFAULT_TEXT_COLOR != subtitleColor) textSubTitle.setTextColor(getResources().getColorStateList(subtitleColor));
		}
	}

	public void setTitleColor(int color) {
		if (textTitle != null) {
			textTitle.setTextColor(getResources().getColor(color));
		}
	}

	private void setSubTitleBottomText(){
		if(null!=textSubTitleBottom)
		{
			if (null!= subtitleBottom && !subtitleBottom.equals(""))
			{
				textSubTitleBottom.setVisibility(VISIBLE);
				textSubTitleBottom.setText(subtitle);
			} else
			{
				textSubTitleBottom.setVisibility(GONE);
			}
			if (DEFAULT_TEXT_COLOR != subtitleColor) textSubTitleBottom.setTextColor(getResources().getColorStateList(subtitleColor));
		}
	}

	public void setEnabled(boolean b){
		if(null!=rootView) rootView.setEnabled(b);
	}

	public void setTitle(String title){
		this.title = title;
		setTitleText();
	}

	public void setSubtitle(String subtitle){
		this.subtitle = subtitle;
		setSubTitleText();
	}

	public void setSubtitleBottom(String subtitle){
		this.subtitleBottom = subtitle;
		setSubTitleBottomText();
	}

	public void setCheckable(boolean checkable)
	{
		this.checkable = checkable;
		if(!checkable)
		{
			if(null!=switchView)switchView.setVisibility(GONE);
			if(null!=checkButton)checkButton.setVisibility(GONE);
		}else{
			if(isSwitch && null!=switchView){
				switchView.setVisibility(VISIBLE);
				switchView.setOnClickListener((v) -> {
					if (null != onCheckedChangeListener) {
						switchCheck();
						onCheckedChangeListener.onCheckedChanged();
					}
				});
			}else
			{
				if(null!=checkButton)
				{
					if (0 != checkImage) checkButton.setImageDrawable(getResources().getDrawable(checkImage));
					checkButton.setVisibility(VISIBLE);
					checkButton.setOnClickListener(new OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							if (null != onCheckedChangeListener) onCheckedChangeListener.onCheckedChanged();
						}
					});
				}
			}
		}
	}

	private void switchCheck()
	{
		setChecked(!checked);
	}

	@Override
	public void setChecked(boolean checked)
	{
		if(checkable){
			this.checked = checked;
			if(checked){
				setSelected(true);
			}else{
				setSelected(false);
			}
			refreshDrawableState();
		}
	}

	@Override
	public boolean isChecked()
	{
		return checked;
	}

	@Override
	public void toggle()
	{
		setChecked(!checked);
	}

}


