package biz.teko.td.SHUB_APP.Profile.Security;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 29.08.2017.
 */

public class SecurityPinSetActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private int from;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_security_pin_set);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		from = getIntent().getIntExtra("FROM", -1);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.SPSA_TITLE);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});

		getFragmentManager().beginTransaction().replace(R.id.prefContainer, new  SecurityPinSetFragment()).commit();
	}

	@Override
	public void onBackPressed()
	{
		switch (from){
			case 0:
				AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
				adb.setTitle(getString(R.string.TITLE_ATTENTION));
				adb.setMessage(R.string.SPSA_PIN_ENTER_DIALOG_MESS);
				adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
				adb.setPositiveButton(R.string.ADB_CONTINUE, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						finish();
					}
				});
				adb.show();
				break;
			case 1:
				adb = Func.adbForCurrentSDK(context);
				adb.setTitle(getString(R.string.TITLE_ATTENTION));
				adb.setMessage(R.string.SPSA_PIN_ENTER_GOBACK_DIALOG_1);
				adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
				adb.setPositiveButton(R.string.ADB_CONTINUE, new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
						finish();
					}
				});
				adb.show();
				break;
			default:
				finish();
				break;
		}


	}
}
