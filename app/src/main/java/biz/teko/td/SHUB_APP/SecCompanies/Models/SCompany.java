package biz.teko.td.SHUB_APP.SecCompanies.Models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementArray;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

/**
 * Created by td13017 on 14.03.2018.
 */

public class SCompany
{
	@Attribute(name="caption")
	public String caption;

	@Attribute(name="title", required = false)
	public String title;

	@Attribute(name="logo")
	public String logo;

	@Attribute(name="message", required = false)
	public String message;

	@ElementList(name = "departs", entry = "depart", required = false)
	public LinkedList<SCDepart> departs;

	@ElementList(name = "rcs", entry = "rc", required = false)
	public LinkedList<SCRc> scrcs;

	@ElementArray(required = false)
	public String[] addresses;

	@ElementArray(required = false)
	public String[] phones;

	@ElementArray(required = false)
	public String[] emails;

	@ElementArray(required = false)
	public String[] sites;

	@ElementList(name = "cphones", entry = "cphone", required = false)
	public LinkedList<CPhone> cphones;
}
