package biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Delay {
    private List<Integer> values = new ArrayList<>();

    public Delay() {
        values.addAll(Arrays.asList(20, 30, 45, 60, 90, 120));
    }

    public List<Integer> getValues() {
        return values;
    }
}
