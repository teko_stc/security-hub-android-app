package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import static biz.teko.td.SHUB_APP.D3DB.MainBar.SubType.SITE;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.SectionGroup;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.MainTabs.Activities.MainSetActivity;
import biz.teko.td.SHUB_APP.MainTabs.Adapters.MainSetElementRecyclerAdapter;
import biz.teko.td.SHUB_APP.MainTabs.Adapters.MainSetPagerAdapter;
import biz.teko.td.SHUB_APP.MainTabs.Adapters.SectionInGroupSelectRecyclerAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.NPresetRadioGroup;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.NPresetValueButton;
import biz.teko.td.SHUB_APP.Wizards.Activity.WizardAllActivity;

public class MainSetFragment extends Fragment {
	private int site_id;
	private boolean shared;
	private Context context;
	private MainSetActivity activity;
	private DBHelper dbHelper;
	private OnSelectListener onSelectListener;
	private SectionInGroupSelectRecyclerAdapter adapter;
	private RecyclerView.Adapter<?> otherAdapter;

	public Site getSectionSite() {
		if (adapter != null)
			return adapter.getCurSite();
		else return null;
	}

	public void setSectionSite(Site sectionSite) {
		if (adapter != null)

			adapter.setCurSite(sectionSite);
		else
			activity.setSectionGroup(null);
	}

	public MainSetFragment(boolean shared) {
		this.shared = shared;
	}


	public static MainSetFragment newInstance(int position, int site_id, boolean shared) {
		MainSetFragment mainSetFragment = new MainSetFragment(shared);
		Bundle b = new Bundle();
		b.putInt("position", position);
		b.putInt("site_id", site_id);
		b.putBoolean("shared", shared);
		mainSetFragment.setArguments(b);
		return mainSetFragment;
	}

	@Override
	public void onResume() {
		super.onResume();
		setSectionSite(null);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		context = container.getContext();

		activity = (MainSetActivity) context;
		dbHelper = DBHelper.getInstance(context);
		View view = inflater.inflate(R.layout.fragment_main_set, container, false);
		site_id = getArguments().getInt("site_id");
		shared = getArguments().getBoolean("shared");
		return getCustomView(view, getArguments().getInt("position"));
	}

	private View getCustomView(View view, int position) {
		FrameLayout customFrame = view.findViewById(R.id.customFrame);
		switch (position) {
			case 0:
				setFragmentLevelOne(customFrame, position);
				break;
			case 1:
				setFragmentLevelTwo(customFrame, position, view);
				break;
			case 2:
				setFragmentLevelThree(customFrame, position, view);
				break;
			case 3:
				setFragmentLevelFour(customFrame, position);
				break;
		}
		if (position == 3)
			onSelectListener.updateState();
		return view;
	}

	private void setFragmentLevelOne(FrameLayout view, int position)
	{
		NPresetRadioGroup presetRadioGroup = new NPresetRadioGroup(context);
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		presetRadioGroup.setLayoutParams(layoutParams);
		presetRadioGroup.setOrientation(LinearLayout.VERTICAL);

		MainBar.Type currentType = activity.getType();
		int i = 0;
		for(MainBar.Type type: MainBar.Type.values()){
			if(type != MainBar.Type.EMPTY)
			{
				NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_checkable_layout);
				presetValueButton.setPosition(i);
				i++;
				presetValueButton.setTitle(type.toString(context));
				presetValueButton.setSubTitle(type.toString(context));
				presetValueButton.setBarType(type);
				presetValueButton.setImage(type.getIconSmall());
				presetValueButton.setAnim(false);
				presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
				presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
				presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
				presetValueButton.bindView();
				presetValueButton.setOnClickListener(view1 -> {
					activity.setType(type);
					onSelectListener.callback(position, site_id);
				});
				presetRadioGroup.addView(presetValueButton);
			}
			if(null!=currentType)
			{
				for(int ii = 0; ii  < presetRadioGroup.getChildCount(); ii++){
					NPresetValueButton presetValueButton1 = (NPresetValueButton) presetRadioGroup.getChildAt(ii);
					if(presetValueButton1.getBarType() == currentType){
						presetValueButton1.setChecked(true);
					}
				}
			}
		}
		view.addView(presetRadioGroup);
		return;
	}

	private View setFragmentLevelTwo(FrameLayout view, int position, View parent) {

		NPresetRadioGroup presetRadioGroup = new NPresetRadioGroup(context);
		FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		presetRadioGroup.setLayoutParams(layoutParams);
		presetRadioGroup.setOrientation(LinearLayout.VERTICAL);

		MainBar.Type currentType = activity.getType();
		if (null != currentType)
			switch (currentType) {
			case ARM:
				int i = 0;
				for (MainBar.SubType subType : MainBar.SubType.values()) {

					NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_main_set_element);
					presetValueButton.setPosition(i);
					i++;
					presetValueButton.setSubType(subType);
					presetValueButton.setTitle(subType.toString(context));
					presetValueButton.setSubTitle(subType.toString(context));

					presetValueButton.setImage(subType.getIconSmall());
					presetValueButton.setAnim(false);
					presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
					presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
					presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
					presetValueButton.bindView();
					presetValueButton.setOnClickListener(view1 -> {
						activity.setSubType(subType);
						if (!shared && subType == SITE)
							activity.setElementId(Integer.toString(site_id));
						onSelectListener.callback(position, site_id);
					});
					presetRadioGroup.addView(presetValueButton);
				}
				MainBar.SubType subType = activity.getSubType();
				if(null!=subType)
				{
					for(int ii = 0; ii  < presetRadioGroup.getChildCount(); ii++){
						NPresetValueButton presetValueButton = (NPresetValueButton) presetRadioGroup.getChildAt(ii);
						if(presetValueButton.getSubType() == subType){
							presetValueButton.setChecked(true);
						}
					}
				}
				break;
			case CONTROL:
			case DEVICE:
				Zone[] zones;
				if(currentType == MainBar.Type.CONTROL) {
					if (!shared)
						zones = dbHelper.getAllRelaysForSite(site_id);
					else
						zones = dbHelper.getAllRelays();
				} else {
					if (!shared)
						zones = dbHelper.getAllZonesForSite(site_id);
					else
						zones = dbHelper.getAllZonesArray();
				}
				if(null!=zones) {
					if (shared) {
						otherAdapter =
								new MainSetElementRecyclerAdapter<Zone>(zones, (zone, pos, holder) -> {
									Site site = dbHelper.getSiteByDeviceId(zone.device_id);
									if(null!=site) {
										NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_main_set_element);
										presetValueButton.setPosition(pos);

										presetValueButton.setTitle(zone.name);
										int image = getImage(dbHelper.getDeviceById(zone.device_id), zone);
										presetValueButton.setImage(0 != image ? image : currentType.getIconSmall());
										presetValueButton.setElementId(generateElementId(zone));
										presetValueButton.setChecked(presetValueButton.getElementId().equals(activity.getElementId()));
										presetValueButton.setAnim(false);
										TextView siteTv = presetValueButton.findViewById(R.id.text_view_site);
										siteTv.setTextColor(getResources().getColorStateList(R.color.n_site_select_text_color_selector));
										siteTv.setVisibility(View.VISIBLE);
										siteTv.setText(dbHelper.getSiteByDeviceId(zone.device_id).name);
										presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
										presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
										presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
										presetValueButton.bindView();
										presetValueButton.setOnClickListener(view12 -> {
											String element_id = generateElementId(zone);
											if (null != element_id) {

												activity.setElementId(element_id);
												onSelectListener.updateState();
												onSelectListener.callback(position, dbHelper.getSiteByDeviceId(zone.device_id).id);
												otherAdapter.notifyDataSetChanged();

											}
										});
										((ViewGroup) holder.itemView).removeAllViews();
										((ViewGroup) holder.itemView).addView(presetValueButton);
									}
								});
						replaceScrollViewWithRecycler(parent, otherAdapter);
					} else {
						i = 0;
						for (Zone zone : zones) {
							NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_main_set_element);
							presetValueButton.setPosition(i);
							i++;
							presetValueButton.setTitle(zone.name);
							int image = getImage(dbHelper.getDeviceById(zone.device_id), zone);
							presetValueButton.setImage(0 != image ? image : currentType.getIconSmall());
							presetValueButton.setElementId(generateElementId(zone));
							presetValueButton.setAnim(false);
							TextView siteTv = presetValueButton.findViewById(R.id.text_view_site);
							if (shared) {
								siteTv.setTextColor(getResources().getColorStateList(R.color.n_site_select_text_color_selector));
								siteTv.setVisibility(View.VISIBLE);
								siteTv.setText(dbHelper.getSiteByDeviceId(zone.device_id).name);
							}
							presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
							presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
							presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
							presetValueButton.bindView();
							presetValueButton.setOnClickListener(view12 -> {
								String element_id = generateElementId(zone);
								if (null != element_id) {
									activity.setElementId(element_id);

									onSelectListener.callback(position, !shared ? site_id : dbHelper.getSiteByDeviceId(zone.device_id).id);
								}
							});
							presetRadioGroup.addView(presetValueButton);
						}
						String element_id = activity.getElementId();
						if (null != element_id && !element_id.equals("")) {
							for (int ii = 0; ii < presetRadioGroup.getChildCount(); ii++) {
								NPresetValueButton presetValueButton = (NPresetValueButton) presetRadioGroup.getChildAt(ii);
								if (presetValueButton.getElementId().equals(element_id)) {
									presetValueButton.setChecked(true);
								}
							}
						}
					}
				}else{
					/*TODO
					  show empty banner*/
				}
				break;
			case CAMERA:

				Camera[] cameras = dbHelper.getAllCameras();
				if(null!=cameras && cameras.length > 0){
					i = 0;
					for(Camera camera:cameras){
						NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_checkable_layout);
						presetValueButton.setPosition(i);
						i++;
						presetValueButton.setTitle(camera.name);
						presetValueButton.setImage(currentType.getIconSmall());
						presetValueButton.setElementId(camera.id);
						presetValueButton.setAnim(false);
						presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
						presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
						presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
						presetValueButton.bindView();
						presetValueButton.setOnClickListener(new View.OnClickListener()
						{
							@Override
							public void onClick(View view)
							{
								String element_id = generateElementId(camera);
								if(null!=element_id)
								{
									activity.setElementId(element_id);
									onSelectListener.callback(position, site_id);
								}
							}
						});
						presetRadioGroup.addView(presetValueButton);
					}
					String element_id = activity.getElementId();
					if(null!=element_id && !element_id.equals(""))
					{
						for(int ii = 0; ii  < presetRadioGroup.getChildCount(); ii++){
							NPresetValueButton presetValueButton = (NPresetValueButton) presetRadioGroup.getChildAt(ii);
							if(presetValueButton.getElementId().equals(element_id)){
								presetValueButton.setChecked(true);
							}
						}
					}
				}
				break;
			}
		view.addView(presetRadioGroup.getChildCount() > 0 ? presetRadioGroup : getEmptyView(currentType));
		return view;
	}

	private View setFragmentLevelThree(FrameLayout view, int position, View parent) {
		MainBar.SubType currentType = activity.getSubType();
		if (null != currentType) {
			Section[] sections;
			switch (currentType) {
				case SITE:
					if (shared) {
						NPresetRadioGroup presetRadioGroup = new NPresetRadioGroup(context);
						FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						presetRadioGroup.setLayoutParams(layoutParams);
						presetRadioGroup.setOrientation(LinearLayout.VERTICAL);
						Site[] sites = dbHelper.getAllSitesArray();

						if (null != sites && sites.length > 0) {
							int i = 0;
							for (Site site : sites) {
								NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_main_set_element);
								presetValueButton.setPosition(i);
								i++;
								presetValueButton.setTitle(site.name);
								presetValueButton.setSubTitle(getResources().getString(R.string.N_MAIN_TITLE_FULL_SITE));
								presetValueButton.setElementId(Integer.toString(site.id));
								presetValueButton.setImage(R.drawable.n_image_main_subtype_site_list_selector);
								presetValueButton.setAnim(false);
								presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
								presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
								presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
								presetValueButton.bindView();
								presetValueButton.setOnClickListener(view12 -> {
									String element_id = "" + site.id;
									if (null != element_id) {
										activity.setElementId(element_id);
										onSelectListener.callback(position, site.id);
									}
								});
								presetRadioGroup.addView(presetValueButton);
							}
							String element_id = activity.getElementId();
							if (null != element_id && !element_id.equals("")) {
								for (int ii = 0; ii < presetRadioGroup.getChildCount(); ii++) {
									NPresetValueButton presetValueButton = (NPresetValueButton) presetRadioGroup.getChildAt(ii);
									if (presetValueButton.getElementId().equals(element_id)) {
										presetValueButton.setChecked(true);
									}
								}
							}
						}
						view.addView(presetRadioGroup.getChildCount() > 0 ? presetRadioGroup : getEmptyView(activity.getType()));
						break;
					}
				case DEVICE:
					if (shared) {
						Device[] devices = dbHelper.getAllDevices();
						otherAdapter = new MainSetElementRecyclerAdapter<Device>(devices, (device, pos, holder) -> {
							NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_main_set_element);
							presetValueButton.setPosition(pos);
							DeviceType deviceType = device.getType();
							if (null != deviceType) {
								presetValueButton.setTitle(deviceType.caption);
								int image = deviceType.getListIcon(context);
								presetValueButton.setImage(0 != image ? image : R.drawable.n_image_astra_common_list_selector);
							}
							TextView siteTv = presetValueButton.findViewById(R.id.text_view_site);
							if (shared) {

								siteTv.setTextColor(getResources().getColorStateList(R.color.n_site_select_text_color_selector));
								siteTv.setVisibility(View.VISIBLE);
								siteTv.setText(dbHelper.getSiteByDeviceId(device.id).name);
							}
							presetValueButton.setElementId(Integer.toString(device.id));
							presetValueButton.setChecked(presetValueButton.getElementId().equals(activity.getElementId()));
							presetValueButton.setAnim(false);
							presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
							presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
							presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
							presetValueButton.bindView();
							presetValueButton.setOnClickListener(view1 -> {
								activity.setElementId(Integer.toString(device.id));
								onSelectListener.callback(position, !shared ? site_id : dbHelper.getSiteByDeviceId(device.id).id);

								onSelectListener.updateState();
								otherAdapter.notifyDataSetChanged();

							});

							((ViewGroup) holder.itemView).removeAllViews();
							((ViewGroup) holder.itemView).addView(presetValueButton);
						});
						replaceScrollViewWithRecycler(parent, otherAdapter);
					} else {
						NPresetRadioGroup presetRadioGroup = new NPresetRadioGroup(context);
						FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						presetRadioGroup.setLayoutParams(layoutParams);
						presetRadioGroup.setOrientation(LinearLayout.VERTICAL);
						Device[] devices = dbHelper.getAllDevicesForSite(site_id);

						if (null != devices && devices.length > 0) {
							int i = 0;
							for (Device device : devices) {
								NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_main_set_element);
								presetValueButton.setPosition(i);
								i++;
								DeviceType deviceType = device.getType();
								if (null != deviceType) {
									presetValueButton.setTitle(deviceType.caption);
									int image = deviceType.getListIcon(context);
									presetValueButton.setImage(0 != image ? image : R.drawable.n_image_astra_common_list_selector);
								}
								TextView siteTv = presetValueButton.findViewById(R.id.text_view_site);
								if (shared) {

									siteTv.setTextColor(getResources().getColorStateList(R.color.n_site_select_text_color_selector));
									siteTv.setVisibility(View.VISIBLE);
									siteTv.setText(dbHelper.getSiteByDeviceId(device.id).name);
								}
								presetValueButton.setElementId(Integer.toString(device.id));
								presetValueButton.setAnim(false);
								presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
								presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
								presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
								presetValueButton.bindView();
								presetValueButton.setOnClickListener(view1 -> {
									activity.setElementId(Integer.toString(device.id));
									onSelectListener.callback(position, !shared ? site_id : dbHelper.getSiteByDeviceId(device.id).id);
								});
								presetRadioGroup.addView(presetValueButton);
							}
							String element_id = activity.getElementId();
							if (null != element_id && !element_id.equals("")) {
								for (int ii = 0; ii < presetRadioGroup.getChildCount(); ii++) {
									NPresetValueButton presetValueButton = (NPresetValueButton) presetRadioGroup.getChildAt(ii);
									if (presetValueButton.getElementId().equals(element_id)) {
										presetValueButton.setChecked(true);
									}
								}
							}
						}
						view.addView(presetRadioGroup.getChildCount() > 0 ? presetRadioGroup : getEmptyView(activity.getType()));
					}
					break;
				case GROUP:
					SectionGroup sectionGroup = activity.getSectionGroup();
					List<Site> sites = new ArrayList<>();
					if (!shared)
						sections = dbHelper.getGuardSectionsForSite(site_id);
					else {
						sections = dbHelper.getGuardSections();
						for (Section section : sections) {
							sites.add(dbHelper.getSiteByDeviceId(section.device_id));
						}
					}
					if (null != sections) {
						if (null != sectionGroup.sections && sectionGroup.sections.length > 0) {
							Section[] s = sectionGroup.sections;
							for (Section sSection : s) {
								for (Section section : sections) {
									if (sSection.checked && sSection.id == section.id && sSection.device_id == section.device_id) {
										section.checked = true;
									}
								}
							}
						}

						adapter = new SectionInGroupSelectRecyclerAdapter(context, sections, (localSections, element, pos) -> {
							if (shared) {
								if (localSections[pos].checked) {
									if (getSectionSite() == null)
										setSectionSite(sites.get(pos));
									else {
										if (sites.get(pos).id != getSectionSite().id) {
											localSections[pos].checked = false;
											((NPresetValueButton) element).setChecked(false);
										}
									}
								}
							}
							LinkedList<Section> sectionList = new LinkedList<>();
							for (Section section : localSections) {

								if (section.checked) sectionList.add(section);
							}
							Section[] checkedSections = new Section[sectionList.size()];
							sectionList.toArray(checkedSections);
							sectionGroup.sections = checkedSections;

							activity.setSectionGroup(sectionGroup);
							if (sectionList.isEmpty()) {
								setSectionSite(null);
								onSelectListener.callback(-1, site_id);
							} else
								onSelectListener.callback(position, site_id);
							adapter.notifyDataSetChanged();
						}, shared, sites);
						replaceScrollViewWithRecycler(parent, adapter);
					}
					break;
				case SECTION:
					if (shared) {
						sections = dbHelper.getSections();
						otherAdapter = new MainSetElementRecyclerAdapter<Section>(sections, (section, pos, holder) -> {
							NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_main_set_element);
							presetValueButton.setPosition(pos);
							presetValueButton.setTitle(section.name);
							SType sType = section.getType();
							presetValueButton.setImage(null != sType ?
									sType.getListIcon(context)
									: R.drawable.n_image_section_common_list_selector);
							TextView siteTv = presetValueButton.findViewById(R.id.text_view_site);
							siteTv.setTextColor(getResources().getColorStateList(R.color.n_site_select_text_color_selector));
							siteTv.setVisibility(View.VISIBLE);
							siteTv.setText(dbHelper.getSiteByDeviceId(section.device_id).name);

							presetValueButton.setSubTitle(sType != null ? sType.caption : "");
							presetValueButton.setElementId(generateElementId(section));
							presetValueButton.setChecked(presetValueButton.getElementId().equals(activity.getElementId()));
							presetValueButton.setAnim(false);
							presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
							presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
							presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
							presetValueButton.bindView();
							presetValueButton.setOnClickListener(view12 -> {
								String element_id = generateElementId(section);
								if (null != element_id) {
									activity.setElementId(element_id);
									onSelectListener.callback(position, dbHelper.getSiteByDeviceId(section.device_id).id);
									onSelectListener.updateState();
									otherAdapter.notifyDataSetChanged();
								}
							});
							((ViewGroup) holder.itemView).removeAllViews();
							((ViewGroup) holder.itemView).addView(presetValueButton);
						});
						replaceScrollViewWithRecycler(parent, otherAdapter);
					} else {

						NPresetRadioGroup nPresetRadioGroup = new NPresetRadioGroup(context);
						FrameLayout.LayoutParams layoutParams1 = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
						nPresetRadioGroup.setLayoutParams(layoutParams1);
						nPresetRadioGroup.setOrientation(LinearLayout.VERTICAL);

						sections = dbHelper.getSectionsForSite(site_id);


						if (null != sections && sections.length > 0) {
							int i = 0;
							for (Section section : sections) {
								NPresetValueButton presetValueButton = new NPresetValueButton(context, R.layout.n_main_set_element);
								presetValueButton.setPosition(i);
								i++;
								presetValueButton.setTitle(section.name);
								SType sType = section.getType();
								presetValueButton.setImage(null != sType ?
										sType.getListIcon(context)
										: R.drawable.n_image_section_common_list_selector);
								TextView siteTv = presetValueButton.findViewById(R.id.text_view_site);
								if (shared) {
									siteTv.setTextColor(getResources().getColorStateList(R.color.n_site_select_text_color_selector));
									siteTv.setVisibility(View.VISIBLE);
									siteTv.setText(dbHelper.getSiteByDeviceId(section.device_id).name);
								}
								presetValueButton.setSubTitle(sType != null ? sType.caption : "");
								presetValueButton.setElementId(generateElementId(section));
								presetValueButton.setAnim(false);
								presetValueButton.setTitleTextColor(R.color.n_site_select_text_color_selector);
								presetValueButton.setSubTitleTextColor(R.color.n_site_select_text_color_selector);
								presetValueButton.setBackground(getResources().getDrawable(R.drawable.n_background_selector_preset_button));
								presetValueButton.bindView();
								presetValueButton.setOnClickListener(view12 -> {
									String element_id = generateElementId(section);
									if (null != element_id) {
										activity.setElementId(element_id);
										onSelectListener.callback(position, !shared ? site_id : dbHelper.getSiteByDeviceId(section.device_id).id);
									}
								});
								nPresetRadioGroup.addView(presetValueButton);
							}
							String element_id = activity.getElementId();
							if (null != element_id && !element_id.equals("")) {
								for (int ii = 0; ii < nPresetRadioGroup.getChildCount(); ii++) {
									NPresetValueButton presetValueButton = (NPresetValueButton) nPresetRadioGroup.getChildAt(ii);
									if (presetValueButton.getElementId().equals(element_id)) {
										presetValueButton.setChecked(true);
									}
								}
							}
						}
						view.addView(nPresetRadioGroup.getChildCount() > 0 ? nPresetRadioGroup : getEmptyView(activity.getType()));
					}
					break;
				default:
					break;
			}
		}
		return view;
	}
	private View setFragmentLevelFour(FrameLayout view, int position){
		/*sectiongroup name*/
		SectionGroup sectionGroup = activity.getSectionGroup();
		if(null!=sectionGroup){
			TextInputLayout editNameTitle = (TextInputLayout) getLayoutInflater().inflate(R.layout.n_edittext_template,null);
			NEditText editName = (NEditText) editNameTitle.findViewById(R.id.nText);
			if(null!=editName){
				if(null!=sectionGroup.name)
				{
					editName.setText(sectionGroup.name);
					onSelectListener.callback(position, site_id);
				}else{
					editName.setText("");
				}
				editName.addTextChangedListener(new TextWatcher()
				{
					@Override
					public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
					{

					}

					@Override
					public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
					{

					}

					@Override
					public void afterTextChanged(Editable editable)
					{
						sectionGroup.name = editName.getText().toString();
						activity.setSectionGroup(sectionGroup);
						onSelectListener.callback(position, site_id);
					}
				});
				editName.setSaveEnabled(false);
				editName.setOnEditorActionListener(new TextView.OnEditorActionListener()
				{
					@Override
					public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent)
					{
						if(i == EditorInfo.IME_ACTION_DONE){
							onSelectListener.doneCallback(position);
						}
						return false;
					}
				});
				editName.setSelection(editName.getText().length());

			}
			if(activity.getCurrentItem() == MainSetPagerAdapter.MAIN_SET_PAGER_COUNT - 1){
				editName.showKeyboard();
			}

			editNameTitle.setHintEnabled(true);
			editNameTitle.setHint(getString(R.string.N_GROUP_NAME));

			view.addView(editNameTitle);
		}
		return view;
	}


	private int getImage(Device device, Zone zone) {
		int image = 0;
		if ((zone.id == 0 || zone.id == 100) && zone.section_id == 0) {
			image = R.drawable.n_image_controller_common_list_selector;
			if (null != device) {
				DeviceType deviceType = device.getType();
				if (null != deviceType) {
					if (null != deviceType.icons) {
						image = deviceType.getListIcon(context);
					}
				}
			}
		} else {
			image = R.drawable.n_image_device_common_list_selector; // TODO set common image for sensor
			switch (zone.getDetector()) {
				case Const.DETECTOR_AUTO_RELAY:
				case Const.DETECTOR_MANUAL_RELAY:
					image = R.drawable.n_image_relay_list_selector;
					break;
				case Const.DETECTOR_SIREN:
					image = R.drawable.n_image_siren_list_selector;
					break;
				case Const.DETECTOR_LAMP:
					image = R.drawable.n_image_lamp_list_selector;
					break;
				case Const.DETECTOR_KEYPAD:
				case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
					image = R.drawable.n_image_keypad_list_selector;
					break;
				default:
					break;
			}
			int model = zone.getModelId();
			switch (model) {
				case Const.SENSORTYPE_WIRED_OUTPUT:
					DWOType dwoType = zone.getDWOType();
					if (null != dwoType && null != dwoType.icons) {
						image = dwoType.getListIcon(context);
					} else if (zone.detector != Const.DETECTOR_AUTO_RELAY && zone.detector != Const.DETECTOR_MANUAL_RELAY) {
						image = R.drawable.n_image_wired_output_common_list_selector;
					}
					break;
				case Const.SENSORTYPE_WIRED_INPUT:
					DWIType dwiType = zone.getDWIType();
					if (null != dwiType && null != dwiType.icons) {
						image = dwiType.getListIcon(context);
						DType dType = dwiType.getDetectorType(zone.getDetector());
						if (null != dType) {
							AType aType = dType.getAlarmType(zone.getAlarm());
							if (null != aType && null != aType.icons) {
								image = aType.getListIcon(context);
							}
						}
					} else {
						image = R.drawable.n_image_wired_input_common_list_selector;
					}
				case Const.SENSORTYPE_UNKNOWN:
					break;
				default:
					ZType zType = zone.getModel();
					if (null != zType && null != zType.icons) {
						image = zType.getListIcon(context);
						DType dType = zType.getDetectorType(zone.getDetector());
						if (null != dType) {
							AType aType = dType.getAlarmType(zone.getAlarm());
							if (null != aType && null != aType.icons) {
								image = aType.getListIcon(context);
							}
						}
					} else {
						image = R.drawable.n_image_device_common_list_selector;
					}
					break;
			}
		}

		return image;
//		return 0!=image ? context.getResources().getDrawable(image) : null;
	}

	private View getEmptyView(MainBar.Type currentType) {
		NWithAButtonElement view = new NWithAButtonElement(context, R.layout.n_empty_element_list);
		String title = getString(R.string.N_MAIN_SET_NO_AVILABLE_ELEMENTS);
		String buttonTitle = getString(R.string.N_GOTO);
		Intent intent = null;
		if (null != currentType) {
			switch (currentType) {
				case ARM:
					if (activity.getSubType() == MainBar.SubType.DEVICE) {
						title = getString(R.string.N_MAINSET_NO_CONTROLLERS);
						buttonTitle = getString(R.string.N_MAINSET_ADD);
						intent = new Intent(context, WizardAllActivity.class);
					} else if ((activity.getSubType() == MainBar.SubType.SECTION) || (activity.getSubType() == MainBar.SubType.GROUP)) {
						title = getString(R.string.N_MAINSET_NO_SECTIONS);
						buttonTitle = getString(R.string.N_MAINSET_SET);
						intent = new Intent(context, WizardAllActivity.class);
					}
					break;
				case DEVICE:
					title = getString(R.string.N_MAINSET_NO_SENSORS);
					buttonTitle = getString(R.string.N_MAINSET_ADD);
//					intent = new Intent(context, WizardZoneSetActivity.class);
					intent = new Intent(context, WizardAllActivity.class);
					break;
				case CONTROL:
					title = getString(R.string.N_MAINSET_NO_RELAYS);
					buttonTitle = getString(R.string.N_MAINSET_ADD);
//					intent = new Intent(context, WizardRelaySetActivity.class);
					intent = new Intent(context, WizardAllActivity.class);
					break;
				case CAMERA:
					title = getString(R.string.N_MAINSET_NO_CAMERAS);
					buttonTitle = getString(R.string.N_MAINSET_ADD);
					intent = new Intent(context, WizardAllActivity.class);
					break;
			}
		}
		view.setTitle(title);
		view.setActionTitle(buttonTitle);
		view.setActionTextColor(R.color.brandColorWhite);
		view.setActionTextSize(25);
		view.setActionIncrease(false);
		Intent finalIntent = intent;
		view.setOnChildClickListener(new NWithAButtonElement.OnChildClickListener() {
			@Override
			public void onChildClick(int action, boolean option) {
				if (null != finalIntent) {
					startActivity(finalIntent);
					if (null != activity) activity.finish();
				}
			}
		});
		return view;
	}

	private void replaceScrollViewWithRecycler(View parent, RecyclerView.Adapter<?> adapter) {
		FrameLayout fl = (FrameLayout) parent.findViewById(R.id.fl_main_set_frame);
		ViewGroup.LayoutParams lp = fl.findViewById(R.id.sv_main_set_cont).getLayoutParams();
		RecyclerView sectionsSelectList = new RecyclerView(context);
		sectionsSelectList.setLayoutParams(lp);
		LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
		layoutManager.setSmoothScrollbarEnabled(true);
		sectionsSelectList.setHasFixedSize(true);

		sectionsSelectList.setLayoutManager(layoutManager);
		sectionsSelectList.setVerticalScrollBarEnabled(true);
		sectionsSelectList.setVerticalFadingEdgeEnabled(true);
		sectionsSelectList.setFadingEdgeLength(Func.dpToPx(44, context));
		sectionsSelectList.setAdapter(adapter);
		fl.removeAllViews();
		(fl).addView(adapter.getItemCount() > 0 ? sectionsSelectList : getEmptyView(activity.getType()));
	}

	private String generateElementId(Section section) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("device", section.device_id);
			jsonObject.put("section", section.id);
			return jsonObject.toString();
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}

	}

	private String generateElementId(Zone zone)
	{
		JSONObject jsonObject  =new JSONObject();
		try
		{
			jsonObject.put("device", zone.device_id);
			jsonObject.put("section", zone.section_id);
			jsonObject.put("zone", zone.id);
			return jsonObject.toString();
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	private String generateElementId(Camera camera)
	{
		JSONObject jsonObject  =new JSONObject();
		try
		{
			jsonObject.put("type", camera.type.toString());
			jsonObject.put("id", camera.type == Camera.CType.IV? camera.id : (camera.type == Camera.CType.DAHUA ? camera.sn : camera.local_id));
			return jsonObject.toString();
		} catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public interface OnSelectListener{
		void callback(int position, int site_id);

		void updateState();

		void doneCallback(int position);
	};

	public void setSelectListener(OnSelectListener onSelectListener){
		this.onSelectListener = onSelectListener;
	}
}
