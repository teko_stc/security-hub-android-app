package biz.teko.td.SHUB_APP.Profile.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import biz.teko.td.AppRater;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NRateFrame;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 13.02.2017.
 */

public class NAboutActivity extends BaseActivity {
	private Context context;
	private ImageView logo;
	private int clickCount = 0;
	private LinearLayout close;
	private TextView title;
	private TextView version;
	private TextView ownerSite;
	private TextView emailSupport;
	private TextView titleSupport;
	private NRateFrame rateFrame;
	private TextView bottomText;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_about);
		context  = this;

		setup();
		bind();
	}

	private void bind()
	{
		if(null!=close) close.setOnClickListener(v->{onBackPressed();});
		if(null!=title) title.setText(getResources().getString(R.string.AA_TITLE));
//		if(null!=logo) logo.setOnClickListener(v -> { setClickLogoReview(); });
		if(null!=version) {
			try
			{
				PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
				if(null!=info) version.setText(getString(R.string.ABOUT_VERSION) + " " + info.versionName);
			} catch (PackageManager.NameNotFoundException e)
			{
				e.printStackTrace();
			}
		}
		if(null!=ownerSite) ownerSite.setOnClickListener(v -> {
			switch (context.getPackageName()){
				case "ru.sh.teleplus":
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https:\\" + getString(R.string.web_site))));
					break;
				default:
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.ABOUT_WEBSITE_LONG))));
					break;
			}
		});
		if(null!=emailSupport) emailSupport.setOnClickListener(v -> {
			Intent intent = new Intent(Intent.ACTION_VIEW);
			Uri data = Uri.parse("mailto:?to=" + getResources().getString(R.string.SUPPORT_EMAIL));
			intent.setData(data);
			startActivity(intent);
		});

		if(!Func.tekoBuild()){
			if(null!=emailSupport) emailSupport.setVisibility(View.GONE);
			if(null!=logo) logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_name));
			if(null!=titleSupport) titleSupport.setVisibility(View.GONE);
			if(null!=bottomText) bottomText.setVisibility(View.GONE);
		}else{
			if(null!=logo) logo.setImageDrawable(getResources().getDrawable(R.drawable.ic_logo_sh));
		}

		if(null!=rateFrame){
			if(!PrefUtils.getInstance(context).isBeenRated()){
				rateFrame.setVisibility(View.VISIBLE);
				rateFrame.setOnRateClickListener(new NRateFrame.OnRateClickListener()
				{
					@Override
					public void onRateClick(int value)
					{
						rateFrame.setRating(value);
						AppRater.review((Activity) context, new AppRater.OnRateResultListener()
						{
							@Override
							public void onResult()
							{
								bind();
								PrefUtils.getInstance(context).setRated();
								Func.pushToast(context, getResources().getString(R.string.THANK_FOR_REVIEW), (NAboutActivity) context);
							}
						});
					}

					@Override
					public void onBack()
					{
						onBackPressed();
					}
				});
			}else{
				rateFrame.setVisibility(View.GONE);
			}
		}
	}

	private void setup()
	{
		close = (LinearLayout) findViewById(R.id.nButtonBack);
		title = (TextView) findViewById(R.id.nTextTitle);
		logo = (ImageView) findViewById(R.id.nImageLogo);
		version = (TextView) findViewById(R.id.nTextVersionTitle);
		ownerSite = (TextView) findViewById(R.id.nTextWebSite);
		bottomText = (TextView) findViewById(R.id.nTextBottom);
		emailSupport = (TextView) findViewById(R.id.nTextSupportEmail);
		titleSupport = (TextView) findViewById(R.id.nTextSupportTitle);
		rateFrame = (NRateFrame) findViewById(R.id.nFrameRate);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}
}
