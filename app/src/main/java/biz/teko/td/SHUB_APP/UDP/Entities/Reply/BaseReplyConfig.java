package biz.teko.td.SHUB_APP.UDP.Entities.Reply;

import android.os.Bundle;
import android.util.Log;

import java.util.Arrays;

import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.ArmWaitReviewed;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.ArmWaitServer;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.DisarmWaitServer;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.RadioLiter;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.RoamingEnabled;
import biz.teko.td.SHUB_APP.UDP.LocalConnection;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;

public class BaseReplyConfig {
    private static final String INTERNET = "Internet";
    protected byte[] data;
    protected byte[] flags;

    public static String getAPNServerValue(String server) {
        return (server.equals("") ? INTERNET : server);
    }

    protected void setBooleanConfig(Bundle bundle, String configType) {
        switch (configType) {
            case ConfigConstants.CONFIG_RADIO_LITER:
                new RadioLiter().set(this.flags, bundle.getString(ConfigConstants.CONFIG_RADIO_LITER)); break;
            case ConfigConstants.CONFIG_WAIT_REVIEWED:
                new ArmWaitReviewed().set(this.flags, bundle.getString(ConfigConstants.CONFIG_WAIT_REVIEWED)); break;
            case ConfigConstants.CONFIG_ARM_SERVER:
                new ArmWaitServer().set(this.flags, bundle.getString(ConfigConstants.CONFIG_ARM_SERVER)); break;
            case ConfigConstants.CONFIG_DISARM_SERVER:
                new DisarmWaitServer().set(this.flags, bundle.getString(ConfigConstants.CONFIG_DISARM_SERVER)); break;
            case ConfigConstants.CONFIG_ROAMING:
                new RoamingEnabled().set(this.flags, bundle.getString(ConfigConstants.CONFIG_ROAMING)); break;
        }
        setNewFromInt(this.flags, 0x08, 4);
    }

    protected void setFlags() {
        flags = getBytes(4, 0x08);
    }

    protected byte[] getBytes(int size, int offset) {
        byte[] array = new byte[size];
        System.arraycopy(data, offset, array, 0, array.length);
        return array;
    }

    protected void setNewFromString(byte[] array, int offset, int size) {
        byte[] a = LocalConnection.deleteZeros(array);
        byte[] bytes = new byte[size];
        System.arraycopy(a, 0, bytes, 0, a.length);
        System.arraycopy(bytes, 0, data, offset, bytes.length);
    }

    protected void setNewFromInt(byte[] array, int offset, int size) {
        byte[] bytes = new byte[size];
        System.arraycopy(array, 0, bytes, 0, Math.min(array.length, bytes.length));
        System.arraycopy(bytes, 0, data, offset, bytes.length);
    }

    protected void setNewFromIp(String ip, int offset) {
        String[] strings = ip.split("\\.");
        byte[] bytes = new byte[4];
        for (int i = 0; i < bytes.length; i++)
            bytes[i] = LocalConnection.leIntToByteArray(Integer.parseInt(strings[i]))[0];
        System.arraycopy(bytes, 0, data, offset, bytes.length);
    }

    protected String getIpFromBytes(byte[] array) {
        return (array[0] & 0xFF) + "." + (array[1] & 0xFF) + "." + (array[2] & 0xFF) + "." + (array[3] & 0xFF);
    }

    protected byte[] afterIntLe(String string) {
        return LocalConnection.leIntToByteArray(Integer.parseInt(string));
    }

    protected byte[] afterShortLe(String string) {
        return LocalConnection.leShortToByteArray(Short.parseShort(string));
    }
}
