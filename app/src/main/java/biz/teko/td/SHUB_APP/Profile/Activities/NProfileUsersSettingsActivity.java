package biz.teko.td.SHUB_APP.Profile.Activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public  class NProfileUsersSettingsActivity extends BaseActivity
{
	private NProfileUsersSettingsActivity context;
	private DBHelper dbHelper;
	private PrefUtils prefUtils;
	private LinearLayout back;
	private NMenuListElement name;
	private NMenuListElement login;
	private NMenuListElement pass;
	private NMenuListElement setEnable;
	private NMenuListElement permissions;
	private NMenuListElement delete;
	private int userId;
	private Operator operator;
	private boolean bound;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private int sending = 0;

	private BroadcastReceiver operatorsReceive = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			bind();
		}
	};

	private BroadcastReceiver operatorChangedReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", -99);
			if(result > 0){
				Func.pushToast(context, getString(R.string.PUA_SUCCESS_CHANGED), (Activity) context);
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_users_settings);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		prefUtils = PrefUtils.getInstance(context);
		userId = getIntent().getIntExtra("user", 0);

		setup();
	}

	private void setup()
	{
		back = (LinearLayout) findViewById(R.id.nButtonBack);
		name = (NMenuListElement) findViewById(R.id.nMenuName);
		login = (NMenuListElement) findViewById(R.id.nMenuLogin);
		pass = (NMenuListElement) findViewById(R.id.nMenuPassword);
		setEnable = (NMenuListElement) findViewById(R.id.nMenuEnable);
		permissions = (NMenuListElement) findViewById(R.id.nMenuPermissions);
		delete  = (NMenuListElement) findViewById(R.id.nMenuDelete);
	}

	private void bind()
	{
		if(0!=userId) operator =dbHelper.getOperatorById(userId);
		if(null!=back) back.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});

		if(null!=operator){
			if(null!=name) {
				name.setTitle(operator.name);
				name.setOnRootClickListener(() -> {
					Intent intent = new Intent(context, NRenameActivity.class);
					intent.putExtra("type", Const.USER);
					intent.putExtra("user", operator.id);
					intent.putExtra("name", operator.name);
					intent.putExtra("transition",true);
					startActivity(intent, getTransitionOptions(name).toBundle());
				});
			}
			if(null!=login){
				login.setTitle(operator.login);
				login.setOnRootClickListener(() -> {
					Intent intent = new Intent(context, NRenameActivity.class);
					intent.putExtra("type", Const.LOGIN);
					intent.putExtra("user", operator.id);
					intent.putExtra("name", operator.login);
					intent.putExtra("transition",true);
					startActivity(intent, getTransitionOptions(login).toBundle());
				});
			}
			if(null!=pass){
				pass.setOnRootClickListener(() -> {
					if((operator.roles & Const.Roles.DOMAIN_ADMIN) ==0 && ((operator.roles&Const.Roles.DOMAIN_HOZ_ORG)>0 || (operator.roles&Const.Roles.DOMAIN_OPER)>0) ) {
						showChangePassDialog();
					}else{
						Func.nShowMessage(context, context.getResources().getString(R.string.ERROR_NO_RULES));
					}
				});
			}
			if(null!=setEnable){
				setEnable.setTitle(operator.active == 1? context.getResources().getString(R.string.PLA_DISABLE_USER) : context.getResources().getString(R.string.PLA_ENABLE_USER));
				setEnable.setOnRootClickListener(this::showEnableDialog);
			}
			if(null!=permissions){
				permissions.setTitle(context.getResources().getStringArray(R.array.user_roles)[operator.getRole()]);
				permissions.setOnRootClickListener(()->{
					if((operator.roles & Const.Roles.DOMAIN_ADMIN) ==0 && ((operator.roles&Const.Roles.DOMAIN_HOZ_ORG)>0 || (operator.roles&Const.Roles.DOMAIN_OPER)>0) ) {
						showRoleSetDialog();
					}else{
						Func.nShowMessage(context, context.getResources().getString(R.string.ERROR_NO_RULES));
					}
				});
			}
			if(null!=delete){
				delete.setOnRootClickListener(()->{
					Intent intent = new Intent(context, NDeleteActivity.class);
					intent.putExtra("type", Const.USER);
					intent.putExtra("user", operator.id);
					startActivityForResult(intent, Const.REQUEST_DELETE);
				});
			}
		}

	}

	private void showChangePassDialog() {
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_change);
		nDialog.setTitle(getString(R.string.N_USER_DOMAIN_CHANGE_PASS_TITLE));
		View view = getLayoutInflater().inflate(R.layout.n_change_pass_view, null);
		nDialog.addView(view);

		NEditText editPass = (NEditText) view.findViewById(R.id.nEditPass);
		NEditText repeatPass = (NEditText) view.findViewById(R.id.nEditRepeatPass);
		TextInputLayout editPassInput = (TextInputLayout) view.findViewById(R.id.nEditPassInput);
		TextInputLayout editRepeatInput = (TextInputLayout) view.findViewById(R.id.nEditRepeatInput);
//		NActionButton buttonPaste = (NActionButton) view.findViewById(R.id.nButtonPaste);

		if(null!=editPass)
		{
			editPass.setFocusableInTouchMode(true);
			editPass.setText("");
			editPass.setSelection(editPass.getText().length());
			editPass.showKeyboard();
			editPass.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					editPassInput.setError(null);
				}

				@Override
				public void afterTextChanged(Editable s)
				{

				}
			});
			editPass.setOnKeyListener(new View.OnKeyListener()
			{
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if((event.getAction() == KeyEvent.ACTION_DOWN)&& (keyCode == KeyEvent.KEYCODE_ENTER)){
						repeatPass.setSelection(repeatPass.getText()!=null ? repeatPass.getText().toString().length() : 0);
					}
					return false;
				}
			});
		}
		if(null!=repeatPass){
			repeatPass.setFocusableInTouchMode(true);
			repeatPass.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					editRepeatInput.setError(null);
				}

				@Override
				public void afterTextChanged(Editable s)
				{
					String result = s.toString().replaceAll(" ", "");
					if (!s.toString().equals(result))
					{
						repeatPass.setText(result);
						repeatPass.setSelection(result.length());
					}
				}
			});
			repeatPass.setOnKeyListener(new View.OnKeyListener()
			{
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
					{
						return changePass(editPass, repeatPass);
					}
					return false;
				}

				private boolean changePass(NEditText editPass, NEditText repeatPass) {
					if (validate(editPass)) {
						if (validate(repeatPass)) {
							if (editPass.getText().toString().equals(repeatPass.getText().toString())) {
								editPass(editPass.getText().toString());
								nDialog.dismiss();
								return true;
							} else {
								repeatPass.showKeyboard();
								editRepeatInput.setError(" ");
							}
						} else {
							repeatPass.showKeyboard();
							editRepeatInput.setError(" ");
						}
					} else {
						editPass.showKeyboard();
						editPassInput.setError(" ");
					}
					return false;
				}
			});
		}

		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value)
				{
					case NActionButton.VALUE_OK:
						changePass(editPass, repeatPass);
						break;
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}

			private void changePass(NEditText editPass, NEditText repeatPass) {
				if (validate(editPass)) {
					if (validate(repeatPass)) {
						if (editPass.getText().toString().equals(repeatPass.getText().toString())) {
							editPass(editPass.getText().toString());
							nDialog.dismiss();
						} else {
							repeatPass.showKeyboard();
							editRepeatInput.setError(" ");
						}
					} else {
						repeatPass.showKeyboard();
						editRepeatInput.setError(" ");
					}
				} else {
					editPass.showKeyboard();
					editPassInput.setError(" ");
				}
			}
		});
		nDialog.show();
	}

	private void editPass(String pass) {
		JSONObject message = new JSONObject();
		try
		{
			message.put("id", operator.id);
			message.put("password", Func.md5(pass));
			nSendMessageToServer(null, D3Request.OPERATOR_SET, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private boolean validate(NEditText name)
	{
		return (null!=name.getText() && !name.getText().toString().equals(""));
	}


	private void showRoleSetDialog()
	{
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_radio_roles);
		nDialog.setTitle(getResources().getString(R.string.PLA_CHANGE_ROLES_MESS));
		nDialog.setRadioChecked((operator.roles&Const.Roles.DOMAIN_HOZ_ORG)>0 ? R.id.nRadioWrite :  R.id.nRadioView);
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value){
				case NActionButton.VALUE_OK:
					JSONObject message = new JSONObject();
					try
					{
						message.put("id", operator.id);
						message.put("roles", nDialog.getRadioChecked() == R.id.nRadioView ? Const.Roles.DOMAIN_OPER : (Const.Roles.DOMAIN_OPER + Const.Roles.DOMAIN_HOZ_ORG));
						nSendMessageToServer(null, D3Request.OPERATOR_SET, message, false);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					nDialog.dismiss();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}
		});

		nDialog.show();
	}

	private void showEnableDialog()
	{
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question);
		nDialog.setTitle(operator.active == 1 ? context.getResources().getString(R.string.PLA_USER_OFF_MESS) : context.getResources().getString(R.string.PLA_USER_ON_MESS));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value){
				case NActionButton.VALUE_OK:
					enableUser();
					nDialog.dismiss();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}
		});
		nDialog.show();
	}

	private void enableUser()
	{
		JSONObject message = new JSONObject();
		try
		{
			message.put("id", operator.id);
			message.put("active", operator.active != 1);
			nSendMessageToServer(null, D3Request.OPERATOR_SET, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public void  onResume(){
		super.onResume();
		bind();
		bindD3();
		registerReceiver(operatorsReceive, new IntentFilter(D3Service.BROADCAST_OPERATORS));
		registerReceiver(operatorChangedReceiver, new IntentFilter(D3Service.BROADCAST_OPERATOR_SET));
	}

	public void onPause(){
		super.onPause();
		if(bound)unbindService(serviceConnection);
		unregisterReceiver(operatorsReceive);
		unregisterReceiver(operatorChangedReceiver);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((NProfileUsersSettingsActivity)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}


}
