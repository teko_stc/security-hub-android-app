package biz.teko.td.SHUB_APP.D3DB;

/**
 * Created by td13017 on 02.02.2017.
 */
public class Address
{
	public AddressElement[] addressElements;
	public String house_number;
	public String apartment_number;

	public Address(AddressElement[] addressElements, String house_number, String apartment_number){
		this.addressElements = addressElements;
		this.house_number = house_number;
		this.apartment_number = apartment_number;
	}
}
