package biz.teko.td.SHUB_APP.UDP.Utils;

public class ConfigConstants {
    public static final String LOCAL_CONFIG_MODEL = "LOCAL_CONFIG_MODEL";
    //region temp
    public static final String LOW_TEMP_1 = "LowTempSection1";
    public static final String HIGH_TEMP_1 = "HighTempSection1";
    public static final String LOW_TEMP_2 = "LowTempSection2";
    public static final String HIGH_TEMP_2 = "HighTempSection2";
    public static final String LOW_TEMP_3 = "LowTempSection3";
    public static final String HIGH_TEMP_3 = "HighTempSection3";
    public static final String LOW_TEMP_4 = "LowTempSection4";
    public static final String HIGH_TEMP_4 = "HighTempSection4";
    public static final String LOW_TEMP_5 = "LowTempSection5";
    public static final String HIGH_TEMP_5 = "HighTempSection5";
    public static final String LOW_TEMP_6 = "LowTempSection6";
    public static final String HIGH_TEMP_6 = "HighTempSection6";
    public static final String LOW_TEMP_7 = "LowTempSection7";
    public static final String HIGH_TEMP_7 = "HighTempSection7";
    public static final String LOW_TEMP_8 = "LowTempSection8";
    public static final String HIGH_TEMP_8 = "HighTempSection8";
    public static String[] temperatures = new String[] {
            LOW_TEMP_1, HIGH_TEMP_1, LOW_TEMP_2,
            HIGH_TEMP_2, LOW_TEMP_3, HIGH_TEMP_3, LOW_TEMP_4, HIGH_TEMP_4, LOW_TEMP_5, HIGH_TEMP_5,
            LOW_TEMP_6, HIGH_TEMP_6, LOW_TEMP_7, HIGH_TEMP_7, LOW_TEMP_8, HIGH_TEMP_8,
    };
    //endregion
    //region apn
    public static final String APN = "APN";
    public static final String APN_SERVER = "APN_SERVER";
    public static final String APN2_SERVER = "APN2_SERVER";
    public static final String APN_LOGIN = "APN_LOGIN";
    public static final String APN2_LOGIN = "APN2_LOGIN";
    public static final String APN_PASSWORD = "APN_PASSWORD";
    public static String[] stringsAPN1 = new String[] {APN_SERVER, APN_LOGIN, APN_PASSWORD};
    public static final String APN2_PASSWORD = "APN2_PASSWORD";
    public static String[] stringsAPN2 = new String[] {APN2_SERVER, APN2_LOGIN, APN2_PASSWORD};
    public static String[] stringsAPNs = new String[] {APN_SERVER, APN_LOGIN, APN_PASSWORD, APN2_SERVER, APN2_LOGIN, APN2_PASSWORD};
    public static final String STRINGS_APN = "STRINGS_APN";
    //endregion
    //region config
    public static final String HUB_VERSION = "HUB_VERSION";
    public static final String CONFIG_PIN = "CONFIG_PIN";
    public static final String CONFIG_RESERVE_PORT = "CONFIG_RESERVE_PORT";
    public static final String CONFIG_MAIN_PORT = "CONFIG_MAIN_PORT";
    public static final String CONFIG_RADIO_OFFSET = "CONFIG_RADIO_OFFSET";
    public static final String CONFIG_SERVER_IP = "CONFIG_SERVER_IP";
    public static final String CONFIG_MASK_IP = "CONFIG_MASK_IP";
    public static final String CONFIG_GATEWAY_IP = "CONFIG_GATEWAY_IP";
    public static final String CONFIG_STATIC_IP = "CONFIG_STATIC_IP";
    public static final String CONFIG_RESERVE_IP = "CONFIG_RESERVE_IP";
    public static String[] stringsConfigIP = new String[] {
            CONFIG_SERVER_IP, CONFIG_MASK_IP, CONFIG_GATEWAY_IP, CONFIG_STATIC_IP, CONFIG_RESERVE_IP};
    public static final String CONFIG_RADIO_LITER = "CONFIG_RADIO_LITER";
    public static final String CONFIG_WAIT_REVIEWED = "CONFIG_WAIT_REVIEWED";
    public static final String CONFIG_ARM_SERVER = "CONFIG_ARM_SERVER";
    public static final String CONFIG_DISARM_SERVER = "CONFIG_DISARM_SERVER";
    public static final String CONFIG_ROAMING = "CONFIG_ROAMING";
    public static String[] stringsWithDialogBoolean = new String[] {
            CONFIG_RADIO_LITER, CONFIG_WAIT_REVIEWED, CONFIG_ARM_SERVER, CONFIG_DISARM_SERVER, CONFIG_ROAMING};
    public static final String CONFIG_ENTRY_DELAY = "CONFIG_ENTRY_DELAY";
    public static final String CONFIG_EXIT_DELAY = "CONFIG_EXIT_DELAY";
    public static String[] stringsWithDialog = new String[] {
            CONFIG_RADIO_LITER, CONFIG_WAIT_REVIEWED, CONFIG_ARM_SERVER,
            CONFIG_DISARM_SERVER, CONFIG_ROAMING, CONFIG_ENTRY_DELAY, CONFIG_EXIT_DELAY};
    //endregion
    //region versions
    public static int hubVersion1 = 711;
    public static int hubVersion2 = 777;
    public static int hubVersion3 = 793;
    public static int hubVersion4 = 727;
    //endregion
}
