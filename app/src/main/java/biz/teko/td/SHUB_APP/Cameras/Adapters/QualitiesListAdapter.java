package biz.teko.td.SHUB_APP.Cameras.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Cameras.Quality;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 06.04.2018.
 */

public class QualitiesListAdapter extends ArrayAdapter
{
	private final LayoutInflater layoutInflater;
	private final Quality[] qualities;
	private final int curQuality;
	private final Context context;

	public QualitiesListAdapter(Context context, int resource, Quality[] qualities, int curQuality)
	{
		super(context, resource);
		this.context = context;
		this.qualities = qualities;
		this.curQuality  =curQuality;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}



	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		Quality quality = qualities[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(quality.name);
		if(curQuality == Integer.valueOf(quality.value)){
			textView.setTextColor(context.getResources().getColor(R.color.brandColorAccentColor));
		}else{
			textView.setTextColor(context.getResources().getColor(R.color.brandColorTextTitle));
		}
		return view;
	}

	public int getCount() {
		return qualities.length;
	}

	public Quality getItem(int position){
		return qualities[position];
	}

	public long getItemId(int position){
		return position;
	}
}
