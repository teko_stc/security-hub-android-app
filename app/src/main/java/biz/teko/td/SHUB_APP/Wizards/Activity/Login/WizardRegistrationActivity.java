package biz.teko.td.SHUB_APP.Wizards.Activity.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.RegisterViewPagerAdapter;

/**
 * Created by td13017 on 07.06.2017.
 */

public class WizardRegistrationActivity extends AppCompatActivity
{
	private RegisterViewPagerAdapter adapter;
	private ViewPager pager;
	private View indicator1;
	private View indicator2;
	private View indicator3;
	private View indicator4;
	private View indicator5;
	private Button next;
	private boolean enableStatus = false;
	private Context context;
	private DBHelper dbHelper;

	@Override
	protected void onCreate(final Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_register);
		context = this;
		dbHelper = DBHelper.getInstance(context);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.WIZ_WELC_REGISTRATION);

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		adapter = new RegisterViewPagerAdapter(getSupportFragmentManager());
		pager = (ViewPager) findViewById(R.id.registerViewPager);
		pager.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true;
			}
		});
		pager.setAdapter(adapter);

		indicator1 = (View) findViewById(R.id.indicator1);
		indicator2 = (View) findViewById(R.id.indicator2);
		indicator3 = (View) findViewById(R.id.indicator3);
		indicator4 = (View) findViewById(R.id.indicator4);
		indicator5 = (View) findViewById(R.id.indicator5);
		updateIndicators(0);

	}

	@Override
	public void onBackPressed() {
		Func.hideKeyboard((WizardRegistrationActivity)context);
		if (pager.getCurrentItem() == 0) {
			super.onBackPressed();
		} else {
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position - 1);
			updateIndicators(position - 1);
		}
	}

	private void updateIndicators(int i)
	{
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int resizeValue = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 25, metrics);
		int defaultValue = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 15, metrics);
		switch (i) {
			case 0:
				indicator1.getLayoutParams().height = resizeValue;
				indicator1.getLayoutParams().width = resizeValue;
				indicator1.requestLayout();

				indicator2.getLayoutParams().height = defaultValue;
				indicator2.getLayoutParams().width = defaultValue;
				indicator2.requestLayout();

				indicator3.getLayoutParams().height = defaultValue;
				indicator3.getLayoutParams().width = defaultValue;
				indicator3.requestLayout();

				indicator4.getLayoutParams().height = defaultValue;
				indicator4.getLayoutParams().width = defaultValue;
				indicator4.requestLayout();

				indicator5.getLayoutParams().height = defaultValue;
				indicator5.getLayoutParams().width = defaultValue;
				indicator5.requestLayout();

				break;

			case 1:
				indicator1.getLayoutParams().height = defaultValue;
				indicator1.getLayoutParams().width = defaultValue;
				indicator1.requestLayout();

				indicator2.getLayoutParams().height = resizeValue;
				indicator2.getLayoutParams().width = resizeValue;
				indicator2.requestLayout();

				indicator3.getLayoutParams().height = defaultValue;
				indicator3.getLayoutParams().width = defaultValue;
				indicator3.requestLayout();

				indicator4.getLayoutParams().height = defaultValue;
				indicator4.getLayoutParams().width = defaultValue;
				indicator4.requestLayout();

				indicator5.getLayoutParams().height = defaultValue;
				indicator5.getLayoutParams().width = defaultValue;
				indicator5.requestLayout();
				break;

			case 2:
				indicator1.getLayoutParams().height = defaultValue;
				indicator1.getLayoutParams().width = defaultValue;
				indicator1.requestLayout();

				indicator2.getLayoutParams().height = defaultValue;
				indicator2.getLayoutParams().width = defaultValue;
				indicator2.requestLayout();

				indicator3.getLayoutParams().height = resizeValue;
				indicator3.getLayoutParams().width = resizeValue;
				indicator3.requestLayout();

				indicator4.getLayoutParams().height = defaultValue;
				indicator4.getLayoutParams().width = defaultValue;
				indicator4.requestLayout();

				indicator5.getLayoutParams().height = defaultValue;
				indicator5.getLayoutParams().width = defaultValue;
				indicator5.requestLayout();
				break;

			case 3:
				indicator1.getLayoutParams().height = defaultValue;
				indicator1.getLayoutParams().width = defaultValue;
				indicator1.requestLayout();

				indicator2.getLayoutParams().height = defaultValue;
				indicator2.getLayoutParams().width = defaultValue;
				indicator2.requestLayout();

				indicator3.getLayoutParams().height = defaultValue;
				indicator3.getLayoutParams().width = defaultValue;
				indicator3.requestLayout();

				indicator4.getLayoutParams().height = resizeValue;
				indicator4.getLayoutParams().width = resizeValue;
				indicator4.requestLayout();

				indicator5.getLayoutParams().height = defaultValue;
				indicator5.getLayoutParams().width = defaultValue;
				indicator5.requestLayout();
				break;
			case 4:
				indicator1.getLayoutParams().height = defaultValue;
				indicator1.getLayoutParams().width = defaultValue;
				indicator1.requestLayout();

				indicator2.getLayoutParams().height = defaultValue;
				indicator2.getLayoutParams().width = defaultValue;
				indicator2.requestLayout();

				indicator3.getLayoutParams().height = defaultValue;
				indicator3.getLayoutParams().width = defaultValue;
				indicator3.requestLayout();

				indicator4.getLayoutParams().height = defaultValue;
				indicator4.getLayoutParams().width = defaultValue;
				indicator4.requestLayout();

				indicator5.getLayoutParams().height = resizeValue;
				indicator5.getLayoutParams().width = resizeValue;
				indicator5.requestLayout();

				break;
		}
	}

//	public void setNextButtonEnable(boolean enableStatus)
//	{
//		this.enableStatus = enableStatus;
//		if(next !=null)
//		{
//			next.setEnabled(enableStatus);
//			next.requestLayout();
//		}
//	}

	public void setNextPage()
	{
		if(pager.getCurrentItem() != 4)
		{
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position + 1);
			updateIndicators(position + 1);
		}else{
			Intent activityIntent = new Intent(getBaseContext(), WizardRegistrationFinishActivity.class);
			startActivity(activityIntent);
			overridePendingTransition(R.animator.enter, R.animator.exit);
		}

	}

	public int getCurrentPage(){
		return  pager.getCurrentItem();
	};

//	//fragment 0
//
	//fragment 1
	public void setVerificationCode(int code)
	{

	}
//
//	//fragment 2 is empty
//
	//fragment 3
	public void setPinCode(int code){
//		ADD LATER

//		UserInfo userInfo = dbHelper.getUserInfo();
//		userInfo.pinCode = Func.md5(String.valueOf(code));
//		dbHelper.addUserInfo(userInfo);
	}
//
//	//fragment 4

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleHelper.onAttach(base));
	}

}
