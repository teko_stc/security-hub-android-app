package biz.teko.td.SHUB_APP.Profile.LocalConfig.Adapters;

import android.content.Context;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.Profile.Models.LocalConfigModel;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class ConfigChooseViewHolder extends RecyclerView.ViewHolder {
    private final ConfigChooseAdapter.OnConfigClickListener listener;
    private NMenuListElement element;
    private String type;
    private final Context context;

    ConfigChooseViewHolder(@NonNull View itemView, ConfigChooseAdapter.OnConfigClickListener listener, Context context) {
        super(itemView);
        this.context = context;
        this.listener = listener;
        initView(itemView);
    }

    private void initView(View view) {
        element = view.findViewById(R.id.nConfigChooseElement);
    }

    public void bind(LocalConfigModel config) {
        type = config.getType();
        if(null!=element)
        {
            element.setSubtitle(config.getTitle());
            String desc = config.getDescription();
            element.setTitle(desc);
            if (desc.equals(context.getResources().getString(R.string.NOT_SPECIFIED))) element.setTitleColor(R.color.n_bc_grey);
            element.setOnRootClickListener(() -> {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onConfigClick(type, element.getTitleView());
                    }
                }
            });
        }
    }
}
