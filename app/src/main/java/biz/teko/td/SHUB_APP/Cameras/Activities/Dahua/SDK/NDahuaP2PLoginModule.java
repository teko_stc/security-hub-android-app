package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK;

import android.os.AsyncTask;

import com.company.NetSDK.EM_LOGIN_SPAC_CAP_TYPE;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.NET_DEVICEINFO_Ex;
import com.company.NetSDK.NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY;
import com.company.NetSDK.NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.NCameraDahuaAddActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NDahuaP2PLoginModule{

//	private static final String SRV_ADDRESS = "http://www.easy4ipcloud.com/";
//	private static final String SRV_ADDRESS = "www.easy4ip.com";
//	private static final String SRV_ADDRESS = "43.131.65.230";
	private static final String SRV_ADDRESS = "www.easy4ipcloud.com";
//	private static final String SRV_ADDRESS = "162.62.8.248";
//	private static final String SRV_ADDRESS = "152.32.228.244";
//	private static final String SRV_ADDRESS = "152.32.227.155";
//	private static final String SRV_ADDRESS = "107.155.53.182";
	private static final  String SRV_PORT = "8800";
//	private static final  String SRV_PORT = "80";
//	private static final  String SRV_PORT = "443";
	private static final String DEVICE_PORT = "37777";
	private static final String P2P_USERNAME = "1b774e0016014aad8cd39cda462ce9c2";
	private static final String P2P_SERVER_SECRET = "302f14c8b2384bffa4d6d9f738c90207";

	public interface CI_login_int{
		void callback(Camera camera);
	}


	private static final String TAG = "NDahuaP2PLoginModule";
	private NDahuaP2PClient dahuaP2PClient;
	private long loginHandle = 0;
	private NET_DEVICEINFO_Ex mDeviceInfo;
	private int errorCode = 0;
	private boolean serviceStarted = false;
	private String deviceName;

	public NDahuaP2PLoginModule() {
		dahuaP2PClient = new NDahuaP2PClient();
	}

	public boolean isServiceStarted() {
		return serviceStarted;
	}

	public boolean startP2pService(String sn, String login, String pass) {
		return startP2pService(SRV_ADDRESS, SRV_PORT, P2P_USERNAME, P2P_SERVER_SECRET, sn, DEVICE_PORT, login, pass);
	}

	public boolean startP2pService( String svrAddress, String svrPort,
									String username, String svrKey,
									String deviceSn, String devicePort,
									String p2pDeviceUsername, String p2pDevicePassword) {
		deviceName = deviceSn;
		Func.log_d(Const.LOG_TAG_DAHUA, "Start p2p service" + " " + deviceSn);
		if(dahuaP2PClient.startService(svrAddress, svrPort, username, svrKey, deviceSn, devicePort, p2pDeviceUsername, p2pDevicePassword)) {
			serviceStarted = true;
			Func.log_d(Const.LOG_TAG_DAHUA, "Start succeed");
		} else {
			serviceStarted = false;
			Func.log_d(Const.LOG_TAG_DAHUA, "Start failed");
			return false;
		}
		return true;
	}

	public boolean stopP2pService() {
		loginHandle = 0;
		serviceStarted = false;
		mDeviceInfo = null;
		errorCode = 0;
		return dahuaP2PClient.stopService();
	}

	public boolean login(Camera camera) {
//		Func.log_d(Const.LOG_TAG_DAHUA, "P2p login: " + camera.login + "@"  + camera.pass);
		Func.log_d(Const.LOG_TAG_DAHUA, "P2p login: " + camera.login);
		return login(camera.login, camera.pass);
	}

	public boolean login(String username, String password) {
		mDeviceInfo = new NET_DEVICEINFO_Ex();
		// default device login ip.
		final String deviceIP = "127.0.0.1";
		NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY stuIn = new NET_IN_LOGIN_WITH_HIGHLEVEL_SECURITY();
		System.arraycopy(deviceIP.getBytes(), 0, stuIn.szIP, 0, deviceIP.getBytes().length);
		stuIn.nPort = dahuaP2PClient.getP2pPort();
		System.arraycopy(username.getBytes(), 0, stuIn.szUserName, 0, username.getBytes().length);
		System.arraycopy(password.getBytes(), 0, stuIn.szPassword, 0, password.getBytes().length);
		stuIn.emSpecCap = EM_LOGIN_SPAC_CAP_TYPE.EM_LOGIN_SPEC_CAP_P2P;
		NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY stuOut = new NET_OUT_LOGIN_WITH_HIGHLEVEL_SECURITY();
		loginHandle = INetSDK.LoginWithHighLevelSecurity(stuIn, stuOut);
		Func.log_d(Const.LOG_TAG_DAHUA, "login loginHandle: " + loginHandle);
		if (0 == loginHandle) {
			errorCode = INetSDK.GetLastError();
			Func.log_d(Const.LOG_TAG_DAHUA, "Failed to Login Device." + " " + Integer.toString(errorCode & 0x7fffffff));
			Func.log_d(Const.LOG_TAG_DAHUA, "Failed to Login Device." + " " + NCameraDahuaAddActivity.getErrorCode(App.getContext().getResources(), errorCode));
			return false;
		}
		mDeviceInfo = stuOut.stuDeviceInfo;
		return true;
	}

	public boolean logout() {
		if (0 == loginHandle) {
			return  false;
		}

		boolean retLogout = INetSDK.Logout(loginHandle);
		Func.log_d(Const.LOG_TAG_DAHUA, "logout loginHandle: " + loginHandle);

		if (retLogout) {
			loginHandle = 0;
		}

		return  retLogout;
	}

	public int errorCode() {
		return errorCode;
	}

	public long getLoginHandle() {
		return loginHandle;
	}

	public NET_DEVICEINFO_Ex getDeviceInfo() {
		return mDeviceInfo;
	}

	public String getDeviceName(){
		return deviceName;
	}

	public AsyncTask<String, Integer, Boolean> startLoginTask(Camera camera, CI_login_int ci_login_int){
		return new P2PLoginTask(camera, ci_login_int).execute();
	}

	private class P2PLoginTask extends AsyncTask<String, Integer, Boolean>
	{
		private Camera cam;
		private CI_login_int ci_login_int;

		public P2PLoginTask(Camera cam, CI_login_int ci_login_int)
		{
			this.cam = cam;
			this.ci_login_int = ci_login_int;
		}

		@Override
		protected void onPreExecute(){
			super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			boolean result =  false;
			if(null!= cam && startP2pService(cam.sn, cam.login, cam.pass)) {
				result = login(cam);
			}
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result){
			Func.log_d(Const.LOG_TAG_DAHUA, "login result:" + (result? "true": "false"));
			if (result) {
				if(isServiceStarted()) {
//					NET_DEVICEINFO_Ex info = getDeviceInfo();

					cam.setLoginHandle(getLoginHandle());
					cam.setDeviceInfo(getDeviceInfo());

					ci_login_int.callback(cam);
				}
			}else{
				ci_login_int.callback(null);
			}
		}
	}
}