package biz.teko.td.SHUB_APP.DevicesTabs.Adapters;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import biz.teko.td.SHUB_APP.DevicesTabs.Fragments.DevicesHistoryTabFragment;
import biz.teko.td.SHUB_APP.DevicesTabs.Fragments.DevicesTabFragment;

public class DevicesViewPagerAdapter extends FragmentStatePagerAdapter
{
	private final int siteId;
	private CharSequence titles[];
	private int numOfTabs;
	private DevicesTabFragment devicesTabFragment;
	private DevicesHistoryTabFragment devicesHistoryTabFragment;

	public DevicesViewPagerAdapter(FragmentManager fm, CharSequence[] titles, int numoftabs, int siteId)
	{
		super(fm);
		this.titles = titles;
		this.numOfTabs = numoftabs;
		this.siteId = siteId;
	}

	@Override
	public Fragment getItem(int position)
	{
		if(position == 0){
			return DevicesTabFragment.newInstance(siteId);
		}else{
			return DevicesHistoryTabFragment.newInstance(siteId);
		}
	}

	@Override
	public int getCount()
	{
		return numOfTabs;
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		return titles[position];
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		Fragment createdFragment = (Fragment) super.instantiateItem(container, position);
		// save the appropriate reference depending on position
		switch (position) {
			case 0:
				devicesTabFragment = (DevicesTabFragment) createdFragment;
				break;
			case 1:
				devicesHistoryTabFragment = (DevicesHistoryTabFragment) createdFragment;
				break;
		}
		return createdFragment;
	}

	public void closeFab(){
		devicesTabFragment.closeFABMenu();
	}
}
