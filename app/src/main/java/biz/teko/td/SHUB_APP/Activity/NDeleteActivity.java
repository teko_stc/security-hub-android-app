package biz.teko.td.SHUB_APP.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.User;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;

public class NDeleteActivity extends BaseActivity
{
//	public  static final int SITE = 0;
//	public  static final int DEVICE = 1;
//	public  static final int SECTION = 2;
//	public  static final int ZONE = 3;
//	public  static final int USER = 4;
//	public static final int LOCAL_USER = 5;
//	public static final int CAMERA_RTSP = 6;

	private User user;
	private Site site;
	private Device device;
	private Section section;
	private Zone zone;
	private Operator operator;
	private User local_user;
	private Camera camera_rtsp;
	private Camera camera_dahua;

	private LinearLayout buttonClose;
	private NWithAButtonElement buttonDelete;
	private boolean transition;
	private String name;
	private Context context;
	private DBHelper dbHelper;
	private UserInfo userInfo;
	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private int sending = 0;
	private int type;
	private String serial;

	private int site_id;
	private int device_id;
	private int section_id;
	private int zone_id;
	private int user_id;
	private int local_user_id;
	private int saved_device_id;
	private int camera_rtsp_id;
	private String camera_dahua_id;

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};

	private BroadcastReceiver deleteReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			switch (type)
			{
				case Const.SITE:
					int device_id  = intent.getIntExtra("device", 0);
					if(null!=site){
						if(saved_device_id == device_id)
							cont();
					}
					break;
				case Const.DEVICE:
					device_id  = intent.getIntExtra("device", 0);
					if(null!=device){
						if(device.account == device_id){
							cont();
						}
					}
					break;
				case Const.SECTION:
					device_id  = intent.getIntExtra("device", 0);
					int section_id = intent.getIntExtra("section", 0);
					int zone_id = intent.getIntExtra("zone", 0);
					if(null!=section){
						if(zone_id == 0 && section_id == section.id && device_id == section.device_id){
							cont();
						}
					}
					break;
				case Const.ZONE:
					device_id  = intent.getIntExtra("device", 0);
					section_id = intent.getIntExtra("section", 0);
					zone_id = intent.getIntExtra("zone", 0);
					if(null!=zone){
						if(zone_id == zone.id && section_id == zone.section_id && device_id == zone.device_id){
							cont();
						}
					}
					break;
				case Const.USER:
					int user_id = intent.getIntExtra("user", 0);
					if(null!=operator){
						if(user_id == operator.id){
							cont();
						}
					}
					break;
				case Const.LOCAL_USER:
					int local_user_id = intent.getIntExtra("local_user_id", 0);
					device_id  = intent.getIntExtra("device_id", 0);
					if(null!=local_user){
						if(local_user.id == local_user_id && local_user.device == device_id){
							cont();
						}
					}
					break;
				case Const.CAMERA_RTSP:
					break;
				default:
					break;
			}
		}
	};

	private void cont()
	{
		Intent intent = new Intent();
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_delete);

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();

		type = getIntent().getIntExtra("type", -1);
		site_id = getIntent().getIntExtra("site", -1);
		device_id = getIntent().getIntExtra("device", -1);
		section_id = getIntent().getIntExtra("section", -1);
		zone_id = getIntent().getIntExtra("zone", -1);
		user_id = getIntent().getIntExtra("user", -1);
		local_user_id = getIntent().getIntExtra("local_user", -1);
		camera_rtsp_id = getIntent().getIntExtra("camera_rtsp", -1);
		camera_dahua_id = getIntent().getStringExtra("camera_dahua");

		serial = getIntent().getStringExtra("serial");
		name = getIntent().getStringExtra("name");

		transition = getIntent().getBooleanExtra("transition", false);

		if(-1!=site_id)site = dbHelper.getSiteById(site_id);
		if(-1!=device_id) device = dbHelper.getDeviceById(device_id);
		if(-1!=section_id) section = dbHelper.getDeviceSectionById(device_id, section_id);
		if(-1!=zone_id) zone = dbHelper.getZoneById(device_id, section_id, zone_id);
		if(-1!=user_id) operator = dbHelper.getOperatorById(user_id);
		if(-1!=local_user_id) local_user = dbHelper.getDeviceUserById(local_user_id, device_id);
		if(-1!=camera_rtsp_id) camera_rtsp = dbHelper.getRTSPCameraById(camera_rtsp_id);
		if(null!=camera_dahua_id) camera_dahua = dbHelper.getDahuaCameraBySn(camera_dahua_id);
		if (user_id != -1 && device_id != -1)
			user = dbHelper.getDeviceUserById(user_id, device_id);

		addTransitionListener(transition);

		setup();
		bind();
	}

	private void bind()
	{
		if(null!=buttonClose) buttonClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});
		if(null!=buttonDelete)
		{
			buttonDelete.setOnChildClickListener(new NWithAButtonElement.OnChildClickListener()
			{
				@Override
				public void onChildClick(int action, boolean option)
				{
					delete();
				}
			});
			String title = "";
			String subTitle = "";
			String actionTitle = null;
			switch (type)
			{
				case Const.SITE:
					title = getString(R.string.N_DELETE_SITE_TITLE) + " " + site.name + "\"?";
					subTitle = getString(R.string.N_DELETE_SITE_MESSAGE);
					break;
				case Const.DEVICE:
					title = getString(R.string.N_DELETE_DEVICE_TITLE_1) + " "  + device.account + getString(R.string.N_DELETE_DEVICE_TITLE_2);
					subTitle = getString(R.string.N_DELETE_DEVICE_MESSAGE);
					actionTitle = getString(R.string.N_DELETE_DEVICE_BUTTON);
					break;
				case Const.SECTION:
					title = getString(R.string.N_DELETE_SECTION_TITLE_1) + section.name + "\"?";
					subTitle = getString(R.string.N_DELETE_SECTION_MESSAGE);
					break;
				case Const.ZONE:
					title = getString(R.string.N_DELETE_ZONE_TITLE_1) + " "  + zone.name + "\"?";
					subTitle = getString(R.string.N_DELETE_ZONE_MESSAGE);
					break;
				case Const.USER:
					title = getString(R.string.N_DELETE_DOMAIN_USER_TITLE) + " " + operator.name + "\"?";
					subTitle = getString(R.string.N_DELETE_DOMAIN_USER_MESSAGE);
					break;
				case Const.LOCAL_USER:
					title = getString(R.string.N_DELETE_USER_TITLE) + " " + local_user.name + "\"?";
					subTitle = getString(R.string.N_DELETE_USER_MESSAGE);
					break;
				case Const.CAMERA_RTSP:
					title = getString(R.string.N_DEL_RTSP_TITLE) + " " + camera_rtsp.name + "\"?";
					subTitle = getString(R.string.N_DEL_RTSP_SUBTITLE);
					break;
				case Const.LOCAL_CONFIG:
					title = getString(R.string.RESET) + " " + name + " " + serial + " " +
							getString(R.string.RESET_ON_START);
					subTitle = getString(R.string.CONFIG_RESET_QUESTION);
					actionTitle = getString(R.string.RESET);
					break;
				case Const.CAMERA_DAHUA:
					title = getString(R.string.N_DAHUA_DELETE_TITLE) + " " + camera_dahua.name + "\"?";
					subTitle = getString(R.string.N_DEL_RTSP_SUBTITLE);
					break;
//				case Const.LOCAL_USER:
//					title = getString(R.string.USER_DELETING) + " \"" + name + "\"?";
//					subTitle = getString(R.string.USER_DELETE_QUESTION);
//					break;
				default:
					break;
			}
			buttonDelete.setTitle(title);
			buttonDelete.setSubtitle(subTitle);
			if(null!=actionTitle) buttonDelete.setActionTitle(actionTitle);
		}
	}

	private void delete()
	{
		switch (type)
		{
			case Const.SITE:
				deleteSite();
				break;
			case Const.DEVICE:
				deleteDevice();
				break;
			case Const.SECTION:
				deleteSection();
				break;
			case Const.ZONE:
				deleteZone();
				break;
			case Const.USER:
				deleteUser();
				break;
			case Const.CAMERA_RTSP:
				deleteRTSPCamera();
				break;
			case Const.LOCAL_USER:
				deleteLocalUser();
				break;
			case Const.LOCAL_CONFIG:
				resetToDefaults();
				break;
			case Const.CAMERA_DAHUA:
				deleteDahuaCamera();
				break;
			default:
				break;
		}
	}

	private void resetToDefaults() {
		setResult(RESULT_OK);
		finish();
	}

	public boolean sendCommandToServer(D3Request d3Request) {
		if (null != myService) {
			if (myService.send(d3Request.toString())) {
				return true;
			} else {
				sendIntentCommandBeenSend(-1);
			}
		} else {
			Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
		}

		return false;
	}

	private void deleteLocalUser() {
		JSONObject commandAddr = new JSONObject();
		try {
			commandAddr.put("index", local_user.id);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		int command_count = Func.getCommandCount(context);
		D3Request d3Request = D3Request.createCommand(userInfo.roles, local_user.device, "USER_DEL", commandAddr, command_count);
		if (d3Request.error == null) {
			if (sendCommandToServer(d3Request))
				sendIntentCommandBeenSend(1);
		} else
			Func.pushToast(context, d3Request.error, (Activity) context);
	}

	public void sendIntentCommandBeenSend(int value) {
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	private void deleteUser()
	{
		JSONObject message = new JSONObject();
		try
		{
			message.put("id", operator.id);
			nSendMessageToServer(null, D3Request.OPERATOR_DEL, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void deleteDahuaCamera()
	{
		if(dbHelper.deleteDahuaCamera(camera_dahua)){
			cont();
		}
	}


	private void deleteRTSPCamera()
	{
		if(dbHelper.deleteRTSPCamera(camera_rtsp_id)){
			cont();
		}
	}

	private void deleteSite()
	{
		Device[] devices = dbHelper.getAllDevicesForSite(site.id);
		if(null!=devices && 0!=devices.length){
			if(devices.length == 1){
				JSONObject message = new JSONObject();
				try
				{
					saved_device_id = devices[0].id;
					message.put("device_id", devices[0].id);
					message.put("domain_id", userInfo.domain);
					message.put("site", site.id);
					nSendMessageToServer(null, D3Request.DEVICE_REVOKE_DOMAIN, message, false);
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
			}else{
				Func.nShowMessage(context, context.getString(R.string.ERROR_CANT_DELETE_SITE_WITH_TWO_OR_MORE_DEVICES));
			}
		}else{
			JSONObject message = new JSONObject();
			try
			{
				message.put("site", site.id);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
			nSendMessageToServer(null, D3Request.SITE_DEL, message, false);
		}
	}

	private void deleteDevice()
	{
		JSONObject message = new JSONObject();
		try
		{
			message.put("device_id", device.id);
			message.put("domain_id", userInfo.domain);
			nSendMessageToServer(null, D3Request.DEVICE_REVOKE_DOMAIN, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void deleteSection()
	{
		if (dbHelper.getDeviceArmStatus(section.device_id) == Const.STATUS_DISARMED)
		{
			JSONObject commandAddr = new JSONObject();
			try
			{
				commandAddr.put("index", section.id);
				nSendMessageToServer(section, Command.SECTION_DEL, commandAddr, true);
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		} else
		{
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
			nDialog.setTitle(getString(R.string.N_MESSAGE_NEED_DISARM));
			nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
			{
				@Override
				public void onActionClick(int value, boolean b)
				{
					switch (value){
						case NActionButton.VALUE_CANCEL:
							nDialog.dismiss();
							break;
					}
				}
			});
			nDialog.show();
		}
	}

	private void deleteZone()
	{
		if (dbHelper.getDeviceArmStatus(zone.device_id) == Const.STATUS_DISARMED)
		{
			JSONObject commandAddr = new JSONObject();
			try
			{
				commandAddr.put("section", zone.section_id);
				commandAddr.put("index", zone.id);
				nSendMessageToServer(dbHelper.getDeviceById(zone.device_id), Command.ZONE_DEL, commandAddr,true); //we should look at device connection status
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		} else
		{
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
			nDialog.setTitle(getString(R.string.N_MESSAGE_NEED_DISARM));
			nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
			{
				@Override
				public void onActionClick(int value, boolean b)
				{
					switch (value){
						case NActionButton.VALUE_CANCEL:
							nDialog.dismiss();
							break;
					}
				}
			});
			nDialog.show();
		}
	}


	private void setup()
	{
		buttonDelete = (NWithAButtonElement) findViewById(R.id.nButtonDelete);
		buttonClose = (LinearLayout) findViewById(R.id.nButtonClose);
	}

	public void  onResume(){
		super.onResume();
		bindD3();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_REVOKE_DOMAIN);
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_OPERATOR_RMV);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_USER_RMV);
		registerReceiver(deleteReceiver, intentFilter);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
	}

	public void onPause(){
		super.onPause();
		if(bound)unbindService(serviceConnection);
		unregisterReceiver(deleteReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};
}
