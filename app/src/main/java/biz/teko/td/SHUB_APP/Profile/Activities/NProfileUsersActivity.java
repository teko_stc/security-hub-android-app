package biz.teko.td.SHUB_APP.Profile.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Profile.Adapters.NProfileUsersListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Wizards.Activity.DomainUsers.WizardDomainUserSetActivity;

/**
 * Created by td13017 on 05.12.2016.
 */
public class NProfileUsersActivity extends BaseActivity
{
	private Context context;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private DBHelper dbHelper;
	private NProfileUsersListAdapter nProfileUsersListAdapter;
	private ListView operatorsList;
	private int sending = 0;
	private String blockCharacterSet = "@";
	private UserInfo userInfo;
	private Cursor cursor;
	private LinearLayout back, add;
	private TextView title;
	private TextView empty;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_users);
		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();
		setup();

	}

	private void bind(){
		setView();
		setClickListeners();
		updateUsersList();
	}
	private void setView() {
		if(null!=title)title.setText(getResources().getString(R.string.N_PROFILE_ACCESS));
	}

	private void setClickListeners() {
		if(null!=back) back.setOnClickListener(view -> onBackPressed());
		if(null!=add) add.setOnClickListener(view -> addNewUser());
	}

	private void setup() {
		back = (LinearLayout) findViewById(R.id.nButtonBack);
		add = (LinearLayout) findViewById(R.id.nButtonAdd);
		title = (TextView) findViewById(R.id.nTextTitle);
		empty  = (TextView) findViewById(R.id.operatorsEmpty);
		operatorsList = (ListView) findViewById(R.id.operatorsList);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	private void updateCursor()
	{
		cursor =  dbHelper.getOperatorsCursorNoCurrent(userInfo.userLogin, userInfo.domain);
	}

	public void onResume()
	{
		super.onResume();
		bind();
		registerReceiver(operatorsReceive, new IntentFilter(D3Service.BROADCAST_OPERATORS));
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(operatorsReceive);
	}

	private BroadcastReceiver operatorsReceive = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateUsersList();
		}
	};

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	private void addNewUser() {
		Intent activityIntent = new Intent(getBaseContext(), WizardDomainUserSetActivity.class);
		startActivityForResult(activityIntent, Const.ADD_DOMAIN_USER);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			updateUsersList();
		}
	}

	private void updateUsersList()
	{
		updateCursor();
		if(null== nProfileUsersListAdapter){
			nProfileUsersListAdapter = new NProfileUsersListAdapter(context, R.layout.card_operator, cursor, 0);
			if(null!=operatorsList){
				operatorsList.setAdapter(nProfileUsersListAdapter);
			}
		}else{
			nProfileUsersListAdapter.update(cursor);
		}

		if(null== cursor || cursor.getCount() == 0 ){
			empty.setVisibility(View.VISIBLE);
		}else{
			empty.setVisibility(View.GONE);
		}

	}
}
