package biz.teko.td.SHUB_APP.SecCompanies.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SecCompanies.Models.SCRc;

public class SecCompRCsBottomSheetAdapter extends ArrayAdapter
{


	private final LayoutInflater layoutInflater;
	private final Context context;
	private final SCRc[] scRcsArray;

	public SecCompRCsBottomSheetAdapter(@NonNull Context context, int resource, SCRc[] scRcsArray)
	{
		super(context, resource);
		this.context = context;
		this.scRcsArray = scRcsArray;
		this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.n_card_for_list, parent, false);
		}
		SCRc scRc = scRcsArray[position];
		TextView textView = (TextView) view.findViewById(R.id.textField);
		textView.setText(scRc.caption!=null? scRc.caption : scRc.city);
		return view;
	}

	public int getCount() {
		return scRcsArray.length;
	}

	public SCRc getItem(int position){
		return scRcsArray[position];
	}

	public long getItemId(int position){
		return position;
	}
}
