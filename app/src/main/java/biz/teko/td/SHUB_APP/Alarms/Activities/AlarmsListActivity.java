package biz.teko.td.SHUB_APP.Alarms.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.LinkedList;

import androidx.annotation.Nullable;

import java.util.LinkedList;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Alarm;
import biz.teko.td.SHUB_APP.D3DB.Alarm;
import biz.teko.td.SHUB_APP.D3DB.Alarm;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.Events.Adapters.NEventsListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NAlarmsTopView;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.Other.NListView;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class AlarmsListActivity extends BaseActivity {
    private NAlarmsTopView alarmsTopView;
    private NListView critList;
    private Context context;
    private DBHelper dbHelper;
    private NEventsListAdapter critAdapter;
    private TextView title;

    public boolean alarmed = false;
    public boolean crit = false;

    private final BroadcastReceiver alarmsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != alarmsTopView) {
                updateAlarmsView();
            }
        }
    };

    private final BroadcastReceiver eventReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != critList) {
                updateCritList();
            }
        }
    };

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_alarms_list);
        context = this;
        dbHelper = DBHelper.getInstance(context);

        setup();
        bind();
    }

    private void setup()
    {
        alarmsTopView = (NAlarmsTopView)  findViewById(R.id.nAlarmsTopView);
        critList = (NListView) findViewById(R.id.nCritList);
    }

    private void bind()
    {
        if(null!=alarmsTopView){
            updateAlarmsView();
        }
        if(null!=critList){
            updateCritList();
        }
    }

    private void updateAlarmsView()
    {
        alarmsTopView.setOnViewClickListener(this::onBackPressed);
        LinkedList<Alarm> alarms = dbHelper.getAlarmsList();
        alarmed = null!=alarms && alarms.size() >0;
        alarmsTopView.setAlarms(alarms);
        alarmsTopView.bind();
    }

    private void updateCritList()
    {
        Cursor cursor = updateCritCursor();
        if(null==critAdapter)
        {
            critAdapter = new NEventsListAdapter((Activity) context, R.layout.n_events_list_element, cursor, 0, false);
            critList.setAdapter(critAdapter);
        }else{
            critAdapter.update(cursor);
        }
        if(critAdapter.getCount() > 0){
            crit = true;
            critList.setVisibility(View.VISIBLE);
        }else {
            crit = false;
            critList.setVisibility(View.GONE);
        }
    }

    private Cursor updateCritCursor()
    {
        return dbHelper.nGetCritEventsCursorAMMode(0, PrefUtils.getInstance(context).getCurrentSite());
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(D3Service.BROADCAST_ALARM_ADD_UPD_RMV);
        intentFilter.addAction(D3Service.BROADCAST_ALARM_OPEN_CLOSE);
        registerReceiver(alarmsReceiver,intentFilter);
        registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_AFFECT));
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        unregisterReceiver(alarmsReceiver);
        unregisterReceiver(eventReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.REQUEST_SHOW_LIST_ELEMENT){
            if(resultCode == RESULT_OK){
                Func.nShowSuccessMessage(context, new NDialog.OnActionClickListener()
                {
                    @Override
                    public void onActionClick(int value, boolean b)
                    {
                        if(!alarmed && !crit) onBackPressed();
                    }
                });
            }
            bind();
        }
    }
}
