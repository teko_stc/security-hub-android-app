package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Alarms.Adapters.NAlarmsListAdapter;
import biz.teko.td.SHUB_APP.D3DB.Alarm;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Other.NListView;

public  class NAlarmsTopView extends LinearLayout
{
	private OnViewClickListener onViewClickListener;
	private LinkedList<Alarm> alarms;
	private TextView title;

	public void setOnViewClickListener(OnViewClickListener onViewClickListener){
		this.onViewClickListener = onViewClickListener;
	}
	public interface OnViewClickListener{
		void onClose();
	}

	public static  final int[] STATE_ALARM = {R.attr.state_alarm};

	private final Context context;
	private int layout;
	private LinearLayout close;
	private NListView list;

	private boolean alarm;

	public NAlarmsTopView(Context context)
	{
		super(context);
		this.context = context;
	}

	public NAlarmsTopView(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setup();
		bind();
	}

	private void setup()
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(layout, this, true);

		close = (LinearLayout) findViewById(R.id.nButtonClose);
		title = (TextView) findViewById(R.id.nTextTitle);

		list = (NListView) findViewById(R.id.nListAlarms);
	}

	public void bind()
	{
		if(null!=close) close.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(null!=onViewClickListener)  onViewClickListener.onClose();
			}
		});
		if(null!=list){
			if(null!=alarms && alarms.size() > 0){
				NAlarmsListAdapter alarmsListAdapter = new NAlarmsListAdapter(context, R.layout.n_alarms_list_element, alarms);
				list.setAdapter(alarmsListAdapter);
				setAlarm(true);
				list.setVisibility(VISIBLE);
				if(null!=title) {
					title.setText(R.string.N_NEW_ALARMS);
					title.setVisibility(VISIBLE);
				}

			}else{
				setAlarm(false);
				list.setVisibility(GONE);
				if(null!=title) title.setVisibility(GONE);
			}
		}
	}

	private void setAlarm(boolean alarm){
		this.alarm = alarm;
		refreshDrawableState();
	}

	public void setAlarms(LinkedList<Alarm> alarms){
		this.alarms = alarms;
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NAlarmsTopView, 0, 0);
		try{
			layout = a.getResourceId(R.styleable.NAlarmsTopView_NAlarmsTopViewLayout, 0);
		}finally
		{
			a.recycle();
		}

	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		if(alarm) {
			mergeDrawableStates(drawableState, STATE_ALARM);
		}
		return drawableState;
	}
}
