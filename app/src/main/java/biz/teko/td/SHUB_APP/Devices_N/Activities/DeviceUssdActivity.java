package biz.teko.td.SHUB_APP.Devices_N.Activities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

public class DeviceUssdActivity extends BaseActivity
{
	private LinearLayout buttonClose;
	private LinearLayout buttonNext;
	private boolean transition;
	private NEditText editUSSD;
	private TextView editUSSDTitle;

	private Context context;
	private DBHelper dbHelper;
	private UserInfo userInfo;
	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private final int sending = 0;
	private int device_id;
	private Device device;
	private final String ussd = "*100#";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_device_ussd_activity);

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();
		device_id = getIntent().getIntExtra("device", -1);


		setup();
		bind();
	}

	private void bind()
	{
		if(-1!=device_id) device = dbHelper.getDeviceById(device_id);

		if(null!=buttonClose) buttonClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		if(null!=buttonNext) buttonNext.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				sendUSSD();
			}
		});

		if(null!=editUSSD) {
			editUSSD.setFocusableInTouchMode(true);
			if(null!=ussd) {
				editUSSD.setText(ussd);
				editUSSD.setSelection(editUSSD.getText().length());
				try
				{
					Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
					f.setAccessible(true);
					f.set(editUSSD, R.drawable.n_cursor);
				} catch (Exception ignored)
				{
				}
				editUSSD.setSelection(editUSSD.getText().length());
			}
			editUSSD.setOnEditorActionListener(new TextView.OnEditorActionListener()
			{
				@Override
				public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent)
				{
					if(i == EditorInfo.IME_ACTION_DONE){
						sendUSSD();
					}
					return false;
				}
			});
			editUSSD.showKeyboard();
		}

		if(null!=editUSSDTitle) {
			editUSSDTitle.setText(R.string.N_USSD_TITLE);
		}


	}

	private void sendUSSD()
	{
		String ussd = editUSSD.getText().toString();
		if(validate()){
			JSONObject commandAddr = new JSONObject();
			try
			{
				commandAddr.put("text", ussd);
				if(nSendMessageToServer(device, Command.USSD, commandAddr, true)){
					onBackPressed();
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}
		}else{
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
			nDialog.setTitle(getString(R.string.N_USSD_CHECK_ENTERED_DATA));
			nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
			{
				@Override
				public void onActionClick(int value, boolean b)
				{
					switch (value){
						case NActionButton.VALUE_CANCEL:
							nDialog.dismiss();
							break;
					}
				}
			});
			nDialog.show();
		}
	}

	private boolean validate()
	{
		return null != editUSSD.getText() && null != editUSSD.getText().toString() && !editUSSD.getText().toString().equals("");
	}

	private void setup()
	{
		buttonClose = (LinearLayout) findViewById(R.id.nButtonClose);
		buttonNext = (LinearLayout) findViewById(R.id.nButtonNext);
		editUSSD = (NEditText) findViewById(R.id.nText);
		editUSSDTitle = (TextView) findViewById(R.id.nTextTitle);
	}

	public void  onResume(){
		super.onResume();
		bind();
		bindD3();
	}

	public void onPause(){
		super.onPause();
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	}
}
