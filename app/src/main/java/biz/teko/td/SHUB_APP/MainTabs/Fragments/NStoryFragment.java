package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;

public class NStoryFragment extends Fragment {
    private static final String IMAGE_RES_BUNDLE = "imageResource";
    private static final String BACKGROUND_BUNDLE = "text";
    private static final String POSITION = "position";
    private NActionButton more;

    public static NStoryFragment newInstance(int imageResource, int text, int position) {
        NStoryFragment storyFragment = new NStoryFragment();
        Bundle arguments = new Bundle();
        arguments.putInt(IMAGE_RES_BUNDLE, imageResource);
        arguments.putInt(BACKGROUND_BUNDLE, text);
        arguments.putInt(POSITION, position);
        storyFragment.setArguments(arguments);
        return storyFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.n_fragment_story, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
    }

    private void init(View view) {
        more = view.findViewById(R.id.nButtonInfo);
        Bundle bundle = getArguments();
        ((ImageView) view.findViewById(R.id.storyImage)).setImageResource(bundle.getInt(IMAGE_RES_BUNDLE, 0));
        view.findViewById(R.id.storyLayout).setBackgroundResource(bundle.getInt(BACKGROUND_BUNDLE, 0));
        setMoreButton(bundle.getInt(POSITION, 0));
    }

    private void setMoreButton(int position) {
        if(null!=more){
            more.setOnButtonClickListener(action -> {
                String uri = getResources().getString(R.string.web_site);
                switch (position)
                {
                    case 2:
                        uri = "https://cloud.security-hub.ru/wiki/doku.php?id=ip-камера";
                        break;
                    case 5:
                        uri = "https://cloud.security-hub.ru/wiki/doku.php?id=уведомление-будильник";
                        break;
                    case 6:
                        uri = "https://cloud.security-hub.ru/wiki/doku.php?id=монитор_тревог_мп";
                        break;
                }
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(browserIntent);
            });
        }
        setButtonPosition(position);
    }

    private void setButtonPosition(int position) {
        switch (position) {
            case 2:
                setMoreButtonPosition(R.drawable.update_info_more_blue, 35, 42);
                break;
            case 5:
            case 6:
                setMoreButtonPosition(R.drawable.update_info_more_white, 36, 44);
                break;
        }
    }

    private void setMoreButtonPosition(int imageId, int start, int bottom) {
        more.setImage(imageId);
        more.setVisibility(View.VISIBLE);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) more.getLayoutParams();
        params.leftMargin = Func.dpToPx(start, getContext());
        params.bottomMargin = Func.dpToPx(bottom, getContext());
    }
}
