package biz.teko.td.SHUB_APP.MainTabs.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;

public class SiteSelectAdapter extends ArrayAdapter
{
	private final Context context;
	private final int layout;
	private final Site[] sites;
	private final int site_id;
	private final LayoutInflater layoutInflater;
	private OnViewItemClick onViewClickListener;

	public enum ACTION{
		SELECT,
		DISMISS
	}

	public interface OnViewItemClick
	{
		void callBackToFragment(ACTION action, int site_id);
	}

	public void setListener(OnViewItemClick onViewItemClick){
		this.onViewClickListener = onViewItemClick;
	}

	public SiteSelectAdapter(Context context, int resource , Site[] sites, int site_id)
	{
		super(context, resource);
		this.context =context;
		this.layout = resource;
		this.sites = sites;
		this.site_id = site_id;
		this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public View getView(final int position, final View convertView, ViewGroup parent)
	{
		View view = convertView;
		if(null==view){
			view = layoutInflater.inflate(layout, null);

		}
		LinearLayout siteCard = (LinearLayout) view.findViewById(R.id.cardButton);

		if (sites[position].id == site_id){
			siteCard.setSelected(true);
			siteCard.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					onViewClickListener.callBackToFragment(ACTION.DISMISS, site_id);
				}
			});
		}else{
			siteCard.setSelected(false);
			siteCard.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					onViewClickListener.callBackToFragment(ACTION.SELECT, sites[position].id);
				}
			});
		}

		TextView siteName = (TextView) view.findViewById(R.id.selectSiteName);
		if(null!=siteName) siteName.setText(sites[position].name);

//		view.setOnClickListener(new View.OnClickListener()
//		{
//			@Override
//			public void onClick(View view)
//			{
//				onViewClickListener.callBackToFragment(ACTION.DISMISS, site_id);
//			}
//		});
		return view;
	}

	public long getItemId(int position){
		return position;
	}

	public int getCount(){
		return sites.length;
	}

	public Site getItem(int position)
	{
		return sites[position];
	}

}
