package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Other.InputFilterMinMax;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardExitSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardDWOTypesSpinnerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardSTypesSpinnerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardSectionsSpinnerAdapter;

public class ExitSetFragment extends Fragment
{
	private int position;
	private Context context;
	private ViewGroup container;
	private WizardExitSetActivity activity;
	private DBHelper dbHelper;
	private View v;
	private D3Service myService;
	private int setDeviceId;
	private int min = 1;
	private int max = 2;
	private int setExitType;
	private int setExitNumber;
	private int setSectionId;
	private int setTypeId;
	private int sending = 0;

	public static ExitSetFragment newInstance(int position){
		ExitSetFragment exitSetFragment = new ExitSetFragment();
		Bundle  b  = new Bundle();
		b.putInt("position", position);
		exitSetFragment.setArguments(b);
		return exitSetFragment;
	}

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				((WizardExitSetActivity)getActivity()).refreshViews();
			}
		}
	};

	private BroadcastReceiver sectionAddReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int section_id = intent.getIntExtra("section", -1);
			final WizardExitSetActivity activity = (WizardExitSetActivity) getActivity();
			final int deviceId = activity.getDeviceId();
			Section[] sections = dbHelper.getSectionsForDeviceWithoutControllerKnownType(deviceId);
			if((sections!=null)&&(sections.length > 0))
			{
				View v = container.getChildAt(0);
				Spinner spinner = (Spinner) v.findViewById(R.id.wizSectionSpinner);
				final WizardSectionsSpinnerAdapter sectionsSpinnerAdapter = new WizardSectionsSpinnerAdapter(context, R.layout.spinner_item_dropdown, sections);
				if(null!=spinner)
				{
					spinner.requestLayout();
					spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
					{
						@Override
						public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
						{
							Section section = sectionsSpinnerAdapter.getItem(position);
							setSectionId = section.id;
							activity.setSectionId(setSectionId);
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent)
						{
							Section section = sectionsSpinnerAdapter.getItem(0);
							setSectionId = section.id;
							activity.setSectionId(setSectionId);
						}
					});
					spinner.setAdapter(sectionsSpinnerAdapter);
					if(section_id != -1){
						setSectionId = section_id;
						Section currentSection = null;
						for(Section section: sections){
							if(section.id == setSectionId){
								currentSection = section;
							}
						}
						if(null!=currentSection){
							spinner.setSelection(sectionsSpinnerAdapter.getPosition(currentSection));
							activity.setSectionId(setSectionId);
						}
					}
				}
			}
		}
	};

	@Override
	public void onResume()
	{
		super.onResume();
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		getActivity().registerReceiver(sectionAddReceiver, new IntentFilter(D3Service.BROADCAST_SECTION_ADD));

	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(commandSendReceiver);
		getActivity().unregisterReceiver(sectionAddReceiver);
	}


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		position = getArguments().getInt("position");
		int layout;
		context = container.getContext();
		this.container = container;
		activity = (WizardExitSetActivity) getActivity();
		dbHelper = DBHelper.getInstance(context);
		switch (position)
		{
			case 0:
				layout = R.layout.fragment_exit_set_2;
				v = inflater.inflate(layout, container, false);
				setFragmentOne(v);
				break;
			case 1:
				/*wired&wireless*/
				layout = R.layout.fragment_exit_set_3;
				v = inflater.inflate(layout, container, false);
				setFragmentTwo(v);
				break;
			default:
				break;
		}

		return v;
	}

	public void setFragmentOne(View fragmentTwo)
	{
		final WizardExitSetActivity activity = (WizardExitSetActivity) getActivity();
		setDeviceId = activity.getDeviceId();
		Device device = dbHelper.getDeviceById(setDeviceId);
		if(null!=device)
		{
			switch (device.type){
				case Const.DEVICETYPE_SH_2:
				case Const.DEVICETYPE_SH_4G:
					max = 6;
					break;
				default:
					max = 2;
					break;
			}
		}

		final EditText editExitNumber = (EditText) v.findViewById(R.id.wizExitNumber);
		final Spinner typesSpinner = (Spinner) v.findViewById(R.id.wizExitTypeSpinner);
		if(null!= typesSpinner && null!= D3Service.d3XProtoConstEvent){
			LinkedList<DWOType> outputTypesList = D3Service.d3XProtoConstEvent.dwotypes;
			DWOType[] dwoTypes = new DWOType[outputTypesList.size()];
			for (int i = 0; i < outputTypesList.size(); i++)
			{
				dwoTypes[i] = outputTypesList.get(i);
			}
			final WizardDWOTypesSpinnerAdapter wizardDWOTypesSpinnerAdapter = new WizardDWOTypesSpinnerAdapter(context, R.layout.spinner_item_dropdown, dwoTypes);
			typesSpinner.setAdapter(wizardDWOTypesSpinnerAdapter);
			typesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
			{
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
				{
					DWOType dwoType = wizardDWOTypesSpinnerAdapter.getItem(position);
					setExitType = dwoType.id;
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent)
				{

				}
			});
		}
		if(null!=editExitNumber){
			editExitNumber.requestFocus();
			editExitNumber.setFilters(new InputFilter[]{new InputFilterMinMax(min, max)});
			editExitNumber.setOnEditorActionListener(new TextView.OnEditorActionListener()
			{
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
					{
						if(null!=typesSpinner)
						{
							editExitNumber.clearFocus();
							Func.hideKeyboard(activity);
							typesSpinner.requestFocus();
						}
						return true;
					}
					return false;
				}
			});
		}

		final Spinner sectionsSpinner = (Spinner) v.findViewById(R.id.wizSectionSpinner);
		final Button newSectionButton = (Button) v.findViewById(R.id.wizZoneAddSectionButton);
		final CheckBox checkSections = (CheckBox) v.findViewById(R.id.wizExitBindToSecCheck);
		final LinearLayout sectionSetLayout = (LinearLayout) v.findViewById(R.id.wizExitBindToSecLayout);
		final TextView sectionSetText = (TextView) v.findViewById(R.id.wizExitBindToSecText);

		if(checkSections != null){
			checkSections.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
			{
				@Override
				public void onCheckedChanged(CompoundButton compoundButton, boolean b)
				{
					if(!b){
						activity.setSectionId(0);
						if(sectionSetLayout !=null && sectionSetText != null){
							sectionSetLayout.setVisibility(View.INVISIBLE);
							sectionSetText.setVisibility(View.VISIBLE);
						}
					}else{
						if(sectionSetLayout !=null && sectionSetText != null){
							sectionSetLayout.setVisibility(View.VISIBLE);
							sectionSetText.setVisibility(View.INVISIBLE);

							Section[] sections = dbHelper.getSectionsForDeviceWithoutControllerKnownType(setDeviceId);
		//					Section[] sections = dbHelper.getAllSectionsForSiteDeviceWithController(setSiteId, setDeviceId);
							if(null!= sections && null!=sectionsSpinner)
							{
								final WizardSectionsSpinnerAdapter sectionsSpinnerAdapter = new WizardSectionsSpinnerAdapter(context, R.layout.spinner_item_dropdown, sections);
								sectionsSpinner.setAdapter(sectionsSpinnerAdapter);
								sectionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
								{
									@Override
									public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
									{
										Section section = sectionsSpinnerAdapter.getItem(position);
										setSectionId = section.id;
										activity.setSectionId(setSectionId);
									}

									@Override
									public void onNothingSelected(AdapterView<?> parent)
									{
										Section section = sectionsSpinnerAdapter.getItem(0);
										setSectionId = section.id;
										activity.setSectionId(setSectionId);
									}
								});
							}

							if(null!=newSectionButton){
								newSectionButton.setOnClickListener(new View.OnClickListener()
								{
									@Override
									public void onClick(View v)
									{
										showSectionAdb(setDeviceId,0, activity);
									}
								});
							}

						}
					}
				}
			});
		}

		final Button next = (Button) v.findViewById(R.id.wizExitNextButton);
		if(null!=next)
		{
			next.setText(R.string.WIZ_GONEXT);
			next.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
							String exitNumber = editExitNumber.getText().toString();
							if (!exitNumber.equals(""))
							{
									activity.setExitNumber(Integer.valueOf(exitNumber));
									activity.setExitType(setExitType);
									activity.setNextPage();
							} else
							{
								Func.nShowMessage(context, context.getString(R.string.WIZ_EXIT_ENTER_EXIT_NUMBER));
							}
						}
			});
		}
	}

	@SuppressLint("SetTextI18n")
	public void setFragmentTwo(View fragmentThree)
	{
		final WizardExitSetActivity activity = (WizardExitSetActivity) getActivity();
		setDeviceId = activity.getDeviceId();
		setExitType = activity.getExitType();
		setExitNumber = activity.getExitNumber();
		setSectionId = activity.getSectionId();

		TextView exitDevice = (TextView) v.findViewById(R.id.wizExitDeviceNameText);
		TextView exitNumber = (TextView) v.findViewById(R.id.wizExitNumberText);
		TextView exitType = (TextView) v.findViewById(R.id.wizExitTypeText);
		TextView sectionNumber = (TextView) v.findViewById(R.id.wizSectionNumberText);
		TextView sectionName = (TextView) v.findViewById(R.id.wizSectionNameText);

		if(null!=exitDevice){
			if(-1!=setDeviceId)
			{
				Device device = dbHelper.getDeviceById(setDeviceId);
				if (null != device)
				{
					exitDevice.setText(device.getType().caption);
				}
			}
		}

		if(null!=exitNumber){
			exitNumber.setText(getString(R.string.WIZ_EXIT_NUMBER_INFO) + " " + Integer.toString(setExitNumber) + " " + getString(R.string.WIZ_EXIT_USE_AS));
		}

		if(null!=exitNumber){
			if(null!=D3Service.d3XProtoConstEvent){
				DWOType dwoType = D3Service.d3XProtoConstEvent.getDWOType(setExitType);
				if(null!=dwoType){
					exitType.setText(dwoType.caption);
				}
			}
		}

		sectionNumber.setText(getString(R.string.WIZ_EXIT_IN_SEC_NUMBER) + " " + Integer.toString(setSectionId));
		sectionName.setText(dbHelper.getSectionNameByIdWithOptions(setDeviceId, setSectionId));


		final EditText editExitName = (EditText) v.findViewById(R.id.wizExitName);
		final Button next = (Button) v.findViewById(R.id.wizExitNextButton);

		editExitName.requestFocus();
		if(null!=editExitName){
			editExitName.requestFocus();
			editExitName.setOnEditorActionListener(new TextView.OnEditorActionListener()
			{
				@Override
				public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
				{
					if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
					{
						if(null!=next)
						{
							Func.hideKeyboard(activity);
						}
						return true;
					}
					return false;
				}
			});
		}

		if(null!=next)
		{
			next.setText(R.string.SET_EXIT_BUTTON);
			next.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					String exitName = editExitName.getText().toString();
					if (!exitName.equals(""))
					{
						JSONObject commandAddr = new JSONObject();
						try
						{
							DWOType dwoType = D3Service.d3XProtoConstEvent.getDWOType(setExitType);
							commandAddr.put("uid", String.format("%02X", 5) + String.format("%02X", setExitNumber) + String.format("%02X", dwoType.o_type));
							commandAddr.put("name", exitName);
							commandAddr.put("section", setSectionId);
							commandAddr.put("detector", dwoType.id);
							nSendMessageToServer(dbHelper.getDeviceById(setDeviceId), Command.ZONE_SET, commandAddr, true);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
					} else
					{
						Func.nShowMessage(context, getString(R.string.WIZ_EXIT_ERROR_ENTER_NAME));
					}
				}
			});
		}
	}

	private AlertDialog showSectionAdb(final int deviceId, int source, final WizardExitSetActivity activity)
	{
		final UserInfo userInfo = dbHelper.getUserInfo();
		AlertDialog.Builder adb = 	Func.adbForCurrentSDK(context);
		switch (source){
			case 0:
				adb.setTitle(R.string.SETA_ADD_SECTION_TITLE);
				break;
			case 1:
				adb.setMessage(R.string.DSF_NO_SECTIONS_MESSAGE);
				break;
		}
		adb.setPositiveButton(R.string.ADB_ADD, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{

			}
		});
		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Func.hideKeyboard(activity);
				dialog.dismiss();
			}
		});
		final AlertDialog ad = adb.create();

		View view = activity.getLayoutInflater().inflate(R.layout.dialog_section_add, null);
		final EditText editName = (EditText) view.findViewById(R.id.editName);
		editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
		int editNameMaxLength = 32;
		editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
		editName.setOnKeyListener(new View.OnKeyListener()
		{

			public boolean onKey(View v, int keyCode, KeyEvent event)
			{
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
				{
					// Perform action on Enter key press
					editName.clearFocus();
					return true;
				}
				return false;
			}
		});

		if(null!=D3Service.d3XProtoConstEvent)
		{
			LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
			LinkedList<SType> sTypes = new LinkedList<SType>();
			Device device = dbHelper.getDeviceById(deviceId);
			if(device.canBeSetFromApp())
			{
				for (SType sType: sTypesList)
				{
					if(null!=sType)
					{
						if (sType.id != 4 && sType.id !=6)
						{
							sTypes.add(sType);
						}
					}
				}
			}else{
				sTypes = sTypesList;
			}

			Spinner sTypeSpinner = (Spinner) view.findViewById(R.id.wizSectionTypeSpinner);
			if (null != sTypeSpinner)
			{
				final WizardSTypesSpinnerAdapter wizardSTypesSpinnerAdapter = new WizardSTypesSpinnerAdapter(context, R.layout.spinner_item_dropdown, sTypes);
				sTypeSpinner.setAdapter(wizardSTypesSpinnerAdapter);
				sTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
				{
					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
					{
						SType sType = wizardSTypesSpinnerAdapter.getItem(position);
						setTypeId = sType.id;
						activity.setSectionType(setTypeId);
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent)
					{
						SType sType = wizardSTypesSpinnerAdapter.getItem(0);
						setTypeId = sType.id;
						activity.setSectionType(setTypeId);
					}
				});
			}
		}
		ad.setView(view);
		ad.show();
		ad.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

		ad.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String name = editName.getText().toString();
				if (!name.equals(""))
				{

					JSONObject commandAddr = new JSONObject();
					try
					{
						commandAddr.put("type", setTypeId);
						commandAddr.put("name", name);
						if(nSendMessageToServer(dbHelper.getDeviceById(deviceId), Command.SECTION_SET, commandAddr, true)){
							ad.dismiss();
						}

					} catch (JSONException e)
					{
						e.printStackTrace();
					}
				} else
				{
					Func.pushToast(context, getString(R.string.SETA_ENTER_SEC_NUM), activity);
				}
			}

		});
		return  ad;
	}

	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}


	public D3Service getLocalService()
	{
		WizardExitSetActivity activity = (WizardExitSetActivity)  getActivity();
		if(activity != null){
			return activity.getLocalService();
		}
		return null;
	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};
}
