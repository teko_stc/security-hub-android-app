package biz.teko.td.SHUB_APP.Cameras.Activities.IV;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.BuildConfig;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 02.03.2018.
 */

public class CamerasLoginActivity extends BaseActivity
{
	private WebView mWebView;
	private static final String baseUrl = BuildConfig.IV_URL;
	private String session;
	private Context context;
	private  String s;
	private PrefUtils prefUtils;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cameras_web);
		this.context = this;
		this.prefUtils = PrefUtils.getInstance(context);
		session = getIntent().getStringExtra("session");
		String url = baseUrl + "session=" + session;

		String lang = prefUtils.getCurrentLang();
		url = url + "&lang=" + lang;

		mWebView = (WebView) findViewById(R.id.webView);
		// включаем поддержку JavaScript
		mWebView.getSettings().setJavaScriptEnabled(true);
		mWebView.getSettings().setUserAgentString("Android WebView");

		mWebView.setWebChromeClient(new WebChromeClient());
		mWebView.setWebViewClient(new WebViewClient(){

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest webResourceRequest)
			{
				return false;
			}

			@Override
			public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
//				handler.proceed(); // Ignore SSL certificate errors
				final AlertDialog.Builder builder = Func.adbForCurrentSDK(context);
				builder.setMessage(R.string.CAMERA_MESSAGE_SSL_ERROR);
				builder.setPositiveButton(R.string.ADB_CONTINUE, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						handler.proceed();
					}
				});
				builder.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						handler.cancel();
						onBackPressed();
					}
				});
				final AlertDialog dialog = builder.create();
				dialog.show();
			}

			@Override
			public void onPageFinished(WebView view, String url)
			{
				super.onPageFinished(view, url);
				if (url.contains("code=")) {
					Intent intent = new Intent();
					setResult(RESULT_OK, intent);
					onBackPressed();
				}
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon)
			{
				super.onPageStarted(view, url, favicon);
				view.setInitialScale(getScale());
			}


		});
		mWebView.clearCache(true);
		if (android.os.Build.VERSION.SDK_INT >= 21) {
			CookieManager.getInstance().removeAllCookies(null);
		}else {
			CookieManager.getInstance().removeAllCookie();
			CookieManager.getInstance().removeSessionCookie();
		}
		if (android.os.Build.VERSION.SDK_INT >= 21) {
			CookieManager.getInstance().setAcceptThirdPartyCookies(mWebView, true);
		}else {
			CookieManager.getInstance().setAcceptCookie(true);
		}
		// указываем страницу загрузки
		mWebView.loadUrl(url);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
			Func.updateContext(context);
		}

		String llang = Locale.getDefault().getLanguage();
		if(null == llang){
			llang = Locale.getDefault().getDisplayLanguage();
		}

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.CAMERA_TITLE_IV_REG);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});
	}

	private int getScale(){
		Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		int width = display.getWidth();
		Double val = new Double(width)/new Double(360);
		val = val * 100d;
		return val.intValue();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		((App)this.getApplication()).stopActivityTransitionTimer();
	}

	@Override
	public void onBackPressed() {
//		if(mWebView.canGoBack()) {
//			mWebView.goBack();
//		} else {
			super.onBackPressed();
//		}
	}

}
