package biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces;

import biz.teko.td.SHUB_APP.Profile.LocalConfig.UdpDevice;

public interface ILoadDevice extends ILoadConfigBase{
//    void startSetAPNActivity(HashMap<String, String> map);
    void startConfigChooseActivity(String ip, int pin);
	void updateList(UdpDevice udpDevice);
}
