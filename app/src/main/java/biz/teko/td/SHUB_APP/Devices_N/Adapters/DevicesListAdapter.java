package biz.teko.td.SHUB_APP.Devices_N.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.util.Pair;
import android.view.View;
import android.widget.ListView;
import android.widget.ResourceCursorAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Devices_N.Activities.DeviceActivity;
import biz.teko.td.SHUB_APP.Devices_N.ServerConnection;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Relays_N.Activities.RelayActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Zones_N.Activities.ZoneActivity;

public class DevicesListAdapter extends ResourceCursorAdapter
{
	private final DBHelper dbHelper;
	private final UserInfo userInfo;
	private Cursor cursor;
	private final Context context;
	private final ServerConnection serverConnection;
	private final int sending = 0;
	private boolean transition = true;
	private boolean device_show = true;

	public DevicesListAdapter(Context context, int layout, Cursor c, int flags, ServerConnection serverConnection, boolean transition, boolean device_show)
	{
		super(context, layout, c, flags);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.serverConnection = serverConnection;
		this.userInfo = dbHelper.getUserInfo();
		this.transition = transition;
		this.device_show = device_show; // for nr list
	}

	public void update(Cursor cursor)
	{
		this.changeCursor(cursor);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor)
	{
		this.cursor = cursor;
		Zone zone = new Zone(cursor, cursor.getInt(14));
		if(null!=zone){
			Site site = dbHelper.getSiteById(zone.site);
			Device device = dbHelper.getDeviceById(zone.device_id);

			NWithAButtonElement elementView = (NWithAButtonElement) view.findViewById(R.id.nDevicesListElement);
//			elementView.refreshStates();
			setArmStatus(elementView, zone, device);
//			setCritStatus(elementView, zone);

			elementView.setTitle(getTitle(zone, device));
			elementView.setSubtitle(null!=site ? site.name : context.getResources().getString(R.string.SITE_NULL));
			elementView.setActionImageNotAnim(!((zone.isManualControllable() || zone.id == 0 || zone.id == 100) && zone.section_id == 0));

			elementView.setDescImage(getDescImage(device, zone));

			elementView.setOnChildClickListener((action, option) -> {
				if((zone.id == 0 || zone.id == 100)&& zone.section_id == 0){
					showDialog(site, device, zone, elementView, R.layout.n_dialog_arm);
				}else{
					if(zone.isManualControllable()){
						showDialog(site, device, zone, elementView,R.layout.n_dialog_on_off);
					}else{
						openZone(site, device, zone, elementView);
					}
				}
			});

			LinkedList<Event> affects = dbHelper.nGetAffectsByZone(zone.device_id, zone.section_id, zone.id);
			elementView.setCritStates(affects);
			elementView.setOnRootClickListener(() -> openZone(site, device, zone, elementView));
		}
	}

	private Drawable getDescImage(Device device, Zone zone)
	{
		int descImage = 0;
		if((zone.id == 0 || zone.id ==100) && zone.section_id == 0){
			descImage = R.drawable.n_image_controller_common_list_selector;
			if(null!=device){
				DeviceType deviceType = device.getType();
				if(null!=deviceType){
					if(null!=deviceType.icons){
						descImage = deviceType.getListIcon(context);
					}
				}
			}
		}else{
			descImage = R.drawable.n_image_device_common_list_selector; // TODO set common image for sensor
			switch (zone.getDetector()){
				case Const.DETECTOR_AUTO_RELAY:
				case Const.DETECTOR_MANUAL_RELAY:
					descImage = R.drawable.n_image_relay_list_selector;
					break;
				case Const.DETECTOR_SIREN:
					descImage = R.drawable.n_image_siren_list_selector;
					break;
				case Const.DETECTOR_LAMP:
					descImage = R.drawable.n_image_lamp_list_selector;
					break;
				case Const.DETECTOR_KEYPAD:
				case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
					descImage = R.drawable.n_image_keypad_list_selector;
					break;
				default:
					break;
			}
			int model = zone.getModelId();
			switch (model){
				case Const.SENSORTYPE_WIRED_OUTPUT:
					DWOType dwoType = zone.getDWOType();
					if (null != dwoType && null != dwoType.icons)
					{
						descImage = dwoType.getListIcon(context);
					}else if(zone.detector != Const.DETECTOR_AUTO_RELAY && zone.detector != Const.DETECTOR_MANUAL_RELAY){
						descImage = R.drawable.n_image_wired_output_common_list_selector;
					}
					break;
				case Const.SENSORTYPE_WIRED_INPUT:
					DWIType dwiType = zone.getDWIType();
					if (null != dwiType && null != dwiType.icons)
					{
						descImage = dwiType.getListIcon(context);
						DType dType = dwiType.getDetectorType(zone.getDetector());
						if(null!=dType){
							AType aType = dType.getAlarmType(zone.getAlarm());
							if(null!=aType && null!=aType.icons){
								descImage = aType.getListIcon(context);
							}
						}
					}else{
						descImage = R.drawable.n_image_wired_input_common_list_selector;
					}
				case Const.SENSORTYPE_UNKNOWN:
					break;
				default:
					ZType zType = zone.getModel();
					if (null != zType && null != zType.icons)
					{
						descImage = zType.getListIcon(context);
						DType dType = zType.getDetectorType(zone.getDetector());
						if(null!=dType){
							AType aType = dType.getAlarmType(zone.getAlarm());
							if(null!=aType && null!=aType.icons){
								descImage = aType.getListIcon(context);
							}
						}
					}else{
						descImage = R.drawable.n_image_device_common_list_selector;
					}
					break;
			}
		}

		return 0!=descImage ? context.getResources().getDrawable(descImage) : null;
	}

	private void showDialog(Site site, Device device, Zone zone, NWithAButtonElement elementView, int layout)
	{
		NDialog nDialog = new NDialog(context, layout);
		NDialog finalNDialog = nDialog;
		finalNDialog.setTitle(getTitle(zone, device));
		finalNDialog.setSubTitle(site.name);
		finalNDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_ARM:
						sendMessageToServer(device, Command.ARM, new JSONObject(), true);
						finalNDialog.dismiss();
						break;
					case NActionButton.VALUE_DISARM:
						sendMessageToServer(device, Command.DISARM, new JSONObject(), true);
						finalNDialog.dismiss();
						break;
					case NActionButton.VALUE_ON:
						JSONObject commandAddress = new JSONObject();
						try
						{
							commandAddress.put("section", zone.section_id);
							commandAddress.put("zone", zone.id);
							commandAddress.put("state", 1);
							sendMessageToServer(zone, Command.SWITCH, commandAddress, true);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						finalNDialog.dismiss();
						break;
					case NActionButton.VALUE_OFF:
						commandAddress = new JSONObject();
						try
						{
							commandAddress.put("section", zone.section_id);
							commandAddress.put("zone", zone.id);
							commandAddress.put("state", 0);
							sendMessageToServer(zone, Command.SWITCH, commandAddress,true);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						finalNDialog.dismiss();
						break;
					case NActionButton.VALUE_INFO:
						openZone(site, device, zone, elementView);
						finalNDialog.dismiss();
						break;
				}
			}
		});
		finalNDialog.show();
	}

	private void setCritStatus(NWithAButtonElement elementView, Zone zone)
	{
		boolean alarm = dbHelper.nGetAlarmEventsByZone(zone);
		boolean attention = dbHelper.nGetAttentionEventsByZone(zone);
		if(alarm){
			elementView.setCrit(0);
		}
		else if(attention){
			elementView.setCrit(1);
		}else{
			elementView.setCrit(-1);
		}
	}

	private void setArmStatus(NWithAButtonElement elementView, Zone zone, Device device)
	{
		elementView.setNoArm(true);
		switch (device.type){
			case Const.DEVICETYPE_SH:
			case Const.DEVICETYPE_SH_1:
			case Const.DEVICETYPE_SH_1_1:
			case Const.DEVICETYPE_SH_2:
			case Const.DEVICETYPE_SH_4G:
				if(zone.section_id == 0 && (zone.id == 0 || zone.id == 100))
				{
					setDeviceArmStatus(elementView, device);
				}else{
					setZoneArmStatus(elementView, zone);
				}
				break;
			default:
				if(zone.section_id ==0 && zone.id == 0){
					setDeviceArmStatus(elementView, device);
				}else{
					setZoneArmStatus(elementView, zone);
				}
				break;
		}
	}

	private void setZoneArmStatus(NWithAButtonElement elementView, Zone zone)
	{
		switch (zone.getDetector()){
			case Const.DETECTOR_AUTO_RELAY:
				if(zone.getModelId() == Const.SENSORTYPE_WIRED_OUTPUT){
					if(dbHelper.getScriptBindedStatusForRelay(zone)){
						elementView.setScripted(true);
					}else{
						elementView.setAuto(true);
					}
					break;
				}
			case Const.DETECTOR_MANUAL_RELAY:
				int status = dbHelper.getRelayStatusById(zone.device_id, zone.section_id, zone.id);
				if(Const.STATUS_ON == status){
					elementView.setOn(true);
				}else{
					elementView.setOff(true);
				}
				break;
			case Const.DETECTOR_LAMP:
			case Const.DETECTOR_SIREN:
			case Const.DETECTOR_KEYPAD:
			case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
			case Const.DETECTOR_BIK:
				elementView.setNoArm(true);
				break;
			default:
				Section section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
				if(null!=section){
					SType sType = section.getType();
					if(null!=sType){
						switch (sType.id){
							case Const.SECTIONTYPE_GUARD:
								if(section.armed > 0){
									elementView.setArmed(true);
								}else{
									elementView.setDisarmed(true);
								}
								break;
							case Const.SECTIONTYPE_TECH_CONTROL:
								if(section.armed > 0){
									elementView.setControl(true);
								}else{
									elementView.setNoControl(true);
								}
								break;
							default:
								elementView.setNoArm(true);
								break;
						}
					}else{
						if(section.armed > 0){
							elementView.setArmed(true);
						}else{
							elementView.setDisarmed(true);
						}
					}
				}
				break;
		}
	}

	private void setDeviceArmStatus(NWithAButtonElement elementView, Device device)
	{
		switch (dbHelper.getDeviceArmStatus(device.id)){
			case Const.STATUS_DISARMED:
				elementView.setDisarmed(true);
				break;
			case Const.STATUS_ARMED:
				elementView.setArmed(true);
				break;
			case Const.STATUS_PARTLY_ARMED:
				elementView.setPartlyArmed(true);
				break;
		}
	}

	private String getTitle(Zone zone, Device device)
	{
		if(zone.section_id == 0 && (zone.id == 0 || zone.id == 100))
		{
			return device.getName();
		}else {
			return zone.name;
		}
	}

	public void openTutorialDevice(Cursor cursor, ListView listView) {
		Zone zone = null;
		if(null!=cursor) zone = new Zone(cursor, cursor.getInt(14));
		if(null != zone) {
			Site site = dbHelper.getSiteById(zone.site);
			Device device = dbHelper.getDeviceById(zone.device_id);
			NWithAButtonElement view = getView(0, null, listView).findViewById(R.id.nDevicesListElement);
			openDeviceActivity(site, device, zone, view);
		}
	}

	private void openZone(Site site, Device device, Zone zone, NWithAButtonElement elementView)
	{
		if(zone.section_id == 0 && (zone.id == 0 || zone.id == 100)){
			if(device_show)openDeviceActivity(site, device, zone, elementView);
		}else{
			switch (zone.detector){
				case 13:
				case 23:
					openRelayActivity(site, device, zone, elementView);
					break;
				default:
					openZoneActivity(site, device,  zone, elementView);
					break;
			}
		}
	}

	private void openDeviceActivity(Site site, Device device, Zone zone, NWithAButtonElement elementView)
	{
		Intent intent = new Intent(context, DeviceActivity.class);
		intent.putExtra("bar_title", getTitle(zone, device));
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		if(transition){
			intent.putExtra("transition", true);
			intent.putExtra("transition_start", getTransitionStartSize(elementView));
			context.startActivity(intent, getTransitionOptions(elementView).toBundle());
		}else{
			context.startActivity(intent);
		}
	}


	private void openRelayActivity(Site site, Device device, Zone zone, NWithAButtonElement elementView)
	{
		Intent intent = new Intent(context, RelayActivity.class);
		intent.putExtra("bar_title", getTitle(zone, device));
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		if(transition){
			intent.putExtra("transition", true);
			intent.putExtra("transition_start", getTransitionStartSize(elementView));
			context.startActivity(intent, getTransitionOptions(elementView).toBundle());
		}else{
			context.startActivity(intent);
		}
	}

	private void openZoneActivity(Site site, Device device, Zone zone, NWithAButtonElement elementView)
	{
		Intent intent = new Intent(context, ZoneActivity.class);
		intent.putExtra("bar_title", getTitle(zone, device));
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		if(transition){
			intent.putExtra("transition", true);
			intent.putExtra("transition_start", getTransitionStartSize(elementView));
			context.startActivity(intent, getTransitionOptions(elementView).toBundle());
		}else{
			context.startActivity(intent);
		}
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((Activity) context,
				new Pair<>(((NWithAButtonElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	private int getTransitionStartSize(View view){
		return  Func.getTextSize(context, ((NWithAButtonElement)view).getTitleView());
	}

	public boolean sendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		Activity activity = (Activity) context;
		if(serverConnection != null && activity!= null){
			serverConnection.nSendMessageToServer(d3Element, Q, data, command);
		}
		return false;
	}
}
