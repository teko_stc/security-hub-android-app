package biz.teko.td.SHUB_APP.Wizards.Adapter.Pager;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import biz.teko.td.SHUB_APP.Wizards.Activity.DomainUsers.WizardDomainUserSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Fragments.DomainUserSetFragment;

public class DomainUserSetViewPagerAdapter extends FragmentPagerAdapter
{
	public DomainUserSetViewPagerAdapter(FragmentManager fm)
	{
		super(fm);
	}

	@Override
	public Fragment getItem(int position)
	{
		return DomainUserSetFragment.newInstance(position);
	}

	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}

	@Override
	public int getItemPosition(@NonNull Object object)
	{
		return POSITION_NONE;
	}

	@Override
	public int getCount()
	{
		return WizardDomainUserSetActivity.DOMAIN_USER_SET_PAGES_COUNT;
	}
}
