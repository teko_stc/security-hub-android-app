package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK;

import android.util.Log;

import com.company.NetSDK.CB_fSearchDevicesCB;
import com.company.NetSDK.DEVICE_NET_INFO_EX;
import com.company.NetSDK.INetSDK;
import com.company.NetSDK.NET_IN_INIT_DEVICE_ACCOUNT;
import com.company.NetSDK.NET_OUT_INIT_DEVICE_ACCOUNT;

import biz.teko.td.SHUB_APP.Utils.Dir.Const;

public class NDahuaInitDevAccountModule
{
	public long lDevSearchHandle = 0;
	///Search device
	public long startSearchDevices(CB_fSearchDevicesCB callback) {

		lDevSearchHandle = INetSDK.StartSearchDevices(callback);
		if(lDevSearchHandle == 0) {
			Log.d(Const.LOG_TAG, "StartSearchDevices Failed!" + INetSDK.GetLastError());
		}

		return lDevSearchHandle;
	}

	///Stop search device
	public void stopSearchDevices() {
		if(lDevSearchHandle != 0) {
			if (INetSDK.StopSearchDevices(lDevSearchHandle)) {
				lDevSearchHandle = 0;
				Log.d(Const.LOG_TAG, "StopSearchDevices Succeed!");
			} else {
				Log.d(Const.LOG_TAG, "StopSearchDevices Failed!" + INetSDK.GetLastError());
			}
		}
	}

	public int initDevAccount(DEVICE_NET_INFO_EX mDeviceInfo, String username, String password, String mInitPhoneOrMail) {
		boolean bRet = false;

		NET_IN_INIT_DEVICE_ACCOUNT inInit = new NET_IN_INIT_DEVICE_ACCOUNT();
		System.arraycopy(mDeviceInfo.szMac, 0, inInit.szMac, 0, mDeviceInfo.szMac.length);

		System.arraycopy(username.getBytes(), 0, inInit.szUserName, 0, username.getBytes().length);

		System.arraycopy(password.getBytes(), 0, inInit.szPwd, 0, password.getBytes().length);

		if((mDeviceInfo.byPwdResetWay >> 1 & 0x01) == 0) {
			System.arraycopy(mInitPhoneOrMail.getBytes(), 0, inInit.szCellPhone, 0, mInitPhoneOrMail.getBytes().length);
		} else if((mDeviceInfo.byPwdResetWay >> 1 & 0x01) == 1) {
			System.arraycopy(mInitPhoneOrMail.getBytes(), 0, inInit.szMail, 0, mInitPhoneOrMail.getBytes().length);
		}

		inInit.byPwdResetWay = mDeviceInfo.byPwdResetWay;

		NET_OUT_INIT_DEVICE_ACCOUNT outInit = new NET_OUT_INIT_DEVICE_ACCOUNT();

		bRet = INetSDK.InitDevAccount(inInit, outInit, 5000, null);
		if(bRet) {
			Log.d(Const.LOG_TAG, "InitDevAccount Succeed!");
		} else {
			Log.d(Const.LOG_TAG, "InitDevAccount Failed!" + " " + INetSDK.GetLastError());
		}
		return bRet ? 1 : INetSDK.GetLastError();
	}

	public boolean initDevAccountByIP(DEVICE_NET_INFO_EX mDeviceInfo, String username, String password, String mInitPhoneOrMail) {
		boolean bRet = false;
		NET_IN_INIT_DEVICE_ACCOUNT inInit = new NET_IN_INIT_DEVICE_ACCOUNT();

		System.arraycopy(mDeviceInfo.szMac, 0, inInit.szMac, 0, mDeviceInfo.szMac.length);

		System.arraycopy(username.getBytes(), 0, inInit.szUserName, 0, username.getBytes().length);

		System.arraycopy(password.getBytes(), 0, inInit.szPwd, 0, password.getBytes().length);

		if((mDeviceInfo.byPwdResetWay >> 1 & 0x01) == 0) {
			System.arraycopy(mInitPhoneOrMail.getBytes(), 0, inInit.szCellPhone, 0, mInitPhoneOrMail.getBytes().length);
		} else if((mDeviceInfo.byPwdResetWay >> 1 & 0x01) == 1) {
			System.arraycopy(mInitPhoneOrMail.getBytes(), 0, inInit.szMail, 0, mInitPhoneOrMail.getBytes().length);
		}

		inInit.byPwdResetWay = mDeviceInfo.byPwdResetWay;

		NET_OUT_INIT_DEVICE_ACCOUNT outInit = new NET_OUT_INIT_DEVICE_ACCOUNT();

		String szDeviceIP = new String(mDeviceInfo.szIP).trim();

		bRet = INetSDK.InitDevAccountByIP(inInit, outInit, 5000, null, szDeviceIP);
		if(bRet) {
			Log.d(Const.LOG_TAG, "InitDevAccountByIP Succeed!");
		} else {
			Log.d(Const.LOG_TAG, "InitDevAccountByIP Failed!" );
		}

		return bRet;
	}
}
