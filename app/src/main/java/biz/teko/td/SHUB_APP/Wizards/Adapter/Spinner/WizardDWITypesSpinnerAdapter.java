package biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class WizardDWITypesSpinnerAdapter extends ArrayAdapter<DWIType>
{
	private final Context context;
	private final DWIType[] dwiTypes;

	public WizardDWITypesSpinnerAdapter(@NonNull Context context, int resource, DWIType[] dwiTypes)
	{
		super(context, resource, dwiTypes);
		this.context = context;
		this.dwiTypes = dwiTypes;
	}

	public int getCount(){
		return dwiTypes.length;
	}

	public DWIType getItem(int position){
		return dwiTypes[position];
	}

	public long getItemId(int position){
		return position;
	}

	@Override
	public int getPosition(@Nullable DWIType item)
	{
		return super.getPosition(item);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		label.setText(dwiTypes[position].caption);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		label.setText(dwiTypes[position].caption);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}
}
