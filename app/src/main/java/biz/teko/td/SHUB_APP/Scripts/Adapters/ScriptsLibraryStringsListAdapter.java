package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.R;

public class ScriptsLibraryStringsListAdapter extends BaseAdapter
{

	private final Context context;
	private final int resource;
	private final String[] strings;
	private final LayoutInflater layoutInflater;
	private final SharedPreferences sp;
	private final String iso;

	public ScriptsLibraryStringsListAdapter(Context context, int resource, String[] strings)
	{
		this.context = context;
		this.resource = resource;
		this.strings = strings;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View view = layoutInflater.inflate(resource, parent, false);
		TextView textView = (TextView) view.findViewById(R.id.textName);
		String cat = Func.getScriptDataForCurrLang(getItem(position), iso);
		if(null!=cat && !cat.equals("")&&cat.length()>0)
		{
			textView.setText(cat.substring(1).replace("/", ": "));
		}else{
			textView.setText(context.getString(R.string.UNKNOWN));
		}
		return view;
	}

	public int getCount() {
		return strings.length;
	}

	@Override
	public String getItem(int position){
		return strings[position];
	}

	public long getItemId(int position){
		return position;
	}
}
