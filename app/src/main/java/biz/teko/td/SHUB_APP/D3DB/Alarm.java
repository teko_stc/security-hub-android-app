package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

import org.json.JSONObject;

import java.util.LinkedList;

public class Alarm
{
	public int id;
	public int device;
	public int state;
	public int type;
	public long create_time;
	public int managed_user;
	public String extra;
	public LinkedList<Event> events;

	Zone zone;

	public Alarm(){

	}

	public Alarm(int id, int device, int state, int type, int create_time, int managed_user, String extra){
		this.id = id;
		this.device =device;
		this.state = state;
		this.type = type;
		this.create_time = create_time;
		this.managed_user = managed_user;
		this.extra = extra;
	}

	public Alarm(Cursor cursor){
		this.id = cursor.getInt(1);
		this.device = cursor.getInt(2);
		this.state = cursor.getInt(3);
		this.type = cursor.getInt(4);
		this.create_time = cursor.getLong(5);
		this.managed_user  = cursor.getInt(6);
		this.extra = cursor.getString(7);
	}

	public Alarm(JSONObject alarmObject){

	}
}
