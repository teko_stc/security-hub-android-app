package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;

public class NElementSelectAdapter extends ArrayAdapter
{
	private final Context context;
	private final D3Element[] elements;
	private final LayoutInflater layoutInflater;
	private final int layout;
	private final DBHelper dbHelper;
	private OnItemClickListener onItemClickListener;

	public void setOnItemClickListener(OnItemClickListener onItemClickListener){
		this.onItemClickListener = onItemClickListener;
	}

	public interface OnItemClickListener{
		void onClick(int id);
	}

	public NElementSelectAdapter(Context context, int resource, D3Element[] elements)
	{
		super(context, resource);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.layout = resource;
		this.elements = elements;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null == view){
			view = layoutInflater.inflate(layout, parent, false);
		}

		D3Element d3Element = elements[position];



		if(null!=d3Element)
		{
			if(d3Element instanceof  Site){
				Site site = (Site) d3Element;
				NWithAButtonElement nWithAButtonElement = view.findViewById(R.id.nDevicesListElement);
				nWithAButtonElement.setTitle(site.name);
				nWithAButtonElement.setSubtitle(null);
				nWithAButtonElement.setDescImage(context.getResources().getDrawable(R.drawable.ic_main_site_list));
				nWithAButtonElement.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						onItemClickListener.onClick(site.id);
					}
				});
			}else if(d3Element instanceof  Device){
				Device device = (Device) d3Element;
				Site site = dbHelper.getSiteByDeviceSection(device.id, 0);
				NWithAButtonElement nWithAButtonElement = view.findViewById(R.id.nDevicesListElement);
				if (null != nWithAButtonElement)
				{
					nWithAButtonElement.setEnabled(true);
					nWithAButtonElement.setOffline(false);
					nWithAButtonElement.setTitle(device.getName());
					nWithAButtonElement.setSubtitle(null!=site ? site.name : context.getResources().getString(R.string.SITE_NULL));
					nWithAButtonElement.setActionImageNotAnim(true);
					nWithAButtonElement.setDescImage(getDescImage(device));
					switch (dbHelper.getDeviceArmStatus(device.id)){ //свич тут , чтобы не вводить в заблуждение при нажатии - отклик на то состояние, в котором устройство при открытии списка
						case Const.STATUS_DISARMED:
							nWithAButtonElement.setDisarmed(true);
							nWithAButtonElement.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
							{
								@Override
								public void onRootClick()
								{
									onItemClickListener.onClick(device.id);
								}
							});
							break;
						case Const.STATUS_ARMED:
							nWithAButtonElement.setArmed(true);
							nWithAButtonElement.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
							{
								@Override
								public void onRootClick()
								{
									onItemClickListener.onClick(Const.STATUS_ARMED);
								}
							});
							break;
						case Const.STATUS_PARTLY_ARMED:
							nWithAButtonElement.setPartlyArmed(true);
							nWithAButtonElement.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
							{
								@Override
								public void onRootClick()
								{
									onItemClickListener.onClick(Const.STATUS_PARTLY_ARMED);
								}
							});
							break;
					}
					if(!dbHelper.isDeviceOnline(device.id)){
						nWithAButtonElement.setEnabled(false);
						nWithAButtonElement.setOffline(true);
						nWithAButtonElement.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
						{
							@Override
							public void onRootClick()
							{
								onItemClickListener.onClick(Const.STATUS_OFFLINE);
							}
						});
					}
				}
			}


		}
		return view;
	}

	private Drawable getDescImage(Device device)
	{
		int descImage = R.drawable.n_image_controller_common_list_selector;
		if(null!=device){
			DeviceType deviceType = device.getType();
			if(null!=deviceType){
				if(null!=deviceType.icons){
					descImage = deviceType.getListIcon(context);
				}
			}
		}
		return context.getResources().getDrawable(descImage);
	}

	private void setTextViewText(View view, int id, String string)
	{
		TextView textView = (TextView) view.findViewById(id);
		textView.setText(string);
	}

	public int getCount() {
		return null!= elements ? elements.length : 0;
	}

	public D3Element getItem(int position){
		return elements[position];
	}

	public long getItemId(int position){
		return position;
	}
}
