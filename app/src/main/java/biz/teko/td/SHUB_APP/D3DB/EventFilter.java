package biz.teko.td.SHUB_APP.D3DB;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.LinkedList;
import java.util.Set;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;

/**
 * Created by td13017 on 21.06.2016.
 */
public class EventFilter extends Event
{
	public boolean isEventShouldBeDispInStatusBarInTime(Event event, Context context)
	{

		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
		Set<String> strings = sharedPreferences.getStringSet("notif_types_list", null);
		boolean coincidence = false;

		if(strings!=null){
			if(strings.size()!=0)
			{
				EClass eClass = D3Service.d3XProtoConstEvent.getClassById(event.classId);
				if(eClass !=null)
				{
					for (String s : strings)
					{
						if (s.equals(eClass.caption))
						{
							coincidence = true;
						}
					}
				}
			}
		}
		return coincidence;

//		if((9 == event.classId)
//				||(((6==event.classId)&&(9==event.reasonId)))
//				||(((7==event.classId)&&(21==event.detectorId)&&(5==event.reasonId)))
//				||((event.classId==8)&&(event.detectorId==8)&&(event.reasonId==7))
//				||(5 == event.classId)||(10==event.classId)
//				)
//		{
//			return false;
//		}
//		return true;
	}



	public LinkedList<Event> getShowableEvents(LinkedList<Event> events, SharedPreferences sp)
	{
		LinkedList<Event> showableEvents = new LinkedList<>();
		for (Event event:events){
			EClass eClass = D3Service.d3XProtoConstEvent.getClassById(event.classId);
			if(eClass!=null ){
				if(event.classId < 5)
				{
					if (Func.getBooleanSPDefTrue(sp, "notif_class_status_" + eClass.id))
					{
						showableEvents.add(event);
					}
				}else{
					if (Func.getBooleanSPDefFalse(sp, "notif_class_status_" + eClass.id))
					{
						showableEvents.add(event);
					}
				}
			}
		}
		return showableEvents;
	}
}
