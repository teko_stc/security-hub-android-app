package biz.teko.td.SHUB_APP.Profile.Fragments;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationManagerCompat;

import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;
import biz.teko.td.SHUB_APP.Utils.NotificationUtils;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class ConnectionPreferenceFragment extends PreferenceFragment
{
	private Activity context;
	private DBHelper dbHelper;
	private SharedPreferences sp;
	private NotificationUtils notificationUtils;
	private PrefUtils prefUtils;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		context = getActivity();
		dbHelper  = DBHelper.getInstance(context);
		prefUtils = PrefUtils.getInstance(context);
		notificationUtils = NotificationUtils.getInstance(context);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			addPreferencesFromResource(R.xml.connection_preferences_oreo);

		} else {
			addPreferencesFromResource(R.xml.connection_preferences);
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		NTopToolbarView toolbar = getActivity().findViewById(R.id.toolbar);

		CheckBoxPreference servicePreference = (CheckBoxPreference) findPreference("notif_connection_mode");
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			if (!NotificationManagerCompat.from(context).areNotificationsEnabled()) {
				((NActionButton) toolbar.getViewById(R.id.nDialogButtonSetting))
						.setOnButtonClickListener(view -> {
							Intent intent = new Intent();
							intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
							intent.putExtra("android.provider.extra.APP_PACKAGE", getActivity().getPackageName());
							startActivity(intent);
						});
				((NActionButton) toolbar.getViewById(R.id.nDialogButtonCancel))
						.setOnButtonClickListener(view -> toolbar.setStateInfo(false));
				toolbar.setStateInfo(true);
				servicePreference.setChecked(Func.foregroundMode(context));
				servicePreference.setEnabled(false);
			} else {
				toolbar.setStateInfo(false);
				servicePreference.setEnabled(true);
//				boolean check = checkServiceNotifState();
//				if (prefUtils.getConnectionMode() != check && !check) {
//					preferenceChangeAction(check);
//				}
				servicePreference.setChecked(Func.foregroundMode(context));
				servicePreference.setOnPreferenceChangeListener((preference, newValue) -> {
					preferenceChangeAction((boolean) newValue);
					servicePreference.setChecked(Func.foregroundMode(context));
					return false;
				});
			}
		} else {
			toolbar.setStateInfo(false);
			servicePreference.setEnabled(true);
			servicePreference.setChecked(PrefUtils.getInstance(context).getConnectionMode());
			servicePreference.setOnPreferenceChangeListener((preference, newValue) -> {
				preferenceChangeAction((boolean) newValue);
				servicePreference.setChecked((boolean) newValue);
				return false;
			});
		}
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		ListView list = view.findViewById(android.R.id.list);
		list.setDivider(new ColorDrawable(Color.TRANSPARENT));
		list.setDividerHeight(0);
	}

	private boolean preferenceChangeAction(boolean b) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
			deleteServiceNotifChannel();
			updateServiceChannelId();
			createServiceNotifChannel(b ? NotificationManager.IMPORTANCE_HIGH : NotificationManager.IMPORTANCE_NONE);
		}else{
			prefUtils.setConnectionMode(b);
		}
		Func.restartD3ServiceForced(context, "Connection Preference Fragment");
		return true;
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private boolean checkServiceNotifState() {
		NotificationChannel notificationChannel = NotificationManagerCompat.from(context)
				.getNotificationChannel(getServiceNotifChannelId());
		if (notificationChannel == null) {
			return false;
		} else
			return notificationChannel.getImportance() != NotificationManagerCompat.IMPORTANCE_NONE;
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void createServiceNotifChannel(int importance){
		notificationUtils.createChannelNoVibro(getServiceNotifChannelId(),
				getString(R.string.NOTIF_CONNECTION_SERVICE_ENABLED),
				importance
		);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private  void deleteServiceNotifChannel(){
		notificationUtils.deleteServiceNotifChannel();
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private String getServiceNotifChannelId() {
		return notificationUtils.getServiceNotifChannelId();
	}

	private void updateServiceChannelId() {
		notificationUtils.updateServiceNotifChannelId();
	}
}
