package biz.teko.td.SHUB_APP.Adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.D3DB.Cluster;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 18.05.2017.
 */

public class ClustersSpinnerAdapter extends ArrayAdapter<Cluster>
{
	private final Context context;
	private final Cluster[] clusters;

	public ClustersSpinnerAdapter(Context context, int resource, Cluster[] clusters)
	{
		super(context, resource, clusters);
		this.context = context;
		this.clusters = clusters;
	}

	public int getCount(){
		return clusters.length;
	}

	public Cluster getItem(int position){
		return clusters[position];
	}

	public long getItemId(int position){
		return position;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		label.setText(clusters[position].name);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(0, Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		label.setText(clusters[position].name);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}
}
