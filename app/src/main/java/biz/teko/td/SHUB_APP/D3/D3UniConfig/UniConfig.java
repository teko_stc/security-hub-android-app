package biz.teko.td.SHUB_APP.D3.D3UniConfig;

import android.content.Context;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.InputStream;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 27.06.2016.
 */

@Root(name="UniConfig")
public class UniConfig
{
	@Attribute(name="version")
	public int version;

	@ElementList(entry="config", inline = true)
	public LinkedList<ConfigNode> configNodes;

	public UniConfig(){};

	public void getPreferenceScreenXML(){}

//	public void PushXML()
//	{
//		Serializer s = new Persister();
//		OutputStream stream = new ByteArrayOutputStream();
//		try
//		{
//			this.version = 0;
//			this.configNodes = new LinkedList<>();
//
//			for( int i = 0; i < 4 ; i++){
//
//				ConfigNode configNode = new ConfigNode();
//				configNode.Description = new TextNode();
//				configNode.Description.Value = "DescriptionValue " + i;
//				configNode.Hardware = i;
//				configNode.Software = i;
//				configNode.Name ="ConfigNode SHUB " + i ;
//
//				configNode.Files = new LinkedList<>();
//				for(int y = 0 ; y < 3; y++){
//					FileNode fileNode = new FileNode();
//					fileNode.Name = "File " + y;
//					fileNode.Description = new TextNode();
//					fileNode.Description.Value = "Value " + y;
//					fileNode.Fields = new LinkedList<>();
//					for(int ii = 0; ii <   3 ; ii++){
//						FieldNode fieldNode = new FieldNode();
//						fieldNode.Address= ii;
//						fieldNode.Name = " Field "  + ii;
//						fieldNode.Count= ii;
//						fieldNode.Offset=ii;
//						fieldNode.Size=ii;
//						fieldNode.Source= new TextNode();
//						fieldNode.Source.Value="Source " + ii;
//						fieldNode.Description = new TextNode();
//						fieldNode.Description.Value = "DescriptionValue " + i;
//						fileNode.Fields.addInNormalQueue(fieldNode);
//					}
//					configNode.Files.addInNormalQueue(fileNode);
//				}
//				configNode.Groups = new LinkedList<>();
//				for(int z = 0 ; z<3; z++){
//					InterfaceNodeGroup interfaceNodeGroup = new InterfaceNodeGroup();
//					interfaceNodeGroup.Description = new TextNode();
//					interfaceNodeGroup.Description.Value="Description " + z;
//					interfaceNodeGroup.Caption = "Caption " + z;
//					interfaceNodeGroup.Count=z;
//					interfaceNodeGroup.Readonly = new ConditionNode();
//					interfaceNodeGroup.Readonly.Type = "Type " + z;
//					interfaceNodeGroup.Readonly.Value = " Value " + z;
//					interfaceNodeGroup.Hide = new ConditionNode();
//					interfaceNodeGroup.Hide.Type = "Type " + z;
//					interfaceNodeGroup.Hide.Value = "Value " + z;
//					interfaceNodeGroup.Members = new LinkedList<>();
//					for(int yy = 0; yy < 3; yy++){
//						InterfaceNodeGroup interfaceNodeGroup1 = new InterfaceNodeGroup();
//						interfaceNodeGroup1.Description = new TextNode();
//						interfaceNodeGroup1.Description.Value="Description " + yy;
//						interfaceNodeGroup1.Caption = "Caption " + yy;
//						interfaceNodeGroup1.Count=yy;
//						interfaceNodeGroup1.Readonly = new ConditionNode();
//						interfaceNodeGroup1.Readonly.Type = "Type " + yy;
//						interfaceNodeGroup1.Readonly.Value = " Value " + yy;
//						interfaceNodeGroup1.Hide = new ConditionNode();
//						interfaceNodeGroup1.Hide.Type = "Type " + yy;
//						interfaceNodeGroup1.Hide.Value = "Value " + yy;
//						interfaceNodeGroup.Members.addInNormalQueue(interfaceNodeGroup1);
//					}
//					for(int zz = 0; zz < 3; zz++){
//						InterfaceNodeControl interfaceNodeControl = new InterfaceNodeControl();
//						interfaceNodeControl.Description= new TextNode();
//						interfaceNodeControl.Description.Value = "Description " + zz;
//						interfaceNodeControl.Count= z;
//						interfaceNodeControl.Caption = "Caption";
//						interfaceNodeControl.Field="Field" + zz;
//						interfaceNodeControl.Inverse= "Inverse" + zz;
//						interfaceNodeControl.Password = "Password";
//						interfaceNodeControl.Type="Type" + zz;
//						interfaceNodeControl.Hide= new ConditionNode();
//						interfaceNodeControl.Hide.Value = " Value "  + zz;
//						interfaceNodeControl.Hide.Type = "Type " + zz;
//						interfaceNodeControl.Readonly = new ConditionNode();
//						interfaceNodeControl.Readonly.Value = " Value " + zz;
//						interfaceNodeControl.Readonly.Type = "Typee" + zz;
//						interfaceNodeControl.Action = new LinkedList<>();
//						for ( int  iii = 0; iii< 3; iii++){
//							ActionNode actionNode = new ActionNode();
//							actionNode.Value = "Value " + iii;
//							interfaceNodeControl.Action.addInNormalQueue(actionNode);
//						}
//						interfaceNodeControl.Options = new LinkedList<>();
//						for ( int yyy = 0; yyy < 3; yyy++ ){
//							OptionNode optionNode = new OptionNode();
//							optionNode.Name = " name " + yyy;
//							optionNode.Value = "value " + yyy;
//							interfaceNodeControl.Options.addInNormalQueue(optionNode);
//						}
//						interfaceNodeControl.Verify = new LinkedList<>();
//						for (int zzz = 0; zzz < 3; zzz ++){
//							VerificationNode verificationNode = new VerificationNode();
//							verificationNode.Value = "Value " + zzz;
//							verificationNode.Type=" Type" + zzz;
//							interfaceNodeControl.Verify.addInNormalQueue(verificationNode);
//						}
//						interfaceNodeGroup.Members.addInNormalQueue(interfaceNodeControl);
//
//					}
//					configNode.Groups.addInNormalQueue(interfaceNodeGroup);
//				}
//
//				this.configNodes.addInNormalQueue(configNode);
//			}
//			s.write(this, stream);
//			Log.e("D3---", stream.toString());
//		} catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}

	public static UniConfig LoadXML(Context context){
		InputStream inputStream = context.getResources().openRawResource(R.raw.config);
		Serializer serializer = new Persister();
		try
		{
			return serializer.read(UniConfig.class, inputStream);
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;

	};


















}
