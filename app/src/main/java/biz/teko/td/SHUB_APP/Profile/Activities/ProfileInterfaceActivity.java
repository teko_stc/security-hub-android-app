package biz.teko.td.SHUB_APP.Profile.Activities;

import android.os.Bundle;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Profile.Fragments.InterfacePreferenceFragment;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;

public class ProfileInterfaceActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_interface);
        initView();
        getFragmentManager().beginTransaction().replace(R.id.preference_layout, new InterfacePreferenceFragment()).commit();

    }

    private void initView() {
        NTopToolbarView toolbar = (NTopToolbarView) findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }
}
