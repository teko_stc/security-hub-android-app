package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import biz.teko.td.SHUB_APP.Adapter.ClustersSpinnerAdapter;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Cluster;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Other.DEPager;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.ImagesViewPagerAdapter;

/**
 * Created by td13017 on 23.06.2017.
 */

public class ControllerSetFragment extends Fragment
{
	private int position;
	private Context context;
	private View v;
	private ImagesViewPagerAdapter adapter;
	private DEPager pager;
	private ImageView buttonLeft;
	private ImageView buttonRight;
	private String sn = null;
	private ViewGroup container;
	private LayoutInflater inflater;

	private DBHelper dbHelper;
	private int cluster_id = 0;
	private int sending = 0;
	private D3Service myService;
	private Cluster[] clusters;
	private OnClickListener onClickListener;

	public  interface OnClickListener
	{
		void onFinishSetup();
		void onCameraStart();
		void onNext();
		void onBack();
		void onSkip();
		void onNextSet(boolean b);
		void onBackSet(int position);

		void setSerial(String s);

		void setName(String s);
		void setCluster(int i);
	}

	public void setListener(OnClickListener onClickListener){
		this.onClickListener = onClickListener;
	}

	public static ControllerSetFragment newInstance(int position){
		ControllerSetFragment controllerSetFragment = new ControllerSetFragment();
		Bundle b = new Bundle();
		b.putInt("position", position);
		controllerSetFragment.setArguments(b);
		return  controllerSetFragment;
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	public void onResume()
	{
		super.onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		position = getArguments().getInt("position");
		int layout;
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		this.container = container;
		switch (position)
		{
			case 0:
				layout = R.layout.fragment_controller_set_1;
				v = inflater.inflate(layout, container, false);
				setFragmentOne(v);
				break;
			case 1:
				layout = R.layout.fragment_controller_set_2;
				v = inflater.inflate(layout, container, false);
				setFragmentTwo(v);
				break;
			case 2:
				layout = R.layout.fragment_controller_set_3;
				v = inflater.inflate(layout, container, false);
				setFragmentThree(v);
				break;
			case 3:
				layout = R.layout.fragment_controller_set_4;
				v = inflater.inflate(layout, container, false);
				setFragmentFour(v);
				break;
			default:
				break;
		}
		onClickListener.onBackSet(position);
		return v;
	}

	private void setFragmentFour(View v)
	{
		adapter = new ImagesViewPagerAdapter(((WizardControllerSetActivity) getActivity()).getSupportFragmentManager());
		pager = (DEPager) v.findViewById(R.id.imagesViewPager);
		pager.setPagingEnabled(false);
		pager.setAdapter(adapter);

		buttonLeft = (ImageView) v.findViewById(R.id.imagesButtonLeft);
		buttonLeft.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				goLeft();
			}

			private void goLeft()
			{
				if(pager.getCurrentItem() != 0){
					int position = pager.getCurrentItem();
					pager.setCurrentItem(position - 1);
				}
			}
		});
		buttonRight = (ImageView) v.findViewById(R.id.imagesButtonRight);
		buttonRight.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				goRight();
			}

			private void goRight()
			{
				if(pager.getCurrentItem() != 4)
				{
					int position = pager.getCurrentItem();
					pager.setCurrentItem(position + 1);
				}
			}
		});
	}

	private void setFragmentThree(View v)
	{
	}

	private void setFragmentTwo(View v)
	{
		final EditText editName = (EditText) v.findViewById(R.id.wizContrName);
		final EditText editSerial = (EditText) v.findViewById(R.id.wizContrNumber);

		ImageView qrImage = (ImageView) v.findViewById(R.id.imageButtonQR);

		editSerial.setFocusable(true);
		editSerial.setOnKeyListener(new View.OnKeyListener()
		{
			@Override
			public boolean onKey(View view, int i, KeyEvent keyEvent)
			{
				return false;
			}
		});
		editSerial.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int start, int before, int count)
			{
				String s = charSequence.toString();
				onClickListener.setSerial(s);
			}

			@Override
			public void afterTextChanged(Editable s)
			{

			}
		});

		int editNameMaxLength = 32;
		editName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(editNameMaxLength)});
		editName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

		editName.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
			{
				String s = charSequence.toString();
				onClickListener.setName(s);
			}

			@Override
			public void afterTextChanged(Editable editable)
			{

			}
		});

		qrImage.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onClickListener.onCameraStart();
			}
		});

		Spinner clustersSpinner = (Spinner) v.findViewById(R.id.wiz_clusters_spinner);
		if(!((PreferenceManager.getDefaultSharedPreferences(context).getString("prof_language_value", App.DEFAULT_ISO).equals("ru")))){
			clusters = dbHelper.getFilteredClusters();
		}else
		{
			clusters = dbHelper.getClusters();
		}
		if(null!= clusters)
		{
			final ClustersSpinnerAdapter clustersSpinnerAdapter = new ClustersSpinnerAdapter(context, R.layout.spinner_item_dropdown, clusters);
			clustersSpinner.setAdapter(clustersSpinnerAdapter);
			clustersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
			{
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
				{
					Cluster cluster = clustersSpinnerAdapter.getItem(position);
					cluster_id = cluster.id;
					onClickListener.setCluster(cluster_id);
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent)
				{
					Cluster cluster = clustersSpinnerAdapter.getItem(0);
					cluster_id = cluster.id;
					onClickListener.setCluster(cluster_id);
				}
			});
		}
	}

	private void setFragmentOne(View v) {
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);
		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanResult != null) {
			container.requestLayout();
			View v = container.getChildAt(1);
			final EditText editSerial = (EditText) v.findViewById(R.id.wizContrNumber);
			String re = scanResult.getContents();
			if(null!=re)
			{
				int i = re.length();
				if (3 < i)
				if (3 < i && Func.checkLC2o5(re))
				{
//					re = re.substring(0, re.length() - 1);
					sn = re.substring(1, re.length() - 1);
					Log.e("D3Scan", re);
					editSerial.setText(sn);
					final WizardControllerSetActivity activity = (WizardControllerSetActivity) getActivity();
					if(null!=activity){
						activity.runOnUiThread(new Runnable()
						{
							@Override
							public void run()
							{
								editSerial.requestFocus();
								editSerial.setSelection(sn.length());
							}
						});
					}
				}
			}

		}
	}
}
