package biz.teko.td.SHUB_APP.SitesTabs.Fragments;

import static biz.teko.td.SHUB_APP.Utils.Dir.Func.setDynamicHeight;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SitesTabs.Adapters.AlarmsListAdapter;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/*
* DEPRECATED 18.11.2020
* */

public class StateDialogFragment extends DialogFragment
{
	private Context context;
	private DBHelper dbHelper;
	private OnClickListener onClickListener;
	private OnEventClickListener onEventClickListener;
	private SharedPreferences sp;
	private Cursor activeLostCursor;
	private Cursor activeCursor;
	private Cursor repairCursor;
	private int l_count;
	private int a_count;
	private int r_count;
	private View view;
	private boolean empty;
	private AlarmsListAdapter lostListAdapter;
	private AlarmsListAdapter activeListAdapter;
	private AlarmsListAdapter repairListAdapter;
	private ListView activeList;
	private ListView repairList;
	private ListView lostList;

	public  interface OnClickListener
	{
		void callback(int id);
	}

	public void setListener(OnClickListener onClickListener){
		this.onClickListener = onClickListener;
	}

	public  interface OnEventClickListener
	{
		void callback(int event_id);
	}

	public void setEventClickListener(OnEventClickListener onEventClickListener){
		this.onEventClickListener = onEventClickListener;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{

		context = getContext();
		dbHelper = DBHelper.getInstance(context);
		sp = PreferenceManager.getDefaultSharedPreferences(context);

		view = ((Activity) context).getLayoutInflater().inflate(R.layout.frame_new_events, null);
		ImageView closeHeader = (ImageView) view.findViewById(R.id.closeButton);
		TextView closeFooter = (TextView) view.findViewById(R.id.footerCloseButton);
		if(null!=closeFooter) closeFooter.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				dismiss();
			}
		});
		if(null!=closeHeader) closeHeader.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				dismiss();
			}
		});


		activeList = (ListView) view.findViewById(R.id.list_current);
		repairList = (ListView) view.findViewById(R.id.list_repair);
		lostList = (ListView) view.findViewById(R.id.list_reboot);
		lostList.setDivider(null);

		updatePrefs();
		updateCursors();

		lostListAdapter = new AlarmsListAdapter((Activity)context, R.layout.card_reboot_device, activeLostCursor, 0, true);
		lostList.setAdapter(lostListAdapter);
		lostList.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
			{
				Cursor c = (Cursor) lostList.getItemAtPosition(i);
				if(null!=c){
					final DBHelper dbHelper = DBHelper.getInstance((Activity)context);
					final Device device = dbHelper.getDeviceById(c.getInt(2));

					final LinearLayout infoFrame = (LinearLayout) view.findViewById(R.id.infoView);
					final LinearLayout dialogFrame = (LinearLayout) view.findViewById(R.id.dialogView);
					if(null!=infoFrame) infoFrame.setVisibility(View.INVISIBLE);
					if(null!=dialogFrame) dialogFrame.setVisibility(View.VISIBLE);

					TextView buttonPositive = (TextView) view.findViewById(R.id.buttonPositive);
					TextView buttonNegative = (TextView) view.findViewById(R.id.buttonNegative);

					buttonPositive.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View view)
						{
							onClickListener.callback(device.id);
						}
					});

					buttonNegative.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View view)
						{
							if(null!=infoFrame) infoFrame.setVisibility(View.VISIBLE);
							if(null!=dialogFrame) dialogFrame.setVisibility(View.INVISIBLE);
						}
					});
				}
			}
		});

		activeListAdapter = new AlarmsListAdapter((Activity)context, R.layout.card_alarm, activeCursor, 0, true);
		activeList.setAdapter(activeListAdapter);
		activeList.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
			{
				Cursor c = (Cursor) activeList.getItemAtPosition(i);
				if(null!=c && c.getCount()!=0){
					onEventClickListener.callback(c.getInt(1));
				}
			}
		});
		repairListAdapter = new AlarmsListAdapter((Activity)context, R.layout.card_repaired_alarm, repairCursor, 0, false);
		repairList.setAdapter(repairListAdapter);
		repairList.setOnItemClickListener(new AdapterView.OnItemClickListener()
		{
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
			{
				Cursor c = (Cursor) repairList.getItemAtPosition(i);
				if(null!=c){
					onEventClickListener.callback(c.getInt(1));
				}
			}
		});

		setDynamicHeight(activeList);
		setDynamicHeight(repairList);
		setDynamicHeight(lostList);
		return view;
	}

	public void updateList(){
		updatePrefs();
		updateCursors();
		lostListAdapter.update(activeLostCursor);
		activeListAdapter.update(activeCursor);
		repairListAdapter.update(repairCursor);

		setDynamicHeight(activeList);
		setDynamicHeight(repairList);
		setDynamicHeight(lostList);
	}

	private void updatePrefs()
	{
		sp.edit().putBoolean("all_events_were_seen", true).apply();
		sp.edit().putLong("app_pause_time", System.currentTimeMillis()/1000).apply();
	}

	private void updateCursors()
	{
		TextView activeTitle = (TextView) view.findViewById(R.id.title_current);
		TextView repairTitle = (TextView) view.findViewById(R.id.title_repair);
		TextView message = (TextView) view.findViewById(R.id.textCriticalMessage);

		long time = sp.getLong("last_activity_closed_time", System.currentTimeMillis() - D3Service.EVENTS_TIME_INTERVAL); // TODO уменьшить интервал, слишком много событий
		Func.log_v("D3ServiceLog", "GET CLOSE TIME:" + time);

		activeLostCursor = dbHelper.getNewActiveLostEvents();
		activeCursor = dbHelper.getNewActiveAttentionEvents(time);
		repairCursor = dbHelper.getNewRepairedAttentionEvents(time);

		a_count = activeCursor!=null? activeCursor.getCount():0;
		l_count = activeLostCursor!=null? activeLostCursor.getCount():0;
		r_count = repairCursor!=null? repairCursor.getCount():0;


		empty = true;

		if(a_count!=0){
			empty = false;
			activeTitle.setVisibility(View.VISIBLE);
		}else{
			activeTitle.setVisibility(View.GONE);
		}

		if(r_count!=0){
			empty = false;
			repairTitle.setVisibility(View.VISIBLE);
		}else{
			repairTitle.setVisibility(View.GONE);
		}

		if(!empty)message.setText(R.string.MESSAGE_CRITICAL_EVENTS);
	}

}
