package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK;

import android.content.Context;

import com.company.NetSDK.CB_fSearchDevicesCB;
import com.company.NetSDK.INetSDK;

public class NDahuaCameraSearchModule
{
	public long lDevSearchHandle = 0;
	Context context;

	public NDahuaCameraSearchModule(Context context) {
		this.context = context;
	}

	//Search device
	public long startSearchDevices(CB_fSearchDevicesCB callback) {
		if (callback == null)
			throw new NullPointerException("callback parameter is null");
		lDevSearchHandle = INetSDK.StartSearchDevices(callback);

		return lDevSearchHandle;
	}

	//Stop search device
	public void stopSearchDevices() {
		if(lDevSearchHandle != 0) {
			INetSDK.StopSearchDevices(lDevSearchHandle);
			lDevSearchHandle = 0;
		}
	}
}
