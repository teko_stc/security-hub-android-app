package biz.teko.td.SHUB_APP.MainTabs.Activities;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.SectionGroup;
import biz.teko.td.SHUB_APP.MainTabs.Adapters.MainSetPagerAdapter;
import biz.teko.td.SHUB_APP.MainTabs.Fragments.MainSetFragment;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.PresetValueButton;
import biz.teko.td.SHUB_APP.Utils.Other.SwipeDisabledViewPager;

public class MainSetActivity extends BaseActivity
{
	private Context context;
	private boolean sharedMainBar = false;

	private PresetValueButton selectedButton;
	private MainSetPagerAdapter adapter;
	private ViewPager pager;
	private MainBar mainBar;
	private LinearLayout closeButton;
	private LinearLayout backButton;
	private LinearLayout nextButton;
	private TextView nextButtonText;
	private TextView mainSetTitle;
	private TextView mainSetMessage;
	private SectionGroup sectionGroup;
	private DBHelper dbHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_main_set);
		context = this;
		dbHelper = DBHelper.getInstance(context);

		int id = getIntent().getIntExtra("id", -1);
		int site = getIntent().getIntExtra("site", -1);
		int x = getIntent().getIntExtra("x", -1);
		int y = getIntent().getIntExtra("y", -1);
		sharedMainBar = getIntent().getBooleanExtra("shared", false);

		if (!sharedMainBar)
			mainBar = new MainBar(id, site, x, y);
		else
			mainBar = new MainBar(id, x, y);

		mainSetTitle = (TextView) findViewById(R.id.mainSetTitle);
		mainSetMessage = (TextView) findViewById(R.id.mainSetMessage);

		closeButton = (LinearLayout) findViewById(R.id.nButtonClose);
		backButton = (LinearLayout) findViewById(R.id.nButtonBack);
		nextButton = (LinearLayout) findViewById(R.id.mainSetButtonNext);
		nextButtonText = (TextView) findViewById(R.id.mainSetButtonNextText);


		if(null!=closeButton)closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				finish();
			}
		});

		if(null!=backButton)backButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				goBack();
			}
		});

		if(null!=nextButton)nextButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				goNext();
			}
		});

		adapter = new MainSetPagerAdapter(getSupportFragmentManager(), sharedMainBar, site, new MainSetFragment.OnSelectListener() {
			@Override
			public void callback(int position, int site_id) {
				mainBar.setSite_id(site_id);
				switch (position) {
					case -1:
						setSectionGroup(null);
						enableNext(false);
						break;
					case 0:
						setSubType(null);
						setElementId(null);
						setSectionGroup(null);
						enableNext(true);
						break;
					case 1:
						if (mainBar.type == MainBar.Type.ARM && mainBar.subtype != MainBar.SubType.SITE) {
							setElementId(null);
						}
						setSectionGroup(null);
						if(null!=getSubType() && null!= nextButtonText){
							if (!sharedMainBar && getSubType() == MainBar.SubType.SITE) {
								nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_READY);
							} else {
								nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_NEXT);
							}
						}
						enableNext(true);
						break;
					case 2:
						if(mainBar.type == MainBar.Type.ARM && mainBar.subtype== MainBar.SubType.GROUP) {
							if (null != getSectionGroup()
									&& null != getSectionGroup().sections && 0 != getSectionGroup().sections.length) {
								enableNext(true);
							} else {
								enableNext(false);
							}
							if (null != nextButtonText)
								nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_NEXT);
							break;
						}
						if(null!=mainBar.element_id && !mainBar.element_id.equals("")){
							enableNext(true);
							if(null!=nextButtonText) nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_READY);
						}
						break;
					case 3:
						if(null!=pager && pager.getCurrentItem() == position)
						{
							SectionGroup sectionGroup = getSectionGroup();
							if (null != sectionGroup)
							{
								if (null != sectionGroup.name && !sectionGroup.name.equals(""))
								{
									enableNext(true);
									if (null != nextButtonText)
										nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_READY);
								} else {
									enableNext(false);
								}
							}
						}
						break;
				}
			}

			@Override
			public void updateState() {
				if (getElementId() != null) {
					enableNext(true);
					nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_READY);
				} else {
					enableNext(false);
				}
			}

			@Override
			public void doneCallback(int position) {
				goNext();
			}
		});
		pager = (SwipeDisabledViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);
	}

	private void  refreshView(){
		switch (pager.getCurrentItem()){
			case 0:
				enableBack(false);
				if(null!=mainBar.type) enableNext(true);
				if(null!=nextButtonText) nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_NEXT);
				if(null!= mainSetMessage) mainSetMessage.setText(R.string.N_MAIN_SET_TITLE_1);
				break;
			case 1:
				enableBack(true);
				if(null!=mainBar && null!=mainBar.type)
				{
					if(mainBar.type == MainBar.Type.ARM){
						if(null!=mainBar.subtype){
							enableNext(true);
							if(!sharedMainBar && mainBar.subtype == MainBar.SubType.SITE){
								if(null!=nextButtonText)nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_READY);
							}else{
								if(null!=nextButtonText)nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_NEXT);
							}
						}else{
							enableNext(false);
						}
					}else{
						if(null!=nextButtonText)nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_READY);
						if(null==getElementId() ) enableNext(false);
					}
				}
				switch (mainBar.type){
					case ARM:
						if(null!= mainSetMessage) mainSetMessage.setText(R.string.N_MAIN_SET_TITLE_2);
						break;
					case CONTROL:
						if(null!= mainSetMessage) mainSetMessage.setText(R.string.N_MAIN_SET_TITLE_3);
						break;
					case DEVICE:
						if(null!= mainSetMessage) mainSetMessage.setText(R.string.N_MAIN_SET_TITLE_4);
						break;
					case CAMERA:
						if(null!= mainSetMessage) mainSetMessage.setText(R.string.N_MAIN_SET_TITLE_5);
						break;
				}
				break;
			case 2:
				if(null!= mainSetMessage) {
					if(mainBar.subtype == MainBar.SubType.GROUP){
						mainSetMessage.setText(R.string.N_MAIN_SET_TITLE_6);
					}else{
						mainSetMessage.setText(R.string.N_MAIN_SET_TITLE_7);
					}
				}
				if(null!=nextButtonText)
					if(mainBar.type == MainBar.Type.ARM && mainBar.subtype == MainBar.SubType.GROUP){
					nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_NEXT);
				}else{
					nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_READY);
				}
				if((mainBar.type == MainBar.Type.ARM && mainBar.subtype == MainBar.SubType.GROUP && null!=getSectionGroup() && null!= getSectionGroup().sections && 0< getSectionGroup().sections.length)
						|| (mainBar.type == MainBar.Type.ARM && mainBar.subtype != MainBar.SubType.GROUP && null!=mainBar.element_id && !mainBar.element_id.equals(""))
						|| (mainBar.type != MainBar.Type.ARM && null!=mainBar.element_id && !mainBar.element_id.equals(""))){
					enableNext(true);
				}else{
					enableNext(false);
				}
				enableBack(true);
				break;
			case 3:
				enableBack(true);
				enableNext(false);
				if(null!=nextButtonText) nextButtonText.setText(R.string.N_MAIN_SET_BUTTON_READY);
				if(null!= mainSetMessage) mainSetMessage.setText(R.string.N_MAIN_SET_SGROUP_NAME);
				break;
		}
		adapter.notifyDataSetChanged();
	}

	private void enableNext(boolean b)
	{
		if(null!=nextButton) nextButton.setVisibility(b ? View.VISIBLE: View.GONE);
	}


	private void enableBack(boolean b)
	{
		if(null!=backButton) backButton.setVisibility(b? View.VISIBLE : View.GONE);
	}

	@Override
	public void onBackPressed()
	{
		goBack();
	}

	private void goBack(){
		setElementId(null);
		int position = getCurrentItem();
		if(0 != position){
			pager.setCurrentItem(position - 1);
			refreshView();
		}else{
			super.onBackPressed();
		}
	}

	private void goNext()
	{

		int position = getCurrentItem();
		if((position == MainSetPagerAdapter.MAIN_SET_PAGER_COUNT - 1 && mainBar.subtype == MainBar.SubType.GROUP)){
			if(addSectionGroup()){
				addMainBar();
			}
		} else if ((position == MainSetPagerAdapter.MAIN_SET_PAGER_COUNT - 2 && mainBar.type == MainBar.Type.ARM && mainBar.subtype != MainBar.SubType.GROUP)
				|| (!sharedMainBar && position == MainSetPagerAdapter.MAIN_SET_PAGER_COUNT - 3 && mainBar.type == MainBar.Type.ARM && mainBar.subtype == MainBar.SubType.SITE)
				|| (position == MainSetPagerAdapter.MAIN_SET_PAGER_COUNT - 3 && !(mainBar.type == MainBar.Type.ARM))) {
			addMainBar();
		} else {
			pager.setCurrentItem(position + 1);
			refreshView();
		}
		if (sharedMainBar && mainBar.subtype != MainBar.SubType.GROUP)
			setElementId(null);
	}

	private void addMainBar()
	{
		if(null!= mainBar){
			DBHelper dbHelper =  DBHelper.getInstance(context);
			dbHelper.addNewBar(mainBar, sharedMainBar);
			super.onBackPressed();
		}
	}

	private boolean addSectionGroup()
	{
		SectionGroup sectionGroup = getSectionGroup();
		if(null!=sectionGroup && null!=sectionGroup.name & !sectionGroup.name.equals("")) {
			long element_id = dbHelper.addSectionGroup(sectionGroup);
			if(0!=element_id){
				setElementId(Long.toString(element_id));
				return true;
			}
		}
		return false;
	}

	public int getCurrentItem(){
		return pager.getCurrentItem();
	}

	public void setElementId(String id)
	{
		this.mainBar.element_id = id;
	}

	public String getElementId()
	{
		return this.mainBar.element_id;
	}

	public void setType(MainBar.Type type)
	{
		this.mainBar.type = type;
	}

	public MainBar.Type getType()
	{
		return this.mainBar.type;
	}

	public void setSubType(MainBar.SubType subType)
	{
		this.mainBar.subtype = subType;
	}

	public MainBar.SubType getSubType(){
		return this.mainBar.subtype;
	}

	public void setSectionGroup(SectionGroup sectionGroup)
	{
		this.sectionGroup = sectionGroup;
	}

	public SectionGroup getSectionGroup()
	{
		return null == sectionGroup ? new SectionGroup():sectionGroup;
	}
}
