package biz.teko.td.SHUB_APP.Events.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Pair;
import android.view.View;
import android.widget.ResourceCursorAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraRecordViewActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.Events.Activities.EventActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NEAListElement;

public class NEventsListAdapter extends ResourceCursorAdapter {
	private final boolean anim;
	private final Context context;
	private final DBHelper dbHelper;

	public void update(Cursor c) {
		this.changeCursor(c);
	}

	public NEventsListAdapter(Context context, int layout, Cursor c, int flags, boolean anim) {
		super(context, layout, c, flags);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.anim = anim;
	}


	@Override
	public void bindView(View view, Context context, Cursor cursor)
	{
		final Event event = dbHelper.getEvent(cursor);
		if(null!=event) {
			NEAListElement v = (NEAListElement) view.findViewById(R.id.nEventsListElement);
			Pair<String,String> eventDesc = event.explainEventRegDescriptionWithClass(context);
			v.setClass(eventDesc.first);
			v.setTitle(eventDesc.second);
			v.setLocation(dbHelper.getSiteNameByDeviceSection(event.device, event.section) + "\n" + event.getEventLocation(context));
			v.setTime(Func.getServerTimeTextShort(context, event.time));
			v.setDate(Func.getServerDateTextShort(context, event.time));
			v.setImage(event.getListIcon(context));
			v.setOnElementClickListener(() -> openEventActivity(event.id, v));

			int action = event.getLink();
			int icon = 0;

			switch (action) {
				case Event.LINK_GEO:
					icon = R.drawable.ic_location_filled;
					break;
				case Event.LINK_VIDEO:
					icon = R.drawable.ic_camera_filled;
					break;
				case Event.LINK_VIDEO_FAILED:
					icon = R.drawable.ic_camera_failed_filled;
					break;
				case Event.NO_LINK:
				default:
					break;
			}
			if(0!=action) {
				v.setActionButtonVisiblity(View.VISIBLE);
				v.setActionButtonImage(icon);
				v.setOnActionClickListener(value -> {
					switch (action) {
						case Event.LINK_GEO:
							openGeo(event);
							break;
						case Event.LINK_VIDEO:
							Intent intent = new Intent(context, NCameraRecordViewActivity.class);
							intent.putExtra("event", event.id);
							context.startActivity(intent);
							((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
							break;
						case Event.LINK_VIDEO_FAILED:
							Func.nShowMessage(context, context.getString(R.string.IV_CLIP_FAILED));
							break;
						case Event.NO_LINK:
						default:
							break;
					}
				});
			}else{
				v.setActionButtonVisiblity(View.GONE);
			}
		}
	}

	private void openGeo(Event event)
	{
		try
		{
			JSONObject jsonObject = new JSONObject(event.jdata);
			String lat = jsonObject.getString("lat");
			String lon = jsonObject.getString("lon");
			//						String uri = String.format(LocaleD3.ENGLISH, "geo:%f,%f", lat, lon);
			String uri = String.format("geo:0,0?q=" + lat + "," + lon + context.getString(R.string.EVENTS_ALARM_PLACE));
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(intent);
		} catch (JSONException e)
		{
			e.printStackTrace();
			Func.nShowMessage(context, context.getString(R.string.N_EVENTS_LOCATION_GET_ERROR));
		}
	}

	private void openEventActivity(int id, NEAListElement v)
	{
		Intent intent = new Intent(context, EventActivity.class);
		intent.putExtra("event", id);
		if(anim){
			intent.putExtra("transition", true);
			intent.putExtra("transition_start", Func.getTextSize(context, ((NEAListElement)v).getTitleTextView()));
			context.startActivity(intent, getTransitionOptions(v).toBundle());
		}else{
			context.startActivity(intent);
		}
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((Activity) context,
				new Pair<>(((NEAListElement)view).getTitleTextView(), Const.TITLE_TRANSITION_NAME));
	}

}
