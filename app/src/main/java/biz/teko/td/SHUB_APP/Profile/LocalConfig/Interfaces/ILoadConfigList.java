package biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces;

import java.util.HashMap;

public interface ILoadConfigList extends ILoadConfigBase {
    void setConfigValues(HashMap<String, String> map);
    void showToast(int resId);
    void close();
}
