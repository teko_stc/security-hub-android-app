package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class NCameraDahuaSettings extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private LinearLayout buttonBack;
	private NMenuListElement name;
	private NMenuListElement delete;
	private String sn;
	private Camera camera;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_dahua_settings);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		sn = getIntent().getStringExtra("cameraId");

		setup();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		bind();
	}

	private void setup()
	{
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		name = (NMenuListElement) findViewById(R.id.nMenuName);
		delete = (NMenuListElement) findViewById(R.id.nMenuDelete);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	private void bind(){
		if(null!=sn){
			camera = dbHelper.getDahuaCameraBySn(sn);
			if(null!=camera){
				if(null!=name) name.setTitle(camera.name);
				name.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						Intent intent = new Intent(context, NRenameActivity.class);
						intent.putExtra("camera_dahua", camera.sn);
						intent.putExtra("name", camera.name);
						intent.putExtra("type", Const.CAMERA_DAHUA);
						startActivity(intent, getTransitionOptions(name).toBundle());
					}
				});
				if(null!=delete)delete.setOnRootClickListener(()->{
					Intent intent = new Intent(context, NDeleteActivity.class);
					intent.putExtra("camera_dahua", sn);
					intent.putExtra("name", camera.name);
					intent.putExtra("type", Const.CAMERA_DAHUA);
					startActivityForResult(intent, Const.REQUEST_DELETE, getTransitionOptions(delete).toBundle());
				});
			}
		}

		if(null!=buttonBack) buttonBack.setOnClickListener((view)->onBackPressed());

	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((NCameraDahuaSettings)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
}
