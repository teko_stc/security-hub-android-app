package biz.teko.td.SHUB_APP.Utils.NViews;

import android.app.Dialog;
import android.content.Context;
import android.text.Spanned;
import android.view.View;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.R;

public class NDialog extends Dialog
{
	private final int layout;
	private NActionButton buttonInfo;
	private OnActionClickListener onActionClickListener;
	private Context context;
	private LinearLayout buttonFrame;

	private NActionButton buttonOn;
	private NActionButton buttonOff;
	private NActionButton buttonOk;
	private NActionButton buttonCancel;
	private FrameLayout dialogCustomFrame;
	private TextView titleView;
	private TextView subTitleView;
	private TextView siteTitleView;
	private RadioGroup radio;
	private CheckBox activityCheck;
	private CheckBox optionCheck;
	private boolean auto_dismiss = false;

	public void setOnActionClickListener(OnActionClickListener onActionClickListener){
		this.onActionClickListener = onActionClickListener;
	}

	public void setAutoDismiss(boolean b)
	{
		auto_dismiss = true;
	}

	public interface OnActionClickListener{
		void onActionClick(int value, boolean b);
	}

	public interface OnButtonClick
	{
		void onClick();
	}

	public NDialog(@NonNull Context context, int layout)
	{
		super(context, R.style.RoundedCornersDialog);

		this.context = context;
		this.layout = layout;

		this.setContentView(layout);

		setup();
		bind();
	}

	private void setup()
	{
		buttonFrame = (LinearLayout) findViewById(R.id.nButtonFrame);
		buttonOn = (NActionButton) findViewById(R.id.nDialogButtonOn);
		buttonOff = (NActionButton) findViewById(R.id.nDialogButtonOff);
		buttonOk = (NActionButton) findViewById(R.id.nDialogButtonOk);
		buttonCancel = (NActionButton) findViewById(R.id.nDialogButtonCancel);
		buttonInfo = (NActionButton) findViewById(R.id.nDialogButtonInfo);
		dialogCustomFrame = (FrameLayout) findViewById(R.id.nCustomFrame);
		titleView = (TextView) findViewById(R.id.nDialogTitle);
		subTitleView = (TextView) findViewById(R.id.nDialogSubTitle);
		siteTitleView = (TextView) findViewById(R.id.nDialogSiteTitle);
		radio = (RadioGroup) findViewById(R.id.nDialogRadio);
		activityCheck = (CheckBox) findViewById(R.id.nDialogCheckBox);
		optionCheck = (CheckBox) findViewById(R.id.nDialogOptionCheckBox);
	}

	private void bind()
	{
		if(null!=buttonOn)
		{
			buttonOn.setOnButtonClickListener(action -> {
				if (null != onActionClickListener)
				{
					click(action, null!=optionCheck ? optionCheck.isChecked():false);;
				}
			});
		}

		if(null!=buttonOff){
			buttonOff.setOnButtonClickListener(action -> {
				if(null!= onActionClickListener) {
					click(buttonOff.getAction(), null != optionCheck ? optionCheck.isChecked() : false);
				}
			});
		}

		if(null!=buttonInfo){
			buttonInfo.setOnButtonClickListener(action -> {
				if(null!= onActionClickListener) {
					click(action, false);
				}
			});
		}
		if(null!=buttonOk)
		{
			enableButtonOk();
			buttonOk.setOnButtonClickListener(action -> {
				if (null != onActionClickListener)
				{
					click(action, null != optionCheck ? optionCheck.isChecked() : false);
				}
			});
		}
		if(null!=buttonCancel)
		{
			buttonCancel.setOnButtonClickListener(action -> {
				if (null != onActionClickListener)
				{
					click(action, false);
				}
			});
		}
		if(null!= activityCheck){
			activityCheck.setOnCheckedChangeListener((buttonView, isChecked) -> enableButtonOk());
		}
	}

	private void click(int action, boolean b)
	{
		onActionClickListener.onActionClick(action, b);
		if(auto_dismiss) dismiss();
	}

	private void enableButtonOk()
	{
		if(null!=buttonOk){
			if(null!= activityCheck){
				buttonOk.setDisable(!activityCheck.isChecked());
			}else{
				buttonOk.setDisable(false);
			}
		}
	}

	public void setRadioChecked(int id){
		if(null!=(RadioButton) findViewById(id)) {
			((RadioButton) findViewById(id)).setChecked(true);
		}
	}

	public int getRadioChecked(){
		if(null!=radio) return radio.getCheckedRadioButtonId();
		return 0;
	}

	/*for scripted relay control dialog*/
	public void setButton(View view){
		NActionButton button = (NActionButton) view;
		if(null!=buttonFrame){
			buttonFrame.addView(button);
			if(null!=button){
				button.setOnButtonClickListener(action -> {
					if(null!= onActionClickListener) onActionClickListener.onActionClick(action, null != optionCheck ? optionCheck.isChecked() : false);
				});
			}
		}
	}

	public void setTitle(String title){
		if(null!=titleView) {
			if(null!=title){
				titleView.setText(title);
				titleView.setVisibility(View.VISIBLE);
			}
			else{
				titleView.setVisibility(View.GONE);
			}
		}
	}

	public void setTitleSpanned(Spanned s){
		if(null!=titleView) {
			if(null!=s){
				titleView.setText(s);
				titleView.setVisibility(View.VISIBLE);
			}
			else{
				titleView.setVisibility(View.GONE);
			}
		}
	}

	public void setSubTitle(String subTitle)
	{
		TextView subTitleView = (TextView) findViewById(R.id.nDialogSubTitle);
		if(null!=subTitleView){
			if(null!=subTitle){
				subTitleView.setText(subTitle);
				subTitleView.setVisibility(View.VISIBLE);
			}else{
				subTitleView.setVisibility(View.GONE);
			}
		}
	}

	public void setSiteTitle(String siteTitle)
	{
		if(null!=siteTitleView){
			if(null!=siteTitle){
				siteTitleView.setText(siteTitle);
				siteTitleView.setVisibility(View.VISIBLE);
			}else{
				siteTitleView.setVisibility(View.GONE);
			}
		}
	}

	public void setPositiveButton(String text){
		if(null!=buttonOk){
			if(null!= text){
				buttonOk.setTitle(text);
				buttonOk.setVisibility(View.VISIBLE);
			}else{
				buttonOk.setVisibility(View.GONE);
			}
		}
	}


	public void setPositiveButton(String text, OnButtonClick onButtonClick){
		if(null!=buttonOk){
			if(null!= text){
				buttonOk.setTitle(text);
				buttonOk.setVisibility(View.VISIBLE);
				buttonOk.setOnButtonClickListener(action -> {
					onButtonClick.onClick();
				});
			}else{
				buttonOk.setVisibility(View.GONE);
			}
		}
	}

	public void setNegativeButton(String text){
		if(null!=buttonCancel){
			if(null!= text && !text.equals("")){
				buttonCancel.setTitle(text);
				buttonCancel.setVisibility(View.VISIBLE);
			}else{
				buttonCancel.setVisibility(View.GONE);
			}
		}
	}


	public void setNegativeButton(String text, OnButtonClick onButtonClick){
		if(null!=buttonCancel){
			if(null!= text){
				buttonCancel.setTitle(text);
				buttonCancel.setVisibility(View.VISIBLE);
				buttonCancel.setOnButtonClickListener(action -> {
					onButtonClick.onClick();
				});
			}else{
				buttonCancel.setVisibility(View.GONE);
			}
		}
	}

	public void setNegativeButton(OnButtonClick onButtonClick){
		buttonCancel.setOnButtonClickListener(action -> {
			onButtonClick.onClick();
		});
	}


	public void setLinkVisiblity(int visiblity){
		if(null!=buttonInfo) buttonInfo.setVisibility(visiblity);
	}


	public void addView(View view)
	{
		if(null!=dialogCustomFrame){
			dialogCustomFrame.addView(view);
		}
	}


	public void setOptionCheck(boolean b)
	{
		if(null!=optionCheck) optionCheck.setChecked(b);
	}

}
