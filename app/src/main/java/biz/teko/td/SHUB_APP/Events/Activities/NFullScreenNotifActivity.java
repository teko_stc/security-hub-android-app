package biz.teko.td.SHUB_APP.Events.Activities;

import android.app.KeyguardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.FrameLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import biz.teko.td.SHUB_APP.Activity.StartUpActivity;
import biz.teko.td.SHUB_APP.Alarms.Adapters.AlarmFullScreenAdapter;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.ActivityLifecycleCall;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class NFullScreenNotifActivity extends AppCompatActivity {
    private NActionButton toApp;
    private NActionButton close;
    private Ringtone ringtone;
    private int countActivities;
    private D3Service d3Service;
    private Disposable eventDisposable;
    private boolean bound = false;
    private ServiceConnection serviceConnection;
    private RecyclerView eventsRecycler;
    private AlarmFullScreenAdapter adapter;
    private List<Event> events = new ArrayList<>();
    private NActionButton mute;
    private FrameLayout divider;
    private boolean sound = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLaunchingLockScreen();
        setContentView(R.layout.n_activity_full_screen_notif);
        initService();
        setup();
        bind();
        setAdapter();
        setListeners();
    }

    private void initService() {
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                d3Service = ((D3Service.myBinderClass) binder).getService();
                setEventObservable();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                bound = false;
            }
        };
        bindService(intent, serviceConnection, BIND_ADJUST_WITH_ACTIVITY | BIND_AUTO_CREATE);
    }

    private void setEventObservable() {
        eventDisposable = d3Service.getEventSubject()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(event -> {
                    if(event.inc){
                        events.add(event);
                    }else{
                        events.remove(event);
                    }
                    Collections.sort(events, (e1, e2) -> e2.id - e1.id);
                    adapter.notifyDataSetChanged();
                    startRingtone(event.classId);
                });
    }

    private void setAdapter() {
        adapter = new AlarmFullScreenAdapter(events, this);
        eventsRecycler.setAdapter(adapter);
        ViewTreeObserver observer = eventsRecycler.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                if (willMyListScroll()) {
                    if(null!=divider) divider.setVisibility(View.VISIBLE);
                }else{
                    if(null!=divider) divider.setVisibility(View.GONE);
                }
            }
        });
    }

    boolean willMyListScroll() {
        if (eventsRecycler.canScrollVertically(1) && //still scrolling
                eventsRecycler.computeVerticalScrollRange() >= eventsRecycler.getHeight()) { //Big enough for scrolling

            return true;
        }
        return false;
    }

    private void setLaunchingLockScreen() {
        if (Func.isCurrentVersionSdk(Build.VERSION_CODES.O_MR1)) {
            setShowWhenLocked(true);
            setTurnScreenOn(true);
            KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
            if (keyguardManager != null)
                keyguardManager.requestDismissKeyguard(this, null);
        } else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }
    }

    private void startRingtone(int classId) {
        if(sound)
        {
            stopRingtone();
            String ringtonePath = PrefUtils.getInstance(getApplicationContext()).getClassNotifAlarmRingtone(classId);
            if (ringtonePath != null && !ringtonePath.isEmpty())
            {
                ringtone = RingtoneManager.getRingtone(this, Uri.parse(ringtonePath));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P)
                {
                    ringtone.setLooping(true);
                }
                ringtone.play();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(null!= events && events.size() > 0){
            startRingtone(events.get(events.size()-1).classId);
        }
    }

    private void stopRingtone() {
        if (ringtone != null && ringtone.isPlaying())
            ringtone.stop();
    }

    @Override
    protected void onStop() {
        stopRingtone();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (bound) {
            d3Service.restartEventSubject();
            unbindService(serviceConnection);
            bound = false;
        }
        stopRingtone();
        eventDisposable.dispose();
        super.onDestroy();
    }

    private void setListeners() {
        close.setOnButtonClickListener(v -> close());
        toApp.setOnButtonClickListener(action -> toApp());
    }

    private void close()
    {
        finish();
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        i.addCategory(Intent.CATEGORY_HOME);
        startActivity(i);
    }

    private void toApp()
    {
        if (ActivityLifecycleCall.getActiveCount() == 0)
            startActivity(new Intent(this, StartUpActivity.class));
        finish();
    }

    private void setup() {
        eventsRecycler = findViewById(R.id.recycler_alarm_fullscreen);
        close = findViewById(R.id.nButtonSkip);
        toApp = findViewById(R.id.nButtonToApp);
        mute = findViewById(R.id.nButtonMute);
        divider = findViewById(R.id.nAlarmListDivider);
    }

    private void bind(){
        if(null!=mute) mute.setOnButtonClickListener(new NActionButton.OnButtonClickListener()
        {
            @Override
            public void onButtonClick(int action)
            {
                stopRingtone();
                sound = false;
                mute.setEnabled(sound);
                mute.setDisable(true);
                mute.setTitle(getString(R.string.N_NOTIF_ALARM_SOUND_DISABLED));
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}
