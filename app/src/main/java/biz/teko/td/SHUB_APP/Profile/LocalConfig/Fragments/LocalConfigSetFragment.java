package biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Arrays;

import biz.teko.td.SHUB_APP.Profile.LocalConfig.Activities.LocalConfigNavigation;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceConfigDto;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.Profile.Models.LocalConfigModel;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

public class LocalConfigSetFragment extends LocalConfigSetBaseFragment
{
    private final DeviceConfigDto deviceConfig;
    private LinearLayout buttonSetConfig, buttonBack;
    private TextView textConfigTitle;
    private NEditText editTextConfig;
    private String type;
    private LocalConfigModel configModel;
    private String newStringValue;

    public LocalConfigSetFragment(LocalConfigNavigation activity, DeviceDTO device, SocketConnection connection, DeviceConfigDto deviceConfig){
        super(activity, device, connection);
        this.deviceConfig = deviceConfig;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_set_config, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        getBaseActivity().setOnBackPressedCallback(this::onBackPressed);
    }

    @Override
    public void onBackPressed() {
        getBaseActivity().openDeviceConfig(getDevice());
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        setView();
        setEditTextView();
        setClickListeners();

        //??
        boolean transition = false;
        getBaseActivity().addTransitionListener(transition);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!getDevice().isTestMode())
            setConfigPresenter.sendTestModeOn();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configModel = deviceConfig.getConfigModel();
        type = configModel.getType();
        //getIntent().getBooleanExtra("transition", false);

    }

    private void setView() {
        textConfigTitle.setText(configModel.getTitle());
        editTextConfig.setText(configModel.getValue());
        if (!Arrays.asList(ConfigConstants.stringsAPNs).contains(type)) {
            editTextConfig.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
            if (Arrays.asList(ConfigConstants.temperatures).contains(type)) {
                editTextConfig.setKeyListener(DigitsKeyListener.getInstance("0123456789-"));
            } else {
                editTextConfig.setKeyListener(DigitsKeyListener.getInstance("0123456789."));
            }        }
        newStringValue = editTextConfig.getText().toString();
    }

    private void setEditTextView() {
        editTextConfig.showKeyboard();
        editTextConfig.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                newStringValue = editTextConfig.getText().toString();
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    private void setClickListeners() {
        buttonBack.setOnClickListener(view -> {
            onBackPressed();
        });
        buttonSetConfig.setOnClickListener(v -> {
            if (validate())
                sendConfig();
        });
    }

    private void sendConfig() {
        setConfigPresenter.setConfig(saveData(), type, deviceConfig.getHubVersion());
    }

    private Bundle saveData() {
        Bundle bundle = new Bundle();
        bundle.putString(type, getReplaceText(newStringValue));
        return bundle;
    }

    private void initView() {
        buttonSetConfig = (LinearLayout) getBaseActivity().findViewById(R.id.nButtonChange);
        buttonBack = (LinearLayout) getBaseActivity().findViewById(R.id.nButtonClose);
        editTextConfig = (NEditText) getBaseActivity().findViewById(R.id.nText);
        textConfigTitle = (TextView) getBaseActivity().findViewById(R.id.nTextTitle);
    }

    private boolean validate() {
        if (Arrays.asList(ConfigConstants.stringsConfigIP).contains(type)) {
            if (isIPCorrect(newStringValue))
                return true;
            Func.pushToast(getBaseActivity(), getResources().getString(R.string.PN_ADD_INCORRECT_IP), getBaseActivity());
            return false;
        } else if (type.equals(ConfigConstants.CONFIG_PIN)) {
            if (TextUtils.isDigitsOnly(newStringValue) && newStringValue.length() == 4)
                return true;
            Func.pushToast(getBaseActivity(), getResources().getString(R.string.PN_ADD_INCORRECT_PIN), getBaseActivity());
            return false;
        } else if (Arrays.asList(ConfigConstants.stringsAPNs).contains(type)) {
            return setAPNLimitations(type);
        } else if (Arrays.asList(ConfigConstants.temperatures).contains(type)) {
            return tempLimitation();
        }
        return true;
    }

    private boolean tempLimitation() {
        try {
            long value = Long.parseLong(newStringValue);
            if (value >= -128 && value <= 127)
                return true;
            return tempError();
        } catch (Exception e) {
            return tempError();
        }
    }

    private boolean tempError() {
        Func.pushToast(getBaseActivity(), getResources().getString(R.string.INVALID_TEMP), getBaseActivity());
        return false;
    }

    private boolean setAPNLimitations(String type) {
        if (!isLatinLetters()) {
            Func.pushToast(getBaseActivity(), getResources().getString(R.string.N_APN_LETTERS_LIMITATION), getBaseActivity());
            return false;
        }
        if (type.equals(ConfigConstants.APN_SERVER) || type.equals(ConfigConstants.APN2_SERVER)) {
            if (getReplaceText(newStringValue).isEmpty()) {
                Func.pushToast(getBaseActivity(), getResources().getString(R.string.N_APN_SERVER_LIMITATION), getBaseActivity());
                return false;
            }
        }
        return true;
    }


    private boolean isLatinLetters() {
        String regex = "^[A-Za-z0-9.]+$";
        return getReplaceText(newStringValue).matches(regex);
    }


    private boolean isIPCorrect(String string) {
        String regex = "^((0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)\\.){3}(0|1\\d?\\d?|2[0-4]?\\d?|25[0-5]?|[3-9]\\d?)$";
        return string.matches(regex);
    }

}
