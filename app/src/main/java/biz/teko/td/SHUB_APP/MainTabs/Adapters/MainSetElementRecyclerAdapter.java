package biz.teko.td.SHUB_APP.MainTabs.Adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.Utils.Other.RadioGroup.NPresetRadioGroup;

public class MainSetElementRecyclerAdapter<T> extends RecyclerView.Adapter<MainSetElementRecyclerAdapter.MainSetElementVH> {
	private final T[] elements;
	private final onBindFunc<T> onBindFunc;
	private int curPos;

	public MainSetElementRecyclerAdapter(T[] elements, MainSetElementRecyclerAdapter.onBindFunc<T> onBindFunc) {
		this.elements = elements;

		//super(context, layout);
		this.onBindFunc = onBindFunc;
	}

	public int getCurPos() {
		return curPos;
	}

	public void setCurPos(int curPos) {
		this.curPos = curPos;
	}

	@NonNull
	@Override
	public MainSetElementVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return MainSetElementVH.newInstance(parent);
	}

	@Override
	public void onBindViewHolder(@NonNull MainSetElementVH holder, int position) {
		onBindFunc.bind(elements[position], position, holder);
	}

	/*@Override
	public void onBindViewHolder(@NonNull SectionInGroupVH holder, int position) {

		NPresetValueButton elementView = holder.itemView.findViewById(R.id.nPresetButton);
		if(null!=elementView)
		{
			final Section section = sections[position];
			elementView.setTitle(section.name);
			SType sType = section.getType();
			if (null != sType) {
				elementView.setSubTitle(sType.caption);
				elementView.setImage(sType.getListIcon(context));
			} else {
				elementView.setImage(R.drawable.n_image_section_common_list_selector);
			}
			elementView.setUncheckable(true);
			elementView.setChecked(section.checked);
			TextView siteTv = elementView.findViewById(R.id.text_view_site);
			if (shared) {
				if (curSite != null && curSite.id != sites.get(position).id) {
					//elementView.setChecked(true);
					siteTv.setActivated(true);
					elementView.getImageSwitcher().setSelected(true);
					elementView.getSubTitleTextView().setActivated(true);
					elementView.getTitleTextView().setActivated(true);
				} else {
					siteTv.setActivated(false);
					elementView.getImageSwitcher().setSelected(false);
					elementView.getSubTitleTextView().setActivated(false);
					elementView.getTitleTextView().setActivated(false);
				}

				siteTv.setTextColor(context.getResources().getColorStateList(R.color.n_site_select_text_color_selector));
				siteTv.setVisibility(View.VISIBLE);
				siteTv.setText(sites.get(position).name);
			}

			elementView.setOnClickListener(view1 -> {
				section.checked = view1.isSelected();
				if (null != onSectionSelectListener)
					onSectionSelectListener.callback(sections, view1, position);
			});
		}
	}
*/
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemCount() {
		return elements.length;
	}

	public interface onBindFunc<T> {
		void bind(T element, int pos, MainSetElementVH holder);
	}

	public static class MainSetElementVH extends RecyclerView.ViewHolder {
		private MainSetElementVH(@NonNull View itemView) {
			super(itemView);
		}

		public static MainSetElementVH newInstance(ViewGroup parent) {
			NPresetRadioGroup presetRadioGroup = new NPresetRadioGroup(parent.getContext());
			FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
			presetRadioGroup.setLayoutParams(layoutParams);
			presetRadioGroup.setOrientation(LinearLayout.VERTICAL);
			return new MainSetElementVH(presetRadioGroup);
		}
	}

}
