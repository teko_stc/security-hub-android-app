package biz.teko.td.SHUB_APP.Profile.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 19.01.2017.
 */
public class ProfileScanerActivity extends Activity
{
	private Button button;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan);

		// Scanner

		button = (Button) findViewById(R.id.assistant_button);
		button.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				IntentIntegrator integrator = new IntentIntegrator(ProfileScanerActivity.this);
				integrator.setOrientationLocked(false);
				integrator.initiateScan();
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanResult != null) {
			String re = scanResult.getContents();
			Log.e("D3Scan", re);
		}
	}
}
