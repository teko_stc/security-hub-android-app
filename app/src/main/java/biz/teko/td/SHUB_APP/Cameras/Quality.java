package biz.teko.td.SHUB_APP.Cameras;

/**
 * Created by td13017 on 06.04.2018.
 */

public class Quality
{
	public String name;
	public String value;

	public Quality(){};

	public Quality(String name, String value){
		this.name = name;
		this.value = value;
	}

}
