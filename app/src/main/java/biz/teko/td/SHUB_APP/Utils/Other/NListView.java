package biz.teko.td.SHUB_APP.Utils.Other;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

public class NListView extends ListView
{
	public NListView(Context context)
	{
		super(context);
		setDynamicHeight();
	}

	public NListView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		setDynamicHeight();
	}

	public NListView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		setDynamicHeight();
	}

	public NListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		setDynamicHeight();
	}

	private void setDynamicHeight() {
		ListAdapter adapter = getAdapter();
		//checkStates adapter if null
		if (adapter == null) {
			return;
		}
		int height = 0;
		int desiredWidth = View.MeasureSpec.makeMeasureSpec(getWidth(), View.MeasureSpec.UNSPECIFIED);
		for (int i = 0; i < adapter.getCount(); i++) {
			View listItem = adapter.getView(i, null, this);
			listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			height += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams layoutParams = getLayoutParams();
		layoutParams.height = height + (getDividerHeight() * (adapter.getCount() - 1));
		setLayoutParams(layoutParams);
		requestLayout();
	}
}
