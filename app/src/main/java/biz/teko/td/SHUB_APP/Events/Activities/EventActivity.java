package biz.teko.td.SHUB_APP.Events.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraRecordViewActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomInfoLayout;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopInfoLayout;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class EventActivity extends BaseActivity
{
	private LinearLayout buttonMenu;
	private Context context;
	private DBHelper dbHelper;
	private PrefUtils prefUtils;
	private NTopInfoLayout topFrame;
	private Event event;
	private NBottomInfoLayout bottomFrame;

	private BroadcastReceiver eventReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			bindView();
		}
	};
	private int event_id;
	private boolean transition;
	private int transition_start;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_event);
		this.context = this;
		dbHelper = DBHelper.getInstance(context);
		prefUtils = PrefUtils.getInstance(context);

		event_id = getIntent().getIntExtra("event", 0);

		transition = getIntent().getBooleanExtra("transition", false);
		transition_start = getIntent().getIntExtra("transition_start" , 0);
		addTransitionListener(transition);

		setupView();
		bindView();

	}

	private void setupView()
	{
		buttonMenu = (LinearLayout) findViewById(R.id.nButtonMenu);
		topFrame = (NTopInfoLayout) findViewById(R.id.nTopInfoFrame);
		bottomFrame = (NBottomInfoLayout) findViewById(R.id.nBottomFrame);
	}

	private void bindView()
	{
		if(0 != event_id) event = dbHelper.getEventById(event_id);

		if(null!=buttonMenu && !prefUtils.getCurrentLang().equals("ru")){
			buttonMenu.setVisibility(View.GONE);
		}

		if(null!=topFrame){
			topFrame.setTitle(event.explainEventRegDescription(context));
			topFrame.setSubTitle(Func.getTimeDateText(context, event.localtime));
			topFrame.setTransition(context, transition, transition_start);
			topFrame.setOnTopNavigationListener(new NTopInfoLayout.OnTopNavigationListener()
			{
				@Override
				public void favoriteChanged()
				{

				}

				@Override
				public void backPressed()
				{
					onBackPressed();
				}

				@Override
				public void setOptions()
				{
					/*todo add video link*/
				}
			});
			int action = event.getLink();
			int icon= 0;

			switch (action){
				case Event.LINK_GEO:
					icon = R.drawable.ic_location_filled;
					break;
				case Event.LINK_VIDEO:
					icon = R.drawable.ic_camera_filled;
					break;
				case Event.LINK_VIDEO_FAILED:
					icon = R.drawable.ic_camera_failed_filled;
					break;
				case Event.NO_LINK:
				default:
					break;
			}
			if(0!=action){
				topFrame.setActionButtonVisiblity(View.VISIBLE);
				topFrame.setActionButtonImage(icon);
				topFrame.setOnActionClickListener(new NTopInfoLayout.OnActionClickListener()
				{
					@Override
					public void onActionClick(int value)
					{
						switch (action){
							case Event.LINK_GEO:
								openGeo();
								break;
							case Event.LINK_VIDEO:
								Intent intent = new Intent(getBaseContext(), NCameraRecordViewActivity.class);
								intent.putExtra("event", event.id);
								startActivity(intent);
								overridePendingTransition(R.animator.enter, R.animator.exit);
								break;
							case Event.LINK_VIDEO_FAILED:
								Func.nShowMessage(context, context.getString(R.string.IV_CLIP_FAILED));
								break;
							case Event.NO_LINK:
							default:
								break;
						}
					}
				});
			}
		}

		setBottomFrame();
	}

	private void openGeo()
	{
		try
		{
			JSONObject jsonObject = new JSONObject(event.jdata);
			String lat = jsonObject.getString("lat");
			String lon = jsonObject.getString("lon");
			//						String uri = String.format(LocaleD3.ENGLISH, "geo:%f,%f", lat, lon);
			String uri = String.format("geo:0,0?q=" + lat + "," + lon + context.getString(R.string.EVENTS_ALARM_PLACE));
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		} catch (JSONException e)
		{
			e.printStackTrace();
			Func.nShowMessage(context, getString(R.string.N_EVENTS_LOCATION_GET_ERROR));
		}
	}

	private void setBottomFrame()
	{
		Zone zone = null;
		Section section = null;
		Device device = null;
		Site site = null;

		if(null!=bottomFrame){
			switch (event.zone){
				case 0:
					if(event.section != 0){
						section = dbHelper.getDeviceSectionById(event.device, event.section);
						break;
					}
				case 100:
					device = dbHelper.getDeviceById(event.device);
					break;
				default:
					device = dbHelper.getDeviceById(event.device);
					section = dbHelper.getDeviceSectionById(event.device, event.section);
					zone = dbHelper.getZoneById(event.device, event.section, event.zone);
					break;
			}
			bottomFrame.setZoneName(null!=zone ? zone.name: null);
//			bottomFrame.setSectionName(null!=section ? section.name : null);
			bottomFrame.setSectionName(event.getSectionName(context));
			bottomFrame.setDeviceName(null!= device? device.getName() + " " + device.account : null);
			bottomFrame.setSiteName(dbHelper.getSiteNameByDeviceSection(event.device, event.section));
			bottomFrame.setDesc(event.getDescription());
		}
	}

	public void showPopup(View v){
		PopupMenu popupMenu = new PopupMenu(context, v);
		MenuInflater inflater = popupMenu.getMenuInflater();
		popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener()
		{
			@Override
			public boolean onMenuItemClick(MenuItem menuItem)
			{
				switch (menuItem.getItemId()){
					case R.id.menu_about:
						Intent browserIntent = new Intent(Intent.ACTION_VIEW, Func.getWikiLink());
						startActivity(browserIntent);
						return true;
					default:
						return false;
				}
			}
		});
		inflater.inflate(R.menu.event_option_actions, popupMenu.getMenu());
		popupMenu.show();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(eventReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(eventReceiver);
	}
}
