package biz.teko.td.SHUB_APP.Profile.LocalConfig.Fragments;

import android.app.ActivityOptions;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import biz.teko.td.SHUB_APP.Fragments.ProgressDialogFragment;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Activities.LocalConfigNavigation;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Adapters.ConfigChooseAdapter;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Adapters.ConfigValuesAdapter;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceConfigDto;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.ILoadConfigList;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.IValueOperation;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Presenters.LocalDeviceConfigPresenter;
import biz.teko.td.SHUB_APP.Profile.Models.LocalConfigModel;
import biz.teko.td.SHUB_APP.Profile.Repository.LocalConfigRepository;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.UDP.Entities.Reply.BaseReplyConfig;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.ArmWaitReviewed;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.ArmWaitServer;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.BaseBooleanConfigEntity;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.Delay;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.DisarmWaitServer;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.RadioLiter;
import biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies.RoamingEnabled;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;
import io.reactivex.Observable;

public class LocalDeviceConfigFragment extends Fragment implements ILoadConfigList {
    private final LocalConfigNavigation activity;
    private View layout;
    private final DeviceDTO device;
    private final SocketConnection connection;
    private final String ip;
    private final int pin ;

    private RecyclerView recyclerConfigAPN, recyclerConfigAPN2, recyclerAnotherConfig, recyclerTemp;
    private List<LocalConfigModel> configs;
    private ProgressDialogFragment progressDialog;
    private LocalDeviceConfigPresenter presenter;
    private LocalConfigModel configModel;
    private String newDialogValue;
    private LinearLayout backButton;
    private TextView titleToolbar, subtitleToolbar, textSettingAPN2, textTemp;
    private NMenuListElement reset;
    private List<String> dialogValues = new ArrayList<>();
    private int dialogPosition = -1, hubVersion;
    private LocalConfigRepository repository;

    private final IValueOperation valueOperation = (value) -> {
        String type = configModel.getType();
        if (presenter.isBooleanValues(type) && !type.equals(ConfigConstants.CONFIG_RADIO_LITER))
            return BaseBooleanConfigEntity.getDescription(value, getResources());
        else
            return value;
    };
    //TODO
    /*@Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        presenter.registerMovement();
        return super.dispatchTouchEvent(ev);
    }*/

    @Override
    public void onResume() {
        super.onResume();
        if (!device.isTestMode()) {
            presenter.sendTestModeOn();
        }
    }

    public LocalDeviceConfigFragment(LocalConfigNavigation activity, DeviceDTO device, SocketConnection connection) {
        this.activity = activity;
        this.device = device;
        this.connection = connection;
        ip = device.getIp();
        pin = device.getPin();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_choose_config, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        activity.setSwipeBackEnable(false);
        layout = getView();
        initView();
        setListeners();
        setPresenter();
        //presenter.registerMovement();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*pin = getIntent().getIntExtra("pin", 0);
        ip = getIntent().getStringExtra("ip");*/

        repository = new LocalConfigRepository(getResources());
        configs = repository.getConfigs();

    }

    private void setPresenter() {
        presenter = new LocalDeviceConfigPresenter(connection);
        presenter.attachDeviceAndActivity(this, device);
        //shakeConnection();
        presenter.sendConfig();
    }


    @Override
    public void onStart() {
        super.onStart();
        activity.setOnBackPressedCallback(this::onBackPressed);
    }

    @Override
    public void onPause() {
        super.onPause();
        //stopConnection();
        //close();
    }

    @Override
    public void onDestroy() {
        if (!presenter.isCancel()) {
            //presenter.sendTestModeOff(ip, pin);
        } else if (presenter.isReset()) {
            Toast.makeText(activity, R.string.CONFIG_RESET_OK, Toast.LENGTH_LONG).show();
        }
        else if (presenter.isAccept()) {
            Toast.makeText(activity, getResources().getString(R.string.LOCAL_CONFIG_OK), Toast.LENGTH_LONG).show();
        }
        presenter.detach();
        presenter = null;
        super.onDestroy();
    }

    @Override
    public void close() {
        activity.runOnUiThread(this::onBackPressed);
    }


    public void onBackPressed() {
        activity.openDeviceList();
        activity.overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        *//*if (requestCode == Const.REQUEST_SET_CONFIG && resultCode == RESULT_OK) {
            Toast.makeText(activity, getResources().getString(R.string.LOCAL_CONFIG_OK), Toast.LENGTH_LONG).show();
            presenter.setCancel(true);
            close();
        } else if (requestCode == Const.REQUEST_RESET_CONFIG && resultCode == RESULT_OK) {
//            shakeConnection();
            presenter.attachConfigListActivity(this, ip, pin);
            presenter.setCancel(true);
            presenter.localConnectionSendReset();
        }*//*
    }*/


    @Override
    public void setConfigValues(HashMap<String, String> map) {
        hubVersion = Integer.parseInt(map.get(ConfigConstants.HUB_VERSION));
        addConfigsVersion();
        for (LocalConfigModel config : configs) {
            String type = config.getType();
            String fromMap = map.get(type);
            if (type.equals(ConfigConstants.APN_SERVER))
                setValueAndDesc(config, BaseReplyConfig.getAPNServerValue(fromMap), BaseReplyConfig.getAPNServerValue(fromMap));
            else if (type.equals(ConfigConstants.APN2_SERVER))
                setValueAndDesc(config, BaseReplyConfig.getAPNServerValue(fromMap), BaseReplyConfig.getAPNServerValue(fromMap));
            else if (presenter.isBooleanValues(type))
                setValueAndDesc(config, fromMap, BaseBooleanConfigEntity.getDescription(type, Boolean.parseBoolean(fromMap), getResources()));
            else
                setValueAndDesc(config, fromMap, (fromMap.isEmpty()) ?
                        getResources().getString(R.string.NOT_SPECIFIED) : fromMap);
        }
        setAdapters(map);
    }

    @Override
    public void showToast(int resId) {
        Toast.makeText(activity, resId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressBar() {
        progressDialog = new ProgressDialogFragment();
        progressDialog.setCancelable(false);
        progressDialog.show(getChildFragmentManager(), "progressBar");
    }

    @Override
    public void dismissProgressBar(boolean successful) {
        progressDialog.dismiss();
        if (!successful)
            Toast.makeText(activity, getResources().getString(R.string.D3_ERR_CONTRACTOR_NOT_FOUND), Toast.LENGTH_SHORT).show();
    }

    /*private void shakeConnection() {
        if (!presenter.isStartShake()) {
            presenter.localConnectionShake();
            presenter.sendReplyTimer();
            ///presenter.setIdlingTimeOut();
        }
    }

    private void stopConnection() {
        presenter.localConnectionStopSession();
        presenter.detach();
    }*/

    public void startSetConfigActivity() {
        activity.openConfigCustomization(new DeviceConfigDto(configModel, hubVersion));
        /*Intent intent = new Intent(activity, ConfigSetActivity.class);
        intent.putExtra("pin", pin);
        intent.putExtra("ip", ip);
        intent.putExtra(ConfigConstants.HUB_VERSION, hubVersion);
        intent.putExtra(ConfigConstants.LOCAL_CONFIG_MODEL, configModel);
        startActivityForResult(intent, Const.REQUEST_SET_CONFIG, getTransitionOptions(nameView).toBundle());*/
    }

    private void setAdapters(HashMap<String, String> map) {
        setApn2Adapter(map);
        setAdapter(repository.getApnSettings(), map, recyclerConfigAPN);
        setAdapter(repository.getAnotherSettings(hubVersion), map, recyclerAnotherConfig);
        setTempAdapter(map);
    }

    private void setApn2Adapter(HashMap<String, String> map) {
        if (hubVersion == 2) {
            recyclerConfigAPN2.setVisibility(View.VISIBLE);
            textSettingAPN2.setVisibility(View.VISIBLE);
            setAdapter(repository.getApn2Settings(), map, recyclerConfigAPN2);
        }
    }

    private void setTempAdapter(HashMap<String, String> map) {
        List<LocalConfigModel> list = repository.getTempSettings(hubVersion);
        if (list != null) {
            recyclerTemp.setVisibility(View.VISIBLE);
            textTemp.setVisibility(View.VISIBLE);
            setAdapter(list, map, recyclerTemp);
        }
    }

    private void setAdapter(List<LocalConfigModel> settings, HashMap<String, String> map, RecyclerView recyclerView) {
        ConfigChooseAdapter adapter = new ConfigChooseAdapter(settings);
        adapter.setOnConfigClickListener((type, type2) -> configClickListener(type));
        recyclerView.setAdapter(adapter);
    }

    private void configClickListener(String type) {
        Observable.fromIterable(configs)
                .filter(model -> model.getType().equals(type))
                .subscribe(model -> configModel = model).isDisposed();
        if (presenter.isDialogValues(configModel.getType()))
            startDialogConfig();
        else
            startSetConfigActivity();
    }

    private void startDialogConfig() {
        if (presenter.isBooleanValues(configModel.getType())) {
            BaseBooleanConfigEntity booleanConfig = null;
            switch (configModel.getType()) {
                case ConfigConstants.CONFIG_RADIO_LITER:
                    booleanConfig = new RadioLiter(); break;
                case ConfigConstants.CONFIG_WAIT_REVIEWED:
                    booleanConfig = new ArmWaitReviewed(); break;
                case ConfigConstants.CONFIG_ARM_SERVER:
                    booleanConfig = new ArmWaitServer(); break;
                case ConfigConstants.CONFIG_DISARM_SERVER:
                    booleanConfig = new DisarmWaitServer(); break;
                case ConfigConstants.CONFIG_ROAMING:
                    booleanConfig = new RoamingEnabled(); break;
            }
            if (booleanConfig != null) {
                dialogValues = booleanConfig.getValues();
                HashMap<Boolean, String> map = booleanConfig.getMap();
                String stringValue = map.get(Boolean.parseBoolean(configModel.getValue()));
                dialogPosition = dialogValues.indexOf(stringValue);
                newDialogValue = stringValue;
            }
        } else {
            Delay delay = new Delay();
            dialogValues = new ArrayList<>();
            Observable.fromIterable(delay.getValues())
                    .subscribe(integer -> dialogValues.add(String.valueOf(integer))).isDisposed();
            dialogPosition = dialogValues.indexOf(configModel.getDescription());
            newDialogValue = configModel.getValue();
        }
        startDialog();
    }

    private void startDialog() {
        NDialog dialog = new NDialog(activity, R.layout.n_dialog_select_value);
        setValuesAdapter(
                dialog.findViewById(R.id.recyclerValues),
                dialog.findViewById(R.id.nDialogTitle)
        );
        dialog.show();
        dialog.setOnActionClickListener((value, b) -> {
            if (value == NActionButton.VALUE_CANCEL)
                dialog.dismiss();
            else if (value == NActionButton.VALUE_OK) {
                dialog.dismiss();
                sendDialogConfig();
            }
        });
    }

    private void setValuesAdapter(RecyclerView recycler, TextView title) {
        title.setText(configModel.getTitle());
        ConfigValuesAdapter adapter = new ConfigValuesAdapter(dialogValues, dialogPosition);
        adapter.setItemClickListener(position -> newDialogValue = dialogValues.get(position));
        adapter.setValueOperation(valueOperation);
        recycler.setAdapter(adapter);
    }

    private void sendDialogConfig() {
        presenter.setDialogConfig(saveDialogData(), configModel.getType(), hubVersion, device);
    }

    private Bundle saveDialogData() {
        Bundle bundle = new Bundle();
        bundle.putString(configModel.getType(), newDialogValue);
        return bundle;
    }

    private void initView() {
        recyclerConfigAPN = layout.findViewById(R.id.recycler_config);
        recyclerConfigAPN2 = layout.findViewById(R.id.recyclerConfigAPN2);
        recyclerAnotherConfig = layout.findViewById(R.id.recyclerAnotherConfig);
        backButton = layout.findViewById(R.id.include_back);
        titleToolbar = layout.findViewById(R.id.text_title_toolbar);
        subtitleToolbar = layout.findViewById(R.id.text_subtitle_toolbar);
        textSettingAPN2 = layout.findViewById(R.id.textSettingAPN2);
        reset = layout.findViewById(R.id.nMenuReset);
        recyclerTemp = layout.findViewById(R.id.recyclerTempConfig);
        textTemp = layout.findViewById(R.id.textSettingTemp);
        subtitleToolbar.setVisibility(View.VISIBLE);
        subtitleToolbar.setText(device.getSerial());
        titleToolbar.setText(device.getName());
    }

    private void setListeners() {
        backButton.setOnClickListener(view ->{
            close();
            presenter.sendTestModeOff();
        });
        reset.setOnRootClickListener(this::showResetActivity);
    }


    private void showResetActivity()
    {
        activity.openFactoryResetConfirmation(device);
        //TODO
       /* Intent intent = new Intent(this, NDeleteActivity.class);
        intent.putExtra("type", Const.LOCAL_CONFIG);
        intent.putExtra("name", getIntent().getStringExtra("name"));
        intent.putExtra("serial", getIntent().getStringExtra("serial"));
        intent.putExtra("transition", true);
        startActivityForResult(intent, Const.REQUEST_RESET_CONFIG);*/
    }

    private void addConfigsVersion() {
        if (hubVersion == 2) {
            presenter.addAPN2(getResources(), configs);
        } else if (hubVersion == 3) {
            presenter.addAPN2(getResources(), configs);
            presenter.addTemp(getResources(), configs);
        } else if (hubVersion == 4) {
            presenter.addTemp(getResources(), configs);
        }
    }

    private void setValueAndDesc(LocalConfigModel config, String value, String desc) {
        config.setValue(value);
        config.setDescription(desc);
    }

//??
    private ActivityOptions getTransitionOptions(View view) {
        return ActivityOptions.makeSceneTransitionAnimation(activity,
                new Pair<>(view, Const.TITLE_TRANSITION_NAME));
    }


}
