package biz.teko.td.SHUB_APP.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 13.10.2017.
 */

public class NRListAdapter extends ArrayAdapter<Event>
{
	private LinkedList<Event> events;
	private Context context;
	private DBHelper dbHelper;

	public NRListAdapter(Context context, LinkedList<Event> objects)
	{
		super(context, R.layout.card_not_ready_zone, objects);
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.events = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		Event event = events.get(position);
		if(v==null){
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			v = inflater.inflate(R.layout.card_not_ready_zone, null, true);
		}
		setTextData(v, event);
		return v;
	}

	public void setTextData(View v, Event event)
	{
		TextView textSectionV = (TextView) v.findViewById(R.id.textSectionName);
		TextView textZoneV = (TextView) v.findViewById(R.id.textZoneName);
		String zoneName = dbHelper.getZoneNameById(event.device, event.section, event.zone);
		String sectionName = dbHelper.getSectionNameByIdWithOptions(event.device, event.section);
		textZoneV.setText(zoneName + " (" + event.zone + ")");
		textSectionV.setText(sectionName + " (" + event.section + ")");
	}
}
