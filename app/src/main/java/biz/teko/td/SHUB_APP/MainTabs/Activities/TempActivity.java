package biz.teko.td.SHUB_APP.MainTabs.Activities;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.view.ViewCompat;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class TempActivity extends BaseActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp);

        context = this;

        LinearLayout closeButton = (LinearLayout) findViewById(R.id.nButtonClose);
        if(null!=closeButton) closeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onBackPressed();
            }
        });

        CheckBox buttonFavorite = (CheckBox) findViewById(R.id.button_favorite);
        buttonFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b)
            {
                Func.pushToast(context, "", (TempActivity) context);
            }
        });

        TextView textView = (TextView) findViewById(R.id.titleText);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        ViewCompat.setTransitionName(textView, Const.TITLE_TRANSITION_NAME);
//        ViewCompat.setTransitionName(imageView, IMAGE_TRANSITION_NAME);

        String typeName =getIntent().getStringExtra("bar_type");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            addTransitionListener(false);
        }
//        Drawable image = getResources().getDrawable(R.drawable.ic_question_grey_500_24dip);
//        MainBar.Type type = MainBar.Type.valueOf(typeName);
//        switch (type){
//            case ARM:
//                image = getResources().getDrawable(R.drawable.ic_partly_armed_big);
//                break;
//            case DEVICE:
//                image = getResources().getDrawable(R.drawable.ic_sensor_brand_color);
//                break;
//            case CAMERA:
//                image = getResources().getDrawable(R.drawable.ic_camera_brand_color);
//                break;
//            case CONTROL:
//                image = getResources().getDrawable(R.drawable.ic_sensor_on);
//                break;
//            case EMPTY:
//                break;
//        }
        if(null!=textView)textView.setText(getIntent().getStringExtra("bar_title"));
//        if(null!=imageView)imageView.setImageDrawable(image);
    }

}
