package biz.teko.td.SHUB_APP.Wizards.Activity.Compoments;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.ExitSetViewPagerAdapter;

public class WizardExitSetActivity extends BaseActivity
{
	private View indicator1;
	private View indicator2;
	private View indicator3;
	private Context context;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private ExitSetViewPagerAdapter adapter;
	private ViewPager pager;
	private int selDeviceId;
	private int selExitNumber;
	private int selExitType;
	private String selExitName;
	private int selSectionId = 0;
	private int selSectionType;
	public final  static int PAGER_COUNT  = 2;

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver newZoneRegisteredReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(pager.getCurrentItem() == (PAGER_COUNT - 1) )
			{
				if(selDeviceId == intent.getIntExtra("device", 0));
				{
					selSectionId = intent.getIntExtra("section", 0);
					int zone = intent.getIntExtra("zone", 0);
					if (zone != 0)
					{
						setNextPage();
					}
				}
			}
		}
	};

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_exit_set);
		context = this;

		serviceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.EXIT_SET_TITLE);

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		ImageView closeButton = (ImageView) findViewById(R.id.wizCloseButton);
		closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});

		selDeviceId = getIntent().getIntExtra("device_id", -1);

		adapter = new ExitSetViewPagerAdapter(getSupportFragmentManager());
		pager = (ViewPager) findViewById(R.id.deviceSetViewPager);
		pager.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true;
			}
		});
		pager.setAdapter(adapter);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(newZoneRegisteredReceiver, new IntentFilter(D3Service.BROADCAST_ZONE_ADD));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(newZoneRegisteredReceiver);
	}

	@Override
	public void onBackPressed() {
		Func.hideKeyboard((WizardExitSetActivity)context);
		if (pager.getCurrentItem() == 0) {
			super.onBackPressed();
		} else {
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position - 1);
		}
	}

	public void setNextPage()
	{
		if(pager.getCurrentItem() != PAGER_COUNT - 1)
		{
			int position = pager.getCurrentItem();
			pager.setCurrentItem(position + 1);
			refreshViews();
		}else{

			Intent activityIntent = new Intent(getBaseContext(), WizardZoneSetFinishActivity.class);
			activityIntent.putExtra("device", selDeviceId );
			activityIntent.putExtra("section", selSectionId);
			activityIntent.putExtra("wizard", 0);
			startActivityForResult(activityIntent, D3Service.ADD_ZONE_IN_SECTION);
			finish();
		}
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	public int getSectionId(){
		return selSectionId;
	}

	public void setSectionId(int sectionId){
		this.selSectionId = sectionId;
	}

	public int getSectionType()
	{
		return selSectionType;
	}

	public void setSectionType(int setSectionType)
	{
		this.selSectionType = setSectionType;
	}


	public int  getDeviceId(){
		return selDeviceId;
	}

	public void  setDeviceid(int device_id){
		this.selDeviceId = device_id;
	}

	public int getExitNumber()
	{
		return selExitNumber;
	}

	public void setExitNumber(int selExitNumber)
	{
		this.selExitNumber = selExitNumber;
	}

	public int getExitType()
	{
		return selExitType;
	}

	public void setExitType(int selExitType)
	{
		this.selExitType = selExitType;
	}

	public String getExitName()
	{
		return selExitName;
	}

	public void setExitName(String selExitName)
	{
		this.selExitName = selExitName;
	}

	public void refreshViews(){
		adapter.notifyDataSetChanged();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.ADD_ZONE_IN_SECTION){
			if(resultCode == RESULT_OK){
				int sectionId = data.getIntExtra("section", 0);
				Intent intent = new Intent();
				intent.putExtra("section", sectionId);
				setResult(RESULT_OK, intent);
				finish();
			}
			else{
				onBackPressed();
			}
		}else{
			onBackPressed();
		}
	}


}
