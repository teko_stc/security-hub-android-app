package biz.teko.td.SHUB_APP.Cameras.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraSettingsActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;

/**
 * Created by td13017 on 26.02.2018.
 */

public class NIVCamerasCursorAdapter extends RecyclerView.Adapter<NIVCamerasCursorAdapter.ViewHolder>
{
	private final Context context;
	private final UserInfo userInfo;
	private String token;
	private final DBHelper dbHelper;
	private Cursor cursor;
	private WebHelper cRHelper;

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_camera, parent, false));
	}

	@Override
	public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
		cursor.moveToPosition(position);
		cRHelper = WebHelper.getInstance();
		token = dbHelper.getUserInfoIVToken();
		final Camera camera = dbHelper.getIVCameraByCursor(cursor);
		setTextViewText(holder.itemView, context, R.id.cameraName, 18, camera.name);
		setTextViewText(holder.itemView, context, R.id.cameraStatus, 14, camera.status == 1 ? context.getString(R.string.CAMERA_STATUS_ONLINE) : context.getString(R.string.CAMERA_STATUS_OFFLINE));
		Zone[] zones =  dbHelper.getZonesByIVCameraId(camera.id);
		setTextViewText(holder.itemView, context, R.id.cameraBind, 14, (null == zones) ? context.getString(R.string.CAMERA_STATUS_UNBINDED) : context.getString(R.string.CAMERA_STATUS_BINDED) );
		//		cRHelper.getCameraPreview(camera.id);
		setPreview(holder.itemView, R.id.imagePreview, camera.id);
		setSettingButtonClick(holder.itemView, camera.id);

		holder.itemView.setOnClickListener(v -> {
//				Intent intent = new Intent(getBaseContext(), CameraActivity.class);
//				intent.putExtra("cameraId", camera.id);
//				((Activity) context).startActivity(intent);
//				((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
		});
	}

	@Override
	public int getItemCount() {
		return cursor.getCount();
	}

	protected static final class ViewHolder extends RecyclerView.ViewHolder{

		public ViewHolder(@NonNull View itemView) {
			super(itemView);
		}
	}

	public NIVCamerasCursorAdapter(Context context, Cursor c)
	{
		super();
		this.cursor = c;
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();
		this.token = dbHelper.getUserInfoIVToken();
	}

	public void update(Cursor cursor){
		this.cursor = cursor;
		notifyDataSetChanged();
	}


	private void setSettingButtonClick(View view, final String cameraId)
	{
		ImageView button = view.findViewById(R.id.cameraSettingsButton);
		if((userInfo.roles& Const.Roles.DOMAIN_ADMIN) == 0)
		{
			button.setVisibility(View.GONE);
		}else
		{
			button.setOnClickListener(v -> {
				Intent intent = new Intent(context, NCameraSettingsActivity.class);
				intent.putExtra("cameraId", cameraId);
				context.startActivity(intent);
				((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
			});
		}
	}

	private void setTextViewText(View view, Context context, int id, int size, String text)
	{
		TextView textView = view.findViewById(id);
		textView.setText(text);
		textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
	}


	public void setPreview(View view, int id, String cameraId)
	{
		ImageView imageView = view.findViewById(id);
		String url = "https://openapi-alpha-eu01.ivideon.com/cameras/" + cameraId + "/live_preview?access_token=" + token;
		Picasso.with(context)
				.load(url)
				.memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
				.into(imageView);
	}
}
