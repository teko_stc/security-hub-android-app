package biz.teko.td.SHUB_APP.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardWelcomeActivity;


/**
 * Created by td13017 on 10.04.2017.
 */

public class D3NewProtocolVersionReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		((D3Service)context).stopConnection();
		Intent intentLoginPage = new Intent(context, WizardWelcomeActivity.class);
		intentLoginPage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intentLoginPage.putExtra("SERVICE", D3Service.NEW_PROTOCOL_VERSION);
		context.getApplicationContext().startActivity(intentLoginPage);	}
}
