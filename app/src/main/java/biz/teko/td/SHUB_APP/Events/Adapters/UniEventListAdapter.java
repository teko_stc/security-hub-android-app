package biz.teko.td.SHUB_APP.Events.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cursoradapter.widget.ResourceCursorAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraRecordViewActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/*DEPRECATED 18.11.2020*/

public class UniEventListAdapter extends ResourceCursorAdapter
{
	Cursor cursor;
	DBHelper dbHelper ;
	int id, type, site_id;
	Context context;
	String deviceModel, deviceSerial;

	public UniEventListAdapter(Context context, int layout, Cursor c, int flags, int type, int site_id)
	{
		super(context, layout, c, flags);
		this.id = id;
		this.cursor = cursor;
		this.context = context;
		this.type = type;
		this.site_id = site_id;
	}

	public void update(Cursor cursor)
	{
		this.changeCursor(cursor);
	}

	@Override
	public void bindView(View view, final Context context, Cursor cursor)
	{
		dbHelper= DBHelper.getInstance(context);
		final Event event = dbHelper.getEvent(cursor);
		final Device device = dbHelper.getDeviceById(event.device);
		if(null!=device) {
			deviceSerial = Integer.toString(device.account);
			deviceModel = device.getName();
		}


			String siteNameString = "";
		if(-1!=site_id)
		{
			siteNameString = dbHelper.getSiteNameById(site_id);
		}else{
			siteNameString =dbHelper.getSiteNameByDeviceSection(event.device, event.section);
		}

		TextView textSite = (TextView) view.findViewById(R.id.textSite);
		textSite.setText(siteNameString + " • " + deviceModel + "(" + deviceSerial + ")");
		TextView textLocal = (TextView) view.findViewById(R.id.textLocal);
		TextView textDescription = (TextView) view.findViewById(R.id.textDescription);
		TextView textTime = (TextView) view.findViewById(R.id.textTime);
		TextView textDate = (TextView) view.findViewById(R.id.textDate);

		view.setBackgroundColor(context.getResources().getColor(R.color.brandColorWhite));
		textTime.setText(Func.getServerTimeText(context, event.time));
		textDate.setText(Func.getServerDateText(context, event.time));

		textLocal.setText(event.getEventLocation(context));

		final String eventExplanation = event.explainEventRegDescription(context);
		textDescription.setText(Html.fromHtml(eventExplanation));
		final ImageView imageView  =(ImageView) view.findViewById(R.id.imageEvent);
		int imageResource = context.getResources().getIdentifier(event.explainIcon(), null, context.getPackageName());
		imageView.setImageResource(imageResource);

		ImageView optionImage = (ImageView) view.findViewById(R.id.imageOption);
		if(null!=optionImage)
		{
			optionImage.setImageResource(android.R.color.transparent);
			optionImage.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{

				}
			});
			if ((event.classId == 11) && (event.reasonId == 1))
			{
				optionImage.setImageResource(R.drawable.ic_location);
				optionImage.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						try
						{
							JSONObject jsonObject = new JSONObject(event.jdata);
							String lat = jsonObject.getString("lat");
							String lon = jsonObject.getString("lon");
							//						String uri = String.format(LocaleD3.ENGLISH, "geo:%f,%f", lat, lon);
							String uri = String.format("geo:0,0?q=" + lat + "," + lon + context.getString(R.string.EVENTS_ALARM_PLACE));
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							(App.getContext()).startActivity(intent);
						} catch (JSONException e)
						{
							e.printStackTrace();
						}


					}
				});
			}
			if(((event.classId == 2)||(event.classId == 1)||(event.classId == 0))&&((event.jdata !=null)&&(!event.jdata.equals("")))){
				if(event.jdata.contains("url")){
					optionImage.setImageResource(R.drawable.ic_camera_brand_color);
					optionImage.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							if ((event.jdata != null) && (!event.jdata.equals("")))
							{
								Intent intent = new Intent(context, NCameraRecordViewActivity.class);
								intent.putExtra("event", event.id);
								((Activity)context).startActivity(intent);
								((Activity)context).overridePendingTransition(R.animator.enter, R.animator.exit);
							}
						}
					});
				}else if(event.jdata.contains("notification")){
					optionImage.setImageResource(R.drawable.ic_videocam_off);
					optionImage.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							Func.nShowMessage(context, context.getString(R.string.IV_CLIP_FAILED));
						}
					});
				}

			}
		}

		view.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Func.showEventInfoDialog(context, event.id);
			}
		});
	}

	@Override
	public Cursor runQueryOnBackgroundThread(CharSequence constraint)
	{
		return super.runQueryOnBackgroundThread(constraint);
	}
}
