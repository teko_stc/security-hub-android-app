package biz.teko.td.SHUB_APP.Wizards.Activity.Compoments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SectionsTabs.Adapters.SectionTypesListAdapter;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

public class NWizardSectionActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private LinearLayout back;
	private LinearLayout add;
	private TextView nameTitle;
	private NEditText nameView;
	private NMenuListElement typeView;
	private LinearLayout limitsView;
	private TextInputLayout bottomLimitInputView;
	private TextInputLayout topLimitInputView;
	private NEditText bottomLimitView;
	private NEditText topLimitView;

	private String name;
	private int device_id;
	private Device device;

	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private int sending = 0;
	private boolean result = false;

	private BroadcastReceiver sectionAddReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int device_id = intent.getIntExtra("device", 0);
			if(null!=device && device.id == device_id){
				result = true;
				goBack();
			}
		}
	};

	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};
	private Section section;
	private NMenuListElement limitSwitch;
	private LinearLayout limitsFrame;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_wizard_section);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		device_id = getIntent().getIntExtra("device_id", 0 );
		if(0!=device_id) device = dbHelper.getDeviceById(device_id);
		section = new Section(Const.SECTIONTYPE_GUARD);

		setup();
		bind();
	}

	private void bind()
	{
		if(null!=back) back.setOnClickListener(v -> onBackPressed());

		if(null!=add)add.setOnClickListener(v -> {
			Func.hideKeyboard((Activity) context);
			addSection();
		});

		if(null!=nameTitle) nameTitle.setText(getResources().getString(R.string.N_WIZ_SECTION_NAME));
		if(null!=nameView) {
			nameView.setFocusableInTouchMode(true);
			nameView.setFocus();
			nameView.showKeyboard();
			nameView.setOnEditorActionListener((textView, i, keyEvent) -> {
				if(i == EditorInfo.IME_ACTION_DONE){
					addSection();
				}
				return false;
			});
		}
		setTypeView();
//		showLimitsView(section.detector == Const.SECTIONTYPE_TECH);
		if(null!=topLimitView) {
			topLimitView.setOnEditorActionListener((textView, i, keyEvent) -> {
				if(i == EditorInfo.IME_ACTION_DONE){
					addSection();
				}
				return false;
			});
		}

		if(null!=limitSwitch){
			limitSwitch.setSubtitle(getString(R.string.N_WIZ_SECTION_LIMITS_SUBTITLE));
			limitSwitch.setOnCheckedChangeListener(new NMenuListElement.OnCheckedChangeListener()
			{
				@Override
				public void onCheckedChanged()
				{
					if(null!=limitsFrame) limitsFrame.setVisibility(limitSwitch.isChecked() ? View.VISIBLE : View.GONE);
					limitSwitch.setSubtitle(limitSwitch.isChecked() ? getString(R.string.N_WIZ_SECTION_LIMITS_ALREADY_SET) : getResources().getString(R.string.N_WIZ_SECTION_LIMITS_SUBTITLE));
				}
			});
		}
	}

	private void setTypeView()
	{
		if(null!=typeView){
			typeView.setTitle(section.getType().caption);
			typeView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					if(null!=nameView) nameView.hideKeyBoard();
					if(null!=device) showSetTypeDialog();
				}
			});
		}
	}

	private void showSetTypeDialog()
	{
		final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(context);

		View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
		TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.STA_SECTION_CHANGE_SELECT_TYPE);

		ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
		listView.setDivider(null);

		LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
		LinkedList<SType> sTypes = new LinkedList<SType>();

		for (SType sType : sTypesList)
		{
			if (null != sType)
			{
				if (sType.id != Const.SECTIONTYPE_FIRE_DOUBLE && sType.id!= Const.SECTIONTYPE_TECH_CONTROL)
				{
					sTypes.add(sType);
				}
			}
		}
		final SType[] sTypesArray = new SType[sTypes.size()];
		for (int i = 0; i < sTypesArray.length; i++)
		{
			sTypesArray[i] = sTypes.get(i);
		}

		if(null!=sTypes){
			SectionTypesListAdapter sectionTypesListAdapter = new SectionTypesListAdapter(context, R.layout.n_card_for_list, sTypesArray, section.detector);
			listView.setAdapter(sectionTypesListAdapter);
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
			{
				@Override
				public void onItemClick(final AdapterView<?> parent, View view, final int position, long id)
				{
					setType((SType) parent.getItemAtPosition(position));
					bottomSheetDialog.dismiss();
				}
			});

		}
		bottomSheetDialog.setContentView(bottomView);
		bottomSheetDialog.show();

	}

	private void setType(SType sType)
	{
		this.section.detector = sType.id;
		setTypeView();
//		showLimitsView(section.detector == Const.SECTIONTYPE_TECH);
	}

	private void showLimitsView(boolean b)
	{
//		if(null!=limitsView) limitsView.setVisibility(b ? View.VISIBLE : View.GONE);
	}

	private void addSection()
	{
		if(null!=nameView) section.name = nameView.getText().toString();
		if(validate()){
			if(!limitSwitch.isChecked()){
				sendSetSectionCommand();
			}else if(getTopLimit().equals("") || getBottomLimit().equals("")){
				NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
				nDialog.setTitle(getString(R.string.N_WIZ_SECTION_ALERT_NO_LIMITS_SET));
				nDialog.setPositiveButton(getResources().getString(R.string.N_BUTTON_YES), () -> {
					sendSetSectionCommand();
					nDialog.dismiss();
				});
				nDialog.setNegativeButton(()->{
					nDialog.dismiss();
				});
				nDialog.show();
			}else if(getTopLimit().equals("") || getBottomLimit().equals("")){
				NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
				nDialog.setTitle(getString(R.string.N_SECTION_TEMO_LIMIT_ALERT_ONLY_ONE_LIMIT));
				nDialog.setPositiveButton(getResources().getString(R.string.N_BUTTON_YES), () -> {
					sendSetSectionCommand();
					nDialog.dismiss();
				});
				nDialog.setNegativeButton(()->{
					nDialog.dismiss();
				});
				nDialog.show();
			}else if(Integer.valueOf(getTopLimit()) < Integer.valueOf(getBottomLimit())){
				Func.nShowMessageSmall(context, getString(R.string.N_SECTION_TEMP_LIMIT_ALERT_TOP_SMALLER_BOTTOM));
			}else{
				sendSetSectionCommand();
			}
		}else{
			Func.pushToast(context, getString(R.string.N_WIZ_SEC_ERROR_ENTER_NAME));
		}
	}

	public String getTopLimit(){
		if(null!=topLimitView && null!=topLimitView.getText()) return topLimitView.getText().toString();
		return "";
	}

	public String getBottomLimit(){
		if(null!=bottomLimitView && null!=bottomLimitView.getText()) return bottomLimitView.getText().toString();
		return "";
	}

	private void sendSetSectionCommand()
	{

		String bottomS = getBottomLimit();
		String topS  = getTopLimit();
		JSONObject commandAddr = new JSONObject();
		try
		{
			commandAddr.put("name", section.name);
			commandAddr.put("type", section.detector);
			if(limitSwitch.isChecked())
			{
				if (!bottomS.equals("")) commandAddr.put("low_temp", Integer.parseInt(bottomS));
				if (!topS.equals("")) commandAddr.put("high_temp", Integer.parseInt(topS));
			}
			nSendMessageToServer(device, Command.SECTION_SET, commandAddr, true);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private boolean validate()
	{
		if(null!=section.name && !section.name.equals("")) return true;
		return false;
	}

	private void setup()
	{
		back = (LinearLayout) findViewById(R.id.nButtonBack);
		add = (LinearLayout) findViewById(R.id.nButtonNext);
		nameTitle = (TextView) findViewById(R.id.nTextTitle);
		nameView = (NEditText) findViewById(R.id.nText);
		typeView = (NMenuListElement) findViewById(R.id.nWizSectionTypeView);

//		limitsView = (LinearLayout) findViewById(R.id.nWizSectionLimitsView);
		limitsFrame = (LinearLayout) findViewById(R.id.nTempLimitsFrame);
		limitSwitch = (NMenuListElement) findViewById(R.id.nTempLimitsSwitch);
		bottomLimitInputView = (TextInputLayout) findViewById(R.id.nInputLayoutBottomLimit);
		topLimitInputView = (TextInputLayout) findViewById(R.id.nInputLayoutTopLimit);

		bottomLimitView = (NEditText) findViewById(R.id.nEditBottomLimit);
		topLimitView = (NEditText) findViewById(R.id.nEditTopLimit);

	}

	private void goBack()
	{
		if(result)
		{
			Intent intent1 = new Intent();
			setResult(RESULT_OK, intent1);
			finish();
		}else{
			onBackPressed();
		}
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	public void  onResume(){
		super.onResume();
		registerReceiver(sectionAddReceiver, new IntentFilter(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		bindD3();
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(sectionAddReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};
}
