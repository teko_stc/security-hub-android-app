package biz.teko.td.SHUB_APP.FaqTab.Faq;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

/**
 * Created by td13017 on 11.02.2017.
 */

public class FChapter
{
	@Attribute(name="id")
	public int id;

	@Attribute(name="caption")
	public String caption;

	@ElementList(name="questions", entry = "question")
	public LinkedList<FQuestion> fQuestions;
}
