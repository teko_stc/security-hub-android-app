package biz.teko.td.SHUB_APP.Utils.NViews;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NStoriesView extends LinearLayout {
    private static final int MAX_PROGRESS = 100;
    private static final int SPACE_BETWEEN_PROGRESS_BARS = 1;
    private static final int PROGRESS_HEIGHT = 3;
    private List<ProgressBar> progressBars = new ArrayList<>();
    private List<ObjectAnimator> animators = new ArrayList<>();
    private StoriesListener storiesListener;
    private int storiesCount = 0;
    private int current = 0;
    private boolean isReverse;

    public interface StoriesListener {
        void onNext();
        void OnPrevious();
        void onComplete();
    }

    public NStoriesView(Context context) {
        super(context);
    }

    public NStoriesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public NStoriesView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private void bindViews() {
        removeAllViews();
        for (int i = 0; i < storiesCount; i++) {
            ProgressBar p = createProgressBar();
            progressBars.add(p);
            addView(p);
            if (storiesCount > i + 1) {
                addView(createSpace());
            }
        }
    }

    private ProgressBar createProgressBar() {
        ProgressBar p = new ProgressBar(getContext(), null, android.R.attr.progressBarStyleHorizontal);
        p.setLayoutParams(new LayoutParams(0, Func.dpToPx(PROGRESS_HEIGHT, getContext()), 1));
        p.setProgressDrawable(ContextCompat.getDrawable(getContext(), R.drawable.n_story_progress_bar_white));
        p.setMax(MAX_PROGRESS);
        return p;
    }

    public void setWhiteStory() {
        setColorStory(R.drawable.n_story_progress_bar_white);
    }

    public void setBlueStory() {
        setColorStory(R.drawable.n_story_progress_bar_blue);
    }

    private void setColorStory(int resId) {
        for (ProgressBar progressBar : progressBars) {
            progressBar.setProgressDrawable(ContextCompat.getDrawable(getContext(), resId));
        }
    }

    private View createSpace() {
        View v = new View(getContext());
        v.setLayoutParams(new LayoutParams(
                Func.dpToPx(SPACE_BETWEEN_PROGRESS_BARS, getContext()), LayoutParams.WRAP_CONTENT
        ));
        return v;
    }

    public void setStoriesCount(int storiesCount) {
        this.storiesCount = storiesCount;
        bindViews();
    }

    public void setStoriesListener(StoriesListener storiesListener) {
        this.storiesListener = storiesListener;
    }

    public void skip() {
        ProgressBar p = progressBars.get(current);
        p.setProgress(p.getMax());
        animators.get(current).cancel();
    }

    public void pause() {
        ProgressBar p = progressBars.get(current);
        p.setProgress(p.getProgress());
        animators.get(current).pause();
    }

    public void resume() {
        ProgressBar p = progressBars.get(current);
        p.setProgress(p.getProgress());
        animators.get(current).resume();
    }

    public void reverse() {
        ProgressBar p = progressBars.get(current);
        p.setProgress(0);
        isReverse = true;
        animators.get(current).cancel();
        if (0 <= (current - 1)) {
            p = progressBars.get(current - 1);
            p.setProgress(0);
            animators.get(--current).start();
        } else {
            animators.get(current).start();
        }
    }

    public void setStoryDuration(long duration) {
        animators.clear();
        for (int i = 0; i < progressBars.size(); i++) {
            animators.add(createAnimator(i, duration));
        }
    }

    public void setStoriesCountWithDurations(long[] durations) {
        storiesCount = durations.length;
        bindViews();
        animators.clear();
        for (int i = 0; i < progressBars.size(); i++) {
            animators.add(createAnimator(i, durations[i]));
        }
    }

    public void playStories() {
        animators.get(0).start();
    }

    private ObjectAnimator createAnimator(int index, long duration) {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBars.get(index), "progress", MAX_PROGRESS);
        animation.setInterpolator(new LinearInterpolator());
        animation.setDuration(duration);
        animation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                current = index;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                animationEnd();
            }

            @Override
            public void onAnimationCancel(Animator animation) { }
            @Override
            public void onAnimationRepeat(Animator animation) { }
        });
        return animation;
    }

    private void animationEnd() {
        if (isReverse) {
            isReverse = false;
            if (storiesListener != null) storiesListener.OnPrevious();
            return;
        }
        int next = current + 1;
        if ((animators.size() - 1) >= next) {
            if (storiesListener != null) storiesListener.onNext();
            animators.get(next).start();
        } else {
            if (storiesListener != null) storiesListener.onComplete();
        }
    }

    public void destroy() {
        for (ObjectAnimator animator : animators) {
            animator.removeAllListeners();
            animator.cancel();
        }
    }

    public int getCurrent() {
        return current;
    }
}
