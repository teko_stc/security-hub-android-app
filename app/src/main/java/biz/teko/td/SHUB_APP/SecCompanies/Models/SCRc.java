package biz.teko.td.SHUB_APP.SecCompanies.Models;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementArray;

public class SCRc
{
	@Attribute(name="caption")
	public String caption;

	@Attribute(name="city", required = false)
	public String city;

	@ElementArray(required = false)
	public String[] addresses;

	@ElementArray(required = false)
	public String[] phones;

	@ElementArray(required = false)
	public String[] emails;

	@ElementArray(required = false)
	public String[] sites;
}
