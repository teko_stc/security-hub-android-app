package biz.teko.td.SHUB_APP.UDP.Entities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import biz.teko.td.SHUB_APP.UDP.LocalConnection;

public class TestMode extends RequestBasePincoded{
    public enum TestModeState {
        Off, On
    }

    public final static byte _command = 1;
    private byte state;

    public TestMode(TestModeState state, int pin) {
        this.command = _command;
        this.magic = LocalConnection.leIntToByteArray(pin);
        this.state = (byte) ((state == TestModeState.On) ? 1 : 0);
    }

    public byte[] getBytes() {
        try {
            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            outStream.write(magic);
            outStream.write(command);
            outStream.write(state);
            return outStream.toByteArray();
        } catch (IOException e) {e.printStackTrace();}
        return null;
    }
}
