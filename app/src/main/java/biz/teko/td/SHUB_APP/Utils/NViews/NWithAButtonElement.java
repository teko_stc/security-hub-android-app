package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.LinkedList;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;

public class NWithAButtonElement extends LinearLayout implements Textable
{
	public static  final int[] STATE_NO_CRIT = {R.attr.state_no_crit};
	public static  final int[] STATE_ALARM = {R.attr.state_alarm};
	public static  final int[] STATE_ATTENTION = {R.attr.state_attention};

	public static  final int[] STATE_INVERT = {R.attr.state_invert};
	public static  final int[] STATE_OFFLINE = {R.attr.state_offline};
	public static  final int[] STATE_RK_OFFLINE = {R.attr.state_rk_offline};
	public static  final int[] STATE_DELAY_ARM = {R.attr.state_delay_arm};
	public static  final int[] STATE_BLACK = {R.attr.state_black};

	public static  final int[] STATE_NO_ARM = {R.attr.state_no_arm};
	public static  final int[] STATE_ARM = {R.attr.state_arm};
	public static  final int[] STATE_DISARM = {R.attr.state_disarm};
	public static  final int[] STATE_PARTLY_ARM = {R.attr.state_partly_arm};

	public static  final int[] STATE_ON = {R.attr.state_on};
	public static  final int[] STATE_OFF = {R.attr.state_off};
	public static  final int[] STATE_AUTO = {R.attr.state_auto};
	public static  final int[] STATE_SCRIPTED = {R.attr.state_scripted};

	public static  final int[] STATE_NO_SECTION_CONTROL = {R.attr.state_no_section_control};
	public static  final int[] STATE_SECTION_CONTROL = {R.attr.state_section_control};

	private static final int DEFAULT_TEXT_COLOR = R.color.n_device_list_text_selector;
	private static final int DEFAULT_BACKGROUND = R.drawable.n_background_devices_list_selector;

	private final Context context;
	private int layout;
	private TextView textTitleView;
	private ImageView imageDescView;
	private ImageView imageStateView;
	private TextView textSubtitleView;
	private CheckBox checkBoxView;
	private NActionButton buttonActionView;

	private String title;
	private String subtitle;
	private Drawable imageDesc;
	private int imageState;
	private int imageAction;
	private String titleAction;
	private int colorAction;
	private int sizeAction;
	private int visibilityAction = -1;
	private boolean option = false;

	private boolean no_crit;
	private boolean alarm;
	private boolean attention;
	private boolean offline;
	private boolean rk_offline;
	private boolean delay;
	private boolean black;
	private boolean inverted;
	private boolean no_arm;
	private boolean armed;
	private boolean disarmed;
	private boolean partly_armed;
	private boolean turned_on;
	private boolean turned_off;
	private boolean auto;
	private boolean scripted;
	private boolean control;
	private boolean no_control;
	private boolean not_animate = false;
	private boolean increase = true;
	private boolean pressed;
	private boolean vibro = false;

	private final Handler handler = new Handler();
	private final Runnable runnable = new Runnable()
	{
		public void run()
		{
			checkPressedState();
		}
	};

	private int titleColor;
	private int subtitleColor;
	private int backGround;
	private FrameLayout rootView;
	private OnRootClickListener onRootClickListener;
	private OnRootLongClickListener onRootLongClickListener;
	private OnChildClickListener onChildClickListener;
	private int valueAction;


	public void setOnRootLongClickListener(OnRootLongClickListener onRootLongClickListener){
		this.onRootLongClickListener = onRootLongClickListener;
	}

	public void setOnRootClickListener(OnRootClickListener onRootClickListener){
		this.onRootClickListener = onRootClickListener;
	}

	public void setOnChildClickListener(OnChildClickListener onChildClickListener){
		this.onChildClickListener = onChildClickListener;
	}

	public interface OnRootLongClickListener{
		void onRootLongClick();
	}

	public interface OnChildClickListener{
		void onChildClick(int action, boolean option);
	}

	public interface OnRootClickListener{
		void onRootClick();
	}

	public NWithAButtonElement(Context context, int layout)
	{
		super(context);
		this.context = context;
		this.layout = layout;
		setupView();
	}

	public NWithAButtonElement(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setupView();
		bindView();
	}

	private void bindView()
	{
		setBackground();
		setTitleText();
		setSubtitleText();
		setDescImageView();
		setStateImageView();
		setActionButton();
		setCheckBox();
	}

	public void refreshStates()
	{
		flushCritStates();
		flushControlStates();
		refreshDrawableState();
	}

	private void flushCritStates(){
		this.no_crit = false;
		this.alarm = false;
		this.attention = false;
		this.offline = false;
		this.rk_offline = false;
		this.delay = false;
	}

	public void flushControlStates()
	{
		this.no_arm = false;
		this.armed = false;
		this.disarmed = false;
		this.partly_armed = false;
		this.turned_off = false;
		this.turned_on = false;
		this.auto = false;
		this.scripted = false;
		this.no_control = false;
		this.control = false;
	}

	private void setBackground()
	{
		if(null!=rootView){
			if(0==backGround){
				backGround = DEFAULT_BACKGROUND;
			}
			rootView.setBackground(getResources().getDrawable(backGround));
			rootView.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View view){}
			});
			rootView.setOnTouchListener(new OnTouchListener()
			{
				@Override
				public boolean onTouch(View view, MotionEvent event)
				{
					switch (event.getAction()){
						case MotionEvent.ACTION_DOWN:
							if(!pressed) {
								handler.postDelayed(runnable, 1000);
								pressed = true;
								setPressed(true); // это не определяется параметр pressed, ожидающий долгое нажатие
							}
							return false;
						case MotionEvent.ACTION_CANCEL:
						case MotionEvent.ACTION_UP:
							if(pressed)
							{
								pressed = false;
								handler.removeCallbacks(runnable);
								setPressed(false);
								if (event.getAction() == MotionEvent.ACTION_UP && null != onRootClickListener)
								{
									onRootClickListener.onRootClick();
								}
							}
							return false;
						case MotionEvent.ACTION_MOVE:
							return false;
						default:
							if(pressed)
							{
								pressed = false;
								handler.removeCallbacks(runnable);
							}
							setPressed(false);
							return false;
					}
				}
			});
		}
	}

	private void checkPressedState()
	{
		if(pressed){
			pressed = false;
			setPressed(false);
			if(null!=onRootLongClickListener) {
				onRootLongClickListener.onRootLongClick();
				if(vibro)((Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(10);
			}
		}
	}

	private void setActionButton()
	{
		if(null!=buttonActionView) {
			if(0!=imageAction) {
				buttonActionView.setImage(imageAction);
			}
			if(null!=titleAction){
				buttonActionView.setTitle(titleAction);
			}
			if(0!=colorAction){
				buttonActionView.setTextColor(colorAction);
			}
			if(0!=sizeAction){
				buttonActionView.setTextSize(sizeAction);
			}
			if(-1!=visibilityAction)
				buttonActionView.setVisibility(visibilityAction);
			buttonActionView.setAction(valueAction);
			buttonActionView.setIncrease(increase);
			buttonActionView.setContolAnimate(not_animate);
			buttonActionView.setOnButtonClickListener(new NActionButton.OnButtonClickListener()
			{
				@Override
				public void onButtonClick(int action)
				{
					if(null!=onChildClickListener) onChildClickListener.onChildClick(action, option);

				}
			});
		}
	}

	private void setCheckBox()
	{
		if(null!=checkBoxView){
			checkBoxView.setChecked(option);
			checkBoxView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
			{
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
				{
					setOption(isChecked);
				}
			});
		}
	}

	private void setOption(boolean b)
	{
		this.option = b;
	}

	private void setDescImageView()
	{
		if(null!= imageDescView && null!= imageDesc) {
			imageDescView.setImageDrawable(imageDesc);
		}
	}

	private void setStateImageView()
	{
		if(null!= imageStateView && 0!= imageState) {
			imageStateView.setImageDrawable(context.getResources().getDrawable(imageState));
		}
	}

	private void setSubtitleText()
	{
		if(null!=textSubtitleView){
			if(null!=subtitle)
			{
				textSubtitleView.setText(subtitle);
				textSubtitleView.setVisibility(VISIBLE);
				if (0 == subtitleColor) {
					subtitleColor = DEFAULT_TEXT_COLOR;
				}
				textSubtitleView.setTextColor(getResources().getColorStateList(subtitleColor));
			}else{
				textSubtitleView.setVisibility(GONE);
			}
		}
	}

	private void setTitleText()
	{
		if(null!=textTitleView){
			if(null!=title)
			{
				textTitleView.setText(title);
				textTitleView.setVisibility(VISIBLE);
				if (0 == titleColor) {
					titleColor = DEFAULT_TEXT_COLOR;
				}
				textTitleView.setTextColor(getResources().getColorStateList(titleColor));
			}else{
				textTitleView.setVisibility(GONE);
			}
		}
	}

	private void setupView()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
		layoutInflater.inflate(layout, this, true);

		rootView = (FrameLayout) findViewById(R.id.nRootLayout);
		textTitleView = (TextView) findViewById(R.id.nTextTitle);
		textSubtitleView = (TextView) findViewById(R.id.nTextSubtitle);
		imageDescView = (ImageView) findViewById(R.id.nImageDevice);
		imageStateView = (ImageView) findViewById(R.id.nImageState);
		buttonActionView = (NActionButton) findViewById(R.id.nImageAction);
		checkBoxView = (CheckBox) findViewById(R.id.nCheckBox);
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NWithAButtonElement, 0 , 0);
		try
		{
			layout = a.getResourceId(R.styleable.NWithAButtonElement_NDeviceListElementLayout, 0);
			title = a.getString(R.styleable.NWithAButtonElement_NDeviceListTitle);
			subtitle = a.getString(R.styleable.NWithAButtonElement_NDeviceListSubTitle);
			titleAction = a.getString(R.styleable.NWithAButtonElement_NDeviceListActionTitle);
			colorAction = a.getResourceId(R.styleable.NWithAButtonElement_NDeviceListTitleColor, R.color.brandColorWhite);
			titleColor = a.getResourceId(R.styleable.NWithAButtonElement_NDeviceListTitleColor, R.color.n_device_list_text_selector);
			subtitleColor = a.getResourceId(R.styleable.NWithAButtonElement_NDeviceListSubtitleColor, R.color.n_device_list_text_selector);
			backGround = a.getResourceId(R.styleable.NWithAButtonElement_NDeviceListBackground, R.drawable.n_background_devices_list_selector);
			imageDesc = a.getDrawable(R.styleable.NWithAButtonElement_NDeviceListDescImage);
			imageState = a.getResourceId(R.styleable.NWithAButtonElement_NDeviceListStateImage, 0);
			imageAction = a.getResourceId(R.styleable.NWithAButtonElement_NDeviceListActionImage, 0);;
			sizeAction = a.getInt(R.styleable.NWithAButtonElement_NDeviceListActionSize, 25);;
			increase = a.getBoolean(R.styleable.NWithAButtonElement_NDeviceListActionIncrease, true);;

		}finally
		{
			a.recycle();
		}
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 17);

		if(no_crit) mergeDrawableStates(drawableState, STATE_NO_CRIT);
		if(alarm) mergeDrawableStates(drawableState, STATE_ALARM);
		if(attention) {
			mergeDrawableStates(drawableState, STATE_ATTENTION);
		}

		if(inverted) mergeDrawableStates(drawableState, STATE_INVERT);
		if(offline) mergeDrawableStates(drawableState,STATE_OFFLINE);
		if(rk_offline) mergeDrawableStates(drawableState,STATE_RK_OFFLINE);
		if(delay) mergeDrawableStates(drawableState, STATE_DELAY_ARM);
		if(black) mergeDrawableStates(drawableState,STATE_BLACK);

		if(no_arm)mergeDrawableStates(drawableState, STATE_NO_ARM);
		if(armed)mergeDrawableStates(drawableState, STATE_ARM);
		if(disarmed) mergeDrawableStates(drawableState, STATE_DISARM);
		if(partly_armed)mergeDrawableStates(drawableState, STATE_PARTLY_ARM);
		if(turned_on)mergeDrawableStates(drawableState, STATE_ON);
		if(turned_off) mergeDrawableStates(drawableState, STATE_OFF);
		if(auto) mergeDrawableStates(drawableState, STATE_AUTO);
		if(scripted) mergeDrawableStates(drawableState, STATE_SCRIPTED);
		if(control) mergeDrawableStates(drawableState, STATE_SECTION_CONTROL);
		if(no_control) mergeDrawableStates(drawableState, STATE_NO_SECTION_CONTROL);
		return drawableState;
	}

	public void setVibro(boolean vibro)
	{
		this.vibro = vibro;
	}

	public void setActionImageNotAnim(boolean animate)
	{
		this.not_animate = animate;
		setActionButton();
	}

	public void setActionIncrease(boolean increase){
		this.increase = increase;
		setActionButton();
	}

	public void setActionValue(int action){
		this.valueAction = action;
		setActionButton();
	}

	public void setActionImage(int imageAction){
		this.imageAction = imageAction;
		setActionButton();
	}

	public void setActionTextColor(int colorAction)
	{
		this.colorAction = colorAction;
		setActionButton();
	}

	public void setActionTextSize(int sizeAction){
		this.sizeAction = sizeAction;
		setActionButton();
	}

	public void setActionTitle(String title){
		this.titleAction = title;
		setActionButton();
	}

	public void setVisibitilyAction(int visibilityAction){
		this.visibilityAction = visibilityAction;
		setActionButton();
	}

	public void setDescImage(Drawable imageDesc){
		this.imageDesc = imageDesc;
		setDescImageView();
	}

	public void setBackground(int backGround){
		this.backGround = backGround;
		setBackground();
	}

	public void setTitle(String title){
		this.title = title;
		setTitleText();
	}

	public void setSubtitle(String subtitle){
		this.subtitle = subtitle;
		setSubtitleText();
	}

	public void setTitleColor(int titleColor){
		this.titleColor = titleColor;
		setTitleText();
	}

	public void setSubtitleColor(int subtitleColor){
		this.subtitleColor = subtitleColor;
	}

	public void setCheck(boolean b){
		setOption(b);
		setCheckBox();
	}

	public TextView getTitleView(){
		return textTitleView;
	}

	public void setCrit(int value){
		switch (value){
			case 0:
				this.no_crit = false;
				this.alarm = true;
				this.attention = false;
				break;
			case 1:
				this.no_crit = false;
				this.alarm = false;
				this.attention = true;
				break;
			case -1:
				this.no_crit = true;
				this.alarm = false;
				this.attention = false;
				break;
		}
		refreshDrawableState();
	}

	public void setNoCrit(boolean no_crit)
	{
		flushCritStates();
		this.no_crit = no_crit;
		refreshDrawableState();
	}

	public void setAlarm(boolean alarm)
	{
		flushCritStates();
		this.alarm = alarm;
		refreshDrawableState();
	}

	public void setAttention(boolean attention){
		flushCritStates();
		this.attention = attention;
		refreshDrawableState();
	}

	public void setOffline(boolean offline)
	{
		flushCritStates();
		this.offline = offline;
		refreshDrawableState();
	}

	public void setRKOffline(boolean rk_offline)
	{
		flushCritStates();
		this.rk_offline = rk_offline;
		refreshDrawableState();
	}

	public void setDelay(boolean delay)
	{
		flushCritStates();
		this.delay = delay;
		refreshDrawableState();
	}

	public void setBlack(boolean black)
	{
		this.black = black;
		refreshDrawableState();
	}

	public void setNoArm(boolean no_arm){
		flushControlStates();
		this.no_arm = no_arm;
		refreshDrawableState();
	}

	public void setArmed(boolean armed){
		flushControlStates();
		this.armed = armed;
		refreshDrawableState();
	}

	public void setDisarmed(boolean disarmed){
		flushControlStates();
		this.disarmed = disarmed;
		refreshDrawableState();
	}

	public void setOn(boolean on){
		flushControlStates();
		this.turned_on = on;
		refreshDrawableState();
	}

	public void setOff(boolean off){
		flushControlStates();
		this.turned_off = off;
		refreshDrawableState();
	}

	public void setAuto(boolean auto){
		flushControlStates();
		this.auto = auto;
		refreshDrawableState();
	}

	public void setScripted(boolean scripted){
		flushControlStates();
		this.scripted = scripted;
		refreshDrawableState();
	}


	public void setControl(boolean control){
		flushControlStates();
		this.control = control;
		refreshDrawableState();
	}

	public void setNoControl(boolean no_control){
		flushControlStates();
		this.no_control = no_control;
		refreshDrawableState();
	}

	public void setPartlyArmed(boolean partly){
		flushControlStates();
		this.partly_armed = partly;
		refreshDrawableState();
	}

	public void setCritStates(LinkedList<Event> affects)
	{
		setNoCrit(true);
		if(null!=affects){
			if(0 < affects.size()){
				boolean alarm = false;
				boolean offline = false;
				boolean rk_offline = false;
				boolean attention = false;
				boolean delay = false;

				for(Event affect:affects){
					switch (affect.classId){
						case Const.CLASS_ALARM:
							alarm = true;
							break;
						case Const.CLASS_SABOTAGE:
						case Const.CLASS_ATTENTION:
							attention = true;
							break;
						case Const.CLASS_MALFUNCTION:
							if(affect.reasonId == Const.REASON_MALF_LOST_CONNECTION){
								if(affect.detectorId == Const.DETECTOR_CONTROLLER){
									offline = true;
								}else{
									rk_offline = true;
								}
							}else{
								attention = true;
							}
							break;
						case Const.CLASS_INFORMATION:
							switch (affect.reasonId){
								case Const.REASON_INFORMATION_ARM_DELAY:
									delay = true;
									break;
								default:
									break;
							}
							break;
						default:
							break;
					}
				}
				if(attention){
					setAttention(true);
				};
				if(rk_offline)
					setRKOffline(true);
				if(offline){
					setOffline(true);
				}
				if(delay){
					setDelay(true);
				}if(alarm){
					setAlarm(true);
				}

			}
		}
	}



}
