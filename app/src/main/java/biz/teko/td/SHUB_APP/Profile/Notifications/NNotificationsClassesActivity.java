package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.os.Bundle;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;

public class NNotificationsClassesActivity extends BaseActivity
{
    private NTopToolbarView toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_notifications_classes);
        setupView();
        setupPrefFragment();
    }

    private void setupPrefFragment() {
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.prefContainer, new NotificationsClassesPreferenceFragment())
                .commit();
    }

    private void setupView() {
        toolbar = (NTopToolbarView)findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }
}