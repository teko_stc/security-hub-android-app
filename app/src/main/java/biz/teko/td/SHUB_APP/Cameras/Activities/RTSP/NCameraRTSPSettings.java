package biz.teko.td.SHUB_APP.Cameras.Activities.RTSP;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class NCameraRTSPSettings extends BaseActivity
{
	private LinearLayout buttonBack;
	private NMenuListElement name;
	private NMenuListElement link;
	private NMenuListElement delete;
	private int camera_id;
	private Context context;
	private DBHelper dbHelper;
	private Camera camera;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_cameras_rtsp_settings);
		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);

		camera_id = getIntent().getIntExtra("camera", -1);

		setup();
	}

	private void setup()
	{
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		name = (NMenuListElement) findViewById(R.id.nMenuName);
		link = (NMenuListElement) findViewById(R.id.nMenuLink);
		delete = (NMenuListElement) findViewById(R.id.nMenuDelete);
	}

	private void bind()
	{
		if(-1 != camera_id) {
			camera = dbHelper.getRTSPCameraById(camera_id);
		}
		if(null!=buttonBack){
			buttonBack.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					onBackPressed();
					overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
				}
			});
		}
		if(null!=camera){
			if(null!=name){
				name.setTitle(camera.name);
				name.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						Intent intent = new Intent(context, NRenameActivity.class);
						intent.putExtra("camera_rtsp", camera.local_id);
						intent.putExtra("name", camera.name);
						intent.putExtra("type", Const.CAMERA_RTSP);
						startActivity(intent, getTransitionOptions(name).toBundle());
					}
				});
			}
			if(null!=link){
				link.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						Intent intent = new Intent(context, NCameraRTSPLink.class);
						intent.putExtra("camera_rtsp", camera.local_id);
						intent.putExtra("type", Const.CAMERA_RTSP);
						startActivity(intent, getTransitionOptions(link).toBundle());
					}
				});
			}
			if(null!=delete){
				delete.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						Intent intent = new Intent(context, NDeleteActivity.class);
						intent.putExtra("camera_rtsp", camera.local_id);
						intent.putExtra("name", camera.name);
						intent.putExtra("type", Const.CAMERA_RTSP);
						startActivityForResult(intent, Const.REQUEST_DELETE, getTransitionOptions(delete).toBundle());
					}
				});
			}
		}
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((NCameraRTSPSettings)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		bind();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
}
