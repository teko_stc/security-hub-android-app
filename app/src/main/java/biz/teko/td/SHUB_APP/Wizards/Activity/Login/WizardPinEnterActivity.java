package biz.teko.td.SHUB_APP.Wizards.Activity.Login;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import androidx.annotation.RequiresApi;
import androidx.legacy.app.ActivityCompat;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.MainNavigationActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDotsView;
import biz.teko.td.SHUB_APP.Utils.NViews.NKeyPadView;
import biz.teko.td.SHUB_APP.Utils.Other.FingerprintHandler;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 29.06.2017.
 */

public class WizardPinEnterActivity extends BaseActivity {
    private Context context;
    private DBHelper dbHelper;
    private int pin;
    private ServiceConnection serviceConnection;
    private D3Service myService;
    private boolean bound;
    private boolean fromAdding = false;
    private boolean fingerPrintActivated = false;
    private static final String KEY_NAME = "yourKey";
    private final int sending = 0;
    private int from;

    private KeyguardManager keyguardManager;
    private FingerprintManager fingerprintManager;
    private KeyStore keyStore;
    private Cipher cipher;
    private FingerprintManager.CryptoObject cryptoObject;

    private FingerprintHandler helper;

    private TextView title;
    private NActionButton forgetPnText;
    private NActionButton buttonChangeUser;
    private NDotsView dotsView;
    private NKeyPadView keypad;
    private boolean is_loading;

    private LinearLayout loading;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_wizard_pin_enter);
        setContentView(R.layout.n_activity_wizard_pin_enter);
        getSwipeBackLayout().setEnableGesture(false);
        context = this;
        dbHelper = DBHelper.getInstance(context);
        from = (getIntent().getIntExtra("FROM", 0));
        checkStates();

        setup();
        bind();

        pin = 0;
    }

    private void setup() {
        title = (TextView) findViewById(R.id.nPinTitle);
        forgetPnText = (NActionButton) findViewById(R.id.nPinLost);
        dotsView = (NDotsView) findViewById(R.id.nPinDots);
        keypad = (NKeyPadView) findViewById(R.id.nPinKeyPad);
        buttonChangeUser = (NActionButton) findViewById(R.id.nPinLogout);
        loading = (LinearLayout) findViewById(R.id.pinEnterProgressLayout);
        LinearLayout backLayout = (LinearLayout) findViewById(R.id.nButtonBack);

        backLayout.setOnClickListener(view -> onBackPressed());
        if (from == D3Service.OPEN_FROM_RESET_PIN) {
            buttonChangeUser.setVisibility(View.GONE);
            forgetPnText.setVisibility(View.GONE);
        } else
            backLayout.setVisibility(View.GONE);

    }

    private void bind() {
        activateFingerPrint();

        if (from == D3Service.OPEN_FROM_ADD_PIN) {
            fromAdding = true;
            if (null != keypad) keypad.setFPVisibility(View.GONE);
            if (null != title) title.setText(R.string.PN_ENTER_CONFIRM_PN);
            if (null != forgetPnText) forgetPnText.setVisibility(View.GONE);
        }

        if (null != keypad) {
            keypad.setOnKeyClickListener(new NKeyPadView.OnKeyClickListener() {
                @Override
                public void onKeyClick(int value) {
                    switch (value) {
                        case NKeyPadView.KEY_BS:
                            dotsView.decrease();
                            pin /= 10;
                            break;
                        case NKeyPadView.KEY_FP:
                            NDialog dialog = new NDialog(context, R.layout.n_dialog_fingerprint);
                            dialog.setTitle(getResources().getString(R.string.FINGERPRINT_AUTH_TITLE));
                            dialog.setSubTitle(getResources().getString(R.string.FIGERPRINT_AUTH_MESS));
                            dialog.setOnActionClickListener(new NDialog.OnActionClickListener() {
                                @Override
                                public void onActionClick(int value, boolean b) {
                                    dialog.dismiss();
                                }
                            });
                            dialog.show();
                            break;
                        default:
                            if (dotsView.increase()) {
                                pin = pin * 10 + value;
                            }
                            if (dotsView.getCount() == dotsView.getTotalCount() && !is_loading) {
                                goNext(1);
                            }
                            break;
                    }
                }
            });
        }

        if (null != forgetPnText) {
            forgetPnText.setOnButtonClickListener(action -> {
                NDialog dialog = new NDialog(context, R.layout.n_dialog_reset_pin);
                NEditText editPW = dialog.findViewById(R.id.passEditText);
                /*TODO
                 *  убрать в диалог*/
                TextInputLayout passwordLayout = dialog.findViewById(R.id.nEditPassInput);
                editPW.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        passwordLayout.setError(null);
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {

                    }
                });
                dialog.setOnActionClickListener((value, b) -> {
                    switch (value) {
                        case NActionButton.VALUE_CANCEL:
                            Func.hideEditTextKeyboard(editPW);
                            dialog.dismiss();
                            break;
                        case NActionButton.VALUE_OK:
                            if (0 != editPW.getText().toString().length()) {
                                String ps = Func.md5(editPW.getText().toString());
                                if (null != ps) {
                                    UserInfo userInfo = dbHelper.getUserInfo();
                                    String gps = userInfo.getUserPassword();
                                    if (0 == gps.compareTo(ps)) {
                                        Func.hideKeyboard((WizardPinEnterActivity) context);
                                        /*yes am*/
                                        dbHelper.setUserInfoPIN("");
                                        Intent intentBackToPinAdd = new Intent(getBaseContext(), WizardPinActivity.class);
                                        intentBackToPinAdd.putExtra("FROM", getIntent().getIntExtra("FROM", -1));
                                        finish();
                                        startActivity(intentBackToPinAdd);
                                        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
                                        Func.hideEditTextKeyboard(editPW);
                                        dialog.dismiss();
                                    } else {
                                        Func.pushToast(context, getString(R.string.PEA_WRONG_PASS_MESS), (WizardPinEnterActivity) context);
                                        passwordLayout.setError(" ");
                                    }
                                }
                            } else {
                                Func.pushToast(context, getString(R.string.PEA_ENTER_PASS_MESS), (WizardPinEnterActivity) context);
                                passwordLayout.setError(" ");
                            }
                            break;
                    }
                });
                dialog.show();
            });
        }
        if (null != buttonChangeUser) {
            buttonChangeUser.setOnButtonClickListener(action -> {
                NDialog dialog = new NDialog(context, R.layout.n_dialog_question_logout);
                dialog.setOptionCheck(true);
                dialog.setTitle(getResources().getString(R.string.PA_EXIT_DIALOG_TITLE_1) + " " + getResources().getString(R.string.application_name) + " " + getResources().getString(R.string.PA_EXIT_DIALOG_TITLE_2));
                dialog.setOnActionClickListener((value, b) -> {
                    switch (value) {
                        case NActionButton.VALUE_OK:
                            dialog.dismiss();
                            Intent manualLogoutIntent  = new Intent(D3Service.BROADCAST_CONNECTION_DROP);
                            manualLogoutIntent.putExtra("AppName", context.getPackageName());
                            manualLogoutIntent.putExtra("base", true);
                            manualLogoutIntent.putExtra("saveData", !b);
                            manualLogoutIntent.putExtra("from", "manual logout");
                            context.sendBroadcast(manualLogoutIntent);
                            break;
                        case NActionButton.VALUE_CANCEL:
                            dialog.dismiss();
                            break;
                    }
                });
                dialog.show();
            });
        }
    }

    private void checkStates() {
        if (!(getIntent().getIntExtra("FROM", -1) == D3Service.OPEN_FROM_ADD_PIN)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                    PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                    if (!pm.isIgnoringBatteryOptimizations(getPackageName())) {
                        Func.showDozeDialog(context);
                    }
                }
            }

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                if (getIntent().getIntExtra("SERVICE", -1) == D3Service.SERVICE_BEEN_STOPED) {
                    Func.showServiceStopedDialog(context);
                }
            }
        }

        if (Func.needLicenseAForBuild()) {
            if (!PrefUtils.getInstance(context).getLicenseAgreed()) {
                Func.showLicenseAgreementDialog(context);
            }
        }
    }


    public static class FingerprintException extends Throwable {
        public FingerprintException(Exception exc) {
            super(exc);
        }
    }

    private void activateFingerPrint() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!PrefUtils.getInstance(context).getFingerprintEnabled()) {
                if (null != keypad) keypad.setFPVisibility(View.GONE);
            } else {
                if (null != keypad) {
                    keypad.setFPVisibility(View.VISIBLE);
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            /*ACTIVATE FINGERPRINT*/
                            fingerPrintActivated = true;

                            //Get an instance of KeyguardManager and FingerprintManager//
                            keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
                            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
                            if (null != fingerprintManager) {
                                //checkStates whether the device has a fingerprint sensor//
                                if (!fingerprintManager.isHardwareDetected()) {
                                    // If a fingerprint sensor isn’t available, then inform the user that they’ll be unable to use your app’s fingerprint functionality//
                                    Func.nShowMessage(context, getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NO_FP_MODULE));
                                } else {
                                    //checkStates whether the user has granted your app the USE_FINGERPRINT permission//
                                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                                        // If your app doesn't have this permission, then display the following text//
                                        Func.nShowMessage(context, getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NO_RULES));
                                    } else {

                                        //checkStates that the user has registered at least one fingerprint//
                                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                                            // If the user hasn’t configured any fingerprints, then display the following message//
                                            Func.nShowMessage(context, getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NO_FINGERPINTS));
                                        } else {

                                            //checkStates that the lockscreen is secured//
                                            if (!keyguardManager.isKeyguardSecure()) {
                                                // If the user hasn’t secured their lockscreen with a PIN password or pattern, then display the following text//
                                                Func.nShowMessage(context, getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_NO_PIN));
                                            } else {
                                                try {
                                                    generateKey();
                                                } catch (FingerprintException e) {
                                                    e.printStackTrace();
                                                }

                                                if (initCipher()) {
                                                    //If the cipher is initialized successfully, then create a CryptoObject instance//
                                                    cryptoObject = new FingerprintManager.CryptoObject(cipher);

                                                    // Here, I’m referencing the FingerprintHandler class that we’ll create in the next section. This class will be responsible
                                                    // for starting the authentication process (via the startAuth method) and processing the authentication process events//
                                                    helper = new FingerprintHandler(context, result -> goNext(result));
                                                    helper.startAuth(fingerprintManager, cryptoObject, fingerPrintActivated);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                Func.nShowMessage(context, getResources().getString(R.string.FINGERPRINT_AUTH_ERROR_COMMON));
                            }
                        }
                    };
                    thread.run();
                }
            }
        } else {
            if (null != keypad) keypad.setFPVisibility(View.GONE);
        }
    }

    //Create a new method that we’ll use to initialize our cipher//
    @TargetApi(Build.VERSION_CODES.M)
    private boolean initCipher() {
        try {
            //Obtain a cipher instance and configure it with the properties required for fingerprint authentication//
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME, null);
            if (null != key) {
                cipher.init(Cipher.ENCRYPT_MODE, key);
                //Return true if the cipher has been initialized successfully//
                return true;
            }
            return false;
        } catch (KeyPermanentlyInvalidatedException e) {

            //Return false if cipher initialization failed//
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void generateKey() throws FingerprintException {
        try {
            // Obtain a reference to the Keystore using the standard Android keystore container identifier (“AndroidKeystore”)//
            keyStore = KeyStore.getInstance("AndroidKeyStore");

            //Generate the key//
            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");

            //Initialize an empty KeyStore//
            keyStore.load(null);

            //Initialize the KeyGenerator//
            keyGenerator.init(new

                    //Specify the operation(s) this key can be used for//
                    KeyGenParameterSpec.Builder(KEY_NAME, KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT).setBlockModes(KeyProperties.BLOCK_MODE_CBC)

                    //Configure this key so that the user has to confirm their identity with a fingerprint each time they want to use it//
                    .setUserAuthenticationRequired(true).setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7).build());

            //Generate the key//
            keyGenerator.generateKey();

        } catch (KeyStoreException | NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException | CertificateException | IOException exc) {
            exc.printStackTrace();
            throw new FingerprintException(exc);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindD3();

    }

    @Override
    protected void onPause() {
        super.onPause();
        unbindService(serviceConnection);
    }

    @Override
    public void onBackPressed() {
        if (fromAdding) {
            NDialog dialog = new NDialog(context, R.layout.n_dialog_question_small);
            dialog.setTitle(getResources().getString(R.string.MESS_FLASH_PIN));
            dialog.setOnActionClickListener(new NDialog.OnActionClickListener() {
                @Override
                public void onActionClick(int value, boolean b) {
                    switch (value) {
                        case NActionButton.VALUE_OK:
                            dbHelper.setUserInfoPIN("");
                            Intent intentBackToPinAdd = new Intent(getBaseContext(), WizardPinActivity.class);
                            intentBackToPinAdd.putExtra("FROM", from);
                            startActivity(intentBackToPinAdd);
                            overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
                            finish();
                            break;
                        case NActionButton.VALUE_CANCEL:
                            dialog.dismiss();
                            break;
                    }
                }
            });
            dialog.show();
        } else
            back();
    }

    private void back() {
        if (from == D3Service.OPEN_FROM_RESET_PIN) {
            setResult(RESULT_CANCELED);
            finish();
        }
        else
            finishAffinity();
    }

    /**
     * 0 - fingerprint
     * 1 - pin code
     */
    public void goNext(int method) {
        AsyncTask enterTask = new AsyncTask() {
            @Override
            protected void onPreExecute() {
                showLoading();
            }

            @Override
            protected void onPostExecute(Object o) {
            }

            @Override
            protected Object doInBackground(Object[] objects) {
                String pinString = Func.md5(String.valueOf(pin));
                String newPin = getIntent().getStringExtra("pin");
                UserInfo userInfo = dbHelper.getUserInfo();
                switch (method) {
                    case 0:
                        enterInApp();
                        fingerPrintActivated = false;
                        break;
                    case 1:
                        if (null != helper) {
                            helper.stopListeningAuthentication();
                        }
                        if ((from == D3Service.OPEN_FROM_RESET_PIN || from == D3Service.OPEN_FROM_ADD_PIN)
                                && null != newPin && newPin.equals(pinString)) {
                            dbHelper.setUserInfoPIN(newPin);
                            enterInApp();
                        } else if (from != D3Service.OPEN_FROM_RESET_PIN && from != D3Service.OPEN_FROM_ADD_PIN
                                && null != userInfo && userInfo.pinCode.equals(pinString)) {
                            enterInApp();
                        } else {
                            activateFingerPrint();
                            Func.pushToast(context, getString(R.string.PN_ADD_INCORRECT_PIN), (WizardPinEnterActivity) context);
                            if (null != dotsView) {
                                dotsView.setCount(0);
                            }
                            pin = 0;
                            dismissLoading();
                        }
                        break;
                }
                return null;
            }
        };
        if (!is_loading) {
            enterTask.execute();
        }
    }

    public void enterInApp() {
        if (!(getIntent().getIntExtra("FROM", -1) == D3Service.OPEN_FROM_RESET_PIN)) {
            startActivity(new Intent(context, MainNavigationActivity.class));
            overridePendingTransition(R.animator.enter, R.animator.exit);
        } else {
            setResult(RESULT_OK);
            overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
        }
        finish();
//		if (getIntent().getIntExtra("FROM", -1) == D3Service.OPEN_HISTORY_FROM_STATUS_BAR)
//		{
//			Intent activityIntent = new Intent(getBaseContext(), MainActivity.class);
////			activityIntent.putExtra("R", D3Service.OPEN_HISTORY_FROM_STATUS_BAR);
////			activityIntent.putExtra("FROM", D3Service.OPEN_FROM_STARTUP_ACTIVITY_AFTER_LOGIN);
//			finish();
//			startActivity(activityIntent);
//			overridePendingTransition(R.animator.enter, R.animator.exit);
//
//		} else
//		{
//			Intent activityIntent = new Intent(getBaseContext(), MainActivity.class);
////			activityIntent.putExtra("FROM", D3Service.OPEN_FROM_STARTUP_ACTIVITY_AFTER_LOGIN);
//			finish();
//			startActivity(activityIntent);
//			overridePendingTransition(R.animator.enter, R.animator.exit);
//
//		}
    }

    private void showLoading() {
        is_loading = true;
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (null != loading) loading.setVisibility(View.VISIBLE);
            }
        });
    }

    private void dismissLoading() {
        is_loading = false;
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (null != loading) loading.setVisibility(View.GONE);
            }
        });
    }

    private void bindD3() {
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = getServiceConnection();
        bindService(intent, serviceConnection, 0);
    }

    private ServiceConnection getServiceConnection() {
        return new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
    }

    public D3Service getLocalService() {
        if (bound) {
            return myService;
        } else {
            bindD3();
            if (null != myService) {
                return myService;
            }
            Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
        }
        return null;

    }

    private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command) {
        return Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
    }

}
