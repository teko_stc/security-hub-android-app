package biz.teko.td.SHUB_APP.Utils.Other.RadioGroup;


import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.R;

public class PresetValueButton extends LinearLayout implements RadioCheckable
{
	private int layout = R.layout.custom_preset_button;
	// Views
	private TextView titleTextView, subTitleTextView;
	private ImageSwitcher imageSwitcher;

	// Constants
	public static final int DEFAULT_TEXT_COLOR = Color.TRANSPARENT;

	// Attribute Variables
	private String title;
	private String subTitle;
	private Drawable image;
	private Drawable imagePressed;
	private int titleTextColor;
	private int subTitleTextColor;
	private int pressedTitleTextColor;

	// Variables
	private Drawable initialBackgroundDrawable;
	private OnClickListener onClickListener;
	private OnTouchListener onTouchListener;
	private boolean checked;
	private ArrayList<OnCheckedChangeListener> onCheckedChangeListeners = new ArrayList<>();
	private boolean animation;
	private Animation in, out, fade_in, fade_out;


	private  float rippleX;
	private float rippleY;
	private  Handler handler;
	int touchAction;
	float radius;
	float endRadius;
	float width, height, speed, duration;
	private Paint paint = new Paint();
	private int position;
	private int value;
	private int index;

	private MainBar.Type barType;
	private MainBar.SubType subType;
	private String elementId;


	//================================================================================
	// Constructors
	//================================================================================

	public PresetValueButton(Context context){
		super(context);
		setupView();
	}

	public PresetValueButton(Context context, int layout) {
		super(context);
		this.layout = layout;
		setupView();
	}

	public PresetValueButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		parseAttributes(attrs);
		setupView();
	}

	@RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
	public PresetValueButton(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		parseAttributes(attrs);
		setupView();
	}

	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
	public PresetValueButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
		super(context, attrs, defStyleAttr, defStyleRes);
		parseAttributes(attrs);
		setupView();
	}

	//================================================================================
	// Init & inflate methods
	//================================================================================

	private void parseAttributes(AttributeSet attrs) {
		TypedArray a = getContext().obtainStyledAttributes(attrs,
				R.styleable.PresetValueButton, 0, 0);
		Resources resources = getContext().getResources();
		try {
			layout = a.getResourceId(R.styleable.PresetValueButton_presetButtonLayout, 0);
			title = a.getString(R.styleable.PresetValueButton_presetButtonTitleText);
			subTitle = a.getString(R.styleable.PresetValueButton_presetButtonSubtitleText);
			position = a.getInt(R.styleable.PresetValueButton_presetButtonPosition, 0);
			image = a.getDrawable(R.styleable.PresetValueButton_presetButtonImage);
			imagePressed = a.getDrawable(R.styleable.PresetValueButton_presetButtonPressedImage);
			titleTextColor = a.getColor(R.styleable.PresetValueButton_presetButtonTitleTextColor, resources.getColor(R.color.brandColorBlack));
			subTitleTextColor = a.getColor(R.styleable.PresetValueButton_presetButtonSubtitleTextColor, resources.getColor(R.color.brandColorTextLightGrey));
			pressedTitleTextColor = a.getColor(R.styleable.PresetValueButton_presetButtonPressedTextColor, resources.getColor(R.color.brandColorDark));
		} finally {
			a.recycle();
		}
	}

	// Template method
	private void setupView() {
		inflateView();
		bindView();
		setCustomTouchListener();
	}

	protected void inflateView() {
		LayoutInflater inflater = LayoutInflater.from(getContext());
		inflater.inflate(layout, this, true);
		imageSwitcher = (ImageSwitcher) findViewById(R.id.image_value);
		imageSwitcher.setFactory(new ViewSwitcher.ViewFactory()
		{
			@Override
			public View makeView()
			{
				ImageView myView = new ImageView(getContext());
				return myView; 			}
		});


		titleTextView = (TextView) findViewById(R.id.text_view_title);
		subTitleTextView = (TextView) findViewById(R.id.text_view_subtitle);
		initialBackgroundDrawable = getBackground();

		handler = new Handler();
		setClickable(true);
	}

	public void bindView() {
		if (subTitleTextColor != DEFAULT_TEXT_COLOR) {
			subTitleTextView.setTextColor(subTitleTextColor);
		}
		if (titleTextColor != DEFAULT_TEXT_COLOR) {
			titleTextView.setTextColor(titleTextColor);
		}
		subTitleTextView.setText(subTitle);
		titleTextView.setText(title);

		if(image == null) image = getResources().getDrawable(R.drawable.ic_check_circle_grey_72);
		if(imagePressed == null) imagePressed = getResources().getDrawable(R.drawable.ic_check_circle_brand_72);
		imageSwitcher.setImageDrawable(image);

		if(animation){
			initAnim();}
	}

	private void initAnim()
	{
		in  = AnimationUtils.loadAnimation(getContext(), R.anim.turn_around);
		out = AnimationUtils.loadAnimation(getContext(), R.anim.turn_around_back);
		fade_in = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
		fade_in.setDuration(500);
		fade_out = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
		fade_out.setDuration(100);
		//		imageSwitcher.setInAnimation(in);
		//		imageSwitcher.setOutAnimation(fade_out);
	}

	//================================================================================
	// Overriding default behavior
	//================================================================================

	@Override
	public void setOnClickListener(@Nullable OnClickListener l) {
		onClickListener = l;
	}

	private void setCustomTouchListener() {
		super.setOnTouchListener(new TouchListener());
	}

	@Override
	public void setOnTouchListener(OnTouchListener onTouchListener) {
		this.onTouchListener = onTouchListener;
	}

	public OnTouchListener getOnTouchListener() {
		return onTouchListener;
	}

	private void onTouchDown(MotionEvent motionEvent) {
		setPressed(true);
		setChecked(true);
	}

	private void onTouchUp(MotionEvent motionEvent) {
		// Handle user defined click listeners
		setPressed(false);
		if (onClickListener != null) {
			onClickListener.onClick(this);
		}
	}
	//================================================================================
	// Public methods
	//================================================================================

	public void setCheckedState() {
		imageSwitcher.setOutAnimation(fade_out);
		imageSwitcher.setInAnimation(in);
		imageSwitcher.setImageDrawable(imagePressed);
		titleTextView.setTextColor(pressedTitleTextColor);
		setSelected(true);
	}

	public void setNormalState() {
		imageSwitcher.setOutAnimation(fade_out);
		imageSwitcher.setInAnimation(fade_in);
		imageSwitcher.setImageDrawable(image);
		titleTextView.setTextColor(titleTextColor);
		setSelected(false);
	}

	public int getValue(){
		return  value;
	}

	public void setValue(int value){
		this.value = value;
	}

	public int getPosition(){
		return position;
	}

	public void setPosition(int position){
		this.position = position;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public void setImage(Drawable drawable){
		this.image = drawable;
	}

	public void setPressedImage(Drawable drawable){
		this.imagePressed = drawable;
	}

	public void setTitleTextColor(int color){
		this.titleTextColor = color;
	}

	public void setSubTitleTextColor(int color){
		this.subTitleTextColor = color;
	}

	public void setPressedTitleTextColor(int color){
		this.pressedTitleTextColor = color;
	}


	public void setAnim(boolean b){
		this.animation = b;
	}
	//================================================================================
	// Checkable implementation
	//================================================================================

	@Override
	public void setChecked(boolean checked) {
		if (this.checked != checked) {
			this.checked = checked;
			if (!onCheckedChangeListeners.isEmpty()) {
				for (int i = 0; i < onCheckedChangeListeners.size(); i++) {
					onCheckedChangeListeners.get(i).onCheckedChanged(this, this.checked);
				}
			}
			if (this.checked) {
				setCheckedState();
			} else {
				setNormalState();
			}
		}
	}

	@Override
	public boolean isChecked() {
		return checked;
	}

	@Override
	public void toggle() {
		setChecked(!checked);
	}

	@Override
	public void addOnCheckChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
		onCheckedChangeListeners.add(onCheckedChangeListener);
	}

	@Override
	public void removeOnCheckChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
		onCheckedChangeListeners.remove(onCheckedChangeListener);
	}

	public int getIndex()
	{
		return index;
	}

	public void setIndex(int id){
		this.index = id;
	}

	public void setBarType(MainBar.Type barType)
	{
		this.barType = barType;
	}

	public MainBar.Type getBarType()
	{
		return barType;
	}

	public void setSubType(MainBar.SubType subType)
	{
		this.subType = subType;
	}
	public MainBar.SubType getSubType(){
		return this.subType;
	}

	public void setElementId(String id)
	{
		this.elementId = id;
	}

	public String getElementId()
	{
		return elementId;
	}

	//================================================================================
	// Inner classes
	//================================================================================
	private final class TouchListener implements OnTouchListener {

		@Override
		public boolean onTouch(final View v, MotionEvent event) {

			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					onTouchDown(event);
					break;
				case MotionEvent.ACTION_UP:
					onTouchUp(event);
					break;
			}
			if (onTouchListener != null) {
				onTouchListener.onTouch(v, event);
			}
			return true;
		}
	}


}