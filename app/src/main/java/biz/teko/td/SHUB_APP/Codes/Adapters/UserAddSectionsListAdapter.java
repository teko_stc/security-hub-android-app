package biz.teko.td.SHUB_APP.Codes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.R;

public class UserAddSectionsListAdapter extends ArrayAdapter<Section>
{
	private final Section[] sections;
	private final Context context;
	private final LayoutInflater layoutInflater;

	public UserAddSectionsListAdapter(Context context, int resource, Section[] sections)
	{
		super(context, resource);
		this.context = context;
		this.sections = sections.clone();
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, @NonNull ViewGroup parent)
	{
		View view = convertView;
		if(null==view)
		{
			view = layoutInflater.inflate(R.layout.card_for_section_zone_check, parent, false);
		}

		CheckBox sectionCheck = (CheckBox) view.findViewById(R.id.sectionCheck);
		TextView sectionName = (TextView) view.findViewById(R.id.sectionName);
		TextView sectionId = (TextView) view.findViewById(R.id.sectionId);

		final Section section = sections[position];

		if(null!=section)
		{
			sectionName.setText(section.name);
			sectionId.setText(context.getString(R.string.USER_ADD_SECTION_NUMBER_X) + " " + section.id);


			if(null!=sectionCheck) {
				sectionCheck.setChecked(section.checked);
				sectionCheck.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						section.checked = !section.checked;
						sectionCheck.setChecked(section.checked);
					}
				});
//				sectionCheck.setOnCheckedChangeListener((buttonView, isChecked) -> section.checked = isChecked);
			}
		}
		return view;
	}

//	private void setup (View view){
//		sectionCheck = (CheckBox) view.findViewById(R.id.sectionCheck);
//		sectionName = (TextView) view.findViewById(R.id.sectionName);
//		sectionId = (TextView) view.findViewById(R.id.sectionId);
//
//	}

	public Section[] getSections(){
		return sections;
	}

	public int getCount() {
		return sections.length;
	}

	public Section getItem(int position){
		return sections[position];
	}

	public long getItemId(int position){
		return position;
	}
}
