package biz.teko.td.SHUB_APP.ZonesTabs;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SlidingTabsLib.SlidingTabLayout;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.ZonesTabs.Adapters.ZonesViewPagerAdapter;

/**
 * Created by td13017 on 10.02.2017.
 */

public class ZonesActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private String siteName;
	private int siteId;
	private int sectionId;
	private int deviceId;
	private int zoneOrRelay;
	private int showSections = 1;
	private String sectionName;
	private ZonesViewPagerAdapter adapter;
	private CharSequence[] titles;
	private int numboftabs = 2;
	private ViewPager pager;
	private SlidingTabLayout tabs;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;

	private BroadcastReceiver connectionRepairReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int status = intent.getIntExtra("status", -1);
			if(status == 1)
			{
				RelativeLayout noConnLayout = (RelativeLayout) findViewById(R.id.noConnLayout);
				if (null != noConnLayout) noConnLayout.setVisibility(View.GONE);
				pager.setVisibility(View.VISIBLE);
				FrameLayout drawerContainer = (FrameLayout) findViewById(R.id.main_drawer_container);
				drawerContainer.invalidate();
			}
		}
	};
	private String deviceSerial, deviceModel;
	private boolean fabShown  = false;

	private BroadcastReceiver serviceAliveReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context1, Intent intent)
		{
			bindD3();
		}
	};

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_zones);
		context = this;
		dbHelper = DBHelper.getInstance(this);

		siteId= getIntent().getIntExtra("site", -1);
		sectionId= getIntent().getIntExtra("section", -1);
		deviceId= getIntent().getIntExtra("device", -1);
		zoneOrRelay = getIntent().getIntExtra("version", 0); // 0 - zones, 1- relays

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		String title = "";
		String subTitle = "";

		siteName = dbHelper.getSiteNameById(siteId);
		title += siteName;

		if(-1!=deviceId){
			Device device = dbHelper.getDeviceById(deviceId);
			if(null!=device){
				deviceSerial = Integer.toString(device.account);
				deviceModel = device.getName();
			}
			if(-1!=sectionId){
				sectionName =  dbHelper.getSectionNameByIdWithOptions(deviceId, sectionId);
				subTitle += deviceModel + "(" + deviceSerial + ")  •  " + sectionName;
			}else{
				subTitle+= deviceModel + "(" + deviceSerial + ")  •  " + (zoneOrRelay == 1? getResources().getString(R.string.RELAY) : getResources().getString(R.string.DEVICES_BIG));
			}
		}else{
			if(-1!=sectionId){
				sectionName =  dbHelper.getSectionNameByIdWithOptions(deviceId, sectionId);
				subTitle += sectionName+ ")  •  " + (zoneOrRelay == 1? getResources().getString(R.string.RELAY) : getResources().getString(R.string.DEVICES_BIG));
			}else{
				subTitle += (zoneOrRelay == 1? getResources().getString(R.string.RELAY) : getResources().getString(R.string.DEVICES_BIG));
			}
		}

		toolbar.setTitle(title);
		toolbar.setSubtitle(subTitle);
		toolbar.setSubtitleTextColor(getResources().getColor(R.color.brandColorWhite));

		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		// Creating The SitesViewPagerAdapter and Passing Fragment Manager, titles fot the Tabs and Number Of Tabs.
		titles = new CharSequence[]{zoneOrRelay == 0 ? context.getString(R.string.SSA_SUBTITLE_DETECTORS): context.getString(R.string.RELAY), context.getString(R.string.SUBTITLE_EVENTS)};
		adapter =  new ZonesViewPagerAdapter(getSupportFragmentManager(), titles, numboftabs, siteId, deviceId, sectionId, zoneOrRelay);

		// Assigning ViewPager View and setting the adapter
		pager = (ViewPager) findViewById(R.id.zonesPager);
		pager.setAdapter(adapter);

		// Assiging the Sliding Tab Layout View
		tabs = (SlidingTabLayout) findViewById(R.id.zonesTabs);
		tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

		// Setting Custom ColorX for the Scroll bar indicator of the Tab View
		tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
			@Override
			public int getIndicatorColor(int position) {
				return getResources().getColor(R.color.tabsScrollColor);
			}
		});

		// Setting the ViewPager For the SlidingTabsLayout
		tabs.setViewPager(pager);
	}

	public void  onResume(){
		super.onResume();
		registerReceiver(connectionRepairReceiver, new IntentFilter(D3Service.BROADCAST_CONNECTION_STATUS_CHANGE));
		registerReceiver(serviceAliveReceiver, new IntentFilter(D3Service.BROADCAST_D3SERVICE_ALIVE));
		bindD3();
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	public ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
				if(null!=myService){
					//					final Socket socket = myService.getReadingConnectionThreadSocket();
					//					final RelativeLayout noConnLayout = (RelativeLayout) findViewById(R.id.noConnLayout);

					if(null!=myService){
						setStatusConnected(myService.getStatus());
					}
				}
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(connectionRepairReceiver);
		unregisterReceiver(serviceAliveReceiver);
		unbindService(serviceConnection);
	}

	private void setStatusConnected(boolean status){
		final RelativeLayout noConnLayout = (RelativeLayout) findViewById(R.id.noConnLayout);
		if(status)
		{
			if (null != noConnLayout) noConnLayout.setVisibility(View.GONE);
			pager.setVisibility(View.VISIBLE);
		}else{
			if(null!=noConnLayout)
				noConnLayout.setVisibility(View.VISIBLE);
			pager.setVisibility(View.GONE);
		}
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	public void setFabStatus(boolean status){
		fabShown = status;
	}



}
