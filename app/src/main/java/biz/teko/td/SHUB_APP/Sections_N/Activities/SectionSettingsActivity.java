package biz.teko.td.SHUB_APP.Sections_N.Activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.LinkedList;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityOptionsCompat;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SectionsTabs.Adapters.SectionTypesListAdapter;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class SectionSettingsActivity extends BaseActivity {

    private Context context;
    private DBHelper dbHelper;
    private int section_id;
    private int device_id;
    private LinearLayout buttonBack;
    private NMenuListElement nameView;
    private NMenuListElement setTypeView;
    private NMenuListElement deleteView;
    private NMenuListElement setLimitsView;
    private Section section;
    private SType sType;

    private BroadcastReceiver updateReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            bindView();
        }
    };
    private boolean bound;
    private ServiceConnection serviceConnection;
    private D3Service myService;
    private int sending = 0;
    private UserInfo userInfo;
    private Device device;

    private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
        }
    };

    private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            int sending = intent.getIntExtra("command_been_send", 0);
            if(sending!=1)
            {
                switch (sending)
                {
                    case 0:
                        int result = intent.getIntExtra("result", 1);
                        if (result < 1)
                        {
                            Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
                        } else
                        {
                            Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
                        }
                        break;
                    case -1:
                        Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
                        break;
                    default:
                        break;
                }
            }
        }
    };

    private final ActivityResultLauncher<Intent> deleteActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(), result -> {
                if(result.getResultCode() == RESULT_OK){
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_section_settings);

        context = this;
        dbHelper = DBHelper.getInstance(context);
        userInfo = dbHelper.getUserInfo();

        section_id = getIntent().getIntExtra("section", -1);
        device_id = getIntent().getIntExtra("device", -1);

        setupView();
        bindView();
    }

    private void bindView()
    {
        section = dbHelper.getDeviceSectionByIdWithExtra(device_id, section_id);
        device = dbHelper.getDeviceById(device_id);

        if(null!=section) sType = section.getType();

        if(null!=buttonBack) buttonBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onBackPressed();
                overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
            }
        });
        if(null!=section) {
            if(null!=nameView){
                nameView.setTitle(section.name);
                if(maySet()){
                    nameView.setOnRootClickListener(() -> {
                        Intent intent = new Intent(context, NRenameActivity.class);
                        intent.putExtra("type", Const.SECTION);
                        intent.putExtra("device", section.device_id);
                        intent.putExtra("section", section.id);
                        intent.putExtra("name", section.name);
                        intent.putExtra("transition",true);
                        startActivity(intent, getTransitionOptions(nameView).toBundle());
                    });
                }else{
                    nameView.setOnRootClickListener(() -> Func.nShowMessage(context, getString(R.string.N_NO_PERMISSION)));
                }
            }
            if(null!=setTypeView) {
                if(null!=sType) setTypeView.setTitle(sType.caption);
                if(maySet()){
                    setTypeView.setOnRootClickListener(() -> changeSectionType());
                }else{
                    setTypeView.setOnRootClickListener(() -> Func.nShowMessage(context, getString(R.string.N_NO_PERMISSION)));
                }
            }
            if(null!=setLimitsView){
                if(maySet()){
                    setLimitsView.setOnRootClickListener(() -> {
                        Intent intent = new Intent(context, SectionTempLimitsActivity.class);
                        intent.putExtra("device", section.device_id);
                        intent.putExtra("section", section.id);
                        intent.putExtra("transition",true);
                        startActivity(intent, getTransitionOptions(setLimitsView).toBundle());
                    });
                }else{
                    setLimitsView.setVisibility(View.GONE);
                }
            }
            if(null!=deleteView){
                if(maySet()){
                    deleteView.setOnRootClickListener(() -> {
                        Intent intent = new Intent(context, NDeleteActivity.class);
                        intent.putExtra("type", Const.SECTION);
                        intent.putExtra("device", section.device_id);
                        intent.putExtra("section", section.id);
                        intent.putExtra("name", section.name);
                        intent.putExtra("transition",true);
                        deleteActivityLauncher.launch(intent, getTransitionOptions(deleteView));
//                        startActivityForResult(intent, Const.REQUEST_DELETE, getTransitionOptions(deleteView).toBundle());
                    });
                }else{
                    deleteView.setVisibility(View.GONE);
                }
            }
        }
    }

    private void changeSectionType(){
        if (Const.STATUS_DISARMED == dbHelper.getDeviceArmStatus(section.device_id))
        {
            final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(context);

            View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
            TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
            title.setText(R.string.STA_SECTION_CHANGE_SELECT_TYPE);

            ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
            listView.setDivider(null);

            LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
            LinkedList<SType> sTypes = new LinkedList<SType>();

            Device device = dbHelper.getDeviceById(section.device_id);
            if (device.canBeSetFromApp())
            {
                for (SType sType : sTypesList)
                {
                    if (null != sType)
                    {
                        if (sType.id != Const.SECTIONTYPE_FIRE_DOUBLE && sType.id!= Const.SECTIONTYPE_TECH_CONTROL)
                        {
                            sTypes.add(sType);
                        }
                    }
                }
            } else
            {
                sTypes = sTypesList;
            }
            final SType[] sTypesArray = new SType[sTypes.size()];
            for (int i = 0; i < sTypesArray.length; i++)
            {
                sTypesArray[i] = sTypes.get(i);
            }

            SectionTypesListAdapter sectionTypesListAdapter = new SectionTypesListAdapter(context, R.layout.n_card_for_list, sTypesArray, section.detector);
            listView.setAdapter(sectionTypesListAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(final AdapterView<?> parent, View view, final int position, long id)
                {
                    if (sending == 0)
                    {
                        String mess = context.getString(R.string.STA_CHANGE_SECTIONTYPE_QUESTION);
                        if (0 != dbHelper.getZonesCountSection(section.device_id, section.id))
                        {
                            mess = context.getString(R.string.STA_CHANGE_SECTION_TYPE_MESSAGE);
                        }
                        NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
                        nDialog.setTitle(mess);
                        nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
                        {
                            @Override
                            public void onActionClick(int value, boolean b)
                            {
                                switch (value){
                                    case NActionButton.VALUE_OK:
                                        SType sType = (SType) parent.getItemAtPosition(position);
                                        if (sType.id != section.detector)
                                        {
                                            JSONObject commandAddr = new JSONObject();
                                            try
                                            {
                                                commandAddr.put("index", section.id);
                                                commandAddr.put("type", sType.id);
                                                nSendMessageToServer(section, Command.SECTION_SET, commandAddr, true);
                                            } catch (JSONException e)
                                            {
                                                e.printStackTrace();
                                            }
                                            nDialog.dismiss();
                                        }
                                        break;
                                    case NActionButton.VALUE_CANCEL:
                                        nDialog.dismiss();
                                        break;
                                }
                            }
                        });
                        nDialog.show();
                    } else
                    {
                        Func.nShowMessage(context, context.getString(R.string.EA_CANNOT_SEND_COMMAND_EXIST));
                    }
                    bottomSheetDialog.dismiss();
                }
            });

            bottomSheetDialog.setContentView(bottomView);

            bottomSheetDialog.setOnShowListener(new DialogInterface.OnShowListener()
            {
                @Override
                public void onShow(DialogInterface dialog)
                {
                    FrameLayout bottomSheet = bottomSheetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

                    if (null != bottomSheet)
                    {
                        BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
                        behavior.setHideable(false);
                    }
                }
            });

            try
            {
                Field mBehaviorField = bottomSheetDialog.getClass().getDeclaredField("mBehavior");
                mBehaviorField.setAccessible(true);

                final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(bottomSheetDialog);
                behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
                {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState)
                    {
                        if (newState == BottomSheetBehavior.STATE_DRAGGING)
                        {
                            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        }
                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset)
                    {
                    }
                });
            } catch (NoSuchFieldException e)
            {
                e.printStackTrace();
            } catch (IllegalAccessException e)
            {
                e.printStackTrace();
            }

            bottomSheetDialog.show();

        } else
        {
            NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
            nDialog.setTitle(getString(R.string.N_SECTION_NEED_DISARM));
            nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
            {
                @Override
                public void onActionClick(int value, boolean b)
                {
                    switch (value){
                        case NActionButton.VALUE_CANCEL:
                            nDialog.dismiss();
                            break;
                    }
                }
            });
            nDialog.show();        }
    }

    private void setupView()
    {
        buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
        nameView = (NMenuListElement) findViewById(R.id.nMenuName);
        setTypeView = (NMenuListElement) findViewById(R.id.nMenuSetType);
        setLimitsView = (NMenuListElement) findViewById(R.id.nMenuSectionLimits);
        deleteView = (NMenuListElement) findViewById(R.id.nMenuDelete);
    }

    private ActivityOptionsCompat getTransitionOptions(View view)
    {
        return ActivityOptionsCompat.makeSceneTransitionAnimation((SectionSettingsActivity)context,((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME);
    }

    private boolean maySet()
    {
        return Func.nGetRemoteDeviceSetRights(userInfo, device);
    }

    private boolean maySystemSet()
    {
        return Func.nGetRemoteSystemSetRights(userInfo);
    }

    public void  onResume(){
        super.onResume();
        bindView();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
//        intentFilter.addAction(D3Service.BROADCAST_AFFECT);
//        intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
        registerReceiver(updateReceiver, intentFilter);
        registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
        registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
        bindD3();
    }

    public void onPause(){
        super.onPause();
        unregisterReceiver(updateReceiver);
        unregisterReceiver(commandResultReceiver);
        unregisterReceiver(commandSendReceiver);
        if(bound)unbindService(serviceConnection);
    }

    private void bindD3(){
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = getServiceConnection();
        bindService(intent, serviceConnection, 0);
    }

    private ServiceConnection getServiceConnection(){
        return new ServiceConnection()
        {
            public void onServiceConnected(ComponentName name, IBinder binder)
            {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name)
            {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
    }

    private D3Service getLocalService()
    {
        if(bound){
            return myService;
        }else{
            bindD3();
            if(null!=myService){
                return myService;
            }
            Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
        }
        return null;

    }

    private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
    {
        return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
    };

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
//    {
//        super.onActivityResult(requestCode, resultCode, data);
//
//    }

}
