package biz.teko.td.SHUB_APP.Utils.Other;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.preference.ListPreference;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import io.reactivex.subjects.PublishSubject;

public class NListPreference extends ListPreference {
    private NDialog dialog;
    private PublishSubject<Integer> changeSubject = PublishSubject.create();
    private boolean validate = true;

    public NListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NListPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public NListPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NListPreference(Context context) {
        super(context);
    }

    @Override
    public Dialog getDialog() {
        return dialog;
    }

    public PublishSubject<Integer> getChangeListener() {
        return changeSubject;
    }

    public void setPermission(boolean validate){
        this.validate = validate;
    }

    @Override
    protected void showDialog(Bundle state) {
        dialog = new NDialog(getContext(), R.layout.n_list_preference_dialog);
        View contentView = onCreateDialogView();
        if (contentView != null) {
            onBindDialogView(contentView);
            dialog.addView(contentView);
        }
        dialog.show();
    }

    @Override
    protected View onCreateDialogView() {
        setNegativeButtonText(null);
        View view = View.inflate(getContext(), R.layout.n_list_preference_view, null);
        CharSequence title = getDialogTitle();
        if (title == null) title = getTitle();
        ((TextView) view.findViewById(R.id.dialogTitleListPref)).setText(title);
        NActionButton cancel = view.findViewById(R.id.nDialogButtonCancel);
        cancel.setOnButtonClickListener(action -> getDialog().dismiss());
        ListView list = view.findViewById(android.R.id.list);
        list.setAdapter(setAdapter());
        list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        return view;
    }

    private ListPreferenceAdapter setAdapter() {
        ListPreferenceAdapter adapter = new ListPreferenceAdapter(getContext(), R.layout.n_list_preference_item, getEntries(), findIndexOfValue(getValue()));
        adapter.setItemClickListener(position -> {
            if(validate)
            {
                setValueIndex(position);
                setSummary(getEntries()[position]);
            }
            getDialog().dismiss();
            changeSubject.onNext(validate ? Integer.parseInt(getValue()): -1);
        });
        return adapter;
    }

    private static class ListPreferenceAdapter extends ArrayAdapter<CharSequence> {
        private CharSequence[] items;
        private int currPosition;
        private ItemClickListener clickListener;

        public interface ItemClickListener {
            void itemClick(int position);
        }

        void setItemClickListener(ItemClickListener clickListener) {
            this.clickListener = clickListener;
        }

        ListPreferenceAdapter(@NonNull Context context, int resource, @NonNull CharSequence[] objects, int currPosition) {
            super(context, resource, objects);
            this.items = objects;
            this.currPosition = currPosition;
        }

        @Override
        public int getCount() {
            return items.length;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.n_list_preference_item, null);
            TextView textView = view.findViewById(R.id.entryListPref);
            RadioButton radioButton = view.findViewById(R.id.radioButtonListPref);
            textView.setText(items[position]);
            if (position == currPosition)
                radioButton.setChecked(true);
            radioButton.setOnClickListener(v -> clickListener.itemClick(position));
            view.setOnClickListener(v -> clickListener.itemClick(position));
            return view;
        }
    }
}