package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Wizards.Activity.Login.WizardRegistrationActivity;

/**
 * Created by td13017 on 09.06.2017.
 */

public class RegisterFragment extends Fragment
{
	int position = 0;
	private Context context;
	private View v;

	private View indicator1;
	private View indicator2;
	private View indicator3;
	private View indicator4;
	int enteredCount;
	private int pin;


	public static RegisterFragment newInstance(int position){
		RegisterFragment registerFragment = new RegisterFragment();
		Bundle b = new Bundle();
		b.putInt("position", position);
		registerFragment.setArguments(b);
		return registerFragment;
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
	{
		position = getArguments().getInt("position");
		int layout = R.layout.fragment_wizard_reg_1;
		context = container.getContext();
		switch (position)
		{
			case 0:
				layout = R.layout.fragment_wizard_reg_1;
				v = inflater.inflate(layout, container, false);
				setFragmentOne(v);
				break;
			case 1:
				layout = R.layout.fragment_wizard_reg_2;
				v = inflater.inflate(layout, container, false);
				setFragmentTwo(v);
				break;
			case 2:
				layout = R.layout.fragment_wizard_reg_3;
				v = inflater.inflate(layout, container, false);
				setFragmentThree(v);
				break;
			case 3:
				layout = R.layout.fragment_wizard_reg_4;
				v = inflater.inflate(layout, container, false);
				setFragmentFour(v);
				break;
			case 4:
				layout = R.layout.fragment_wizard_reg_5;
				v = inflater.inflate(layout, container, false);
				setFragmentFive(v);
				break;
			default:
				break;
		}

		return v;
	}

	private void setFragmentOne(View v)
	{
		final Button next = (Button) v.findViewById(R.id.regNextButton);
		next.setText(R.string.WIZ_WELC_REGISTRATION);
		next.setEnabled(false);
		next.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				final Activity activity = (WizardRegistrationActivity)context;
				if(null!=activity){
					WizardRegistrationActivity wizardRegistrationActivity = (WizardRegistrationActivity) activity;
					wizardRegistrationActivity.setNextPage();
				}
			}
		});

		final EditText editName = (EditText) v.findViewById(R.id.wizRegLogin);
		final EditText editEmail = (EditText) v.findViewById(R.id.wizRegEmail);
		final EditText editPass = (EditText) v.findViewById(R.id.wizRegPass);
		final EditText editPassConfirm = (EditText) v.findViewById(R.id.wizRegConfirmPass);
		final CheckBox licCheck = (CheckBox) v.findViewById(R.id.licCheckBox);

		editName.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if(actionId == EditorInfo.IME_ACTION_NEXT){
					editEmail.requestFocus();
					return true;
				}
				return false;
			}
		});

		editEmail.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if(actionId == EditorInfo.IME_ACTION_NEXT){
					editPass.requestFocus();
					return true;
				}
				return false;
			}
		});

		editPass.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if(actionId == EditorInfo.IME_ACTION_NEXT){
					editPassConfirm.requestFocus();
					return true;
				}
				return false;
			}
		});
		editPass.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD);
		editPass.setTransformationMethod(new Func.AsteriskPasswordTransformationMethod());

		editPassConfirm.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if(actionId == EditorInfo.IME_ACTION_NEXT){
					if(licCheck.isChecked())
					{
//						Intent activityIntent = new Intent(getBaseContext(), WizardRegistrationFinishActivity.class);
//						((Activity) context).startActivity(activityIntent);
//						((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
						Func.hideKeyboard((WizardRegistrationActivity) context);
						Activity activity = (WizardRegistrationActivity)context;
						if(null!=activity){
							WizardRegistrationActivity wizardRegistrationActivity = (WizardRegistrationActivity) activity;
							wizardRegistrationActivity.setNextPage();
						}
					}else{
						Func.pushToast(context, getString(R.string.WIZARD_REGISTER_ACCEPT_LICENSE_TOAST), (Activity)context);
					}
					return true;
				}
				return false;
			}
		});
		editPassConfirm.setInputType(InputType.TYPE_TEXT_VARIATION_WEB_PASSWORD);
		editPassConfirm.setTransformationMethod(new Func.AsteriskPasswordTransformationMethod());

		licCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
		{
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				next.setEnabled(isChecked);
			}
		});

	}

	private void setFragmentTwo(View v)
	{
		Button next = (Button) v.findViewById(R.id.regNextButton);
		next.setText(R.string.WIZ_REG_ACCEPT);
		next.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				final Activity activity = (WizardRegistrationActivity)context;
				if(null!=activity){
					WizardRegistrationActivity wizardRegistrationActivity = (WizardRegistrationActivity) activity;
					wizardRegistrationActivity.setNextPage();
				}
			}
		});

		((WizardRegistrationActivity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		final EditText editCode = (EditText) v.findViewById(R.id.editCode);
		editCode.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true;
			}
		});

		LinearLayout[] linearLayouts = new LinearLayout[11];

		LinearLayout button0 = (LinearLayout) v.findViewById(R.id.buttonCode0);
		linearLayouts[0] = button0;
		LinearLayout button1 = (LinearLayout) v.findViewById(R.id.buttonCode1);
		linearLayouts[1] = button1;
		LinearLayout button2 = (LinearLayout) v.findViewById(R.id.buttonCode2);
		linearLayouts[2] = button2;
		LinearLayout button3 = (LinearLayout) v.findViewById(R.id.buttonCode3);
		linearLayouts[3] = button3;
		LinearLayout button4 = (LinearLayout) v.findViewById(R.id.buttonCode4);
		linearLayouts[4] = button4;
		LinearLayout button5 = (LinearLayout) v.findViewById(R.id.buttonCode5);
		linearLayouts[5] = button5;
		LinearLayout button6 = (LinearLayout) v.findViewById(R.id.buttonCode6);
		linearLayouts[6] = button6;
		LinearLayout button7 = (LinearLayout) v.findViewById(R.id.buttonCode7);
		linearLayouts[7] = button7;
		LinearLayout button8 = (LinearLayout) v.findViewById(R.id.buttonCode8);
		linearLayouts[8] = button8;
		LinearLayout button9 = (LinearLayout) v.findViewById(R.id.buttonCode9);
		linearLayouts[9] = button9;
		LinearLayout buttonBackSpace = (LinearLayout) v.findViewById(R.id.buttonCodeBackSpace);
		linearLayouts[10] = buttonBackSpace;

		for(int i = 0; i  < linearLayouts.length; i++){
			LinearLayout currentButton = linearLayouts[i];
			final int finalI = i;
			currentButton.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(finalI!=10)
					{
						if (editCode.getText().length() < 6)
						{
							buttonAction();
						}
					}else{
						if (editCode.getText().length() > 0)
						{
							buttonDelAction();
						}
					}
					editCode.setSelection(editCode.getText().length());
					final Activity activity = (WizardRegistrationActivity)context;
					if(null!=activity){
						WizardRegistrationActivity wizardRegistrationActivity = (WizardRegistrationActivity) activity;
						wizardRegistrationActivity.setVerificationCode(Integer.parseInt(editCode.getText().toString()));
					}
				}

				private void buttonDelAction()
				{
					editCode.setText(editCode.getText().subSequence(0, editCode.getText().length()-1));
				}

				private void buttonAction()
				{
					editCode.setText(editCode.getText() + String.valueOf(finalI));
				}
			});
		}
	}

	public void setFragmentThree(View v){
		Button next = (Button) v.findViewById(R.id.regNextButton);
		next.setText(R.string.WIZ_GONEXT);
		next.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				final Activity activity = (WizardRegistrationActivity)context;
				if(null!=activity){
					WizardRegistrationActivity wizardRegistrationActivity = (WizardRegistrationActivity) activity;
					wizardRegistrationActivity.setNextPage();
				}
			}
		});
	}


	public void setFragmentFour(View v)
	{
		final Button next = (Button) v.findViewById(R.id.regNextButton);
		next.setText(R.string.WIZ_GONEXT);
		next.setEnabled(false);
		next.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				final Activity activity = (WizardRegistrationActivity)context;
				if(null!=activity){
					WizardRegistrationActivity wizardRegistrationActivity = (WizardRegistrationActivity) activity;
					wizardRegistrationActivity.setNextPage();
				}
			}
		});

		enteredCount = 0;
		pin = 0;
		final Activity activity = (WizardRegistrationActivity)context;

		indicator1 = (View) v.findViewById(R.id.indicator1);
		indicator2 = (View) v.findViewById(R.id.indicator2);
		indicator3 = (View) v.findViewById(R.id.indicator3);
		indicator4 = (View) v.findViewById(R.id.indicator4);

		LinearLayout[] linearLayouts = new LinearLayout[12];

		LinearLayout button0 = (LinearLayout) v.findViewById(R.id.buttonCode0);
		linearLayouts[0] = button0;
		LinearLayout button1 = (LinearLayout) v.findViewById(R.id.buttonCode1);
		linearLayouts[1] = button1;
		LinearLayout button2 = (LinearLayout) v.findViewById(R.id.buttonCode2);
		linearLayouts[2] = button2;
		LinearLayout button3 = (LinearLayout) v.findViewById(R.id.buttonCode3);
		linearLayouts[3] = button3;
		LinearLayout button4 = (LinearLayout) v.findViewById(R.id.buttonCode4);
		linearLayouts[4] = button4;
		LinearLayout button5 = (LinearLayout) v.findViewById(R.id.buttonCode5);
		linearLayouts[5] = button5;
		LinearLayout button6 = (LinearLayout) v.findViewById(R.id.buttonCode6);
		linearLayouts[6] = button6;
		LinearLayout button7 = (LinearLayout) v.findViewById(R.id.buttonCode7);
		linearLayouts[7] = button7;
		LinearLayout button8 = (LinearLayout) v.findViewById(R.id.buttonCode8);
		linearLayouts[8] = button8;
		LinearLayout button9 = (LinearLayout) v.findViewById(R.id.buttonCode9);
		linearLayouts[9] = button9;
		LinearLayout buttonBackSpace = (LinearLayout) v.findViewById(R.id.buttonCodeBackSpace);
		linearLayouts[10] = buttonBackSpace;
		LinearLayout buttonFingerPrint = (LinearLayout) v.findViewById(R.id.buttonFingerPrint);
		linearLayouts[11] = buttonFingerPrint;


		for(int i = 0; i < linearLayouts.length-1; i++){
			final int finalI = i;
			linearLayouts[i].setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(finalI != 10)
					{
						if (enteredCount < 4)
						{
							enteredCount++;
							pin = pin*10 + finalI;
						}
					}else{
						if(enteredCount > 0){
							enteredCount--;
							pin = pin/10;
						}
					}
					updatePinIndicators(enteredCount);
					if(null!=activity){
						WizardRegistrationActivity wizardRegistrationActivity = (WizardRegistrationActivity) activity;
						if(enteredCount == 4){
							next.setEnabled(true);
							wizardRegistrationActivity.setPinCode(pin);
						}else{
							next.setEnabled(false);
						}
					}
				}

				private void updatePinIndicators(int enteredCount)
				{
					DisplayMetrics metrics = getResources().getDisplayMetrics();
					int resizeValue = (int) TypedValue.applyDimension(
							TypedValue.COMPLEX_UNIT_DIP, 20, metrics);
					int defaultValue = (int) TypedValue.applyDimension(
							TypedValue.COMPLEX_UNIT_DIP, 10, metrics);
					switch (enteredCount){
						case 0:
							indicator1.getLayoutParams().height = defaultValue;
							indicator1.getLayoutParams().width = defaultValue;
							indicator1.requestLayout();
							indicator2.getLayoutParams().height = defaultValue;
							indicator2.getLayoutParams().width = defaultValue;
							indicator2.requestLayout();
							indicator3.getLayoutParams().height = defaultValue;
							indicator3.getLayoutParams().width = defaultValue;
							indicator3.requestLayout();
							indicator4.getLayoutParams().height = defaultValue;
							indicator4.getLayoutParams().width = defaultValue;
							indicator4.requestLayout();
							break;
						case 1:
							indicator1.getLayoutParams().height = resizeValue;
							indicator1.getLayoutParams().width = resizeValue;
							indicator1.requestLayout();
							indicator2.getLayoutParams().height = defaultValue;
							indicator2.getLayoutParams().width = defaultValue;
							indicator2.requestLayout();
							indicator3.getLayoutParams().height = defaultValue;
							indicator3.getLayoutParams().width = defaultValue;
							indicator3.requestLayout();
							indicator4.getLayoutParams().height = defaultValue;
							indicator4.getLayoutParams().width = defaultValue;
							indicator4.requestLayout();
							break;
						case 2:
							indicator1.getLayoutParams().height = resizeValue;
							indicator1.getLayoutParams().width = resizeValue;
							indicator1.requestLayout();
							indicator2.getLayoutParams().height = resizeValue;
							indicator2.getLayoutParams().width = resizeValue;
							indicator2.requestLayout();
							indicator3.getLayoutParams().height = defaultValue;
							indicator3.getLayoutParams().width = defaultValue;
							indicator3.requestLayout();
							indicator4.getLayoutParams().height = defaultValue;
							indicator4.getLayoutParams().width = defaultValue;
							indicator4.requestLayout();
							break;
						case 3:
							indicator1.getLayoutParams().height = resizeValue;
							indicator1.getLayoutParams().width = resizeValue;
							indicator1.requestLayout();
							indicator2.getLayoutParams().height = resizeValue;
							indicator2.getLayoutParams().width = resizeValue;
							indicator2.requestLayout();
							indicator3.getLayoutParams().height = resizeValue;
							indicator3.getLayoutParams().width = resizeValue;
							indicator3.requestLayout();
							indicator4.getLayoutParams().height = defaultValue;
							indicator4.getLayoutParams().width = defaultValue;
							indicator4.requestLayout();
							break;
						case 4:
							indicator1.getLayoutParams().height = resizeValue;
							indicator1.getLayoutParams().width = resizeValue;
							indicator1.requestLayout();
							indicator2.getLayoutParams().height = resizeValue;
							indicator2.getLayoutParams().width = resizeValue;
							indicator2.requestLayout();
							indicator3.getLayoutParams().height = resizeValue;
							indicator3.getLayoutParams().width = resizeValue;
							indicator3.requestLayout();
							indicator4.getLayoutParams().height = resizeValue;
							indicator4.getLayoutParams().width = resizeValue;
							indicator4.requestLayout();
							break;
						default:
							break;
					}
				}
			});
		}

		linearLayouts[11].setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				//OPEN FINGERPRINT
			}
		});

	}

	private void setFragmentFive(View v)
	{
		Button next = (Button) v.findViewById(R.id.regNextButton);
		next.setText(R.string.WIZ_REG_GOREADY);
		next.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				final Activity activity = (WizardRegistrationActivity)context;
				if(null!=activity){
					WizardRegistrationActivity wizardRegistrationActivity = (WizardRegistrationActivity) activity;
					wizardRegistrationActivity.setNextPage();
				}
			}
		});

//		getFragmentManager().beginTransaction().replace(R.id.reg_notif_fragment_layout, new RegisterNotifPrefFragment()).commit();
	}
}
