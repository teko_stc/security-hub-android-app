package biz.teko.td.SHUB_APP.Profile.Repository;

import android.content.res.Resources;

import java.util.ArrayList;
import java.util.List;

import biz.teko.td.SHUB_APP.Profile.Models.LocalConfigModel;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;

public class LocalConfigRepository {
    private List<LocalConfigModel> configs = new ArrayList<>();
    private Resources resources;

    public LocalConfigRepository(Resources resources) {
        this.resources = resources;
    }

    public List<LocalConfigModel> getApnSettings() {
        return getSettings(0, 2);
    }

    public List<LocalConfigModel> getApn2Settings() {
        return getSettings(3, 5);
    }

    public List<LocalConfigModel> getAnotherSettings(int hubVersion) {
        if (hubVersion == 1)
            return getSettings(3, 18);
        else return getSettings(6, 21);
    }

    public List<LocalConfigModel> getTempSettings(int hubVersion) {
        if (hubVersion == 3) {
            return getSettings(22, 37);
        } else if (hubVersion == 4) {
            return getSettings(19, 34);
        }
        return null;
    }

    private List<LocalConfigModel> getSettings(int a, int b) {
        List<LocalConfigModel> settings = new ArrayList<>();
        for (int i = a; i <= b; i++)
            settings.add(configs.get(i));
        return settings;
    }

    public List<LocalConfigModel> getConfigs() {
        configs.add(new LocalConfigModel(resources.getString(R.string.N_APN_SERVER), ConfigConstants.APN_SERVER));
        configs.add(new LocalConfigModel(resources.getString(R.string.N_APN_LOGIN), ConfigConstants.APN_LOGIN));
        configs.add(new LocalConfigModel(resources.getString(R.string.N_APN_PASSWORD), ConfigConstants.APN_PASSWORD));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_PIN), ConfigConstants.CONFIG_PIN));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_RESERVE_PORT), ConfigConstants.CONFIG_RESERVE_PORT));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_MAIN_PORT), ConfigConstants.CONFIG_MAIN_PORT));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_RADIO_OFFSET), ConfigConstants.CONFIG_RADIO_OFFSET));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_SERVER_IP), ConfigConstants.CONFIG_SERVER_IP));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_MASK_IP), ConfigConstants.CONFIG_MASK_IP));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_GATEWAY_IP), ConfigConstants.CONFIG_GATEWAY_IP));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_STATIC_IP), ConfigConstants.CONFIG_STATIC_IP));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_RESERVE_IP), ConfigConstants.CONFIG_RESERVE_IP));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_RADIO_LITER), ConfigConstants.CONFIG_RADIO_LITER));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_WAIT_REVIEWED), ConfigConstants.CONFIG_WAIT_REVIEWED));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_ARM_SERVER), ConfigConstants.CONFIG_ARM_SERVER));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_DISARM_SERVER), ConfigConstants.CONFIG_DISARM_SERVER));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_ROAMING), ConfigConstants.CONFIG_ROAMING));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_ENTRY_DELAY), ConfigConstants.CONFIG_ENTRY_DELAY));
        configs.add(new LocalConfigModel(resources.getString(R.string.LOCAL_CONFIG_EXIT_DELAY), ConfigConstants.CONFIG_EXIT_DELAY));
        return configs;
    }
}
