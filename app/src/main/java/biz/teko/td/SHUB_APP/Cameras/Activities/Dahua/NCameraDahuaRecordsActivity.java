package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.company.PlaySDK.IPlaySDK;

import java.io.File;
import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public  class NCameraDahuaRecordsActivity extends BaseActivity
{
	private final int FLAG_RECORD_FILE_END = 2;
	ArrayList<String> mFilesPath = new ArrayList<String>();
	ListView mFileList;
	int port = IPlaySDK.PLAYGetFreePort() ;
	boolean bPlay = false;

	private String sn;
	private Context context;
	private DBHelper dbHelper;
	private Camera camera;
	private NTopToolbarView toolbar;
	private ListView list;
	private TextView emptyFrame;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_dahua_records);

		context = this;
		dbHelper = DBHelper.getInstance(context);

		setup();
	}

	private void setup()
	{
		sn = getIntent().getStringExtra("cameraId");
		if(null!=sn) camera = dbHelper.getDahuaCameraBySn(sn);

		toolbar =  (NTopToolbarView) findViewById(R.id.toolbar);
		list = (ListView) findViewById(R.id.file_list);
		emptyFrame = (TextView) findViewById(R.id.nTextEmpty);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		bind();
	}

	private void bind()
	{
		if(null!=toolbar)
		{
			toolbar.setTitle("");
			toolbar.setOnBackClickListener(() -> onBackPressed());
		}

		if(null!=list){
			ArrayList<String> files = getFiles();
			if(null!=files && files.size() > 0)
			{
				list.setVisibility(View.VISIBLE);
				emptyFrame.setVisibility(View.GONE);
				list.setAdapter(new ArrayAdapter<String>(this, R.layout.n_record_list_element, R.id.nTextTitle, files));
			}else{
				list.setVisibility(View.GONE);
				emptyFrame.setVisibility(View.VISIBLE);
			}
			list.setOnItemClickListener((parent, view, position, id) -> {
				TextView tv = (TextView)view.findViewById(R.id.nTextTitle);
				String filename = tv.getText().toString();
				showFile(position);
			});
		}
	}

	private void showFile(int position)
	{
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final View view = LayoutInflater.from(this).inflate(R.layout.n_dialog_record,null);
		final SurfaceView sv = (SurfaceView)view.findViewById(R.id.n_record_surface);
		builder.setView(view);
		builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				stopPlayback();
			}
		});
		Dialog dialog = builder.create();
		Window window = dialog.getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		params.dimAmount = 0f;
		window.setAttributes(params);
		dialog.show();
		sv.getHolder().addCallback(new SurfaceHolder.Callback() {
			public void surfaceCreated(SurfaceHolder holder){
				if(bPlay) {
					IPlaySDK.PLAYSetDisplayRegion(port, 0, null, holder.getSurface(), 1);
				}
			}

			public void surfaceChanged(SurfaceHolder holder, int format, int width,
									   int height)	{
			}

			public void surfaceDestroyed(SurfaceHolder holder){
				if(bPlay) {
					IPlaySDK.PLAYSetDisplayRegion(port, 0, null, holder.getSurface(), 0);
				}
			}
		});
		if (!bPlay) {
			bPlay = true;
			playBackEx(sv, mFilesPath.get(position));
		}
	}

	private void playBackEx(SurfaceView sv,final String file){
		long userData = 0;
		if(IPlaySDK.PLAYSetFileEndCallBack(port, (i, l) -> {
			Handler handler = new Handler(Looper.getMainLooper()){
				@Override
				public void handleMessage(Message msg){
					switch (msg.what){
						case FLAG_RECORD_FILE_END:
							stopPlayback();
							break;
						default:
							break;
					}
				}
			};
			Message msg = handler.obtainMessage(2);
			msg.what = FLAG_RECORD_FILE_END;
			handler.sendMessage(msg);
		},userData) == 0){
			Func.log_d(Const.LOG_TAG_DAHUA, "PLAYSetFileEndCallBack failed"+IPlaySDK.PLAYGetLastError(port));
			return;
		}
		if (IPlaySDK.PLAYOpenFile(port,file) == 0) {
			Func.log_d(Const.LOG_TAG_DAHUA, "PLAYOpenFile failed"+IPlaySDK.PLAYGetLastError(port));
			return;
		}
		if (IPlaySDK.PLAYSetDecodeThreadNum(port,4) == 0) {
			Func.log_d(Const.LOG_TAG_DAHUA, "PLAYSetDecodeThreadNum failed"+IPlaySDK.PLAYGetLastError(port));
			return;
		}
		if (IPlaySDK.PLAYPlay(port,sv.getHolder().getSurface()) == 0) {
			Func.log_d(Const.LOG_TAG_DAHUA, "PLAYPlay failed"+IPlaySDK.PLAYGetLastError(port));
			return;
		}
		if (IPlaySDK.PLAYPlaySound(port) == 0) {
			Func.log_d(Const.LOG_TAG_DAHUA, "PLAYPlaySound failed"+IPlaySDK.PLAYGetLastError(port));
			return;
		}
	}

	private void stopPlayback(){
		IPlaySDK.PLAYRigisterDrawFun(port,0,null,0);
		IPlaySDK.PLAYStopSound();
		IPlaySDK.PLAYCleanScreen(port,0,0,0,1,0);
		IPlaySDK.PLAYStop(port);
		IPlaySDK.PLAYCloseFile(port);
		bPlay = false;
	}

	private ArrayList<String> getFiles(){
		File[]  files1 = (new File(PrefUtils.getInstance(context).getMediaFilesDirectory() + "/" + camera.sn)).listFiles();
		ArrayList<String>  list = new ArrayList<String>();
		if(files1 != null) {
			for (int i = 0; i < files1.length; i++) {
//				ToolKits.writeLog(files1[i].getAbsoluteFile().getAbsolutePath());
				if(!files1[i].isDirectory()) {
					list.add(files1[i].getName());
					mFilesPath.add(files1[i].getAbsoluteFile().getAbsolutePath());
				}
			}
		}
		Func.log_d(Const.LOG_TAG_DAHUA, "list.size == "+list.size());
		return list;
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}
}
