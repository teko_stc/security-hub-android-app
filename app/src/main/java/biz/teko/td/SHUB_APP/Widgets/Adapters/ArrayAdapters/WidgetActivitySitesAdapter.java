package biz.teko.td.SHUB_APP.Widgets.Adapters.ArrayAdapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 23.05.2017.
 */

public class WidgetActivitySitesAdapter extends ArrayAdapter<Site>
{
	private final Context context;
	private final Site[] sites;
	private int selectedPosition = 0;
	boolean selected = true;

	public WidgetActivitySitesAdapter(Context context, int resource, Site[] sites)
	{
		super(context, resource, sites);
		this.context = context;
		this.sites = sites;
	}
//
//	public int getCount(){
//		return sites.length;
//	}
//
	public Site getSelectedItem(){
		if(selected)
		{
			return sites[selectedPosition];
		}else{
			return null;
		}
	}
//
	public long getSelectedItemId(int position){
		if(selected)
		{
			return selectedPosition;
		}else{
			return -1;
		}
	}

	public Site getItem(int position){
		return sites[position];
	}

	public long getItemId(int position){
		return position;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandColorLight));
		label.setText(sites[position].name);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		label.setPadding(0, Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandColorLight));
		label.setText(sites[position].name);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;	}

//	@NonNull
//	@Override
//	public View getView(int position, View convertView, ViewGroup parent)
//	{
//		View v = convertView;
//		if(v == null){
//			v = (LayoutInflater.from(context).inflate(R.layout.widget_activity_list_element, null));
//			RadioButton radioButton = (RadioButton)  v.findViewById(R.id.widget_sites_list_element_button);
//			radioButton.setText(sites[position].name);
//		}
//		RadioButton radioButton = (RadioButton)v.findViewById(R.id.widget_sites_list_element_button);
//		radioButton.setChecked(position == selectedPosition);
//		radioButton.setTag(position);
//		radioButton.setOnClickListener(new View.OnClickListener() {
//			@Override
//			public void onClick(View view) {
//				selectedPosition = (Integer)view.getTag();
//				notifyDataSetChanged();
//				if(context instanceof WidgetConfigActivity){
//					((WidgetConfigActivity)context).setSiteIdFromFragment(getSelectedItem().id);
//				}
//			}
//		});
//		return v;
//	}

}
