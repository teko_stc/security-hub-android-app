package biz.teko.td.SHUB_APP.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;

public class PopupActivity extends AppCompatActivity
{

	int totalCount = 0;
	private int classId;
	private int detectorId;
	private int reasonId;
	private String data;
	private int additionalCount;
	private ImageView eventImage;
	private TextView eventDescription, eventAdditional, textPlus;
	private Window window;

	@Override
	protected void onNewIntent(Intent intent)
	{
		super.onNewIntent(intent);
		getDataFromIntent(intent);
		updateViews();
		if(null!=window)
			window.addFlags(
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
						WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
						WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
						WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		window = getWindow();
		window.addFlags(
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
						WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
						WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
						WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		setContentView(R.layout.activity_popup);



		eventImage = (ImageView) findViewById(R.id.eventImage);
		eventDescription = (TextView) findViewById(R.id.eventDescription);
		eventAdditional = (TextView) findViewById(R.id.eventAdditional);
		textPlus = (TextView) findViewById(R.id.textPlus);

		Intent intent = getIntent();
		getDataFromIntent(intent);
		updateViews();

		ImageButton closeButton = (ImageButton) findViewById(R.id.button_popup_close);
		closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	private void updateViews()
	{
		if(null!= D3Service.d3XProtoConstEvent)
		{
			eventImage.setImageResource(getResources().getIdentifier(D3Service.d3XProtoConstEvent.getAffectIconBig(classId, detectorId, reasonId), null, getPackageName()));
		}
		eventDescription.setText(Html.fromHtml(data));
		if(additionalCount > 0)
		{
			totalCount+=additionalCount;
		}else{
			totalCount++;
		}
		if(totalCount > 1)
		{
			eventAdditional.setVisibility(View.VISIBLE);
			textPlus.setVisibility(View.VISIBLE);
			eventAdditional.setText(Integer.toString(totalCount - 1) + " " + getString(R.string.POPUP_NEW_MESSAGES));
		}
	}

	public void getDataFromIntent(Intent intent)
	{
		classId = intent.getIntExtra("eventClass", -1);
		detectorId = intent.getIntExtra("eventDetector", -1);
		reasonId = intent.getIntExtra("eventReason", -1);
		data = intent.getStringExtra("eventData");
		additionalCount = intent.getIntExtra("addCount", 0);
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		finish();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleHelper.onAttach(base));
	}
}