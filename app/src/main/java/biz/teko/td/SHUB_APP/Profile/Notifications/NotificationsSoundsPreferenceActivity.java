package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.LocaleHelper;

/**
 * Created by td13017 on 04.08.2017.
 */

public class NotificationsSoundsPreferenceActivity extends AppCompatActivity
{
	private Context context;
	private DBHelper dbHelper;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sounds_preferences);
		context = this;
		dbHelper = DBHelper.getInstance(context);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.PSA_TITLE);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		getFragmentManager().beginTransaction().replace(R.id.prefContainer, new NotificationsSoundsPreferenceFragment()).commit();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		((App)this.getApplication()).stopActivityTransitionTimer();
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(LocaleHelper.onAttach(base));
	}


}
