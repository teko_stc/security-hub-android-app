package biz.teko.td.SHUB_APP.D3DB;

import android.content.Context;
import android.database.Cursor;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Models.Pair;

public class MainBar
{
	public enum Type {
		EMPTY (0, R.string.BAR_EMPTY, R.drawable.ic_plus, 0),
		ARM (1, R.string.BAR_ARM, R.drawable.n_image_main_type_arm_list_selector,  R.drawable.ic_main_arm),
		CONTROL (2, R.string.BAR_CONTROL, R.drawable.n_image_main_type_control_list_selector, R.drawable.ic_main_control),
		DEVICE (3, R.string.BAR_DEVICE, R.drawable.n_image_main_type_device_list_selector, R.drawable.ic_main_device),
		CAMERA (4, R.string.BAR_CAMERA, R.drawable.n_image_main_type_camera_list_selector, R.drawable.ic_main_camera);

		private final int id;
		private final int name;
		private final int iconSmall;
		private int iconMain;

		private Type(int i, int name, int iconSmall, int iconMain){
			this.id = i;
			this.name = name;
			this.iconSmall = iconSmall;
			this.iconMain = iconMain;
		}

//		public boolean equalsName(String otherName) {
//			// (otherName == null) check is not needed because name.equals(null) returns false
//			return name.equals(otherName);
//		}

		public int getId(){return  this.id;};

		public String toString(Context context) {
			return context.getString(this.name);
		}

		public int getIconSmall(){
			return this.iconSmall;
		}

//		public int getIconSmallInvert(){
//			return this.iconSmallInvert;
//		}

		public int getIconMain(){
			return this.iconMain;
		}

	}

	public enum SubType{
		SITE(0, R.string.N_MAIN_TITLE_ALL_SITE, R.drawable.n_image_main_subtype_site_list_selector, R.drawable.ic_main_arm),
		DEVICE(1, R.string.CONTROLLER, R.drawable.n_image_main_subtype_controller_list_selector, R.drawable.ic_sh_main),
		GROUP(2, R.string.SECTION_GROUP, R.drawable.n_image_main_subtype_sgroup_list_selector, R.drawable.ic_section_group_main),
		SECTION(3, R.string.PARTITION, R.drawable.n_image_main_subtype_section_list_selector, R.drawable.ic_section_common_main);

		private final int id;
		private final int name;
		private final int iconSmall;
		private final int iconMain;

		SubType(int id, int name, int iconSmall, int iconMain)
		{
			this.id = id;
			this.name = name;
			this.iconSmall = iconSmall;
			this.iconMain = iconMain;
		}

		public int getId(){return  this.id;};

		public String toString(Context context) {
			return context.getString(this.name);
		}

		public int getIconSmall(){
			return this.iconSmall;
		}

		public int getIconMain(){
            return this.iconMain;
        }
    }

    public Type type;
    public SubType subtype;

    public int id, site_id, x, y;
    public String element_id;

    public MainBar(int id, int x, int y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }

    public MainBar() {
    }

    ;

    public MainBar(Cursor c) {
        if (null != c) {
            this.id = c.getInt(0);
            this.type = Type.valueOf(c.getString(1));
            this.subtype = null != c.getString(2) ? !c.getString(2).equals("") ? SubType.valueOf(c.getString(2)) : null : null;
            this.site_id = c.getInt(3);
            this.element_id = c.getString(4);
			this.x = c.getInt(5);
			this.y = c.getInt(6);
		}
	}

	public MainBar(int x, int y){
		this.x = x;
		this.y = y;
		this.type = Type.EMPTY;
    }

    public MainBar(int id, int site, int x, int y) {
        this.id = id;
        this.site_id = site;
        this.x = x;
        this.y = y;
    }

    public void setSite_id(int site_id) {
        this.site_id = site_id;
    }

    public MainBar(int id, Type type, int x, int y) {
        this.id = id;
        this.type = type;
        this.x = x;
        this.y = y;
    }

    public MainBar(int id, Type type, int site_id, int x, int y) {
        this.id = id;
		this.type = type;
		this.site_id = site_id;
		this.x = x;
		this.y = y;
	}



	public MainBar(int id, int site_id, Pair coord)
	{
		this.id = id;
		this.site_id = site_id;
		this.x = coord.x;
		this.y = coord.y;
	}
}
