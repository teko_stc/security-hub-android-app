package biz.teko.td.SHUB_APP.MainTabs.Fragments;

import static android.app.Activity.RESULT_OK;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import biz.teko.td.SHUB_APP.Activity.BaseInterfaceFragmentTemp;
import biz.teko.td.SHUB_APP.Activity.MainNavigation;
import biz.teko.td.SHUB_APP.Activity.MainNavigationActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.BuildConfig;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.NCameraDahuaActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.RTSP.NCameraRTSPActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.SectionGroup;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Devices_N.Activities.DeviceActivity;
import biz.teko.td.SHUB_APP.MainTabs.Activities.MainSetActivity;
import biz.teko.td.SHUB_APP.MainTabs.Activities.SiteSettingsActivity;
import biz.teko.td.SHUB_APP.MainTabs.Rounters.NotifProblemRouter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Relays_N.Activities.RelayActivity;
import biz.teko.td.SHUB_APP.Sections_N.Activities.SectionActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NStartTutorialView;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.WizardAllActivity;
import biz.teko.td.SHUB_APP.Zones_N.Activities.ZoneActivity;

public class MainFragment extends BaseInterfaceFragmentTemp
{
	private final MainNavigation activity;
	private View layout;
	private static final int DEF_POSITION = 1;
	private static final String STORY_FRAGMENT = "story_fragment";
	private boolean sharedMainGrid = PrefUtils.getInstance(getContext()).getSharedMainScreenMode();

	private DBHelper dbHelper;
	private UserInfo userInfo;
	private Drawer drawerResult;
	private SharedPreferences sp;
	private int from;

	private int site_id;

	private static Cursor activeCursor;
	private static Cursor repairCursor;
	private static Cursor activeLostCursor;
	private boolean bound;
	private D3Service myService;
	private ServiceConnection serviceConnection;
	private final int sending  = 0;
	private OnFragmentRefreshListener onFragmentRefreshListener;
	private PrefUtils prefUtils;
	private FrameLayout mainFrame;
	private LinearLayout newSiteFrame;
	private NActionButton newSiteButton;
	private NDialog progressDialog;
	private boolean wait;

	private Timer timer;
	private TimerTask timerTask;
	private final long MAX_WAIT_TIME_MS = 10000;
	private NStartTutorialView tutorial;
	private NMainStoriesFragment storiesFragment;
	private ClickableGridFrame mainGridFragment;

	private final BroadcastReceiver sitesUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			wait = false;
			bind();
		}
	};

	public MainFragment(){

		this.activity = (MainNavigationActivity) requireContext();
	}

	public MainFragment(MainNavigation activity) {
		this.activity = activity;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.activity_main_bottom, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		layout = getView();
		setup();
	}

	private void updateBottomHelper(){
		activity.setBottomInfoBehaviour(activity);
	}

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateBottomHelper();
		}
	};

	private final BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			activity.showCommandResultSnackbar(context, intent.getIntExtra("result", -1));
		}
	};

	private final BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							activity.showSnackBar(context, Func.handleResult(context,result));
						} else
						{
							activity.showSnackBar(context,  context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						activity.showSnackBar(context, context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};

	@Override
	public void nrAction(int id)
	{
		nSendMessageToServer(dbHelper.getDeviceById(id), Command.ARM, new JSONObject(), true);
	}

	public interface OnFragmentRefreshListener{
		void  onRefresh();
	}

	public void setOnFragmentRefreshListener(OnFragmentRefreshListener onFragmentRefreshListener){
		this.onFragmentRefreshListener = onFragmentRefreshListener;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		activity.getSwipeBackLayout().setEnableGesture(false);

		this.dbHelper = DBHelper.getInstance(activity);
		this.prefUtils = PrefUtils.getInstance(activity);
		this.userInfo = dbHelper.getUserInfo();
		this.sp = PreferenceManager.getDefaultSharedPreferences(activity);
		this.from = activity.getIntent().getIntExtra("FROM", -1);
		activity.setOnBackPressedCallback(this::onBackPressed);
	}

//	private boolean isNeedSHowStories() {
//		return Func.isRuLang(activity) && prefUtils.getStartStories() &&
//				Func.isInstallFromUpdate(activity) != 0 && storiesFragment == null;
//	}

//	private void setupStartStories() {
//		if (isNeedSHowStories()) {
//			if (tutorial != null) tutorial.setVisibility(View.GONE);
//			showStoryDialog();
//		}else{
//			disableStartStories();
//		}
//	}

//	private void showStoryDialog() {
//
//		NDialog nDialog= new NDialog(getContext(), R.layout.n_dialog_question_vertical_small);
//		nDialog.setCancelable(false);
//		nDialog.setTitle(getResources().getString(R.string.APP_UPDATED_STORY_START) + " " +
//				BuildConfig.VERSION_NAME +
//				getResources().getString(R.string.APP_UPDATED_STORY_END)
//		);
//		nDialog.setPositiveButton(getResources().getString(R.string.YES), () -> {
//			nDialog.dismiss();
//			showStoriesFragment(); });
//		nDialog.setNegativeButton(getResources().getString(R.string.SKIP), () -> {
//			nDialog.dismiss();
//			disableStartStories();
//			setupTutorial();
//		});
//		nDialog.show();
//	}

//    private void showStoriesFragment() {
//		storiesFragment = new NMainStoriesFragment();
//		storiesFragment.setListener(() -> {
//			closeStoryFragment(storiesFragment);
//			setupTutorial();
//		});
//		storiesFragment.show(getChildFragmentManager(), "stories");
////        getChildFragmentManager()
////                .beginTransaction()
////                .replace(R.id.work_frame, storiesFragment, STORY_FRAGMENT)
////                .commit();
//    }

//	private void closeStoryFragment(Fragment fragment) {
//		storiesFragment.dismiss();
////		getChildFragmentManager()
////				.beginTransaction()
////				.remove(fragment)
////				.commitNow();
//		disableStartStories();
//	}

//	private void disableStartStories() {
//		prefUtils.setStartStories(false);
//		if (tutorial != null) tutorial.setVisibility(View.VISIBLE);
//		else checkNotificationProblems();
//	}

	private void checkNotificationProblems()
	{
//		if(prefUtils.getNotificationProblem()) showNotificationProblemsDialog();
		new NotifProblemRouter(getChildFragmentManager(), activity).setup();
	}

	@Override
	public void onResume() {
		super.onResume();
		sharedMainGrid = PrefUtils.getInstance(getContext()).getSharedMainScreenMode();
		bind();
		bindD3();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_ALARM_ADD_UPD_RMV);
		activity.registerReceiver(updateReceiver, intentFilter);
		activity.registerReceiver(sitesUpdateReceiver, new IntentFilter(D3Service.BROADCAST_SITES));
		activity.registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		activity.registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));

		if (null != onFragmentRefreshListener) onFragmentRefreshListener.onRefresh();

		updateBottomHelper();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		activity.unregisterReceiver(updateReceiver);
		activity.unregisterReceiver(sitesUpdateReceiver);
		activity.unregisterReceiver(commandResultReceiver);
		activity.unregisterReceiver(commandSendReceiver);
		if(bound)
			activity.unbindService(serviceConnection);

	}

	private void setup()
	{
		newSiteFrame = layout.findViewById(R.id.nMainNewSiteFrame);
		newSiteButton = layout.findViewById(R.id.nButtonNewSite);
		mainFrame = layout.findViewById(R.id.nMainGridFrame);
	}

	private void bind()
	{
		site_id = PrefUtils.getInstance(activity).getCurrentSite();
		if(site_id == 0) {
			site_id = dbHelper.getFirstSite();
		}else{
			Site site = dbHelper.getSiteById(site_id);
			if(null==site) site_id = dbHelper.getFirstSite();// if site been just deleted
		}

		if(0!=site_id){
			setSiteMainFragment(site_id);
			if (isNeedShowTutorial()) {
				setupTutorial();
			}else{
				checkNotificationProblems();
			}
//			?? 22.12.2021 no more need??
//			setupStartStories();
		}else{
			if(null!=mainFrame) mainFrame.setVisibility(View.GONE);
			if(null!=newSiteFrame) newSiteFrame.setVisibility(View.VISIBLE);
			if(null!= newSiteButton) {
				newSiteButton.setOnButtonClickListener(action -> {
					Intent intent = new Intent(activity, WizardControllerSetActivity.class);
					intent.putExtra("from", 1);
					startActivity(intent);
				});
			}

		}
		if(!wait && null!=progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			if(null!=timer)timer.cancel();
		}
		updateBottomHelper();
	}

	private void setupTutorial() {
		tutorial = new NStartTutorialView(activity, activity, 1);
		tutorial.setNextActivityListener(() -> {
			activity.openNewDevices();
			activity.setFirstTutorial(true);
		});
		tutorial.setSkipListener(this::checkNotificationProblems);
	}

	private boolean isNeedShowTutorial() {
		boolean flag = tutorial == null && prefUtils.getStartTutorial() &&
				dbHelper.getAllZones().getCount() > 0;
		if (storiesFragment == null) {
			return flag;
		}
		return flag;
	}

	private void setSiteMainFragment(int site_id) {
		ClickableGridFrame mainGridFragment;
		if (!sharedMainGrid) {
			PrefUtils.getInstance(activity).setCurrentSite(site_id);
			mainGridFragment = MainGridFragment.newInstance(site_id);
		} else {
			mainGridFragment = SharedMainGridFragment.getInstance();
		}
		mainGridFragment.setClickListener(new MainGridFragment.OnClickListener() {
			@Override
			public void onNewClickListener() {
				startActivity(new Intent(activity, WizardAllActivity.class));
			}

			@Override
			public void onEmptySharedClickListener() {
				MainBar mainBar = dbHelper.getEmptyBar(site_id, true);
				Intent intent = new Intent(activity, MainSetActivity.class);
				intent.putExtra("id", mainBar.id);
				intent.putExtra("site", sharedMainGrid ? mainBar.site_id : site_id);
				intent.putExtra("x", mainBar.x);
				intent.putExtra("shared", true);
				intent.putExtra("y", mainBar.y);
				startActivity(intent);
			}

			@Override
			public void onSiteChooseClick(int site_id) {
				showSiteSelectFrame(activity, site_id);
			}

			@Override
			public void onViewSettings(int site_id) {
				Intent intent = new Intent(activity, SiteSettingsActivity.class);
				intent.putExtra("site", site_id);
				startActivityForResult(intent, Const.REQUEST_DELETE);
				activity.overridePendingTransition(R.animator.enter, R.animator.exit);
			}

			@Override
			public void onBarClick(int id, View view) {
//				Func.pushToast(context, mainBar.type.name() + " SHORT " + Integer.toString(mainBar.x) + Integer.toString(mainBar.y), (MainActivity) context);
				MainBar mainBar = dbHelper.getMainBarById(id);
				if (null != mainBar) {
					switch (mainBar.type) {
						case EMPTY:
							Intent intent = new Intent(activity, MainSetActivity.class);
							intent.putExtra("id", mainBar.id);
							intent.putExtra("site", sharedMainGrid ? mainBar.site_id : site_id);
							intent.putExtra("x", mainBar.x);
							intent.putExtra("y", mainBar.y);
							startActivity(intent);
							break;
						case ARM:
							showArmDialog(mainBar);
							break;
						case CONTROL:
							showControlDialog(mainBar);
							break;
						case DEVICE:
							openZoneActivity(mainBar);
							break;
						case CAMERA:
							openCameraActivity(mainBar);
							break;
						default:

							break;
					}
				}
			}

			@Override
			public void onBarLongClick(int id) {
				MainBar mainBar = dbHelper.getMainBarById(id);
				if (null != mainBar) {
					switch (mainBar.type) {
						case EMPTY:
							break;
						default:
							NDialog nDialog = new NDialog(activity, R.layout.n_dialog_question);
							nDialog.setTitle(getString(R.string.N_MAIN_DELETE_MESSAGE));
							nDialog.setOnActionClickListener(new NDialog.OnActionClickListener() {
								@Override
								public void onActionClick(int value, boolean b) {
									switch (value) {
										case NActionButton.VALUE_OK:
											if (dbHelper.deleteMainBar(mainBar)) {
												onFragmentRefreshListener.onRefresh();
											}
											nDialog.dismiss();
											break;
										case NActionButton.VALUE_CANCEL:
											nDialog.dismiss();
											break;
									}
								}
							});
							nDialog.show();
							break;
					}
				}
			}
		});
		getChildFragmentManager().beginTransaction().replace(R.id.nMainGridFrame, mainGridFragment.getFragment()).addToBackStack("0").commit();
		this.mainGridFragment = mainGridFragment;
	}


	public ClickableGridFrame getFragment() {
		return mainGridFragment;
	}

	private void openCameraActivity(MainBar mainBar)
	{
		try{
			JSONObject jsonObject = new JSONObject(mainBar.element_id);
			switch (Camera.CType.valueOf(jsonObject.getString("type"))){
				case IV:
					Intent intentIV = new Intent(activity, NCameraActivity.class);
					intentIV.putExtra("cameraId", jsonObject.getString("id"));
					startActivity(intentIV);
					break;
				case RTSP:
					Intent intentRTSP = new Intent(activity, NCameraRTSPActivity.class);
					intentRTSP.putExtra("cameraId", jsonObject.getString("id"));
					startActivity(intentRTSP);
					break;
				case DAHUA:
					Intent intentDahua = new Intent(activity, NCameraDahuaActivity.class);
					intentDahua.putExtra("cameraId", jsonObject.getString("id"));
					startActivity(intentDahua);
					break;
			}
		}catch (JSONException e){
			e.printStackTrace();
		}

	}

	private void openRelayActivity(MainBar mainBar, Zone zone)
	{
		Intent intent = new Intent(activity, RelayActivity.class);
		intent.putExtra("title", zone.name);
		intent.putExtra("site", mainBar.site_id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		startActivity(intent);
	}

	private void openZoneActivity(MainBar mainBar)
	{
		Intent intentDevice = new Intent(activity, ZoneActivity.class);
		try
		{

			JSONObject idjson = new JSONObject(mainBar.element_id);
			Zone zone = dbHelper.getZoneById(idjson.getInt("device"), idjson.getInt("section"), idjson.getInt("zone"));
			if(null!=zone) {
				intentDevice.putExtra("bar_title", zone.name);
				intentDevice.putExtra("site", dbHelper.getSiteByDeviceId(zone.device_id).id);
				intentDevice.putExtra("device", zone.device_id);
				intentDevice.putExtra("section", zone.section_id);
				intentDevice.putExtra("zone", zone.id);
				//intentDevice.putExtra("bar_title", zone.name);
				//intentDevice.putExtra("site", mainBar.site_id);
				//intentDevice.putExtra("zone", zone.id);
				//intentDevice.putExtra("section", zone.section_id);
				//intentDevice.putExtra("device", zone.device_id);
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		startActivity(intentDevice);
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation(activity,
				new Pair<>(view.findViewById(R.id.nTextTitle), Const.TITLE_TRANSITION_NAME));
	}

	private void showControlDialog(MainBar mainBar)
	{
		String title = mainBar.type.toString();
		try
		{
			JSONObject idjson = new JSONObject(mainBar.element_id);
			Zone zone = dbHelper.getZoneById(idjson.getInt("device"), idjson.getInt("section"), idjson.getInt("zone"));
			switch (zone.detector){
				case Const.DETECTOR_MANUAL_RELAY:
					if (null != zone) {
						title = zone.name;
					}
					NDialog nDialog = new NDialog(activity, R.layout.n_dialog_on_off);
					if (sharedMainGrid) {
						TextView siteTitle = nDialog.findViewById(R.id.nDialogSiteTitle);
						siteTitle.setVisibility(View.VISIBLE);

						siteTitle.setText(dbHelper.getSiteByDeviceId(zone.device_id).name);
					}
					nDialog.setTitle(title);
					nDialog.setSubTitle(getString(R.string.N_MAIN_SUBTITLE_REMOTE_DEVICE));
					nDialog.setOnActionClickListener((value, b) -> {
						switch (value) {
							case NActionButton.VALUE_ON:
								JSONObject commandAddress = new JSONObject();
								try {
									commandAddress.put("section", zone.section_id);
									commandAddress.put("zone", zone.id);
									commandAddress.put("state", 1);
									nSendMessageToServer(zone, Command.SWITCH, commandAddress, true);
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
								nDialog.dismiss();
								break;
							case NActionButton.VALUE_OFF:
								commandAddress = new JSONObject();
								try
								{
									commandAddress.put("section", zone.section_id);
									commandAddress.put("zone", zone.id);
									commandAddress.put("state", 0);
									nSendMessageToServer(zone, Command.SWITCH, commandAddress, true);
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
								nDialog.dismiss();
								break;
							case NActionButton.VALUE_INFO:
								openRelayActivity(mainBar, zone);
								nDialog.dismiss();
								break;
						}
					});
					nDialog.show();
					break;
				default:
					openRelayActivity(mainBar, zone);
					break;
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	//dialog with actoin after bar onclick
	private void showArmDialog(MainBar mainBar)
	{
		if(null!=mainBar.subtype){
			String title = mainBar.type.toString();
			String subTitle = null;
			Intent intent = null;
			boolean infoLinkVisiblity = true;
			switch (mainBar.subtype){
				case SITE:
					title = getString(R.string.N_MAIN_TITLE_FULL_SITE);
					infoLinkVisiblity = false;
					JSONObject commandAddress = new JSONObject();
					showNSiteDialog(mainBar.site_id, dbHelper.getSiteNameById(mainBar.site_id), title, subTitle, intent, R.layout.n_dialog_arm, commandAddress, infoLinkVisiblity);
					break;
				case DEVICE:
					Device device = dbHelper.getDeviceById(Integer.valueOf(mainBar.element_id));
					if (null != device) {
						title = device.getName();
						subTitle = getString(R.string.N_MAIN_TITLE_CONTROLLER);
						intent = new Intent(activity, DeviceActivity.class);
						intent.putExtra("bar_title", title);
						intent.putExtra("site", mainBar.site_id);
						intent.putExtra("device", device.id);
						commandAddress = new JSONObject();
						showNDeviceDialog(device, dbHelper.getSiteByDeviceId(device.id).name,
								title, subTitle, intent, R.layout.n_dialog_arm, commandAddress, infoLinkVisiblity);
					}

					break;
				case GROUP:
					SectionGroup sectionGroup = dbHelper.getSectionGroup(Integer.valueOf(mainBar.element_id));
					if (null != sectionGroup) {
						title = sectionGroup.name;
						subTitle = activity.getResources().getString(R.string.SECTION_GROUP);
					}
					infoLinkVisiblity = false;
					commandAddress = new JSONObject();
					if (sectionGroup != null && sectionGroup.sections.length > 0)
						showNSGroupDialog(sectionGroup,
								dbHelper.getSiteByDeviceId(sectionGroup.sections[0].device_id).name,
								title, subTitle, intent, R.layout.n_dialog_arm, commandAddress, infoLinkVisiblity);
					break;
				case SECTION:
					try {
						JSONObject idjson = new JSONObject(mainBar.element_id);
						if (null != idjson) {
							int device_id = idjson.getInt("device");
							int section_id = idjson.getInt("section");
							Section section = dbHelper.getDeviceSectionById(device_id, section_id);
							if (null != section) {
								intent = new Intent(activity, SectionActivity.class);
								intent.putExtra("site", mainBar.site_id);
								intent.putExtra("device", section.device_id);
								intent.putExtra("section", section.id);

								switch (section.detector) {
									case Const.SECTIONTYPE_GUARD:
										showNSectionDialog(section,
												dbHelper.getSiteByDeviceId(section.device_id).name,
												//dbHelper.getSiteNameById(mainBar.site_id),
												intent, R.layout.n_dialog_arm, infoLinkVisiblity);
										break;
									case Const.SECTIONTYPE_TECH_CONTROL:
										showNSectionDialog(section,
												dbHelper.getSiteByDeviceId(section.device_id).name,
												intent, R.layout.n_dialog_control, infoLinkVisiblity);
										break;
									default:
										startActivity(intent);
										break;
								}


							}

						}
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					break;
			}
		}
	}

	private void showNSiteDialog(int site, String siteName, String title, String subTitle, Intent intent, int layout, JSONObject commandAddress, boolean v) {
		NDialog nDialog = new NDialog(activity, layout);
		nDialog.setTitle(title);
		nDialog.setSubTitle(subTitle);
		if (sharedMainGrid) {
			TextView siteTitle = nDialog.findViewById(R.id.nDialogSiteTitle);
			siteTitle.setVisibility(View.VISIBLE);
			siteTitle.setText(siteName);
		}
		if (!v) nDialog.setLinkVisiblity(View.GONE);
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value) {
				case NActionButton.VALUE_ARM:
					Device[] devices = dbHelper.getAllDevicesForSite(site);
					if (null != devices && devices.length > 0) {
						for (Device device : devices) {
							nSendMessageToServer(device, Command.ARM, commandAddress, true);
						}
					}
					nDialog.dismiss();
					break;
				case NActionButton.VALUE_DISARM:
					devices = dbHelper.getAllDevicesForSite(site);
					if(null!=devices && devices.length > 0)
					{
						for(Device device:devices)
						{
							nSendMessageToServer(device, Command.DISARM, commandAddress, true);
						}
					}
					nDialog.dismiss();
					break;
				case NActionButton.VALUE_INFO:
					startActivity(intent);
					nDialog.dismiss();
					break;
			}
		});
		nDialog.show();
	}

	private void showNDeviceDialog(Device device, String siteName, String title, String subTitle, Intent intent, int layout, JSONObject commandAddress, boolean v) {
		NDialog nDialog = new NDialog(activity, layout);
		nDialog.setTitle(title);
		nDialog.setSubTitle(subTitle);
		if (sharedMainGrid) {
			TextView siteTitle = nDialog.findViewById(R.id.nDialogSiteTitle);
			siteTitle.setVisibility(View.VISIBLE);

			siteTitle.setText(siteName);
		}
		if (!v) nDialog.setLinkVisiblity(View.GONE);
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value) {
				case NActionButton.VALUE_ARM:
					nSendMessageToServer(device, Command.ARM, commandAddress, true);
					break;
				case NActionButton.VALUE_DISARM:
					nSendMessageToServer(device, Command.DISARM, commandAddress, true);
					break;
				case NActionButton.VALUE_INFO:
					startActivity(intent);
					break;
			}
			nDialog.dismiss();
		});
		nDialog.show();
	}

	private void showNSectionDialog(Section section, String siteName, Intent intent, int layout, boolean v) {
		NDialog nDialog = new NDialog(activity, layout);
		nDialog.setTitle(section.name);
		nDialog.setSubTitle(getString(R.string.N_MAIN_SUBTITLE_SECTION));

		if (sharedMainGrid) {
			TextView siteTitle = nDialog.findViewById(R.id.nDialogSiteTitle);
			siteTitle.setVisibility(View.VISIBLE);
			siteTitle.setText(siteName);
		}
		if (!v) nDialog.setLinkVisiblity(View.GONE);
		JSONObject commandAddress = new JSONObject();
		try {
			commandAddress.put("section", section.id);
			commandAddress.put("zone", 0);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_ARM:
						nSendMessageToServer(section,Command.ARM, commandAddress, true);
						break;
					case NActionButton.VALUE_DISARM:
						nSendMessageToServer(section,Command.DISARM, commandAddress, true);
						break;
					case NActionButton.VALUE_INFO:
						startActivity(intent);
						break;
				}
				nDialog.dismiss();
			}
		});
		nDialog.show();
	}

	private void showNSGroupDialog(SectionGroup sectionGroup, String siteName, String title, String subTitle, Intent intent, int layout, JSONObject commandAddress, boolean v) {
		NDialog nDialog = new NDialog(activity, layout);
		nDialog.setTitle(title);
		nDialog.setSubTitle(subTitle);
		if (!v) nDialog.setLinkVisiblity(View.GONE);
		if (sharedMainGrid) {
			TextView siteTitle = nDialog.findViewById(R.id.nDialogSiteTitle);
			siteTitle.setVisibility(View.VISIBLE);

			siteTitle.setText(siteName);
		}
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener() {
			@Override
			public void onActionClick(int value, boolean b) {
				switch (value) {
					case NActionButton.VALUE_ARM:
						if (null != sectionGroup.sections && sectionGroup.sections.length > 0) {
							for (Section section : sectionGroup.sections) {
								JSONObject ncommandAddress = new JSONObject();
								try
								{
									ncommandAddress.put("section",section.id);
									ncommandAddress.put("zone", 0);
									nSendMessageToServer(section,Command.ARM, ncommandAddress, true);
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
							}
						}
						nDialog.dismiss();
						break;
					case NActionButton.VALUE_DISARM:
						if(null!=sectionGroup.sections && sectionGroup.sections.length > 0)
						{
							for(Section section:sectionGroup.sections){
								JSONObject ncommandAddress = new JSONObject();
								try
								{
									ncommandAddress.put("section",section.id);
									ncommandAddress.put("zone", 0);
									nSendMessageToServer(section,Command.DISARM, ncommandAddress, true);
								} catch (JSONException e)
								{
									e.printStackTrace();
								}
							}
						}
						nDialog.dismiss();
						break;
					case NActionButton.VALUE_INFO:
						startActivity(intent);
						nDialog.dismiss();
						break;
				}
			}
		});
		nDialog.show();
	}

//	@Override
//	public void onWindowFocusChanged(boolean hasFocus)
//	{
//		if(hasFocus){
//
//			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//
//			/*SHOW ALARM LIST*/
//			if (from == D3Service.OPEN_FROM_STARTUP_ACTIVITY_AFTER_LOGIN || from == D3Service.OPEN_HISTORY_FROM_STATUS_BAR)
//			{
////				TODO OPEN
////				/*Show rating dialog*/
////				AppRater.appLaunched(context);
////				/*Show domain lang dialog*/
////				checkLocales(sp);
////				/*Show new events dialog*/
////				checkStates(context, sp);
//
//				from = -1;
//			}
////			TODO ADD NEW FEATURES AND OPEN IN NEW RELEASE
////			UPDATE CHECK OPEN WITH NEW UPDATE
////			else{
////				/*Show updated features dialog*/
////				if((!context.getPackageName().equals("public.shub")) || PreferenceManager.getDefaultSharedPreferences(context).getString("prof_language_value", "ru").equals("ru")) {
////					checkUpdates(context, sp);
////				}
////			}
//		}
//	}

//	public void checkUpdates(Context context, SharedPreferences sp){
//		long updateTime = Func.isInstallFromUpdate(context);
//		if (0 != updateTime)
//		{
//			if (updateTime != sp.getLong(Func.Prefs.PREF_APP_UPDATE_TIME, 0)){
//				if (updateTime > sp.getLong(Func.Prefs.PREF_APP_UPDATE_TIME, 0) + 48*3600*1000)
//				{
//					showUpdatesList(context);
//				}
//				sp.edit().putLong(Func.Prefs.PREF_APP_UPDATE_TIME, updateTime).apply();
//			}
//		}
//	}

	private void showSiteSelectFrame(final Context context, int site_id){

		SiteSelectFragment siteSelectFragment = SiteSelectFragment.getInstance(site_id);
		siteSelectFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
		siteSelectFragment.setListener(new SiteSelectFragment.OnClickListener()
		{
			@Override
			public void callback(SiteSelectFragment.Result result, int site_id)
			{
				switch (result){
					case CHANGE:
						setSiteMainFragment(site_id);
//						Func.pushToast(context, Integer.toString(site_id),(MainActivity)context);
						break;
					default:
						break;

				}
			}
		});

		siteSelectFragment.show(getChildFragmentManager(), "tagSetSite");
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
				//setStatusConnected();
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}


	private void bindD3(){
		Intent intent = new Intent(activity, D3Service.class);
		serviceConnection = getServiceConnection();
		activity.bindService(intent, serviceConnection, 0);
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(activity, activity.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	public boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), activity, command, sending);
	}


	public void onBackPressed()
	{
//		if (getChildFragmentManager().findFragmentByTag(STORY_FRAGMENT) != null) {
//			closeStoryFragment(storiesFragment);
//		} else
		if (tutorial != null && tutorial.getVisibility() == View.VISIBLE) {
			showFinishTutorialDialog();
		} else {
			showExitDialog();
		}
	}
	private void showFinishTutorialDialog() {
		NDialog nDialog = new NDialog(activity, R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.TUTORIAL_FINISH_QUESTION));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value) {
				case NActionButton.VALUE_OK:
					finishTutorial();
					nDialog.dismiss();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}

		});
		nDialog.show();
	}

	private void finishTutorial() {
		tutorial.finishTutorial();
		tutorial = null;
	}

	private void showExitDialog() {
		NDialog nDialog = new NDialog(activity, R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.N_LOGOUT_Q));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value){
				case NActionButton.VALUE_OK:
					//finishAffinity();
					activity.finishAffinity();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}
		});
		nDialog.show();
	}

	//TODO: test onActivity result
	@Override
	public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				wait = true;
				progressDialog = new NDialog(activity, R.layout.n_dialog_progress);
				progressDialog.setTitle(null);
				progressDialog.show();
				timer = new Timer();
				timerTask = new TimerTask()
				{
					public void run()
					{
						wait = false;
						if(null!=progressDialog)progressDialog.dismiss();
					}
				};
				timer.schedule(timerTask, MAX_WAIT_TIME_MS);
			}
		}
	}
}
