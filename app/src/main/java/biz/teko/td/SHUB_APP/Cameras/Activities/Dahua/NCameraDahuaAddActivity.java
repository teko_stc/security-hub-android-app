package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.company.NetSDK.FinalVar;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Timer;
import java.util.TimerTask;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaCameraSearchModule;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaInitDevAccountModule;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NDahuaP2PLoginModule;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK.NetSDKLib;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.databinding.NActivityDahuaAddBinding;

public class NCameraDahuaAddActivity extends BaseActivity
{
	public static final int NET_USER_FLASEPWD_TRYTIME = FinalVar.NET_USER_FLASEPWD_TRYTIME;
	public static final int NET_LOGIN_ERROR_PASSWORD = FinalVar.NET_LOGIN_ERROR_PASSWORD;
	public static final int NET_LOGIN_ERROR_USER = FinalVar.NET_LOGIN_ERROR_USER;
	public static final int NET_LOGIN_ERROR_TIMEOUT = FinalVar.NET_LOGIN_ERROR_TIMEOUT;
	public static final int NET_LOGIN_ERROR_RELOGGIN = FinalVar.NET_LOGIN_ERROR_RELOGGIN;
	public static final int NET_LOGIN_ERROR_LOCKED = FinalVar.NET_LOGIN_ERROR_LOCKED;
	public static final int NET_LOGIN_ERROR_BLACKLIST = FinalVar.NET_LOGIN_ERROR_BLACKLIST;
	public static final int NET_LOGIN_ERROR_BUSY = FinalVar.NET_LOGIN_ERROR_BUSY;
	public static final int NET_LOGIN_ERROR_CONNECT = FinalVar.NET_LOGIN_ERROR_CONNECT;
	public static final int NET_LOGIN_ERROR_NETWORK = FinalVar.NET_LOGIN_ERROR_NETWORK;

	private static final long MAX_WAIT_TIME_MS = 15000;
	private static final String FIND_DAHUA_CAMERA = "Get dahua camera broadcast";
	private static final String SERVER = "www.easy4ip.com";
	private static final String SERVER_PORT = "9116";
	private static final String DEVICE_PORT = "37777";

	private static final int ERROR_TIMEOUT = -2;
	private static final int SUCCESS_RESULT = 1;
	private static final int ERROR_NO_INITIALIZATION = -3;

	private NActivityDahuaAddBinding binding;
	private Context context;
	private boolean toCapture = false;
	private boolean search = false;
	private Application app;

	private NDahuaCameraSearchModule nDahuaCameraSearchModule;
	private NDahuaInitDevAccountModule nDahuaInitDevAccountModule;
	private NDahuaP2PLoginModule p2pLoginModule;

	private String sn;
	private String name;
	private String login;
	private String pass;
	private Camera camera;

	private NDialog progressDialog;
	private Timer timer;
	private TimerTask timerTask;
	private boolean wait;

	private BroadcastReceiver dahuaCameraSearchReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(wait){
				stopWaiting();
				if(SUCCESS_RESULT == intent.getIntExtra("result", 0)){
					//addCamera
					p2pLogin();
				}else{
					showFailed(result);
				}
			}else{
				stopWaiting();
				showFailed(result);
			}
		}
	};
	private DBHelper dbHelper;

	private void showFailed(int result)
	{
		switch (result){
			case ERROR_TIMEOUT:
				Func.nShowMessage(context, getString(R.string.N_DAHUA_ERROR_TIMEOUT));
				break;
			case ERROR_NO_INITIALIZATION:
				Func.nShowMessage(context, getString(R.string.N_DAHUA_ERROR_NO_INIT));
				break;
			default:
				Func.nShowMessage(context, getString(R.string.N_DAHUA_ERROR_CANT_CONNECT));
				break;
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_dahua_add);

		context = this;
		app = getApplication();
		dbHelper = DBHelper.getInstance(context);

		try
		{
			NetSDKLib.getInstance().init();
		}catch (Exception e){
			Func.log_e(Const.LOG_TAG_DAHUA, e.toString());
		}

		binding = NActivityDahuaAddBinding.inflate(getLayoutInflater());
		setContentView(binding.getRoot());

		nDahuaCameraSearchModule = new NDahuaCameraSearchModule(context);
		nDahuaInitDevAccountModule = new NDahuaInitDevAccountModule();

		bind();

	}

	private void bind()
	{
		binding.toolbar.setOnBackClickListener(this::onBackPressed);
		binding.buttonQR.setOnClickListener(v -> scan());
		binding.nDahuaAddNextButton.setOnClickListener(v -> {
			if(validate())
			{
				setCamera();
				next();
			}else{
				Func.nShowMessage(context, getString(R.string.N_DAHUA_ADD_EMPTY_FIELDS_ERROR));
			}
		});
	}

	private void setCamera()
	{
		camera = new Camera(getSN(), getName(), getLogin(), getPass());
	}

	private void next()
	{
		startWaiting();
		startDahuaSearch();
	}

	private void startWaiting(){
		progressDialog = new NDialog(context,R.layout.n_dialog_progress);
		progressDialog.setCancelable(false);
		progressDialog.setTitle(getString(R.string.N_DAHUA_LOCAL_SEARCH));
		progressDialog.show();

		wait = true;
		timer = new Timer();
		timer = new Timer();
		timer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				if(wait)
				{
					wait = false;
					Intent intent = new Intent(FIND_DAHUA_CAMERA);
					intent.putExtra("result", ERROR_TIMEOUT);
					sendBroadcast(intent);
				}
			}
		}, MAX_WAIT_TIME_MS);
	}

	private  void stopWaiting(){
		if(wait) wait = !wait;
		if (null != timer) timer.cancel();
		progressDialog.dismiss();
	}

	private void startDahuaSearch()
	{
		if(checkConnection()){
			//search device in local
			search = true;
			nDahuaCameraSearchModule.startSearchDevices(device_net_info_ex -> {
				String s = D3Service.decodeUTF8(device_net_info_ex.szSerialNo).replace(" ","");
				s = s.trim();
				if(null!=s && s.equals(sn)){
					//					stopDahuaSearch();
					//initdev
					if(search)
					{
						search = false;
						boolean res = false;
						int initRes = nDahuaInitDevAccountModule.initDevAccount(device_net_info_ex, login, pass, "");
						switch (initRes)
						{
							case 1:
								res = true;
								break;
							case FinalVar.NET_ERROR_DEVICE_ALREADY_INIT:
								//								Func.pushToast(context, "Already Init");
								res = true;
								break;
							default:
								res = nDahuaInitDevAccountModule.initDevAccountByIP(device_net_info_ex, login, pass, "");
								break;
						}
						if (res)
						{
							Intent intent = new Intent(FIND_DAHUA_CAMERA);
							intent.putExtra("result", SUCCESS_RESULT);
							sendBroadcast(intent);
						} else
						{
							Intent intent = new Intent(FIND_DAHUA_CAMERA);
							intent.putExtra("result", ERROR_NO_INITIALIZATION);
							sendBroadcast(intent);
						}
					}
				};
			});
		}else{
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
			nDialog.setPositiveButton(getString(R.string.N_DAHUA_GO_TO_WIFI), () -> {
				startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));
				nDialog.dismiss();
			});
		}
		return ;
	}

	private void p2pLogin()
	{
		p2pLoginModule = new NDahuaP2PLoginModule();
		new P2PLoginTask().execute();
	}

	private class P2PLoginTask extends AsyncTask<String, Integer, Boolean>
	{
		@Override
		protected void onPreExecute(){
			super.onPreExecute();
			progressDialog = new NDialog(context,R.layout.n_dialog_progress);
			progressDialog.setCancelable(false);
			progressDialog.setTitle(getString(R.string.N_DAHUA_AUTHORIZATION));
			progressDialog.show();
		}

		@Override
		protected Boolean doInBackground(String... params) {
			boolean result =  false;
			if(p2pLoginModule.startP2pService(sn, login, pass)) {
				result = p2pLoginModule.login(login, pass);
			}
			return result;
		}

		@Override
		protected void onPostExecute(Boolean result){
			progressDialog.dismiss();
			if(!p2pLoginModule.isServiceStarted()) {
				Func.nShowMessage(context, getString(R.string.N_DAHUA_CANT_CONNECT_DAHUA_SERVER));
//				ToolKits.showMessage(P2PLoginActivity.this, res.getString(R.string.failed_start_server));
			} else {
				if (result) {
					NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
					nDialog.setTitle(getString(R.string.N_DAHUA_ADD_SUCCESS));
					nDialog.setOnActionClickListener((value, b) -> {
						onBackPressed();
					});
					nDialog.show();

//					NET_DEVICEINFO_Ex info = p2pLoginModule.getDeviceInfo();
					dbHelper.addDahuaCamera(camera);
					p2pLoginModule.logout();
					p2pLoginModule.stopP2pService();

//					app.setLoginHandle(p2pLoginModule.getLoginHan dle());
//					app.setDeviceInfo(p2pLoginModule.getDeviceInfo());
//					app.setDevName(p2pLoginModule.getDeviceName());

//					startActivity(new Intent(P2PLoginActivity.this, FunctionListActivity.class));
				} else {
					Func.nShowMessage(context, getErrorCode(getResources(), p2pLoginModule.errorCode()));
				}
			}
		}
	}

	private boolean checkConnection()
	{
		ConnectivityManager connection = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		return connection.getActiveNetworkInfo() != null && connection.getActiveNetworkInfo().isConnected();
	}

	private boolean validate()
	{
		return (null!=getSN()
				&& null!=getName()
				&& null!=getLogin()
				&& null!=getPass());
	}

	private String getSN(){
		if(null!=binding.dahuaSNTEditText.getText()){
			sn = binding.dahuaSNTEditText.getText().toString();
			if(sn.length()>0 && !sn.equals("")){
				return sn;
			}
		}
		return null;
	}

	private String getPass()
	{

		if(null!=binding.dahuaPasswordEditText.getText()){
			pass = binding.dahuaPasswordEditText.getText().toString();
			if(pass.length() > 0 && !pass.equals("")) {
				pass = pass.trim();
				return  pass;
			}
		}
		return null;
	}

	private String getLogin()
	{
		if(null!=binding.dahuaLoginEditText.getText()){
			login = binding.dahuaLoginEditText.getText().toString();
			if(login.length() > 0 && !login.equals("")) {
				login=login.trim();
				return login;
			}
		}
		return null;
	}

	private String getName()
	{

		if(null!= binding.dahuaNameEditText.getText()){
			name = binding.dahuaNameEditText.getText().toString();
			if(name.length() > 0 && !name.equals(""))
				name = name.trim();
				return name;
		}
		return null;
	}



	private void stopDahuaSearch()
	{
		search = false;
		nDahuaCameraSearchModule.stopSearchDevices();
	}

	private void scan()
	{
		if(this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
		{
			toCapture = true;
			IntentIntegrator integrator = new IntentIntegrator(this);
			integrator.setOrientationLocked(false);
			integrator.setBeepEnabled(false);
			integrator.initiateScan(Const.BARCODE_CODES);
		}else{
			Func.nShowMessage(context, getString(R.string.ERROR_CANT_SCAN_NO_CAMERA_HARDWARE));
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(dahuaCameraSearchReceiver);
		if(toCapture){
			((App)this.getApplication()).stopActivityTransitionTimer();
		}
		toCapture = false;
		if(null!=p2pLoginModule && p2pLoginModule.isServiceStarted()) {
			p2pLoginModule.logout();
			p2pLoginModule.stopP2pService();
		}

	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		if (search) stopDahuaSearch();
		NetSDKLib.getInstance().cleanup();
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(dahuaCameraSearchReceiver, new IntentFilter(FIND_DAHUA_CAMERA));
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);
		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanResult != null) {
			String s = scanResult.getContents();
			if(null!=s){
				switch(scanResult.getFormatName()){
					case "QR_CODE":
						if(null!=s) {
							binding.dahuaSNTEditText.setText(s.substring(s.indexOf(":")+1, s.indexOf(",")));
						}
						break;
					case "CODE_128":
						if(null!=s) {
							binding.dahuaSNTEditText.setText(s);
						}
						break;
					default:
						Func.pushToast(context, getString(R.string.N_DAHUA_SCAN_READ_ERROR));
						break;
				}
			}
		}
	}

	public static String getErrorCode(Resources res, int errorCode) {
		switch(errorCode) {
			case NET_USER_FLASEPWD_TRYTIME:
				return res.getString(R.string.NET_USER_FLASEPWD_TRYTIME);
			case NET_LOGIN_ERROR_PASSWORD:
				return res.getString(R.string.NET_LOGIN_ERROR_PASSWORD);
			case NET_LOGIN_ERROR_USER:
				return res.getString(R.string.NET_LOGIN_ERROR_USER);
			case NET_LOGIN_ERROR_TIMEOUT:
				return res.getString(R.string.NET_LOGIN_ERROR_TIMEOUT);
			case NET_LOGIN_ERROR_RELOGGIN:
				return res.getString(R.string.NET_LOGIN_ERROR_RELOGGIN);
			case NET_LOGIN_ERROR_LOCKED:
				return res.getString(R.string.NET_LOGIN_ERROR_LOCKED);
			case NET_LOGIN_ERROR_BLACKLIST:
				return res.getString(R.string.NET_LOGIN_ERROR_BLACKLIST);
			case NET_LOGIN_ERROR_BUSY:
				return res.getString(R.string.NET_LOGIN_ERROR_BUSY);
			case NET_LOGIN_ERROR_CONNECT:
				return res.getString(R.string.NET_LOGIN_ERROR_CONNECT);
			case NET_LOGIN_ERROR_NETWORK:
				return res.getString(R.string.NET_LOGIN_ERROR_NETWORK);
			default:
				return res.getString(R.string.ERROR);
		}
	}

}
