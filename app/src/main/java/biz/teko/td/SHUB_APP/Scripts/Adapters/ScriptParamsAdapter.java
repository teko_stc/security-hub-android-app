package biz.teko.td.SHUB_APP.Scripts.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Param;

public class ScriptParamsAdapter extends ArrayAdapter<Param>
{
	private final Context context;
	private final LinkedList<Param> params;
	private final DBHelper dbHelper;

	public ScriptParamsAdapter(Context context, LinkedList<Param> params )
	{
		super(context, R.layout.card_script_param, params);
		this.context = context;
		this.params = params;
		this.dbHelper = DBHelper.getInstance(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		Param param = params.get(position);
		if(null!=param.type)
		{
			switch (param.type)
			{
				case "int":
					if (v == null)
					{
						LayoutInflater inflater = ((Activity) context).getLayoutInflater();
						v = inflater.inflate(R.layout.card_script_param_int, null, true);
					}
					break;
				case "sections":
					if (v == null)
					{
						LayoutInflater inflater = ((Activity) context).getLayoutInflater();
						v = inflater.inflate(R.layout.card_script_param_sections, null, true);
					}
					break;
				default:
					if (v == null)
					{
						LayoutInflater inflater = ((Activity) context).getLayoutInflater();
						v = inflater.inflate(R.layout.card_script_param, null, true);
					}
					break;
			}
		}else{
			if (v == null)
			{
				LayoutInflater inflater = ((Activity) context).getLayoutInflater();
				v = inflater.inflate(R.layout.card_script_param, null, true);
			}
		}
		v.setEnabled(false);
		v.setOnClickListener(null);
		return v;	}
}
