package biz.teko.td.SHUB_APP.Widgets.Adapters.ArrayAdapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Widgets.Classes.ColorX;

/**
 * Created by td13017 on 26.05.2017.
 */

public class WidgetActivityColorsAdapter extends ArrayAdapter<ColorX>
{
	private final Context context;
	private final ColorX[] colors;
	private int selectedPosition = 0;
	boolean selected = true;

	public WidgetActivityColorsAdapter(Context context, int resource, ColorX[] colors)
	{
		super(context, resource, colors);
		this.context = context;
		this.colors = colors;
	}
	//
	//	public int getCount(){
	//		return sites.length;
	//	}
	//
	public ColorX getSelectedItem(){
		if(selected)
		{
			return colors[selectedPosition];
		}else{
			return null;
		}
	}
	//
	public long getSelectedItemId(int position){
		if(selected)
		{
			return selectedPosition;
		}else{
			return -1;
		}
	}

	public ColorX getItem(int position){
		return colors[position];
	}

	public long getItemId(int position){
		return position;
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandColorLight));
		label.setText(colors[position].color);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		label.setPadding(0, Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandColorLight));
		label.setText(colors[position].color);
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;	}
}
