package biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.SDK;

import android.util.Log;

import com.mm.android.dhproxy.client.DHProxyClient;

import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NDahuaP2PClient {
	private final static String TAG = "P2pClient";
	private DHProxyClient p2pClient;
	private int mLocalPort; /// the port used for p2p service
	private boolean mServiceStopped = false;

	public NDahuaP2PClient() {
		p2pClient = new DHProxyClient();
		mLocalPort = 0;
		mServiceStopped = false;
	}

	public int getP2pPort() {
		return this.mLocalPort;
	}

	public synchronized boolean stopService() {
		Log.d(TAG, "stopService");

		if (mServiceStopped) {
			return true;
		}

		if (mLocalPort > 0) {
			if (0 != p2pClient.delPort(mLocalPort)) {
				Log.d(TAG, "delPort " + mLocalPort);
			}
			mLocalPort = 0;
		}

		if (0 != p2pClient.exit()) {
			Log.d(TAG, "exit ");
		}

		mServiceStopped = true;
		return true;
	}

	public boolean startService(String svrAddress, String svrPort, String username,String svrKey,
								String deviceSn, String devicePort, String p2pDeviceUsername, String p2pDevicePassword) {
		Func.log_d(Const.LOG_TAG_DAHUA, "Start Service --> Begin.");
		String strClientType = "NetsdkDemo";
		if (!p2pClient.initWithName(svrAddress,Integer.parseInt(svrPort), svrKey, strClientType, username)) {
			Func.log_d(Const.LOG_TAG_DAHUA, "Failed to init P2p Client.");
			return  false;
		}

		String strDeviceInfo = p2pClient.getDeviceInfo(deviceSn);
		String strVesion = "";
		String strRandsalt = "";
		if (strDeviceInfo.length() != 0)
		{
			// get version
			int nVerPos = strDeviceInfo.indexOf("devp2pver");
			if(nVerPos != -1) {
				int nVerValueBeginPos = nVerPos + 12;
				int nVerValueEndPos = strDeviceInfo.indexOf('"', nVerValueBeginPos + 1);
				if(nVerValueEndPos != -1)
				{
					strVesion = strDeviceInfo.substring(nVerValueBeginPos, nVerValueEndPos);
				}
			}
			// get randsalt
			int nRandsaltPos = strDeviceInfo.indexOf("randsalt");
			if(nRandsaltPos != -1) {
				int nRandsaltValueBeginPos = nRandsaltPos + 11;
				int nRandsaltValueEndPos = strDeviceInfo.indexOf('"', nRandsaltValueBeginPos + 1);
				if(nRandsaltValueEndPos != -1)
				{
					strRandsalt = strDeviceInfo.substring(nRandsaltValueBeginPos, nRandsaltValueEndPos);
				}
			}
		}
		boolean bClientOnline = false;
		boolean bDeviceOnline = false;
		int nLoopCount = 3;
		while (nLoopCount > 0) {
			mLocalPort = p2pClient.addPortEx(deviceSn, Integer.parseInt(devicePort), 0, p2pDeviceUsername, p2pDevicePassword, strRandsalt, strVesion);
			Func.log_d(Const.LOG_TAG_DAHUA, "mLocalPort : " + mLocalPort);
			int nTry2 = 0;
			while (nTry2 < 30) {    ///200次，每次100ms，总时间为20s；如果设置的总时间太少(比如20 * 100)，p2p不一定成功
				int nStatus = p2pClient.portStatus(mLocalPort);
				Func.log_d(Const.LOG_TAG_DAHUA, "nStatus : " + nStatus);
				if (1 == nStatus) {
					Func.log_d(Const.LOG_TAG_DAHUA, "Start Service --> End. add port ok . port = " + mLocalPort);
					return true;
				} else if (2 == nStatus) {
					if (!bClientOnline) {
						// status
						if (p2pClient.status() == 3) {
							Func.log_d(Const.LOG_TAG_DAHUA, "client is online.");
							bClientOnline = true;
						}
					}

					if (!bDeviceOnline) {
						if (1 == p2pClient.query(deviceSn)) {
							Func.log_d(Const.LOG_TAG_DAHUA, "device is online.");
							bDeviceOnline = true;
						}
					}

					break;
				}
				nTry2 ++;

				try {
					Thread.sleep(10);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			p2pClient.delPort(mLocalPort);
			nLoopCount --;
		}

		Func.log_d(Const.LOG_TAG_DAHUA, "Start Service --> End. failed to start p2p service.");
		return false;
	}
}
