package biz.teko.td.SHUB_APP.Delegation.Activities;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;

/**
 * Created by td13017 on 21.06.2017.
 */

public class DelegateSendActivity extends BaseActivity {
    private ServiceConnection serviceConnection;
    private D3Service myService;
    private boolean bound;
    private Context context;

    private final BroadcastReceiver delegationCodeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String code = intent.getStringExtra("user_code");
            if (null != code) {
                if (null != buttonGetCode) {
                    buttonGetCode.setVisibility(View.GONE);
                    textCode.setVisibility(View.VISIBLE);
                    textCode.setText(code);
                }
            }
        }
    };
    private NActionButton buttonGetCode;
    private TextView textCode;
    private boolean toShare = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delegate_send);
        context = this;
        final DBHelper dbHelper = DBHelper.getInstance(context);

        setToolbar();

        final int siteId = getIntent().getIntExtra("site", -1);
        if (-1 != siteId) {
            Site site = dbHelper.getSiteById(siteId);
            TextView siteIdText = (TextView) findViewById(R.id.delegate_site_name);
            siteIdText.setText(site.name);
        }

        textCode = (TextView) findViewById(R.id.delegate_text_code);

        buttonGetCode = (NActionButton) findViewById(R.id.delegate_button_get_code);
        buttonGetCode.setOnButtonClickListener(action -> {
            if (siteId != -1) {
                if (null != myService) {
                    UserInfo userInfo = dbHelper.getUserInfo();
                    JSONObject message = new JSONObject();
                    try {
                        message.put("site_id", siteId);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    D3Request d3Request = D3Request.createMessage(userInfo.roles, "DELEGATE_NEW", message);
                    myService.send(d3Request.toString());
                } else {
                    Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
                }
            } else {
                Func.nShowMessage(context, context.getString(R.string.DEL_INCORRECT_SITE_DATA));
            }
        });

        ImageButton buttonCopyCode = (ImageButton) findViewById(R.id.delegate_copy_code_button);
        buttonCopyCode.setOnClickListener(v -> {
            String code = textCode.getText().toString();
            if (!code.equals("")) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("delegate_code", textCode.getText());
                clipboard.setPrimaryClip(clip);
                Func.pushToast(context, getString(R.string.COPIED_IN_BUFFER), (DelegateSendActivity) context);
            } else {
                Func.pushToast(context, getString(R.string.DELEGATE_GET_CODE_AT_FIRST), (DelegateSendActivity) context);
            }
        });

        ImageButton buttonShareCode = (ImageButton) findViewById(R.id.delegate_copy_share_button);
        buttonShareCode.setOnClickListener(v -> {
            String code = textCode.getText().toString();
            if (!code.equals("")) {
                toShare = true;
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.DELEGATION_SHARE_SUBJECT));
                sharingIntent.putExtra(Intent.EXTRA_TEXT, code);
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.DELEGATION_SHARE_TITLE)));
            } else {
                Func.pushToast(context, getString(R.string.DELEGATE_GET_CODE_AT_FIRST), (DelegateSendActivity) context);
            }
        });


    }

    private void setToolbar() {
        NTopToolbarView toolbar = (NTopToolbarView) findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);
    }

    @Override
    protected void onResume() {
//		((App)this.getApplication()).stopActivityTransitionTimer();
//		toShare = false;
        registerReceiver(delegationCodeReceiver, new IntentFilter(D3Service.BROADCAST_DELEGATE_NEW));
        super.onResume();
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = (D3Service) myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
        bindService(intent, serviceConnection, 0);
    }

    @Override
    protected void onPause() {
//		if(!toShare){
//			((App)this.getApplication()).startActivityTransitionTimer(context);
//		}
        super.onPause();
        unbindService(serviceConnection);
        unregisterReceiver(delegationCodeReceiver);
    }
}
