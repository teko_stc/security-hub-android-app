package biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies;

import java.util.Map;

public class ArmWaitReviewed extends BaseBooleanConfigEntity {

    public ArmWaitReviewed() {
        map.put(false, "Allow");
        map.put(true, "Disallow");
        setValues();
    }

    public boolean get(byte flag) {
        now = ((1 << 7 & flag) > 0);
        return now;
    }

    public void set(byte[] flags, String choice) {
        for (Map.Entry<Boolean, String> entry : map.entrySet()) {
            if (entry.getValue().equals(choice))
                now = entry.getKey();
        }
        byte mask;
        mask = (byte)(1 << 7);
        if (now)
            flags[0] = (byte) (flags[0] | mask);
        else
            flags[0] = (byte) (flags[0] & ~mask);
        }
}
