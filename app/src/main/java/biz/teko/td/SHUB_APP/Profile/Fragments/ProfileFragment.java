package biz.teko.td.SHUB_APP.Profile.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicReference;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.MainNavigation;
import biz.teko.td.SHUB_APP.Activity.MainNavigationActivity;
import biz.teko.td.SHUB_APP.Activity.StartUpActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.CamerasLoginActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Activities.EventsDevActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NAboutActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NLicenseActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NProfileLanguageActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.NProfileUsersActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.ProfileConnectionActivity;
import biz.teko.td.SHUB_APP.Profile.Activities.ProfileInterfaceActivity;
import biz.teko.td.SHUB_APP.Profile.CurrentUser.NCurrentUserActivity;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.Activities.LocalConfigurationNavigationActivity;
import biz.teko.td.SHUB_APP.Profile.Notifications.NotificationsActivity;
import biz.teko.td.SHUB_APP.Profile.Security.SecurityActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SecCompanies.SecCompaniesActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDefaultListElement;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDotsView;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by td13017 on 25.05.2016.
 */
public class ProfileFragment extends Fragment {

    private BaseActivity activity;
    private View layout;
    Drawer drawerResult;
    public final static int DEF_POSITION_MASTER = 6;
    public final static int DEF_POSITION = 5;
    private DBHelper dbHelper;
    private UserInfo userInfo;

    private D3Service myService;
    private ServiceConnection serviceConnection;
    private boolean bound;
    private final int sending = 0;

    private Button buttonAddCotnr;
    private Button buttonChangePW;
    private EditText editNewPW;
    private EditText editOldPW;
    private ProfileFragment profileActivity;
    private Button buttonChangePN;
    private Button buttonExit;
    private EditText editPW;
    private Button buttonShowUsers;
    private Button buttonLocalConfig;
    private Button buttonAbout;
    private Button buttonSetNotifications;

    private NDialog waitDialog;
    private boolean wait;
    private Timer timer;

    public ProfileFragment(){
        this.activity = (MainNavigationActivity) requireContext();
    }

    public ProfileFragment (MainNavigation activity){
        this.activity =activity;
    }

    private final BroadcastReceiver openIVLoginWebPageAfterGetSaveSession = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (null != waitDialog && waitDialog.isShowing()) {
                if (wait) {
                    wait = false;
                    if (null != timer) timer.cancel();
                    String session = intent.getStringExtra("session");
                    if (null != session) {
                        Intent webIntent = new Intent(getActivity(), CamerasLoginActivity.class);
                        webIntent.putExtra("session", session);
                        startActivityForResult(webIntent, Const.REQUEST_IV_LOGIN);
                    }
                } else {
                    Func.nShowMessage(context, getString(R.string.N_PROFILE_IV_UNAVAILABLE));
                }
                waitDialog.dismiss();
            }
        }
    };

    private final BroadcastReceiver operatorChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int result = intent.getIntExtra("result", -99);
            if (result > 0) {
                Func.pushToast(context, getString(R.string.PUA_SUCCESS_CHANGED), (Activity) context);
            } else {
                Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
            }
        }
    };

    private final BroadcastReceiver updateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateBottomHelper();
        }
    };

    private void updateBottomHelper() {
        activity.setBottomInfoBehaviour(getContext());
    }


    private SharedPreferences sp;
    private PrefUtils prefUtils;
    private NDefaultListElement profile_current_user;
    private NDefaultListElement profile_support;
    private NDefaultListElement profile_security;
    private NDefaultListElement profile_connection;
    private NDefaultListElement profile_language;
    private NDefaultListElement profile_apn;
    private TextView profile_controllers_section;
    private NDefaultListElement profile_debug;
    private NDefaultListElement profile_companies;
    private NDefaultListElement profile_shop;
    private NDefaultListElement profile_legal;
    private NDefaultListElement profile_about;
    private NDefaultListElement profile_users;
    private NDefaultListElement profile_notifications;
    private TextView profile_accounts_section;
    private NDefaultListElement profile_ivideon;
    private NDefaultListElement profile_interface;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       return inflater.inflate(R.layout.n_activity_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((BaseActivity)getActivity()).setOnBackPressedCallback(this::onBackPressed);
        ((BaseActivity)getActivity()).getSwipeBackLayout().setEnableGesture(false);
        layout = getView();
        setupView();
    }

    @Override
    public void onStart() {
        super.onStart();
        bindView();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelper = DBHelper.getInstance(getActivity());
        prefUtils = PrefUtils.getInstance(getActivity());
        userInfo = dbHelper.getUserInfo();

        sp = PreferenceManager.getDefaultSharedPreferences(getActivity());

        //setContentView(R.layout.n_activity_profile);
        //setBottomNavigation(R.id.bottom_navigation_item_menu, activity);


        serviceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };


    }

    private void setupView() {
        profile_current_user = layout.findViewById(R.id.nFrameCurrentUser);
        profile_support = layout.findViewById(R.id.nLinearSupport);
        profile_security = layout.findViewById(R.id.nLinearSecurity);
        profile_users = layout.findViewById(R.id.nLinearUsers);
        profile_notifications = layout.findViewById(R.id.nLinearNotifications);
        profile_connection = layout.findViewById(R.id.nLinearConnection);
        profile_language = layout.findViewById(R.id.nLinearLanguage);
        profile_controllers_section = layout.findViewById(R.id.nControllersSectionText);
        profile_debug = layout.findViewById(R.id.nLinearHistory);
        profile_apn = layout.findViewById(R.id.nLinearApn);
        profile_companies = layout.findViewById(R.id.nLinearCompanies);
        profile_shop = layout.findViewById(R.id.nLinearShop);
        profile_legal = layout.findViewById(R.id.nLinearLegal);
        profile_about = layout.findViewById(R.id.nLinearAbout);
        profile_accounts_section = layout.findViewById(R.id.nAccountsSection);
        profile_ivideon = layout.findViewById(R.id.nLinearIVideon);
        profile_interface = layout.findViewById(R.id.nLinearInterface);
    }

    private void bindView() {
        if (null != profile_support)
            if(!Func.isRuLang(getActivity())){
                profile_support.setVisibility(View.GONE);
            }else{
                profile_support.setOnRootClickListener(this::showCallDialog);
            }

        if (null != profile_current_user) {
            if (null != userInfo) {
                Operator operator = dbHelper.getCurrentOperator(userInfo);
                if (null != operator) {
                    profile_current_user.setTitle(operator.name);
                }
            }
            profile_current_user.setOnRootClickListener(() -> {
                Intent intent = new Intent(getActivity(), NCurrentUserActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
            });
        }

        if (null != profile_security) {
            profile_security.setOnRootClickListener(() -> {
                Intent intent = new Intent(getActivity(), SecurityActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
            });
        }

        if (null != profile_users)
            profile_users.setOnRootClickListener(() -> {
                UserInfo userInfo = dbHelper.getUserInfo();
                if ((userInfo.roles & Const.Roles.DOMAIN_ADMIN) != 0) {
                    Intent intent = new Intent(getActivity(), NProfileUsersActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
                } else {
                    Func.nShowMessage(getActivity(), getActivity().getString(R.string.EA_NO_RIGHTS));
                }
            });

        if (null != profile_notifications)
            profile_notifications.setOnRootClickListener(() -> {
                Intent intent = new Intent(getActivity(), NotificationsActivity.class);
                startActivityForResult(intent, Const.REQUEST_TURN_AM);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
            });

        if (null != profile_debug) {
            profile_debug.setOnRootClickListener(() -> {
                Intent intent = new Intent(getActivity(), EventsDevActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
            });
        }

        if (null != profile_apn) {
            profile_apn.setOnRootClickListener(() -> {
                Intent intent = new Intent(getActivity(), LocalConfigurationNavigationActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
            });
        }

		if(null!=profile_shop){
		    if(!Func.isRuLang(getActivity())){
		        profile_shop.setVisibility(View.GONE);
            }else{
		        profile_shop.setOnRootClickListener(() -> {
                    NDialog enterShopDialog = new NDialog(getActivity(), R.layout.n_dialog_question_small);
                    enterShopDialog.setTitleSpanned(Html.fromHtml(getResources().getString(R.string.OPEN_SHOP_DIALOG_MESSAGE)));
                    enterShopDialog.setOnActionClickListener((value, b) -> {
                        switch (value){
                            case NActionButton.VALUE_OK:
                                enterShopDialog.dismiss();
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.SEC_HUB_WEBSITE_LONG)));
                                startActivity(browserIntent);
                                break;
                            case NActionButton.VALUE_CANCEL:
                                enterShopDialog.dismiss();
                                break;
                        }
                    });
                    enterShopDialog.show();
                });
            }
		}

        if (null != profile_legal) {
            if (Func.needLicenseAForBuild()) {
                profile_legal.setOnRootClickListener(() -> {
                    Intent intent = new Intent(getActivity(), NLicenseActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
                });
            } else {
            	profile_legal.setVisibility(View.GONE);
            }
        }

        if (null != profile_about)
            profile_about.setOnRootClickListener(() -> {
                Intent intent = new Intent(getActivity(), NAboutActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
            });

        if (null != profile_language) {
            if (Func.needLangForBuild(getActivity())) {
                profile_language.setOnRootClickListener(() -> {
                    Intent intent = new Intent(getActivity(), NProfileLanguageActivity.class);
                    startActivityForResult(intent, D3Service.CHOOSE_LANGUAGE);
                    getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
                });
            } else {
            	profile_language.setVisibility(View.GONE);
            }
        }

        if (null != profile_connection) {
            profile_connection.setOnRootClickListener(() -> {
                Intent intent = new Intent(getActivity(), ProfileConnectionActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
            });
        }
        if (null != profile_interface)
            profile_interface.setOnRootClickListener(() -> {
                Intent intent = new Intent(getActivity(), ProfileInterfaceActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
            });

        if (null != profile_companies) {
            if (!Func.isRuLang(getActivity())) {
                profile_companies.setVisibility(View.GONE);
            } else {
                profile_companies.setOnRootClickListener(() -> {
                    Intent intent = new Intent(getActivity().getBaseContext(), SecCompaniesActivity.class);
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
                });
            }
        }
        if(Func.needCamerasForBuild()){
            if (null != profile_ivideon) {
                profile_ivideon.setOnRootClickListener(() -> {
                    String iv_token = dbHelper.getUserInfoIVToken();
                    if (null != iv_token && !iv_token.equals("")) {
                        NDialog nDialog = new NDialog(getActivity(), R.layout.n_dialog_question_small);
                        nDialog.setTitle(getString(R.string.N_PROFILE_IV_UNBIND_Q));
                        nDialog.setOnActionClickListener(new NDialog.OnActionClickListener() {
                            @Override
                            public void onActionClick(int value, boolean b) {
                                switch (value) {
                                    case NActionButton.VALUE_OK:
                                        UserInfo userInfo = dbHelper.getUserInfo();
                                        JSONObject message = new JSONObject();
                                        try {
                                            message.put("domain", userInfo.domain);
                                            nSendMessageToServer(null, D3Request.IV_DEL_USER, message, false);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        nDialog.dismiss();
                                        break;
                                    case NActionButton.VALUE_CANCEL:
                                        nDialog.dismiss();
                                        break;
                                }
                            }
                        });
                        nDialog.show();
                    } else {
                        loginIV();
                    }
                });
            }
        }else{
            if(null!=profile_accounts_section) profile_accounts_section.setVisibility(View.GONE);
            if(null!=profile_ivideon) profile_ivideon.setVisibility(View.GONE);
        }
    }

    private void loginIV() {
        getActivity().runOnUiThread(() -> {
            waitDialog = new NDialog(getActivity(), R.layout.n_dialog_progress);
            waitDialog.setCancelable(false);
            waitDialog.setTitle(getString(R.string.DIALOG_WAIT_TITLE));
            waitDialog.show();
        });
        wait = true;
        getSafeSession();

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (wait) {
                    wait = false;
                    getActivity().sendBroadcast(new Intent(D3Service.BROADCAST_OPERATOR_SAFE_SESSION));
                }
            }
        }, 15000);
    }

    private void getSafeSession() {
        nSendMessageToServer(null, D3Request.OPERATOR_SAFE_SESSION, new JSONObject(), false);
    }

    private void showCallDialog() {
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(TELEPHONY_SERVICE);
        if (telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
            Func.nShowMessage(getActivity(), getString(R.string.ERROR_CANT_MAKE_CALL_HARDWARE));
        } else {
            if (!prefUtils.getPinEnabled()) {

                NDialog dialog = new NDialog(getActivity(), R.layout.n_dialog_call);
                dialog.setOnActionClickListener((value, b) -> {
                    switch (value){
                        case NActionButton.VALUE_CANCEL:
                            dialog.dismiss();
                            break;
                        case NActionButton.VALUE_OK:
                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(getString(R.string.TEL) + getResources().getString(R.string.support_phone)));
                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                                return;
                            }
                            startActivity(intent);
                            dialog.dismiss();
                            break;
                    }
                });
                dialog.show();

            } else {
                NDialog dialog = new NDialog(getActivity(), R.layout.n_dialog_with_pin_call);
                NDotsView dotsView = dialog.findViewById(R.id.nPinDots);
                NEditText pinEditText = dialog.findViewById(R.id.editPin);
                AtomicReference<String> shared = new AtomicReference<>("");
                Func.setDotsPinEditText(pinEditText, shared, dotsView);
                dialog.setOnActionClickListener((value, b) -> {
                    switch (value){
                        case NActionButton.VALUE_CANCEL:
                            dialog.dismiss();
                            break;
                        case NActionButton.VALUE_OK:
                            callSupport(shared.get(), dialog);
                            pinEditText.setText("");
                            dotsView.setCount(0);
                            break;
                    }
                });
                dialog.show();
            }
        }
    }

    private void callSupport(String pinText, NDialog dialog) {
        if (pinText.length() != 4) {
            Func.pushToast(getActivity(), getString(R.string.ERROR_WRONG_PIN_CODE), getActivity());
        } else {
            int p = Integer.parseInt(pinText);
            String pin = Func.md5(String.valueOf(p));
            String dbPin = DBHelper.getInstance(getActivity()).getUserInfo().pinCode;
            if (pin.equals(dbPin)) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(getString(R.string.TEL) + getResources().getString(R.string.support_phone)));
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 101);
                    return;
                }
                startActivity(intent);
                dialog.dismiss();
            } else {
                Func.pushToast(getActivity(), getString(R.string.ERROR_WRONG_PIN), getActivity());
            }
        }
    }
    //TODO: why?
    /*@Override
    public void onPrepareOptionsMenu(Menu menu) {
        return true;
    }*/

    @Override
    public void onResume() {
        super.onResume();
        bindD3();
        getActivity().registerReceiver(openIVLoginWebPageAfterGetSaveSession, new IntentFilter(D3Service.BROADCAST_OPERATOR_SAFE_SESSION));
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(D3Service.BROADCAST_AFFECT);
        intentFilter.addAction(D3Service.BROADCAST_ALARM_ADD_UPD_RMV);
        getActivity().registerReceiver(updateReceiver, intentFilter);
        updateBottomHelper();
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unbindService(serviceConnection);
        getActivity().unregisterReceiver(updateReceiver);
        getActivity().unregisterReceiver(openIVLoginWebPageAfterGetSaveSession);
    }

    public void onBackPressed() {
        if ((drawerResult != null) && (drawerResult.isDrawerOpen())) {
            drawerResult.closeDrawer();
        } else {
            NDialog nDialog = new NDialog(getActivity(), R.layout.n_dialog_question);
            nDialog.setTitle(getString(R.string.N_LOGOUT_Q));
            nDialog.setOnActionClickListener((value, b) -> {
                switch (value) {
                    case NActionButton.VALUE_OK:
                        getActivity().finishAffinity();
                        break;
                    case NActionButton.VALUE_CANCEL:
                        nDialog.dismiss();
                        break;
                }

            });
            nDialog.show();
        }
    }
    private void bindD3() {
        Intent intent = new Intent(getActivity(), D3Service.class);
        serviceConnection = getServiceConnection();
        getActivity().bindService(intent, serviceConnection, 0);
    }

    private ServiceConnection getServiceConnection() {
        return new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name) {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
    }

    private D3Service getLocalService() {
        if (bound) {
            return myService;
        } else {
            bindD3();
            if (null != myService) {
                return myService;
            }
            Func.showRestartServiceMessage(getActivity(), getActivity().getString(R.string.EA_D3_REPEAT_CONNECTION));
        }
        return null;

    }

    private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command) {
        return Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), getActivity(), command, sending);
    }

    //TODO: test onActivityResult
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.REQUEST_IV_LOGIN) {
            if (resultCode == RESULT_OK) {
                getToken();
                Func.nShowSuccessMessage(getActivity());
            }
        } else if (requestCode == Const.REQUEST_TURN_AM) {
            updateBottomHelper();
        } else if (requestCode == D3Service.CHOOSE_LANGUAGE) {
//            finish();
//            startActivity(getIntent());
//            recreate();

            if(resultCode == RESULT_OK){
                Intent in = new Intent(getActivity(), StartUpActivity.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(in);
                requireActivity().finish();
//                Runtime.getRuntime().exit(0);
            }
        }
    }

    private void getToken() {
        UserInfo userInfo = dbHelper.getUserInfo();
        JSONObject message = new JSONObject();
        try {
            message.put("domain", userInfo.domain);
            nSendMessageToServer(null, D3Request.IV_GET_TOKEN, message, false);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
