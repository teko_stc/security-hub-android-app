package biz.teko.td.SHUB_APP.Activity;

import static biz.teko.td.SHUB_APP.Utils.Dir.Func.setDynamicHeight;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SitesTabs.Adapters.AlarmsListAdapter;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class NewEventsActivity extends BaseActivity
{

	private Context context;
	private ListView activeList;
	private AlarmsListAdapter activeListAdapter;
	private DBHelper dbHelper;
	private ListView repairList;
	private AlarmsListAdapter repairListAdapter;
	private int l_count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.frame_new_events);
		getSwipeBackLayout().setEnableGesture(false);

		context = this;
		dbHelper = DBHelper.getInstance(context);

		ImageView closeHeader = (ImageView) findViewById(R.id.closeButton);
		TextView closeFooter = (TextView) findViewById(R.id.footerCloseButton);
		if(null!=closeFooter) closeFooter.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});
		if(null!=closeHeader) closeHeader.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		sp.edit().putBoolean("all_events_were_seen", true).apply();
		long time = sp.getLong("last_activity_closed_time", System.currentTimeMillis() - D3Service.EVENTS_TIME_INTERVAL); // TODO уменьшить интервал, слишком много событий
		Func.log_v("D3ServiceLog", "GET CLOSE TIME:" + time);

		TextView message = (TextView) findViewById(R.id.textCriticalMessage);
		TextView activeTitle = (TextView) findViewById(R.id.title_current);
		TextView repairTitle = (TextView) findViewById(R.id.title_repair);
		activeList = (ListView) findViewById(R.id.list_current);
		repairList = (ListView) findViewById(R.id.list_repair);

		Cursor activeCursor = dbHelper.getNewActiveAttentionEvents(time);
		Cursor activeLostCursor = dbHelper.getNewActiveLostEvents();
		Cursor repairCursor = dbHelper.getNewRepairedAttentionEvents(time);


		int a_count = activeCursor!=null? activeCursor.getCount():0;
		l_count = activeLostCursor!=null? activeLostCursor.getCount():0;
		int r_count = repairCursor!=null? repairCursor.getCount():0;
		if(a_count==0 && r_count==0){
			activeTitle.setVisibility(View.GONE);
			repairTitle.setVisibility(View.GONE);
			activeList.setVisibility(View.GONE);
			repairList.setVisibility(View.GONE);
		}else{
			message.setText(R.string.MESSAGE_CRITICAL_EVENTS);
			if(a_count!=0){
				activeListAdapter = new AlarmsListAdapter(context, R.layout.card_alarm, activeCursor, 0, true);
				activeList.setAdapter(activeListAdapter);
			}else{
				activeTitle.setVisibility(View.GONE);
				activeList.setVisibility(View.GONE);
			}
			if(r_count!=0){
				repairListAdapter = new AlarmsListAdapter(context, R.layout.card_repaired_alarm, repairCursor, 0, false);
				repairList.setAdapter(repairListAdapter);
			}else{
				repairTitle.setVisibility(View.GONE);
				repairList.setVisibility(View.GONE);
			}

			setDynamicHeight(activeList);
			setDynamicHeight(repairList);
		}
	}

	@Override
	protected void onPause()
	{
		super.onPause();
	}

	@Override
	public void onBackPressed()
	{
		Intent intent = new Intent();
		setResult(RESULT_OK, intent);
		if (l_count != 0){

		}
		super.onBackPressed();
	}




}
