package biz.teko.td.SHUB_APP.FaqTab.RecycleHolders;

import android.view.View;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 08.02.2017.
 */

public class ChapterGroupViewHolder extends GroupViewHolder
{

	private TextView chapterId;
	private TextView chapterName;

	public ChapterGroupViewHolder(View itemView)
	{
		super(itemView);
		chapterName = (TextView) itemView.findViewById(R.id.chapterName);
	}

	public void setChapterName(ExpandableGroup group)
	{
		this.chapterName.setText(group.getTitle());
	}

//	public void setSiteid(FaqGroup group)
//	{
//		this.chapterId.setText(group.getChapterId());
//	}
}
