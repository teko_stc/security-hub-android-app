package biz.teko.td.SHUB_APP.Cameras.Activities.RTSP;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

public  class NCameraRTSPLink extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private UserInfo userInfo;
	private LinearLayout buttonClose;
	private LinearLayout buttonNext;
	private TextView editLinkTitle;
	private NEditText editLink;
	private int camera_id;
	private Camera camera;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_camera_rtsp_link);

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();
		this.camera_id = getIntent().getIntExtra("camera_rtsp", -1);

		setup();
		bind();
	}

	private void setup()
	{
		buttonClose = (LinearLayout) findViewById(R.id.nButtonClose);
		buttonNext = (LinearLayout) findViewById(R.id.nButtonNext);
		editLinkTitle = (TextView) findViewById(R.id.nTextTitle);
		editLink = (NEditText) findViewById(R.id.nText);
	}

	private void bind()
	{
		if(-1!=camera_id){
			camera = dbHelper.getRTSPCameraById(camera_id);
		}
		if(null!=buttonClose) buttonClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		if(null!=buttonNext) buttonNext.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				setName();
			}
		});
		if(null!= editLinkTitle){
			editLinkTitle.setText("RTSP-ссылка");
		}
		if(null!=camera){
			if(null!= editLink){
				editLink.setText(camera.link);
				editLink.setSelection(editLink.getText().length());
				editLink.showKeyboard();
			}
		}
	}

	private void setName()
	{
		if(validate()){
			if(dbHelper.setLinkRTSPCamera(camera.local_id, editLink.getText().toString().trim())){
				onBackPressed();
			}
		}else{
			Func.nShowMessage(context, getString(R.string.N_RTSP_LINK_ERROR));
		}
	}

	private boolean validate()
	{
		if(null!= editLink.getText() && !editLink.getText().toString().equals("")){
			try{
				String arg  = editLink.getText().toString().substring(0, 7);
				if(null!=arg && arg.equals("rtsp://")){
					return true;
				}
				return false;
			}catch (Exception e){
				return false;
			}
		}else{
			return false;
		}
	}
}
