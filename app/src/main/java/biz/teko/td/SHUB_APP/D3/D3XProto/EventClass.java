package biz.teko.td.SHUB_APP.D3.D3XProto;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

public class EventClass
{
	@Attribute(name="id")
	public int id;

	@Attribute(name="caption")
	public String caption;

	@ElementList(name="reasons", entry="reason", required = false)
	public LinkedList<Reason> reasons;

	public EventClass(){}

	public EventClass(int id, String caption, LinkedList<Reason> reasons)
	{
		this.id = id;
		this.caption = caption;
		this.reasons = reasons;
	}

	public String getReasonCaption(int id)
	{
		Reason reason = getReasonById(id);
		if(null==reason){
			return null;
		}
		return reason.caption;
	}

	public Reason getReasonById(int id){
		LinkedList<Reason> reasons  =this.reasons;
		for(Reason reason:reasons){
			if(id == reason.id){
				return reason;
			}
		}
		return  null;
	}
}

class Reason
{
	@Attribute(name="id")
	public int id;

	@Attribute(name="caption")
	public String caption;

	public Reason() {}
}