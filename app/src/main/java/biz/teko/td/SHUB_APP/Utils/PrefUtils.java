package biz.teko.td.SHUB_APP.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.BuildConfig;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.EClass;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class PrefUtils
{
	/*don't drop 4 all*/
	private static final String LICENSE_AGREEMENT = "license_agree_pref";
	private static final String PREF_CURRENT_USER = "current_user_id";
	private static final String PREF_IMPORT_SETTINGS = "pref_import_settings_db_for_user";
	private static final String PREF_IMPORT_NOTIF_SETTINGS = "pref_import_notif_settings_for_user";
	private final static String APP_BEEN_RATED = "pref_app_been_rated";
	private final static String CURRENT_LANG = "prof_language_value";
	private final static String REQUEST_COUNT = "request_count";

	/*drop 4 all*/
	private final static String ENABLE_PIN = "prof_enable_pin";
	private final static String FINGERPRINT_AUTH = "prof_finger";
	private static final String NOTIFICATION_PROBLEM = "pref_notification_problem";
	private static final String NOTIFICATION_PROBLEM_MIN_TIME = "pref_notification_problem_min_time";
	private static final String NOTIFICATION_PROBLEM_DIF_TIME = "pref_notification_problem_dif_time";

	private static final String NOTIFICATION_STATUS = "notif_status";

	/*save in case checked for current user*/
	private final static String CURRENT_SITE = "current_site";
	private final static String AM = "notif_inapp_am";
	private static final String CONNECTION_MODE = "notif_connection_mode";

	public static final String SHARED_MAIN_SCREEN_MODE = "shared_main_screen";
	public static final String CONN_DROP_NOTIF_MODE = "conn_drop_notif_pref";
	public static final String CONNECTION_DROP_NOTIFICATION_CHANNEL = "conn_drop_notif_channel_id";
	public static final String SERVICE_NOTIFICATION_CHANNEL = "service_notif_channel_id";
    private static final String COMMAND_COUNT = "command_count";
	private static final String NOTIFICATION_COUNT = "notification_count";

	private static final String NOTIFICATION_CHANNEL = "pref_channel_id_";
	private static final String NOTIFICATION_CHANNEL_COUNTER = "pref_channel_counter";

	public static final String KEY_PREF_CLASS_NOTIF_STATUS = "notif_class_status_";

	public static final String KEY_PREF_CLASS_NOTIF_CLASS_VIBRO = "notif_class_vibro_";
	public static final String KEY_PREF_CLASS_NOTIF_CLASS_SOUND_STATUS = "notif_class_sound_status_";
	public static final String KEY_PREF_CLASS_NOTIF_CLASS_TTS = "notif_tts_";
	public static final String KEY_PREF_CLASS_NOTIF_CLASS_RINGTONE = "notif_class_sound_";
	public static final String KEY_PREF_CLASS_NOTIF_CLASS_LED = "notif_class_led_";
	public static final String KEY_PREF_CLASS_NOTIF_CLASS_POPUP = "notif_class_popup_";
	public static final String KEY_PREF_CLASS_NOTIF_CLASS_PRIORITY = "notif_class_priority_";
	public static final String KEY_PREF_CLASS_NOTIF_CLASS_INAPP_VIBRO = "notif_inapp_vibro";
	public static final String KEY_PREF_CLASS_NOTIF_CLASS_INAPP_SOUND = "notif_inapp_sound";

	public static final String KEY_PREF_CLASS_NOTIF_OREO_IMPORTANCE = "notif_class_importance_oreo_";
	public static final String KEY_PREF_CLASS_NOTIF_OREO_RINGTONE = "notif_class_ringtone_oreo";
	public static final String KEY_PREF_CLASS_NOTIF_OREO_VIBRO = "notif_class_vibro_oreo";
	public static final String KEY_PREF_CLASS_NOTIF_OREO_LIGHTS = "notif_class_lights_oreo";
	public static final String KEY_PREF_CLASS_NOTIF_OREO_BADGE = "notif_class_badge_oreo";


//	public static final String KEY_PREF_ALARM_MODE_OREO = "notif_alarm_oreo_";
//	public static final String KEY_PREF_ALARM_RINGTONE_OREO = "notif_ringtone_fullscreen_oreo_";
//	public static final String KEY_PREF_ALARM_INTERVAL_OREO = "notif_interval_oreo_";

	public static final String KEY_PREF_CLASS_NOTIF_ALARMALARM_MODE = "notif_alarm_oreo_";
	public static final String KEY_PREF_CLASS_NOTIF_ALARMALARM_RINGTONE = "notif_ringtone_fullscreen_oreo_";
	public static final String KEY_PREF_CLASS_NOTIF_ALARMALARM_INTERVAL = "notif_interval_oreo_";

	public static final String NOTIFICATION_CHANNEL_IMPORTANCE = "pref_channel_importance_";
	public static final String NOTIFICATION_CHANNEL_SOUND = "pref_channel_sound_";
	public static final String NOTIFICATION_CHANNEL_VIBRATE = "pref_channel_vibrate_";
	public static final String NOTIFICATION_CHANNEL_LIGHT = "pref_channel_light_";
	public static final String NOTIFICATION_CHANNEL_BADGE = "pref_channel_badge";


	public static final String MEDIA_FILES_DIR = "pref_media_dir";


	public static final String DIVIDER = "_";

	private static final String KEY_START_TUTORIAL = "pref_start_tutorial";
	private static final String KEY_START_TUTORIAL_PAGE = "pref_start_tutorial_page";
	private static final String KEY_START_STORIES = "pref_start_stories";

	private static PrefUtils instance;
	private final Context context;
	private static SharedPreferences sp;

	public  PrefUtils(Context context){
		this.context = context;
	}

	public static PrefUtils getInstance(Context context){
		if(null == instance){
			instance = new PrefUtils(context);
			sp = instance.getDefaultSharedPreferences();

		}
		return instance;
	}

	public static void updateInstance(Context context){
		instance = new PrefUtils(context);
		sp = instance.getDefaultSharedPreferences();
	}

	/*-------------------*/
	public boolean isBeenRated(){
		int r = _getInt(APP_BEEN_RATED, 0);
		return r == 1;
	}

	public void setRated(){
		_setInt(APP_BEEN_RATED,1);
	}

	public int getCurrentSite()
	{
		return getInt(CURRENT_SITE, 0);
	}

	public void setCurrentSite(int site_id){
		setInt(CURRENT_SITE, site_id);
	}

	public void removeCurrentSite()
	{
		remove(CURRENT_SITE);
	}

	public String getCurrentLang() {
		return _getString(CURRENT_LANG, context.getString(R.string.default_iso));
	}

	public String getCurrentLangNull() {
		return _getString(CURRENT_LANG, null);
	}

	public void setCurrentLang(String lang) {
		_setString(CURRENT_LANG, lang);
	}

	public void setPinEnabled(boolean b) {
		_setBoolean(ENABLE_PIN, b);
	}

	public boolean getPinEnabled() {
		return _getBooleanSPDefTrue(ENABLE_PIN);
	}

	public void removePinEnabled() {
		_remove(ENABLE_PIN);
	}

	public void removeSharedScreen() {
		remove(SHARED_MAIN_SCREEN_MODE);
	}

	public int getConnDropNotifChannelId() {
		return _getInt(CONNECTION_DROP_NOTIFICATION_CHANNEL, -2);
	}

	public void setConnDropNotifChannelId(int id) {
		_setInt(CONNECTION_DROP_NOTIFICATION_CHANNEL, id);
	}

	public int getServiceNotifChannelId(){
		return _getInt(SERVICE_NOTIFICATION_CHANNEL, -1);
	}

	public void setServiceNotificationChannelId(int id) {
		_setInt(SERVICE_NOTIFICATION_CHANNEL, id);
	}

	public void setFingerprintEnabled(boolean b) {
		_setBoolean(FINGERPRINT_AUTH, b);
	}

	public boolean getFingerprintEnabled() {
		return _getBooleanSPDefFalse(FINGERPRINT_AUTH);
	}

	public void removeFingerPrintEnabled() {
		_remove(FINGERPRINT_AUTH);
	}

	public void setNotificationsProblem(boolean b) {
		_setBoolean(NOTIFICATION_PROBLEM, b);
	}

	public boolean getNotificationProblem() {
		return _getBooleanSPDefFalse(NOTIFICATION_PROBLEM);
	}

	public void removeNotificationProblem(){
		_remove(NOTIFICATION_PROBLEM);
	}

	public void setNotificationsProblemMinTime(long time){
		_setLong(NOTIFICATION_PROBLEM_MIN_TIME, time);
	}

	public long getNotificationProblemMinTime(){
		return _getLong(NOTIFICATION_PROBLEM_MIN_TIME, 0);
	}

	public void removeNotificationProbemMinTime(){
		_remove(NOTIFICATION_PROBLEM_MIN_TIME);
	}

	public void setNotificationsProblemDifTime(long time){
		_setLong(NOTIFICATION_PROBLEM_DIF_TIME, time);
	}

	public long getNotificationProblemDifTime(){
		return _getLong(NOTIFICATION_PROBLEM_DIF_TIME, 0);
	}

	public void removeNotificationProblemDifTime(){
		_remove(NOTIFICATION_PROBLEM_DIF_TIME);
	}



	public void setCommandCount(int count){
		setInt(COMMAND_COUNT, count);
	}

	public int getCommandCount(){
		return getInt(COMMAND_COUNT, 0);
	}

	public void setNotificationCount(int count){
		setInt(NOTIFICATION_COUNT, count);
	}

	public int getNotificationCount(){
		return getInt(NOTIFICATION_COUNT, 0);
	}

	public void setConnectionMode(boolean b) {
		_setBoolean(CONNECTION_MODE, b);
	}

	public boolean getConnectionMode() {
		return _getBooleanSPDefFalse(CONNECTION_MODE);
	}

	public boolean getSharedMainScreenMode() {
		return getBooleanSPDefFalse(SHARED_MAIN_SCREEN_MODE);
	}

	public void setSharedMainScreenMode(boolean b) {
		setBoolean(SHARED_MAIN_SCREEN_MODE, b);
	}

	/*alarm monitor 16.10.2020*/
	public boolean getUseAM() {
		return getBooleanSPDefTrue(AM);
	}

	public void setUseAM(boolean b) {
		setBoolean(AM, b);
	}

	public boolean getConnDropNotifMode() {
		return _getBooleanSPDefTrue(CONN_DROP_NOTIF_MODE);
	}

	public void setConnDropNotifMode(boolean b) {
		_setBoolean(CONN_DROP_NOTIF_MODE, b);
	}
	/*--notifications--*/

	public int getNotificationChannelId(int classId) {
//		if (0 == id ){
//			id = getNotificationChannelCounter();
//			setNotificationChannelId(classId, id);
//			updateNotificationChannelCounter();
//		}
		return getInt(getClassKey(NOTIFICATION_CHANNEL, classId), classId);
	}

	public void setNotificationChannelId(int classId, int id){
		setInt(getClassKey(NOTIFICATION_CHANNEL, classId), id);
	}

	public void updateNotificationChannelCounter(){
		setNotificationChannelCounter(getNotificationChannelCounter() + 1);
	}

	public int getNotificationChannelCounter(){
		return _getInt(NOTIFICATION_CHANNEL_COUNTER, 16);
	}

	public void setNotificationChannelCounter(int i){
		_setInt(NOTIFICATION_CHANNEL_COUNTER, i);
	}

	public void setStartTutorial(boolean value) {
		_setBoolean(KEY_START_TUTORIAL, value);
	}

	public boolean getStartTutorial() {
		return _getBooleanSPDefTrue(KEY_START_TUTORIAL);
	}

	public void setStartStories(boolean value) {
		_setBoolean(KEY_START_STORIES, value);
	}

	public boolean getStartStories() {
		return _getBooleanSPDefTrue(KEY_START_STORIES);
	}

	/*----notification < oreo---*/

	public boolean getNotificationStatus(){
		return getBooleanSPDefTrue(NOTIFICATION_STATUS);
	}

	public void setNotificationStatus(boolean b){
		setBoolean(NOTIFICATION_STATUS, b);
	}

	public boolean getClassNotificationStatus(int classId){
		return classId < 5 ? getBooleanSPDefTrue(getClassKey(KEY_PREF_CLASS_NOTIF_STATUS, classId))
				:  getBooleanSPDefFalse(getClassKey(KEY_PREF_CLASS_NOTIF_STATUS, classId));
	}

	public void setClassNotificationStatus(int classId, boolean value) {
		setBoolean(getClassKey(KEY_PREF_CLASS_NOTIF_STATUS, classId), value);
	}

	public boolean getClassNotificationPriority(int classId){
		return  getBooleanSPDefTrue(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_PRIORITY, classId));
	}

	public boolean getClassNotificationVibro(int classId){
		return getBooleanSPDefTrue(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_VIBRO, classId));
	}

	public boolean getClassNotificationInAppVibro(int classId){
		return getBooleanSPDefTrue(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_INAPP_VIBRO, classId));
	}

	public boolean getClassNotificationSound(int classId){
		return getBooleanSPDefTrue(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_SOUND_STATUS, classId));
	}

	public boolean getClassNotificationInAppSound(int classId){
		return getBooleanSPDefTrue(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_INAPP_SOUND, classId));
	}

	public String getClassNotificationLed(int classId){
		return getString(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_LED, classId), "0");
	}

	public String getClassNotificationPopUp(int classId){
			return getString(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_POPUP, classId), "0");
		}

	public String getClassNotificationRingtone(int classId){
		return getString(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_RINGTONE, classId), context.getResources().getString(R.string.SUMMARY_DEFAULT));
	}


	/*----notification all---*/

	public boolean getClassNotificationTts(int classId){
		return classId < 5 ? getBooleanSPDefTrue(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_TTS, classId)) : getBooleanSPDefFalse(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_TTS, classId));
	}

	public void setClassNotificationTts(boolean b, int classId){
		setBoolean(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_TTS, classId), b);
	}

	public String getClassNotificatonAlarmMode(int classId){
		return ((Build.VERSION.SDK_INT < Build.VERSION_CODES.M) || (Func.isCurrentVersionSdk(Build.VERSION_CODES.M) && Settings.canDrawOverlays(App.getContext())))
				? (getString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_MODE, classId), classId == 0 ? Integer.toString(Const.NOTIF_ALARM_TYPE_REPEAT) :Integer.toString(Const.NOTIF_ALARM_TYPE_NONE)))
				: Integer.toString(0);
	}

	public void setClassNotificationAlarmMode(String value, int classId){
		setString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_MODE, classId), value);
	}

	public int getClassNotifAlarmInterval(int classId){
		int value = Integer.valueOf(getString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_INTERVAL, classId), Integer.toString(1)));
		return value > 10 ? 10 : value;
	}

	public void setClassNotificationAlarmInterval(int value, int classId){
		setString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_INTERVAL, classId), Integer.toString(value));
	}

	public String getClassNotifAlarmRingtone(int classId){
		return getString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_RINGTONE, classId), "");
	}

	public void setClassNotifAlarmRingtone(String value, int classId){
		setString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_RINGTONE, classId), value);
	}

	/*-----*/


	public int getRequestId()
	{
		_setInt(REQUEST_COUNT, _getInt(REQUEST_COUNT, 3) + 1);
		return (_getInt(REQUEST_COUNT, 3));
	}

	public void setLicenseAgreed() {
		_setBoolean(LICENSE_AGREEMENT, true);
	}

	public boolean getLicenseAgreed()
	{
		return _getBooleanSPDefFalse(LICENSE_AGREEMENT);
	}

	public void setCurrentUserId(int id){
		_setInt(PREF_CURRENT_USER, id);
	}

	public int getCurrentUserId()
	{
		return _getInt(PREF_CURRENT_USER, 0);
	}

	// import settings from old db and preferences if true
	public boolean getImportSettings(){
		return _getBooleanSPDefTrue(PREF_IMPORT_SETTINGS);
	}

	public void setImportSettings(boolean b){
		_setBoolean(PREF_IMPORT_SETTINGS, b);
	}

	public boolean getImportNotifChannels(){
		return _getBooleanSPDefTrue(PREF_IMPORT_NOTIF_SETTINGS);
	}

	public void setImportNotifChannels(boolean b){
		_setBoolean(PREF_IMPORT_NOTIF_SETTINGS, b);
	}


	/*---GET--------------*/
	private SharedPreferences getDefaultSharedPreferences() {
		return context.getSharedPreferences(getDefaultSharedPreferencesName(context),
				getDefaultSharedPreferencesMode());
	}

	private static String getDefaultSharedPreferencesName(Context context) {
		return context.getPackageName() + "_preferences";
	}

	private static int getDefaultSharedPreferencesMode() {
		return Context.MODE_PRIVATE;
	}


	/*-----------GET 4 common---------------------*/

	private static boolean _getBooleanSPDefTrue(String key){
		boolean result;
		try{
			result = sp.getBoolean(key, true);
		}catch (Exception e){
			try{
				result = sp.getInt(key, 1)!=0;
			}catch (Exception e1){
				result = false;
			};
		};
		return result;
	}

	private static boolean _getBooleanSPDefFalse(String key){
		boolean result;
		try{
			result = sp.getBoolean(key, false);
		}catch (Exception e){
			try{
				result = sp.getInt(key, 0)!=0;
			}catch (Exception e1){
				result = false;
			};
		};
		return result;
	}


	private int _getInt(String key, int deflt)
	{
		return sp.getInt(key, deflt);
	}

	private String _getString(String key, String s)
	{
		return sp.getString(key, s);
	}

	private long _getLong(String key, long deflt)
	{
		return sp.getLong(key, deflt);
	}

	/*-------------SET 4 common---------------*/

	private void _setBoolean(String key, boolean b){
		sp.edit().putBoolean(key, b).apply();
	}

	private void _setInt(String key, int value){
		sp.edit().putInt(key, value).apply();
	}

	private void _setLong(String key, long value){
		sp.edit().putLong(key, value).apply();
	}

	private void _setString(String key, String value){
		sp.edit().putString(key, value).apply();
	}

	private void _remove(String key){
		sp.edit().remove(key).apply();
	}

	/*-----------GET 4 current user---------------------*/

	private boolean getBooleanSPDefTrue(String key){
		boolean result;
		try{
			result = sp.getBoolean(getKey(key), true);
		}catch (Exception e){
			try{
				result = sp.getInt(getKey(key), 1)!=0;
			}catch (Exception e1){
				result = false;
			};
		};
		return result;
	}

	private boolean getBooleanSPDefFalse(String key){
		boolean result;
		try{
			result = sp.getBoolean(getKey(key), false);
		}catch (Exception e){
			try{
				result = sp.getInt(getKey(key), 0)!=0;
			}catch (Exception e1){
				result = false;
			};
		};
		return result;
	}


	private int getInt(String key, int deflt)
	{
		return sp.getInt(getKey(key), deflt);
	}

	private String getString(String key, String s)
	{
		return sp.getString(getKey(key), s);
	}

	private long getLong(String key)
	{
		return sp.getLong(getKey(key), 0);
	}

	/*-------------SET 4 current user---------------*/

	private void setBoolean(String key, boolean b){
		sp.edit().putBoolean(getKey(key), b).apply();
	}

	private void setInt(String key, int value){
		sp.edit().putInt(getKey(key), value).apply();
	}

	private void setLong(String key, long value){
		sp.edit().putLong(getKey(key), value).apply();
	}

	private void setString(String key, String value){
		sp.edit().putString(getKey(key), value).apply();
	}

	private void remove(String key){
		sp.edit().remove(getKey(key)).apply();
	}

	/*-----*/

	public void importSettings(LinkedList<EClass> classes)
	{
		setCurrentSite(_getInt(CURRENT_SITE, 0));
		setUseAM(_getBooleanSPDefTrue(AM));
//		setConnectionMode(_getBooleanSPDefFalse(CONNECTION_MODE));

		for(EClass eClass:classes){
			if(Build.VERSION_CODES.O > BuildConfig.VERSION_CODE){

			}else
			{
				int id = _getInt(getClassKey(NOTIFICATION_CHANNEL, eClass.id), -1);
				if (-1 != id)
				{
					setNotificationChannelId(eClass.id, id);
				}
			}

			setClassNotificationAlarmMode(
					((Build.VERSION.SDK_INT < Build.VERSION_CODES.M) || (Func.isCurrentVersionSdk(Build.VERSION_CODES.M) && Settings.canDrawOverlays(App.getContext())))
					? ((_getString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_MODE, eClass.id), eClass.id == 0 ? Integer.toString(Const.NOTIF_ALARM_TYPE_REPEAT) :Integer.toString(Const.NOTIF_ALARM_TYPE_NONE))))
					: Integer.toString(Const.NOTIF_ALARM_TYPE_NONE), eClass.id);
			setClassNotificationAlarmInterval(Integer.valueOf(_getString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_INTERVAL, eClass.id), Integer.toString(0))), eClass.id);
			setClassNotifAlarmRingtone(_getString(getClassKey(KEY_PREF_CLASS_NOTIF_ALARMALARM_RINGTONE, eClass.id), ""), eClass.id);

			setClassNotificationTts(eClass.id < 5 ? _getBooleanSPDefTrue(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_TTS, eClass.id)) : _getBooleanSPDefFalse(getClassKey(KEY_PREF_CLASS_NOTIF_CLASS_TTS, eClass.id)), eClass.id);
		}

		setImportSettings(false);
	}



	public String getClassKey(String key, int classId){
		return key + Integer.toString(classId);
	}

	public String getFullClassKey(String key, int classId){
		return getKey(key + Integer.toString(classId));
	}

	private String getKey(String key)
	{
		return key + DIVIDER + getCurrentUserId();
	}

	public void setNotificationChannelImportance(int classId, int importance)
	{
		setInt(getClassKey(NOTIFICATION_CHANNEL_IMPORTANCE, classId), importance);
	}

	public int getNotificationChannelImportance(int classId){
		return classId > 4 ? getInt(getClassKey(NOTIFICATION_CHANNEL_IMPORTANCE, classId), 0) : getInt(getClassKey(NOTIFICATION_CHANNEL_IMPORTANCE, classId), 4);
	}

	public void removeNotificationChannelImportance(int classId){
		remove(getClassKey(NOTIFICATION_CHANNEL_IMPORTANCE, classId));
	}

	public void setNotificationChannelSound(int classId, Uri sound)
	{
		setString(getClassKey(NOTIFICATION_CHANNEL_SOUND, classId), sound.toString());
	}

	public Uri getNotificationChannelSound(int classId){
		return Uri.parse(getString(getClassKey(NOTIFICATION_CHANNEL_SOUND, classId), context.getResources().getString(R.string.SUMMARY_DEFAULT)));
	}

	public void removeNotificationChannelSound(int classId){
		remove(getClassKey(NOTIFICATION_CHANNEL_SOUND, classId));
	}
	public void setNotificationChannelVibrate(int classId, boolean shouldVibrate)
	{
		setBoolean(getClassKey(NOTIFICATION_CHANNEL_VIBRATE, classId), shouldVibrate);
	}


	public boolean getNotificationChannelVibrate(int classId){
		return  classId > 4 ? getBooleanSPDefFalse(getClassKey(NOTIFICATION_CHANNEL_VIBRATE, classId)) : getBooleanSPDefTrue(getClassKey(NOTIFICATION_CHANNEL_VIBRATE, classId));
	}

	public void removeNotificationChannelVibrate(int classId){
		remove(getClassKey(NOTIFICATION_CHANNEL_VIBRATE, classId));
	}
	public void setNotificationChannelLight(int classId, boolean shouldShowLights)
	{
		setBoolean(getClassKey(NOTIFICATION_CHANNEL_LIGHT, classId), shouldShowLights);
	}

	public boolean getNotificationChannelLight(int classId){
		return  getBooleanSPDefFalse(getClassKey(NOTIFICATION_CHANNEL_LIGHT, classId));
	}

	public void removeNotificationChannelLight(int classId){
		remove(getClassKey(NOTIFICATION_CHANNEL_LIGHT, classId));
	}

	public void setNotificationChannelBadge(int classId, boolean canShowBadge)
	{
		setBoolean(getClassKey(NOTIFICATION_CHANNEL_BADGE, classId), canShowBadge);
	}

	public boolean getNotificationChannelBadge(int classId){
		return  getBooleanSPDefFalse(getClassKey(NOTIFICATION_CHANNEL_BADGE, classId));
	}
	public void removeNotificationChannelBadge(int classId){
		remove(getClassKey(NOTIFICATION_CHANNEL_BADGE, classId));
	}

	public String getMediaFilesDirectory(){
		return _getString(MEDIA_FILES_DIR, Func.getMediaFilesDirectory(context));
	}

	public void setMediaFilesDirectory(String dir){
		_setString(MEDIA_FILES_DIR, dir);
	}

}
