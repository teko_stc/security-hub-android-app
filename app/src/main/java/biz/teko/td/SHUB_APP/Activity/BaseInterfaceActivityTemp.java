package biz.teko.td.SHUB_APP.Activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.widget.ListView;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.Devices_N.Adapters.DevicesListAdapter;
import biz.teko.td.SHUB_APP.Devices_N.ServerConnection;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;


/*02.10.2020
* TODO временно
* для главного экрана и списка устройств/разделов
* чтобы исключить кнопку перевзятия, пока не доедалана логика поиска неудавшейся команды*/

public abstract class BaseInterfaceActivityTemp extends BaseActivity implements ServerConnection {
	public abstract void nrAction(int id);

	private AlertDialog.Builder nrDialogBuilder;
	private NDialog nrDialog;
	private DevicesListAdapter nrListAdapter;
	private ListView nrList;
	private final LinkedList<Event> eventsNRList = new LinkedList<>();

	private final BroadcastReceiver notReadyReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int eventId = intent.getIntExtra("eventId", -1);
			Event event = DBHelper.getInstance(context).getEventById(eventId);
			if(null!=event){
				showNotReadyMessage(event, context);
			}
		}
	};

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(notReadyReceiver, new IntentFilter(D3Service.BROADCAST_SITE_NOT_READY));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(notReadyReceiver);
	}

	public void showNotReadyMessage(Event event, final Context context)
	{
		//NO REPEAT
		eventsNRList.add(event);
		final LinkedList<Event> eventsList = eventsNRList;
		if((null!=nrDialog)&&(nrDialog.isShowing())){
			//updateAdapter
			if(nrListAdapter != null){
				nrListAdapter.update(getNRCursor(context));
			}
		}else
		{
			if((DBHelper.getInstance(context).getUserInfo().roles& Const.Roles.DOMAIN_HOZ_ORG)!=0){
				nrDialog = new NDialog(this, R.layout.n_dialog_question_no_repeat);
				nrDialog.setTitle(context.getResources().getString(R.string.MESS_NOT_READY));

				nrList = new ListView(context);
				nrList.setDivider(null);

				nrListAdapter = new DevicesListAdapter(context, R.layout.n_nr_devices_list_element, getNRCursor(context), 0, this, false, false);
				nrList.setAdapter(nrListAdapter);

				nrDialog.addView(nrList);
				nrDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
				{
					@Override
					public void onActionClick(int value, boolean b)
					{
						switch (value){
							case NActionButton.VALUE_OK:
								if ((eventsList != null) && (eventsList.size() != 0))
								{
									int deviceId = eventsList.get(0).device;
									nrAction(deviceId);
									eventsNRList.clear();
								}
								eventsNRList.clear();
								nrDialog.dismiss();
								break;
							case NActionButton.VALUE_CANCEL:
								eventsNRList.clear();
								nrDialog.dismiss();
								break;
						}
					}
				});

				nrDialog.show();
			}

		}
	}

	private Cursor getNRCursor(Context context)
	{
		return DBHelper.getInstance(context).getNAllNRElementsCursorSiteOrder(eventsNRList);
	}
}
