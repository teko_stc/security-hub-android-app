package biz.teko.td.SHUB_APP.Devices_N.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.R;

final class DeviceViewHolder extends RecyclerView.ViewHolder {

    private DeviceViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public static DeviceViewHolder newInstance(ViewGroup parent) {
        return new DeviceViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.n_devices_list_element, parent, false));
    }
}
