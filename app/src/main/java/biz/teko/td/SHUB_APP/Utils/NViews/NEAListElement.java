package biz.teko.td.SHUB_APP.Utils.NViews;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import biz.teko.td.SHUB_APP.R;

public class NEAListElement extends LinearLayout
{
	private final Context context;
	private int layout;
	private boolean increase;
	private TextView titleTextView;
	private TextView classTextView;
	private OnElementClickListener onElementClickListener;

	private String title;
	private String c;
	private String location;
	private String time;
	private String date;
	private int image;
	private TextView locationTextView;
	private TextView timeTextView;
	private TextView dateTextView;
	private ImageView imageView;

	private NActionButton actionButton;
	private int actionButtonVisiblity = GONE;
	private int actionButtonImage = 0;

	private NTopInfoLayout.OnActionClickListener onActionClickListener;
	private boolean transition;

	public void setOnActionClickListener(NTopInfoLayout.OnActionClickListener onActionClickListener){
		this.onActionClickListener = onActionClickListener;
	}

	public interface OnActionClickListener
	{
		void onActionClick(int value);
	}

	public void setOnElementClickListener(OnElementClickListener onElementClickListener){
		this.onElementClickListener = onElementClickListener;
	}

	public interface OnElementClickListener{
		void onClick();
	}

	public NEAListElement(Context context)
	{
		super(context);
		this.context = context;
		setupView();
	}

	public NEAListElement(Context context, int layout)
	{
		super(context);
		this.context = context;
		this.layout = layout;
		setupView();
	}


	private void setupView()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(getContext());
		layoutInflater.inflate(layout, this, true);

		classTextView = (TextView) findViewById(R.id.nTextClass);
		titleTextView = (TextView) findViewById(R.id.nTextTitle);
		locationTextView = (TextView) findViewById(R.id.nTextLocation);
		timeTextView = (TextView) findViewById(R.id.nTextTime);
		dateTextView = (TextView) findViewById(R.id.nTextDate);

		imageView = (ImageView) findViewById(R.id.nImageEvent);

		actionButton = (NActionButton) findViewById(R.id.nButtonAction);
	}

	public void bindView(){
		setTitleText();
	}

	public NEAListElement(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setupView();
		bindView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NEAListElement, 0, 0);
		try{
			layout = a.getResourceId(R.styleable.NEAListElement_NEAListElementLayout, 0);
			title = a.getString(R.styleable.NEAListElement_NEAListElementTitle);
		}finally
		{
			a.recycle();
		}
	}

	private void setActionButton(){
		if(null!=actionButton){
			actionButton.setVisibility(actionButtonVisiblity);
			actionButton.setImage(actionButtonImage);
			actionButton.setIncrease(true);
			actionButton.setOnButtonClickListener(new NActionButton.OnButtonClickListener()
			{
				@Override
				public void onButtonClick(int action)
				{
					if(null!=onActionClickListener) onActionClickListener.onActionClick(action);
				}
			});
		}
	}

	private void setClassText()
	{
		if (null != classTextView && null != c && !c.equals("")) {
			classTextView.setText(Html.fromHtml(c));
			classTextView.setVisibility(VISIBLE);
		} else if (null != classTextView) {
			classTextView.setVisibility(GONE);
		}
	}

	private void setTitleText()
	{
		if(null!=titleTextView && null!=title){
			titleTextView.setText(Html.fromHtml(title));
		}
	}

	public TextView getTitleTextView(){
		return  titleTextView;
	}

	public void setClass(String c){
		this.c = c;
		setClassText();
	}

	public void setTitle(String title){
		this.title = title;
		setTitleText();
	}

	public void setLocation(String location){
		this.location = location;
		setLocationText();
	}

	private void setLocationText()
	{
		if(null!=locationTextView && null!=location){
			locationTextView.setText(location);
		}
	}

	public void setTime(String time){
		this.time = time;
		setTimeText();
	}

	private void setTimeText()
	{
		if(null!=timeTextView&&null!=time){
			timeTextView.setText(time);
		}
	}

	public void setDate(String date){
		this.date = date;
		setDateText();
	}

	private void setDateText()
	{
		if(null!=dateTextView && null!=date){
			dateTextView.setText(date);
		}
	}

	public void setImage(int image){
		this.image = image;
		setImageView();
	}

	private void setImageView()
	{
		if(null!=imageView && 0!=image){
			imageView.setImageDrawable(context.getResources().getDrawable(image));
		}
	}

	public void setIncrease(boolean b){
		this.increase = b;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				animateDown();
				return true;
			case MotionEvent.ACTION_CANCEL:
			case MotionEvent.ACTION_UP:
				animateUp();
				if(event.getAction() ==MotionEvent.ACTION_UP
						&& null!=onElementClickListener) {
					onElementClickListener.onClick();
				}
				return true;
			case MotionEvent.ACTION_MOVE:
				return false;
			default:
				animateUp();
		}
		return false;
	}

	private void animateDown() {
		AnimatorSet reducer = (AnimatorSet) AnimatorInflater.loadAnimator(context, increase ? R.animator.increase_size_1_2 : R.animator.reduce_size);
		reducer.setTarget(titleTextView.getParent());
		reducer.start();
	}

	private void animateUp() {
		AnimatorSet regainer = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.regain_size);
		regainer.setTarget(titleTextView.getParent());
		regainer.start();
	}

	public void setActionButtonVisiblity(int visiblity){
		this.actionButtonVisiblity = visiblity;
		setActionButton();
	}

	public void setActionButtonImage(int imageAction){
		this.actionButtonImage = imageAction;
		setActionButton();
	}

}
