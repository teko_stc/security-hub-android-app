package biz.teko.td.SHUB_APP.Profile.Security;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;

/**
 * Created by td13017 on 21.08.2017.
 */

public class SecurityActivity extends BaseActivity {
    private Context context;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security_preference);
        context = this;
        dbHelper = DBHelper.getInstance(context);
        initView();
        getFragmentManager().beginTransaction().replace(R.id.prefContainer, new SecurityPreferenceFragment()).commit();
    }

    private void initView() {
        NTopToolbarView toolbar = (NTopToolbarView) findViewById(R.id.toolbar);
        toolbar.setOnBackClickListener(this::onBackPressed);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
    }
}
