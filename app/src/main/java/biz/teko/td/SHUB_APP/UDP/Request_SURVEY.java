package biz.teko.td.SHUB_APP.UDP;

import java.nio.ByteBuffer;

/**
 * Created by td13017 on 11.01.2017.
 */

public class Request_SURVEY extends Request
{

	public final static byte Command = 0x00;

	public byte lock_tests;
	public int pin_code;
	public int magic;

	public Request_SURVEY(byte lock_tests, int pin_code){
		this.command = 0x00; //1
		this.lock_tests = lock_tests; //1
		this.pin_code = pin_code; //2
//		this.magic = 0xC07F1607; //4
//		this.magic = 0x07167fc0; //4
		this.magic = 0x73117a6e;
	}

	public byte[] getBytes(){
		byte[] buf = new byte[5];

		ByteBuffer byteBuffer = ByteBuffer.allocate(buf.length);
		byteBuffer.putInt(magic);
		byteBuffer.put(command);
//		byteBuffer.put(lock_tests);
//		byteBuffer.put(D3Service.beIntToByteArray(pin_code), 2, 2);
//		byteBuffer.putInt(magic);
		return  byteBuffer.array();
	}


}
