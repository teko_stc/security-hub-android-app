package biz.teko.td.SHUB_APP.Utils.NViews;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewAnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Other.TimerTextHelper;

public class NActionButton extends LinearLayout
{
	public static  final int[] STATE_DISABLED = {R.attr.state_dis};
	public static  final int[] STATE_SELECT = {R.attr.state_select};
	public static  final int[] STATE_HOLDED = {R.attr.state_holded};

	private OnButtonClickListener onButtonClickListener;
	private boolean is_pressed = false;
	private boolean is_ripple_showing = false;
	private boolean is_animation_showing =false;
	private boolean disabled = false;
	private boolean is_selected = false;
	private boolean is_holded = false;

	private boolean selectable = false;
	private boolean holdable = false;
	private OnButtonHoldListener onButtonHoldListener;

	private TimerTextHelper timeHelper;

	public interface OnButtonHoldListener{
		void onButtonHold(boolean is_pressed);
	}

	public void setOnButtonHoldListener(OnButtonHoldListener onButtonHoldListener){
		this.onButtonHoldListener = onButtonHoldListener;
	}

	public void setOnButtonClickListener(OnButtonClickListener onButtonClickListener){
		this.onButtonClickListener = onButtonClickListener;
	}

    public interface OnButtonClickListener{
		void onButtonClick(int action);
	}

//	/*не получается использовать enum, так как при свиче в дальнейшем нужны константы
//	надо поискать решение*/
//	public Value value;
//
//	public  enum Value {
//		on,
//		off,
//		info,
//		arm,
//		disarm,
//		reset
//	}

	public static final int VALUE_ON = 10001;
	public static final int VALUE_OFF = 10002;
	public static final int VALUE_INFO = 10003;
	public static final int VALUE_ARM = 10004;
	public static final int VALUE_DISARM = 10005;
	public static final int VALUE_RESET = 10006;



	public static final int VALUE_OK = 10010;
	public static final int VALUE_CANCEL = 10011;

	private final Context context;
	private int layout;
	private int action;
	private String title;
	private String subtitle;
	private int image;
	private int color;
	private int subTitleColor;
	private int size;
	private int sizeSubTitle;

	private ImageView imageView;
	private TextView titleView;
	private TextView subTitleView;
	private FrameLayout revealView;

	private boolean vibration;
	private boolean ripple;
	private boolean increase;
	private boolean not_animate;
	private FrameLayout innerView;

	public NActionButton(Context context)
	{
		super(context);
		this.context = context;
	}

	public NActionButton(Context context, int layout){
		super(context);
		this.context = context;
		this.layout = layout;
		setupView();
	}

	public NActionButton(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setupView();
		bindView();
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.NActionButton, 0, 0);
		try{
			layout = a.getResourceId(R.styleable.NActionButton_NActionButtonLayout, 0);
			image = a.getResourceId(R.styleable.NActionButton_NActionButtonImage, 0);
			action = a.getInt(R.styleable.NActionButton_NActionButtonAction, 0);
			title = a.getString(R.styleable.NActionButton_NActionButtonTitle);
			subtitle = a.getString(R.styleable.NActionButton_NActionButtonSubTitle);
			color = a.getResourceId(R.styleable.NActionButton_NActionButtonColor, R.color.n_text_dark_grey);
			subTitleColor = a.getResourceId(R.styleable.NActionButton_NActionButtonSubTitleColor, 0);
			size = a.getInt(R.styleable.NActionButton_NActionButtonTextSize, 15);
			sizeSubTitle = a.getInt(R.styleable.NActionButton_NActionButtonTextSubTitleSize, 0);
			increase = a.getBoolean(R.styleable.NActionButton_NActionButtonIncrease, false);
			vibration = a.getBoolean(R.styleable.NActionButton_NActionButtonVibrate, false);
			ripple = a.getBoolean(R.styleable.NActionButton_NActionButtonRipple, false);
			selectable = a.getBoolean(R.styleable.NActionButton_NActionButtonSelectable, false);
			holdable = a.getBoolean(R.styleable.NActionButton_NActionButtonHoldable, false);
			disabled = a.getBoolean(R.styleable.NActionButton_NActionButtonDisabled, false);
			//			value = Value.values()[a.getInt(R.styleable.NActionButton_NActionButtonValue, 0)];
		}finally
		{
			a.recycle();
		}
	}

	private void setupView()
	{
		LayoutInflater layoutInflater = LayoutInflater.from(context);
		layoutInflater.inflate(layout, this, true);

		imageView = (ImageView) findViewById(R.id.nImage);
		titleView = (TextView) findViewById(R.id.nTextTitle);
		subTitleView = (TextView) findViewById(R.id.nTextSubTitle);
//		revealView  = (FrameLayout) findViewById(R.id.nRevealView);
		innerView  = (FrameLayout) findViewById(R.id.nInnerView);
	}

	private void bindView()
	{
		if(null!=imageView) setImageView();
		if(null!=titleView) {
			setTitleView();
		}
		if(null!=subTitleView){
			setSubTitleView();
		}

		if(null!=revealView){
			setRevealView();
		}
	}

	private void setRevealView()
	{
		/*reveal works with delay */
//		FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) revealView.getLayoutParams();
//		lp.height = getHeight();
//		lp.width = getWidth();
//		revealView.setLayoutParams(lp);
//		revealView.setVisibility(VISIBLE);
//		revealView.callOnClick();
	}

	private void setTitleView()
	{
		if(null!=title && null!=titleView) {
			if(0!=color)titleView.setTextColor(context.getResources().getColorStateList(color));
			if(0!=size)titleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, size);
			if(null!=title)titleView.setText(title);
		}
	}

	private void setSubTitleView()
	{
		if(null!=subtitle && null!=subTitleView) {
			if(0!=subTitleColor)subTitleView.setTextColor(context.getResources().getColorStateList(subTitleColor));
			if(0!=sizeSubTitle)subTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, sizeSubTitle);
			if(null!=subtitle)subTitleView.setText(subtitle);
		}
	}

	private void setImageView()
	{
		if(null!=imageView){
			if(0!=image){
				imageView.setImageDrawable(getResources().getDrawable(image));
				imageView.setVisibility(VISIBLE);
			}else{
				imageView.setVisibility(GONE);
			}
		}
	}

	public void setText(int textResId) {
		if (titleView != null) titleView.setText(textResId);
	}

	public void setText(String text) {
		if (titleView != null) titleView.setText(text);
	}

	public void setTitle(String title){
		this.title = title;
		setTitleView();
	}

	public void setSubTitle(String subTitle){
		this.subtitle = subTitle;
		setSubTitleView();
	}

	public void setImage(int image){
		this.image = image;
		setImageView();
	}

	public void setTextColor(int color){
		this.color = color;
		setTitleView();
	}

	/*так как нет возможности менять цвет текста по кастомному параметру из-за особенностей textview,
	* который их не понимает*/
	public void setTitleEnable(boolean b){
		if(null!=titleView) titleView.setEnabled(b);
	}

	public void setTextSize(int size){
		this.size = size;
		setTitleView();
	}

	public void setTextSubTitleSize(int size){
		this.sizeSubTitle = size;
		setSubTitleView();
	}

	public void setAction(int action){
		this.action  = action;
	}

	public int getAction(){
		return this.action;
	}

	public void setIncrease(boolean increase){
		this.increase = increase;
	}


	public void setContolAnimate(boolean animate)
	{
		this.not_animate = animate;
	}

	/*кастомное отключение кнопки
	* с изменением цвета текста на ней*/
	public void setDisable(boolean disabled){
		this.disabled = disabled;
		if(null!=titleView)titleView.setEnabled(!disabled);
		refreshDrawableState();
	}

	public void setSelectable(boolean selectable){
		this.selectable = selectable;
	}

	private void switchSelection(){
		is_selected = !is_selected;
		refreshDrawableState();
	}

	public void setSelection(boolean is_selected){
		this.is_selected = is_selected;
		if(is_selected){
			startTime(titleView);
		}else{
			stopTime(titleView);
		}
		refreshDrawableState();
	}

	private void stopTime(TextView titleView)
	{
		if(null!=timeHelper) timeHelper.stop();
	}

	private void startTime(TextView titleView)
	{
		timeHelper = new TimerTextHelper(this);
		timeHelper.start();
	}

	public void setHoldable(boolean holdable){
		this.holdable = holdable;
	}

	private void setHolded(boolean holded){
		this.is_holded = holded;
		refreshDrawableState();
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 3);
		if(disabled) {
			mergeDrawableStates(drawableState, STATE_DISABLED);
		}
		if(is_selected)
			mergeDrawableStates(drawableState, STATE_SELECT);
		if(is_holded)
			mergeDrawableStates(drawableState, STATE_HOLDED);
		return drawableState;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if(!disabled)
		{
			switch (event.getAction())
			{
				case MotionEvent.ACTION_DOWN:
					is_pressed = !is_pressed;
					/*reveal works with delay */
					//				if(ripple) {
					//					setRevealView();
					//					startRipple();
					//				}
					if (!not_animate)
					{
						animateDown();
					}
					if(holdable)
					{
						holdAction();
					}
					return true;
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
					is_pressed = !is_pressed;
					//				if(ripple && !is_ripple_showing) rippleHide();
					if (!not_animate && !is_animation_showing)
					{
						animateUp();
					}
					if (event.getAction() == MotionEvent.ACTION_UP && null != onButtonClickListener)
					{
						if(isEnabled()) {
							buttonClick(action);
							if(selectable) switchSelection();
						}

					}
					if(holdable)
					{
						holdAction();
					};
					return true;
				case MotionEvent.ACTION_MOVE:
					return true;
				default:
					is_pressed = !is_pressed;
					//				if(ripple &&!is_ripple_showing) rippleHide();
					if (!not_animate && !is_animation_showing)
					{
						animateUp();
					}
					if(holdable)
					{
						holdAction();
					}
			}
		}
		return false;
	}

	private void holdAction()
	{
		is_holded = !is_holded;
		refreshDrawableState();
		if(null!=onButtonHoldListener) onButtonHoldListener.onButtonHold(is_holded);
	}

	private void buttonClick(int action)
	{
		onButtonClickListener.onButtonClick(action);
		if(vibration)((Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE)).vibrate(7);
	}

	private void animateUp()
	{
		AnimatorSet regainer = (AnimatorSet) AnimatorInflater.loadAnimator(context, R.animator.regain_size);
		regainer.setTarget(null!=innerView ? innerView : this);
		regainer.start();
	}

	private void animateDown()
	{
		is_animation_showing = true;

		Animator reducer = AnimatorInflater.loadAnimator(context, increase ? R.animator.increase_size_1_2 : R.animator.reduce_size);
		reducer.setTarget(null!=innerView ? innerView : this);
		reducer.addListener(new AnimatorListenerAdapter()
		{
			@Override
			public void onAnimationEnd(Animator animation)
			{
				super.onAnimationEnd(animation);
				is_animation_showing = false;
				if(!is_pressed) animateUp();
			}
		});
		reducer.start();
	}

	private void startRipple(){
		if(null!=revealView)
		{
			is_ripple_showing = true;
			revealView.setVisibility(VISIBLE);
			FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) revealView.getLayoutParams();
			lp.height = getHeight();
			lp.width = getWidth();
			revealView.setLayoutParams(lp);

			int cx = getWidth() / 2;
			int cy = getHeight() / 2;

			int finalRadius = Math.max(getMeasuredWidth(), getMeasuredHeight());
			int startRadius = Math.max(getMeasuredWidth()/2, getMeasuredHeight()/2);

			Animator anim = ViewAnimationUtils.createCircularReveal(revealView, cx, cy, 0 , finalRadius);
			anim.addListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationEnd(Animator animation)
				{
					super.onAnimationEnd(animation);
					is_ripple_showing = false;
					if(!is_pressed) rippleHide();
				}
			});
			anim.setDuration(300);
			anim.start();
		}
	}

	private void rippleHide(){
		if(null!=revealView)
			revealView.setVisibility(GONE);
	}

	public boolean is_pressed(){
		return is_pressed;
	}

}
