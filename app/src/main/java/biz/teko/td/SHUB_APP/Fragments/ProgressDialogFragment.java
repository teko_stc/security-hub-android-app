package biz.teko.td.SHUB_APP.Fragments;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;

public class ProgressDialogFragment extends DialogFragment {

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new NDialog(getContext(), R.layout.fragment_dialog_progressbar);
    }
}
