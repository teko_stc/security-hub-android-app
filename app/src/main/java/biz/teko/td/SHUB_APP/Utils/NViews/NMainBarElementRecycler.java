package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import biz.teko.td.SHUB_APP.R;

public class NMainBarElementRecycler extends NMainBarElement {
	public static final int STATE_ALARM = R.drawable.ic_alarm_overlay;
	public static final int STATE_ATTENTION = R.drawable.ic_attention_overlay;
	public static final int STATE_OFFLINE = R.drawable.ic_overlay_offline;
	public static final int STATE_DELAY_ARM = R.drawable.ic_delay_off;
	public static final int STATE_NO_CRIT = android.R.color.transparent;
	public static final int STATE_NO_ARM = android.R.color.transparent;

	public static final int STATE_ON = R.drawable.ic_on_overlay;
	public static final int STATE_OFF = R.drawable.ic_off_overlay;

	public static final int STATE_AUTO = R.drawable.ic_auto_overlay;
	public static final int STATE_SCRIPTED = R.drawable.ic_overlay_scripted;

	public NMainBarElementRecycler(Context context, int layout) {
		super(context, layout);
	}

	public NMainBarElementRecycler(Context context, @Nullable AttributeSet attrs) {
		super(context, attrs);
	}

	public void emptyStates() {

		((ImageView) findViewById(R.id.nImageBarStateOverlay)).setImageResource(android.R.color.transparent);

		((ImageView) findViewById(R.id.nImageBarOverlay)).setImageResource(android.R.color.transparent);
		((ImageView) findViewById(R.id.nImageBarRelayType)).setImageResource(android.R.color.transparent);

	}

	public void setAlarm(boolean no_arm) {

		((ImageView) findViewById(R.id.nImageBarStateOverlay)).setImageResource(STATE_ALARM);
	}

	public void setAttention(boolean attention) {
		((ImageView) findViewById(R.id.nImageBarStateOverlay)).setImageResource(STATE_ATTENTION);

	}

	public void setOffline(boolean offline) {
		((ImageView) findViewById(R.id.nImageBarStateOverlay)).setImageResource(STATE_OFFLINE);
	}

	public void setDelayArm(boolean delay_arm) {
		((ImageView) findViewById(R.id.nImageBarStateOverlay)).setImageResource(STATE_DELAY_ARM);
	}

	public void setNoArm(boolean no_arm) {
		((ImageView) findViewById(R.id.nImageBarOverlay)).setImageResource(STATE_NO_ARM);
	}

	public void setOn(boolean on) {
		((ImageView) findViewById(R.id.nImageBarOverlay)).setImageResource(STATE_ON);

	}

	public void setOff(boolean off) {
		((ImageView) findViewById(R.id.nImageBarOverlay)).setImageResource(STATE_OFF);
	}

	//only for relay
	public void setAuto(boolean auto) {
		((ImageView) findViewById(R.id.nImageBarRelayType)).setImageResource(STATE_AUTO);

	}

	//only for relay
	public void setScripted(boolean scripted) {


		((ImageView) findViewById(R.id.nImageBarRelayType)).setImageResource(STATE_SCRIPTED);
	}
}

