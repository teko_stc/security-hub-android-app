package biz.teko.td.SHUB_APP.Wizards.Activity.Controller;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Adapter.ClustersSpinnerAdapter;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Cluster;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class WizardControllerBindActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;
	private int site_id;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private int sending = 0;
	private boolean toCapture = false;
	private int cluster_id;
	private int errorCount = 0;

	private BroadcastReceiver assingResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", 0);
			if(result > 0){
				Intent intentFinish = new Intent(getBaseContext(), WizardControllerBindFinishActivity.class);
				intentFinish.putExtra("site_id", site_id);
				((Activity) context).finish();
				((Activity) context).startActivity(intentFinish);
				((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
			}else{
				errorCount++;
				if(errorCount > 5){
					Func.nShowMessage(context, Func.handleResult(context, result) + "\n" + getString(R.string.ATTETION_BUN_CHANCE));
				}else
				{
					Func.nShowMessage(context, Func.handleResult(context, result));
				}
			}
		}
	};

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_controller_bind);

		context = this;
		dbHelper = DBHelper.getInstance(context);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		site_id = getIntent().getIntExtra("site_id", -1);

		if(-1 != site_id){
			toolbar.setTitle(dbHelper.getSiteNameById(site_id));
		}
		toolbar.setSubtitle(R.string.WIZ_CONTR_BIND_TITLE);
		toolbar.setSubtitleTextColor(getResources().getColor(R.color.brandColorWhite));
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
				overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
			}
		});

		serviceConnection = new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		final EditText editSerial = (EditText) findViewById(R.id.wizContrNumber);
		final Button next = (Button) findViewById(R.id.contrSetNextButton);

		ImageView qrImage = (ImageView) findViewById(R.id.imageButtonQR);

		editSerial.setFocusable(true);
		editSerial.setOnEditorActionListener(new TextView.OnEditorActionListener()
		{
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
			{
				if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
				{
					editSerial.clearFocus();
					Func.hideKeyboard((WizardControllerBindActivity) context);
					return true;
				}
				return false;
			}
		});


		qrImage.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
				{
					toCapture = true;
					IntentIntegrator integrator = new IntentIntegrator(WizardControllerBindActivity.this);
					integrator.setOrientationLocked(false);
					integrator.setBeepEnabled(false);
					integrator.initiateScan(Const.BARCODE_CODES);
				}else{
					Func.nShowMessage(context, getString(R.string.ERROR_CANT_SCAN_NO_CAMERA_HARDWARE));
				}
			}
		});

		Spinner clustersSpinner = (Spinner) findViewById(R.id.wiz_clusters_spinner);
		Cluster[] clusters = dbHelper.getClusters();
		if(null!= clusters)
		{
			final ClustersSpinnerAdapter clustersSpinnerAdapter = new ClustersSpinnerAdapter(context, R.layout.spinner_item_dropdown, clusters);
			clustersSpinner.setAdapter(clustersSpinnerAdapter);
			clustersSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
			{
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
				{
					Cluster cluster = clustersSpinnerAdapter.getItem(position);
					cluster_id = cluster.id;
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent)
				{

				}
			});
		}

		next.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if (0!=editSerial.getText().length())
				{
					UserInfo userInfo = dbHelper.getUserInfo();
					long i = Long.valueOf(editSerial.getText().toString());
					if (i >= 10000)
					{
						long sn = i / 10000;
						long pn = i % 10000;
						JSONObject message = new JSONObject();
						try
						{
							message.put("site_id", site_id);
							message.put("account", sn);
							message.put("pincode", pn);
							message.put("domain_id", userInfo.domain);
							message.put("cluster_id", cluster_id);
							message.put("section", 0);


						} catch (JSONException e)
						{
							e.printStackTrace();
						}
						D3Request d3Request = D3Request.createMessage(userInfo.roles, "DEVICE_FORCE_ASSIGN", message);
						if(d3Request.error == null){
							sendMessageToServer(d3Request);
						}else{
							Func.pushToast(context, d3Request.error, (Activity) context);
						}
					} else
					{
						Func.pushToast(context, getString(R.string.CAA_ERROR_SN), (WizardControllerBindActivity) context);
					}
				} else
				{
					Func.pushToast(context, getString(R.string.CAA_ENTER_SN), (WizardControllerBindActivity) context);
				}
			}
		});
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unregisterReceiver(assingResultReceiver);
		if(!toCapture){
			((App)this.getApplication()).startActivityTransitionTimer(context);
		}
		((App)this.getApplication()).stopActivityTransitionTimer();
		toCapture = false;
		unbindService(serviceConnection);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		registerReceiver(assingResultReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_FORCE_ASSIGN));
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}

	private boolean sendCommandToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				} else
				{
					sendIntentCommandBeenSend(-1);
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};

	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		super.onActivityResult(requestCode, resultCode, intent);
		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
		if (scanResult != null) {
			final EditText editSerial = (EditText) findViewById(R.id.wizContrNumber);
			String re = scanResult.getContents();
			if(null!=re)
			{
				int i = re.length();
				if (3 < i)
				if (3 < i && Func.checkLC2o5(re))
				{
					re = re.substring(0, re.length() - 1);
					final String sn = re.substring(1);
					Log.e("D3Scan", re);
					editSerial.setText(sn);
					editSerial.requestFocus();
					editSerial.setSelection(sn.length());
				}
			}

		}
	}
}
