package biz.teko.td.SHUB_APP.D3.D3XProtoEvent;

import android.content.Context;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

public class DWIType
{
	@Attribute(name = "id")
	public int id;

	@Attribute(name = "caption")
	public String caption;

	@ElementList(name = "detectortypes", entry = "detectortype", required = false)
	public LinkedList<DType> detectortypes;

	@ElementList(name = "icons", entry = "icon", required = false)
	public LinkedList<Icon> icons;

	public DType getDetectorType(int id){
		LinkedList<DType> detectortypes = this.detectortypes;
		if(null!=detectortypes)
		for(DType dType :detectortypes){
			if(dType.id == id){
				return dType;
			}
		}
		return null;
	}

	public DType getDefDetectorType(){
		LinkedList<DType> detectortypes = this.detectortypes;
		if(null!=detectortypes)
		for(DType dType :detectortypes){
			if(dType.def == 1){
				return dType;
			}
		}
		return null;
	}

	public int getListIcon(Context context)
	{
		return context.getResources().getIdentifier(this.icons.get(0).src, null, context.getPackageName());
	}

	public int getMainIcon(Context context)
	{
		return context.getResources().getIdentifier(this.icons.get(1).src, null, context.getPackageName());
	}

	public int getBigIcon(Context context)
	{
		return context.getResources().getIdentifier(this.icons.get(2).src, null, context.getPackageName());
	}
}
