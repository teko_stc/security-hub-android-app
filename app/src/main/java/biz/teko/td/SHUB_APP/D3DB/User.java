package biz.teko.td.SHUB_APP.D3DB;

import android.database.Cursor;

public class User
{
	public int id, device;
	public String name, comment, phone;
	public String sections;

	public User(){}

	public User(int id, int device, String name, String comment, String phone, String sections){
		this.id = id;
		this.device = device;
		this.name = name;
		this.comment = comment;
		this.phone = phone;
		this.sections = sections;
	}

	public User(Cursor cursor){
		this.id = cursor.getInt(1);
		this.device = cursor.getInt(2);
		this.name = cursor.getString(3);
		this.comment = cursor.getString(4);
		this.phone = cursor.getString(5);
		this.sections = cursor.getString(6);

	}
}
