package biz.teko.td.SHUB_APP.Wizards.Activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.NCameraDahuaAddActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.CamerasLoginActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Delegation.Activities.DelegateAddActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptsLibraryActivity;
import biz.teko.td.SHUB_APP.Scripts.Adapters.NElementSelectAdapter;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDefaultListElement;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.NWizardSectionActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardExitSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardRelaySetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardUserSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardZoneSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerBindActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.Controller.WizardControllerSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Activity.DomainUsers.WizardDomainUserSetActivity;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner.WizardSTypesSpinnerAdapter;

public class WizardAllActivity extends BaseActivity
{
	public  static final int SITE = 0;
	public  static final int DEVICE = 1;
	public  static final int SECTION = 2;
	public  static final int ZONE = 3;
	public  static final int WL_RELAY = 4;
	public  static final int INPUT = 5;
	public  static final int OUTPUT = 6;
	public  static final int WD_RELAY = 7;
	public  static final int DOMAIN_USER = 8;
	public  static final int USER = 9;
	public  static final int SCRIPT = 10;


	public  static final long SECTION_WAITING_TIME = 5000;

	private LinearLayout closeButton;
	private LinearLayout backButton;
	private LinearLayout nextButton;
	private NDefaultListElement newSiteButton;
	private NDefaultListElement delegSiteButton;
	private NDefaultListElement newDeviceButton;
	private NDefaultListElement newZoneButton;
	private NDefaultListElement newSectionButton;
	private NDefaultListElement newInputButton;
	private NDefaultListElement newOutputButton;
	private NDefaultListElement newScript;
	private NDefaultListElement newDomainUserButton;
	private NDefaultListElement newCodeButton;
	private NDefaultListElement newWLRelayButton;
	private NDefaultListElement newWDRelayButton;
	private NDefaultListElement newCameraButton;

	private Context context;
	private DBHelper dbHelper;
	private UserInfo userInfo;
	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private final int sending = 0;
	private int type;

	private NDialog waitDialog;
	private boolean wait;
	private Timer timer;
	private int device_id;

	private Section savedSection = null;
	private boolean waiting4section = false;
	private final Handler sectionAddHandler = new Handler();
	private final Runnable sectionAddRunnable = new Runnable()
	{
		public void run()
		{
			waiting4section = false;
		}
	};

	private NBottomSheetDialog cameraTypeSetDialog;

	private final BroadcastReceiver sectionAddReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int device = intent.getIntExtra("device", 0);
			int detector = intent.getIntExtra("detector", -1);
			int zone = intent.getIntExtra("zone", 0);
			if(null!=savedSection && waiting4section
					&& savedSection.device_id == device
					&& savedSection.detector == detector
					&& 0 == zone){
				waiting4section = false;
				sectionAddHandler.removeCallbacks(sectionAddRunnable);
				Func.nShowSuccessMessage(context);
			}
		}
	};

	private final BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private final BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				switch (sending)
				{
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1)
						{
							Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
						} else
						{
							Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};

	/*WHEN we get new safe session we open login iv page to login in their system*/
	private final BroadcastReceiver openIVLoginWebPageAfterGetSaveSession = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(null!=waitDialog && waitDialog.isShowing())
			{
				if(wait)
				{
					wait = false;
					if (null != timer) timer.cancel();
					String session = intent.getStringExtra("session");
					if (null != session)
					{
						Intent webIntent = new Intent(getBaseContext(), CamerasLoginActivity.class);
						webIntent.putExtra("session", session);
						startActivityForResult(webIntent, Const.REQUEST_IV_LOGIN);
					}
				}else{
					Func.nShowMessage(context, getString(R.string.N_WIZ_ALL_IV_UNAVAILABLE));
				}
				waitDialog.dismiss();
			}
		}
	};

	private final BroadcastReceiver libraryScriptsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if (waitDialog != null && waitDialog.isShowing())
			{
				waitDialog.dismiss();
				if (wait)
				{
					wait = false;
					if (null != timer) timer.cancel();
					openLibrarylist();
				}else{
					Func.nShowMessage(context, getString(R.string.SCRIPT_ERROR_CANT_LOAD_LIB));
				}
				device_id = 0;
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{

		super.onCreate(savedInstanceState);
		boolean transparent = getIntent().getBooleanExtra("transparent", false);
		setContentView(R.layout.activity_wizard_all);

		this.context=this;
		dbHelper = DBHelper.getInstance(context);
		userInfo  = dbHelper.getUserInfo();

		setup();
		bind();
		if (transparent)
			showCameraTypeSelect();
	}

	private void bind()
	{

		if(null!=closeButton)closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				finish();
			}
		});

		if(null!=backButton)backButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		if(null!=newSiteButton) newSiteButton.setOnRootClickListener(() -> {
			Intent intent = new Intent(getBaseContext(), WizardControllerSetActivity.class);
			intent.putExtra("from", 1);
			startActivity(intent);
			overridePendingTransition(R.animator.enter, R.animator.exit);
		});

		if(null!=delegSiteButton) delegSiteButton.setOnRootClickListener(() -> {
			Intent intent = new Intent(getBaseContext(), DelegateAddActivity.class);
			context.startActivity(intent);
			((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
		});
		if(null!=newDeviceButton) newDeviceButton.setOnRootClickListener(() -> showElementChoose(DEVICE));
		if(null!=newZoneButton) newZoneButton.setOnRootClickListener(() -> showElementChoose(ZONE));
		if(null!=newWLRelayButton) newWLRelayButton.setOnRootClickListener(() -> showElementChoose(WL_RELAY));
		if(null!=newWDRelayButton) newWDRelayButton.setOnRootClickListener(() -> showElementChoose(WD_RELAY));
		if(null!=newSectionButton) {
			newSectionButton.setOnRootClickListener(() -> showElementChoose(SECTION));
		}
		if(null!=newInputButton) {
			newInputButton.setOnRootClickListener(new NDefaultListElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					showElementChoose(INPUT);
				}
			});
		}
		if(null!=newOutputButton) {
			newOutputButton.setOnRootClickListener(new NDefaultListElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					showElementChoose(OUTPUT);
				}
			});
		}

		if(null!=newScript){
			newScript.setOnRootClickListener(new NDefaultListElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					showElementChoose(SCRIPT);
				}
			});
		}

		if(null!=newDomainUserButton) {
			newDomainUserButton.setOnRootClickListener(new NDefaultListElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					Intent activityIntent = new Intent(getBaseContext(), WizardDomainUserSetActivity.class);
					startActivityForResult(activityIntent, Const.ADD_DOMAIN_USER);
				}
			});
		}
		if(null!=newCodeButton) newCodeButton.setOnRootClickListener(new NDefaultListElement.OnRootClickListener()
		{
			@Override
			public void onRootClick()
			{
				showElementChoose(USER);
			}
		});

		if(null!=newCameraButton) newCameraButton.setOnRootClickListener(new NDefaultListElement.OnRootClickListener()
		{
			@Override
			public void onRootClick()
			{
				showCameraTypeSelect();
			}
		});
	}

	private void setup()
	{
		closeButton = (LinearLayout) findViewById(R.id.nButtonClose);
		backButton = (LinearLayout) findViewById(R.id.nButtonBack);
		nextButton = (LinearLayout) findViewById(R.id.mainSetButtonNext);

		newSiteButton = (NDefaultListElement) findViewById(R.id.nWASiteNew);
		delegSiteButton = (NDefaultListElement) findViewById(R.id.nWASiteDeleg);
		newDeviceButton = (NDefaultListElement) findViewById(R.id.nWADevice);
		newZoneButton = (NDefaultListElement) findViewById(R.id.nWAZone);
		newWLRelayButton = (NDefaultListElement) findViewById(R.id.nWAWLRelay);
		newCameraButton = (NDefaultListElement) findViewById(R.id.nWACamera);

		newSectionButton = (NDefaultListElement) findViewById(R.id.nWASection);
		newInputButton = (NDefaultListElement) findViewById(R.id.nWAWiredInput);
		newOutputButton = (NDefaultListElement) findViewById(R.id.nWAWiredOutput);
		newWDRelayButton = (NDefaultListElement) findViewById(R.id.nWAWDRelay);
		newScript = (NDefaultListElement) findViewById(R.id.nWAScript);
		newDomainUserButton = (NDefaultListElement) findViewById(R.id.nWADomainUser);
		newCodeButton = (NDefaultListElement) findViewById(R.id.nWACode);
	}

	private void showElementChoose(int type)
	{
		final NBottomSheetDialog deviceSetDialog = new NBottomSheetDialog(context);
		View bottomView = ((Activity) context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_biggest, null);
		TextView title = bottomView.findViewById(R.id.dialogBottomTitle);
		ListView listView = bottomView.findViewById(R.id.dialogBottomList);

		D3Element[] elements = null;
		boolean exist_nes = true;

		String titleText= getString(R.string.N_WIZARD_ALL_SELECT_CONTROLLER);

		NElementSelectAdapter.OnItemClickListener clickListener = null;
		switch (type){
			case DEVICE:
				elements = dbHelper.getAllSitesArray();
				if(null!=elements && elements.length > 0)
				{
					titleText = getString(R.string.N_WIZARD_ALL_SELECT_SITE);
					clickListener = new NElementSelectAdapter.OnItemClickListener()
					{
						@Override
						public void onClick(int id)
						{
							if (0 != id)
							{
								Intent intent = new Intent(getBaseContext(), WizardControllerBindActivity.class);
								intent.putExtra("site_id", id);
								startActivityForResult(intent, D3Service.ADD_NEW_DEVICE);
								((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
								deviceSetDialog.dismiss();
							}
						}
					};
				}else{
					exist_nes = false;
					titleText = getString(R.string.N_WA_NO_SITES_TO_SET_DEVICE);
				}
				break;
			case SECTION:
				elements = dbHelper.getAllSHDevices();
				if(null!=elements && elements.length > 0)
				{
					clickListener = new NElementSelectAdapter.OnItemClickListener()
					{
						@Override
						public void onClick(int id)
						{
							if (id > 0)
							{
//								showSectionAddDialog(id);
								Intent intent = new Intent(getBaseContext(), NWizardSectionActivity.class);
								intent.putExtra("device_id", id);
								startActivityForResult(intent, D3Service.ADD_SECTION_IN_DEVICE);
								overridePendingTransition(R.animator.enter, R.animator.exit);
								deviceSetDialog.dismiss();
							} else
							{
								switch (id)
								{
									case Const.STATUS_ARMED:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM));
										break;
									case Const.STATUS_PARTLY_ARMED:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM_FROM_PARTLY));
										break;
									case Const.STATUS_OFFLINE:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NO_CONNECTION));
										break;
								}
							}
						}
					};
				}else{
					exist_nes = false;
					titleText = getString(R.string.N_WA_NO_DEVICES_TO_SET_ELEMENTS);
				}
				break;
			case WL_RELAY:
			case WD_RELAY:
				elements = dbHelper.getAllSHDevices();
				if(null!=elements && elements.length >0)
				{
					clickListener = new NElementSelectAdapter.OnItemClickListener()
					{
						@Override
						public void onClick(int id)
						{
							if (0 < id)
							{
								Intent intent = new Intent(getBaseContext(), WizardRelaySetActivity.class);
								intent.putExtra("device_id", id);
								intent.putExtra("con_type", type == WL_RELAY ? 0 : 1);
								startActivity(intent);
								overridePendingTransition(R.animator.enter, R.animator.exit);
								deviceSetDialog.dismiss();
							} else
							{
								switch (id)
								{
									case Const.STATUS_ARMED:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM));
										break;
									case Const.STATUS_PARTLY_ARMED:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM_FROM_PARTLY));
										break;
									case Const.STATUS_OFFLINE:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NO_CONNECTION));
										break;
								}
							}
						}
					};
				}else{
					exist_nes = false;
					titleText = getString(R.string.N_WA_NO_DEVICES_TO_SET_ELEMENTS);
				}
				break;
			case ZONE:
				elements = dbHelper.getAllSHDevices();
				if(null!=elements && elements.length > 0)
				{
					clickListener = new NElementSelectAdapter.OnItemClickListener()
					{
						@Override
						public void onClick(int id)
						{
							if (0 != id)
							{
								if (0 < id)
								{
									Intent intent = new Intent(getBaseContext(), WizardZoneSetActivity.class);
									intent.putExtra("device_id", id);
									intent.putExtra("con_type", Const.WIRELESS);
									context.startActivity(intent);
									((Activity) context).overridePendingTransition(R.animator.enter, R.animator.exit);
									deviceSetDialog.dismiss();
								} else
								{
									switch (id)
									{
										case Const.STATUS_ARMED:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM));
											break;
										case Const.STATUS_PARTLY_ARMED:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM_FROM_PARTLY));
											break;
										case Const.STATUS_OFFLINE:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NO_CONNECTION));
											break;
									}
								}
							}
						}
					};
				}else{
					exist_nes = false;
					titleText = getString(R.string.N_WA_NO_DEVICES_TO_SET_ELEMENTS);
				}
				break;
			case INPUT:
				elements = dbHelper.getAllSHDevices();
				if(null!=elements && elements.length >0)
				{
					clickListener = new NElementSelectAdapter.OnItemClickListener()
					{
						@Override
						public void onClick(int id)
						{
							if (id > 0)
							{
								Intent intent = new Intent(getBaseContext(), WizardZoneSetActivity.class);
								intent.putExtra("device_id", id);
								intent.putExtra("con_type", Const.WIRED);
								startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
								overridePendingTransition(R.animator.enter, R.animator.exit);
								deviceSetDialog.dismiss();
							} else
							{
								switch (id)
								{
									case Const.STATUS_ARMED:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM));
										break;
									case Const.STATUS_PARTLY_ARMED:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM_FROM_PARTLY));
										break;
									case Const.STATUS_OFFLINE:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NO_CONNECTION));
										break;
								}
							}
						}
					};
				}else{
					exist_nes = false;
					titleText = getString(R.string.N_WA_NO_DEVICES_TO_SET_ELEMENTS);
				}
				break;
			case OUTPUT:
				elements = dbHelper.getAllSHDevices();
				if(null!=elements && elements.length > 0)
				{
					clickListener = new NElementSelectAdapter.OnItemClickListener()
					{
						@Override
						public void onClick(int id)
						{
							if (id > 0)
							{
								Intent intent = new Intent(getBaseContext(), WizardExitSetActivity.class);
								intent.putExtra("device_id", id);
								startActivityForResult(intent, D3Service.ADD_ZONE_IN_SECTION);
								overridePendingTransition(R.animator.enter, R.animator.exit);
								deviceSetDialog.dismiss();
							} else
							{
								switch (id)
								{
									case Const.STATUS_ARMED:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM));
										break;
									case Const.STATUS_PARTLY_ARMED:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM_FROM_PARTLY));
										break;
									case Const.STATUS_OFFLINE:
										Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NO_CONNECTION));
										break;
								}
							}
						}
					};
				}else{
					exist_nes = false;
					titleText = getString(R.string.N_WA_NO_DEVICES_TO_SET_ELEMENTS);
				}
				break;
			case SCRIPT:
				elements = dbHelper.getAllSHDevices();
				if(null!=elements && elements.length > 0)
				{
					clickListener = new NElementSelectAdapter.OnItemClickListener()
					{
						@Override
						public void onClick(int id)
						{
							if (0 != id)
							{
								if (0 < id)
								{
									getLibraryScripts(id);
									deviceSetDialog.dismiss();
								} else
								{
									switch (id)
									{
										case Const.STATUS_ARMED:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM));
											break;
										case Const.STATUS_PARTLY_ARMED:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM_FROM_PARTLY));
											break;
										case Const.STATUS_OFFLINE:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NO_CONNECTION));
											break;
									}
								}
							}
						}
					};
				}else{
					exist_nes = false;
					titleText = getString(R.string.N_WA_NO_DEVICES_TO_SET_ELEMENTS);
				}
				break;
			case USER:
				elements = dbHelper.getAllSHDevices();
				if(null!=elements && elements.length > 0)
				{
					clickListener = new NElementSelectAdapter.OnItemClickListener()
					{
						@Override
						public void onClick(int id)
						{
							if (0 != id)
							{
								if (0 < id)
								{
									Intent intent = new Intent(getBaseContext(), WizardUserSetActivity.class);
									intent.putExtra("device_id", id);
									startActivity(intent);
									overridePendingTransition(R.animator.enter, R.animator.exit);
									deviceSetDialog.dismiss();
								} else
								{
									switch (id)
									{
										case Const.STATUS_ARMED:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM));
											break;
										case Const.STATUS_PARTLY_ARMED:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NEED_DISARM_FROM_PARTLY));
											break;
										case Const.STATUS_OFFLINE:
											Func.nShowMessage(context, getString(R.string.N_WIZARD_ALL_NO_CONNECTION));
											break;
									}
								}
							}
						}
					};
				}else{
					exist_nes = false;
					titleText = getString(R.string.N_WA_NO_DEVICES_TO_SET_ELEMENTS);
				}
				break;
			default:
				break;
		}

		if(exist_nes)
		{
			NElementSelectAdapter nElementSelectAdapter = new NElementSelectAdapter(context, R.layout.n_devices_list_element, elements);
			nElementSelectAdapter.setOnItemClickListener(clickListener);
			listView.setAdapter(nElementSelectAdapter);

			title.setText(titleText);

			deviceSetDialog.setContentView(bottomView);
			deviceSetDialog.show();
		}else{
			Func.nShowMessage(context, titleText);
		}
	}

	private void showCameraTypeSelect()
	{
		cameraTypeSetDialog = new NBottomSheetDialog(context);
		View dialogView = getLayoutInflater().inflate(R.layout.dialog_bottom_with_linear, null);
		TextView title = dialogView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.N_CAMERA_TYPE_SELECT);

		LinearLayout dialogFrame = dialogView.findViewById(R.id.nBottomDialogLinearFrame);

		if(null!=dialogView && null!=dialogFrame){
			if(Func.needCamerasForBuild())
			{
				NWithAButtonElement ivButton = new NWithAButtonElement(context, R.layout.n_cameras_type_select_element_layout);
				ivButton.setTitle("iVideon");
				String iv_token = dbHelper.getUserInfoIVToken();
				if (null != iv_token && !iv_token.equals(""))
				{
					ivButton.setSubtitle(getString(R.string.N_WIZ_ALL_IV_ALREADY_BINDED));
					ivButton.setTitleColor(R.color.n_brand_blue);
					ivButton.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_small);
							nDialog.setTitle(getString(R.string.N_WIZ_ALL_IV_BIND_Q));
							nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
							{
								@Override
								public void onActionClick(int value, boolean b)
								{
									switch (value)
									{
										case NActionButton.VALUE_OK:
											UserInfo userInfo = dbHelper.getUserInfo();
											JSONObject message = new JSONObject();
											try
											{
												message.put("domain", userInfo.domain);
												nSendMessageToServer(null, D3Request.IV_DEL_USER, message, false);
											} catch (JSONException e)
											{
												e.printStackTrace();
											}
											nDialog.dismiss();
											break;
										case NActionButton.VALUE_CANCEL:
											nDialog.dismiss();
											break;
									}
								}
							});
							nDialog.show();
							cameraTypeSetDialog.dismiss();
						}
					});

				} else
				{
					ivButton.setSubtitle(getString(R.string.N_WIZ_ALL_IV_BIND_PRESS));
					ivButton.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							cameraTypeSetDialog.dismiss();
							loginIV();
						}
					});

				}
				ivButton.setBackground(R.drawable.n_background_devices_list_selector);

				dialogFrame.addView(ivButton);
			}

			NWithAButtonElement rtspButton = new NWithAButtonElement(context, R.layout.n_cameras_type_select_element_layout);
			rtspButton.setTitle(getString(R.string.N_WIZ_ALL_IP_CAM));
			rtspButton.setSubtitle(getString(R.string.N_WIZ_ALL_RTSP_SET_PRESS));
			rtspButton.setEnabled(true);
			rtspButton.setBackground(R.drawable.n_background_devices_list_selector);
			rtspButton.setOnRootClickListener(new NWithAButtonElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					showRTSPAddDialog();
					cameraTypeSetDialog.dismiss();
				}
			});

			dialogFrame.addView(rtspButton);

			NWithAButtonElement dahuaButton = new NWithAButtonElement(context, R.layout.n_cameras_type_select_element_layout);
			dahuaButton.setTitle("Dahua");
			dahuaButton.setSubtitle(getString(R.string.N_DAHUA_SET_BOTTOM_SUBTITLE));
			dahuaButton.setEnabled(true);
			dahuaButton.setBackground(R.drawable.n_background_devices_list_selector);
			dahuaButton.setOnRootClickListener(() -> {
				openDahua();
				cameraTypeSetDialog.dismiss();
			});

			dialogFrame.addView(dahuaButton);


			cameraTypeSetDialog.setContentView(dialogView);
			cameraTypeSetDialog.show();


		}

	}

	private void openDahua()
	{
		Intent intent = new Intent(context, NCameraDahuaAddActivity.class);
		startActivity(intent);
		overridePendingTransition(R.animator.enter, R.animator.exit);
	}

	private void loginIV()
	{
		runOnUiThread(() -> {
			waitDialog = new NDialog(context, R.layout.n_dialog_progress);
			waitDialog.setCancelable(false);
			waitDialog.setTitle(getString(R.string.DIALOG_WAIT_TITLE));
			waitDialog.show();
		});
		wait = true;
		getSafeSession();
		timer = new Timer();
		timer.schedule(new TimerTask()
		{
			@Override
			public void run()
			{
				if(wait)
				{
					wait = false;
					sendBroadcast(new Intent(D3Service.BROADCAST_OPERATOR_SAFE_SESSION));
				}
			}
		}, 15000);
	}

	private void getSafeSession()
	{
		nSendMessageToServer(null, D3Request.OPERATOR_SAFE_SESSION, new JSONObject(), false);
	}


	private void showRTSPAddDialog()
	{
		NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_add);
		nDialog.setTitle(getString(R.string.N_WIZ_ALL_RTSP_NEW));
		View view = getLayoutInflater().inflate(R.layout.n_rtsp_add_veiw, null);
		nDialog.addView(view);

		NEditText editName = view.findViewById(R.id.nEditName);
		NEditText editLink = view.findViewById(R.id.nEditLink);
		TextInputLayout editNameInput = view.findViewById(R.id.nEditNameInput);
		TextInputLayout editLinkInput = view.findViewById(R.id.nEditLinkInput);
		NActionButton buttonPaste = view.findViewById(R.id.nButtonPaste);

		if(null!=editName)
		{
			editName.setFocusableInTouchMode(true);
			editName.setText("");
			editName.setSelection(editName.getText().length());
			editName.showKeyboard();
			editName.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					editNameInput.setError(null);
				}

				@Override
				public void afterTextChanged(Editable s)
				{

				}
			});
			editName.setOnKeyListener(new View.OnKeyListener()
			{
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if((event.getAction() == KeyEvent.ACTION_DOWN)&& (keyCode == KeyEvent.KEYCODE_ENTER)){
						editLink.setSelection(editLink.getText()!=null ? editLink.getText().toString().length() : 0);
					}
					return false;
				}
			});
		}
		if(null!=editLink){
			editLink.setFocusableInTouchMode(true);
			editLink.addTextChangedListener(new TextWatcher()
			{
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after)
				{

				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count)
				{
					editLinkInput.setError(null);
				}

				@Override
				public void afterTextChanged(Editable s)
				{
					String result = s.toString().replaceAll(" ", "");
					if (!s.toString().equals(result))
					{
						editLink.setText(result);
						editLink.setSelection(result.length());
					}
				}
			});
			editLink.setOnKeyListener(new View.OnKeyListener()
			{
				@Override
				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
					{
						if (validate(editName))
						{
							if(validate(editLink,"rtsp://")){
								nDialog.dismiss();
								addCameraRTSP(editName, editLink);
							}else{
								editLinkInput.setError(getString(R.string.N_WIZ_ALL_RTSP_LINK_ERROR));
							}
						}else{
							editName.showKeyboard();
							editNameInput.setError(" ");
						}
						return true;
					}
					return false;
				}
			});
		}
		if(null!=buttonPaste){
			buttonPaste.setOnButtonClickListener(new NActionButton.OnButtonClickListener()
			{
				@Override
				public void onButtonClick(int action)
				{
					ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
					ClipData clipData = clipboard.getPrimaryClip();
					if(null!=clipData && clipData.getItemCount()!=0)
					{
						String buffer = clipData.getItemAt(0).getText().toString();
						if(null!=editLink)editLink.setText(buffer);
					}else{
						Func.pushToast(context, getString(R.string.BUFFER_IS_EMPTY), (WizardAllActivity) context);
					}
				}
			});
		}

		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value)
				{
					case NActionButton.VALUE_OK:
						if (validate(editName))
						{
							if(validate(editLink,"rtsp://")){
								nDialog.dismiss();
								addCameraRTSP(editName, editLink);
							}else{
								editLinkInput.setError(getString(R.string.N_WIZ_ALL_RTSP_LINK_ERROR));
							}
						}else{
							editName.showKeyboard();
							editNameInput.setError(" ");
						}						break;
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}
		});
		nDialog.show();

	}

	private void addCameraRTSP(NEditText editName, NEditText editLink)
	{
		Camera camera = new Camera();
		camera.name = editName.getText().toString();
		camera.link = editLink.getText().toString();
		if(dbHelper.addRTSPCamera(camera)){
			Func.nShowSuccessMessage(context);
		}else{

		}
	}

	private boolean validate(NEditText name)
	{
		return (null!=name.getText() && !name.getText().toString().equals(""));
	}

	private boolean validate(NEditText link, String s){
		if(null!=link.getText() && !link.getText().toString().equals("")){
			try{
				String arg  = link.getText().toString().substring(0, 7);
				return null != arg && arg.equals(s);
			}catch (Exception e){
				return false;
			}
		}else{
			return false;
		}
	}

	private void showSectionAddDialog(final int deviceId)
	{
		NDialog nDialog = new NDialog(context,R.layout.n_dialog_question_add);

		nDialog.setTitle(getResources().getString(R.string.SETA_ADD_SECTION_TITLE));

		View sectionView = getLayoutInflater().inflate(R.layout.n_section_add_view, null);
		NEditText editName = sectionView.findViewById(R.id.editName);
		if(null!=editName)
		{
			editName.setFocusableInTouchMode(true);
			editName.setText("");
			editName.setSelection(editName.getText().length());
			editName.setOnKeyListener(new View.OnKeyListener()
			{

				public boolean onKey(View v, int keyCode, KeyEvent event)
				{
					return (event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER);
				}
			});
			editName.showKeyboard();
		}

		final int[] type = new int[1];
		if(null!=D3Service.d3XProtoConstEvent)
		{
			LinkedList<SType> sTypesList = D3Service.d3XProtoConstEvent.sectiontypes;
			LinkedList<SType> sTypes = new LinkedList<SType>();
			Device device = dbHelper.getDeviceById(deviceId);
			if(device.canBeSetFromApp())
			{
				for (SType sType: sTypesList)
				{
					if(null!=sType)
					{
						if (sType.id != Const.SECTIONTYPE_FIRE_DOUBLE && sType.id != Const.SECTIONTYPE_TECH_CONTROL)
						{
							sTypes.add(sType);
						}
					}
				}
			}else{
				sTypes = sTypesList;
			}

			Spinner sTypeSpinner = sectionView.findViewById(R.id.wizSectionTypeSpinner);
			if (null != sTypeSpinner)
			{
				final WizardSTypesSpinnerAdapter wizardSTypesSpinnerAdapter = new WizardSTypesSpinnerAdapter(context, R.layout.spinner_item_dropdown, sTypes);
				sTypeSpinner.setAdapter(wizardSTypesSpinnerAdapter);
				sTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
				{
					@Override
					public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
					{
						SType sType = wizardSTypesSpinnerAdapter.getItem(position);
						type[0] = sType.id;
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent)
					{
						SType sType = wizardSTypesSpinnerAdapter.getItem(0);
						type[0] = sType.id;
					}
				});
			}
		}

		nDialog.addView(sectionView);
		nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
		{
			@Override
			public void onActionClick(int value, boolean b)
			{
				switch (value){
					case NActionButton.VALUE_OK:
						String name = editName.getText().toString();
						if (!name.equals(""))
						{

							JSONObject commandAddr = new JSONObject();
							try
							{
								commandAddr.put("type", type[0]);
								commandAddr.put("name", name);
								if(nSendMessageToServer(dbHelper.getDeviceById(deviceId), Command.SECTION_SET, commandAddr, true)){
									waiting4section = true;
									Section section = new Section();
									section.device_id = deviceId;
									section.detector = type[0];
									savedSection = section;
									sectionAddHandler.postDelayed(sectionAddRunnable, SECTION_WAITING_TIME);

									nDialog.dismiss();
								}

							} catch (JSONException e)
							{
								e.printStackTrace();
							}
						} else
						{
							Func.pushToast(context, getString(R.string.SETA_ENTER_SEC_NUM), (Activity) context);
						}
						break;
					case NActionButton.VALUE_CANCEL:
						nDialog.dismiss();
						break;
				}
			}
		});

		nDialog.show();
	}

	public void getLibraryScripts(int device_id)
	{
		if(null!= getLocalService())
		{
			this.device_id = device_id;
			runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					waitDialog = new NDialog(context, R.layout.n_dialog_progress);
					waitDialog.setTitle(getString(R.string.DIALOG_WAIT_TITLE));
					waitDialog.show();
				}
			});
			wait = true;
			sendScriptsRequest(device_id);
			timer = new Timer();
			timer.schedule(new TimerTask()
			{
				@Override
				public void run()
				{
					wait = false;
					sendBroadcast(new Intent(D3Service.BROADCAST_LIBRARY_SCRIPTS));
				}
			}, 15000);
		}
	}

	private void sendScriptsRequest(int device_id)
	{
		Device device  = dbHelper.getDeviceById(device_id);
		JSONObject message = new JSONObject();
		try
		{
			message.put("config_version", device.config_version);
			nSendMessageToServer(null, D3Request.LIBRARY_SCRIPTS, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void openLibrarylist()
	{
		Intent intent = new Intent(getBaseContext(), ScriptsLibraryActivity.class);
		intent.putExtra("device_id", device_id);
//		intent.putExtra("site_id", siteId);
		startActivity(intent);
	}

	public void  onResume(){
		super.onResume();
		registerReceiver(libraryScriptsReceiver, new IntentFilter(D3Service.BROADCAST_LIBRARY_SCRIPTS));
		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		registerReceiver(openIVLoginWebPageAfterGetSaveSession, new IntentFilter(D3Service.BROADCAST_OPERATOR_SAFE_SESSION));
		registerReceiver(sectionAddReceiver, new IntentFilter(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV));
		bindD3();
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(libraryScriptsReceiver);
		unregisterReceiver(commandResultReceiver);
		unregisterReceiver(commandSendReceiver);
		unregisterReceiver(openIVLoginWebPageAfterGetSaveSession);
		unregisterReceiver(sectionAddReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_IV_LOGIN){
			if(resultCode == RESULT_OK){
				getToken();
				Func.nShowSuccessMessage(context);
			}
		}else if(requestCode == D3Service.ADD_SECTION_IN_DEVICE){
			if(resultCode == RESULT_OK){
				Func.nShowSuccessMessage(context);
			}
		}
	}

	private void getToken()
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject message = new JSONObject();
		try
		{
			message.put("domain", userInfo.domain);
			nSendMessageToServer(null, D3Request.IV_GET_TOKEN, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
}
