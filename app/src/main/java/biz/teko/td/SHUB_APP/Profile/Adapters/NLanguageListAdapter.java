package biz.teko.td.SHUB_APP.Profile.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Profile.Models.Language;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

/**
 * Created by td13017 on 20.04.2018.
 */

public class NLanguageListAdapter extends ArrayAdapter<Language>
{
	private OnElementClickListener onElementClickListener;

	public interface OnElementClickListener{
		void onClick(Language language);
	}

	public void setOnElementClickListener(OnElementClickListener onElementClickListener){
		this.onElementClickListener = onElementClickListener;
	}

	private final String curLang;
	private final int layout;
	private Context context;
	private LayoutInflater layoutInflater;
	private Language[] languages;

	public NLanguageListAdapter(Context context, int resource, Language[] languages, String curLang)
	{
		super(context, resource);
		this.context = context;
		this.languages = languages;
		this.curLang = curLang;
		this.layout = resource;
		layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
		View view = convertView;
		if (null==view) {
			view = layoutInflater.inflate(layout, parent, false);
		}
		Language language = languages[position];

		NMenuListElement element = (NMenuListElement) view.findViewById(R.id.nLangElement);
		if(null!=element){
			element.setTitle(language.name);
			if(curLang.equals(language.value)){
				element.setChecked(true);
			}else{
				element.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
				{
					@Override
					public void onRootClick()
					{
						if(null!=onElementClickListener) onElementClickListener.onClick(language);
					}
				});
			}
		}
		return view;
	}

	public int getCount() {
		return languages.length;
	}

	public Language getItem(int position){
		return languages[position];
	}

	public long getItemId(int position){
		return position;
	}
}

