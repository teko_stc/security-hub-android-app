package biz.teko.td.SHUB_APP.D3DB;

import org.json.JSONException;
import org.json.JSONObject;

public class LocaleD3
{
	public int id;
	public String iso;
	public String description;

	public LocaleD3(){
	}

	public LocaleD3(int id, String iso, String description){
		this.id = id;
		this.iso = iso;
		this.description = description;
	}

	public LocaleD3(JSONObject localeJson)
	{
		try
		{
			this.id = localeJson.getInt("id");
			this.iso = localeJson.getString("iso");
			this.description = localeJson.getString("description");
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}
}
