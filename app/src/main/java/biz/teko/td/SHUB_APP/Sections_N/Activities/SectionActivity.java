package biz.teko.td.SHUB_APP.Sections_N.Activities;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Activity.BaseInterfaceActivity;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Events.Activities.EventActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NAdapters.NDSDetectorsGridRecyclerAdapter;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopLayout;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class SectionActivity extends BaseInterfaceActivity
{
    private Context context;
    private DBHelper dbHelper;
    private String title;
    private int section_id;
    private int device_id;
    private int site_id;
    private boolean transition;
    private int transition_start;
    private NTopLayout topFrame;
    private Section section;
    private boolean favorited;
    private MainBar mainBar;
    private SType sType;
    private RecyclerView zonesGrid;
    private Cursor cursor;
    private NDSDetectorsGridRecyclerAdapter gridAdapter;
    private final static int COLUMN_COUNT = 2;

    private final int sending = 0;
    private ServiceConnection serviceConnection;
    private D3Service myService;
    private boolean bound;
    private UserInfo userInfo;

    private boolean binded;

    private final BroadcastReceiver updateReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if(Const.ACTION_RMV == intent.getIntExtra("action", -1)
                    && section_id == intent.getIntExtra("section", -1)
                    && device_id == intent.getIntExtra("device", -1)){
                onBackPressed();
            }else {
                
                bind();
            }
        }
    };

    private final BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
        }
    };

    private final BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
    {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            int sending = intent.getIntExtra("command_been_send", 0);
            if(sending!=1)
            {
                switch (sending)
                {
                    case 0:
                        int result = intent.getIntExtra("result", 1);
                        if (result < 1)
                        {
                            Func.showSnackBar(context, findViewById(android.R.id.content), Func.handleResult(context,result));
                        } else
                        {
                            Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_BEEN_SEND));
                        }
                        break;
                    case -1:
                        Func.showSnackBar(context, findViewById(android.R.id.content) , context.getString(R.string.COMMAND_NOT_BEEN_SEND));
                        break;
                    default:
                        break;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.n_activity_section);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        dbHelper = DBHelper.getInstance(context);
        userInfo = dbHelper.getUserInfo();

        title  = getIntent().getStringExtra("bar_title");
        section_id = getIntent().getIntExtra("section", -1);
        device_id = getIntent().getIntExtra("device", -1);
        site_id = getIntent().getIntExtra("site", -1);

        transition = getIntent().getBooleanExtra("transition",false);
        transition_start = getIntent().getIntExtra("transition_start", 0);

        addTransitionListener(transition);

        topFrame = (NTopLayout) findViewById(R.id.nTopFrame);
        zonesGrid = (RecyclerView) findViewById(R.id.nGridZones);

        zonesGrid.setLayoutManager(new GridLayoutManager(this, COLUMN_COUNT));

        bind();
    }
    private void bind()
    {
        binded = true;
        section = dbHelper.getDeviceSectionById(device_id, section_id);

        if(null!=section)
        {
            mainBar = getRefreshedMainBar();
            if (null != mainBar) favorited = true;
            else
                favorited = false;
            if (null != D3Service.d3XProtoConstEvent) {
                sType = D3Service.d3XProtoConstEvent.getSTypeById(section.detector);
            }
            setTopFrame();
            setGridFrame();

        }else{
            onBackPressed();
        }
    }

    private void setGridFrame()
    {
        if(null!= zonesGrid){
            updateGrid();
        }

    }

    private void updateGrid()
    {
        updateCursor();
        if(null!=cursor && null==gridAdapter) {
            gridAdapter = new NDSDetectorsGridRecyclerAdapter(context, cursor);
            zonesGrid.setAdapter(gridAdapter);
            if(gridAdapter.getItemCount() <= 0){
                findViewById(R.id.nTextEmpty).setVisibility(View.VISIBLE);
            }else{
                findViewById(R.id.nTextEmpty).setVisibility(View.GONE);
            }
        }else{
            gridAdapter.update(cursor);
        }
        //Func.setDynamicHeight(zonesGrid, COLUMN_COUNT);
    }

    private void updateCursor()
    {
        cursor = dbHelper.getAllZonesCursorForDeviceSectionNoController(section.device_id, section.id, Const.ALL);
    }

    private void setTopFrame()
    {
        if (null != topFrame)
        {
            topFrame.setType(NTopLayout.Type.section);
            topFrame.setBigImage(null!=sType ? sType.getBigIcon(context): R.drawable.n_image_section_common_big_selector);
            topFrame.setTransition(this, transition, transition_start);
            topFrame.setFavorited(favorited);
            topFrame.setTitle(section.name);
            if(null!=sType){
                switch (sType.id){
                    case Const.SECTIONTYPE_GUARD:
                        topFrame.setControlType(NTopLayout.ControlType.arm);
                        if(section.armed > 0){
                            topFrame.setArmed(Const.STATUS_ARMED);
                        }else{
                            topFrame.setArmed(Const.STATUS_DISARMED);
                        }
                        break;
                    case Const.SECTIONTYPE_TECH_CONTROL:
                        topFrame.setControlType(NTopLayout.ControlType.tech);
                        topFrame.setNoControl(section.armed == 0);
                        break;
                    case Const.SECTIONTYPE_ALARM:
                    case Const.SECTIONTYPE_TECH:
                        topFrame.setControlType(NTopLayout.ControlType.reset);
                        topFrame.setArmed(Const.STATUS_NO_ARM_CONTROL);break;
                    case Const.SECTIONTYPE_FIRE:
                    case Const.SECTIONTYPE_FIRE_DOUBLE:
                        topFrame.setControlType(NTopLayout.ControlType.reset_fire);
                        topFrame.setArmed(Const.STATUS_NO_ARM_CONTROL);
                        break;
                    default:
                        topFrame.setControlType(NTopLayout.ControlType.none);
                        topFrame.setArmed(Const.STATUS_NO_ARM_CONTROL);
                        break;
                }
            }
            topFrame.setOnActionListener(new NTopLayout.OnActionListener()
            {
                @Override
                public void onFavoriteChanged()
                {
                    if(!favorited){
                        addOnMain();
                    }else{
                        if(null!=mainBar){
                            if(dbHelper.deleteMainBar(mainBar)) {
                                favorited = false;
                                topFrame.setFavorited(favorited);
                            }
                        }
                    }
                }

                @Override
                public void backPressed()
                {
                    onBackPressed();
                }

                @Override
                public void onSettingsPressed()
                {
                    Intent intent = new Intent(context, SectionSettingsActivity.class);
                    intent.putExtra("section", section.id);
                    intent.putExtra("device", section.device_id);
                    startActivityForResult(intent, Const.REQUEST_DELETE);
                    overridePendingTransition(R.animator.enter, R.animator.exit);
                }

                @Override
                public void onControlPressed(int value)
                {
                    switch (value){
                        case NActionButton.VALUE_ARM:
                            try
                            {
                                JSONObject commandAddress = new JSONObject();
                                commandAddress.put("section",section.id);
                                commandAddress.put("zone", 0);
                                nSendMessageToServer(section, Command.ARM, commandAddress, true);
                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }

                            break;
                        case NActionButton.VALUE_DISARM:
                            try
                            {
                                JSONObject commandAddress = new JSONObject();
                                commandAddress.put("section",section.id);
                                commandAddress.put("zone", 0);
                                nSendMessageToServer(section, Command.DISARM, commandAddress, true);
                            } catch (JSONException e)
                            {
                                e.printStackTrace();
                            }
                            break;
                        case NActionButton.VALUE_RESET:
                            final Zone[] allZones = dbHelper.getAllZonesForDeviceSection(section.device_id, section.id);
                            if(null!=allZones){
                                for (Zone zone : allZones)
                                {
                                    try
                                    {
                                        JSONObject commandAddress = new JSONObject();
                                        commandAddress.put("section", zone.section_id);
                                        commandAddress.put("zone", zone.id);
                                        nSendMessageToServer(section, Command.RESET, commandAddress, true);
                                        dbHelper.closeEventDroppedStatus(zone);
                                    } catch (JSONException e)
                                    {
                                        e.printStackTrace();
                                    }
                                }
                                
                                bind();
                            }
                            break;
                    }
                }

                @Override
                public void onLinkPressed(int id)
                {
                    /*scripted relay link*/
                }

                @Override
                public void onStateClick(int type)
                {
                    /*no action for sections*/
                }
            });

            topFrame.setNormal(true);

            /*if section or zone been alarmed ib last 10 min & not dropped*/
            long max_time = dbHelper.getAlarmEventMaxTimeForSection(section);
            boolean been_alarmed = System.currentTimeMillis()/1000 - max_time <= Const.SECTION_ZONE_TO_DROP_ALARM_TIME;

            LinkedList<Event> affects = dbHelper.getAffectsBySiteDeviceSectionId(site_id, section.device_id, section.id);
//            if(null!=affects)
                topFrame.setStates(affects, section, been_alarmed);

            topFrame.bindView();
        }
    }

    private void openEventActivity(int id)
    {
        Intent intent = new Intent(context, EventActivity.class);
        intent.putExtra("event", id);
        startActivity(intent);
        overridePendingTransition(R.animator.enter, R.animator.exit);
    }

    private void addOnMain() {
        boolean shared = PrefUtils.getInstance(context).getSharedMainScreenMode();

        Cursor cursor = dbHelper.getMainBars(site_id, shared);
        if (null == cursor || 0 == cursor.getCount()) {
            dbHelper.fillBars(site_id, shared);
        }
        MainBar mainBar = dbHelper.getEmptyBar(site_id, shared);
        if (null != mainBar) {
            mainBar.type = MainBar.Type.ARM;
            mainBar.subtype = MainBar.SubType.SECTION;

            JSONObject jsonObject = new JSONObject();
            try
            {
                jsonObject.put("device", section.device_id);
                jsonObject.put("section", section.id);
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            mainBar.element_id = jsonObject.toString();
            if (dbHelper.addNewBar(mainBar, shared)) {
                favorited = true;
                topFrame.setFavorited(favorited);
                this.mainBar = getRefreshedMainBar();
            } else {
                Func.pushToast(context, "DB ERROR", (SectionActivity) context);
            }
        }else{
            NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
            nDialog.setTitle(getString(R.string.N_ERROR_NO_SPACE_ON_MAIN));
            nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
            {
                @Override
                public void onActionClick(int value, boolean b)
                {
                    switch (value){
                        case NActionButton.VALUE_CANCEL:
                            nDialog.dismiss();
                            break;
                    }
                }
            });
            nDialog.show();
        }
    }

    private MainBar getRefreshedMainBar()
    {
        return dbHelper.getMainBarForSection(site_id, section);
    }

    public void  onResume(){
        super.onResume();
        if(!binded) {
            
            bind();
        }
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
        intentFilter.addAction(D3Service.BROADCAST_AFFECT);
        intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
        registerReceiver(updateReceiver, intentFilter);
        registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
        registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
        bindD3();
    }

    public void onPause(){
        super.onPause();
        binded = false;
        unregisterReceiver(updateReceiver);
        unregisterReceiver(commandResultReceiver);
        unregisterReceiver(commandSendReceiver);
        if(bound)unbindService(serviceConnection);
    }

    private void bindD3(){
        Intent intent = new Intent(this, D3Service.class);
        serviceConnection = getServiceConnection();
        bindService(intent, serviceConnection, 0);
    }

    private ServiceConnection getServiceConnection(){
        return new ServiceConnection()
        {
            public void onServiceConnected(ComponentName name, IBinder binder)
            {
                Log.d("D3-", "onServiceConnected");
                D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
                myService = myBinder.getService();
                bound = true;
            }

            public void onServiceDisconnected(ComponentName name)
            {
                Log.d("D3-", "onServiceDisconnected");
                bound = false;
            }
        };
    }

    private D3Service getLocalService()
    {
        if(bound){
            return myService;
        }else{
            bindD3();
            if(null!=myService){
                return myService;
            }
            Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
        }
        return null;

    }

    public boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
    {
        return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Const.REQUEST_DELETE){
            if(resultCode == RESULT_OK){
                finish();
            }
        }
    }

    @Override
    public void nrAction(int id)
    {
        nSendMessageToServer(section, Command.ARM, new JSONObject(), true);
    }
}
