package biz.teko.td.SHUB_APP.Profile.Notifications;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationManagerCompat;

import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NTopToolbarView;
import biz.teko.td.SHUB_APP.Utils.NotificationUtils;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

/**
 * Created by td13017 on 24.03.2017.
 */

public class NotificationsPreferenceFragment extends PreferenceFragment {
    private Context context;
    private Preference notif_types;
    private CheckBoxPreference notif_am;
    private CheckBoxPreference notif_drop_conn;
    private NotificationUtils notificationUtils;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
        notificationUtils = NotificationUtils.getInstance(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            addPreferencesFromResource(App.getMode() ? R.xml.notification_preferences_oreo : R.xml.notification_preferences_oreo_light);
        else
            addPreferencesFromResource(App.getMode() ? R.xml.notification_preferences : R.xml.notification_preferences_light);
        initPreference();
    }

    private void initPreference() {
        notif_types = findPreference("notif_types");
        notif_am = (CheckBoxPreference) findPreference("notif_inapp_am");
        notif_drop_conn = (CheckBoxPreference) findPreference("conn_drop_notif_pref");
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView list = view.findViewById(android.R.id.list);
        list.setDivider(new ColorDrawable(Color.TRANSPARENT));
        list.setDividerHeight(0);
    }

    private final PrefUtils prefUtils = PrefUtils.getInstance(context);

    @Override
    public void onResume() {
        super.onResume();
        if (NotificationManagerCompat.from(context).areNotificationsEnabled()) {
            notif_types.setEnabled(true);
            notif_types.setOnPreferenceClickListener(preference -> {
                Intent intent = new Intent(context, NNotificationsClassesActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.animator.enter, R.animator.exit);
                return false;
            });
        } else {
            notif_types.setEnabled(false);
        }

        notif_am.setChecked(PrefUtils.getInstance(context).getUseAM());
        notif_am.setOnPreferenceChangeListener((preference, newValue) -> {
            PrefUtils.getInstance(context).setUseAM((boolean) newValue);
            notif_am.setChecked((boolean) newValue);
            return false;
        });
        NTopToolbarView toolbar = getActivity().findViewById(R.id.toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!NotificationManagerCompat.from(context).areNotificationsEnabled()) {
                ((NActionButton) toolbar.getViewById(R.id.nDialogButtonSetting))
                        .setOnButtonClickListener(view -> {
                            Intent intent = new Intent();
                            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                            intent.putExtra("android.provider.extra.APP_PACKAGE", getActivity().getPackageName());
                            startActivity(intent);
                        });
                ((NActionButton) toolbar.getViewById(R.id.nDialogButtonCancel))
                        .setOnButtonClickListener(view -> toolbar.setStateInfo(false));
                toolbar.setStateInfo(true);
                notif_drop_conn.setChecked(checkConnDropNotifState());
                notif_drop_conn.setEnabled(false);
            } else /*if(!checkConnDropNotifAllowed())*/ {
                toolbar.setStateInfo(false);
                notif_drop_conn.setEnabled(true);
                notif_drop_conn.setChecked(checkConnDropNotifState());
                notif_drop_conn.setOnPreferenceChangeListener((preference, newValue) -> {
                    recreateConnDropChannel((Boolean) newValue ?
                            NotificationManager.IMPORTANCE_HIGH :
                            NotificationManager.IMPORTANCE_NONE);
//                    PrefUtils.getInstance(context).setConnDropNotifMode((Boolean) newValue);
                    notif_drop_conn.setChecked((Boolean) newValue);
                    return false;
                });

            }
        } else {
            toolbar.setStateInfo(false);
            notif_drop_conn.setEnabled(true);
            notif_drop_conn.setChecked(PrefUtils.getInstance(context).getConnDropNotifMode());

            notif_drop_conn.setOnPreferenceChangeListener((preference, newValue) -> {
                PrefUtils.getInstance(context).setConnDropNotifMode((boolean) newValue);
                notif_drop_conn.setChecked((boolean) newValue);
                return false;
            });
        }

//		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//		{
//			CheckBoxPreference checkBoxPreference = (CheckBoxPreference) findPreference("notif_status");
//			checkBoxPreference.setChecked(NotificationManagerCompat.from(context).areNotificationsEnabled());
//		}
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private boolean checkConnDropNotifState() {
        NotificationChannel notificationChannel = NotificationManagerCompat.from(context).getNotificationChannel(getConnectionDropChannelId());
        if (notificationChannel == null) {
//            recreateConnDropChannel(NotificationManager.IMPORTANCE_DEFAULT);
            return false;
        } else
            return (notificationChannel.getImportance() != NotificationManagerCompat.IMPORTANCE_NONE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void recreateConnDropChannel(int importance) {
        deleteConnectionDropChannel();
        updateConnectionDropChannelId();
        createConnectionDropChannel(importance);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createConnectionDropChannel(int importance) {
        notificationUtils.createChannel(getConnectionDropChannelId(),
                getString(R.string.CONNECTION_DROP_NOTIFICATION),
                importance
        );
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void deleteConnectionDropChannel(){
        notificationUtils.deleteConnectionDropNotifChannel();
    }

    private String getConnectionDropChannelId() {
        return notificationUtils.getConnectionDropNotifChannelId();
    }

    private void updateConnectionDropChannelId() {
        notificationUtils.updateConnectionDropNotifChannelId();
    }
}
