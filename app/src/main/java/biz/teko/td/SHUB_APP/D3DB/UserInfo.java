package biz.teko.td.SHUB_APP.D3DB;

import biz.teko.td.SHUB_APP.Utils.Dir.Const;

/**
 * Created by td13017 on 15.06.2016.
 */
public class UserInfo extends D3Element
{
	public String userLogin;
	public String userPassword;
	public int domain = 0;
	public Long serverTime;
	public String pinCode = "";
	public int roles = 0;
	public String ivToken = "";
	public String locale = "";

	public UserInfo(){}

	public UserInfo(int id, String userLogin, String userPassword, Long serverTime){
		this.id = id;
		this.userLogin = userLogin;
		this.userPassword = userPassword;
		this.serverTime = serverTime;
	}

	public UserInfo(int id, String userLogin, String userPassword, int roles, int domain, Long serverTime, String pinCode, String ivToken, String locale){
		this.id = id;
		this.userLogin = userLogin;
		this.userPassword = userPassword;
		this.roles = roles;
		this.domain = domain;
		this.serverTime = serverTime;
		this.pinCode = pinCode;
		this.ivToken = ivToken;
		this.locale = locale;
	}



	public Long getServerTime()
	{
		return serverTime;
	}

	public void setServerTime(Long serverTime)
	{
		this.serverTime = serverTime;
	}

	public void setId(int id){
		this.id = id;
	}

	public void setUserLogin(String userLogin){
		this.userLogin = userLogin;
	}

	public void setUserPassword(String userPassword){
		this.userPassword = userPassword;
	}

	public int getId()
	{
		return id;
	}

	public String getUserLogin()
	{
		return userLogin;
	}

	public String getUserPassword()
	{
		return userPassword;
	}

	public int getDomain()
	{
		return domain;
	}

	public void setDomain(int domain)
	{
		this.domain = domain;
	}

	public int getRoleValue()
	{
		if(0!=(this.roles& Const.Roles.TRINKET)){return 4;}
		else if(0!=(this.roles&Const.Roles.DOMAIN_ADMIN)){return 1;}
		else if (0!=(this.roles&Const.Roles.DOMAIN_ENGIN)){return 2;}
		else if (0!=(this.roles&Const.Roles.DOMAIN_OPER)){return 3;}
		else{return 0;}
	}

	public boolean isDEngineer(){
		return (roles&Const.Roles.DOMAIN_ENGIN)!=0;
	}


}
