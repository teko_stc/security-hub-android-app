package biz.teko.td.SHUB_APP.Utils.Other;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public  class NGridView extends GridView
{
	public NGridView(Context context)
	{
		super(context);
	}

	public NGridView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public NGridView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	public NGridView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	public void setAdapter(ListAdapter adapter)
	{
		super.setAdapter(adapter);
	}


	@Override
	public void setNumColumns(int numColumns)
	{
		super.setNumColumns(numColumns);
	}

	public void setDynamicHeight(int colums)
	{
		ListAdapter adapter = getAdapter();
		//checkStates adapter if null
		if (adapter == null) {
			return;
		}
		int height = 0;
		int last_h = 0;
		int desiredWidth = View.MeasureSpec.makeMeasureSpec(getWidth(), View.MeasureSpec.UNSPECIFIED);
		for (int i = 0; i < adapter.getCount(); i++) {
			View listItem = adapter.getView(i, null, this);
			listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
			int h = listItem.getMeasuredHeight();
			if((i%colums)==0){
				height += h;
				last_h = h;
			}else if(h>last_h){
				height= height-last_h+h;
			}
		}
		ViewGroup.LayoutParams layoutParams = getLayoutParams();
		layoutParams.height = height + Func.dpToPx(24, getContext());
		setLayoutParams(layoutParams);
		requestLayout();
	}
}
