package biz.teko.td.SHUB_APP.Devices_N.Fragments;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.BaseInterfaceFragmentTemp;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Devices_N.Adapters.DevicesListPagerAdapter;
import biz.teko.td.SHUB_APP.MainTabs.Fragments.SiteSelectFragment;
import biz.teko.td.SHUB_APP.MainTabs.Rounters.NotifProblemRouter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SlidingTabsLib.SlidingTabLayout;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NStartTutorialView;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Wizards.Activity.WizardAllActivity;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class DevicesListFragment extends BaseInterfaceFragmentTemp {

	private View layout;
	private CharSequence[] titles;
	private final Subject<String> querySubject = PublishSubject.create();
	private static final int numoftabs = 2;
	private int site_id;
	private DBHelper dbHelper;
	private String lastQuery = "";
	private PrefUtils prefUtils;
	private DevicesListPagerAdapter adapter;
	private ViewPager pager;
	private SlidingTabLayout tabs;
	private NStartTutorialView tutorial;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private UserInfo userInfo;


	public void onBackPressed()
	{
		if (tutorial != null) {
			showFinishTutorialDialog();
		} else {
			showExitDialog();
		}
	}

	@Override
	public void nrAction(int id) {
		nSendMessageToServer(dbHelper.getDeviceById(id), Command.ARM, new JSONObject(), true);
	}

	private final BroadcastReceiver commandResultReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			((BaseActivity) getActivity()).showCommandResultSnackbar(context, intent.getIntExtra("result", -1));
//			Func.showCommandResultSnackbar(context, layout, intent.getIntExtra("result", -1));
		}
	};
	private final BroadcastReceiver commandSendReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			int sending = intent.getIntExtra("command_been_send", 0);
			if (sending != 1) {
				switch (sending) {
					case 0:
						int result = intent.getIntExtra("result", 1);
						if (result < 1) {
							((BaseActivity) getActivity()).showSnackBar(context, Func.handleResult(context, result));
						} else {
							((BaseActivity) getActivity()).showSnackBar(context, context.getString(R.string.COMMAND_BEEN_SEND));
						}
						break;
					case -1:
						((BaseActivity) getActivity()).showSnackBar(context, context.getString(R.string.COMMAND_NOT_BEEN_SEND));
						break;
					default:
						break;
				}
			}
		}
	};
	private Filter filter = new Filter("");
	private Disposable subjectDisposable;

	private void search(String query) {
		filter = new Filter(query.trim());
		adapter.setFilter(filter);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		layout = getView();


		LinearLayout addNewElementView = layout.findViewById(R.id.nButtonMainAdd);
		if (null != addNewElementView)
			addNewElementView.setOnClickListener(_e -> startActivity(new Intent(((BaseActivity) getActivity()), WizardAllActivity.class)));


		titles = new CharSequence[]{getString(R.string.N_PAGER_DEVICE), getString(R.string.N_PAGER_SECTION)};

		adapter = new DevicesListPagerAdapter(getChildFragmentManager(), titles, numoftabs, site_id);

		// Assigning ViewPager View and setting the adapter
		pager = layout.findViewById(R.id.nPagerDevices);
		pager.setAdapter(adapter);

		// Assiging the Sliding Tab Layout View
		tabs = layout.findViewById(R.id.nTabsDevices);
		tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

		// Setting Custom ColorX for the Scroll bar indicator of the Tab View
		tabs.setCustomTabColorizer(position -> getResources().getColor(R.color.n_brand_blue));
		tabs.setCustomTabView(R.layout.n_devices_tab, R.id.nTextTab);

		// Setting the ViewPager For the SlidingTabsLayout
		tabs.setViewPager(pager);
		LinearLayout buttonMainSiteSelect = view.findViewById(R.id.nButtonSiteSelect);
		if (null != buttonMainSiteSelect) buttonMainSiteSelect.setOnClickListener(view1 ->
				showSiteSelectFrame(site_id));
		TextView siteName = (TextView) view.findViewById(R.id.nTextSiteName);
		Site site =dbHelper.getSiteById(site_id);
		if(null!=site && null!=site.name){
			siteName.setText(site.name.trim());
		}
		siteName.setMaxLines(1);
		CustomSearchView searchView = layout.findViewById(R.id.search_view_main);
		searchView.setOnSearchStateListener((open -> {
			if (open)
				layout.findViewById(R.id.nButtonSiteSelect).setVisibility(View.GONE);
			else
				layout.findViewById(R.id.nButtonSiteSelect).setVisibility(View.VISIBLE);
			return null;
		}));
		searchView.setQueryTextChangeListener(new CustomSearchView.QueryTextListener() {
			@Override
			public void onQueryTextSubmit(String query) {
				search(query);
			}

			@Override
			public void onQueryTextChange(String newText) {
				querySubject.onNext(newText);
			}
		});
	}


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.n_activity_devices, container, false);

	}

	private void updateSiteName() {
		TextView siteName = getView().findViewById(R.id.nTextSiteName);
		siteName.setText(dbHelper.getSiteById(site_id).name.trim());
	}

	private void showSiteSelectFrame(int site_id) {

		SiteSelectFragment siteSelectFragment = SiteSelectFragment.getInstance(site_id);
		siteSelectFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
		siteSelectFragment.setListener((result, site_id1) -> {
			switch (result) {
				case CHANGE:
					updateSiteId(site_id1);
					updateSiteName();
					break;
				default:
					break;

			}
		});

		siteSelectFragment.show(getChildFragmentManager(), "tagSetSite");
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = DBHelper.getInstance(getActivity());
		prefUtils = PrefUtils.getInstance(getActivity());

		site_id = prefUtils.getCurrentSite();
		if (site_id == 0) {
			site_id = dbHelper.getFirstSite();
		}else{
			Site site = dbHelper.getSiteById(site_id);
			if(null==site) site_id = dbHelper.getFirstSite(); // if site been just deleted
		}

		((BaseActivity) getActivity()).setOnBackPressedCallback(this::onBackPressed);

	}

	@Override
	public void onStart() {
		super.onStart();
		if (tutorial != null && !PrefUtils.getInstance(getActivity()).getStartTutorial()) {
			tutorial.finishTutorial();
		}
	}

	@Override
	public void  onResume() {
		super.onResume();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_ALARM_ADD_UPD_RMV);
		getActivity().registerReceiver(updateReceiver, intentFilter);
		getActivity().registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		bindD3();
		updateBottomHelper();
		setupTutorial();
		adapter.setFilter(filter);
		subjectDisposable = querySubject
				.debounce(400, TimeUnit.MILLISECONDS)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe((String q) -> {
					if (!q.trim().equals(lastQuery.trim())) {
						search(q);
						lastQuery = q;
					}
				});
		CustomSearchView searchView = layout.findViewById(R.id.search_view_main);

		if (!filter.getQuery().isEmpty()) {
			searchView.expand(false);
		}
	}

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			updateBottomHelper();
		}
	};

	@Override
	public void onPause() {
		super.onPause();
		(getActivity()).unregisterReceiver(updateReceiver);
		(getActivity()).unregisterReceiver(commandResultReceiver);
		(getActivity()).unregisterReceiver(commandSendReceiver);
		if (bound)
			(getActivity()).unbindService(serviceConnection);
		subjectDisposable.dispose();
	}

	public void updateSiteId(int site_id) {
		PrefUtils.getInstance(getContext()).setCurrentSite(site_id);
		this.site_id = site_id;
		adapter.setSiteId(site_id);

	}

	private void setupTutorial() {
		if (tutorial == null && PrefUtils.getInstance(getActivity()).getStartTutorial()) {
			tutorial = new NStartTutorialView(getActivity(), getActivity(),
					((BaseActivity) getActivity()).isFirstTutorial() &&
							dbHelper.getNAllDevicesCursor().getCount() > 0 ? 7 : 10);
			tutorial.setNextActivityListener(this::startDeviceActivityTutorial);
			tutorial.setSkipListener(this::checkNotificationProblems);
		}
	}

	private void checkNotificationProblems() {
		new NotifProblemRouter(((BaseActivity) getActivity()).getSupportFragmentManager(), ((BaseActivity) getActivity())).setup();
	}

	private void startDeviceActivityTutorial() {
		adapter.getDevicesListFragment().startDeviceActivityTutorial();
	}


	private void bindD3() {
		Intent intent = new Intent(((BaseActivity) getActivity()), D3Service.class);
		serviceConnection = getServiceConnection();
		getActivity().bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else {
			bindD3();
			if (null != myService) {
				return myService;
			}
			Func.showRestartServiceMessage(getActivity(), this.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	public boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command) {
		int sending = 0;
		return Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), ((BaseActivity) getActivity()), command, sending);
	}


	private void showFinishTutorialDialog() {
		NDialog nDialog = new NDialog(((BaseActivity) getActivity()), R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.TUTORIAL_FINISH_QUESTION));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value) {
				case NActionButton.VALUE_OK:
					finishTutorial();
					nDialog.dismiss();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}

		});
		nDialog.show();
	}

	private void finishTutorial() {
		tutorial.finishTutorial();
		tutorial = null;
	}

	private void showExitDialog() {
		NDialog nDialog = new NDialog(((BaseActivity) getActivity()), R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.N_LOGOUT_Q));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value) {
				case NActionButton.VALUE_OK:
					getActivity().finishAffinity();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}

		});
		nDialog.show();
	}

	private void updateBottomHelper() {
		((BaseActivity) getActivity()).setBottomInfoBehaviour(((BaseActivity) getActivity()));
	}
}
