package biz.teko.td.SHUB_APP.Alarms.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import biz.teko.td.SHUB_APP.Alarms.ViewHolders.AlarmFullScreenViewHolder;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.R;

public class AlarmFullScreenAdapter extends RecyclerView.Adapter<AlarmFullScreenViewHolder> {
    private List<Event> events;
    private Context context;
    private DBHelper dbHelper;

    public AlarmFullScreenAdapter(List<Event> events, Context context) {
        this.events = events;
        this.context = context;
        dbHelper = DBHelper.getInstance(context);
    }

    @NonNull
    @Override
    public AlarmFullScreenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AlarmFullScreenViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.n_alarm_fullscreen_event_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AlarmFullScreenViewHolder holder, int position) {
        Event event = events.get(position);
        String location = dbHelper.getSiteNameByDeviceSection(
                event.device, event.section) + "\n" + event.getEventLocation(context);
        holder.bind(events.get(position), location, context);
    }

    @Override
    public int getItemCount() { return events.size(); }
}
