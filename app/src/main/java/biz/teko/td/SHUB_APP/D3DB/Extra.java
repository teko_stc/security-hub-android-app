package biz.teko.td.SHUB_APP.D3DB;

import org.json.JSONException;
import org.json.JSONObject;

public class Extra
{
	int device_id;
	int section_id;
	int zone_id;
	String name;
	String value;

	public Extra(){

	}

	public Extra(int device_id, int section_id, int zone_id, String name , String value){
		this.device_id = device_id;
		this.section_id = section_id;
		this.zone_id = zone_id;
		this.name = name;
		this.value = value;
	}

	public Extra(String name, String value){
		this.name = name;
		this.value = value;
	}

	public String getLowLimit(){
		try
		{
			JSONObject jsonObject = new JSONObject(value);
			String val = jsonObject.getString("low_temp");
			return (Integer.parseInt(val) != -128) ? val : null;
		} catch (JSONException e)
		{
			return null;
		}
	}

	public String getTopLimit(){
		try
		{
			JSONObject jsonObject = new JSONObject(value);
			String val = jsonObject.getString("high_temp");
			return (Integer.parseInt(val) != -128 ? val : null);
		} catch (JSONException e)
		{
			return null;
		}
	}
}
