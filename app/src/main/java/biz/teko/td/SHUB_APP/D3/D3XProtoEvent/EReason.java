package biz.teko.td.SHUB_APP.D3.D3XProtoEvent;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

/**
 * Created by td13017 on 05.08.2016.
 */
public class EReason
{
	@Attribute(name = "id")
	public int id;

	@Attribute(name = "caption")
	public String caption;

	@Attribute(name = "iconSmall", required = false)
	public String iconSmall;

	@Attribute(name = "iconBig", required = false)
	public String iconBig;

	@Attribute(name = "backGroundColor")
	public String backGroundColor;

	@Attribute(name = "affect")
	public String affect;

	@ElementList(name = "statements", entry = "statement", required = false)
	public LinkedList<EStatement> statements;

	public EStatement getStatementById(int id)
	{
		LinkedList<EStatement> statements = this.statements;
		for(EStatement statement:statements){
			if(statement.id == id){
				return statement;
			}
		}
		return null;
	}
}
