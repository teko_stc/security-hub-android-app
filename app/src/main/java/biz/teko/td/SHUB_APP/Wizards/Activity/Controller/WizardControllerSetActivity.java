package biz.teko.td.SHUB_APP.Wizards.Activity.Controller;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.google.zxing.integration.android.IntentIntegrator;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.MainNavigationActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.Other.SwipeDisabledViewPager;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.ContrSetViewPagerAdapter;
import biz.teko.td.SHUB_APP.Wizards.Fragments.ControllerSetFragment;

/**
 * Created by td13017 on 07.06.2017.
 */

public class WizardControllerSetActivity extends BaseActivity
{
	private static final long MAX_WAIT_TIME_MS = 15000;
	private ContrSetViewPagerAdapter adapter;
	private SwipeDisabledViewPager pager;
	private Context context;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private int addedSiteID = 0;
	private int errorCount = 0;


	private final BroadcastReceiver siteAddResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(wait)
			{
				stopProgress();
				if (null != timer) timer.cancel();
				int result = intent.getIntExtra("result", 0);
				if(result > 0){
					if(pager.getCurrentItem() == 1)
					{
						addedSiteID = result;
						goNext();
					}
				}else{
					errorCount++;
					if(errorCount > 5){
						Func.nShowMessage(context, Func.handleResult(context, result) + "\n" + getString(R.string.ATTETION_BUN_CHANCE));
					}else
					{
						Func.nShowMessage(context, Func.handleResult(context, result));
					}
				}
			}
		}
	};

	private boolean toCapture;
	private int from;
	private LinearLayout buttonNext;
	private LinearLayout buttonBack;
	private LinearLayout closeButton;
	private final int sending = 0;
	private String serial;
	private String name;
	private int cluster = 0;
	private View activityRootView;
	private FrameLayout buttonFrame;
	private boolean wait;
	private NDialog progressDialog;
	private Timer timer;
	private TimerTask timerTask;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_controller_setup);
		context = this;

		setup();
		bind();
	}

	private void bind()
	{
		from = getIntent().getIntExtra("from", 0);

		if(null!=closeButton)closeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				skip();
			}
		});


		if(null!=buttonNext){
			buttonNext.setVisibility(View.VISIBLE);
			buttonNext.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					setNextPage();
				}
			});
		}

		if(null!=buttonBack) {
			buttonBack.setVisibility(View.VISIBLE);
			buttonBack.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View view)
				{
					onBackPressed();
				}
			});
		}
		adapter = new ContrSetViewPagerAdapter(getFragmentManager(), new ControllerSetFragment.OnClickListener()
		{
			@Override
			public void onFinishSetup()
			{

			}

			@Override
			public void onCameraStart()
			{
				if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY))
				{
					setToCapture(true);
					IntentIntegrator integrator = new IntentIntegrator((WizardControllerSetActivity) context);
					integrator.setOrientationLocked(false);
					integrator.setBeepEnabled(false);
					integrator.initiateScan(Const.BARCODE_CODES);
				}else{
					Func.nShowMessage(context, getString(R.string.ERROR_CANT_SCAN_NO_CAMERA_HARDWARE));
				}
			}

			@Override
			public void onNext()
			{
				setNextPage();
			}

			@Override
			public void onBack()
			{
				onBackPressed();
			}

			@Override
			public void onSkip()
			{
				skip();
			}

			@Override
			public void onNextSet(boolean b)
			{
				if(null!=buttonNext)buttonNext.setEnabled(b);
			}

			@Override
			public void onBackSet(int position)
			{
//				if(null!=buttonBack)buttonBack.setVisibility(position>0?View.VISIBLE:View.GONE);
			}

			@Override
			public void setSerial(String s)
			{
				serial = s;
			}

			@Override
			public void setName(String s)
			{
				name = s;
			}

			@Override
			public void setCluster(int i)
			{
				cluster = i;
			}
		});

		if(null!=pager)
		{
			pager.setAdapter(adapter);
//			pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//				@Override
//				public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//
//				}
//
//				@Override
//				public void onPageSelected(int position) {
//					pager.reMeasureCurrentPage(pager.getCurrentItem());
//				}
//
//				@Override
//				public void onPageScrollStateChanged(int state) {
//
//				}
//			});
			refreshViews();
		}

		activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(){
			@Override
			public void onGlobalLayout()
			{
				int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
				if (heightDiff > Func.dpToPx(200, context))
				{
					if(null!=buttonFrame) buttonFrame.setVisibility(View.GONE);
				}else{
					if(null!=buttonFrame) buttonFrame.setVisibility(View.VISIBLE);
				}
			}
		});

		}

	private void setup()
	{
		closeButton = (LinearLayout) findViewById(R.id.nButtonClose);
		buttonNext = (LinearLayout) findViewById(R.id.mainSetButtonNext);
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		pager = (SwipeDisabledViewPager) findViewById(R.id.controllerSetViewPager);
		activityRootView = findViewById(R.id.controllerSetupRootFrame);
		buttonFrame = (FrameLayout) findViewById(R.id.nButtonFrame);
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		bindD3();
		registerReceiver(siteAddResultReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_ASSIGN_DOMAIN));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		if(toCapture){
			((App)this.getApplication()).stopActivityTransitionTimer();
//			((App)this.getApplication()).startActivityTransitionTimer(context);
		}
//		((App)this.getApplication()).stopActivityTransitionTimer();
		toCapture = false;
		unbindService(serviceConnection);
		unregisterReceiver(siteAddResultReceiver);
	}

	@Override
	public void onBackPressed() {
		Func.hideKeyboard((WizardControllerSetActivity)context);
		if (pager.getCurrentItem() == 0) {
			skip();
		} else if(pager.getCurrentItem()==2){
			super.onBackPressed();
		}else{
			goBack();

		}
	}


	private void showSkipDialog()
	{
		AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
		adb.setTitle(R.string.SKIP_CONTROLLER_SET_TITLE);
		adb.setMessage(R.string.SKIP_CONTROLLER_SET_MESSAGE);
		adb.setPositiveButton(R.string.ADB_OK, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				Intent activityIntent = new Intent(getBaseContext(), MainNavigationActivity.class);
				startActivity(activityIntent);
				overridePendingTransition(R.animator.enter, R.animator.exit);
				finish();
			}
		});
		adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
		{
			@Override
			public void onClick(DialogInterface dialog, int which)
			{
				dialog.dismiss();
			}
		});
		adb.show();
	}

	public int getCurrentItem(){
		return pager.getCurrentItem();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		Fragment uploadType = getFragmentManager().findFragmentById(R.id.controllerSetViewPager);
		if (uploadType != null) {
			uploadType.onActivityResult(requestCode, resultCode, intent);
		}
		super.onActivityResult(requestCode, resultCode, intent);
	}

	public void setNextPage()
	{
		if(pager.getCurrentItem() != 3)
		{
			if(pager.getCurrentItem() == 1)
			{
				if(validate()){
					JSONObject message = new JSONObject();
					try
					{
						if (cluster != 0)
						{
							message.put("cluster_id", cluster);
						} else
						{
							message.put("cluster_id", 1);

						}
						message.put("account", Long.valueOf(serial)/10000);
						message.put("pin_code", Long.valueOf(serial)%10000);
						message.put("new_site_name", name);
						if(nSendMessageToServer(null, D3Request.DEVICE_ASSIGN_DOMAIN, message, false)){
							startProgress();
						}
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
				}
			}else
			{
				goNext();
			}
		}else{
			finish();
		}
	}

	private void startProgress()
	{
		wait = true;
		progressDialog = new NDialog(context, R.layout.n_dialog_progress);
		progressDialog.setTitle(null);
		progressDialog.show();
		timer = new Timer();
		timerTask = new TimerTask()
		{
			public void run()
			{
				stopProgress();
			}
		};
		timer.schedule(timerTask, MAX_WAIT_TIME_MS);
	}

	private void stopProgress()
	{
		wait = false;
		if(null!=progressDialog)progressDialog.dismiss();
	}

	private boolean validate()
	{
		if(null!=name && !name.equals(""))
		{
			if (null != serial && !serial.equals(""))
			{
				long i = Long.valueOf(serial);
				if (i >= 10000)
				{
					return true;
				}else
				{
					Func.nShowMessage(context, getString(R.string.CAA_ERROR_SN));
				}
			} else
			{
				Func.nShowMessage(context, getString(R.string.CAA_ENTER_SN));
			}
		}else{
			Func.nShowMessage(context, getString(R.string.CAA_ENTER_NAME));
		}
		return false;
	}

	private void goNext()
	{
		Func.hideKeyboard((WizardControllerSetActivity) context);
		int position = pager.getCurrentItem();
		pager.setCurrentItem(position + 1);
		refreshViews();
	}

	private void goBack()
	{
		int position = pager.getCurrentItem();
		pager.setCurrentItem(position - 1);
		refreshViews();
	}

	private void refreshViews()
	{
		switch (getCurrentItem()){
			case 0:
				buttonBack.setVisibility(View.GONE);
				buttonNext.setVisibility(View.VISIBLE);
				break;
			case 1:
				buttonBack.setVisibility(View.VISIBLE);
				buttonNext.setVisibility(View.VISIBLE);
				break;
			case 2:
				buttonBack.setVisibility(View.GONE);
				buttonNext.setVisibility(View.VISIBLE);
				break;
			case 3:
				buttonBack.setVisibility(View.VISIBLE);
				buttonNext.setVisibility(View.VISIBLE);
				break;
			case 4:
				buttonBack.setVisibility(View.VISIBLE);
				buttonNext.setVisibility(View.VISIBLE);
				break;
			default:
				break;
		}
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	}

	public void setToCapture(boolean b){
		toCapture = b;
	}

	public int getFrom(){
		return from;
	}

	public void skip()
	{
		switch (from){
			case 0:
				showSkipDialog();
				break;
			case 1:
				super.onBackPressed();
				break;
			default:
				break;
		}
	}
}
