package biz.teko.td.SHUB_APP.Cameras.Activities.IV;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Cameras.Adapters.SitesListAdapter;
import biz.teko.td.SHUB_APP.Cameras.Adapters.ZonesListAdapter;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;

/**
 * Created by td13017 on 27.02.2018.
 */

public class NCameraBindActivity extends BaseActivity
{
	private static final int FROM_CAMERA = 0;
	private static final int FROM_SETTINGS = 1;
	private Context context;
	private DBHelper dbHelper;
	private String id;
	private Site selSite = null;
	private Zone selZone = null;

	private NActionButton buttonSetSite;
	private NActionButton buttonSetZone;
	private NActionButton add;
	private LinearLayout close;
	private LinearLayout back;
	private TextView title;
	private TextView subTitle;

	private int sending = 0;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private int from = 0;
	private boolean binded = false;

	private BroadcastReceiver bindingResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int result = intent.getIntExtra("result", -999);
			if(result > 0){
				Func.pushToast(context, getString(R.string.CAMERA_BINDED_SUCCESS), (Activity) context);
				binded = true;
				onBackPressed();
			}else{
				Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
			}
		}
	};

	private Camera camera;


	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
		registerReceiver(bindingResultReceiver, new IntentFilter(D3Service.BROADCAST_IV_BIND_CAM));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(bindingResultReceiver);
	}

	public D3Service getLocalService()
	{
		return bound ? myService : null;
	}


	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera_bind);
		this.context = this;
		dbHelper = DBHelper.getInstance(context);
		id = getIntent().getStringExtra("cameraId");
		from = getIntent().getIntExtra(Const.OPEN_IV_KEY_FROM, 0);

		serviceConnection = new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};


		setup();
		bind();
	}

	private void setup()
	{
		add = (NActionButton) findViewById(R.id.nBindButton);
		close = (LinearLayout) findViewById(R.id.nButtonClose);
		back = (LinearLayout) findViewById(R.id.nButtonBack);
		title = (TextView) findViewById(R.id.nTextTitle);
		subTitle = (TextView) findViewById(R.id.nTextSubTitle);
		buttonSetSite = (NActionButton) findViewById(R.id.nCameraBindSiteButton);
		buttonSetZone = (NActionButton) findViewById(R.id.nCameraBindZoneButton);
	}

	private void bind()
	{
		camera = dbHelper.getIVCameraById(id);

		if(null!=back){
			back.setVisibility(from == Const.OPEN_IV_FROM_SETTINGS ? View.VISIBLE : View.GONE);
			back.setOnClickListener(v -> {
				onBackPressed();
			});
		}

		if(null!=close) {
			close.setVisibility(from == Const.OPEN_IV_FROM_CAMERA ? View.VISIBLE : View.GONE);
			close.setOnClickListener(v -> onBackPressed());
		}

		if(null!=camera)
		{
			if(null!=title && null!=camera.name) title.setText(camera.name);
			if(null!=subTitle) subTitle.setText(getResources().getString(R.string.CAMERA_BINDING_TITLE));
			if(null!=buttonSetSite)
				buttonSetSite.setSelected(null!=selSite);
				buttonSetSite.setOnButtonClickListener(action -> {
					showSiteChooserDialog();
				});

			if(null!=buttonSetZone){
				buttonSetZone.setSelected(null!=selZone);
				buttonSetZone.setOnButtonClickListener(action -> {
					if (selSite != null)
					{
						showZoneChooserDialog();
					} else
					{
						Func.nShowMessage(context, getString(R.string.CAMERA_MESSAGE_CHOOSE_OBJECT));
					}
				});
			}


			add.setDisable(null==selSite || null == selZone);

			add.setOnButtonClickListener(action ->
			{
				UserInfo userInfo = dbHelper.getUserInfo();
				JSONObject message = new JSONObject();
				try
				{
					message.put("id", camera.id);
					message.put("device", selZone.device_id);
					message.put("section", selZone.section_id);
					message.put("zone", selZone.id);
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
				D3Request d3Request = D3Request.createMessage(userInfo.roles, "IV_BIND_CAM", message);
				if (d3Request.error == null)
				{
					sendMessageToServer(d3Request);
				} else
				{
					Func.pushToast(context, d3Request.error, (Activity) context);
				}
			});
		}
	}

	private void showSiteChooserDialog()
	{
		showSites();
	}

	private void showZoneChooserDialog()
	{
		showZones();
	}

	private void showZones()
	{
		final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(context);

		View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
		TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.CAMERA_TITLE_CHOOSE_ZONE);
		ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
		listView.setDivider(null);
		if(null!=selSite)
		{
			Zone[] zones = dbHelper.getZonesNotBindedToCam(selSite.id);
			if (null != zones)
			{
				ZonesListAdapter zonesListAdapter = new ZonesListAdapter(context, R.layout.n_card_for_list, zones, null!=selZone ? selZone.id : -1);
				listView.setAdapter(zonesListAdapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
				{
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id)
					{
						Zone zone = (Zone) parent.getItemAtPosition(position);
						setZone(zone);
						bottomSheetDialog.dismiss();
					}
				});
			}
		}

		bottomSheetDialog.setContentView(bottomView);
		bottomSheetDialog.show();
	}

	public  void showSites(){
		final NBottomSheetDialog bottomSheetDialog = new NBottomSheetDialog(context);

		View bottomView = ((Activity)context).getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_big, null);
		TextView title = (TextView) bottomView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.CAMERA_TITLE_CHOOSE_OBJECT);
		ListView listView = (ListView) bottomView.findViewById(R.id.dialogBottomList);
		listView.setDivider(null);
		Site[] sites = dbHelper.getAllSitesArray();
		if(null!=sites) {
			SitesListAdapter sitesListAdapter = new SitesListAdapter(context, R.layout.n_card_for_list, sites, null!=selSite ? selSite.id : -1);
			listView.setAdapter(sitesListAdapter);
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
			{
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					Site site = (Site) parent.getItemAtPosition(position);
					setSite(site);
					bottomSheetDialog.dismiss();
				}
			});
		}

		bottomSheetDialog.setContentView(bottomView);
		bottomSheetDialog.show();
	}

	public void setSite(Site site)
	{
		this.selSite = site;
		if(null!=buttonSetSite)
		{
			buttonSetSite.setSelected(null != selSite);
			buttonSetSite.setTitle(null!=selSite ? selSite.name : getResources().getString(R.string.CAMERA_BIND_SELECT));
		}
		setZone(null);
	}

	public void setZone(Zone zone){
		this.selZone = zone;
		if(null!=buttonSetZone){
			buttonSetZone.setSelected(null!=selZone);
			buttonSetZone.setTitle(null!=selZone ? selZone.name : getResources().getString(R.string.CAMERA_BIND_SELECT));
		}
		add.setDisable(null==selSite || null == selZone);
	}

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};


	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		if(from == Const.OPEN_IV_FROM_SETTINGS) overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}
}
