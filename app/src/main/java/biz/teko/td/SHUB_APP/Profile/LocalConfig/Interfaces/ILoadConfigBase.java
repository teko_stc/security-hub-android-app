package biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces;

public interface ILoadConfigBase {
    void showProgressBar();
    void dismissProgressBar(boolean successful);
}
