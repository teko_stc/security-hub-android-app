package biz.teko.td.SHUB_APP.Devices_N.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.CursorJoiner;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Devices_N.Fragments.DevicesListFragment;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Sections_N.Activities.SectionActivity;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Models.DeviceInfo;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class SectionsListRecyclerAdapter extends RecyclerView.Adapter<DeviceViewHolder> {

	private final CompositeDisposable disposables = new CompositeDisposable();

	private final Context context;
	private final DevicesListFragment devicesListFragment;
	private final DBHelper dbHelper;
	private final UserInfo userInfo;
	private final int sending = 0;
	private boolean synchronous = true;
	private Cursor cursor;

	Subject<Pair<DeviceInfo, NWithAButtonElement>> deviceInformationScreenSubject =
			PublishSubject.create();

	public SectionsListRecyclerAdapter(Context context, Cursor c, DevicesListFragment devicesListFragment) {
		super();
		this.cursor = c;
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.devicesListFragment = devicesListFragment;
		this.userInfo = dbHelper.getUserInfo();
	}

	@Override
	public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
		super.onAttachedToRecyclerView(recyclerView);
		disposables.add(deviceInformationScreenSubject
				.throttleFirst(200, TimeUnit.MILLISECONDS)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe((pair)->
						openSectionActivity(pair.first.getSite(), pair.first.getSection(),
								pair.second)));
	}

	@Override
	public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
		super.onDetachedFromRecyclerView(recyclerView);
		disposables.dispose();
	}

	private void showControlDialog(NWithAButtonElement elementView, Section section, Site site, int layout)
	{
		NDialog nDialog = new NDialog(context, layout);
		NDialog finalNDialog = nDialog;
		finalNDialog.setTitle(section.name);
		if(null!=site) finalNDialog.setSubTitle(site.name);
		NDialog finalNDialog1 = nDialog;
		finalNDialog.setOnActionClickListener((value, b) -> {
			switch (value){
				case NActionButton.VALUE_ARM:
					try
					{
						JSONObject commandAddress = new JSONObject();
						commandAddress.put("section",section.id);
						commandAddress.put("zone", 0);
						sendMessageToServer(section, Command.ARM, commandAddress,true);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					finalNDialog1.dismiss();
					break;
				case NActionButton.VALUE_DISARM:
					try
					{
						JSONObject commandAddress = new JSONObject();
						commandAddress.put("section",section.id);
						commandAddress.put("zone", 0);
						sendMessageToServer(section, Command.DISARM, commandAddress, true);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					finalNDialog1.dismiss();
					break;
				case NActionButton.VALUE_INFO:
					requestSectionActivity(site, section, elementView);
					finalNDialog.dismiss();
					break;
			}
		});
		finalNDialog.show();
		setSynchronous(true);
	}
	public boolean isSynchronous() {
		return synchronous;
	}

	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}
	private void requestSectionActivity(Site site, Section section, NWithAButtonElement elementView)
	{
		deviceInformationScreenSubject.onNext(
			new Pair<>(new DeviceInfo(null, null, null, site, section), elementView));
	}

	private void openSectionActivity(Site site, Section section, NWithAButtonElement elementView)
	{
		Intent intent = new Intent(context, SectionActivity.class);
		intent.putExtra("bar_title", section.name);
		intent.putExtra("site", site.id);
		intent.putExtra("device", section.device_id);
		intent.putExtra("section", section.id);
		intent.putExtra("transition", true);
		intent.putExtra("transition_start", getTransitionStartSize(elementView));
		context.startActivity(intent, getTransitionOptions(elementView).toBundle());
	}

	private boolean compareCursors(Cursor cursor1, Cursor cursor2 ){

		CursorJoiner joiner = new CursorJoiner(cursor1, cursor1.getColumnNames() , cursor2, cursor2.getColumnNames() );
		for (CursorJoiner.Result joinerResult : joiner) {
			switch (joinerResult) {
				case LEFT:
					return true;
				case RIGHT:
					return true;
			}
		}
		return false;
	}

	public void update(Cursor cursor) {
		//if(compareCursors(this.cursor, cursor)) {
		this.cursor = (cursor);
		notifyDataSetChanged();
		//}
	}

	private int getTransitionStartSize(View view){
		return  Func.getTextSize(context, ((NWithAButtonElement)view).getTitleView());
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((Activity) context,
				new Pair<>(((NWithAButtonElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	public boolean sendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		if(context != null){
			devicesListFragment.nSendMessageToServer(d3Element, Q, data, command);
		}
		return false;
	}

	@NonNull
	@Override
	public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return DeviceViewHolder.newInstance(parent);
	}

	@Override
	public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {
		if(!synchronous) {
			Single<DeviceInfo> deviceSubject = Single.create(
					emitter -> {
						try {
							emitter.onSuccess(fetchDeviceInfo(position));
						} catch (Throwable th) {
							if (!emitter.isDisposed())
								emitter.onError(th);
						}
					}
			);
			disposables.add(deviceSubject.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe((DeviceInfo deviceInfo) ->
			{
				applyToView(deviceInfo, holder);
			}));
		} else {
			applyToView(fetchDeviceInfo(position), holder );
		}
	}
	public DeviceInfo fetchDeviceInfo(int position){
		cursor.moveToPosition(position);
		Section section = new Section(cursor);
		Site site = dbHelper.getSiteByDeviceSection(section.device_id, section.id);
		LinkedList<Event> affects = dbHelper.nGetAffectsBySection(section.device_id, section.id);
		return new DeviceInfo.DeviceInfoBuilder().setAffects(affects).setSite(site).setSection(section).createDeviceInfo();
	}
	public void applyToView(DeviceInfo deviceInfo, RecyclerView.ViewHolder holder){
		SType sType = deviceInfo.getSection().getType();
		NWithAButtonElement elementView = holder.itemView.findViewById(R.id.nDevicesListElement);
//			NDevicesListElementView elementView = (NDevicesListElementView) view;
		elementView.refreshStates();
		elementView.setTitle(deviceInfo.getSection().name);
		if (null != deviceInfo.getSite())
			elementView.setSubtitle(deviceInfo.getSite().name);
		if (null != sType) {
			elementView.setDescImage(context.getResources().getDrawable(sType.getListIcon(context)));
			switch (sType.id) {
				case Const.SECTIONTYPE_GUARD:
					if (deviceInfo.getSection().armed > 0) {
						elementView.setArmed(true);
					} else {
						elementView.setDisarmed(true);
					}
					break;
				case Const.SECTIONTYPE_TECH_CONTROL:
					if (deviceInfo.getSection().armed > 0) {
						elementView.setControl(true);
					} else {
						elementView.setNoControl(true);
					}
					break;
				default:
					break;
			}
		} else {
			elementView.setDescImage(context.getResources().getDrawable(R.drawable.ic_section_common_list));
		}
		elementView.setOnChildClickListener((action, option) -> {
			if (null != sType) {
				switch (sType.id) {
					case Const.SECTIONTYPE_GUARD:
						showControlDialog(elementView,
								deviceInfo.getSection(), deviceInfo.getSite(), R.layout.n_dialog_arm);
						break;
					case Const.SECTIONTYPE_TECH_CONTROL:
						showControlDialog(elementView,
								deviceInfo.getSection(), deviceInfo.getSite(), R.layout.n_dialog_control);
						break;
					default:
						requestSectionActivity(deviceInfo.getSite(), deviceInfo.getSection(), elementView);
						break;
				}
			} else {
				requestSectionActivity(deviceInfo.getSite(), deviceInfo.getSection(), elementView);
			}
		});
		elementView.setOnRootClickListener(() ->
				requestSectionActivity(deviceInfo.getSite(), deviceInfo.getSection(), elementView));
		elementView.setCritStates(deviceInfo.getAffects());
	}

	@Override
	public int getItemCount() {
		return cursor.getCount();
	}
}
