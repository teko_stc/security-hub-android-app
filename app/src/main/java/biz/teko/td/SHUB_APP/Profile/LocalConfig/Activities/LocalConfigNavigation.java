package biz.teko.td.SHUB_APP.Profile.LocalConfig.Activities;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceConfigDto;
import biz.teko.td.SHUB_APP.Profile.LocalConfig.DeviceDTO;
import biz.teko.td.SHUB_APP.UDP.SocketConnection;

public abstract class LocalConfigNavigation extends BaseActivity {
    public abstract void openDeviceConfig(DeviceDTO deviceDTO);
    public abstract void openDeviceList();
    public abstract void openConfigCustomization(DeviceConfigDto deviceConfig);
    public abstract void openFactoryResetConfirmation(DeviceDTO deviceConfig);
    public abstract void showNoWifiDialog();
    public abstract SocketConnection getConnection();
}
