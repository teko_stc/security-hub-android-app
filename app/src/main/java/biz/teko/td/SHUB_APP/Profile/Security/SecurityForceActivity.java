package biz.teko.td.SHUB_APP.Profile.Security;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 29.08.2017.
 */

public class SecurityForceActivity extends BaseActivity
{
	private Context context;
	private DBHelper dbHelper;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_security_force);

		context = this;
		dbHelper = DBHelper.getInstance(context);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		toolbar.setTitle(R.string.SECFA_TITLE);
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		toolbar.setNavigationOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onBackPressed();
			}
		});

		getFragmentManager().beginTransaction().replace(R.id.prefContainer, new SecurityForceObjectsFragment()).commit();
	}

	@Override
	public void onBackPressed()
	{
		if(getFragmentManager().getBackStackEntryCount() > 0){
			getFragmentManager().popBackStack();
		}else
		{
			finish();
		}
	}
}
