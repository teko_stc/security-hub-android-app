package biz.teko.td.SHUB_APP.MainTabs.Adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.MainBar;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.SectionGroup;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.NViews.NMainBarElement;

public class MainGridAdapter extends CursorAdapter
{
	private OnGridElementClickListener onGridElementClickListener;

	private final boolean shared;
	private final Context context;
	private final DBHelper dbHelper;

	public MainGridAdapter(Context context, Cursor c, boolean shared, boolean autoRequery, OnGridElementClickListener onGridElementClickListener) {
		super(context, c, autoRequery);
		this.shared = shared;
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.onGridElementClickListener = onGridElementClickListener;
	}

	public boolean isShared() {
		return shared;
	}

	public void update(Cursor cursor) {
		this.changeCursor(cursor);
	}

	@Override
	public int getCount() {
		final int count = super.getCount();
		if (shared)
			return count + 1;
		else return count;
	}

	@Override
	public int getItemViewType(int position) {
		if (shared && position == getCount() - 1) {
			return MainBar.Type.EMPTY.getId();
		} else {
			Cursor cursor = (Cursor) getItem(position);
			return getItemViewType(cursor);
		}

	}

	private int getItemViewType(Cursor cursor) {
		MainBar.Type type = MainBar.Type.valueOf(cursor.getString(1));
		return type.getId();
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if (shared && position == getCount() - 1) {
			MainBar bar;
			if (getCount() == 1) {
				bar = new MainBar(0, 0);
			} else {
				MainBar prevBar = new MainBar((Cursor) getItem(position - 1));
				if (prevBar.x == 0)
					bar = new MainBar(1, prevBar.y);
				else
					bar = new MainBar(0, prevBar.y + 1);
			}
			NMainBarElement mainBarView = getBarView(bar);

			mainBarView.setOnElementClickListener(new NMainBarElement.OnElementClickListener() {
				@Override
				public void onClick() {
					onGridElementClickListener.onEmptyElementClick();
				}

				@Override
				public void onLongClick() {

					//onGridElementClickListener.onElementLongClick(finalBar.id);
				}
			});

			mainBarView.setVibro(true);
			mainBarView.acceptStates();

			return mainBarView;

		} else {
			MainBar bar = new MainBar((Cursor) getItem(position));
			if (shared && bar.type == MainBar.Type.EMPTY)
				return null;

			if (null != bar) {
				NMainBarElement mainBarView = getBarView(bar);

				if (bar.type != MainBar.Type.EMPTY) {

					int bigImage = bar.type.getIconMain();
//				int bigImage = R.drawable.n_background_disable;

					String title = null;
					String subTitle = null;
					String siteTitle = null;

					int armStatus = Const.STATUS_NO_ARM_CONTROL;
					int controlStatus = Const.STATUS_NO_CONTROL;

					LinkedList<Event> affects = new LinkedList<>();

					switch (bar.type) {
						case ARM:
							bigImage = bar.subtype.getIconMain();
							switch (bar.subtype) {
								case SITE:
									Site site = dbHelper.getSiteById(Integer.valueOf(bar.element_id));
									if (null != site) {
										if (shared) {
											siteTitle = site.name;
										}
										title = context.getString(R.string.N_MAIN_TITLE_ALL_SITE);
										armStatus = dbHelper.getSiteArmStatus(site.id);
										affects = dbHelper.nGetAffectsBySiteId(site.id);
									} else {
										mainBarView = dropBar(bar);
									}
									break;
								case DEVICE:
									Device device = dbHelper.getDeviceById(Integer.valueOf(bar.element_id));
									if (null != device) {
                                        DeviceType deviceType = device.getType();
                                        if (null != deviceType) {
                                            title = deviceType.caption;
                                            if (null != deviceType.icons) {
                                                bigImage = deviceType.getMainIcon(context);
                                            }
                                        }
                                        if (shared) {

											site = dbHelper.getSiteByDeviceId(device.id);
											if(null!=site){
												siteTitle = site.name;
											} else{
												mainBarView = dropBar(bar);
											}
                                        }
                                        armStatus = dbHelper.getDeviceArmStatus(device.id);
                                        /*get states*/
                                        affects = dbHelper.getAffectsByDeviceId(device.id);
                                    } else {
										mainBarView = dropBar(bar);
									}
									break;
								case GROUP:
									SectionGroup sectionGroup = dbHelper.getSectionGroup(Integer.valueOf(bar.element_id));
									if (null != sectionGroup) {
										if (shared && sectionGroup.sections.length > 0) {
											site = dbHelper.getSiteByDeviceId(sectionGroup.sections[0].device_id);
											if(null!=site){
												siteTitle = site.name;
											} else{
												mainBarView = dropBar(bar);
											}
										}
										title = sectionGroup.name;
										armStatus = sectionGroup.getArmed();
									} else {
										mainBarView = dropBar(bar);
									}
									break;
								case SECTION:
									try {
										JSONObject idjson = new JSONObject(bar.element_id);
										if (null != idjson) {
											int device_id = idjson.getInt("device");
											int section_id = idjson.getInt("section");
											Section section = dbHelper.getDeviceSectionById(device_id, section_id);

											if (null != section) {
                                                if (shared) {
													site = dbHelper.getSiteByDeviceId(section.device_id);
													if(null!=site){
														siteTitle = site.name;
													} else{
														mainBarView = dropBar(bar);
													}
                                                }
                                                bigImage = R.drawable.ic_section_common_main;
                                                title = section.name;
                                                SType sType = section.getType();
                                                if (null != sType && null != sType.icons) {
                                                    bigImage = sType.getMainIcon(context);
                                                }
                                                switch (section.detector) {
                                                    case Const.SECTIONTYPE_FIRE:
                                                    case Const.SECTIONTYPE_TECH:
                                                    case Const.SECTIONTYPE_FIRE_DOUBLE:
													case Const.SECTIONTYPE_ALARM:
														armStatus = Const.STATUS_NO_ARM_CONTROL;
														subTitle = context.getString(R.string.N_MAIN_SUBTITLE_24H);
														break;
													case Const.SECTIONTYPE_GUARD:
														armStatus = section.armed > 0 ? Const.STATUS_ARMED : Const.STATUS_DISARMED;
														break;
													case Const.SECTIONTYPE_TECH_CONTROL:
														armStatus = section.armed > 0 ? Const.STATUS_NO_ARM_CONTROL : Const.STATUS_NO_SECTION_CONTROL;
														break;
												}
												/*get states*/
												affects = dbHelper.nGetAffectsBySection(section.device_id, section.id);
											} else {
												mainBarView = dropBar(bar);
											}
										}
									} catch (JSONException e) {
										e.printStackTrace();
									}
									break;
							}

							break;
						case CAMERA:
							try {
								JSONObject jsonObject = new JSONObject(bar.element_id);
								switch (Camera.CType.valueOf(jsonObject.getString("type"))) {
									case IV:
										Camera camera = dbHelper.getIVCameraById(jsonObject.getString("id"));
										if (null != camera) {
											title = camera.name;
										} else {
											mainBarView = dropBar(bar);
										}
										break;
									case RTSP:
										camera = dbHelper.getRTSPCameraById(Integer.valueOf(jsonObject.getString("id")));
										if (null != camera) {
											title = camera.name;
										} else {
											mainBarView = dropBar(bar);
										}
										break;
									case DAHUA:
										camera = dbHelper.getDahuaCameraBySn(jsonObject.getString("id"));
										if(null!=camera){
											title = camera.name;
										}else{
											mainBarView  = dropBar(bar);
										}
										break;
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
							break;
						case CONTROL:
						case DEVICE:
							try {
								JSONObject idjson = new JSONObject(bar.element_id);
								Zone zone = dbHelper.getZoneById(idjson.getInt("device"), idjson.getInt("section"), idjson.getInt("zone"));
								if (null != zone) {
                                    if (shared) {
                                        TextView siteNameView = mainBarView.getRootView().findViewById(R.id.nTextBarSiteTitle);
                                        siteNameView.setVisibility(View.VISIBLE);
                                        String siteName = dbHelper.getSiteByDeviceId(zone.device_id).name;
                                        siteNameView.setText(siteName);
                                    }
                                    title = zone.name;
                                    if (bar.type != MainBar.Type.CONTROL) {
                                        switch (zone.getDetector()) {
                                            case Const.DETECTOR_KEYPAD:
                                            case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
                                                bigImage = R.drawable.n_image_keypad_main_selector;
                                                break;
                                            default:
                                                bigImage = R.drawable.n_image_device_common_main_selector;
                                                break;
										}
										if (zone.getModelId() == Const.SENSORTYPE_WIRED_INPUT) {
											DWIType dwiType = zone.getDWIType();
											if (null != dwiType && null != dwiType.icons) {
												bigImage = dwiType.getMainIcon(context);
												DType dType = dwiType.getDetectorType(zone.getDetector());
												if (null != dType) {
													AType aType = dType.getAlarmType(zone.getAlarm());
													if (null != aType && null != aType.icons) {
														bigImage = aType.getMainIcon(context);
													}
												}
											}
										} else if (zone.getModelId() == Const.SENSORTYPE_WIRED_OUTPUT) {
											DWOType dwoType = zone.getDWOType();
											if (null != dwoType && null != dwoType.icons) {
												bigImage = dwoType.getMainIcon(context);
											}
										} else {
											ZType zType = zone.getModel();
											if (null != zType && null != zType.icons) {
												bigImage = zType.getMainIcon(context);
												DType dType = zType.getDetectorType(zone.getDetector());
												if (null != dType) {
													AType aType = dType.getAlarmType(zone.getAlarm());
													if (null != aType && null != aType.icons) {
														bigImage = aType.getMainIcon(context);
													}
												}
											}
										}

										Section section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
										if (null != section && 0 != section.id) {
											switch (zone.getDetector()) {
												case Const.DETECTOR_LAMP:
												case Const.DETECTOR_SIREN:
												case Const.DETECTOR_SZO:
													//show device section name in subtitle for lamp, siren, szo
													subTitle = dbHelper.nGetSectionNameById(zone.device_id, zone.section_id);
												case Const.DETECTOR_KEYPAD:
												case Const.DETECTOR_KEYPAD_WITH_DISPLAY:
												case Const.DETECTOR_BIK:
													break;
												default:
													switch (section.detector) {
														case Const.SECTIONTYPE_FIRE:
														case Const.SECTIONTYPE_TECH:
														case Const.SECTIONTYPE_FIRE_DOUBLE:
														case Const.SECTIONTYPE_ALARM:
															break;
														case Const.SECTIONTYPE_TECH_CONTROL:
															armStatus = section.armed > 0 ? Const.STATUS_NO_ARM_CONTROL : Const.STATUS_NO_SECTION_CONTROL;
															break;
														case Const.SECTIONTYPE_GUARD:
														default:
															armStatus = section.armed > 0 ? Const.STATUS_ARMED : Const.STATUS_DISARMED;
															break;
													}
													break;
											}
										}
									} else {
										bigImage = R.drawable.ic_relay_main;

										if (zone.getModelId() == Const.SENSORTYPE_WIRED_OUTPUT) {
											DWOType dwoType = zone.getDWOType();
											if (null != dwoType && null != dwoType.icons) {
												bigImage = dwoType.getMainIcon(context);
											}
										} else {
											ZType zType = zone.getModel();
											if (null != zType && null != zType.icons) {
												bigImage = zType.getMainIcon(context);
											}
										}
										switch (zone.detector) {
											case Const.DETECTOR_AUTO_RELAY:
												//set sctipt icon only if it exist. if not - set auto overlay
												int sctiptIcon = 0;
												Script script = null;
												if (dbHelper.getScriptBindedStatusForRelay(zone)) {
													script = dbHelper.getDeviceScriptsByBind(zone.device_id, zone.id);
													if (null != script)
														sctiptIcon = script.getIcon();
												}
												if (0 != sctiptIcon) {
													bigImage = sctiptIcon;
												} else {
													if (null != script) {
														mainBarView.setScripted(true);
													} else {
														mainBarView.setAuto(true);
													}
												}
												if (zone.getModelId() == Const.SENSORTYPE_WIRED_OUTPUT) {
													break; //to avoid status set
												}
											case Const.DETECTOR_MANUAL_RELAY:
												armStatus = dbHelper.getRelayStatusById(zone.device_id, zone.section_id, zone.id);
												break;
											default:
												break;
										}
									}

									/*get states*/
									affects = dbHelper.getAffectsByZoneId(bar.site_id, zone.device_id, zone.section_id, zone.id);
								} else {
									mainBarView = dropBar(bar);
								}
							} catch (JSONException e) {
								e.printStackTrace();
							}
							break;
					}

					mainBarView.setTitle(title);
					mainBarView.setImageBig(bigImage);
					mainBarView.setType(bar.type);

					/*states*/
					switch (armStatus) {
						case Const.STATUS_DISARMED:
							mainBarView.setDisarmed(true);
							subTitle = context.getString(R.string.N_MAIN_SUBTITLE_DISARMED);
							break;
						case Const.STATUS_ARMED:
							mainBarView.setArmed(true);
							subTitle = context.getString(R.string.N_MAIN_SUBTITLE_ARMED);
							break;
						case Const.STATUS_PARTLY_ARMED:
							mainBarView.setPartlyArmed(true);
							subTitle = context.getString(R.string.N_MAIN_SUBTITLE_PARTLY_ARMED);
							break;
						case Const.STATUS_NO_SECTION_CONTROL:
							mainBarView.setNoControl(true);
							subTitle = context.getString(R.string.N_MAIN_SUBTITLE_NO_SECTION_CONTROL);
							break;
						case Const.STATUS_OFF:
							mainBarView.setOff(true);
							subTitle = context.getString(R.string.N_MAIN_SUBTITLE_OFF);
							break;
						case Const.STATUS_ON:
							mainBarView.setOn(true);
							subTitle = context.getString(R.string.N_MAIN_SUBTITLE_ON);
							break;
						default:
							mainBarView.setNoArm(true);
							break;
					}
					mainBarView.setSitetitle(siteTitle);
					mainBarView.setSubtitle(subTitle);
					mainBarView.setStates(affects);
				}

				MainBar finalBar = bar;
				mainBarView.setOnElementClickListener(new NMainBarElement.OnElementClickListener() {
					@Override
					public void onClick() {
						onGridElementClickListener.onElementClick(finalBar.id);
					}

					@Override
					public void onLongClick() {
						onGridElementClickListener.onElementLongClick(finalBar.id);
					}
				});

				mainBarView.setVibro(true);
				mainBarView.acceptStates();

				return mainBarView;

			}
		}

		return view;
	}

	@Override
	public int getViewTypeCount() {
		return MainBar.Type.values().length;
	}

	private int getLayout(MainBar bar) {
		if (bar.type == MainBar.Type.EMPTY) {
			return R.layout.n_main_bar_grid_element_empty_view;
		} else if (shared) {
			return R.layout.n_main_bar_grid_element_shared_view;
		} else
			return R.layout.n_main_bar_grid_element_default_view;
	}

	private NMainBarElement dropBar(MainBar bar) {
		dbHelper.deleteMainBar(bar);
//		if(null!=onGridElementClickListener)onGridElementClickListener.onRefresh();
		return getBarView(new MainBar(bar.x, bar.y));
	}

	private NMainBarElement getBarView(MainBar bar) {
		return new NMainBarElement(context, getLayout(bar));
	}

	public interface OnGridElementClickListener {
		void onElementClick(int id);

		void onEmptyElementClick();

		void onElementLongClick(int id);

		void onRefresh();
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return null;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
	}

	private void setTextViewText(View view, Context context, int id, String text)
	{
		TextView textView = (TextView) view.findViewById(id);
		if(null!=textView)textView.setText(text);
	}

	private void setVisiblity(View convertView, Context context, int id, boolean b)
	{
		View view = (View) convertView.findViewById(id);
		if(null!=view)view.setVisibility(b? View.VISIBLE: View.GONE);
	}

	private void setImageViewImage(View view, Context context, int id, int src)
	{
		ImageView imageView = (ImageView) view.findViewById(id);
		if(null!=imageView)imageView.setImageDrawable(context.getResources().getDrawable(src));
	}

}
