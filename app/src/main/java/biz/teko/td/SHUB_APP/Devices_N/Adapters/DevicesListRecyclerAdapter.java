package biz.teko.td.SHUB_APP.Devices_N.Adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.util.Pair;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.AType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWIType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DWOType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.DeviceType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.Devices_N.Activities.DeviceActivity;
import biz.teko.td.SHUB_APP.Devices_N.Fragments.Filter;
import biz.teko.td.SHUB_APP.Devices_N.ServerConnection;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Relays_N.Activities.RelayActivity;
import biz.teko.td.SHUB_APP.Utils.Device.DeviceConfigHelper;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.Models.DeviceInfo;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Zones_N.Activities.ZoneActivity;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class DevicesListRecyclerAdapter extends RecyclerView.Adapter<DeviceViewHolder> {
	private final DBHelper dbHelper;
	private final UserInfo userInfo;
	private Cursor cursor;
	private final Context context;
	private Filter filter = new Filter("");
	private final ServerConnection serverConnection;
	private final boolean transition;
	private final DeviceConfigHelper deviceConfigHelper = new DeviceConfigHelper();
	private final boolean device_show;

	private boolean synchronous = true;

	public Filter getFilter() {
		return filter;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
	}

	public Cursor getCursor() {
		return cursor;
	}

	Subject<Pair<DeviceInfo, NWithAButtonElement>> deviceInformationScreenSubject =
			PublishSubject.create();

	private final CompositeDisposable disposables = new CompositeDisposable();

	@Override
	public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
		super.onAttachedToRecyclerView(recyclerView);
		disposables.add(deviceInformationScreenSubject
				.throttleFirst(200, TimeUnit.MILLISECONDS)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe((pair)->
						openActivity(pair.first.getSite(), pair.first.getDevice(),
						pair.first.getZone(), pair.second)));
	}

	@Override
	public int getItemCount() {
		return cursor.getCount();
	}


	@NonNull
	@Override
	public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		return DeviceViewHolder.newInstance(parent);

	}
	private void openActivity(Site site, Device device, Zone zone, NWithAButtonElement elementView){
		if(zone.section_id == 0 && (zone.id == 0 || zone.id == 100)){
			if(device_show)
				openDeviceActivity(site, device, zone, elementView);
		}else{
			switch (zone.detector){
				case 13:
				case 23:
					openRelayActivity(site, device, zone, elementView);
					break;
				default:
					openZoneActivity(site, device,  zone, elementView);
					break;
			}
		}
	}

	@Override
	public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
		super.onDetachedFromRecyclerView(recyclerView);
		disposables.dispose();
	}

	@Override
	public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {
		NWithAButtonElement elementView = holder.itemView.findViewById(R.id.nDevicesListElement);
		if (!synchronous) {
			Single<DeviceInfo> deviceSubject = Single.create(
					emitter -> {
						try {
							emitter.onSuccess(fetchData(position));
						} catch (Throwable th) {
							if (!emitter.isDisposed())
								emitter.onError(th);
						}
					}
			);
			disposables.add(
					deviceSubject.subscribeOn(Schedulers.io())
							.observeOn(AndroidSchedulers.mainThread())
							.subscribe((DeviceInfo deviceInfo) ->
									applyDeviceInfoFiltered(deviceInfo, elementView, filter)));
		} else {
			applyDeviceInfoFiltered(fetchData(position), elementView, filter);
		}
	}
	public boolean isSynchronous() {
		return synchronous;
	}

	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}

	private void applyDeviceInfoFiltered(DeviceInfo deviceInfo, NWithAButtonElement elementView, Filter filter) {
		if (deviceInfo.getZone() != null) {
			String query = filter.getQuery().toLowerCase();
			String subtitle = null != deviceInfo.getSite() ? deviceInfo.getSite().name
					: context.getResources().getString(R.string.SITE_NULL);
			String title = getTitle(deviceInfo.getZone(), deviceInfo.getDevice());
			if (deviceConfigHelper.checkIfController(deviceInfo) &&
					!query.isEmpty()) {
				if (!(subtitle.toLowerCase().contains(query)
						|| title.toLowerCase().contains(query))) {
					elementView.setVisibility(View.GONE);
					return;
				}
			}
			elementView.setVisibility(View.VISIBLE);
			setArmStatus(elementView, deviceInfo.getZone(), deviceInfo.getDevice());
			elementView.setTitle(title);
			elementView.setSubtitle(subtitle);
			elementView.setActionImageNotAnim(!((deviceInfo.getZone().isManualControllable() ||
					deviceInfo.getZone().id == 0 || deviceInfo.getZone().id == 100)
					&& deviceInfo.getZone().section_id == 0));
			elementView.setDescImage(getDescImage(deviceInfo.getDevice(), deviceInfo.getZone()));
			elementView.setOnChildClickListener((action, option) -> {
				if ((deviceInfo.getZone().id == 0 || deviceInfo.getZone().id == 100) && deviceInfo.getZone().section_id == 0) {
					showDialog(deviceInfo.getSite(), deviceInfo.getDevice(), deviceInfo.getZone(), elementView, R.layout.n_dialog_arm);
				} else {
					if (deviceInfo.getZone().isManualControllable()) {
						showDialog(deviceInfo.getSite(), deviceInfo.getDevice(), deviceInfo.getZone(), elementView, R.layout.n_dialog_on_off);
					} else {
						addActivityOpenRequest(deviceInfo, elementView);
					}
				}
			});

			elementView.setCritStates(deviceInfo.getAffects());
			elementView.setOnRootClickListener(() ->
					addActivityOpenRequest(deviceInfo, elementView));

		}
	}
	private DeviceInfo fetchData(int position){
		cursor.moveToPosition(position);
		Zone zone = new Zone(cursor, cursor.getInt(14));
		Site site = dbHelper.getSiteById(zone.site);
		Device device = dbHelper.getDeviceById(zone.device_id);
		LinkedList<Event> affects =
				dbHelper.nGetAffectsByZone(zone.device_id, zone.section_id, zone.id);
		return new DeviceInfo.DeviceInfoBuilder()
				.setZone(zone)
				.setAffects(affects)
				.setDevice(device)
				.setSite(site)
				.createDeviceInfo();

	}



	public DevicesListRecyclerAdapter(Context context, Cursor c, ServerConnection serverConnection, boolean transition, boolean device_show)
	{
		super();
		this.cursor = c;
		this.context = context;
		this.dbHelper = DBHelper.getInstance(context);
		this.serverConnection = serverConnection;
		this.userInfo = dbHelper.getUserInfo();
		this.transition = transition;
		this.device_show = device_show; // for nr list
	}

	public void update(Cursor cursor)
	{
		this.cursor = cursor;
		notifyDataSetChanged();
	}

	private Drawable getDescImage(Device device, Zone zone)
	{
		int descImage;
		if((zone.id == 0 || zone.id ==100) && zone.section_id == 0){
			descImage = R.drawable.n_image_controller_common_list_selector;
			if(null!=device){
				DeviceType deviceType = device.getType();
				if(null!=deviceType){
					if(null!=deviceType.icons){
						descImage = deviceType.getListIcon(context);
					}
				}
			}
		}else{
			descImage = R.drawable.n_image_device_common_list_selector; // TODO set common image for sensor
			switch (zone.getDetector()){
				case Const.DETECTOR_AUTO_RELAY:
				case Const.DETECTOR_MANUAL_RELAY:
					descImage = R.drawable.n_image_relay_list_selector;
					break;
				case Const.DETECTOR_SIREN:
					descImage = R.drawable.n_image_siren_list_selector;
					break;
				case Const.DETECTOR_LAMP:
					descImage = R.drawable.n_image_lamp_list_selector;
					break;
				case Const.DETECTOR_KEYPAD:
					descImage = R.drawable.n_image_keypad_list_selector;
					break;
				default:
					break;
			}
			int model = zone.getModelId();
			switch (model){
				case Const.SENSORTYPE_WIRED_OUTPUT:
					DWOType dwoType = zone.getDWOType();
					if (null != dwoType && null != dwoType.icons)
					{
						descImage = dwoType.getListIcon(context);
					}else if(zone.detector != Const.DETECTOR_AUTO_RELAY && zone.detector != Const.DETECTOR_MANUAL_RELAY){
						descImage = R.drawable.n_image_wired_output_common_list_selector;
					}
					break;
				case Const.SENSORTYPE_WIRED_INPUT:
					DWIType dwiType = zone.getDWIType();
					if (null != dwiType && null != dwiType.icons)
					{
						descImage = dwiType.getListIcon(context);
						DType dType = dwiType.getDetectorType(zone.getDetector());
						if(null!=dType){
							AType aType = dType.getAlarmType(zone.getAlarm());
							if(null!=aType && null!=aType.icons){
								descImage = aType.getListIcon(context);
							}
						}
					}else{
						descImage = R.drawable.n_image_wired_input_common_list_selector;
					}
				case Const.SENSORTYPE_UNKNOWN:
					break;
				default:
					ZType zType = zone.getModel();
					if (null != zType && null != zType.icons)
					{
						descImage = zType.getListIcon(context);
						DType dType = zType.getDetectorType(zone.getDetector());
						if(null!=dType){
							AType aType = dType.getAlarmType(zone.getAlarm());
							if(null!=aType && null!=aType.icons){
								descImage = aType.getListIcon(context);
							}
						}
					}else{
						descImage = R.drawable.n_image_device_common_list_selector;
					}
					break;
			}
		}

		return 0!=descImage ? context.getResources().getDrawable(descImage) : null;
	}

	private void showDialog(Site site, Device device, Zone zone, NWithAButtonElement elementView, int layout)
	{
		NDialog nDialog = new NDialog(context, layout);
		NDialog finalNDialog = nDialog;
		finalNDialog.setTitle(getTitle(zone, device));
		finalNDialog.setSubTitle(site.name);
		finalNDialog.setOnActionClickListener((value, b) -> {
			finalNDialog.dismiss();
			switch (value){
				case NActionButton.VALUE_ARM:
					sendMessageToServer(device, Command.ARM, new JSONObject(), true);
					break;
				case NActionButton.VALUE_DISARM:
					sendMessageToServer(device, Command.DISARM, new JSONObject(), true);
					break;
				case NActionButton.VALUE_ON:
					JSONObject commandAddress = new JSONObject();
					try
					{
						commandAddress.put("section", zone.section_id);
						commandAddress.put("zone", zone.id);
						commandAddress.put("state", 1);
						sendMessageToServer(zone, Command.SWITCH, commandAddress, true);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					break;
				case NActionButton.VALUE_OFF:
					commandAddress = new JSONObject();
					try
					{
						commandAddress.put("section", zone.section_id);
						commandAddress.put("zone", zone.id);
						commandAddress.put("state", 0);
						sendMessageToServer(zone, Command.SWITCH, commandAddress,true);
					} catch (JSONException e)
					{
						e.printStackTrace();
					}
					break;
				case NActionButton.VALUE_INFO:
					addActivityOpenRequest(
							new DeviceInfo(zone, null,  device, site, null), elementView);
					break;
			}
		});
		finalNDialog.show();
		setSynchronous(true);
	}

	private void setCritStatus(NWithAButtonElement elementView, Zone zone)
	{
		boolean alarm = dbHelper.nGetAlarmEventsByZone(zone);
		boolean attention = dbHelper.nGetAttentionEventsByZone(zone);
		if(alarm){
			elementView.setCrit(0);
		}
		else if(attention){
			elementView.setCrit(1);
		}else{
			elementView.setCrit(-1);
		}
	}

	private void setArmStatus(NWithAButtonElement elementView, Zone zone, Device device)
	{
		elementView.setNoArm(true);
		switch (device.type){
			case Const.DEVICETYPE_SH:
			case Const.DEVICETYPE_SH_1:
			case Const.DEVICETYPE_SH_1_1:
			case Const.DEVICETYPE_SH_2:
			case Const.DEVICETYPE_SH_4G:
				if(zone.section_id == 0 && (zone.id == 0 || zone.id == 100))
				{
					setDeviceArmStatus(elementView, device);
				}else{
					setZoneArmStatus(elementView, zone);
				}
				break;
			default:
				if(zone.section_id ==0 && zone.id == 0){
					setDeviceArmStatus(elementView, device);
				}else{
					setZoneArmStatus(elementView, zone);
				}
				break;
		}
	}

	private void setZoneArmStatus(NWithAButtonElement elementView, Zone zone)
	{
		switch (zone.getDetector()){
			case Const.DETECTOR_AUTO_RELAY:
				if(zone.getModelId() == Const.SENSORTYPE_WIRED_OUTPUT){
					if(dbHelper.getScriptBindedStatusForRelay(zone)){
						elementView.setScripted(true);
					}else{
						elementView.setAuto(true);
					}
					break;
				}
			case Const.DETECTOR_MANUAL_RELAY:
				int status = dbHelper.getRelayStatusById(zone.device_id, zone.section_id, zone.id);
				if(Const.STATUS_ON == status){
					elementView.setOn(true);
				}else{
					elementView.setOff(true);
				}
				break;
			case Const.DETECTOR_LAMP:
			case Const.DETECTOR_SIREN:
			case Const.DETECTOR_KEYPAD:
			case Const.DETECTOR_BIK:
				elementView.setNoArm(true);
				break;
			default:
				Section section = dbHelper.getDeviceSectionById(zone.device_id, zone.section_id);
				if(null!=section){
					SType sType = section.getType();
					if(null!=sType){
						switch (sType.id){
							case Const.SECTIONTYPE_GUARD:
								if(section.armed > 0){
									elementView.setArmed(true);
								}else{
									elementView.setDisarmed(true);
								}
								break;
							case Const.SECTIONTYPE_TECH_CONTROL:
								if(section.armed > 0){
									elementView.setControl(true);
								}else{
									elementView.setNoControl(true);
								}
								break;
							default:
								elementView.setNoArm(true);
								break;
						}
					}else{
						if(section.armed > 0){
							elementView.setArmed(true);
						}else{
							elementView.setDisarmed(true);
						}
					}
				}
				break;
		}
	}

	private void setDeviceArmStatus(NWithAButtonElement elementView, Device device)
	{
		switch (dbHelper.getDeviceArmStatus(device.id)){
			case Const.STATUS_DISARMED:
				elementView.setDisarmed(true);
				break;
			case Const.STATUS_ARMED:
				elementView.setArmed(true);
				break;
			case Const.STATUS_PARTLY_ARMED:
				elementView.setPartlyArmed(true);
				break;
		}
	}

	private String getTitle(Zone zone, Device device)
	{
		if(zone.section_id == 0 && (zone.id == 0 || zone.id == 100))
		{
			return device.getName();
		}else {
			return zone.name;
		}
	}

	/*public void openTutorialDevice(Cursor cursor, ListView listView) {
		Zone zone = null;
		if(null!=cursor) zone = new Zone(cursor, cursor.getInt(14));
		if(null != zone) {
			Site site = dbHelper.getSiteById(zone.site);
			Device device = dbHelper.getDeviceById(zone.device_id);
			NWithAButtonElement view = getView(0, null, listView).findViewById(R.id.nDevicesListElement);
			openDeviceActivity(site, device, zone, view);
		}
	}*/

	private void addActivityOpenRequest(DeviceInfo deviceInfo, NWithAButtonElement elementView)
	{
		deviceInformationScreenSubject.onNext(
				new Pair<>(deviceInfo, elementView));
	}

	private void openDeviceActivity(Site site, Device device, Zone zone, NWithAButtonElement elementView)
	{
		Intent intent = new Intent(context, DeviceActivity.class);
		intent.putExtra("bar_title", getTitle(zone, device));
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		if(transition){
			intent.putExtra("transition", true);
			intent.putExtra("transition_start", getTransitionStartSize(elementView));
			context.startActivity(intent, getTransitionOptions(elementView).toBundle());
		}else{
			context.startActivity(intent);
		}
	}


	private void openRelayActivity(Site site, Device device, Zone zone, NWithAButtonElement elementView)
	{
		Intent intent = new Intent(context, RelayActivity.class);
		intent.putExtra("bar_title", getTitle(zone, device));
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		if(transition){
			intent.putExtra("transition", true);
			intent.putExtra("transition_start", getTransitionStartSize(elementView));
			context.startActivity(intent, getTransitionOptions(elementView).toBundle());
		}else{
			context.startActivity(intent);
		}
	}

	private void openZoneActivity(Site site, Device device, Zone zone, NWithAButtonElement elementView)
	{
		Intent intent = new Intent(context, ZoneActivity.class);
		intent.putExtra("bar_title", getTitle(zone, device));
		intent.putExtra("site", site.id);
		intent.putExtra("device", zone.device_id);
		intent.putExtra("section", zone.section_id);
		intent.putExtra("zone", zone.id);
		if(transition){
			intent.putExtra("transition", true);
			intent.putExtra("transition_start", getTransitionStartSize(elementView));
			context.startActivity(intent, getTransitionOptions(elementView).toBundle());
		}else{
			context.startActivity(intent);
		}
	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((Activity) context,
				new Pair<>(((NWithAButtonElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	private int getTransitionStartSize(View view){
		return  Func.getTextSize(context, ((NWithAButtonElement)view).getTitleView());
	}

	public boolean sendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		Activity activity = (Activity) context;
		if(serverConnection != null && activity!= null){
			serverConnection.nSendMessageToServer(d3Element, Q, data, command);
		}
		return false;
	}
}
