package biz.teko.td.SHUB_APP.Wizards.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.Codes.Adapters.UserAddSectionsListAdapter;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.ZType;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Activity.Compoments.WizardUserSetActivity;

public class UserSetFragment extends Fragment
{
	private int position;
	private Context context;
	private ViewGroup container;
	private WizardUserSetActivity activity;
	private DBHelper dbHelper;
	private View v;
	private int setRegType;
	private int selDeviceId;
	private int selRegType;
	private String selSections;
	private String selName;
	private String selPass;
	private UserAddSectionsListAdapter userAddSectionsListAdapter;
	private Section[] sections;
	private LinkedList<Section> cSectionsList;
	private TextView sectionsText;
	private D3Service myService;
	private int sending = 0;
	private TextView textUserType;
	private TextView textInteractiveTitle;
	private ImageView imageInteractive;
	private Button buttonInteractive;
	private Button buttonNext;
	private LinearLayout sectionsLayout;
	private String selUID;
	private int selUIDFormat;
	private LinkedList<Section> selSectionsArray;


	public static UserSetFragment newInstance(int position){
		UserSetFragment userSetFragment = new UserSetFragment();
		Bundle b = new Bundle();
		b.putInt("position", position);
		userSetFragment.setArguments(b);
		return userSetFragment;
	}

	private BroadcastReceiver commandSendReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			sending = intent.getIntExtra("command_been_send", 0);
			if(sending!=1)
			{
				((WizardUserSetActivity)getActivity()).refreshViews();
			}
		}
	};

	private BroadcastReceiver interactiveReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(position == WizardUserSetActivity.PAGER_COUNT -2)
			{
				WizardUserSetActivity activity = (WizardUserSetActivity) getActivity();
				if (null != activity)
				{
					selRegType = activity.getRegType();
					if (selRegType == 1)
					{
						if (0 == intent.getIntExtra("status", 0))
						{
							String jdata = intent.getStringExtra("jdata");
							if(!jdata.equals("")){
								try
								{
									JSONObject jsonObject = new JSONObject(jdata);
									int uid_type = jsonObject.getInt("uid_type");
									if(uid_type == 2)
									{
										String uid = jsonObject.getString("uid");
										int uid_format = 0;
										if(null!=uid) uid_format = Integer.parseInt(uid.substring(0, 2), 16);
										if (null != textInteractiveTitle)
										{
											textInteractiveTitle.setText(R.string.WIZ_USER_BEEN_FOUND);
										}

										if (null != imageInteractive)
										{
											imageInteractive.setImageDrawable(context.getResources().getDrawable(R.drawable.success_image));
										}

										if (null != textUserType)
										{
											textUserType.setVisibility(View.VISIBLE);
											switch (uid_format){
												case 1:
													if (null != D3Service.d3XProtoConstEvent)
													{
														char[] chars = uid.toCharArray();
														int type = (int) Character.digit(chars[chars.length - 2 ],16);
														ZType zType = D3Service.d3XProtoConstEvent.getZTypeById(type);
														if(null!=zType){
															textUserType.setText(D3Service.d3XProtoConstEvent.getZTypeById(type).caption);
															if(null!=imageInteractive){
																imageInteractive.setImageResource(context.getResources().getIdentifier(zType.img, null, context.getPackageName()));
															}
														}
													}
													break;
												case 3:
													textUserType.setText("TOUCH MEMORY");
													break;
												default:
													textUserType.setText(uid);
													break;
											}

										}

										if (null != buttonNext)
										{
											buttonNext.setVisibility(View.VISIBLE);
										}

										if (null != buttonInteractive)
										{
											buttonInteractive.setText(R.string.WIZ_USER_RESET_INTERACTIVE_AND_REPEAT);
											buttonInteractive.setEnabled(true);
										}

										activity.setUID(uid);
										activity.setUIDFormat(uid_format);
									}else{
										activity.refreshViews();
										Func.nShowMessage(context, context.getString(R.string.WIZ_ERROR_ZONE_INSTEAD_USER));
									}

								} catch (JSONException e)
								{
									e.printStackTrace();
								}

							}else{
								activity.refreshViews();
								Func.nShowMessage(context, context.getString(R.string.WIZ_USER_INTERACTIVE_TIMEOUT));
							}
						}else{
							activity.refreshViews();
						}
					}
				}
			}
		}
	};

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		position = getArguments().getInt("position");
		int layout;
		context = container.getContext();
		this.container = container;
		activity = (WizardUserSetActivity) getActivity();
		dbHelper = DBHelper.getInstance(context);
		switch (position)
		{
			case 0:
				layout = R.layout.fragment_user_set_1;
				v = inflater.inflate(layout, container, false);
				setFragmentOne(v);
				break;
			case 1:
				if(null!=activity){
					setRegType = activity.getRegType();
				}
				if(setRegType == 0){
					layout = R.layout.fragment_user_set_2_1;
				}else{
					layout = R.layout.fragment_user_set_2_2;
				}
				v = inflater.inflate(layout, container, false);
				setFragmentTwo(v);
				break;
			case 2:
				/*wired&wireless*/
				layout = R.layout.fragment_user_set_3;
				v = inflater.inflate(layout, container, false);
				setFragmentThree(v);
				break;
			default:
				break;
		}

		return v;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		getActivity().registerReceiver(commandSendReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_BEEN_SEND));
		getActivity().registerReceiver(interactiveReceiver, new IntentFilter(D3Service.BROADCAST_INTERACTIVE_STATUS));
//		getActivity().registerReceiver(newUserRegReceiver, new IntentFilter(D3Service.BROADCAST_USER_ADD));
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(commandSendReceiver);
		getActivity().unregisterReceiver(interactiveReceiver);
//		getActivity().unregisterReceiver(newUserRegReceiver);
	}

	private void setFragmentThree(View v)
	{
		final WizardUserSetActivity activity = (WizardUserSetActivity) getActivity();
		if(null!=activity)
		{
			selDeviceId = activity.getDeviceId();
			selRegType = activity.getRegType();
			selSections = activity.getSections();
			selSectionsArray = activity.getSectionArray();
			selName = activity.getName();
			selPass = activity.getPass();
			selUID = activity.getUID();
			selUIDFormat = activity.getUIDFormat();

			LinearLayout manualLayout = (LinearLayout) v.findViewById(R.id.userFinishManualLayout);
			LinearLayout interactiveLayout = (LinearLayout) v.findViewById(R.id.userFinishInteractiveLayout);

			Button nextButton = (Button) v.findViewById(R.id.wizUserNextButton);

			if(0 == selRegType){
				/*CODE*/
				if(null!=manualLayout){
					manualLayout.setVisibility(View.VISIBLE);
				}
				if(null!=interactiveLayout){
					interactiveLayout.setVisibility(View.GONE);
				}
				final TextView nameText = (TextView) v.findViewById(R.id.userFinishManualName);
				TextView passText = (TextView) v.findViewById(R.id.userFinishManualPass);
				TextView sectionsText = (TextView) v.findViewById(R.id.userFinishManualSections);

				if(null!=nameText){
					nameText.setText(selName);
				}
				if(null!=passText){
					passText.setText(selPass);
				}
				if(null!=sectionsText){
					sectionsText.setText(selSections);
				}

				if(null!=nextButton){
					nextButton.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							JSONObject commandAddress = new JSONObject();
							UserInfo userInfo = dbHelper.getUserInfo();
							myService = getService();
							try
							{
//								commandAddress.put("index", 0);
								commandAddress.put("name", selName);
								commandAddress.put("password", selPass);
								if(selSectionsArray !=null && selSectionsArray.size() != 0)
								{
									JSONArray cSections = new JSONArray();
									for(int i = 0; i < selSectionsArray.size(); i++){
										cSections.put(selSectionsArray.get(i).id);
									}
									commandAddress.put("sections", cSections);
									int command_count = Func.getCommandCount(context);
									D3Request d3Request = D3Request.createCommand(userInfo.roles, selDeviceId, "USER_SET", commandAddress, command_count);
									if (d3Request.error == null)
									{
										if (sendCommandToServer(d3Request))
										{
											sendIntentCommandBeenSend(1);
										}
									} else
									{
										Func.pushToast(context, d3Request.error, (Activity) context);
									}
								}
							} catch (JSONException e)
							{
								e.printStackTrace();
							}
						}
					});
				}

			}else{
				/*HW*/
				if(null!=manualLayout){
					manualLayout.setVisibility(View.GONE);
				}
				if(null!=interactiveLayout){
					interactiveLayout.setVisibility(View.VISIBLE);
				}
				final TextView nameText = (TextView) v.findViewById(R.id.userFinishInteractiveName);
				sectionsText = (TextView) v.findViewById(R.id.userFinishInteractiveSections);

				if (null != nameText)
				{
					nameText.requestFocus();
					nameText.setOnEditorActionListener(new TextView.OnEditorActionListener()
					{
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
						{
							if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
							{

								Func.hideKeyboard(activity);
								return true;
							}
							return false;
						}
					});
				}

				final LinkedList<Section> sectionsList = new LinkedList<>();
				sections = dbHelper.getGuardSectionsForDeviceWithoutZones(selDeviceId);
				if (null != sections)
				{
					for (Section section : sections)
					{
						sectionsList.add(section);
					}
				}

				if (null != sectionsText)
				{
					sectionsText.setText(R.string.UAA_PRESS_TO_CHOOSE);
					sectionsText.setOnClickListener(new SectionListShowOnClick());
				}

				if (null != nextButton)
				{
					nextButton.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							String name = nameText.getText().toString();
							if (!name.equals(""))
							{
								String currentSections = activity.getSections();
								if (!currentSections.equals(""))
								{
									selSectionsArray = activity.getSectionArray();
									JSONObject commandAddress = new JSONObject();
									UserInfo userInfo = dbHelper.getUserInfo();
									myService = getService();
									try
									{
//										commandAddress.put("uid_format", selUIDFormat);
										commandAddress.put("uid", selUID);
										commandAddress.put("name", name);
										if(selSectionsArray !=null && selSectionsArray.size() != 0)
										{
											JSONArray cSections = new JSONArray();
											for (int i = 0; i < selSectionsArray.size(); i++)
											{
												cSections.put(selSectionsArray.get(i).id);
											}
											commandAddress.put("sections", cSections);
											int command_count = Func.getCommandCount(context);
											D3Request d3Request = D3Request.createCommand(userInfo.roles, selDeviceId, "USER_SET", commandAddress, command_count);
											if (d3Request.error == null)
											{
												if (sendCommandToServer(d3Request))
												{
													sendIntentCommandBeenSend(1);
												}
											} else
											{
												Func.pushToast(context, d3Request.error, (Activity) context);
											}
										}
									} catch (JSONException e)
									{
										e.printStackTrace();
									}
								} else
								{
									Func.nShowMessage(context, context.getString(R.string.WIZ_USER_CHOOSE_SECTIONS));
								}
						} else
							{
								Func.nShowMessage(context, context.getString(R.string.WIZ_USER_ENTER_NAME));
							}
						}
					});
				}


			}

		}
	}

	private void setFragmentTwo(View v)
	{
		final WizardUserSetActivity activity = (WizardUserSetActivity) getActivity();
		if(null!=activity){
			selDeviceId = activity.getDeviceId();
			selRegType = activity.getRegType();

			if(0 == setRegType)
			{
				/*CODE*/
				final TextView nameText = (TextView) v.findViewById(R.id.wizUserEditName);
				final TextView passText = (TextView) v.findViewById(R.id.wizUserEditPass);
				sectionsText = (TextView) v.findViewById(R.id.wizUserEditSections);

				Button buttonNext = (Button) v.findViewById(R.id.wizUserNextButton);

				if (null != nameText)
				{
					nameText.requestFocus();
					nameText.setOnEditorActionListener(new TextView.OnEditorActionListener()
					{
						@Override
						public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
						{
							if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_UNSPECIFIED)
							{

								if (null != passText)
								{
									passText.requestFocus();
								}
								return true;
							}
							return false;
						}
					});
				}


				final LinkedList<Section> sectionsList = new LinkedList<>();
				sections = dbHelper.getGuardSectionsForDeviceWithoutZones(selDeviceId);
				if (null != sections)
				{
					for (Section section : sections)
					{
						sectionsList.add(section);
					}
				}

				if (null != sectionsText)
				{
					sectionsText.setText(R.string.UAA_PRESS_TO_CHOOSE);
					sectionsText.setOnClickListener(new SectionListShowOnClick());
				}

				if (null != buttonNext)
				{
					buttonNext.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							String name = nameText.getText().toString();
							if (!name.equals(""))
							{
								String pass = passText.getText().toString();
								if(!pass.equals("")){
									if (!activity.getSections().equals(""))
									{
										activity.setName(name);
										activity.setPass(pass);
										activity.setNextPage();
									} else
									{
										Func.nShowMessage(context, context.getString(R.string.WIZ_USER_CHOOSE_SECTIONS));
									}
								}else{
									Func.nShowMessage(context, getString(R.string.WIZZ_USERS_ENTER_CODE));
								}
							} else
							{
								Func.nShowMessage(context, context.getString(R.string.WIZ_USER_ENTER_NAME));
							}
						}
					});
				}
			}else{
				/*HW*/
				buttonInteractive = (Button) v.findViewById(R.id.wizUserInteractiveButton);
				buttonNext = (Button) v.findViewById(R.id.wizUserNextButton);
				textUserType = (TextView) v.findViewById(R.id.wizUserType);
				textInteractiveTitle = (TextView) v.findViewById(R.id.wizUserSearchText);
				imageInteractive = (ImageView) v.findViewById(R.id.wizUserSearchImage);

				boolean interactive = dbHelper.getDeviceInteractiveStatus(selDeviceId);

				if(null!= buttonNext){
					buttonNext.setVisibility(View.GONE);
				}

				if(null!=textUserType){
					textUserType.setVisibility(View.GONE);
				}

				if(null!=textInteractiveTitle){
					if(interactive)
					{
						textInteractiveTitle.setText(R.string.WIZ_USER_MESS_IN_INTERACTIVE);
					}else{
						textInteractiveTitle.setText(R.string.WIZ_USER_MESS_INTERACTIVE_TURN_ON);
					}
				}

				if(null!=imageInteractive){
					if(interactive){
						imageInteractive.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_cloud_search_outline_siteorange));
					}else{
						imageInteractive.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_cloud_search_outline_brandcolor));
					}
				}

				if(null!=buttonInteractive){
					if(interactive){
						buttonInteractive.setText(context.getString(R.string.WIZ_INTERACTIVE_STATEMENT_ON));
						buttonInteractive.setEnabled(false);
					}else{
						buttonInteractive.setText(R.string.WIZ_INTERACTIVE_TURN_ON);
						buttonInteractive.setEnabled(true);
					}

					buttonInteractive.setOnClickListener(new View.OnClickListener()
					{
						@Override
						public void onClick(View v)
						{
							UserInfo userInfo = dbHelper.getUserInfo();
							myService = getService();
							JSONObject commandAddr = new JSONObject();
							int command_count = Func.getCommandCount(context);
							D3Request d3Request = D3Request.createCommand(userInfo.roles, selDeviceId, "INTERACTIVE", commandAddr, command_count);
							if (d3Request.error == null)
							{
								if (sendCommandToServer(d3Request))
								{
									sendIntentCommandBeenSend(1);
								}
							} else
							{
								Func.pushToast(context, d3Request.error, (Activity) context);
							}
						}
					});
				}

				if(null!= buttonNext){
					if(null!= buttonNext){
						buttonNext.setOnClickListener(new View.OnClickListener()
						{
							@Override
							public void onClick(View v)
							{
								if(null!=activity){
									activity.setNextPage();
								}
							}
						});
					}
				}
			}

		}
	}

	private void setFragmentOne(View v)
	{
		Button codeButton = (Button) v.findViewById(R.id.wizUserAddButtonCode);
		Button hwButton = (Button) v.findViewById(R.id.wizUserAddButtonHW);

		if(null!=codeButton){
			codeButton.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(null!=activity){
						setRegType = 0;
						activity.setRegType(setRegType);
						activity.setNextPage();
					}
				}
			});
		}

		if(null!=hwButton){
			hwButton.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(null!=activity){
						setRegType = 1;
						activity.setRegType(setRegType);
						activity.setNextPage();
					}
				}
			});
		}
	}

	public void setSections(Section[] sections){
		WizardUserSetActivity activity = (WizardUserSetActivity) getActivity();
		if(null!= activity)
		{
			this.sections = sections;
			if (null != cSectionsList)
			{
				cSectionsList.clear();
			} else
			{
				cSectionsList = new LinkedList<>();
			}
			for (Section section : sections)
			{
				if (section.checked)
				{
					cSectionsList.add(section);
				}
			}

			if (cSectionsList.size() > 0)
			{
				String[] sectionIds = new String[cSectionsList.size()];
				for (int i = 0; i < cSectionsList.size(); i++)
				{
					sectionIds[i] = Integer.toString(cSectionsList.get(i).id);
				}
				String sectionString = Arrays.toString(sectionIds);
				sectionsText.setText(sectionString.substring(1, sectionString.length() - 1));
				activity.setSections(sectionString.substring(1, sectionString.length() - 1));
				activity.setSectionsArray(cSectionsList);

			} else
			{
				sectionsText.setText(R.string.UAA_PRESS_TO_CHOOSE);
				activity.setSections("");
			}
		}
	}

	public Section[] getSections(){
		return sections;
	}

	private class SectionListShowOnClick implements View.OnClickListener
	{
		private Section[] sections;

		public SectionListShowOnClick(){
		}

		@Override
		public void onClick(View v)
		{
				AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
				final AlertDialog checkSectionsDialog = adb.create();
				checkSectionsDialog.setTitle(getString(R.string.USER_ADD_MESSAGE_ENTER_SECTIONS));

				FrameLayout frameLayout = (FrameLayout) new FrameLayout(context);
				frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

				this.sections = getSections();

				ListView sectionsCheckList = new ListView(context);
				if (null!=sections && sections.length != 0)
				{
					userAddSectionsListAdapter = new UserAddSectionsListAdapter(context, R.layout.card_for_section_zone_check, sections);
					sectionsCheckList.setAdapter(userAddSectionsListAdapter);
					frameLayout.addView(sectionsCheckList);
				} else
				{
					TextView noSectionsText = new TextView(context);
					noSectionsText.setPadding(Func.dpToPx(20, context), 0, Func.dpToPx(20, context), 0);
					noSectionsText.setText(R.string.USER_ADD_ERROR_NO_COMPATIBLE_SECTIONS);
					frameLayout.addView(noSectionsText);
				}

				checkSectionsDialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.OK), new DialogInterface.OnClickListener()
				{

					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						if (null != userAddSectionsListAdapter)
						{
							sections = userAddSectionsListAdapter.getSections();
							setSections(sections);
						}
						dialog.dismiss();
					}
				});

				checkSectionsDialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.ADB_CANCEL), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
				checkSectionsDialog.setView(frameLayout);

				checkSectionsDialog.show();
		}
	}

	private boolean sendCommandToServer(D3Request d3Request)
	{
		if (null != myService)
		{
			if (myService.send(d3Request.toString()))
			{
				return true;
			} else
			{
				sendIntentCommandBeenSend(-1);
			}
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
		}
		return false;
	};

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if (null != myService)
		{
			if (myService.send(d3Request.toString()))
			{
				return true;
			}
		} else
		{
			Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
		}
		return false;
	};


	public void sendIntentCommandBeenSend(int value){
		Intent intent = new Intent(D3Service.BROADCAST_COMMAND_BEEN_SEND);
		intent.putExtra("command_been_send", value);
		context.sendBroadcast(intent);
	}

	public D3Service getService()
	{
		WizardUserSetActivity activity = (WizardUserSetActivity) getActivity();
		if(activity != null){
			return activity.getLocalService();
		}
		return null;
	}
}
