package biz.teko.td.SHUB_APP.Utils.Other;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

public class SwipeDisabledViewPager extends ViewPager
{
	private int mCurrentPagePosition = 0;

	public SwipeDisabledViewPager(Context context) {
		super(context);
	}

	public SwipeDisabledViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		// returning false will not propagate the swipe event
		return false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		return false;
	}

//	@Override
//	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		try {
//			boolean wrapHeight = MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.AT_MOST;
//			if (wrapHeight) {
//				View child = getChildAt(mCurrentPagePosition);
//				if (child != null) {
//					child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
//					int h = child.getMeasuredHeight();
//
//					heightMeasureSpec = MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY);
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//	}
//
//	public void reMeasureCurrentPage(int position) {
//		mCurrentPagePosition = position;
//		requestLayout();
//	}
}
