package biz.teko.td.SHUB_APP.UDP.LocalConfigEntiies;

import android.content.res.Resources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants;

public class BaseBooleanConfigEntity {
    protected static boolean now;
    protected HashMap<Boolean, String> map = new HashMap<>();
    protected List<String> values = new ArrayList<>();

    public void setValues() {
        for (Map.Entry<Boolean, String> entry : map.entrySet())
            values.add(entry.getValue());
    }

    public List<String> getValues() {
        return values;
    }

    public HashMap<Boolean, String> getMap() {
        return map;
    }

    public static String getDescription(String value, Resources resources) {
        switch (value) {
            case "Disallow": return resources.getString(R.string.LOCAL_CONFIG_DISALLOW);
            case "Allow": return resources.getString(R.string.LOCAL_CONFIG_ALLOW);
        }
        return "";
    }

    public static String getDescription(String type, boolean bool, Resources resources) {
        if (type.equals(ConfigConstants.CONFIG_RADIO_LITER))
            return (bool ? "1" : "3");
        return (bool ? resources.getString(R.string.LOCAL_CONFIG_DISALLOW) :
                resources.getString(R.string.LOCAL_CONFIG_ALLOW));
    }
}
