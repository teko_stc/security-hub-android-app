package biz.teko.td.SHUB_APP.MainTabs.Activities;

import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.LinearLayout;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.NDeleteActivity;
import biz.teko.td.SHUB_APP.Activity.NRenameActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Delegation.Activities.DelegateListActivity;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class SiteSettingsActivity extends BaseActivity
{
	private Context context;
	private boolean bound;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private int sending = 0;
	private LinearLayout buttonBack;
	private Site site;
	private NMenuListElement nameView;
//	private NMenuListElement delegateView;
	private NMenuListElement delegateRefuseView;
	private NMenuListElement delegateControlView;
	private NMenuListElement deleteView;
	private DBHelper dbHelper;
	private UserInfo userInfo;

	private BroadcastReceiver sitesUpdateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			bind();
		}
	};

	private int site_id;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_site_settings);
		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();

		site_id = getIntent().getIntExtra("site", -1);

		setup();
		bind();
	}

	private void bind()
	{
		buttonBack.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		site = dbHelper.getSiteById(site_id);
		if(null!=site){
			boolean delegated = false;
			if(0 == site.delegated){
				if(null!=delegateRefuseView) delegateRefuseView.setVisibility(View.GONE);
			}else{
				if (site.domain != userInfo.domain){
					delegated = true;
//					if(null!=delegateView) delegateView.setVisibility(View.GONE);
					if(null!=delegateControlView) delegateControlView.setVisibility(View.GONE);
					if(null!=deleteView) deleteView.setVisibility(View.GONE);
				}else{
					if(null!=delegateRefuseView) delegateRefuseView.setVisibility(View.GONE);
				}
			}
			if (null != nameView)
			{
				nameView.setTitle(site.name);
				if(maySet() && !delegated){
					nameView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NRenameActivity.class);
							intent.putExtra("type", Const.SITE);
							intent.putExtra("site", site.id);
							intent.putExtra("name", site.name);
							intent.putExtra("transition",true);
							startActivity(intent, getTransitionOptions(nameView).toBundle());
						}
					});
				}else{
					nameView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Func.nShowMessage(context, getString(R.string.N_NO_PERMISSION));
						}
					});
				}
			}

			if(null!=delegateRefuseView) {
				if(mayControl()){
					delegateRefuseView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							NDialog nDialog = new NDialog(context, R.layout.n_dialog_question);
							nDialog.setTitle(getString(R.string.N_SITE_SETTINGS_DELEGATION_REFUSE));
							nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
							{
								@Override
								public void onActionClick(int value, boolean b)
								{
									switch (value){
										case NActionButton.VALUE_OK:
											refuseDelegation();
											onBackPressed();
											break;
										case NActionButton.VALUE_CANCEL:
											nDialog.dismiss();
											break;
									}
								}
							});
							nDialog.show();
						}
					});
				}else{
					delegateRefuseView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Func.nShowMessage(context, getString(R.string.N_NO_PERMISSION));
						}
					});
				}
			}
			if(null!=delegateControlView) delegateControlView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
			{
				@Override
				public void onRootClick()
				{
					Intent intent = new Intent(context, DelegateListActivity.class);
					intent.putExtra("site", site.id);
					startActivity(intent);
					overridePendingTransition(R.animator.enter, R.animator.exit);
				}
			});
//			if(null!=delegateView) delegateView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
//			{
//				@Override
//				public void onRootClick()
//				{
//					Intent intent = new Intent(context, DelegateSendActivity.class);
//					intent.putExtra("site", site.id);
//					startActivity(intent);
//				}
//			});
			if(null!=deleteView) {
				if(mayControl()){
					deleteView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Intent intent = new Intent(context, NDeleteActivity.class);
							intent.putExtra("type", Const.SITE);
							intent.putExtra("site", site.id);
							intent.putExtra("name", site.name);
							intent.putExtra("transition",true);
							startActivityForResult(intent, Const.REQUEST_DELETE, getTransitionOptions(nameView).toBundle());
						}
					});
				}else{
					deleteView.setOnRootClickListener(new NMenuListElement.OnRootClickListener()
					{
						@Override
						public void onRootClick()
						{
							Func.nShowMessage(context, getString(R.string.N_NO_PERMISSION));
						}
					});
				}
			}
		}
//		refresh();
	}

	private void refresh()
	{
//		findViewById(R.id.nRootView).invalidate();
		getWindow().getDecorView().findViewById(android.R.id.content).invalidate();
	}

	private boolean mayControl()
	{
		return Func.nGetRemoteSystemAdminRights(userInfo);
	}

	private boolean maySet()
	{
		return Func.nGetRemoteSystemLawyerRights(userInfo);
	}

	private void deleteSite()
	{
		JSONObject message = new JSONObject();
		try
		{
			message.put("site", site.id);
			nSendMessageToServer(null, D3Request.SITE_DEL, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void refuseDelegation()
	{
		JSONObject message = new JSONObject();
		try
		{
			message.put("site_id", site.id);
			message.put("domain_id", userInfo.domain);
			nSendMessageToServer(null, D3Request.DELEGATE_REVOKE, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void setup()
	{
		buttonBack = (LinearLayout) findViewById(R.id.nButtonBack);
		nameView = (NMenuListElement) findViewById(R.id.nMenuName);
//		delegateView = (NMenuListElement) findViewById(R.id.nMenuDelegate);
		delegateRefuseView = (NMenuListElement) findViewById(R.id.nMenuDelegationRefuse);
		delegateControlView = (NMenuListElement) findViewById(R.id.nMenuDelegationControl);
		deleteView = (NMenuListElement) findViewById(R.id.nMenuDelete);

	}

	private ActivityOptions getTransitionOptions(View view)
	{
		return ActivityOptions.makeSceneTransitionAnimation((SiteSettingsActivity)context,
				new Pair<>(((NMenuListElement)view).getTitleView(), Const.TITLE_TRANSITION_NAME));
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
		overridePendingTransition(R.animator.from_left_enter, R.animator.from_left_exit);
	}

	public void  onResume(){
		super.onResume();
		bind();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		registerReceiver(sitesUpdateReceiver, intentFilter);
		bindD3();
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(sitesUpdateReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == Const.REQUEST_DELETE){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}
}
