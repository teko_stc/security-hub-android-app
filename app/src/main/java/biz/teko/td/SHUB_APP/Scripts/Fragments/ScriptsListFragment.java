package biz.teko.td.SHUB_APP.Scripts.Fragments;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Timer;
import java.util.TimerTask;

import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptActivity;
import biz.teko.td.SHUB_APP.Scripts.Activities.ScriptsLibraryActivity;
import biz.teko.td.SHUB_APP.Scripts.Adapters.NElementSelectAdapter;
import biz.teko.td.SHUB_APP.Scripts.Adapters.NSitesWScriptsCursorAdapter;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NBottomSheetDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Wizards.Activity.WizardAllActivity;

public class ScriptsListFragment extends Fragment
{
	private static final int TO_DEVICE_SCRIPT = 0;
	private static final int TO_LIBRARY = 1;
	private static final int NO_ACTION = -1;
	private final BaseActivity activity;
	private View layout;
	private DBHelper dbHelper;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private final int sending = 0;
	private RecyclerView scriptsList;
	private boolean wait = false;
	private int device_id;
	private int script_id;
	private final  static int DEF_POSITION = 5;

	private NDialog materialDialog;
	private Timer timer;
	private Cursor cursor;
	private NSitesWScriptsCursorAdapter scriptsAdapter;
	private NWithAButtonElement noScriptsText;
	private final boolean act = true; // false - редактирование, true - установка нового и переход на списки из библиотеки
	private int action;
	private String deviceSerial;
	private String deviceModel;
	private int position;

	public ScriptsListFragment(BaseActivity activity) {
		this.activity = activity;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.n_activity_scripts, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		layout = getView();
		activity.setOnBackPressedCallback(this::onBackPressed);
		setup();
		bind();
	}

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateBottomHelper();
		}
	};
	private LinearLayout newButton;


	private void updateBottomHelper(){
		activity.setBottomInfoBehaviour(activity);
	}

	private final BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			Func.showCommandResultSnackbar(context, layout.findViewById(android.R.id.content), intent.getIntExtra("result", -1));
		}
	};

	private final BroadcastReceiver libraryScriptsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if (materialDialog != null && materialDialog.isShowing())
			{
				materialDialog.dismiss();
				if (wait)
				{
					wait = false;
					switch (action){
						case TO_DEVICE_SCRIPT:
							openDeviceScript();
							break;
						case TO_LIBRARY:
							openLibrarylist();
							break;
						default:
							break;
					}
					if (null != timer) timer.cancel();
				}else{
					Func.nShowMessage(context, getString(R.string.SCRIPT_ERROR_CANT_LOAD_LIB));
				}
				device_id = 0;
				action = NO_ACTION;
			}
		}
	};

	private final BroadcastReceiver updateListReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateList();
		}
	};
	private UserInfo userInfo;
	private Drawer drawerResult;
	private PrefUtils prefUtils;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		activity.getSwipeBackLayout().setEnableGesture(false);

		dbHelper = DBHelper.getInstance(activity);
		userInfo = dbHelper.getUserInfo();
		prefUtils = PrefUtils.getInstance(activity);
		position = activity.getIntent().getIntExtra("position", 0);

//		setBottomNavigation(R.id.bottom_navigation_item_scripts, context);


	}

	private void bind()
	{
		updateList();
		if(null!=newButton){
			newButton.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					startActivity(new Intent(activity, WizardAllActivity.class));
				}
			});
		}
		if(null!=noScriptsText){
			noScriptsText.setOnChildClickListener(new NWithAButtonElement.OnChildClickListener()
			{
				@Override
				public void onChildClick(int action, boolean option)
				{
					showDeviceChoose();
				}
			});
		}
	}

	private void setup()
	{
		noScriptsText = layout.findViewById(R.id.nNoSctiptsView);
		scriptsList = layout.findViewById(R.id.deviceScriptsList);
		newButton = layout.findViewById(R.id.nButtonMainAdd);
	}

	private void updateList(){
		refreshList();
		refreshViewVisiblity();
	}

	private void refreshList(){
		refreshCursor();
		refreshAdapter();
	}

	private void refreshCursor (){
		cursor = dbHelper.getSitesWithScriptsCursor(PrefUtils.getInstance(activity).getCurrentSite());
	}

	private void refreshAdapter()
	{
		if(null == scriptsAdapter){
			scriptsAdapter = new NSitesWScriptsCursorAdapter(activity, cursor);
			scriptsAdapter.setOnElementClickListener((device, script) -> {
				device_id = device;
				script_id = script;
				action = TO_DEVICE_SCRIPT;
				getLibraryScripts(device_id);
			});
			if(null!=scriptsList)
				scriptsList.setAdapter(scriptsAdapter);
		}else{
			scriptsAdapter.update(cursor);
		}
	}


	private void refreshViewVisiblity()
	{
		if(null!=scriptsList && null != noScriptsText){
			if(null!=scriptsAdapter && scriptsAdapter.getItemCount() != 0)
			{
				scriptsList.setVisibility(View.VISIBLE);
				noScriptsText.setVisibility(View.GONE);
			}else{
				scriptsList.setVisibility(View.GONE);
				noScriptsText.setVisibility(View.VISIBLE);
			}
		}
	}

	private void showDeviceChoose()
	{
		final NBottomSheetDialog deviceSetDialog = new NBottomSheetDialog(activity);

		View bottomView = activity.getLayoutInflater().inflate(R.layout.dialog_bottom_with_list_biggest, null);
		TextView title = bottomView.findViewById(R.id.dialogBottomTitle);
		title.setText(R.string.N_SCRIPTS_SELECT_CONTROLLER);

		ListView listView = bottomView.findViewById(R.id.dialogBottomList);
		listView.setDivider(null);

		Device[] devices = dbHelper.getAllSHDevices();

		NElementSelectAdapter nElementSelectAdapter = new NElementSelectAdapter(activity, R.layout.n_devices_list_element, devices);
		nElementSelectAdapter.setOnItemClickListener(id -> {
			if(0!=id){
				if(0 < id){
					device_id = id;
					action = TO_LIBRARY;
					getLibraryScripts(device_id);
					deviceSetDialog.dismiss();
				}else{
					switch (id){
						case Const.STATUS_ARMED:
							Func.nShowMessage(activity, getString(R.string.N_SCRITPS_NEED_DISARM));
							break;
						case Const.STATUS_PARTLY_ARMED:
							Func.nShowMessage(activity, getString(R.string.N_SCRIPTS_NEDE_DISARM_FROM_PARTLY));
							break;
						case Const.STATUS_OFFLINE:
							Func.nShowMessage(activity, getString(R.string.N_DISARM_NO_CONNECTION));
							break;
					}
				}
			}
		});
		listView.setAdapter(nElementSelectAdapter);

		deviceSetDialog.setContentView(bottomView);
		deviceSetDialog.setOnShowListener(dialog -> {
			FrameLayout bottomSheet = deviceSetDialog.findViewById(com.google.android.material.R.id.design_bottom_sheet);

			if (null != bottomSheet)
			{
				BottomSheetBehavior behavior = BottomSheetBehavior.from(bottomSheet);
				behavior.setHideable(false);
			}
		});

		try
		{
			Field mBehaviorField = deviceSetDialog.getClass().getDeclaredField("behavior");
			mBehaviorField.setAccessible(true);

			final BottomSheetBehavior behavior = (BottomSheetBehavior) mBehaviorField.get(deviceSetDialog);
			behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback()
			{
				@Override
				public void onStateChanged(@NonNull View bottomSheet, int newState)
				{
					if (newState == BottomSheetBehavior.STATE_DRAGGING)
					{
						behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
					}
				}

				@Override
				public void onSlide(@NonNull View bottomSheet, float slideOffset)
				{
				}
			});
		} catch (NoSuchFieldException e)
		{
			e.printStackTrace();
		} catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
		deviceSetDialog.show();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		bindD3();
		activity.registerReceiver(libraryScriptsReceiver, new IntentFilter(D3Service.BROADCAST_LIBRARY_SCRIPTS));
		activity.registerReceiver(updateListReceiver, new IntentFilter(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV));
		activity.registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT_UPD));
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		activity.registerReceiver(updateReceiver, intentFilter);
		updateList();

		updateBottomHelper();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		activity.unbindService(serviceConnection);
		activity.unregisterReceiver(libraryScriptsReceiver);
		activity.unregisterReceiver(commandResultReceiver);
		activity.unregisterReceiver(updateListReceiver);
		activity.unregisterReceiver(updateReceiver);
	}

	public void onBackPressed()
	{
		NDialog nDialog = new NDialog(activity, R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.N_LOGOUT_Q));
		nDialog.setOnActionClickListener((value, b) -> {
            switch (value){
                case NActionButton.VALUE_OK:
                    activity.finishAffinity();
                    break;
                case NActionButton.VALUE_CANCEL:
                    nDialog.dismiss();
                    break;
            }

        });
		nDialog.show();
	}


	public void getLibraryScripts(int device_id)
	{
		if(null!= myService)
		{
			activity.runOnUiThread(() -> {
				materialDialog = new NDialog(activity, R.layout.n_dialog_progress);
				materialDialog.setTitle(getString(R.string.DIALOG_WAIT_TITLE));
				materialDialog.show();
			});
			wait = true;
			sendScriptsRequest(device_id);
			timer = new Timer();
			timer.schedule(new TimerTask()
			{
				@Override
				public void run()
				{
					wait = false;
					activity.sendBroadcast(new Intent(D3Service.BROADCAST_LIBRARY_SCRIPTS));
				}
			}, 15000);
		}
	}

	private void openLibrarylist()
	{
		Intent intent = new Intent(activity, ScriptsLibraryActivity.class);
		intent.putExtra("device_id", device_id);
//		intent.putExtra("site_id", siteId);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			// Apply activity transition
			startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
		} else
		{
			//!!
			// Swap without transition
			startActivity(intent);
			activity.overridePendingTransition(R.animator.from_down_enter, R.animator.from_down_exit);
		}
	}


	public void openDeviceScript(){
		Intent deviceScriptsIntent =  new Intent(activity, ScriptActivity.class);
		deviceScriptsIntent.putExtra("script_id", script_id);
//		deviceScriptsIntent.putExtra("site_id", site_id);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
		{
			// Apply activity transition
			Activity activity = this.activity;
			startActivity(deviceScriptsIntent, ActivityOptions.makeSceneTransitionAnimation(activity).toBundle());
		} else
		{
			// Swap without transition
			startActivity(deviceScriptsIntent);
			activity.overridePendingTransition(R.animator.from_down_enter, R.animator.from_down_exit);
		}
	}

	private void sendScriptsRequest(int device_id)
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		Device device  = dbHelper.getDeviceById(device_id);
		JSONObject message = new JSONObject();
		try
		{
			message.put("config_version", device.config_version);
			nSendMessageToServer(device, D3Request.LIBRARY_SCRIPTS, message,false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}


	private void bindD3(){
		Intent intent = new Intent(activity, D3Service.class);
		serviceConnection = getServiceConnection();
		activity.bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(activity, activity.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), activity, command, sending);
	}

	public void setSelScript(int id)
	{
		this.script_id = id;
	}
}
