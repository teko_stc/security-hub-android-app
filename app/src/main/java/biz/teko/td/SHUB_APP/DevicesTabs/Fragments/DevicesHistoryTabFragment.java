package biz.teko.td.SHUB_APP.DevicesTabs.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.Events.Adapters.UniEventListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class DevicesHistoryTabFragment extends Fragment
{
	private Context context;
	private int siteId;
	private FrameLayout frameLayout;
	private ListView eventList;
	private TextView t;
	private DBHelper dbHepler;
	private Cursor cursor;
	private UniEventListAdapter eventListAdapter;
	private FrameLayout view;
	private final boolean showRelays = false; // false, так как в списке событий для страницы с устройствами показывают все события, не зависимо от того, отображаем реле отдельно или нет
	private final int zoneOrRelay = 0;

	public static DevicesHistoryTabFragment newInstance(int siteId){
		DevicesHistoryTabFragment devicesHistoryTabFragment = new DevicesHistoryTabFragment();
		Bundle b = new Bundle();
		b.putInt("site", siteId);
		devicesHistoryTabFragment.setArguments(b);
		return devicesHistoryTabFragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		context = container.getContext();
		dbHepler = DBHelper.getInstance(context);

		siteId = getArguments().getInt("site");
		//MAIN LAYOUT
		frameLayout =  (FrameLayout) new FrameLayout(container.getContext());
		frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		eventList = new ListView(container.getContext());
		t = new TextView(container.getContext());
		t.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		t.setGravity(Gravity.CENTER);

		refreshList();

		view = frameLayout;
		return view;
	}

	private void refreshList(){
		ArrayList<Integer> classes = Func.getCurrentHistoryClasses(context);
		Cursor cursor = getRefreshedCursor(classes);
		if(null!=cursor && cursor.getCount()>0){
			if(null!=eventList)eventList.setVisibility(View.VISIBLE);
			if(null!=t)t.setVisibility(View.GONE);
			if(null!=eventListAdapter)
			{
				eventListAdapter.update(cursor);
			}else{
				eventListAdapter = new UniEventListAdapter(context , R.layout.card_event_uni, cursor , 0, 1, siteId);
				if( null!=eventList)
				{
					eventList.setAdapter(eventListAdapter);
					frameLayout.addView(eventList);
				}
			}
		}else{
			if(null!=eventList)eventList.setVisibility(View.GONE);
			if(null!=t)t.setVisibility(View.VISIBLE);
		}
	}

	private Cursor getRefreshedCursor(ArrayList<Integer> classes){
		return dbHepler.getEventsForSiteWithFilter(siteId, classes, showRelays, zoneOrRelay);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		refreshList();
		getActivity().registerReceiver(eventsReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(eventsReceiver);
	}

	private final BroadcastReceiver eventsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			refreshList();
		}
	};
}
