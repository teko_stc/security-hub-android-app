package biz.teko.td.SHUB_APP.SitesTabs;

import java.util.List;

import biz.teko.td.SHUB_APP.D3DB.Site;

/**
 * Created by td13017 on 08.02.2017.
 */

public class ExpendableGroup<T>
{
	List<T> items;
	Site site;

	public ExpendableGroup(Site site, List<T> items){
		this.site = site;
		this.items = items;
	}
}
