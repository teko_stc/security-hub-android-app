package biz.teko.td.SHUB_APP.Profile.Interface;

import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceFragment;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class ProfileInterfaceFragment extends PreferenceFragment
{
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
		{
			addPreferencesFromResource(R.xml.profile_interface_preferences_oreo);
		}else{
			addPreferencesFromResource(R.xml.profile_interface_preferences);
		}
	}
}
