package biz.teko.td.SHUB_APP.D3.D3UniConfigUI;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

/**
 * Created by td13017 on 25.07.2016.
 */

@Root(name = "header")
public class HeaderUI
{
	public HeaderUI() {}

	@Attribute(name = "android:fragment")
	public String andr;

	@Attribute(name="android:title")
	public String title;
}
