package biz.teko.td.SHUB_APP.Events.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.Events.Activities.EventActivity;
import biz.teko.td.SHUB_APP.Utils.NViews.NEAListElement;

public class NAffectsListAdapter extends ArrayAdapter<Event>
{
	private final LinkedList<Event> affects;
	private final int layout;
	private final Context context;

	public NAffectsListAdapter(@NonNull Context context, int resource, LinkedList<Event> affects)
	{
		super(context, resource);
		this.context = context;
		this.layout =resource;
		this.affects = affects;
	}

	@NonNull
	@Override
	public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
	{
		NEAListElement view = (NEAListElement) convertView;
		if(null==view)
		{
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = (NEAListElement) layoutInflater.inflate(layout, parent, false);
		}
		Event affect = affects.get(position);
		if(null!=affect){
			view.setTitle(affect.explainAffectRegDescription(context));
		}
//		view.bindView();
		view.setOnElementClickListener(new NEAListElement.OnElementClickListener()
		{
			@Override
			public void onClick()
			{
				openEventActivity(affect.id);
			}
		});
		return view;
	}

	private void openEventActivity(int id)
	{
		Intent intent = new Intent(context, EventActivity.class);
		intent.putExtra("event", id);
		context.startActivity(intent);
	}

	public int getCount(){
		return affects.size();
	}

	public Event getItem(int position){
		return affects.get(position);
	}

	public  long getItemId(int position){
		return  position;
	}

}
