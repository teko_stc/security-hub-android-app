package biz.teko.td.SHUB_APP.HTTP.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by td13017 on 22.02.2018.
 */

public class IVResponse
{
	@SerializedName("result")
	public IVResult result;

	@SerializedName("success")
	public String success;


}
