package biz.teko.td.SHUB_APP.MainTabs.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import biz.teko.td.SHUB_APP.MainTabs.Fragments.NStoryFragment;
import biz.teko.td.SHUB_APP.R;

public class NStoryPagerAdapter extends FragmentPagerAdapter {
    private List<Integer> images = new ArrayList<>();
    private List<Integer> backgrounds = new ArrayList<>();

    public NStoryPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        setupData();
    }

    private void setupData() {
        images.add(R.drawable.update_info_1);
        images.add(R.drawable.update_info_2);
        images.add(R.drawable.update_info_3);
        images.add(R.drawable.update_info_4);
        images.add(R.drawable.update_info_5);
        images.add(R.drawable.update_info_6);
        images.add(R.drawable.update_info_7);
        images.add(R.drawable.update_info_8);
        backgrounds.add(R.color.brandColorWhite);
        backgrounds.add(R.color.n_brand_blue);
        backgrounds.add(R.color.brandColorWhite);
        backgrounds.add(R.color.n_brand_blue);
        backgrounds.add(R.color.brandColorWhite);
        backgrounds.add(R.color.n_brand_blue);
        backgrounds.add(R.color.n_brand_blue);
        backgrounds.add(R.color.n_brand_blue);
        backgrounds.add(R.color.brandColorWhite);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return NStoryFragment.newInstance(images.get(position), backgrounds.get(position), position);
    }

    @Override
    public int getCount() {
        return images.size();
    }
}
