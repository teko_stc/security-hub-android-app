package biz.teko.td.SHUB_APP.UDP;

import java.nio.ByteBuffer;

public class Request_SET_APN extends Request
{
	private static final byte Command = 0x04;

	public String APN1;
	public String APN2;

	public Request_SET_APN(String APN1, String APN2){
		this.APN1 = APN1;
		this.APN2 = APN2;
		this.command = Command;
	}

	public byte[] getBytes(){
		byte[] buf =  new byte[256];
		ByteBuffer byteBuffer = ByteBuffer.allocate(buf.length);
		byteBuffer.put(command);
		byte[] apn1 = new byte[32];
		byte[] apn2 = new byte[32];
		if(null!=APN1){
			byte[] APN1Bytes = APN1.getBytes();
			System.arraycopy(APN1.getBytes(), 0 , apn1, 0 ,APN1Bytes.length);
		}else{
			APN1 = "internet";
			byte[] APN1Bytes = APN1.getBytes();
			System.arraycopy(APN1.getBytes(), 0 , apn1, 0 ,APN1Bytes.length);
		}
		if(null!=APN2){
			byte[] APN2Bytes = APN2.getBytes();
			System.arraycopy(APN2Bytes, 0 , apn2, 0 ,APN2Bytes.length);
		}else{
			APN2 = "internet";
			byte[] APN2Bytes = APN2.getBytes();
			System.arraycopy(APN2Bytes, 0 , apn2, 0 ,APN2Bytes.length);
		}
		byteBuffer.put(apn1);
		byteBuffer.put(apn2);

//		byte[] apn1 = new byte[32];
//		byte[] apn2 = new byte[32];
//		System.arraycopy(apn1, 0, buf, 1, apn1.length);
//		System.arraycopy(apn2, 0, buf, 33, apn2.length);
		return  byteBuffer.array();
	}
}
