package biz.teko.td.SHUB_APP.Cameras.Fragments;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mikepenz.materialdrawer.Drawer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Activity.MainNavigationActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.NCameraDahuaActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.Dahua.NDahuaHelper;
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraActivity;
import biz.teko.td.SHUB_APP.Cameras.Activities.RTSP.NCameraRTSPActivity;
import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;
import biz.teko.td.SHUB_APP.Wizards.Activity.WizardAllActivity;

public  class NCamerasListFragment extends Fragment
{

	private final BaseActivity activity;
	private View layout;
	private DBHelper dbHelper;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private final int sending = 0;
	private UserInfo userInfo;
	private Drawer drawerResult;
	private PrefUtils prefUtils;
	private int position;
	private LinearLayout buttonAdd;

	private final LinkedList<ApiElement> messagesQueue = new LinkedList<>();

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			updateBottomHelper();
		}
	};

	private OnFragmentRefreshListener onFragmentRefreshListener;
	public void setOnFragmentRefreshListener(OnFragmentRefreshListener onFragmentRefreshListener){
		this.onFragmentRefreshListener = onFragmentRefreshListener;
	}
	public interface OnFragmentRefreshListener{
		void onRefresh(boolean b, Camera.CType cType);
	}

	private boolean repeat = false;
	private final BroadcastReceiver IVGetTokenResponseFromServer = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getIntExtra("result", 0 ) <= 0){
				if(null!=onFragmentRefreshListener)
					onFragmentRefreshListener.onRefresh(true, Camera.CType.IV);
			}
		}
	};

	private final BroadcastReceiver IVCamerasGetReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(intent.getIntExtra("result", -1) == Const.IV_ERROR_NOT_AUTHORIZED){
				if (repeat)
				{
					repeat=!repeat;
					getToken();
				}else{
					if(onFragmentRefreshListener!= null)
						onFragmentRefreshListener.onRefresh(true, Camera.CType.IV);
				}
			}
			else if(intent.getIntExtra("result", -1) == Const.IV_CAMERA_GET_SUCCESS)
			{
				if (onFragmentRefreshListener != null)
					onFragmentRefreshListener.onRefresh(true, Camera.CType.IV);
			}
		}
	};

	private final BroadcastReceiver dahuaCamerasGetReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(null!=onFragmentRefreshListener) {
				onFragmentRefreshListener.onRefresh(true, Camera.CType.DAHUA);
			}
		}
	};

	public NCamerasListFragment(){
		this.activity = (MainNavigationActivity) requireContext();

	}

	public NCamerasListFragment(BaseActivity activity) {
		this.activity = activity;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		return inflater.inflate(R.layout.n_activity_cameras, container, false);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		layout = getView();
		activity.setOnBackPressedCallback(this::onBackPressed);
		setup();
		bind();
		activity.getSwipeBackLayout().setEnableGesture(false);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		dbHelper = DBHelper.getInstance(activity);
		userInfo = dbHelper.getUserInfo();
		prefUtils = PrefUtils.getInstance(activity);
		position = activity.getIntent().getIntExtra("position", 0);


	}

	private void setup()
	{
		buttonAdd = layout.findViewById(R.id.nButtonMainAdd);
	}

	private void addCameraDialog(){
		Intent intent = new Intent(activity, WizardAllActivity.class);
		//ns nr
		intent.putExtra("transparent", true);
		startActivity(intent);
	}
	private void bind()
	{
		if(null!=buttonAdd) buttonAdd.setOnClickListener((v)-> addCameraDialog());
		NCamerasListInnerFragment nCamerasListFragment = new NCamerasListInnerFragment();
		nCamerasListFragment.setOnElementClickListener(new NCamerasListInnerFragment.OnElementClickListener()
		{
			@Override
			public void onNewClick()
			{
				Intent intent = new Intent(activity, WizardAllActivity.class);
				startActivity(intent);
			}

			@Override
			public void getIVCameras()
			{
				update();
			}

			@Override
			public void onCameraClick(Camera camera)
			{
				switch (camera.type){
					case IV:
						Intent intent = new Intent(activity, NCameraActivity.class);
						intent.putExtra("cameraId", camera.id);
						startActivity(intent);
						break;
					case RTSP:
						intent = new Intent(activity, NCameraRTSPActivity.class);
						intent.putExtra("cameraId", Integer.toString(camera.local_id));
						startActivity(intent);
						break;
					case DAHUA:
						intent = new Intent(activity, NCameraDahuaActivity.class);
						intent.putExtra("cameraId", camera.sn);
//						intent.putExtra("cameraHandle", camera.getLoginHandle());
						startActivity(intent);
						break;
				}
			}

			@Override
			public void onCameraLongClick(Camera camera)
			{
				switch (camera.type){
					case IV:
						break;
					default:
						NDialog nDialog = new NDialog(activity, R.layout.n_dialog_question);
						nDialog.setTitle(getString(R.string.N_RTSP_DELETE_Q));
						nDialog.setOnActionClickListener((value, b) -> {
							switch (value){
								case NActionButton.VALUE_OK:
									switch (camera.type){
										case RTSP:
											dbHelper.deleteRTSPCamera(camera.local_id);
											break;
										case DAHUA:
											dbHelper.deleteDahuaCamera(camera);
											break;
									}
									if(null!=onFragmentRefreshListener)onFragmentRefreshListener.onRefresh(false, camera.type);
									nDialog.dismiss();
									break;
								case NActionButton.VALUE_CANCEL:
									nDialog.dismiss();
									break;
							}
						});
						nDialog.show();
						break;
				}

			}
		});
		getChildFragmentManager().beginTransaction().replace(R.id.work_frame, nCamerasListFragment).addToBackStack("0").commit();
	}

	private void update()
	{
		if(!getCameras()){
			getToken();
		}
	}

	private boolean getCameras()
	{
		String iv_token = dbHelper.getUserInfoIVToken();
		if (!(null == iv_token || iv_token.equals("")))
		{
			WebHelper webHelper = WebHelper.getInstance();
			webHelper.getCamerasList(iv_token);
			return true;
		}else{
			return false;
		}
	}

	private void getToken()
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject message = new JSONObject();
		try
		{
			message.put("domain", userInfo.domain);
			nSendMessageToServer(null, D3Request.IV_GET_TOKEN, message, false);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}


	@Override
	public void onResume()
	{
		super.onResume();
		bindD3();
		repeat = false;
		activity.registerReceiver(IVCamerasGetReceiver, new IntentFilter(WebHelper.BROADCAST_CAMERA_GET));
		activity.registerReceiver(IVGetTokenResponseFromServer, new IntentFilter(D3Service.BROADCAST_IV_GET_TOKEN));
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_ALARM_ADD_UPD_RMV);
		activity.registerReceiver(updateReceiver, intentFilter);
		activity.registerReceiver(dahuaCamerasGetReceiver, new IntentFilter(NDahuaHelper.BROADCAST_GET_DAHUA_PREVIEW));
		updateBottomHelper();
	}

	private void updateBottomHelper(){
		activity.setBottomInfoBehaviour(activity);
	}
	@Override
	public void onPause()
	{
		super.onPause();
		activity.unbindService(serviceConnection);
		activity.unregisterReceiver(IVCamerasGetReceiver);
		activity.unregisterReceiver(IVGetTokenResponseFromServer);
		activity.unregisterReceiver(updateReceiver);
		activity.unregisterReceiver(dahuaCamerasGetReceiver);
	}

	public void onBackPressed()
	{
		NDialog nDialog = new NDialog(activity, R.layout.n_dialog_question);
		nDialog.setTitle(getString(R.string.N_LOGOUT_Q));
		nDialog.setOnActionClickListener((value, b) -> {
			switch (value){
				case NActionButton.VALUE_OK:
					activity.finishAffinity();
					break;
				case NActionButton.VALUE_CANCEL:
					nDialog.dismiss();
					break;
			}

		});
		nDialog.show();
	}


	private void bindD3(){
		Intent intent = new Intent(activity, D3Service.class);
		serviceConnection = getServiceConnection();
		activity.bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
				if(null!=messagesQueue && messagesQueue.size() > 0){
					for(ApiElement apiElement : messagesQueue){
						nSendMessageToServer(apiElement);
					}
					messagesQueue.clear();
				}
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	public D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			//			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(ApiElement apiElement)
	{
		return Func.nSendMessageToServer(apiElement.d3Element, apiElement.Q, apiElement.data, getLocalService(), activity, apiElement.command, sending);
	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		ApiElement apiElement = new ApiElement(d3Element, Q, data, command);
		D3Service service = getLocalService();
		if(null==service){
			messagesQueue.add(apiElement);
			return false;
		}else
		{
			return Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), activity, command, sending);
		}
	}

	class ApiElement{

		D3Element d3Element;
		String Q;
		JSONObject data;
		boolean command;

		ApiElement(D3Element d3Element, String Q, JSONObject data, boolean command){
			this.d3Element = d3Element;
			this.Q = Q;
			this.data  =data;
			this.command = command;
		}

	}

}
