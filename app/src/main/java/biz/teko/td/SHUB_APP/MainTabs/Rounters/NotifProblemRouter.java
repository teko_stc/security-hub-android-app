package biz.teko.td.SHUB_APP.MainTabs.Rounters;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentManager;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.MainTabs.Fragments.ServiceInfoDialogFragment;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NotificationUtils;
import biz.teko.td.SHUB_APP.Utils.PrefUtils;

public class NotifProblemRouter {
    private final NotificationUtils notificationUtils;
    private FragmentManager fragmentManager;
    private Context context;
    private PrefUtils prefUtils;

    public NotifProblemRouter(FragmentManager fragmentManager, Context context) {
        this.fragmentManager = fragmentManager;
        this.context = context;
        this.notificationUtils = NotificationUtils.getInstance(context);
        prefUtils = PrefUtils.getInstance(context);
    }

    public void setup() {
//        if (prefUtils.getNotificationProblem())
            showNotificationProblemsDialog();
    }

    private void showNotificationProblemsDialog() {
        JSONObject jsonObject = new JSONObject();
        try
        {
            if(!Func.isServiceRunningInForeground(context, D3Service.class))
            {
                ServiceInfoDialogFragment infoDialog = new ServiceInfoDialogFragment();
                infoDialog.setInputListeners(new ServiceInfoDialogFragment.IInputListeners()
                {
                    @Override
                    public void onEnable()
                    {
                        startForegroundD3Service();
                    }

                    @Override
                    public void onMore()
                    {
                        startMoreInfoLink();
                    }
                });
                infoDialog.show(fragmentManager, "serviceInfo");
                jsonObject.put("action", "Show notification problems dialog (foreground service disabled)");
            }else{
                jsonObject.put("action", "Foreground service already enabled");
                PrefUtils.getInstance(context).setNotificationsProblem(false);
                Func.log_d(D3Service.LOG_TAG_EVENTS + " Main activity", "Reset notification problems pref");
            }

            Func.log_d(D3Service.LOG_TAG_EVENTS + "Router", jsonObject.toString());
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

    }

    private void startMoreInfoLink() {
        String url = "https://cloud.security-hub.ru/wiki/doku.php?id=%D1%81%D0%B5%D1%80%D0%B2%D0%B8%D1%81_%D1%83%D0%B2%D0%B5%D0%B4%D0%BE%D0%BC%D0%BB%D0%B5%D0%BD%D0%B8%D0%B9_security_hub";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        context.startActivity(intent);
    }

    private void startForegroundD3Service() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            deleteServiceNotifChannel();
            updateServiceChannelId();
            createServiceNotifChannel(NotificationManager.IMPORTANCE_HIGH);
        }else{
            prefUtils.setConnectionMode(true);
        }
        Func.restartD3ServiceForced(context, "Router");
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createServiceNotifChannel(int importance){
        notificationUtils.createChannelNoVibro(getServiceNotifChannelId(),
                context.getResources().getString(R.string.NOTIF_CONNECTION_SERVICE_ENABLED),
                importance
        );
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private  void deleteServiceNotifChannel(){
        notificationUtils.deleteServiceNotifChannel();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String getServiceNotifChannelId() {
        return notificationUtils.getServiceNotifChannelId();
    }

    private void updateServiceChannelId() {
        notificationUtils.updateServiceNotifChannelId();
    }
}
