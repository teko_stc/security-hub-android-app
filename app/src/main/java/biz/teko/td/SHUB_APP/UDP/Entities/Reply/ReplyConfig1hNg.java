package biz.teko.td.SHUB_APP.UDP.Entities.Reply;

import android.os.Bundle;

import java.util.HashMap;

import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_1;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_2;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_3;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_4;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_5;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_6;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_7;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.HIGH_TEMP_8;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_1;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_2;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_3;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_4;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_5;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_6;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_7;
import static biz.teko.td.SHUB_APP.UDP.Utils.ConfigConstants.LOW_TEMP_8;

public class ReplyConfig1hNg extends ReplyConfig1h {

    public ReplyConfig1hNg(byte[] data) {
        super(data);
    }

    @Override
    public void setNewAnotherConfig(Bundle bundle, String configType) {
        super.setNewAnotherConfig(bundle, configType);
        setTempConfig(bundle, configType);
    }

    private void setTempConfig(Bundle bundle, String configType) {
        switch (configType) {
            case LOW_TEMP_1:
                setNewTemp(0x2C6, LOW_TEMP_1, bundle); break;
            case HIGH_TEMP_1:
                setNewTemp(0x2C7, HIGH_TEMP_1, bundle); break;
            case LOW_TEMP_2:
                setNewTemp(0x2C8, LOW_TEMP_2, bundle); break;
            case HIGH_TEMP_2:
                setNewTemp(0x2C9, HIGH_TEMP_2, bundle); break;
            case LOW_TEMP_3:
                setNewTemp(0x2CA, LOW_TEMP_3, bundle); break;
            case HIGH_TEMP_3:
                setNewTemp(0x2CB, HIGH_TEMP_3, bundle); break;
            case LOW_TEMP_4:
                setNewTemp(0x2CC, LOW_TEMP_4, bundle); break;
            case HIGH_TEMP_4:
                setNewTemp(0x2CD, HIGH_TEMP_4, bundle); break;
            case LOW_TEMP_5:
                setNewTemp(0x2CE, LOW_TEMP_5, bundle); break;
            case HIGH_TEMP_5:
                setNewTemp(0x2CF, HIGH_TEMP_5, bundle); break;
            case LOW_TEMP_6:
                setNewTemp(0x2D0, LOW_TEMP_6, bundle); break;
            case HIGH_TEMP_6:
                setNewTemp(0x2D1, HIGH_TEMP_6, bundle); break;
            case LOW_TEMP_7:
                setNewTemp(0x2D2, LOW_TEMP_7, bundle); break;
            case HIGH_TEMP_7:
                setNewTemp(0x2D3, HIGH_TEMP_7, bundle); break;
            case LOW_TEMP_8:
                setNewTemp(0x2D4, LOW_TEMP_8, bundle); break;
            case HIGH_TEMP_8:
                setNewTemp(0x2D5, HIGH_TEMP_8, bundle); break;
        }
    }

    private void setNewTemp(int offset, String type, Bundle bundle) {
        this.data[offset] = Byte.parseByte(bundle.getString(type));
    }

    @Override
    public HashMap<String, String> getMap() {
        HashMap<String, String> map = super.getMap();
        return getTempMap(map);
    }

    private HashMap<String, String> getTempMap(HashMap<String, String> map) {
        map.put(LOW_TEMP_1, String.valueOf(getTemp(0x2C6)[0]));
        map.put(HIGH_TEMP_1, String.valueOf(getTemp(0x2C7)[0]));
        map.put(LOW_TEMP_2, String.valueOf(getTemp(0x2C8)[0]));
        map.put(HIGH_TEMP_2, String.valueOf(getTemp(0x2C9)[0]));
        map.put(LOW_TEMP_3, String.valueOf(getTemp(0x2CA)[0]));
        map.put(HIGH_TEMP_3, String.valueOf(getTemp(0x2CB)[0]));
        map.put(LOW_TEMP_4, String.valueOf(getTemp(0x2CC)[0]));
        map.put(HIGH_TEMP_4, String.valueOf(getTemp(0x2CD)[0]));
        map.put(LOW_TEMP_5, String.valueOf(getTemp(0x2CE)[0]));
        map.put(HIGH_TEMP_5, String.valueOf(getTemp(0x2CF)[0]));
        map.put(LOW_TEMP_6, String.valueOf(getTemp(0x2D0)[0]));
        map.put(HIGH_TEMP_6, String.valueOf(getTemp(0x2D1)[0]));
        map.put(LOW_TEMP_7, String.valueOf(getTemp(0x2D2)[0]));
        map.put(HIGH_TEMP_7, String.valueOf(getTemp(0x2D3)[0]));
        map.put(LOW_TEMP_8, String.valueOf(getTemp(0x2D4)[0]));
        map.put(HIGH_TEMP_8, String.valueOf(getTemp(0x2D5)[0]));
        return map;
    }

    private byte[] getTemp(int offset) {
        return getBytes(1, offset);
    }

    @Override
    public byte[] getData() {
        return data;
    }
}
