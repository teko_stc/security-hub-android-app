package biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3.D3XProtoEvent.SType;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 28.06.2017.
 */

public class WizardSectionsSpinnerAdapter extends ArrayAdapter<Section>
{
	private final Context context;
	private final Section[] sections;

	public WizardSectionsSpinnerAdapter(Context context, int resource, Section[] sections)
	{
		super(context, resource, sections);
		this.context = context;
		this.sections = sections;
	}

	public int getCount(){
		return sections.length;
	}

	public Section getItem(int position){
		return sections[position];
	}

	public long getItemId(int position){
		return position;
	}

	@Override
	public int getPosition(@Nullable Section item)
	{
		return super.getPosition(item);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
//		String sType = sections[position].id == 0? "" : D3Service.d3XProtoConstEvent.getSTypeById(sections[position].detector).caption;
		SType sType = D3Service.d3XProtoConstEvent.getSTypeById(sections[position].detector);
		label.setText(sections[position].name + ( sections[position].id == 0? "" : "(" + (null!=sType ? sType.caption : "") + ")"));
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
//		String sType = sections[position].id == 0? "" :  D3Service.d3XProtoConstEvent.getSTypeById(sections[position].detector).caption;
		SType sType = D3Service.d3XProtoConstEvent.getSTypeById(sections[position].detector);
		label.setText(sections[position].name + ( sections[position].id == 0? "" : "(" + (null!=sType ? sType.caption : "") + ")"));
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;	}
}
