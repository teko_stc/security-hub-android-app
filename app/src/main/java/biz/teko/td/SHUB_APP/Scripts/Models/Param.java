package biz.teko.td.SHUB_APP.Scripts.Models;

public class Param
{
	public String name;
	public String format;
	public String type;
	public String description;
	public String group = null;
	public String value;
	public String def;

	public Param(){}

	public Param(String name, String value){
		this.name = name;
		this.value = value;
	}

	public Param(String name, String format, String type, String description){
		this.name = name;
		this.format = format;
		this.type = type;
		this.description = description;
	}

	public Param(String name, String format, String type, String description, String group){
		this.name = name;
		this.format = format;
		this.type = type;
		this.description = description;
		this.group = group;
	}

}
