package biz.teko.td.SHUB_APP.Scripts.Activities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.TextStyle;

import java.util.LinkedList;
import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Scripts.Models.Param;
import biz.teko.td.SHUB_APP.Scripts.Models.Script;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class ScriptActivity extends BaseActivity
{
	private int scriptId;
	private DBHelper dbHelper;
	private Context context;
	private Script script;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private SharedPreferences sp;
	private String iso;
	private Device device;
	private LinkedList<Param> paramsList;
	private int siteId;
	private int sending;

//	private BroadcastReceiver commandResultReceiver = new BroadcastReceiver()
//	{
//		@Override
//		public void onReceive(Context context, Intent intent)
//		{
//			Func.showCommandResultSnackbar(context, findViewById(android.R.id.content), intent.getIntExtra("result", -1));
//		}
//	};

	private TextView editButton;
	private ImageButton scriptInfoButton;
	private UserInfo userInfo;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_script);

		context = this;
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		iso = sp.getString("prof_language_value", context.getString(R.string.default_iso));

		siteId = getIntent().getIntExtra("site_id", -1);
		scriptId = getIntent().getIntExtra("script_id", -1);
		if(-1!= scriptId){
			script = dbHelper.getFullDeviceScriptByScriptId(scriptId);
			device =  dbHelper.getDeviceById(script.device);
		}

		serviceConnection = new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};

		ImageView backButton = (ImageView) findViewById(R.id.scriptCloseButton);
		backButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		editButton = (TextView) findViewById(R.id.scriptEditButton);
		editButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				goToEdit();
			}
		});

		scriptInfoButton = (ImageButton) findViewById(R.id.scriptInfoButton);
		if(null!=scriptInfoButton) scriptInfoButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				showScriptInfo();
			}
		});

		TextView deleteButton = (TextView) findViewById(R.id.scriptDelButton);
		deleteButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				deleteScript();
			}
		});

		if(!Func.getRemoteSetRights(userInfo, dbHelper, siteId, script.device)){
			if(null!=editButton) editButton.setVisibility(View.GONE);
			if(null!=deleteButton) deleteButton.setVisibility(View.GONE);
		}

		setViews();
	}

	private void setViews()
	{
		TextView scriptName = (TextView) findViewById(R.id.scriptName);
		scriptName.setText(script.name);

		TextView scriptBind = (TextView) findViewById(R.id.scriptBindText);
		if(null!=scriptBind && null!=script && null!=device)scriptBind.setText(dbHelper.getZoneNameById(device.id, script.bind));

		TextView scriptDest = (TextView) findViewById(R.id.scriptDestination);
		if(null!=device){
			scriptDest.setText(device.getName() + " " + context.getString(R.string.DTA_SERIAL_NUMBER) + Integer.toString(device.account));
		}



		LinearLayout paramsFrame = (LinearLayout) findViewById(R.id.paramsFrame);
		TextView noParamsText = (TextView) findViewById(R.id.noParamsText);
		JSONArray paramsArray  = null;
		try
		{
			paramsArray = new JSONArray(script.params);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		if(paramsArray != null && paramsArray.length() !=0)
		{
			paramsFrame.setVisibility(View.VISIBLE);
			noParamsText.setVisibility(View.GONE);

			paramsList = new LinkedList<>();
			try
			{
				for (int i = 0; i < paramsArray.length(); i++)
				{
					JSONObject paramObject = paramsArray.getJSONObject(i);
					Param param = new Param(paramObject.getString("name"), paramObject.getString("value"));
					paramsList.add(param);
				}
			} catch (JSONException e)
			{
				e.printStackTrace();
			}

			try
			{
				JSONArray paramsLibArray = null;
				try
				{
					paramsLibArray = new JSONArray(script.paramsLib);
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
				showParams(paramsFrame, paramsLibArray);
			} catch (NullPointerException e)
			{
				e.printStackTrace();
				if(null!=editButton) editButton.setVisibility(View.GONE);
				if(null!=scriptInfoButton) scriptInfoButton.setEnabled(false);
			}
		}else{
			paramsFrame.setVisibility(View.GONE);
			noParamsText.setVisibility(View.VISIBLE);
		}
	}

	//PARSE PARAMS
	private void showParams(LinearLayout paramsFrame, JSONArray paramsLibArray) // 0- vert, 1 -hor
	{
		try
		{
			for (int i = 0; i < paramsLibArray.length(); i++)
			{
				JSONObject paramLibObject = paramsLibArray.getJSONObject(i);
				String type = getS(paramLibObject, "type");
				if (null != type)
				{
					switch (type)
					{
						case "time":
							addTimeFrame(paramsFrame, paramLibObject);
							break;
						case "group":
						case "time_utc_group":
							addGroupFrame(paramsFrame, paramLibObject);
							break;
						default:
							addFrame(type, paramsFrame, paramLibObject);
							break;

					}
				}else{
					addFrame(null, paramsFrame, paramLibObject);
				}
			}


		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

//	private void addHorizontalGroupFrame(LinearLayout paramsFrame, JSONObject paramLibObject)
//	{
//		LinearLayout paramFrame = new LinearLayout(context);
//		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//		paramFrame.setLayoutParams(lp);
//		paramFrame.setOrientation(LinearLayout.VERTICAL);
//
//		TextView description = new TextView(context);
//		description.setText(getScriptDataForCurrLang(getS(paramLibObject, "description")));
//		paramFrame.addView(description);
//
//		try
//		{
//			showParams(paramFrame, new JSONArray(getS(paramLibObject, "group")));
//		} catch (JSONException e)
//		{
//			e.printStackTrace();
//		}
//
//		paramsFrame.addView(paramFrame);
//	}

	private void addGroupFrame(LinearLayout paramsFrame, JSONObject paramLibObject)
	{
		LinearLayout paramFrame = new LinearLayout(context);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramFrame.setPadding(0, Func.dpToPx(4, context), 0, Func.dpToPx(4, context));
		paramFrame.setLayoutParams(lp);
		paramFrame.setOrientation(LinearLayout.VERTICAL);

		addDescriptionInFrame(paramFrame, paramLibObject);

		try
		{
			showParams(paramFrame, new JSONArray(getS(paramLibObject, "group")));
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		paramsFrame.addView(paramFrame);
	}

	private void addTimeFrame(LinearLayout paramsFrame, JSONObject paramLibObject)
	{
		LinearLayout paramFrame = new LinearLayout(context);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramFrame.setPadding(0, Func.dpToPx(4, context), 0, Func.dpToPx(4, context));
		paramFrame.setLayoutParams(lp);
		paramFrame.setGravity(Gravity.CENTER);
		paramFrame.setOrientation(LinearLayout.VERTICAL);

		addDescriptionInFrame(paramFrame, paramLibObject);

		LinearLayout paramFrame2 = new LinearLayout(context);
		LinearLayout.LayoutParams lp2 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramFrame2.setLayoutParams(lp2);
		paramFrame2.setOrientation(LinearLayout.HORIZONTAL);

		try
		{
			showParams(paramFrame2, new JSONArray(getS(paramLibObject, "group")));
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		paramFrame.addView(paramFrame2);
		paramsFrame.addView(paramFrame);
	}

	private void addFrame(String type, LinearLayout paramsFrame, JSONObject paramLibObject)
	{
		LinearLayout paramFrame = new LinearLayout(context);
		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		paramFrame.setGravity(Gravity.CENTER);
		paramFrame.setLayoutParams(lp);
		paramFrame.setOrientation(LinearLayout.VERTICAL);

		addDescriptionInFrame(paramFrame,paramLibObject);

		String name = getS(paramLibObject, "name");
		if(null!= name){
			for(Param param:paramsList){
				if(name.equals(param.name)){
					addValueInFrame(type, paramFrame,type!=null? type.equals("list")? getListExtraTitle(paramLibObject, param.value):param.value:null);
				}
			}
		}
		paramsFrame.addView(paramFrame);
	}

	private String getListExtraTitle(JSONObject paramLibObject, String value)
	{
		JSONArray extraArray = null;
		try
		{
			extraArray = paramLibObject.getJSONArray("extra");
			if(null!=extraArray){
				int count = extraArray.length();
				if(0!=count){
					for(int i = 0; i < count; i++){
						JSONObject extraObject = extraArray.getJSONObject(i);
						if(Integer.valueOf(value) == extraObject.getInt("value")){
							return Func.getScriptDataForCurrLang(getS(extraObject, "description"), iso);
						}
					}
				}
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	private void addValueInFrame(String type, LinearLayout paramFrame, String value)
	{
		TextView valueText = new TextView(context);
		ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		valueText.setPadding(0, Func.dpToPx(8, context), 0, Func.dpToPx(8, context));
		valueText.setLayoutParams(layoutParams);
		valueText.setTextSize(TypedValue.COMPLEX_UNIT_SP,24);
		valueText.setGravity(Gravity.CENTER);
		Typeface typeface = ResourcesCompat.getFont(context, R.font.open_sans);
		valueText.setTypeface(typeface);
		if(null!=type && null !=value)
		{
			switch (type)
			{
				case "int":
					valueText.setText(value);
					break;
				case "xfloat":
					valueText.setText(Float.toString(Float.parseFloat(value)/100));
					break;
				case "zone":
					valueText.setText(dbHelper.getZoneNameById(device.id, Integer.valueOf(value)));
					break;
				case "section":
					valueText.setText(dbHelper.getSectionNameByIdWithOptions(device.id, Integer.valueOf(value)));
					break;
				case "zones":
				case "sections":
					int intValue = Integer.valueOf(value);
					String valueString = "";
					if(0 < intValue)
					{
						int i1 = 1;
						while ((intValue >> (i1 - 1)) > 0)
						{
							if (((intValue >> (i1 - 1)) & 0x1) == 1)
							{
								if (type.equals("sections"))
								{
									valueString += dbHelper.getSectionNameByIdWithOptions(script.device, i1) + ", ";
								} else
								{
									valueString += dbHelper.getZoneNameById(script.device, i1) + ", ";
								}
							}
							i1++;
						}
						valueString = valueString.substring(0, valueString.length() - 2);
					}else{
						if (type.equals("sections"))
						{
							valueString += getString(R.string.SCRIPT_NO_SELECTED_SECTIONS);
						} else
						{
							valueString += getString(R.string.SCRIPT_NO_SELECTED_ZONES);
						}
					}
					valueText.setText(valueString);
					break;
				case "bool":
					valueText.setText(value);
					break;
				case "bit_mask":
					valueText.setText(value);
					break;
				case "time_zone":
				case "time_zone_m":
					valueText.setText(ZoneOffset.ofTotalSeconds(Integer.valueOf(value)*60).getDisplayName(TextStyle.NARROW, Locale.getDefault()));
					break;
				case "time_m_utc":
					int time = Integer.parseInt(value);
					time += Func.getTimeZoneOffset();
					int hour = time / 60;
					int minute = time % 60;
					valueText.setText((hour > 9 ? hour : "0" + hour )+ " : " + (minute > 9 ? minute : "0" + minute));
					break;
				default:
					valueText.setText(value);
					break;
			}
		}else{
			valueText.setText(value);
		}
		paramFrame.addView(valueText);
	}

	private void addDescriptionInFrame(LinearLayout paramFrame, JSONObject paramLibObject)
	{
		addStringInFrame(paramFrame, Func.getScriptDataForCurrLang(getS(paramLibObject, "description"), iso));
	}

	private void addStringInFrame(LinearLayout paramFrame, String s)
	{
		TextView description = new TextView(context);
		ViewGroup.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		description.setLayoutParams(layoutParams);
		description.setText(s);
		paramFrame.addView(description);
	}

	private String getS(JSONObject jsonObject, String key)
	{
		try{
			return jsonObject.getString(key);
		}catch (JSONException e){
			return null;
		}
	}


	private void showScriptInfo()
	{
		String scriptInfo =  Func.getScriptDataForCurrLang(script.description, iso);
		Func.showInfo(context, scriptInfo);
	}



	private void deleteScript()
	{
		if(null!=device){
			if (dbHelper.isDeviceOnline(device.id))
			{
				if (0 == dbHelper.getSiteDeviceArmStatus(siteId, device.id))
				{
					AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
					adb.setMessage(R.string.SCRIPT_DEL_MESSAGE);
					adb.setPositiveButton(context.getString(R.string.DELETE), new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialogInterface, int i)
						{
							sendDelete();
						}
					});
					adb.setNegativeButton(R.string.ADB_CANCEL, new DialogInterface.OnClickListener()
					{
						@Override
						public void onClick(DialogInterface dialogInterface, int i)
						{
							dialogInterface.dismiss();
						}
					});
					adb.show();
				} else
				{
					Func.nShowMessage(context, context.getString(R.string.SCRIPTS_ERROR_ARMED));
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.ERROR_NO_DEVICE_INFO));
		}

	}

	private void sendDelete()
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject jsonObject = new JSONObject();
		try
		{
			jsonObject.put("device_id", script.device);
			jsonObject.put("bind", script.bind);
			D3Request d3Request = D3Request.createMessage(userInfo.roles, "SCRIPT_DEL", jsonObject);
			if(null==d3Request.error){
				sendMessageToServer(d3Request);
				finish();
			}else{
				Func.pushToast(context, d3Request.error, (Activity) context);
			}

		} catch (JSONException e)
		{
			e.printStackTrace();
		}

	}

	private void goToEdit()
	{
		if(null!=device){
			if (dbHelper.isDeviceOnline(device.id))
			{
				if (0 == dbHelper.getSiteDeviceArmStatus(siteId, device.id))
				{
					Intent intent = new Intent(getBaseContext(), WizardScriptSetActivity.class);
					intent.putExtra("script_id", script.id);
					intent.putExtra("lib_script_id", script.libId);
					intent.putExtra("device_id", null!=device? device.id : -1);
					intent.putExtra("site_id", siteId);
					intent.putExtra("target", 1);
					overridePendingTransition(R.animator.enter, R.animator.exit);
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
					{
						// Apply activity transition
						startActivityForResult(intent, D3Service.EDIT_SCRIPT, ActivityOptions.makeSceneTransitionAnimation((Activity) context).toBundle());
					} else
					{
						// Swap without transition
						startActivityForResult(intent, D3Service.EDIT_SCRIPT);
						overridePendingTransition(R.animator.enter, R.animator.exit);
					}			} else
				{
					Func.nShowMessage(context, context.getString(R.string.SCRIPTS_ERROR_ARMED));
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_NO_CONNECTION_MESSAGE));
			}
		}else{
			Func.nShowMessage(context, context.getString(R.string.ERROR_NO_DEVICE_INFO));
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
//		registerReceiver(commandResultReceiver, new IntentFilter(D3Service.BROADCAST_COMMAND_RESULT));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
//		unregisterReceiver(commandResultReceiver);
	}

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				}
			} else
			{
				Func.nShowMessage(context, context.getString(R.string.EA_D3_OFF_1) + " " + context.getString(R.string.application_name) + " " + context.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(this, getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode == D3Service.EDIT_SCRIPT){
			if(resultCode == RESULT_OK){
				Intent intent = new Intent();
				setResult(RESULT_OK, intent);
				finish();
			}
		}
	}

//	@Override
//	public void onBackPressed()
//	{
//		NControlDialog nControlDialog = new NControlDialog(context, R.layout.n_dialog_question);
//		nControlDialog.setTitle("Хотите выйти из приложения?");
//		nControlDialog.setOnActionClickListener(new NControlDialog.OnActionClickListener()
//		{
//			@Override
//			public void onActionClick(int value)
//			{
//				switch (value){
//					case NActionButton.VALUE_OK:
//						finishAffinity();
//						break;
//					case NActionButton.VALUE_CANCEL:
//						nControlDialog.dismiss();
//						break;
//				}
//
//			}
//		});
//		nControlDialog.show();
//	}
}
