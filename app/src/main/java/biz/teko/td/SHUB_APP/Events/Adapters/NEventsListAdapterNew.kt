package biz.teko.td.SHUB_APP.Events.Adapters

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.util.Pair
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import biz.teko.td.SHUB_APP.Cameras.Activities.IV.NCameraRecordViewActivity
import biz.teko.td.SHUB_APP.D3DB.DBHelper
import biz.teko.td.SHUB_APP.D3DB.Event
import biz.teko.td.SHUB_APP.Events.Activities.EventActivity
import biz.teko.td.SHUB_APP.R
import biz.teko.td.SHUB_APP.Utils.Dir.Const
import biz.teko.td.SHUB_APP.Utils.Dir.Func
import biz.teko.td.SHUB_APP.Utils.NViews.NEAListElement
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class NEventsListAdapterNew(
    private val context: Context,
    private val layout: Int,
    private var cursor: Cursor,
    flags: Int,
    anim: Boolean
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dbHelper: DBHelper = DBHelper.getInstance(context)
    private val flags: Int = flags
    private val anim: Boolean = anim
    private val eventList = ArrayList<Event>()
    private val nEventsListItems: MutableList<NEventsListItem> = mutableListOf()
    private val HEADER_VIEW = 1
    private val ITEM_VIEW = 2

    fun update(c: Cursor) {
        cursor = c
        if (eventList.isNotEmpty()) eventList.clear()
        if (nEventsListItems.isNotEmpty()) nEventsListItems.clear()
        events
        //I tried it with DifUtil. I didn't like the way it worked.
        notifyDataSetChanged()
    }

    private val events: Unit
        private get() {
            if (cursor.moveToFirst()) {
                do {
                    eventList.add(dbHelper.getEvent(cursor))
                } while (cursor.moveToNext())
                eventGroups()
            }
        }

    private fun eventGroups(){
        val groupedEvents = eventList.groupBy {
            Func.getServerDateTextShort(context, it.time) + "      " +
                    Func.getWeekDayText(context, it.time * 1000)
        }
        for((key, value) in groupedEvents){
            nEventsListItems.add(NEventsListItem(HEADER_VIEW, key, null))
            value.forEach {
                nEventsListItems.add(NEventsListItem(ITEM_VIEW, null, it))
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if(viewType == HEADER_VIEW){
            val view = LayoutInflater.from(context).inflate(R.layout.n_events_list_element_head_view, parent, false)
            NEventsHeaderViewHolder(view)
        } else{
            val view = LayoutInflater.from(context).inflate(layout, parent, false)
            NEventsViewHolder(view, context, dbHelper, flags, anim)
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(getItemViewType(position) == 1) {
            val nEventsHeaderViewHolder = holder as NEventsHeaderViewHolder
            nEventsListItems[position].title?.let { nEventsHeaderViewHolder.bind(it) }
        }
        else{
            val nEventsViewHolder = holder as NEventsViewHolder
            nEventsViewHolder.bind(nEventsListItems[position].event)
        }
    }

    override fun getItemCount(): Int {
        return nEventsListItems.size
    }

    override fun getItemViewType(position: Int): Int {
        return nEventsListItems[position].type
    }

    class NEventsHeaderViewHolder(
        private val view: View
    ) : RecyclerView.ViewHolder(view) {

        fun bind(title: String){
            val nTextTitle = view.findViewById<TextView>(R.id.nTextTitle)
            nTextTitle.text = title
        }
    }

    class NEventsViewHolder(
        private val view: View,
        private val context: Context,
        private val dbHelper: DBHelper,
        private val flags: Int,
        private val anim: Boolean
    ) : RecyclerView.ViewHolder(view) {

        fun bind(event: Event?) {
            if (null != event) {
                val v = view.findViewById<View>(R.id.nEventsListElement) as NEAListElement
                val eventDesc = event.explainEventRegDescriptionWithClass(context)
                v.setClass(eventDesc.first)
                v.setTitle(eventDesc.second)
                v.setLocation(
                    """
                        ${dbHelper.getSiteNameByDeviceSection(event.device, event.section)}
                        
                        """.trimIndent() + event.getEventLocation(
                        context
                    )
                )
                v.setTime(Func.getServerTimeTextShort(context, event.time))
                v.setDate(Func.getServerDateTextShort(context, event.time))
                v.setImage(event.getListIcon(context))
                v.setOnElementClickListener { openEventActivity(event.id, v) }
                val action = event.link
                var icon = 0
                when (action) {
                    Event.LINK_GEO -> icon = R.drawable.ic_location_filled
                    Event.LINK_VIDEO -> icon = R.drawable.ic_camera_filled
                    Event.LINK_VIDEO_FAILED -> icon = R.drawable.ic_camera_failed_filled
                    Event.NO_LINK -> {}
                    else -> {}
                }
                if (0 != action) {
                    v.setActionButtonVisiblity(View.VISIBLE)
                    v.setActionButtonImage(icon)
                    v.setOnActionClickListener {
                        when (action) {
                            Event.LINK_GEO -> openGeo(event)
                            Event.LINK_VIDEO -> {
                                val intent = Intent(context, NCameraRecordViewActivity::class.java)
                                intent.putExtra("event", event.id)
                                context.startActivity(intent)
                                (context as Activity).overridePendingTransition(
                                    R.animator.enter,
                                    R.animator.exit
                                )
                            }
                            Event.LINK_VIDEO_FAILED -> Func.nShowMessage(
                                context, context.getString(R.string.IV_CLIP_FAILED)
                            )
                            Event.NO_LINK -> {}
                            else -> {}
                        }
                    }
                } else {
                    v.setActionButtonVisiblity(View.GONE)
                }
            }
        }

        private fun openGeo(event: Event) {
            try {
                val jsonObject = JSONObject(event.jdata)
                val lat = jsonObject.getString("lat")
                val lon = jsonObject.getString("lon")
                //						String uri = String.format(LocaleD3.ENGLISH, "geo:%f,%f", lat, lon);
                val uri =
                    String.format("geo:0,0?q=" + lat + "," + lon + context.getString(R.string.EVENTS_ALARM_PLACE))
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)
            } catch (e: JSONException) {
                e.printStackTrace()
                Func.nShowMessage(context, context.getString(R.string.N_EVENTS_LOCATION_GET_ERROR))
            }
        }

        private fun openEventActivity(id: Int, v: NEAListElement) {
            val intent = Intent(context, EventActivity::class.java)
            intent.putExtra("event", id)
            if (anim) {
                intent.putExtra("transition", true)
                intent.putExtra("transition_start", Func.getTextSize(context, v.titleTextView))
                context.startActivity(intent, getTransitionOptions(v).toBundle())
            } else {
                context.startActivity(intent)
            }
        }

        private fun getTransitionOptions(view: View): ActivityOptions {
            return ActivityOptions.makeSceneTransitionAnimation(
                context as Activity,
                Pair((view as NEAListElement).titleTextView, Const.TITLE_TRANSITION_NAME)
            )
        }
    }

    init {
        events
    }

    data class NEventsListItem(val type: Int, val title: String?, val event: Event?)
}