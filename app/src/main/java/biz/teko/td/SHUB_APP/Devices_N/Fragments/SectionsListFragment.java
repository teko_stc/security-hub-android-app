package biz.teko.td.SHUB_APP.Devices_N.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Devices_N.Adapters.SectionsListRecyclerAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NWithAButtonElement;
import biz.teko.td.SHUB_APP.Wizards.Activity.WizardAllActivity;

public class SectionsListFragment extends Fragment {
	private Context context;
	private DBHelper dbHelper;
	private Filter filter = new Filter("");
	private int site;
	private UserInfo userInfo;
	private RecyclerView sectionsList;
	private NWithAButtonElement noElementsView;
	private Cursor cursor;
	private SectionsListRecyclerAdapter sectionsListAdapter;

	public static SectionsListFragment newInstance(int siteId, Filter filter) {
		SectionsListFragment sectionsListFragment = new SectionsListFragment();
		sectionsListFragment.setSiteId(siteId);
		sectionsListFragment.setFilter(filter);
		return sectionsListFragment;
	}

	public Cursor getCursor() {
		return cursor;
	}

	public void setCursor(Cursor cursor) {
		this.cursor = cursor;
		sectionsListAdapter.update(cursor);
	}

	public Filter getFilter() {
		return filter;
	}

	private final BroadcastReceiver updateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			sectionsListAdapter.setSynchronous(true);
			updateSectionsList();
		}
	};

	public void updateContents() {
		updateCursor(filter);
		if (null != sectionsListAdapter)
			sectionsListAdapter.update(cursor);
	}

	public void setFilter(Filter filter) {
		this.filter = filter;

		if (dbHelper != null && sectionsListAdapter != null) {
			updateCursor(filter);
			sectionsListAdapter.update(cursor);
		}
	}

	public void setSiteId(int id) {
		Bundle b = new Bundle();
		b.putInt("site", id);
		setArguments(b);
		site = id;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.n_fragment_devices_list, container, false);
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		site = getArguments().getInt("site");
		userInfo = dbHelper.getUserInfo();

		noElementsView = v.findViewById(R.id.noElementsView);

		sectionsList = v.findViewById(R.id.nDevicesList);

		return v;
	}

	public void updateSectionsList() {
		updateCursor(filter);
		updateView();
	}

	private void updateView() {
		if (null != sectionsListAdapter) {
			sectionsListAdapter.update(cursor);
		} else {
			sectionsListAdapter = new SectionsListRecyclerAdapter(context, cursor, (DevicesListFragment) getParentFragment());
			sectionsList.setAdapter(sectionsListAdapter);
		}
		refreshViewVisiblity();
	}

	private void refreshViewVisiblity()
	{
		if (null != cursor && 0 < cursor.getCount()) {
			if (null != noElementsView) noElementsView.setVisibility(View.GONE);
			if (null != sectionsList)
				sectionsList.setVisibility(View.VISIBLE);
		} else if (filter.getQuery().isEmpty()) {
			if (null != noElementsView) {
				noElementsView.setVisibility(View.VISIBLE);
				if( 0 != dbHelper.getDevicesCount())
				{
					noElementsView.setTitle(getString(R.string.N_SECTIONS_MESSAGE_NO_SECTIONS));
				}else if(0 != dbHelper.getSitesCount()){
					noElementsView.setTitle(getString(R.string.N_DEVICES_MESSAGE_NO_DEVICES));
					noElementsView.setActionTitle(getString(R.string.N_DEVICES_BUTTON_SET));
				}else{
					noElementsView.setTitle(getString(R.string.N_MAIN_EMPTY));
					noElementsView.setActionTitle(getString(R.string.N_MAIN_EMPTY_START));
				}
				noElementsView.setOnChildClickListener((action, option) ->
						startActivity(new Intent(context, WizardAllActivity.class)));
			}
			if (null != sectionsList)
				sectionsList.setVisibility(View.GONE);
		}
	}


	private void updateCursor(Filter filter) {
		cursor = dbHelper.getNAllSectionsCursorBySiteIdFiltered(site, filter.getQuery());
	}


	public void onResume() {
		super.onResume();
		if (sectionsListAdapter != null)
			sectionsListAdapter.setSynchronous(sectionsListAdapter.getItemCount() != 0);
		updateSectionsList();
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(D3Service.BROADCAST_SITE_SECTION_ZONE_UPD_RMV);
		intentFilter.addAction(D3Service.BROADCAST_AFFECT);
		intentFilter.addAction(D3Service.BROADCAST_DEVICE_SCRIPT_UPD_ADD_RMV);
		getActivity().registerReceiver(updateReceiver, intentFilter);
	}

	public void onPause(){
		super.onPause();
		getActivity().unregisterReceiver(updateReceiver);
	}

//	public boolean sendMessageToServer(D3Request d3Request, boolean command)
//	{
//		Activity activity = (DevicesListActivity)context;
//		if(activity != null){
//			DevicesListActivity devicesActivity = (DevicesListActivity) activity;
//			return  devicesActivity.sendMessageToServer(d3Request, command);
//		}
//		return false;
//	}
}
