package biz.teko.td.SHUB_APP.D3.D3XProtoEvent;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;

import java.util.LinkedList;

/**
 * Created by td13017 on 05.08.2016.
 */
public class EClass
{
	@Attribute(name = "id")
	public int id;

	@Attribute(name = "caption")
	public String caption;

	@Attribute(name = "iconSmall", required = false)
	public String iconSmall;

	@Attribute(name = "iconBig", required = false)
	public String iconBig;

	@Attribute(name="description")
	public String description;

	@Attribute(name = "affect", required = false)
	public String affect;

	@ElementList(name = "detectors", entry = "detector", required = false)
	public LinkedList<EDetector> detectors;

	@ElementList(name = "reasons", entry = "reason", required = false)
	public LinkedList<EReason> reasons;

	@ElementList(name = "statements", entry = "statement", required = false)
	public LinkedList<EStatement> statements;

	public EDetector getDetectorById(int id){
		LinkedList<EDetector> detectors = this.detectors;
		for(EDetector detector:detectors){
			if(detector.id == id){
				return detector;
			}
		}
		return null;
	}

	public EReason getReasonById(int id){
		LinkedList<EReason> reasons = this.reasons;
		if(reasons!=null)
		for(EReason reason:reasons){
			if(reason.id == id){
				return reason;
			}
		}
		return null;
	}

	public EStatement getStatementById(int id){
		LinkedList<EStatement> eStatements = this.statements;
		for(EStatement statement:eStatements){
			if(statement.id == id){
				return statement;
			}
		}
		return null;
	}

	public EClass(){}

	public EClass(int id, String caption){
		this.id = id;
		this.caption  = caption;
	}
}
