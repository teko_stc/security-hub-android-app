package biz.teko.td.SHUB_APP.Profile.LocalConfig.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.NViews.NMenuListElement;

public class DeviceLocalConfigListAdapter extends RecyclerView.Adapter<DeviceLocalConfigListAdapter.DeviceLocalConfigViewHolder> {

    private ArrayList<Device> mList = new ArrayList<>();
    private OnDeviceClickListener onDeviceClickListener;


    public void setDevices(ArrayList<Device> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    public void setOnDeviceClickListener(OnDeviceClickListener onDeviceClickListener) {
        this.onDeviceClickListener = onDeviceClickListener;
    }

    @NonNull
    @Override
    public DeviceLocalConfigViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new DeviceLocalConfigViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.n_device_config_list_element, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceLocalConfigViewHolder deviceLocalConfigViewHolder, int i) {
//        deviceForApnViewHolder.setData(mList.get(i).getName(), mList.get(i).account);
        /*07.04.2021 из config'a  в данный момент нет возможности вытащить тип устройства, но лкоально конфигурироваться может только sh*/
        deviceLocalConfigViewHolder.setData("Security Hub", mList.get(i).account,  mList.get(i).getDeviceVersion());
    }

    @Override
    public int getItemCount() { return mList.size(); }

    class DeviceLocalConfigViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final NMenuListElement element;

        public DeviceLocalConfigViewHolder(View view){
            super(view);
            element = view.findViewById(R.id.nDeviceConfigElement);
            element.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onDeviceClickListener != null) onDeviceClickListener.onDeviceClick(mList.get(getAdapterPosition()));
        }

        public void setData(String name, int serial, double version) {
            element.setTitle(name + " (" + version + ")");
            element.setSubtitle("S/N " + serial);
        }
    }

//    public static class DeviceForApn {
//        private int serial;
//        private int config;
//        private String ip;
//
//        public DeviceForApn(int serial, int config, String ip) {
//            this.serial = serial;
//            this.config = config;
//            this.ip = ip;
//        }
//
//        public String getName() {
////            return Func.getDeviceModel(1);
//            return App.getContext().getResources().getStringArray(R.array.device_types)[1];
//        }
//
//        public int getSerial() {
//            return serial;
//        }
//
//        public int getConfig() {
//            return config;
//        }
//
//        public String getIp() {
//            return ip;
//        }
//    }

    public interface OnDeviceClickListener {
        void onDeviceClick(Device device);
    }
}
