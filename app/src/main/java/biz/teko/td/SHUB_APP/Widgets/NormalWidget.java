package biz.teko.td.SHUB_APP.Widgets;

import android.app.PendingIntent;
import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.RemoteViews;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Event;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 15.05.2017.
 */

public class NormalWidget extends AppWidgetProvider
{
	public static final String BROADCAST_WIDGET_SITE_CONTROL = "Get command from widget";
	public static final int SENSOR_SENSITIVITY = 4;
	private DBHelper dbHelper;
	private static boolean onPocket = false;

	@Override
	public void onEnabled(Context context)
	{
		super.onEnabled(context);
		dbHelper = DBHelper.getInstance(context);
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds)
	{
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		dbHelper = DBHelper.getInstance(context);
		SharedPreferences sp = context.getSharedPreferences(WidgetConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
		for (int id : appWidgetIds)
		{
			updateWidget(context, appWidgetManager, sp, id);
		}
	}

	public static void updateWidget(Context context, AppWidgetManager appWidgetManager, SharedPreferences sp, int id)
	{
		DBHelper dbHelper = DBHelper.getInstance(context);
		int site_id = sp.getInt(WidgetConfigActivity.WIDGET_SITE_ID + id, -1);
		String color = sp.getString(WidgetConfigActivity.WIDGET_COLOR + id, "#FFFFFFFF");
		if(site_id!=-1)
		{
			RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_normal_layout);
			Site site = dbHelper.getSiteById(site_id);
			if(null != site)
			{
				int colorI = (int) Long.parseLong(color.substring(1), 16);
				remoteViews.setTextViewText(R.id.widget_site_name, site.name);
				remoteViews.setInt(R.id.widgetPattern, "setBackgroundColor", colorI);

				String[] armStatements = context.getResources().getStringArray(R.array.arm_statement_titles);
				int armStatus = dbHelper.getSiteArmStatus(site.id);
				remoteViews.setTextViewText(R.id.widget_site_arm, armStatements[armStatus]);
				switch (armStatus){
					case 0:
						remoteViews.setImageViewResource(R.id.widget_statement_icon, R.drawable.ic_restriction_shield_grey_500_big);
						break;
					case 1:
						remoteViews.setImageViewResource(R.id.widget_statement_icon, R.drawable.ic_security_checked_green_500_big_48);
						break;
					case 2:
						remoteViews.setImageViewResource(R.id.widget_statement_icon, R.drawable.ic_partly_armed_big);
						break;
					default:
						break;
				}

				String stText = context.getString(R.string.STATEMENT_OK);
				remoteViews.setTextViewText(R.id.widget_site_st, stText);


				LinkedList<Event> affects = dbHelper.getAffectsBySiteId(site_id);
				if (null != affects)
				{
					int maxClassId = 4;
					Event affectPriorityMax = null;
					for (Event affect : affects)
					{
						if (affect.classId < 5)
						{
							if ((maxClassId + 1) > affect.classId)
							{
								maxClassId = affect.classId;
								affectPriorityMax = affect;
							}
						}
					}

					int alarmI = 0;
					int sabI = 0;
					int warnI = 0;
					int malfI = 0;

					int stCount = 0;

					for (Event affect : affects)
					{
						switch (affect.classId)
						{
							case 0:
								remoteViews.setImageViewResource(R.id.widget_statement_icon, context.getResources().getIdentifier(affect.explainAffectIconBig(), null, context.getPackageName()));
								if (alarmI == 0)
								{
									alarmI++;
									if (stCount == 0)
									{
										stCount++;
										stText = context.getString(R.string.ALARM_BIG);
									}
								}
								break;
							case 1:
								if (affect.classId == affectPriorityMax.classId)
								{
									remoteViews.setImageViewResource(R.id.widget_statement_icon, context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
									if (0 == sabI)
									{
										sabI++;
										if (stCount == 0)
										{
											stCount++;
											stText = context.getString(R.string.SAB_BIG);
										} else
										{
											stText += context.getString(R.string.SAB_SMALL);
										}
									}
								} else
								{
									//										imageSab.setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
									if (0 == sabI)
									{
										sabI++;
										if (stCount == 0)
										{
											stCount++;
											stText = context.getString(R.string.SAB_BIG);
										} else
										{
											stText += context.getString(R.string.SAB_SMALL);
										}
									}
								}
								break;
							case 2:
								if (affect.classId == affectPriorityMax.classId)
								{
									remoteViews.setImageViewResource(R.id.widget_statement_icon, context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
									if (0 == malfI)
									{
										malfI++;
										if (stCount == 0)
										{
											stCount++;
											stText = context.getString(R.string.MALF_BIG);
										} else
										{
											stText += context.getString(R.string.MALF_SMALL);
										}
									}
								} else
								{
									//										imageMalf.setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
									if (0 == malfI)
									{
										malfI++;
										if (stCount == 0)
										{
											stCount++;
											stText = context.getString(R.string.MALF_BIG);
										} else
										{
											stText += context.getString(R.string.MALF_SMALL);
										}
									}
								}
								break;
							case 3:
								if (affect.classId == affectPriorityMax.classId)
								{
									remoteViews.setImageViewResource(R.id.widget_statement_icon, context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
									if (0 == warnI)
									{
										warnI++;
										if (stCount == 0)
										{
											stCount++;
											stText = context.getString(R.string.WARN_BIG);
										} else
										{
											stText += context.getString(R.string.WARN_SMALL);
										}
									}
								} else
								{
									//										imageAtt.setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
									if (0 == warnI)
									{
										remoteViews.setImageViewResource(R.id.widget_statement_icon, context.getResources().getIdentifier(affect.explainAffectIconSmall(), null, context.getPackageName()));
										warnI++;
										if (stCount == 0)
										{
											stCount++;
											stText = context.getString(R.string.WARN_BIG);
										} else
										{
											stText += context.getString(R.string.WARN_SMALL);
										}
									}
								}
								break;
							default:
								break;
						}
					}

					remoteViews.setTextViewText(R.id.widget_site_st, stText);
				}

//				remoteViews.setOnClickPendingIntent(R.id.button_widget_arm, setPendingIntent(context, site.id, "ARM"));
//				remoteViews.setOnClickPendingIntent(R.id.button_widget_disarm, setPendingIntent(context, site.id, "DISARM"));
				appWidgetManager.updateAppWidget(id, remoteViews);

//				SensorManager sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
//				Sensor proximity = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
//				SensorEventListener sensorEventListener = new SensorEventListener()
//				{
//					@Override
//					public void onSensorChanged(SensorEvent event)
//					{
//						if (event.sensor.getType() == Sensor.TYPE_PROXIMITY)
//						{
//							if (event.values[0] >= -SENSOR_SENSITIVITY && event.values[0] <= SENSOR_SENSITIVITY)
//							{
//								//near
//								onPocket = true;
//							} else
//							{
//								//far
//								onPocket = false;
//							}
//						}
//					}
//
//					@Override
//					public void onAccuracyChanged(Sensor sensor, int accuracy)
//					{
//
//					}
//				};
//				sensorManager.registerListener(sensorEventListener, proximity, SensorManager.SENSOR_DELAY_NORMAL);


			}else{
				remoteViews.setViewVisibility(R.id.widgetErrorText, View.VISIBLE);
				appWidgetManager.updateAppWidget(id, remoteViews);
				AppWidgetHost host = new AppWidgetHost(context, 0);
				host.deleteAppWidgetId(id);
			}
		}
	}

	private static PendingIntent setPendingIntent(Context context, int site_id, String action) {
		Intent intent = new Intent(context, NormalWidget.class);
		intent.putExtra("site_id", site_id);
		intent.setAction(action);

		return PendingIntent.getBroadcast(context, site_id, intent, Func.getPendingFlags(PendingIntent.FLAG_UPDATE_CURRENT));
	}

	@Override
	public void onReceive(Context context, Intent intent)
	{

		if(!onPocket)
		{
			int site_id = intent.getIntExtra("site_id", -1);
			if (site_id != -1)
			{
				switch (intent.getAction())
				{
					case "ARM":
						Intent intent1 = new Intent(BROADCAST_WIDGET_SITE_CONTROL);
						intent1.putExtra("site_id", site_id);
						intent1.putExtra("arm", 1);
						context.sendBroadcast(intent1);
						break;
					case "DISARM":
						Intent intent2 = new Intent(BROADCAST_WIDGET_SITE_CONTROL);
						intent2.putExtra("site_id", site_id);
						intent2.putExtra("arm", 0);
						context.sendBroadcast(intent2);
						break;
				}
			}
		}else{
			super.onReceive(context, intent);
		}
	}



	@Override
	public void onDeleted(Context context, int[] appWidgetIds)
	{
		super.onDeleted(context, appWidgetIds);
		SharedPreferences sp = context.getSharedPreferences(WidgetConfigActivity.WIDGET_PREF, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sp.edit();
		for(int widgetID:appWidgetIds){
			editor.remove(WidgetConfigActivity.WIDGET_SITE_ID + widgetID);
		}
		editor.commit();
	}

	@Override
	public void onDisabled(Context context)
	{
		super.onDisabled(context);
	}
}
