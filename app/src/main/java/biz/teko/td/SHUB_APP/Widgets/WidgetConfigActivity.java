package biz.teko.td.SHUB_APP.Widgets;


import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import biz.teko.td.SHUB_APP.Activity.StartUpActivity;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Widgets.Fragments.WidgetActivityFragmentNoSites;
import biz.teko.td.SHUB_APP.Widgets.Fragments.WidgetActivityFragmentSites;

/**
 * Created by td13017 on 23.05.2017.
 */

public class WidgetConfigActivity extends Activity
{
	int widgetID = AppWidgetManager.INVALID_APPWIDGET_ID;
	Intent resulValue;
	DBHelper dbHelper;
	private int site_id = -1;
	public static String WIDGET_PREF = "widget_pref";
	public static String WIDGET_SITE_ID = "widget_id";
	public static String WIDGET_COLOR = "widget_color";

	private String color = "#FFFFFFFF";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		dbHelper  = DBHelper.getInstance(this);

		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if(null!=extras){
			widgetID = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		}
		if(widgetID == AppWidgetManager.INVALID_APPWIDGET_ID){
			finish();
		}

		resulValue = new Intent();
		resulValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetID);

		setResult(RESULT_CANCELED, resulValue);

		setContentView(R.layout.activity_widget_config);

		final Button buttonAddWidget = (Button) findViewById(R.id.button_widget_add);

		if((dbHelper.getUserInfo() == null)||(dbHelper.getSitesCount() == 0)){
			getFragmentManager().beginTransaction().replace(R.id.widget_sites_list_frame, new WidgetActivityFragmentNoSites(), null).commit();
			buttonAddWidget.setText("Войти");
			buttonAddWidget.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Intent intent1 = new Intent(getBaseContext(), StartUpActivity.class);
					intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					getApplicationContext().startActivity(intent1);
					finish();
				}
			});
		}else{
			getFragmentManager().beginTransaction().replace(R.id.widget_sites_list_frame, new WidgetActivityFragmentSites(), null).commit();
			buttonAddWidget.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					if(site_id != -1){
						SharedPreferences sp = getSharedPreferences(WIDGET_PREF, MODE_PRIVATE);
						SharedPreferences.Editor editor = sp.edit();
						editor.putInt(WIDGET_SITE_ID + widgetID, site_id);
						editor.putString(WIDGET_COLOR + widgetID, color);
						editor.commit();
						AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(v.getContext());
						NormalWidget.updateWidget(v.getContext(), appWidgetManager, sp, widgetID);
						setResult(RESULT_OK, resulValue);
						finish();
					}
				}
			});
		}
	}

	public void setSiteIdFromFragment(int id){
		site_id = id;
	}

	public void setColorFromFragment(String colorFromFragment){
		color = colorFromFragment;
	}


}
