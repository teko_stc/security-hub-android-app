package biz.teko.td.SHUB_APP.Utils.Other;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class NEditText extends EditText
{
	private Handler handler = new Handler();

	public NEditText(Context context)
	{
		super(context);
	}

	public NEditText(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

//	@Override
//	public boolean requestFocus(int direction, Rect previouslyFocusedRect)
//	{
//		return super.requestFocus(direction, previouslyFocusedRect);
//	}

	public void showKeyboard() {
		EditText editText = this;
		editText.setSelection(editText.getText().length());
		handler.postDelayed(() -> {
			editText.requestFocus();
			InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT,0);
		},100);
	}

	public void setFocus() {
		EditText editText = this;
		editText.setSelection(editText.getText().length());
		handler.postDelayed(() -> editText.requestFocus(),100);
	}




//	@Override
//	public void setOnFocusChangeListener(OnFocusChangeListener l)
//	{
//		super.setOnFocusChangeListener(l);
//		if(isFocused()){
//			showKeyboard();
//		}else{
//			hideKeyBoeard();
//		}
//	}
////
	public void hideKeyBoard()
	{
		EditText editText = this;
		editText.setSelection(editText.getText().length());
		handler.postDelayed(() -> {
			InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
//			imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		},100);
	}
}
