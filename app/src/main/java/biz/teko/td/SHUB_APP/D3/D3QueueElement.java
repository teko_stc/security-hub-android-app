package biz.teko.td.SHUB_APP.D3;
/**
 * Created by td13017 on 07.06.2016.
 */
public class D3QueueElement
{
	public REQ_TYPE req_type;
	public String data;

	public enum REQ_TYPE{
		JSON,
		WEB
	}

	public D3QueueElement(REQ_TYPE req_type, String data) {
		super();
		this.req_type = req_type;
		this.data = data;
	}
}
