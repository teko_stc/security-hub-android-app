package biz.teko.td.SHUB_APP.D3.D3XProto;

import android.content.Context;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.InputStream;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.R;

/**
* Created by td13017 on 23.06.2016.
*/

@Root(name = "xml")
public class D3XProtoConst
{
	@ElementList(name = "classes", entry="class")
	public LinkedList<EventClass> eventClasses;

	@ElementList(name = "detectors", entry="detector")
	public LinkedList<Detector> detectors;

	@ElementList(name = "statements", entry="statement")
	public LinkedList<Statement> statements;

	public D3XProtoConst() {}

	public static D3XProtoConst load(Context context){
		InputStream inputStream = context.getResources().openRawResource(R.raw.x_proto_const_x);
		Serializer serializer = new Persister();
		try
		{
			return serializer.read(D3XProtoConst.class, inputStream);
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public EventClass getClassById(int id){
		LinkedList<EventClass> classes  =this.eventClasses;
		for(EventClass eventClass:eventClasses){
			if(id == eventClass.id){
				return eventClass;
			}
		}
		return  null;
	}

	public Detector getDetectorById(int id){
		LinkedList<Detector> detectors  =this.detectors;
		for(Detector detector:detectors){
			if(id == detector.id){
				return detector;
			}
		}
		return  null;
	}

	public Statement getStatementById(int id){
		LinkedList<Statement> statements  =this.statements;
		for(Statement statement:statements){
			if(id == statement.id){
				return statement;
			}
		}
		return  null;
	}

//	public String translateToEvent(Event event, Context context){
//		try {
//			D3XProtoConst d3XProtoConst = new D3XProtoConst(context);
//			String eventR = d3XProtoConst.eventClasses.get(event.classId).caption+d3XProtoConst.detectors.get(event.detectorId).caption+d3XProtoConst.statements.get(event.active).caption;
//			return eventR;
//		}catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//		return null;
//	}


}


