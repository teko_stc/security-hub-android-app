package biz.teko.td.SHUB_APP.Wizards.Activity.DomainUsers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Wizards.Adapter.Pager.DomainUserSetViewPagerAdapter;

public class WizardDomainUserSetActivity extends BaseActivity
{
	public static final int DOMAIN_USER_SET_PAGES_COUNT = 5;

	private DomainUserSetViewPagerAdapter adapter;
	private ViewPager pager;
	private String userName;
	private String userLogin;
	private int role;
	private String roleTitle;
	private String pass;
	private ServiceConnection serviceConnection;
	private D3Service myService;
	private boolean bound;
	private int sending = 0;
	private DBHelper dbHelper;

	private BroadcastReceiver operatorChangedReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			if(getCurrentItem() == DOMAIN_USER_SET_PAGES_COUNT - 2)
			{
				int result = intent.getIntExtra("result", 0);
				if (result > 0)
				{
//					Func.pushToast(context, getString(R.string.PUA_SUCCESS_CHANGED), (Activity) context);
					goNext();
				} else
				{
					Func.pushToast(context, Func.handleResult(context, result), (Activity) context);
				}
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wizard_domainuser_set);
		dbHelper = DBHelper.getInstance(this);

		adapter = new DomainUserSetViewPagerAdapter(getSupportFragmentManager());
		pager = (ViewPager) findViewById(R.id.domainUserSetViewPager);
		pager.setOnTouchListener(new View.OnTouchListener()
		{
			@Override
			public boolean onTouch(View v, MotionEvent event)
			{
				return true;
			}
		});
		pager.setAdapter(adapter);

		serviceConnection = new ServiceConnection() {
			public void onServiceConnected(ComponentName name, IBinder binder) {
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = (D3Service) myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name) {
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};


	}

	@Override
	protected void onResume()
	{
		super.onResume();
		Intent intent = new Intent(this, D3Service.class);
		bindService(intent, serviceConnection, 0);
		registerReceiver(operatorChangedReceiver, new IntentFilter(D3Service.BROADCAST_OPERATOR_SET));
	}

	@Override
	protected void onPause()
	{
		super.onPause();
		unbindService(serviceConnection);
		unregisterReceiver(operatorChangedReceiver);
	}

	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}

	@Override
	public void onBackPressed()
	{
		goPrevious();
	}

	public void refreshViews(){
		adapter.notifyDataSetChanged();
	}

	public void goPrevious(){
		int position = getCurrentItem();
		if(position != 0){
			pager.setCurrentItem(position - 1);
			refreshViews();
		}else{
			super.onBackPressed();
		}
	}

	public void goNext()
	{
		int position = getCurrentItem();
		if(position != DOMAIN_USER_SET_PAGES_COUNT - 1){
			pager.setCurrentItem(position + 1);
			refreshViews();
		}else{
			/*FINISH*/
			goBack();
		}
	}

	public void sendUserRegRequest()
	{
		UserInfo userInfo = dbHelper.getUserInfo();
		JSONObject message = new JSONObject();
		try
		{
			message.put("id", 0);
			message.put("name", userName);
			message.put("login", userLogin);
			message.put("contacts", userName);
			message.put("password", pass);
			message.put("active", true);
			message.put("roles", role);
			message.put("domain", 0);
			message.put("org", 0);
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
		D3Request d3Request = D3Request.createMessage(userInfo.roles, "OPERATOR_SET", message);
		if(d3Request.error == null){
			sendMessageToServer(d3Request);
			Func.hideKeyboard((Activity) this);
		}else{
			Func.pushToast(this, d3Request.error, (Activity) this);
		}
	}

	public int getCurrentItem(){
		return pager.getCurrentItem();
	}

	public void setUserName(String name)
	{
		this.userName = name;
	}

	public String getUserName()
	{
		return this.userName;
	}

	public void setUserLogin(String login)
	{
		this.userLogin = login;
	}

	public String getUserLogin()
	{
		return userLogin;
	}

	public void setUserRole(int role){
		this.role = role;
	}

	public int getUserRole(){
		return  this.role;
	}

	public void setUserPass(String pass)
	{
		this.pass = pass;
	}

	public String getUserPass(){
		return this.pass;
	}

	private boolean sendMessageToServer(D3Request d3Request)
	{
		if(sending == 0)
		{
			if (null != myService)
			{
				if (myService.send(d3Request.toString()))
				{
					return true;
				}
			} else
			{
				Func.nShowMessage(this, this.getString(R.string.EA_D3_OFF_1) + " " + this.getString(R.string.application_name) + " " + this.getString(R.string.EA_D3_OFF_2));
			}
		}else{
			Func.nShowMessage(this, getString(R.string.EA_IMPOSSIBLE_SEND_ANOTHER_DATA));
		}
		return false;
	};

	private void goBack()
	{
		Intent intent = new Intent();
		setResult(RESULT_OK, intent);
		finish();
	}

	public String getRoleTitle()
	{
		return this.roleTitle;
	}

	public void setRoleTitle(String roleTitle)
	{
		this.roleTitle = roleTitle;
	}
}
