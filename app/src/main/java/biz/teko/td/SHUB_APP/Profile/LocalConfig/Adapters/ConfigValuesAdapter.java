package biz.teko.td.SHUB_APP.Profile.LocalConfig.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import biz.teko.td.SHUB_APP.Profile.LocalConfig.Interfaces.IValueOperation;
import biz.teko.td.SHUB_APP.R;

public class ConfigValuesAdapter extends RecyclerView.Adapter<ConfigValuesAdapter.ConfigValuesViewHolder> {
    private final List<String> values;
    private int currPosition;
    private OnItemClickListener itemClickListener;
    private IValueOperation valueOperation;

    public void setValueOperation(IValueOperation valueOperation) {
        this.valueOperation = valueOperation;
    }

    public interface OnItemClickListener {
        void itemClick(int position);
    }

    public ConfigValuesAdapter(List<String> values, int currPosition) {
        this.values = values;
        this.currPosition = currPosition;
    }

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public ConfigValuesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ConfigValuesViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.n_list_preference_item, parent, false),
                itemClickListener,
                valueOperation);
    }

    @Override
    public void onBindViewHolder(@NonNull ConfigValuesViewHolder holder, int position) {
        holder.bind(values.get(position), position);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public class ConfigValuesViewHolder extends RecyclerView.ViewHolder {
        private RadioButton radioButton;
        private TextView textTitle;
        private final OnItemClickListener itemClickListener;
        private final IValueOperation valueOperation;

        ConfigValuesViewHolder(@NonNull View itemView, OnItemClickListener itemClickListener, IValueOperation valueOperation) {
            super(itemView);
            initView(itemView);
            this.itemClickListener = itemClickListener;
            this.valueOperation = valueOperation;
            setClickListeners(itemView);
        }

        private void setClickListeners(View view) {
            view.setOnClickListener(v -> itemClick());
            radioButton.setOnClickListener(v -> itemClick());
        }

        private void itemClick() {
            int position = getAdapterPosition();
            if (position != RecyclerView.NO_POSITION) {
                currPosition = position;
                notifyDataSetChanged();
                itemClickListener.itemClick(position);
            }
        }

        public void bind(String title, int position) {
            textTitle.setText(valueOperation.getTextDialogValue(title));
            radioButton.setChecked(currPosition == position);
        }

        private void initView(View itemView) {
            radioButton = itemView.findViewById(R.id.radioButtonListPref);
            textTitle = itemView.findViewById(R.id.entryListPref);
        }
    }
}
