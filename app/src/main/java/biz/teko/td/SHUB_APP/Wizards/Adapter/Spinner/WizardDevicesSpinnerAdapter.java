package biz.teko.td.SHUB_APP.Wizards.Adapter.Spinner;

import android.content.Context;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

public class WizardDevicesSpinnerAdapter extends ArrayAdapter<Device>
{
	private final Context context;
	private final Device[] devices;
	private final DBHelper dbHelper;

	public WizardDevicesSpinnerAdapter(Context context, int resource, Device[] devices)
	{
		super(context, resource, devices);
		this.context = context;
		this.devices = devices;
		this.dbHelper = DBHelper.getInstance(context);
	}

	public int getCount(){
		return devices.length;
	}

	public Device getItem(int position){
		return devices[position];
	}

	public long getItemId(int position){
		return position;
	}

	@Override
	public int getPosition(@Nullable Device item)
	{
		return super.getPosition(item);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		String deviceModel = devices[position].getName();
		label.setText(deviceModel + "(" + devices[position].account + ")");
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent)
	{
		TextView label = new TextView(context);
		label.setTextColor(context.getResources().getColor(R.color.brandButtonDarkGreyTextColor));
		String deviceModel = devices[position].getName();
		label.setText(deviceModel + "(" + devices[position].account + ")");
		label.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
		label.setPadding(Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context), Func.dpToPx(6, context));
		return label;	}
}
