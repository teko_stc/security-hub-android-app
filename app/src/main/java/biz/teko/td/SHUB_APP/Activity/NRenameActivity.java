package biz.teko.td.SHUB_APP.Activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;

import biz.teko.td.SHUB_APP.Cameras.Camera;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.Command;
import biz.teko.td.SHUB_APP.D3DB.D3Element;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.Device;
import biz.teko.td.SHUB_APP.D3DB.Operator;
import biz.teko.td.SHUB_APP.D3DB.Section;
import biz.teko.td.SHUB_APP.D3DB.Site;
import biz.teko.td.SHUB_APP.D3DB.User;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.D3DB.Zone;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;
import biz.teko.td.SHUB_APP.Utils.NViews.NActionButton;
import biz.teko.td.SHUB_APP.Utils.NViews.NDialog;
import biz.teko.td.SHUB_APP.Utils.Other.NEditText;

public class NRenameActivity extends BaseActivity
{
	private Site site;
	private User local_user;
	private Device device;
	private Section section;
	private Zone zone;
	private Operator operator;
	private Camera camera;

	private LinearLayout buttonClose;
	private LinearLayout buttonNext;
	private boolean transition;
	private NEditText editName;
	private TextView editNameTitle;
	private String name;
	private Context context;
	private DBHelper dbHelper;
	private UserInfo userInfo;
	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;
	private int sending = 0;
	private int type;

	private int site_id;
	private int device_id;
	private int section_id;
	private int zone_id;
	private int user_id;
	private int local_user_id;
	private int camera_rtsp_id;
	private String camera_iv_id;
	private String camera_dahua_id;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.n_activity_rename);

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();

		type = getIntent().getIntExtra("type", -1);
		site_id = getIntent().getIntExtra("site", -1);
		device_id = getIntent().getIntExtra("device", -1);
		section_id = getIntent().getIntExtra("section", -1);
		zone_id = getIntent().getIntExtra("zone", -1);
		user_id = getIntent().getIntExtra("user", -1);
		local_user_id = getIntent().getIntExtra("local_user", -1);
		camera_rtsp_id = getIntent().getIntExtra("camera_rtsp", -1);
		camera_iv_id = getIntent().getStringExtra("camera_iv");
		camera_dahua_id = getIntent().getStringExtra("camera_dahua");

		name = getIntent().getStringExtra("name");
		transition = getIntent().getBooleanExtra("transition", false);

		if(-1!=site_id)site = dbHelper.getSiteById(site_id);
		if(-1!=device_id) device = dbHelper.getDeviceById(device_id);
		if(-1!=section_id) section = dbHelper.getDeviceSectionById(device_id, section_id);
		if(-1!=zone_id) zone = dbHelper.getZoneById(device_id, section_id, zone_id);
		if(-1!=user_id) operator = dbHelper.getOperatorById(user_id);
		if(-1!=camera_rtsp_id) camera = dbHelper.getRTSPCameraById(camera_rtsp_id);
		if(null!=camera_iv_id) camera = dbHelper.getIVCameraById(camera_iv_id);
		if(null!=camera_dahua_id) camera = dbHelper.getDahuaCameraBySn(camera_dahua_id);
		if (local_user_id != -1 && device_id != -1)	local_user = dbHelper.getDeviceUserById(local_user_id, device_id);

		addTransitionListener(transition);

		setup();
		bind();

	}

	private void bind()
	{
		if(null!=buttonClose) buttonClose.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				onBackPressed();
			}
		});

		if(null!=buttonNext) buttonNext.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View view)
			{
				setName();
			}
		});

		if(null!=editName) {
			editName.setFocusableInTouchMode(true);
			if(null!=name) {
				editName.setText(name);
				editName.setSelection(editName.getText().length());
				try
				{
					Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
					f.setAccessible(true);
					f.set(editName, R.drawable.n_cursor);
				} catch (Exception ignored)
				{
				}
				editName.setSelection(editName.getText().length());
			}
			editName.setOnEditorActionListener(new TextView.OnEditorActionListener()
			{
				@Override
				public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent)
				{
					if(i == EditorInfo.IME_ACTION_DONE){
						setName();
					}
					return false;
				}
			});
			editName.showKeyboard();
		}
		if(null!=editNameTitle) {
			switch (type){
				case Const.SITE:
					editNameTitle.setText(R.string.N_TITLE_SITE_NAME);
					break;
				case Const.DEVICE:
					break;
				case Const.SECTION:
					editNameTitle.setText(R.string.N_TITLE_SECTION_NAME);
					break;
				case Const.ZONE:
					editNameTitle.setText(R.string.N_TITLE_ZONE_NAME);
					break;
				case Const.USER:
					editNameTitle.setText(R.string.N_TITLE_USER_NAME);
					break;
				case Const.LOGIN:
					editNameTitle.setText(R.string.N_TITLE_LOGIN);
					break;
				case Const.CAMERA_RTSP:
				case Const.CAMERA_IV:
				case Const.CAMERA_DAHUA:
					editNameTitle.setText(R.string.N_TITLE_CAMERA_NAME);
					break;
				case Const.LOCAL_USER:
					editNameTitle.setText(R.string.N_LOCAL_USER_NAME);
					break;
				default:
					break;
			}
		}
	}

	private void setName()
	{
		if(validate()){

			switch (type){
				case Const.SITE:
					renameSite();
					break;
				case Const.DEVICE:
					break;
				case Const.SECTION:
					renameSection();
					break;
				case Const.ZONE:
					renameZone();
					break;
				case Const.USER:
					renameUser();
					break;
				case Const.LOGIN:
					changeLogin();
					break;
				case Const.CAMERA_RTSP:
					renameRTSPCamera();
					break;
				case Const.CAMERA_IV:
					renameIVCamera();
					break;
				case Const.CAMERA_DAHUA:
					renameDahuaCamera();
					break;
				case Const.LOCAL_USER:
					renameLocalUser();
					break;
				default:
					break;
			}

		}else{
			NDialog nDialog = new NDialog(context, R.layout.n_dialog_question_no);
			nDialog.setTitle(getString(R.string.N_RENAME_MESSAGE_CHECK_ENTERED_DATA));
			nDialog.setOnActionClickListener(new NDialog.OnActionClickListener()
			{
				@Override
				public void onActionClick(int value, boolean b)
				{
					switch (value){
						case NActionButton.VALUE_CANCEL:
							nDialog.dismiss();
							break;
					}
				}
			});
			nDialog.show();
		}
	}

	private void renameDahuaCamera()
	{
		if(dbHelper.renameDahuaCamera(camera_dahua_id, editName.getText().toString().trim())){
			onBackPressed();
		}
	}

	private void renameIVCamera()
	{
		WebHelper.getInstance().renameCamera(camera.id, editName.getText().toString(), dbHelper.getUserInfoIVToken());
		onBackPressed();
	}

	private void renameLocalUser() {
		JSONObject commandAddr = new JSONObject();
		try
		{
			commandAddr.put("index", local_user.id);
			commandAddr.put("name", editName.getText().toString());
			if(nSendMessageToServer(device, Command.USER_SET, commandAddr, true)){
				onBackPressed();
			};
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void changeLogin()
	{
		JSONObject message = new JSONObject();
		try
		{
			message.put("id", operator.id);
			message.put("login", editName.getText().toString().trim());
			if(nSendMessageToServer(null, D3Request.OPERATOR_SET, message, false)){
				onBackPressed();
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void renameRTSPCamera()
	{
		if(dbHelper.renameRTSPCamera(camera_rtsp_id, editName.getText().toString().trim())){
			onBackPressed();
		}
	}

	private void renameUser()
	{
		JSONObject message = new JSONObject();
		try
		{
			message.put("id", operator.id);
			message.put("name", editName.getText().toString().trim());
			if(nSendMessageToServer(null, D3Request.OPERATOR_SET, message, false)){
				onBackPressed();
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void renameSection()
	{
		String newName = editName.getText().toString();
		JSONObject commandAddr = new JSONObject();
		try
		{
			commandAddr.put("index", section.id);
			commandAddr.put("name", newName);
			if(nSendMessageToServer(section, Command.SECTION_SET, commandAddr, true)){
				onBackPressed();
			}
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void renameSite()
	{
		String newName = editName.getText().toString();
		//DO CHANGE NAME
		JSONObject message = new JSONObject();
		try
		{
			message.put("id", site.id);
			message.put("name", newName);
			if(nSendMessageToServer(null, D3Request.SITE_SET, message, false)){
				onBackPressed();
			};
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private void renameZone()
	{
		String newName = editName.getText().toString();
		JSONObject commandAddr = new JSONObject();
		try
		{
			commandAddr.put("section", zone.section_id);
			commandAddr.put("index", zone.id);
			commandAddr.put("name", newName);
			if(nSendMessageToServer(zone, Command.ZONE_SET, commandAddr, true)){
				onBackPressed();
			};
		} catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	private boolean validate()
	{
		if(null!=editName.getText().toString() && !editName.getText().toString().equals("")){
			return true;
		}
		return false;
	}

	private void setup()
	{
		buttonClose = (LinearLayout) findViewById(R.id.nButtonClose);
		buttonNext = (LinearLayout) findViewById(R.id.nButtonNext);
		editNameTitle = (TextView) findViewById(R.id.nTextTitle);
		editName = (NEditText) findViewById(R.id.nText);
	}

	public void  onResume(){
		super.onResume();

		bindD3();
	}

	public void onPause(){
		super.onPause();
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	private boolean nSendMessageToServer(D3Element d3Element, String Q, JSONObject data, boolean command)
	{
		return  Func.nSendMessageToServer(d3Element, Q, data, getLocalService(), context, command, sending);
	};
}
