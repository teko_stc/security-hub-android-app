package biz.teko.td.SHUB_APP.Cameras;

import android.database.Cursor;

import com.company.NetSDK.NET_DEVICEINFO_Ex;

/**
 * Created by td13017 on 15.09.2017.
 */

public class Camera
{
	public String id;
	public int local_id;
	public CType type;
	public String name;

	public int status;

	public int quality = -1;
	public String audio = "both";

	public String link;

	public String sn;
	public String login;
	public String pass;
	public byte[] preview;

	private long loginHandle;
	private NET_DEVICEINFO_Ex deviceInfo;

	public void setLoginHandle(long loginHandle)
	{
		this.loginHandle = loginHandle;
	}

	public void setDeviceInfo(NET_DEVICEINFO_Ex deviceInfo)
	{
		this.deviceInfo = deviceInfo;
	}

	public long getLoginHandle()
	{
		return loginHandle;
	}

	public NET_DEVICEINFO_Ex getDeviceInfo()
	{
		return deviceInfo;
	}


	public enum CType{
		DEFAULT,
		IV,
		RTSP,
		DAHUA
	}

	public Camera() {}


	public Camera(Cursor cursor, CType type)
	{
		switch (type)
		{
			case DAHUA:
				this.name = cursor.getString(1);
				this.sn = cursor.getString(2);
				this.login = cursor.getString(3);
				this.pass = cursor.getString(4);
				this.preview = cursor.getBlob(5);
				this.type = type;
				break;
			default:
				break;
		}
	}

	public Camera(Cursor cursor){
		this.id = cursor.getString(1);
		this.name = cursor.getString(2);
		this.status = cursor.getInt(3);
		this.quality = cursor.getInt(4);
		this.audio = cursor.getString(5);
		this.type= CType.IV;
	}

	public Camera(int id, String name,  String link){
		this.local_id = id;
		this.name = name;
		this.link = link;
		this.type = CType.RTSP;
	}

	public Camera(String sn, String name, String login, String pass){
		this.sn = sn;
		this.name = name;
		this.login = login;
		this.pass = pass;
		this.type = CType.DAHUA;
	}

}
