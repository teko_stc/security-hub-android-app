package biz.teko.td.SHUB_APP.FaqTab.RecycleHolders;

import android.content.Context;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

import java.util.LinkedList;

import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 08.02.2017.
 */

public class QuestionViewHolder extends ChildViewHolder
{
	private TextView questionId;
	private TextView questionAnswer;
	private Context context;
	public LinearLayout questionImage;
	public TextView questionName;

	public QuestionViewHolder(View itemView)
	{
		super(itemView);
		context = itemView.getContext();
		questionName = (TextView) itemView.findViewById(R.id.questionName);
		questionAnswer = (TextView) itemView.findViewById(R.id.questionAnswer);
		questionImage = (LinearLayout) itemView.findViewById(R.id.imageAnswer);
	}

//	public void setAffectImage(Affect affect){
//		questionImage.setImageResource(context.getResources().getIdentifier(affect.decribeAffectIconBig(), null, context.getPackageName()));
//	}

	public void setQuestionName(String questionName)
	{
		this.questionName.setText(questionName);
	}

	public void setQuestionImages(LinkedList<String> srcs)
	{
		questionImage.removeAllViews();
		if(srcs!=null)
		{
			for(String src:srcs)
			{
				ImageView imageView = new ImageView(context);
				imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				imageView.setImageResource(context.getResources().getIdentifier(src, null, context.getPackageName()));
				questionImage.addView(imageView);
			}
		}
	}

	public void setQuestionAnswer(SpannableString questionAnswer)
	{
		this.questionAnswer.setText(questionAnswer);
		this.questionAnswer.setMovementMethod(LinkMovementMethod.getInstance());
	}
}
