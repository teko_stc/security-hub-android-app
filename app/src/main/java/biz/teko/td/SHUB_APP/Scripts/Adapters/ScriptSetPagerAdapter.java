package biz.teko.td.SHUB_APP.Scripts.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import biz.teko.td.SHUB_APP.Scripts.Fragments.ScriptSetFragment;

public class ScriptSetPagerAdapter extends FragmentPagerAdapter
{
	private final int count;
	private final int scriptId;
	private final int deviceId;
	private final int siteId;
	private final int bind;
	private final int scriptLibId;

	public ScriptSetPagerAdapter(FragmentManager fm, int count, int scriptId, int scriptLibId, int deviceId, int siteId, int bind)
	{
		super(fm);
		this.count = count;
		this.scriptId = scriptId;
		this.scriptLibId = scriptLibId;
		this.deviceId = deviceId;
		this.siteId = siteId;
		this.bind = bind;
	}

	@Override
	public Fragment getItem(int position)
	{
		return ScriptSetFragment.getInstance(position, scriptId, scriptLibId, deviceId, siteId, bind);
	}

	@Override
	public int getCount()
	{
		return count;
	}

	@Override
	public void notifyDataSetChanged()
	{
		super.notifyDataSetChanged();
	}

	@Override
	public int getItemPosition(@NonNull Object object)
	{
		return POSITION_NONE;
	}
}
