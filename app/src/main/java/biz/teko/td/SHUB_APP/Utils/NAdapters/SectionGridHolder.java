package biz.teko.td.SHUB_APP.Utils.NAdapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.R;

final class SectionGridHolder extends RecyclerView.ViewHolder {

    public static SectionGridHolder newInstance(ViewGroup parent) {
        return new SectionGridHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.n_sections_grid_element_view, parent, false));
    }

    private SectionGridHolder(@NonNull View itemView) {
        super(itemView);
    }

    public void hide(){
        itemView.findViewById(R.id.nSectionElement).setVisibility(View.GONE);
        itemView.findViewById(R.id.innerFrame).setVisibility(View.GONE);
        itemView.findViewById(R.id.nTextEmpty).setVisibility(View.GONE);
    }

    public  void setEmpty(){
        itemView.findViewById(R.id.nTextEmpty).setVisibility(View.VISIBLE);
    }

    public void reset(){
        itemView.findViewById(R.id.nSectionElement).setVisibility(View.VISIBLE);
    }
}
