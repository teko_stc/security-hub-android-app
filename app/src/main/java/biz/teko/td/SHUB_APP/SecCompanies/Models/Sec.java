package biz.teko.td.SHUB_APP.SecCompanies.Models;

import android.content.Context;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.InputStream;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 14.03.2018.
 */

public class Sec
{
	@ElementList(name="companies", entry="company")
	public LinkedList<SCompany> companies;

	public static Sec load(Context context){
		InputStream inputStream = context.getResources().openRawResource(R.raw.security_companies);
		Serializer serializer = new Persister();
		try
		{
			return serializer.read(Sec.class, inputStream);
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
