package biz.teko.td.SHUB_APP.SitesTabs;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mikepenz.materialdrawer.Drawer;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import biz.teko.td.SHUB_APP.Activity.BaseActivity;
import biz.teko.td.SHUB_APP.Application.App;
import biz.teko.td.SHUB_APP.D3.D3Request;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.SitesTabs.Adapters.AlarmsListAdapter;
import biz.teko.td.SHUB_APP.SitesTabs.Adapters.SitesViewPagerAdapter;
import biz.teko.td.SHUB_APP.SlidingTabsLib.SlidingTabLayout;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 07.02.2017.
 */

public class SitesActivity extends BaseActivity
{
	CharSequence Titles[];
	int Numboftabs =2;
	private SitesViewPagerAdapter adapter;
	private ViewPager pager;
	private SlidingTabLayout tabs;
//	private DrawerBuilder drawerBuilder;
	private Context context;
	private DBHelper dbHelper;
	private Drawer drawerResult;
	private final  static int DEF_POSITION = 2;
	private ServiceConnection serviceConnection;
	private boolean bound;
	private D3Service myService;

	private boolean fabShown = false;
	private FrameLayout frameLayout;
	private ListView alarmList;
	private AlarmsListAdapter alarmListAdapter;
	private WebHelper webHelper;
	private ActivityInfo info;
	private boolean startTimer;
	private int from;
	private UserInfo userInfo;
	private int sending  = 0;
	private SharedPreferences sp;

//	private  BroadcastReceiver newEventsReceiver = new BroadcastReceiver()
//	{
//		@Override
//		public void onReceive(Context context, Intent intent)
//		{
//			if(null!=newEventDialogFragment) newEventDialogFragment.updateList();
//		}
//	};


	private BroadcastReceiver serviceAliveReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context1, Intent intent)
		{
			bindD3();
		}
	};


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sites);
		getSwipeBackLayout().setEnableGesture(false);

		// Creating The Toolbar and setting it as the Toolbar for the activity

		this.context = this;
		this.dbHelper = DBHelper.getInstance(context);
		this.userInfo = dbHelper.getUserInfo();
		this.sp = PreferenceManager.getDefaultSharedPreferences(context);

		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		toolbar.setClickable(true);
		getSupportActionBar().setTitle(R.string.HOME);
		try
		{
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		} catch (NullPointerException e)
		{
			e.printStackTrace();
			Log.e("TCP", "S: Error", e);
		}

		//Navi drawer build

		//drawerResult.getRecyclerView().setPadding(drawerResult.getRecyclerView().getPaddingLeft(), Func.dpToPx(20, context), drawerResult.getRecyclerView().getPaddingRight(), drawerResult.getRecyclerView().getPaddingBottom());

		// Creating The SitesViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
		Titles= new CharSequence[]{getResources().getString(R.string.SUNTITLE_OBJECTS), getResources().getString(R.string.SUBTITLE_EVENTS)};
		adapter =  new SitesViewPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);

		// Assigning ViewPager View and setting the adapter
		pager = (ViewPager) findViewById(R.id.sitesPager);
		pager.setAdapter(adapter);

		// Assiging the Sliding Tab Layout View
		tabs = (SlidingTabLayout) findViewById(R.id.sitesTabs);
		tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

		// Setting Custom ColorX for the Scroll bar indicator of the Tab View
		tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
			@Override
			public int getIndicatorColor(int position) {
				return getResources().getColor(R.color.tabsScrollColor);
			}
		});

		// Setting the ViewPager For the SlidingTabsLayout
		tabs.setViewPager(pager);

		from =getIntent().getIntExtra("FROM", -1);

	}

	private BroadcastReceiver connectionRepairReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int status = intent.getIntExtra("status", -1);
			if(status == 1)
			{
				setStatusConnected();
			}
		}
	};

	public void onDestroy()
	{
		super.onDestroy();
	}

	public void onStop()
	{
		super.onStop();
	}

	public void onStart()
	{
		super.onStart();
	}

	public void  onResume(){
		super.onResume();

//		moved into onresume for tests/ cause open every time change focus
//		checkStates(context, sp);

		registerReceiver(serviceAliveReceiver, new IntentFilter(D3Service.BROADCAST_D3SERVICE_ALIVE));
		registerReceiver(connectionRepairReceiver, new IntentFilter(D3Service.BROADCAST_CONNECTION_STATUS_CHANGE));
//		registerReceiver(newEventsReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));

		PackageManager packageManager = getPackageManager();
		try
		{
			info = packageManager.getActivityInfo(this.getComponentName(), 0);
			((App)this.getApplication()).setPreviousActivity(info.name);
		} catch (PackageManager.NameNotFoundException e)
		{
			e.printStackTrace();
		}
		bindD3();
	}

	public void onPause(){
		super.onPause();
		unregisterReceiver(connectionRepairReceiver);
		unregisterReceiver(serviceAliveReceiver);
//		unregisterReceiver(newEventsReceiver);
		if(bound)unbindService(serviceConnection);
	}

	private void bindD3(){
		Intent intent = new Intent(this, D3Service.class);
		serviceConnection = getServiceConnection();
		bindService(intent, serviceConnection, 0);
	}

	private ServiceConnection getServiceConnection(){
		return new ServiceConnection()
		{
			public void onServiceConnected(ComponentName name, IBinder binder)
			{
				Log.d("D3-", "onServiceConnected");
				D3Service.myBinderClass myBinder = (D3Service.myBinderClass) binder;
				myService = myBinder.getService();
				bound = true;
				setStatusConnected();
			}

			public void onServiceDisconnected(ComponentName name)
			{
				Log.d("D3-", "onServiceDisconnected");
				bound = false;
			}
		};
	}

	private D3Service getLocalService()
	{
		if(bound){
			return myService;
		}else{
			bindD3();
			if(null!=myService){
				return myService;
			}
			Func.showRestartServiceMessage(context, context.getString(R.string.EA_D3_REPEAT_CONNECTION));
		}
		return null;

	}

	public boolean sendMessageToServer(D3Request d3Request, boolean command)
	{
//		return  Func.sendMessageToServer(null, d3Request, getLocalService(), context, command, sending);
		return  false;
	};
//
//	@Override
//	public void onWindowFocusChanged(boolean hasFocus)
//	{
//		if(hasFocus){
//
//			/*SHOW ALARM LIST*/
//			if (from == D3Service.OPEN_FROM_STARTUP_ACTIVITY_AFTER_LOGIN || from == D3Service.OPEN_HISTORY_FROM_STATUS_BAR)
//			{
//				/*Show rating dialog*/
//				AppRater.appLaunched(context);
//				/*Show domain lang dialog*/
////				not ready, commented
////				if(Func.needLangForBuild())checkLocales(sp);
//				/*Show new events dialog*/
////				checkStates(context, sp);
//
//				from = -1;
//			}
//
//
//
////			UPDATE CHECK OPEN WITH NEW UPDATE
////			else{
////				/*Show updated features dialog*/
////				if((!context.getPackageName().equals("public.shub")) || PreferenceManager.getDefaultSharedPreferences(context).getString("prof_language_value", "ru").equals("ru")) {
////					checkUpdates(context, sp);
////				}
////			}
//		}
//	}
//
//	private void checkLocales(SharedPreferences sp)
//	{
//		String packageName = context.getPackageName();
//		switch (packageName){
//			case "public.shub":
//			case "teko.shub":
//			case "dev.shub":
//				String locale = sp.getString("prof_language_value", App.DEFAULT_ISO);
//				if(null == locale){
//					String lang = Locale.getDefault().getLanguage();
//					if(null == lang){
//						lang = Locale.getDefault().getDisplayLanguage();
//					}
//					sp.edit().putString("prof_language_value", lang).apply();
//					locale = lang;
//				if((!((userInfo.roles&Const.Roles.DOMAIN_ADMIN) == 0))&&(Func.getBooleanSPDefFalse(sp, "prof_language_changed"))&&(!locale.equals(userInfo.locale))){
//					sp.edit().putBoolean("prof_language_changed", false).apply();
//					Func.showLocalesDifferenceDialog(context);
//				}
//				break;
//			default:
//				break;
//		}
//	}
//
//	public void checkUpdates(Context context, SharedPreferences sp){
//		long updateTime = Func.isInstallFromUpdate(context);
//		if (0 != updateTime)
//		{
//			if (updateTime != sp.getLong(Func.Prefs.PREF_APP_UPDATE_TIME, 0)){
//				if (updateTime > sp.getLong(Func.Prefs.PREF_APP_UPDATE_TIME, 0) + 48*3600*1000)
//				{
//					showUpdatesList(context);
//				}
//				sp.edit().putLong(Func.Prefs.PREF_APP_UPDATE_TIME, updateTime).apply();
//			}
//		}
//	}
//
//	private void showUpdatesList(Context context)
//	{
//		UpdatesDialogFragment updatesDialogFragment = new UpdatesDialogFragment();
//		updatesDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
//		updatesDialogFragment.show(getSupportFragmentManager(), "tagNewEvents");
//	}
//
//
//	public void checkStates(Context context, SharedPreferences sp){
//		DBHelper dbHelper  =DBHelper.getInstance(context);
//		UserInfo userInfo = dbHelper.getUserInfo();
//		if (((userInfo.roles&Const.Roles.TRINKET) == 0) && (Func.getBooleanSPDefTrue(sp, "notif_inapp_am")))
//		{
//			Func.log_d("D3Service", "Not trinket and active in-app notifications");
//
//			boolean seen = Func.getBooleanSPDefFalse(sp, "all_events_were_seen");
//			Func.log_d("D3Service", "All_events_were_seen: " + seen);
//			Func.log_d("D3Service", "Current_time: " + System.currentTimeMillis()/1000);
//			Func.log_d("D3Service", "A_pause_time: " + sp.getLong("app_pause_time", 0));
//			Func.log_d("D3Service", "Last_activity_closed_time: " + sp.getLong("last_activity_closed_time", (System.currentTimeMillis() - D3Service.EVENTS_TIME_INTERVAL)/1000));
//
//			if (((!seen) || (System.currentTimeMillis()/1000 - sp.getLong("app_pause_time", 0) > 5))
//					&& dbHelper.checkCritEventsExistence(sp.getLong("last_activity_closed_time", (System.currentTimeMillis() - D3Service.EVENTS_TIME_INTERVAL)/1000)))
//			{
//				Func.log_d("D3Service", "New alarms");
//				showNewEventsFrame(context, sp);
//			}else{
//				Func.log_d("D3Service", "No new alarms");
////                Func.log_d("D3Service", "All events were seen in main cause no new events");
//				sp.edit().putBoolean("all_events_were_seen", true).apply();
//			}
//		}
//	}
//
//	private void showNewEventsFrame(final Context context, SharedPreferences sp)
//	{
//		if(null != newEventDialogFragment
//		&& null != newEventDialogFragment.getDialog()
//		&& newEventDialogFragment.getDialog().isShowing()
//		&& !newEventDialogFragment.isRemoving())
//		{
//			newEventDialogFragment.updateList();
//		}else{
//			showNewDialog(context, sp);
//		}
//	}
//
//	private void showNewDialog(final Context context, SharedPreferences sp)
//	{
//		newEventDialogFragment = new StateDialogFragment();
//		newEventDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
//		newEventDialogFragment.setListener(new StateDialogFragment.OnClickListener()
//		{
//			@Override
//			public void callback(int id)
//			{
//				UserInfo userInfo = dbHelper.getUserInfo();
//				if (dbHelper.getDeviceConnectionStatus(id))
//				{
//					int command_count = Func.getCommandCount(context);
//					D3Request d3Request = D3Request.createCommand(userInfo.roles, id, "REBOOT", new JSONObject(), command_count);
//					if (d3Request.error == null)
//					{
//						Func.sendMessageToServer(d3Request, getLocalService(), context, true, sending);
//					} else
//					{
//						Func.pushToast(context, d3Request.error, (Activity) context);
//					}
//				} else
//				{
//					Func.nShowMessage(context, getString(R.string.EA_NO_CONNECTION_MESSAGE));
//				}
//			}
//		});
//		newEventDialogFragment.setEventClickListener(new StateDialogFragment.OnEventClickListener()
//		{
//			@Override
//			public void callback(int event_id)
//			{
//				Func.showEventInfoDialog(context, event_id);
//			}
//		});
//		newEventDialogFragment.show(getSupportFragmentManager(), "tagNewEvents");
//	}


	private void setStatusConnected(){
		if(null!=myService)
		{
			final RelativeLayout noConnLayout = (RelativeLayout) findViewById(R.id.noConnLayout);
			if (myService.getStatus())
			{
				if (null != noConnLayout) noConnLayout.setVisibility(View.GONE);
				pager.setVisibility(View.VISIBLE);
			} else
			{
				if (null != noConnLayout) noConnLayout.setVisibility(View.VISIBLE);
				pager.setVisibility(View.GONE);
			}
		}
	}

	public void setFabStatus(boolean status){
		fabShown = status;
	}

	public Drawer getDrawer(){
		return  drawerResult;
	}

	@Override
	public void onBackPressed()
	{
		if ((drawerResult != null) && (drawerResult.isDrawerOpen()))
		{
			drawerResult.closeDrawer();
		}else{
			if(fabShown){
				adapter.closeFab();
			}
			else
			{
				AlertDialog.Builder adb = Func.adbForCurrentSDK(context);
				adb.setMessage(R.string.EXIT_FROM_APP_MESSAGE);
				adb.setPositiveButton(getResources().getString(R.string.YES), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialogInterface, int i)
					{
						finishAffinity();
					}
				});
				adb.setNegativeButton(getResources().getString(R.string.NO), new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialogInterface, int i)
					{
						dialogInterface.dismiss();
					}
				});
				adb.show();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
	}
}
