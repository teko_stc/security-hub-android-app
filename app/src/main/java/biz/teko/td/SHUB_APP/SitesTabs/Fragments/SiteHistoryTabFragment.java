package biz.teko.td.SHUB_APP.SitesTabs.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.D3DB.UserInfo;
import biz.teko.td.SHUB_APP.Events.Adapters.UniEventListAdapter;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Const;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 07.02.2017.
 */

public class SiteHistoryTabFragment extends Fragment
{
	View view;
	private DBHelper dbHelper;
	private UniEventListAdapter eventListAdapter;
	private FrameLayout frameLayout;
	private ListView eventList;
	private TextView t;
	private Cursor cursor;
	private Context context;
	private UserInfo userInfo;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		userInfo = dbHelper.getUserInfo();

		//MAIN LAYOUT
		frameLayout =  (FrameLayout) new FrameLayout(container.getContext());
		frameLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		eventList = new ListView(container.getContext());
		eventList.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
		t = new TextView(container.getContext());
		t.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
		t.setGravity(Gravity.CENTER);
		t.setPadding(Func.dpToPx(20, context),Func.dpToPx(20, context), Func.dpToPx(20, context), Func.dpToPx(20, context));
		t.setVisibility(View.GONE);
		t.setText(R.string.HISTORY_NO_EVENTS);
		frameLayout.addView(t);

		refreshList();

		view = frameLayout;
		return view;
	}

	private void refreshList(){
		ArrayList<Integer> classes = Func.getCurrentHistoryClasses(context);
		Cursor cursor = getRefreshedCursor(classes);
		if (null != cursor && cursor.getCount() > 0)
		{
			if (null != eventList) eventList.setVisibility(View.VISIBLE);
			if (null != t) t.setVisibility(View.GONE);
			if (null != eventListAdapter)
			{
				eventListAdapter.update(cursor);
			} else
			{
				eventListAdapter = new UniEventListAdapter(context, R.layout.card_event_uni, cursor, 0, 0, -1);
				if (null != eventList)
				{
					eventList.setAdapter(eventListAdapter);
					frameLayout.addView(eventList);
				}
			}
		} else
		{
			if (null != eventList) eventList.setVisibility(View.GONE);
			if (null != t) t.setVisibility(View.VISIBLE);
		}
	}

	private Cursor getRefreshedCursor(ArrayList<Integer> classes){
		return (0==(userInfo.roles& Const.Roles.TRINKET)) ? dbHelper.getAllEventsCursorForCurrentFilter(classes): dbHelper.getAllActualEventsCursorFromDB();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		refreshList();
		getActivity().registerReceiver(eventsReceiver, new IntentFilter(D3Service.BROADCAST_EVENT));
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getActivity().unregisterReceiver(eventsReceiver);
	}

	private BroadcastReceiver eventsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			refreshList();
		}
	};
}
