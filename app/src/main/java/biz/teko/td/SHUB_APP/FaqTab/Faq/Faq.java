package biz.teko.td.SHUB_APP.FaqTab.Faq;

import android.content.Context;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.InputStream;
import java.util.LinkedList;

import biz.teko.td.SHUB_APP.R;

/**
 * Created by td13017 on 11.02.2017.
 */

public class Faq
{
	@ElementList(name ="chapters", entry="chapter")
	public LinkedList<FChapter> fChapters;

	public FChapter getChapterById(int id){
		LinkedList<FChapter> fChapters = this.fChapters;
		for(FChapter fChapter:fChapters){
			if(fChapter.id==id){
				return fChapter;
			}
		}
		return null;
	}

	public static Faq load(Context context){
		InputStream inputStream = context.getResources().openRawResource(R.raw.faq);
		Serializer serializer = new Persister();
		try
		{
			return serializer.read(Faq.class, inputStream);
		}catch (Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
