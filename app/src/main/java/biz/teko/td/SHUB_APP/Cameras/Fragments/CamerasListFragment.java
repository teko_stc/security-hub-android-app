package biz.teko.td.SHUB_APP.Cameras.Fragments;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import biz.teko.td.SHUB_APP.Cameras.Adapters.NIVCamerasCursorAdapter;
import biz.teko.td.SHUB_APP.D3.D3Service;
import biz.teko.td.SHUB_APP.D3DB.DBHelper;
import biz.teko.td.SHUB_APP.HTTP.Helpers.WebHelper;
import biz.teko.td.SHUB_APP.R;
import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 15.09.2017.
 */

public class CamerasListFragment extends Fragment
{
	private Context context;

//	private static String URL_GET_CAMERAS = "http://openapi-alpha-eu01.ivideon.com/cameras?";
//	private static String URL_GET_CAMERAS_PARAMS = "http://openapi-alpha-eu01.ivideon.com/cameras?op=FIND&access_token=100-Ue9ce0c14-f36b-4df8-ae08-7325c1f9e986";
//	private static String URL_START_RECORDING = "http://openapi-alpha-eu01.ivideon.com/cameras/XXX/?op=REC&access_token=";
//	private static String token ="100-Ue9ce0c14-f36b-4df8-ae08-7325c1f9e986";

	private WebHelper webHelper;
	private DBHelper dbHelper;
	private RecyclerView camerasList;
	private Cursor camerasCursor;
	private NIVCamerasCursorAdapter camerasListAdapter;
	private D3Service myService;


	private final BroadcastReceiver camerasListRelationsReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
		if(camerasList!=null) {
			camerasCursor = dbHelper.getIVCamerasCursor();
			if (null != camerasCursor)
			{
				if (camerasListAdapter!=null){
					camerasListAdapter.update(camerasCursor);
				}else{
					camerasListAdapter = new NIVCamerasCursorAdapter(context,  camerasCursor);
					camerasList.setAdapter(camerasListAdapter);
				}
			}

		}
		}
	};

	/*WHEN we get new token from server with info
	* how it feels
	* if it good - get cameras list
	* bad - get safe session and open login page*/

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.tab_cameras_list, container, false);
		context = container.getContext();
		dbHelper = DBHelper.getInstance(context);
		camerasCursor = dbHelper.getIVCamerasCursor();
		camerasList = v.findViewById(R.id.camerasList);
		//camerasList.setDivider(null);
		if(null!=camerasCursor && camerasCursor.getCount()!=0)
		{
			camerasListAdapter = new NIVCamerasCursorAdapter(context, camerasCursor);
			camerasList.setAdapter(camerasListAdapter);
			v.findViewById(R.id.textNoCamerasInAcc).setVisibility(View.GONE);
			camerasList.setVisibility(View.VISIBLE);
		}else{
			v.findViewById(R.id.textNoCamerasInAcc).setVisibility(View.VISIBLE);
			camerasList.setVisibility(View.GONE);
		}
		return v;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		context.registerReceiver(camerasListRelationsReceiver, new IntentFilter(D3Service.BROADCAST_IV_GET_RELATIONS));
		if(camerasList!=null) {
			camerasCursor = dbHelper.getIVCamerasCursor();
			if (null != camerasCursor)
			{
				if (camerasListAdapter!=null){
					camerasListAdapter.update(camerasCursor);
				}else{
					camerasListAdapter = new NIVCamerasCursorAdapter(context, camerasCursor);
					camerasList.setAdapter(camerasListAdapter);
				}
			}

		}
	}

	@Override
	public void onPause()
	{
		super.onPause();

		context.unregisterReceiver(camerasListRelationsReceiver);

		Func.clearCache(context);
	}

	/*
* STANDART APP CONNECTIONS
*
* */
//	public void getCamerasAsync(){
//		new AsyncTask<Object, Object, Void>(){
//
//			@Override
//			protected Void doInBackground(Object... params)
//			{
//				try
//				{
//					URL url = new URL(URL_GET_CAMERAS_PARAMS);
//
//					HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//
//					urlConnection.setReadTimeout(10000);
//					urlConnection.setConnectTimeout(15000);
//					urlConnection.setRequestMethod("POST");
//					urlConnection.setDoInput(true);
//					urlConnection.setDoOutput(true);
//
////					List<AbstractMap.SimpleEntry> data = new ArrayList<AbstractMap.SimpleEntry>();
////					data.add(new AbstractMap.SimpleEntry<>("op", "FIND"));
////					data.add(new AbstractMap.SimpleEntry<>("access_token", token));
//
////					OutputStream os = urlConnection.getOutputStream();
////					BufferedWriter writer = new BufferedWriter(
////							new OutputStreamWriter(os, "UTF-8"));
////					writer.write(getQuery(data));
////					writer.flush();
////					writer.close();
////					os.close();
////
//					urlConnection.connect();
//
//
//					int status = urlConnection.getResponseCode();
//					if(status >= urlConnection.HTTP_BAD_REQUEST)
//					{
//						InputStream in = new BufferedInputStream(urlConnection.getErrorStream());						Func.log_v(D3Service.LOG_TAG, "GOT IT !!");
//						Func.log_v(D3Service.LOG_TAG, "GOT ERROR !!");
//					}
//					else
//					{
//						Func.log_v(D3Service.LOG_TAG, "GOT IT !!");
//						BufferedReader buf = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//						StringBuilder builderLine = new StringBuilder();
//						String line;
//						while ((line = buf.readLine()) != null) {
//							builderLine.append(line).append('\n');
//						}
//						line = builderLine.toString();
//						Func.log_v(D3Service.LOG_TAG, "GOT IT !! \n" + line);
//						urlConnection.disconnect();
//					}
//
//
//				} catch (IOException e)
//				{
//					e.printStackTrace();
//				}
//				return null;
//			}
//		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null);
//	}
//
//	private String getQuery(List<AbstractMap.SimpleEntry> params) throws UnsupportedEncodingException
//	{
//		StringBuilder result = new StringBuilder();
//		boolean first = true;
//		for(AbstractMap.SimpleEntry entry : params){
//			if (first)
//				first = false;
//			else
//				result.append("&");
//
//			result.append(URLEncoder.encode((String) entry.getKey(), "UTF-8"));
//			result.append("=");
//			result.append(URLEncoder.encode((String) entry.getTitle(), "UTF-8"));
//		}
//
//		return result.toString();
//	}
}
