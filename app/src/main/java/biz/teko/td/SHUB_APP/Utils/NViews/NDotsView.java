package biz.teko.td.SHUB_APP.Utils.NViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import biz.teko.td.SHUB_APP.R;

public class NDotsView extends LinearLayout
{
	private static final int DEFAULT_COUNT = 4;

	private final Context context;
	private int layout;
	private int total_count;
	private int selected_count = 0;
	private NDotView[] views;

	public NDotsView(Context context)
	{
		super(context);
		this.context = context;
	}

	public NDotsView(Context context, @Nullable AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		parseAttributes(attrs);
		setup();
	}

	private void setup()
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(layout, this, true);

		fillViews();

		setOrientation(HORIZONTAL);
	}

	private void fillViews()
	{
		views = new NDotView[total_count];
		for(int i = 0; i  < total_count; i++)
		{
			views[i] = new NDotView(context, R.layout.n_dot_view);
			addView(views[i]);
		}
	}


	private void updateViews()
	{
		for(int i=0; i< total_count; i++){
			views[i].setFilled(i<selected_count);
		}
	}

	public boolean increase(){
		if(selected_count!=total_count) {
			selected_count++;
			updateViews();
			return true;
		}
		return false;
	}


	public void decrease(){
		if(selected_count!=0)selected_count--;
		updateViews();
	}

	public int getCount(){
		return selected_count;
	}

	private void parseAttributes(AttributeSet attrs)
	{
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.NDotsView, 0 ,0);
		try{
			layout = a.getResourceId(R.styleable.NDotsView_NDotsLayout, 0);
			total_count = a.getInt(R.styleable.NDotsView_NDotsCount, DEFAULT_COUNT);
		}finally
		{
			a.recycle();
		}
	}

	public int getTotalCount()
	{
		return total_count;
	}

	public void setCount(int count)
	{
		this.selected_count  = count;
		updateViews();
	}
}
