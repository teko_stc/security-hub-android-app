package biz.teko.td.SHUB_APP.D3.D3UniConfig;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;

/**
 * Created by td13017 on 29.06.2016.
 */
public class FieldNode extends NamedNode{
	@Attribute(name="address")
	public int Address;

	@Attribute(name="size")
	public int Size;

	@Attribute(name="count", required = false)
	public int Count;

	@Attribute(name="offset", required = false)
	public int Offset;

	@Element(name="source", required = false)
	public TextNode Source;
}
