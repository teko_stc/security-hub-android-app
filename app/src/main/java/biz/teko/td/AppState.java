package biz.teko.td;

import android.database.Cursor;

public class AppState
{
	private static Cursor activeLostCursor;
	private static Cursor activeCursor;
	private static Cursor repairCursor;

//	public static void checkStates(Context context, SharedPreferences sp){
//		DBHelper dbHelper  =DBHelper.newInstance(context);
//		UserInfo userInfo = dbHelper.getUserInfo();
//		if (((userInfo.roles&Const.Roles.TRINKET) == 0) && (Func.getBooleanSPDefTrue(sp, "notif_inapp_am")))
//		{
//			Func.log_d("D3Service", "Not trinket and active in-app notifications");
//			activeLostCursor = dbHelper.getNewActiveLostEvents();
//			if (!Func.getBooleanSPDefFalse(sp, "all_events_were_seen") || (activeLostCursor != null && activeLostCursor.getCount() >0))
//			{
//				Func.log_d("D3Service", "Haven't seen all events");
//				long time = sp.getLong("last_activity_closed_time", System.currentTimeMillis() - D3Service.EVENTS_TIME_INTERVAL);
//				activeCursor = dbHelper.getNewActiveAttentionEvents(time);
//				repairCursor = dbHelper.getNewRepairedAttentionEvents(time);
//
//				if((null!=activeLostCursor && 0!=activeLostCursor.getCount())
//						|| (null!=activeCursor && 0!=activeCursor.getCount())
//						|| (null!=repairCursor && 0!= repairCursor.getCount()))
//				{
//					Func.log_d("D3Service", "New alarms");
//					showNewEventsFrame(context, sp);
//				}else{
//					Func.log_d("D3Service", "No new alarms");
//					Func.log_d("D3Service", "All events were seen in main cause no new events");
//					sp.edit().putBoolean("all_events_were_seen", true).apply();
//				}
//			}
//		}
//
//	}
//
//	private static void showNewEventsFrame(final Context context, SharedPreferences sp)
//	{
//		NewEventDialogFragment newEventDialogFragment = new NewEventDialogFragment();
//		newEventDialogFragment.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
//		newEventDialogFragment.show(getSupportFragmentManager(), "tagNewEvents");
//
////
//	}

//	public static D3Service getService(Context context)
//	{
//		Activity activity = (SitesActivity)context;
//		if(activity != null){
//			SitesActivity mainActivity = (SitesActivity) activity;
//			return mainActivity.getLocalService();
//		}
//		return null;
//	}
//
//
//	public static class NewEventDialogFragment extends DialogFragment
//	{
//
//		private int sending = 0;
//		private D3Service myService;
//		private Context context;
//
//		@Nullable
//		@Override
//		public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
//		{
//
//			context = getContext();
//			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
//
//			View view = ((Activity) context).getLayoutInflater().inflate(R.layout.frame_new_events, null);
//			ImageView closeHeader = (ImageView) view.findViewById(R.id.closeButton);
//			TextView closeFooter = (TextView) view.findViewById(R.id.footerCloseButton);
//			if(null!=closeFooter) closeFooter.setOnClickListener(new View.OnClickListener()
//			{
//				@Override
//				public void onClick(View view)
//				{
//					dismiss();
//				}
//			});
//			if(null!=closeHeader) closeHeader.setOnClickListener(new View.OnClickListener()
//			{
//				@Override
//				public void onClick(View view)
//				{
//					dismiss();
//				}
//			});
//
//			sp.edit().putBoolean("all_events_were_seen", true).apply();
//			long time = sp.getLong("last_activity_closed_time", System.currentTimeMillis() - D3Service.EVENTS_TIME_INTERVAL); // TODO уменьшить интервал, слишком много событий
//			Func.log_v("D3ServiceLog", "GET CLOSE TIME:" + time);
//
//			TextView message = (TextView) view.findViewById(R.id.textCriticalMessage);
//			TextView activeTitle = (TextView) view.findViewById(R.id.title_current);
//			TextView repairTitle = (TextView) view.findViewById(R.id.title_repair);
//			ListView activeList = (ListView) view.findViewById(R.id.list_current);
//			ListView repairList = (ListView) view.findViewById(R.id.list_repair);
//			final ListView lostList = (ListView) view.findViewById(R.id.list_reboot);
//			lostList.setDivider(null);
//
//			int a_count = activeCursor!=null? activeCursor.getCount():0;
//			int l_count = activeLostCursor!=null? activeLostCursor.getCount():0;
//			int r_count = repairCursor!=null? repairCursor.getCount():0;
//
//			if(l_count!=0){
//				AlarmsListAdapter activeListAdapter = new AlarmsListAdapter((Activity)context, R.layout.card_reboot_device, activeLostCursor, 0, true);
//				lostList.setAdapter(activeListAdapter);
//				lostList.setOnItemClickListener(new AdapterView.OnItemClickListener()
//				{
//					@Override
//					public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
//					{
//						Cursor c = (Cursor) lostList.getItemAtPosition(i);
//						if(null!=c){
//							final DBHelper dbHelper = DBHelper.getInstance((Activity)context);
//							final Device device = dbHelper.getDeviceById(c.getInt(2));
//
//							final LinearLayout infoFrame = (LinearLayout) view.findViewById(R.id.infoView);
//							final LinearLayout dialogFrame = (LinearLayout) view.findViewById(R.id.dialogView);
//							if(null!=infoFrame) infoFrame.setVisibility(View.INVISIBLE);
//							if(null!=dialogFrame) dialogFrame.setVisibility(View.VISIBLE);
//
//							TextView buttonPositive = (TextView) view.findViewById(R.id.buttonPositive);
//							TextView buttonNegative = (TextView) view.findViewById(R.id.buttonNegative);
//
//							buttonPositive.setOnClickListener(new View.OnClickListener()
//							{
//								@Override
//								public void onClick(View view)
//								{
//									UserInfo userInfo = dbHelper.getUserInfo();
//									if (dbHelper.getDeviceConnectionStatus(device.id))
//									{
//										int command_count = Func.getCommandCount(context);
//										D3Request d3Request = D3Request.createCommand(userInfo.roles, device.id, "REBOOT", new JSONObject(), command_count);
//										if (d3Request.error == null)
//										{
//											Func.sendMessageToServer(d3Request, getService(context), context, true, sending);
//										} else
//										{
//											Func.pushToast(context, d3Request.error, (Activity) context);
//										}
//									} else
//									{
//										Func.nShowMessage(context, getContext().getString(R.string.EA_NO_CONNECTION_MESSAGE));
//									}
//								}
//							});
//
//							buttonNegative.setOnClickListener(new View.OnClickListener()
//							{
//								@Override
//								public void onClick(View view)
//								{
//									if(null!=infoFrame) infoFrame.setVisibility(View.VISIBLE);
//									if(null!=dialogFrame) dialogFrame.setVisibility(View.INVISIBLE);
//								}
//							});
//						}
//					}
//				});
//			}else{
//				lostList.setVisibility(View.GONE);
//			}
//
//			boolean empty = true;
//			if(a_count!=0){
//				empty = false;
//				AlarmsListAdapter activeListAdapter = new AlarmsListAdapter((Activity)context, R.layout.card_alarm, activeCursor, 0, true);
//				activeList.setAdapter(activeListAdapter);
//			}else{
//				activeTitle.setVisibility(View.GONE);
//				activeList.setVisibility(View.GONE);
//			}
//			if(r_count!=0){
//				empty = false;
//				AlarmsListAdapter repairListAdapter = new AlarmsListAdapter((Activity)context, R.layout.card_repaired_alarm, repairCursor, 0, false);
//				repairList.setAdapter(repairListAdapter);
//			}else{
//				repairTitle.setVisibility(View.GONE);
//				repairList.setVisibility(View.GONE);
//			}
//			if(!empty)message.setText(R.string.MESSAGE_CRITICAL_EVENTS);
//			setDynamicHeight(activeList);
//			setDynamicHeight(repairList);
//			setDynamicHeight(lostList);
//			return view;
//		}
//	}
}
