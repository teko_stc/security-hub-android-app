package biz.teko.td;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.View;

import biz.teko.td.SHUB_APP.R;

public class AppUpdate
{
	public static void appUpdated(Context context, SharedPreferences sp){

	}

	private static void showUpdatesList(Context context)
	{
		AlertDialog.Builder adb = new AlertDialog.Builder(context, R.style.DialogFragmentTheme);
		View view = ((Activity)context).getLayoutInflater().inflate(R.layout.frame_updated_dialog, null);
		adb.setView(view);
		AlertDialog dialog = adb.create();
		dialog.show();

	}
}
