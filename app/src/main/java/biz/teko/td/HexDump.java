package biz.teko.td;

/**
 * Created by td13017 on 29.07.2016.
 */
public class HexDump
{
	private static int charNibble(char c){
		return
				c >= '0' && c <= '9'
				? (c - '0')
				: (
					c >= 'A' && c<='F'
						? (c + 10 - 'A' )
						: (
							c >= 'a' && c <= 'f'
								? (c + 10 - 'a' )
								: 0
						)
				);
	}

	private static char nibbleChar(byte b){
		final char[]  hex = {'0', '1', '2', '3' ,'4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
		return b < 16 ? hex[b] : 0;
	}

	public static int[] hexBin(String string){
		int l = string.length() & 0xFFFFFFFE;
		int[] b = new int[l>>1];
		for(int i = 0; i < l; i+=2){
			b[i>>1] = ((charNibble(string.charAt(i)))<<4) +(charNibble(string.charAt(i+1)));
		}
		return b;
	};

	public static  String binHex (int[] bytes){
		int l = bytes.length;
		String s = "";
		for (int b : bytes)
		{
			s += nibbleChar((byte) (b >> 4));
			s += nibbleChar((byte) (b & 0x0F));
		}
		return s;
	}

	public  static  void putBites(){};

}
