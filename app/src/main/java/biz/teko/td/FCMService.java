package biz.teko.td;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import biz.teko.td.SHUB_APP.Utils.Dir.Func;

/**
 * Created by td13017 on 28.04.2018.
 */

public class FCMService extends FirebaseMessagingService
{
	@Override
	public void handleIntent(Intent intent)
	{
		super.handleIntent(intent);
		Func.log_d("D3Service" , "FCM NOTIFICATION MESSAGE: ");

	}

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		/*TODO add data analization to catch CONN_DROP*/
		Func.log_d("D3Service" , "FCM DATA MESSAGE: " + remoteMessage.getMessageId());
//		Func.restartD3Service(getApplicationContext(),  "FCMService");
		Func.restartD3Service(getBaseContext(),  "FCMService");
	}
}

